<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.2.1 2015-01-28
// version 1.3 2015-02-15
// version 1.6 2016-04-09
// version 1.6 2017-07-30

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_attributes.php

include_once("lib/standard.php");

class PlantAttributes extends Standard {

  // given
  private $given_plant_id;

  // given_plant_id
  public function set_given_plant_id($var) {
    $this->given_plant_id = $var;
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // attributes
  private $id;
  private $plant_obj;
  private $attribute_name;
  private $attribute_value;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // attribute_name
  public function set_attribute_name($var) {
    $this->attribute_name = $var;
  }
  public function get_attribute_name() {
    return $this->attribute_name;
  }

  // attribute_value
  public function set_attribute_value($var) {
    $this->attribute_value = $var;
  }
  public function get_attribute_value() {
    return $this->attribute_value;
  }

  // method
  private function make_plant_attribute() {
    $obj = new PlantAttributes($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_id()) {
      $this->set_type("get_by_plant_id");
      
    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug builds type = " . $this->get_type() . "<br />\n";

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // set order_by
    $order_by = " ORDER BY plant_attributes.id";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT plant_attributes.* FROM plant_attributes WHERE plant_attributes.id = " . $this->get_given_id() . ";";

      // debug
      print "debug plant_attributes sql = " . $sql . "\n";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT plant_attributes.* FROM plant_attributes WHERE plant_attributes.plant_id = " . $this->get_given_plant_id() . " ORDER BY attribute_name;";
      
    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_attributes.* FROM plant_attributes " . $order_by . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_attribute();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_attribute_name(pg_result($results, $lt, 2));
        $obj->set_attribute_value(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_given_variables() {
    $markup = "";

    $markup .= parent::output_given_variables();

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_table($given_type = "") {
    $markup = "";

    include_once("plants.php");
    $plant_obj = new Plants($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $plant_data = $plant_obj->get_name_with_link_given_id($this->get_given_plant_id(), $user_obj);
    if ($given_type == "simple") {
      // simple format
      // loop to make data cells
      $markup .= "<table class=\"plants\">\n";
      $markup .= "<tr>\n";
      $markup .= "<td class=\"header\">";
      $markup .= "name";
      $markup .= "</td>\n";
      $markup .= "<td class=\"header\">";
      $markup .= "value";
      $markup .= "</td>\n";
      $markup .= "</tr>\n";
      foreach ($this->get_list_bliss()->get_list() as $obj) {  
        $markup .= "<tr>\n";
        // name
        $markup .= "<td>";
        $markup .= $obj->get_attribute_name();
        $markup .= "</td>\n";
        // value
        $markup .= "<td>";
        $markup .= $obj->get_attribute_value();
        $markup .= "</td>\n";
        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
    } else {
      // default format (table)
      $markup .= "<h2>given plant: " . $plant_data . "</h2>\n";
      // build table
      include_once("lib/html_table.php");
      $table_obj = new HtmlTable();
      $table_attribute_class = "plants";
      $table_type = "";
      // loop to make data cells
      foreach ($this->get_list_bliss()->get_list() as $obj) {
        // make data cells
        $row_matrix = array(
          array("", $obj->get_attribute_name(), "", ""),
          array("", $obj->get_attribute_value(), "", ""),
        );
        $table_obj->make_row($row_matrix);
      }
      $markup .= $table_obj->craft_table($table_attribute_class, $table_type);
    }

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    return $markup;
  }

  // method
  public function get_plant_attribute_list_given_plant_id($given_plant_id, $given_user_obj, $given_type = "") {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_table($given_type);
    }

    return $markup;
  }

  // method
  public function get_plant_attribute_count_given_plant_id($given_plant_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    $markup .= $this->get_list_bliss()->get_count();

    return $markup;
  }

  // method
  public function output_subsubmenu() {
    return "";
  }

  // method
  public function get_plant_attribute_count_given_plant_id_and_attribute_set
($given_plant_id, $given_attribute_set, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $obj) {
 
      if ($given_attribute_set == "B") {
        if ($obj->get_attribute_name() == "wildlife") {
          return 1;
        }
        if ($obj->get_attribute_name() == "invertebrates shelter") {
          return 1;
        }
        if ($obj->get_attribute_name() == "nectory") {
          return 1;
        }
      }
      if ($given_attribute_set == "NF") {
        if ($obj->get_attribute_name() == "use" && $obj->get_attribute_value() == "nitrogen fixer") {
          return 1;
        }
      }
      if ($given_attribute_set == "DP") {
        if ($obj->get_attribute_name() == "use" && $obj->get_attribute_value() == "Aromic Pest Confuser") {
          return 1;
        } else if ($obj->get_attribute_name() == "use" && $obj->get_attribute_value() == "detracts voles") {
          return 1;
        } else if ($obj->get_attribute_name() == "use" && $obj->get_attribute_value() == "detracts wireworm") {
          return 1;
        } else if ($obj->get_attribute_name() == "use" && $obj->get_attribute_value() == "insect repellent") {
          return 1;
        }
      }
      if ($given_attribute_set == "DA") {
        if ($obj->get_attribute_name() == "dynamic accumulator") {
          return 1;
        }
      }
      if ($given_attribute_set == "F") {
        if ($obj->get_attribute_name() == "tea") {
          return 1;
        } else if ($obj->get_attribute_name() == "edible") {
          return 1;
        } else if ($obj->get_attribute_name() == "edible greens") {
          return 1;
        } else if ($obj->get_attribute_name() == "medicinal") {
          return 1;
        } else if ($obj->get_attribute_name() == "culinary") {
          return 1;
        }
      }
      if ($given_attribute_set == "NS") {
        if ($obj->get_attribute_name() == "native") {
          return 1;
        }
      } 
   }

    return $markup;
  }

}
