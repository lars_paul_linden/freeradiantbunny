<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/books.php

include_once("lib/socation.php");

class Books extends Socation {

  // attribute
  private $url;
  private $scene_element_obj;

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // scene_element_obj
  private function get_scene_element_obj() {
    if (! isset($this->scene_element_obj)) {
      include_once("scene_elements.php");
      $this->scene_element_obj = new SceneElements($this->get_given_config());
    }
    return $this->scene_element_obj;
  }

  // method
  private function make_book() {
    $obj = new Books($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT books.* FROM books WHERE books.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT books.* FROM books;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT DISTINCT ON (books.sort, books.id) books.* FROM books, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'books' AND books.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY books.sort DESC, books.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_book();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_url(pg_result($results, $lt, 2));
        $obj->set_description(pg_result($results, $lt, 3));
        $obj->set_sort(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    #$markup .= "  <td class=\"header\">\n";
    #$markup .= "    project\n";
    #$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    url\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $book) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // projects
      if (0) {
        $markup .= "  <td>\n";
        $padding = "";
        $float = "";
        $width = "65";
        // todo fix undefined method
        //$markup .= "    " . $book->get_project_book_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
        $markup .= "  </td>\n";
       }

      // status
      // todo problem is that the font-size is a poor fix for big string issue
      $markup .= "  <td style=\"text-align: center; background-color: " . $book->get_status_background_color() . "; font-size: 50%;\">\n";
      $markup .= "    " . $book->get_status() . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= "  <td>\n";
      $markup .= "    " . $book->get_sort() . "\n";
      $markup .= "  </td>\n";

      // id with link
      $markup .= "  <td>\n";
      $markup .= "    " . $book->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // name with link
      $markup .= "  <td>\n";
      $markup .= "    " . $book->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // book url
      $markup .= "  <td>\n";
      if ($book->get_url()) {
        $markup .= "    <a href=\"" . $book->get_url() . "\">" . $book->get_url() . "</a>\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $book) {

      // project
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    project\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $book->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $book->get_id_with_link() . "\n";
      $markup .= "</tr>\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $book->get_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // description
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $book->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // url
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      if ($book->get_url()) {
        $markup .= "    <a href=\"" . $book->get_url() . "\">" . $book->get_url() . "</a>\n";
      }
      $markup .= "</tr>\n";

      // scene_element processes
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    scene element related processes\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      include_once("scene_elements.php");
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $book->get_scene_element_obj()->get_processes_given_scene_element(__class__, $book->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug books: ignore, not related<br />\n";
    } else {
      $markup .= "debug books: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug books: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error books: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $class) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $class->get_id() . " " . $class->get_name_with_link() . " documentation " . $class->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $class->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error books: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "books: no books found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

}
