<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-11
// version 1.6 2016-12-24
// version 1.8 2017-04-29
// version 1.8 2017-07-10

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/webpage_maxonomies.php

include_once("lib/sproject.php");

class WebpageMaxonomies extends Sproject {

  // given
  private $given_domain_tli;
  private $given_webpage_id;
  private $given_maxonomy_id;
  private $given_categorization;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_webpage_id
  public function set_given_webpage_id($var) {
    $this->given_webpage_id = $var;
  }
  public function get_given_webpage_id() {
    return $this->given_webpage_id;
  }

  // given_maxonomy_id
  public function set_given_maxonomy_id($var) {
    $this->given_maxonomy_id = $var;
  }
  public function get_given_maxonomy_id() {
    return $this->given_maxonomy_id;
  }

  // given_categorization
  public function set_given_categorization($var) {
    $this->given_categorization = $var;
  }
  public function get_given_categorization() {
    return $this->given_categorization;
  }

  // attributes
  private $id;
  private $webpage_obj;
  private $maxonomy_obj;
  private $ocm;

  // id
  private function set_id($var) {
    $this->id = $var;
  }
  private function get_id() {
    return $this->id;
  }

  // webpage_obj
  private function get_webpage_obj() {
    if (! isset($this->webpage_obj)) {
      include_once("webpages.php");
      $this->webpage_obj = new Webpages($this->get_given_config());
    }
    return $this->webpage_obj;
  }

  // maxonomy_obj
  public function get_maxonomy_obj() {
    if (! isset($this->maxonomy_obj)) {
      include_once("maxonomies.php");
      $this->maxonomy_obj = new Maxonomies($this->get_given_config());
    }
    return $this->maxonomy_obj;
  }

  // ocm
  private function set_ocm($var) {
    $this->ocm = $var;
  }
  private function get_ocm() {
    return $this->ocm;
  }

  // method
  private function make_webpage_maxonomy() {
    $obj = new WebpageMaxonomies($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else if ($this->get_given_webpage_id()) {
      $this->set_type("get_by_webpage_id");

    } else if ($this->get_given_maxonomy_id()) {
      $this->set_type("get_by_maxonomy_id");

    } else if ($this->get_given_categorization()) {
      $this->set_type("get_by_categorization");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_maxonomies.*, maxonomies.*, domains.tli, domains.name, domains.img_url FROM webpage_maxonomies, maxonomies, webpages, domains WHERE webpage_maxonomies.maxonomy_id = maxonomies.id AND webpage_maxonomies.webpage_id = webpages.id AND webpages.temp_domain_tli = domains.tli AND webpage_maxonomies.id = " . $this->get_given_id() . " AND domains.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_maxonomies.*, maxonomies.*, domains.tli, domains.name, domains.img_url FROM webpage_maxonomies, maxonomies, webpages, domains WHERE webpage_maxonomies.maxonomy_id = maxonomies.id AND webpage_maxonomies.webpage_id = webpages.id AND webpages.temp_domain_tli = domains.tli AND domains.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_by_webpage_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_maxonomies.*, maxonomies.*, domains.tli, domains.name, domains.img_url FROM webpage_maxonomies, maxonomies, webpages, domains WHERE webpage_maxonomies.maxonomy_id = maxonomies.id AND webpage_maxonomies.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND webpages.id = " . $this->get_given_webpage_id() . " AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY maxonomies.how_to_measure;";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_maxonomies.*, maxonomies.*, domains.tli, domains.name, domains.img_url FROM webpage_maxonomies, maxonomies, webpages, domains WHERE webpage_maxonomies.maxonomy_id = maxonomies.id AND webpage_maxonomies.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND domains.tli = '" . $this->get_given_domain_tli() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY maxonomies.how_to_measure;";

      // debug
      //print "debug webpage_maxonomies sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_maxonomy_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_maxonomies.*, maxonomies.*, domains.tli, domains.name, domains.img_url FROM webpage_maxonomies, maxonomies, webpages, domains WHERE webpage_maxonomies.maxonomy_id = maxonomies.id AND webpage_maxonomies.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND maxonomies.id = " . $this->get_given_maxonomy_id() . " ORDER BY maxonomies.how_to_measure;";

    } else if ($this->get_type() == "get_by_categorization") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_maxonomies.*, maxonomies.*, domains.tli, domains.name, domains.img_url FROM webpage_maxonomies, maxonomies, webpages, domains WHERE webpage_maxonomies.maxonomy_id = maxonomies.id AND webpage_maxonomies.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND domains.tli = '" . $this->get_given_domain_tli() . "' AND maxonomies.categorization = '" . $this->get_given_categorization() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY maxonomies.order_by;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    // debug
    //print "debug webpage_maxonomies " . $markup . "<br />\n";

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_webpage_id" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_by_categorization") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage_maxonomy();
        $obj->get_webpage_obj()->set_id(pg_result($results, $lt, 0));
        $obj->get_maxonomy_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_maxonomy_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_maxonomy_obj()->set_name(pg_result($results, $lt, 3));
        $obj->get_maxonomy_obj()->set_img_url(pg_result($results, $lt, 4));
        $obj->get_maxonomy_obj()->set_status(pg_result($results, $lt, 5));
        $obj->get_maxonomy_obj()->set_user_name(pg_result($results, $lt, 6));
        $obj->get_maxonomy_obj()->set_sort(pg_result($results, $lt, 7));
        $obj->get_maxonomy_obj()->set_description(pg_result($results, $lt, 8));
        $obj->get_maxonomy_obj()->set_categorization(pg_result($results, $lt, 9));
        $obj->get_maxonomy_obj()->set_group_name(pg_result($results, $lt, 10));
        $obj->get_maxonomy_obj()->set_how_to_measure(pg_result($results, $lt, 11));
        $obj->get_maxonomy_obj()->set_ocm(pg_result($results, $lt, 12));
        $obj->get_maxonomy_obj()->set_order_by(pg_result($results, $lt, 13));
        $obj->get_webpage_obj()->get_domain_obj()->set_id(pg_result($results, $lt, 14));
        $obj->get_webpage_obj()->get_domain_obj()->set_name(pg_result($results, $lt, 15));
        $obj->get_webpage_obj()->get_domain_obj()->set_img_url(pg_result($results, $lt, 16));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
      // debug
      //print "debug webpage_maxonomies " . $markup . "<br />\n";
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_project_id()) {
      $markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    webpage_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    maxonomy_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    report\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage_maxonomy) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $webpage_maxonomy->get_webpage_obj()->get_domain_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $webpage_maxonomy->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $webpage_maxonomy->get_webpage_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $webpage_maxonomy->get_maxonomy_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // status
      if ($obj->get_status()) {
        $markup .= "<p>status = ";
        $markup .= $obj->get_status() . "</p>\n";
      } else {
        // error
        $markup .= $this->get_db_dash()->output_error("Error: No status.");
      }

      // description
      $markup .= "<div class=\"paragraphs\">\n";
      $markup .= "<h2>description</h2>\n";
      $markup .= "<p>" . $obj->get_description() . "</p>\n";
      $markup .= "</div><!-- end class paragraphs -->\n";

      // report
      $markup .= "<div class=\"paragraphs\">\n";
      $markup .= "<h2>report</h2>\n";
      if ($obj->get_report()) {
        $markup .= "<p>" . $obj->get_report() . "</p>\n";
      } else {
        // error
        $markup .= $this->get_db_dash()->output_error("Error: No report.");
        // todo the line below was not working
        //$markup .= "<p style=\"error\">No report.</p>\n";
      }
      $markup .= "</div><!-- end class paragraphs -->\n";


      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup = "not yet built in webpage_maxonomies";

    return $markup;
  }

  // method
  public function get_list_of_maxonomy_obj_given_webpage_id($given_webpage_id, $given_user_obj) {

    $maxonomy_obj_array = array();

    // set
    $this->set_given_webpage_id($given_webpage_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) { 
      array_push($maxonomy_obj_array, $obj->get_maxonomy_obj());
    }

    return $maxonomy_obj_array;
  }

  // method
  public function get_problemography($given_domain_tli, $given_user_obj) {
    $maxonomy_array = array();

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_given_categorization("problemography");
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) { 
      array_push($maxonomy_array, $obj->get_maxonomy_obj()->get_name());
    }

    return $maxonomy_array;
  }

  // method
  public function get_count_given_tli($given_domain_tli, $given_user_obj) {

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    return $this->get_list_bliss()->get_count();
  }

  // method
  protected function output_preface() {
    $markup = "";

    // todo should this be in the parent class?
    $markup .= "<br />\n";

    return $markup;
  }

}
