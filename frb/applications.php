<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-20
// version 1.2 2015-01-04
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/applications.php

include_once("lib/socation.php");

class Applications extends Socation {

  // given
  private $given_name;
  private $given_domain_tli;
  private $given_view = "all"; // default

  // given_name
  public function set_given_name($var) {
    $this->given_name = $var;
  }
  public function get_given_name() {
    return $this->given_name;
  }

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attributes
  private $url;
  private $source_code_url;
  private $development;

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // source_code_url
  public function set_source_code_url($var) {
    $this->source_code_url = $var;
  }
  public function get_source_code_url() {
    return $this->source_code_url;
  }

  // development
  public function set_development($var) {
    $this->development = $var;
  }
  public function get_development() {
    return $this->development;
  }

  // method
  private function make_application() {
    $obj = new Applications($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

     // debug
     //print "debug applications type = " . $this->get_type() . "<br />\n";

     // initialize
     $sql = "";

     if ($this->get_type() == "get_by_id") {
       // security: only get the rows owned by the user
       $sql = "SELECT applications.* FROM applications WHERE applications.id = " . $this->get_given_id() . ";";

     } else if ($this->get_type() == "get_all") {
       // security: table has no user_name
       // select given view
       // todo this is on the hard-coded side so make user configurable groups
       if ($this->get_given_view() == "mudia-stack") {
         $sql = "SELECT applications.* FROM applications WHERE status = 'mudia-stack' ORDER BY sort DESC, applications.name, source_code_url DESC;";
       } else {
         $sql = "SELECT applications.* FROM applications ORDER BY sort DESC, applications.name, source_code_url DESC;";
       }

     } else if ($this->get_type() == "get_by_project_id") {
       // security: only get the rows owned by the user
       // todo: cannot span 2 databases to get this data
       $sql = "SELECT applications.* FROM applications, host_applications, domains, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE applications.id = host_applications.application_id AND host_applications.domain_tli = domains.tli AND domains.tli = scene_elements.class_primary_key_string AND scene_elements.class_name_string = 'domains' AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY sort DESC, applications.name, source_code_url DESC;";

     } else if ($this->get_type() == "get_by_domain_tli") {
       // security: only get the rows owned by the user
      // assumes machine is parent and domain is child
      $sql = "select applications.* from applications, host_applications WHERE applications.id = host_applications.application_id AND host_applications.domain_tli = '" . $this->get_given_domain_tli() . "' ORDER BY applications.sort DESC;";

     } else if ($this->get_type() == "get_count") {
       $sql = "select applications.name from applications, domain_applications, domains where applications.id = domain_application_id AND domain_applications.domain_tli = domains.tli AND domains.tli = '" . $this->get_given_domain_tli() . "';";

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM applications;";

     } else {
       $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
     }

     // execute function
     if ($sql) {
       $markup .= parent::load_data($this, $sql);
     }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_application();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_url(pg_result($results, $lt, 2));
        $obj->set_source_code_url(pg_result($results, $lt, 3));
        $obj->set_development(pg_result($results, $lt, 4));
        $obj->set_sort(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->set_description(pg_result($results, $lt, 7));
        $obj->set_status(pg_result($results, $lt, 8));
      }
    } else if ($this->get_type() == "get_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_application();
        $obj->set_name(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_application();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

     if (isset($_GET['name'])) {
       $this->set_given_name($_GET['name']);
       // debug
       //print "<p><em>application<em></p>\n";
       //print "<h1>" . $this->get_given_name() . "<h1>\n";
     }

     // output
     $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

     // toggle visibility of sidecar here
     $sidecar_show = 0;

     $markup .= "<div id=\"tenperday\">\n";
     $markup .= "<table class=\"plants\">\n";

     $num = 0;
     foreach ($this->get_list_bliss()->get_list() as $application) {

       // sort
       $markup .= "<tr>\n";
       $markup .= "  <td class=\"colhead\" width=\"150px\">\n";
       $markup .= "    sort\n";
       $markup .= "  </td>\n";
       $column_name = "sort";
       include_once("lib/timekeeper.php");
       $timekeeper_obj = new Timekeeper();
       $color = $timekeeper_obj->calculate_cell_color($column_name, $application->get_sort());
       $markup .= "  <td valign=\"middle\" style=\"background: $color;\">\n";
       $markup .= "    " . $application->get_sort() . "<br />\n";
       $markup .= "  </td>\n";

       $markup .= "</tr>\n";
       $markup .= "<tr>\n";

       $markup .= "  <td class=\"colhead\">\n";
       $markup .= "    id\n";
       $markup .= "  </td>\n";
       $markup .= "  <td class=\"mid\">\n";
       $markup .= "    " . $application->get_id_with_link() . "<br />\n";
       $markup .= "  </td>\n";

       $markup .= "</tr>\n";
       $markup .= "<tr>\n";

       // img_url
       $markup .= "  <td class=\"colhead\">\n";
       $markup .= "    img_url\n";
       $markup .= "  </td>\n";
       $markup .= "  <td>\n";
       $padding = "0px 0px 0px 0px";
       $float = "";
       $width = "33";
       $height = "33";
       $markup .= "    " . $application->get_img_url_as_img_element($padding, $float, $width, $height) . "<br />\n";
       $markup .= "  </td>\n";

       $markup .= "</tr>\n";
       $markup .= "<tr>\n";

       $markup .= "  <td class=\"colhead\">\n";
       $markup .= "    name (homepage)\n";
       $markup .= "  </td>\n";
       // name with link
       $markup .= "  <td>\n";
       $url = $application->get_url();
       $markup .= "    <a href=\"" . $url . "\">" . $application->get_name() . "</a>\n";
       $markup .= "  </td>\n";

       $markup .= "</tr>\n";
       $markup .= "<tr>\n";

       $markup .= "<tr>\n";

       // status
       $markup .= "  <td class=\"colhead-narrow\">\n";
       $markup .= "    status\n";
       $markup .= "  </td>\n";
       $markup .= "  <td style=\"background-color: " . $application->get_status_background_color() . "\">\n";
       $url = $application->get_status();
       $markup .= "    <a href=\"" . $url . "\">" . $application->get_status() . "</a><br />\n";
       $markup .= "  </td>\n";
       $markup .= "</tr>\n";

       // url
       $markup .= "  <td class=\"colhead-narrow\">\n";
       $markup .= "    url\n";
       $markup .= "  </td>\n";
       $markup .= "  <td>\n";
       $url = $application->get_url();
       $markup .= "    <a href=\"" . $url . "\">" . $application->get_url() . "</a><br />\n";
       $markup .= "  </td>\n";
       $markup .= "</tr>\n";

       // description
       $markup .= "  <td class=\"colhead\">\n";
       $markup .= "    description\n";
       $markup .= "  </td>\n";
       $markup .= "  <td>\n";
       $markup .= "    " . $application->get_description() . "<br />\n";
       $markup .= "  </td>\n";

       $markup .= "</tr>\n";
       $markup .= "<tr>\n";

       // source_code_url
       $markup .= "  <td class=\"colhead-narrow\">\n";
       $markup .= "    source_code_url\n";
       $markup .= "  </td>\n";
       // source_code_url
       if ($application->get_source_code_url() == "AppMap") {
         $markup .= "  <td style=\"background-color: #339955;\">\n";
       } else {
         $markup .= "  <td>\n";
       }
       $markup .= $application->get_source_code_url() ;
       // todo salvage attempt to make short
       //$short_length = -20;
       //$short_name = "..." . substr($application->get_source_code_url(), $short_length);
       //$markup .= "    <a href=\"" . $url . "\">" . $short_name . "</a><br />\n";
       $markup .= "  </td>\n";

       $markup .= "</tr>\n";
       $markup .= "<tr>\n";

       $markup .= "  <td class=\"colhead-narrow\">\n";
       $markup .= "    development\n";
       $markup .= "  </td>\n";
       $markup .= "  <td>\n";
       $markup .= $application->get_development();
       // todo salvage attempt to make short
       //$short_length = -20;
       //$short_name = "..." . substr($application->get_development(), $short_length);
       //$markup .= "    <a href=\"" . $url . "\">" . $short_name . "</a><br />\n";
       $markup .= "  </td>\n";

       $markup .= "</tr>\n";

     }
     $markup .= "</table>\n";

     // sidecar
     if ($sidecar_show) {
       $markup .= "<h3>this <em>application<em> used in following <em>scene_elements</em></h3>\n";
       include_once("scene_elements.php");
       $obj = new SceneElements($this->get_given_config());
       $user_obj = $this->get_user_obj();
       $given_application_id = $this->get_given_id();
       $markup .= $obj->get_scene_elements_given_application_id($user_obj, $given_application_id);
    }
    return $markup;
  }

  // method
  private function output_table($sidecar_show = 0) {
    $markup = "";

    $markup .= $this->output_view();

    // may be a subset
    if ($this->get_given_domain_tli()) {
      $url = $this->url("domains/" . $this->get_given_domain_tli());
      $markup .= "<p><a href=\"" . $url . "\">" . $this->get_given_domain_tli() . "</a></p>\n";
    }

    $markup .=  "<div id=\"tenperday\">\n";
    $markup .=  "<table class=\"plants\">\n";

    // heading
    $markup .=  "<tr>\n";
    $markup .=  "  <td class=\"header\">\n";
    $markup .=  "    #\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"header\">\n";
    $markup .=  "    status\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"header\" width=\"150px\">\n";
    $markup .=  "    sort\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"header\">\n";
    $markup .=  "    id\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"header\">\n";
    $markup .=  "    img_url\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"header\">\n";
    $markup .=  "    name\n";
    $markup .=  "  </td>\n";

    // domain_tli via host_applications
    $markup .=  "  <td class=\"header\" style=\"width: 420px;\">\n";
    $markup .=  "    domain_tli\n";
    $markup .=  "  </td>\n";

    // scene_elements
    // todo decided that this was needed all the times
    //if ($sidecar_show) {
    //  $markup .=  "  <td class=\"header\">\n";
    //  $markup .=  "    scene_elements\n";
    //  $markup .=  "  </td>\n";
    //}

$markup .=  "  <td class=\"header\" style=\"width: 420px; font-size: 60%;\">\n";
    $markup .=  "    url<br />\n";
    $markup .=  "    source_code_url<br />\n";
    $markup .=  "    development<br />\n";
    $markup .=  "  </td>\n";

    // description
    //$markup .=  "  <td class=\"colhead-narrow\">\n";
    //$markup .=  "    description\n";
    //$markup .=  "  </td>\n";
    $markup .=  "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $application) {

      $markup .=  "<tr>\n";

      // num
      $num++;
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $num . "\n";
      $markup .=  "  </td>\n";

      // status 
      if ($application->get_status() == "zoneline") {
        $markup .= "  <td style=\"background-color: " . $application->get_status_background_color() . "\">\n";
      } else if ($application->get_status() == "mudia-stack") {
        $markup .=  "  <td style=\"background-color: #CC3399;font-size: 80%;\">\n";
      } else {
        $markup .=  "  <td style=\"background-color: #EFEFEF;font-size: 80%;\">\n";
      }
      $markup .=  "    " . $application->get_status() . "<br />\n";
      $markup .=  "  </td>\n";

      // sort
      // old sort cell
      //$column_name = "sort";
      //include_once("lib/timekeeper.php");
      //$timekeeper_obj = new Timekeeper();
      //$color = $timekeeper_obj->calculate_cell_color($column_name, $application->get_sort());
      //$markup .= "  <td valign=\"middle\" style=\"background: $color;\">\n";
      //$markup .=  "    " . $application->get_sort() . "<br />\n";
      //$markup .=  "  </td>\n";
      // new sort cell
      $markup .= $application->get_sort_cell();

      // id
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $application->get_id_with_link() . "<br />\n";
      $markup .=  "  </td>\n";

      // img_url
      $markup .=  "  <td>\n";
      $padding = "0px 0px 0px 0px";
      $float = "";
      $width = "33";
      $height = "33";
      $markup .=  "    " . $application->get_img_url_as_img_element($padding, $float, $width, $height) . "<br />\n";
      $markup .=  "  </td>\n";

      // name with link
      $markup .=  "  <td>\n";
      $markup .=  "    " . $application->get_name_with_link_natural() . "<br />\n";
      $markup .=  "  </td>\n";

      // domain_tli via host_applications
      $markup .=  "  <td style=\"font-size: 50%;\">\n";
      include_once("host_applications.php");
      $obj = new HostApplications($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $application_id = $application->get_id();
      $markup .= $obj->get_domain_tli_given_application_id($user_obj, $application_id);
      $markup .=  "  </td>\n";

      // scene_elements
      //if ($sidecar_show) {
      //  $markup .=  "  <td>\n";
      //  include_once("scene_elements.php");
      //  $obj = new SceneElements($this->get_given_config());
      //  $user_obj = $this->get_user_obj();
      //  $given_application_id = $application->get_id();
      //  $markup .= $obj->get_scene_elements_given_application_id($user_obj, $given_application_id);
      //  $markup .=  "  </td>\n";
      //}

      // url source_code_url development
      // note three urls in 1 cell
      // todo show warning if not defined
      if ($application->get_url() && $application->get_source_code_url() && $application->get_development()) {
        $markup .=  "  <td style=\"background-color: #6699FF; font-size: 50%;\">\n";
      } else {
        $markup .=  "  <td style=\"background-color: #CCCCCC; font-size: 50%;\">\n";
      }
      // url
      $url = $application->get_url();
      if ($url) {
        $markup .=  "doc " . "<a href=\"" . $url . "\" class=\"show\">" . $url . "</a>\n";
      } else {
        $markup .=  "doc undefined\n";
      }
      $markup .=  "<br />\n";
      // source_code_url
      $url = $application->get_source_code_url() ;
      if ($url) {
        $markup .=  "src " . "<a href=\"" . $url . "\">" . $url . "</a>\n";
      } else {
        $markup .=  "src  undefined\n";
      }
      $markup .=  "<br />\n";
      // development url
      $development = $application->get_development();
      if ($development) {
        $markup .=  "dev " . $development . "\n";
      } else {
        $markup .=  "dev undefined\n";
      }
      $markup .=  "<br />\n";
      $markup .=  "  </td>\n";

      $markup .=  "</tr>\n";

    }
    $markup .=  "</table>\n";

    return $markup;
  }

  // method
  function get_count_applications_given_tli($given_tli) {
      $count_application = "unknown";

      // load data from database
      $this->set_given_domain_tli($given_tli);
      $this->set_type("get_count");
      $markup = $this->prepare_query();
      /**
       * @todo do something with the markup returned above
       */

      // only output if there are items to output
      if ($this->get_list_bliss()->get_count() > 0) {
        foreach ($this->get_list_bliss()->get_list() as $obj) {
          $count_application += 1;
          // $count_application .= $obj->get_name() . "<br />\n";
        }
      } else {
        // no rows found
        $count_application = 0;
      }

      return $count_application;
  }

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function xget_applications_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // check for database errors
    if ($markup) {
      $markup .= "<p style=\"error\">Error: Unable to prepare query.</p>\n";
      //return $markup;
    }

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_aggregate();
    } else {
      $markup .= "<p>No applications found.</p>";
    }

    return $markup;
  }

  // method
  public function get_img_url_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    // debug
    //print "debug applications: given_id " . $this->get_given_id() . "<br />\n";

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // return default image
      // move this to the config file so that the user can modify
      $markup .= "http://mudia.com/dash/_images/swept_ins_icon_small_blue.png";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) {

      // pre-process data
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= $obj->get_img_url($padding, $float, $width);
    }

    return $markup;
  }

  // method
  public function get_applications_list_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<ol style=\"margin: 0px; padding: 4px; list-style-type: none;\">\n";
      foreach ($this->get_list_bliss()->get_list() as $application) {
        $markup .= "<li style=\"margin: 0px; padding: 0px 0px 2px 0px; list-style-type: none;\">" . $application->get_name_with_link() . "</li>\n";;
      }
      $markup .= "</ol>\n";

    } else {
      $markup .= "<p>No applications found.</p>";
    }

    return $markup;
  }

  // method
  public function get_applications_comma_list_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $last_flag = $this->get_list_bliss()->get_count() - 1;
      foreach ($this->get_list_bliss()->get_list() as $application) {
        $markup .= $application->get_name_with_link();
        if ($last_flag) {
          $last_flag--;
          $markup .= ", ";
        }
      }

    } else {
      $markup .= "<p>No applications found.</p>";
    }

    return $markup;
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Domains") {
      $markup .= "debug applications: <strong>check if they overlap</strong><br />\n";
    } else {
      $markup .= "debug applications: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  private function output_view() {
    $markup = "";

    $markup .= "<p>view: ";
    $views = array("in progress", "mudia-stack", "all");
    $last_pos = count($views) - 1;
    foreach ($views as $view) {
      $markup .= $this->get_menu_item($view, "view");
      if ($view != $views[$last_pos]) {
        $markup .= " | ";
      }
    }
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            // todo change this hard-ceded string to function that gets
            $table_name = "applications";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $application_obj = new Applications($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $application_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            //$target_page = $parameter->get_value();
            //$this->redirect_helper($target_page);
          }
          // 3
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // method
  public function get_obj_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 1) {
      foreach ($this->get_list_bliss()->get_list() as $application) {
        return $application;
      }
    }
  }

}
