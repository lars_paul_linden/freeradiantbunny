<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04
// version 1.5 2015-10-16
// version 1.5 2017-06-24
// version 1.6 2017-11-11

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/machines.php

include_once("lib/socation.php");

class Machines extends Socation {

  // given
  private $given_machine_id;
  private $given_domain_tli;
  private $given_view = "online"; // default

  // given_machine_id
  public function set_given_machine_id($var) {
    $this->given_machine_id = $var;
  }
  public function get_given_machine_id() {
    return $this->given_machine_id;
  }

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attribute
  private $cpu;
  private $filesystems;
  private $user_name;
  private $ip_address;

  // cpu
  public function set_cpu($var) {
    $this->cpu = $var;
  }
  public function get_cpu() {
    return $this->cpu;
  }

  // filesystems
  public function set_filesystems($var) {
    $this->filesystems = $var;
  }
  public function get_filesystems() {
    return $this->filesystems;
  }

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // ip_address
  public function set_ip_address($var) {
    $this->ip_address = $var;
  }
  public function get_ip_address() {
    return $this->ip_address;
  }

  // img_url
  public function set_img_url($var) {
    $this->img_url = $var;
  }
  public function get_img_url() {
    if (! $this->img_url) {
      $config = $this->get_given_config();
      $base_url = $config->get_base_url();

      // target examples and testing examples: 
      // todo try to a more flexible and less hard-coding; central repository
      // todo or a series of places that are checked (online, offline)
      //return $base_url . "http://basecamp/~blaireric/dev/public_html_dev/frb-clone4/freeradiantbunny-demo/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html/dash/_images/machine_stamp.png";
     // target example:
      // http://basecamp/~blaireric/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html/dash/_images/machine_stamp.png

      return $base_url . "../../../../dev/public_html_dev/frb-clone4/freeradiantbunny-demo/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html/dash/_images/machine_stamp.png";
    }
    return $this->img_url;
  }

  // method
  private function make_machine() {
    $obj = new Machines($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_machine_id()) {
      $this->set_type("get_by_machine_id");

    } else {
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "select machines.* from machines WHERE machines.id = " . $this->get_given_id() . " AND machines.user_name = '" . $this->get_user_obj()->name . "';";
    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "select machines.* from machines WHERE machines.user_name = '" . $this->get_user_obj()->name . "' ORDER BY machines.sort DESC, machines.status, machines.name;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      // todo: cannot span 2 databases to get this data
      $sql = "SELECT DISTINCT ON (machines.sort, machines.id) machines.* FROM machines, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'machines' AND machines.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY machines.sort DESC, machines.id;";

    } else if ($this->get_type() == "get_by_machine_id") {
      // security: only get the rows owned by the user
      $sql = "select machines.* from machines WHERE machines.id = " . $this->get_given_machine_id() . " AND machines.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      // assumes machine is parent and domain is child
      $sql = "select machines.* from machines, hosts WHERE hosts.parent_class_name_string = 'machines' AND CAST(hosts.parent_class_primary_key_string as integer) = machines.id AND hosts.child_class_name_string = 'domains' AND hosts.child_class_primary_key_string = '" . $this->get_given_domain_tli() . "' ORDER BY machines.sort DESC;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_machine_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_machine();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_cpu(pg_result($results, $lt, 3));
        $obj->set_filesystems(pg_result($results, $lt, 4));
        $obj->set_sort(pg_result($results, $lt, 5));
        $obj->set_status(pg_result($results, $lt, 6));
        $obj->set_user_name(pg_result($results, $lt, 7));
        $obj->set_ip_address(pg_result($results, $lt, 8));
        $obj->set_img_url(pg_result($results, $lt, 9));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    s.e. count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    ip_address\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    cpu\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    hosts count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\" style=\"width: 120px;\">\n";
    $markup .= "    hosts\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"colhead\">\n";
    //$markup .= "    filesystems\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"colhead\">\n";
    //$markup .= "    host_ databases\n";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $total_scene_element_count = 0;

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $machine) {

      $markup .= "<tr>\n";

      // num
      $markup .= "  <td class=\"mid\">\n";
      $num++;
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      // status
      if ($machine->get_status() == "zoneline") {
        $markup .= "  <td valign=\"top\" style=\"background-color: #C0509F;\">\n";
      } else {
        $markup .= "  <td valign=\"top\" style=\"background-color: #CCC;\">\n";
      }
      $markup .= "    " . $machine->get_status() . "<br />\n";
      $markup .= "  </td>\n";

      // sort
      // todo clean up old way
      //$column_name = "sort";
      //include_once("lib/timekeeper.php");
      //$timekeeper_obj = new Timekeeper();
      //$color = $timekeeper_obj->calculate_cell_color($column_name, $machine->get_sort());
      //$markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"mid\">\n";
      //$markup .= "    " . $machine->get_sort() . "<br />\n";
      //$markup .= "  </td>\n";
      // sort
      $markup .= $machine->get_sort_cell();

      // id
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $machine->get_id_with_link() . "<br />\n";
      $markup .= "  </td>\n";

      // img_url
      $markup .= "  <td class=\"mid\">\n";
      $padding = "";
      $float = "";
      $width = "52";
      $image_data = $machine->get_img_as_img_element_with_link($padding, $float, $width);
      $markup .= $image_data . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $machine->get_name_with_link() . "<br />\n";
      $markup .= "  </td>\n";

      // s.e. count
      include_once("scene_elements.php");
      $scene_element_obj = new SceneElements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      // get actual scene_elements count
      $scene_element_count = $scene_element_obj->get_count_given_machine_id($machine->get_id(), $user_obj);
      if ($scene_element_count) {
        $color= "#CCC";
        $total_scene_element_count += $scene_element_count;
      } else {
        $color= "red";
      }
      // get scene_elements with links
      // todo creating a whole different object is bad but bugfree
      $scene_element_obj2 = new SceneElements($this->get_given_config());
      $scene_elements_markup = $scene_element_obj2->get_list_given_machine_id($machine->get_id(), $user_obj);
      $markup .= "  <td style=\"background: $color; text-align: left; vertical-align: middle;\" class=\"mid\">\n";
      // todo need links to the actual scene_elements
      //$markup .= "    " . $scene_element_count . "<br />\n";
      $markup .= "    " . $scene_elements_markup . "\n";
      $markup .= "  </td>\n";

      // ip_address
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $machine->get_ip_address() . "\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "    " . $machine->get_description() . "<br />\n";
      $markup .= "  </td>\n";

      // cpu
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "    " . $machine->get_cpu() . "\n";
      $markup .= "  </td>\n";

      // scene_elements that match
      //$markup .= "  <td class=\"mid\" align=\"left\" style=\"width: 120px;\">\n";
      //include_once("scene_elements.php");
      //$scene_element_obj = new SceneElements($this->get_given_config());
      //$class_name = get_class($this);
      //$id = $machine->get_id();
      //$user_obj = $this->get_user_obj();
      //$markup .= $scene_element_obj->get_scene_elements_list($class_name, $id, $user_obj);
      //$markup .= "  </td>\n";

      // hosts count
      include_once("hosts.php");
      $host_obj = new Hosts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $host_count = $host_obj->get_count_given_machine_id($machine->get_id(), $user_obj);
      if ($host_count) {
        $color= "#CCC";
      } else {
        $color= "red";
      }
      $markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"mid\">\n";
      $markup .= "    " . $host_count . "<br />\n";
      $markup .= "  </td>\n";

      // hosts
      include_once("hosts.php");
      $host_obj = new Hosts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $hosts_string = $host_obj->get_hosts_given_machine_id($machine->get_id(), $user_obj);
      if ($hosts_string) {
        $color= "#CCC";
      } else {
        $color= "red";
      }
      $markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"mid\">\n";
      $markup .= "    " . $hosts_string . "<br />\n";
      $markup .= "  </td>\n";

      // filesystem
      //$markup .= "  <td class=\"mid\" align=\"left\">\n";
      //$markup .= "    " . $machine->get_filesystems() . "<br />\n";
      //$markup .= "  </td>\n";

      if (0) {
        // host_databases
        include_once("host_databases.php");
        $host_database_obj = new HostDatabases($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $host_databases_string = $host_database_obj->get_databases_given_machine_id($machine->get_id(), $user_obj);
        if ($host_databases_string) {
          $color= "#CCC";
          // todo need to refactor this into a count number
          //$total_host_databases_count += $host_databases_count;
        } else {
          $color= "red";
        }
        $markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"mid\">\n";
        $markup .= "    " . $host_databases_string . "\n";
        $markup .= "  </td>\n";
      }

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    $markup .= "<p>total_scene_element_count = " . $total_scene_element_count . "</p>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    $total_scene_element_count = 0;

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $machine) {

      $markup .= "<tr>\n";

      // num
      $markup .= "  <td class=\"colhead\" style=\"width: 120px;\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\" style=\"width: 320px;\">\n";
      $num++;
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // status
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      if ($machine->get_status() == "zoneline") {
        $markup .= "  <td valign=\"top\" style=\"background-color: #C0509F;\">\n";
      } else {
        $markup .= "  <td valign=\"top\" style=\"background-color: #CCC;\">\n";
      }
      $markup .= "    " . $machine->get_status() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // sort
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      // todo clean up old way
      //$column_name = "sort";
      //include_once("lib/timekeeper.php");
      //$timekeeper_obj = new Timekeeper();
      //$color = $timekeeper_obj->calculate_cell_color($column_name, $machine->get_sort());
      //$markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"unknown\">\n";
      //$markup .= "    " . $machine->get_sort() . "<br />\n";
      //$markup .= "  </td>\n";
      // sort
      $markup .= $machine->get_sort_cell();

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // id
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\">\n";
      $markup .= "    " . $machine->get_id_with_link() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // img_url
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\">\n";
      $padding = "";
      $float = "";
      $width = "52";
      $image_data = $machine->get_img_as_img_element_with_link($padding, $float, $width);
      $markup .= $image_data . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // name
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\">\n";
      $markup .= "    <h2>" . $machine->get_name_with_link() . "</h2>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // description
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\">\n";
      $markup .= "    " . $machine->get_description() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // filesystem
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    filesystem\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\" align=\"left\">\n";
      $markup .= "    " . $machine->get_filesystems() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // cpu
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    cpu\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\" align=\"left\">\n";
      $markup .= "    " . $machine->get_cpu() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // ip_address
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    ip_address\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"unknown\">\n";
      $markup .= "    " . $machine->get_ip_address() . "\n";
      $markup .= "  </td>\n";

      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";

      // scene_elements that match
      //$markup .= "  <td class=\"unknown\" align=\"left\" >\n";
      //include_once("scene_elements.php");
      //$scene_element_obj = new SceneElements($this->get_given_config());
      //$class_name = get_class($this);
      //$id = $machine->get_id();
      //$user_obj = $this->get_user_obj();
      //$markup .= $scene_element_obj->get_scene_elements_list($class_name, $id, $user_obj);
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // hosts count
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    hosts count\n";
      $markup .= "  </td>\n";
      include_once("hosts.php");
      $host_obj = new Hosts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $host_count = $host_obj->get_count_given_machine_id($machine->get_id(), $user_obj);
      if ($host_count) {
        $color= "#CCC";
      } else {
        $color= "red";
      }
      $markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"unknown\">\n";
      $markup .= "    " . $host_count . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // hosts
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    hosts\n";
      $markup .= "  </td>\n";
      include_once("hosts.php");
      $host_obj = new Hosts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $hosts_string = $host_obj->get_hosts_given_machine_id($machine->get_id(), $user_obj);
      if ($hosts_string) {
        $color= "#CCC";
      } else {
        $color= "red";
      }
      $markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"unknown\">\n";
      $markup .= "    " . $hosts_string . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // host_databases
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    host_ databases\n";
      $markup .= "  </td>\n";
      include_once("host_databases.php");
      $host_database_obj = new HostDatabases($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $host_databases_string = $host_database_obj->get_databases_given_machine_id($machine->get_id(), $user_obj);
      if ($host_databases_string) {
        $color= "#CCC";
        // todo need to refactor this into a count number
        //$total_host_databases_count += $host_databases_count;
      } else {
        $color= "red";
      }
      $markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\" class=\"unknown\">\n";
      $markup .= "    " . $host_databases_string . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // s.e. count
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    s.e. count\n";
      $markup .= "  </td>\n";
      include_once("scene_elements.php");
      $scene_element_obj = new SceneElements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      // get actual scene_elements count
      $scene_element_count = $scene_element_obj->get_count_given_machine_id($machine->get_id(), $user_obj);
      if ($scene_element_count) {
        $color= "#CCC";
        $total_scene_element_count += $scene_element_count;
      } else {
        $color= "red";
      }
      // get scene_elements with links
      // todo creating a whole different object is bad but bugfree
      $scene_element_obj2 = new SceneElements($this->get_given_config());
      $scene_elements_markup = $scene_element_obj2->get_list_given_machine_id($machine->get_id(), $user_obj);
      $markup .= "  <td style=\"background: $color; text-align: left; vertical-align: middle;\" class=\"unknown\">\n";
      // todo need links to the actual scene_elements
      //$markup .= "    " . $scene_element_count . "<br />\n";
      $markup .= "    " . $scene_elements_markup . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    $markup .= "<p>total_scene_element_count = " . $total_scene_element_count . "</p>\n";

    return $markup;
  }

  // method
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function get_name_and_link_given_id($given_user_obj, $given_machine_id) {
    $markup = "";

    $this->set_given_machine_id($given_machine_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      foreach ($this->get_list_bliss()->get_list() as $machine) {
        $markup .= $machine->get_name_with_link();
      }

    } else {
      $markup .= "No machine found.";
    }

    return $markup;
  }

  // method
  public function get_machines_list_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    // note assumes zero and positive integers
    if ($this->get_list_bliss()->get_count() == 0) {
      $markup .= "No machines found.\n";
    } else if ($this->get_list_bliss()->get_count() == 1) {
      foreach ($this->get_list_bliss()->get_list() as $machine) {
        $markup .= $machine->get_name_with_link();
      }
    } else if ($this->get_list_bliss()->get_count() > 1) {

      $markup .= "<ol style=\"margin: 0px; padding: 4px; list-style-type: none;\">\n";
      foreach ($this->get_list_bliss()->get_list() as $machine) {
        $markup .= "<li style=\"margin: 0px; padding: 0px 0px 2px 0px; list-style-type: none;\">" . $machine->get_name_with_link() . "</li>\n";
      }
      $markup .= "</ol>\n";
    }

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Domains") {
      $markup .= "debug machines: <strong>check for hosts class</strong><br />\n";
      //include_once("hosts.php");
      //$host_obj = new Hosts($this->get_given_config());
    } else {
      $markup .= "debug machines: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug machines: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error machines: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $machine) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $machine->get_id() . " " . $machine->get_name_with_link() . " use " . $machine->get_energy_flows_dollars("usage") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $machine->get_energy_flows_dollars("usage");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error machines: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "machines: no machines found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // debug
    //$markup .= $this->get_id() . "\n";

    if ($given_account_name == "usage") {
      // note extract XML from description field
      $field_string = $this->get_description();
      // debug
      //$markup .= $field_string;

      // get first line
      // extract string based upon substring
      $account_rule_string = substr($field_string, 0, strpos($field_string, "account-rule"));
      //$markup .= $account_rule_string;

      // narrow
      // extract string based upon substring
      $amount_string = substr($account_rule_string, 0, strpos($account_rule_string, "-dollars-per-month"));
      $markup .= $amount_string;

    } else {
      $markup .= "error machines: given_account_type not known.<br />\n";
    }

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "machines";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $machine_obj = new Machines($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $machine_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug machines target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
          // 3
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // method
  public function get_obj_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 1) {
      foreach ($this->get_list_bliss()->get_list() as $machine) {
        return $machine;
      }
    }
  }

}
