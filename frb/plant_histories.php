<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_histories.php

include_once("lib/standard.php");

class PlantHistories extends Standard {

  // given
  private $given_plant_list_plant_id;
  private $given_seed_packet_id;
  private $given_plant_list_id;
  private $given_plant_id;
  private $given_project_id;
  //private $given_sort;

  // given_plant_list_plant_id;
  public function set_given_plant_list_plant_id($var) {
    $error_message = $this->get_validator_obj()->validate_id($var, "plant_list_plant_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_list_plant_id = $var;
    }
  }
  public function get_given_plant_list_plant_id() {
    return $this->given_plant_list_plant_id;
  }

  // given_seed_packet_id;
  public function set_given_seed_packet_id($var) {
    $this->given_seed_packet_id = $var;
  }
  public function get_given_seed_packet_id() {
    return $this->given_seed_packet_id;
  }

  // given_plant_list_id;
  public function set_given_plant_list_id($var) {
    //$error_message = $this->get_validation_obj()->validate_id($var, "plant_list_id");
    //if ($error_message) {
    //  $this->set_error_message($error_message);
    //} else {
    $this->given_plant_list_id = $var;
    //}
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_plant_id;
  public function set_given_plant_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "plant_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_id = $var;
    }
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // given_project_id;
  public function set_given_project_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "project_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_project_id = $var;
    }
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // should the sort be a "given" or is it an interface communication?
  // given_sort
  //public function set_given_sort($given_sort) {
  //  $this->given_sort = $given_sort;
  //}
  //public function get_given_sort() {
  //  return $this->given_sort;
  //}

  // attributes
  private $id;
  private $plant_list_plant_obj;
  private $seed_packet_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_list_plant_obj
  public function get_plant_list_plant_obj() {
    if (! isset($this->plant_list_plant_obj)) {
      include_once("plant_list_plants.php");
      $this->plant_list_plant_obj = new PlantListPlants($this->get_given_config());
    }
    return $this->plant_list_plant_obj;
  }

  // seed_packet_obj
  public function get_seed_packet_obj() {
    if (! isset($this->seed_packet_obj)) {
      include_once("seed_packets.php");
      $this->seed_packet_obj = new SeedPackets($this->get_given_config());
    }
    return $this->seed_packet_obj;
  }

  // method
  private function make_plant_history() {
    $obj = new PlantHistories($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  public function make_plant_list() {
    include_once("plant_lists.php");
    $obj = new PlantLists($this->get_given_config());
    $this->get_list_bliss()->add_item($obj);
    return $obj ;
  }

  // method
  //public function deal_with_parameters() {
  //  $markup = "";
  //  // define parameter namespace
  //  $parameters = array();
  //  // create an instance for each parameter
  //  include_once("lib/parameter.php");
  //  // sort
  //  $parameter_b = new Parameter();
  //  $parameter_b->set_name("sort");
  //  $parameter_b->set_validator_function_string("sort");
  //  array_push($parameters, $parameter_b);
  //  // get parameters (if any) and validate
  //  $this->process_parameters($parameters);
  //  // deal with aftermath
  //  foreach ($parameters as $parameter) {
  //    if ($parameter->get_error_message()) {
  //      // error, so get message
  //      $markup .= $parameter->get_error_message();
  //    } else {
  //      // no error, so see if there is a user_value
  //      if (! $parameter->get_value()) {
  //        // no users_value, so do nothing
  //      } else {
  //        // users_value exists, so store
  //        // store depending upon the parameter_name
  //        if ($parameter->get_name() == "sort") {
  //          $this->set_given_sort($parameter->get_value());
  //        }
  //      }
  //    }
  //  }
  //  return $markup;
  //}

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_list_id()) {
      $this->set_type("get_by_plant_list_id");

    } else if ($this->get_given_plant_id()) {
      $this->set_type("get_by_plant_id");

    } else if ($this->get_given_plant_list_plant_id()) {
      $this->set_type("get_by_plant_list_plant_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_seed_packet_id()) {
      $this->set_type("get_by_seed_packet_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT plant_histories.*, plants.id, plants.common_name FROM plant_list_plants, plant_histories, plants WHERE plant_histories.id = " . $this->get_given_id() . " AND plant_list_plants.plant_id = plants.id AND plant_histories.plant_list_plant_id = plant_list_plants.id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_project_id") {
      // a project has plant_lists, so this is a precursor because a user must select a plant list
      //$sql = "";

    } else if ($this->get_type() == "get_by_plant_history_id") {
      $sql = "SELECT plant_histories.*, plants.id, plants.common_name FROM plant_list_plants, plant_histories, plants WHERE plant_histories.id = " . $this->get_given_id() . " AND plant_list_plants.plant_id = plants.id AND plant_histories.plant_list_plant_id = plant_list_plants.id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_seed_packet_id") {
      $sql = "SELECT plant_histories.*, plants.id, plants.common_name FROM plant_list_plants, plant_histories, plants WHERE plant_histories.seed_packet_id = " . $this->get_given_seed_packet_id() . " AND plant_list_plants.plant_id = plants.id AND plant_histories.plant_list_plant_id = plant_list_plants.id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT plant_histories.*, plants.id, plants.common_name FROM plant_list_plants, plant_histories, plants WHERE plants.id = " . $this->get_given_plant_id() . " AND plant_list_plants.plant_id = plants.id AND plant_histories.plant_list_plant_id = plant_list_plants.id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_plant_list_id") {
      $sql = "SELECT plant_histories.id, plant_histories.plant_list_plant_id, plant_histories.seed_packet_id, plants.id, plants.common_name, varieties.id, varieties.name, seed_packets.supplier_id, suppliers.name, seed_packets.packed_for_year, seed_packets.days_to_germination, seed_packets.days_to_maturity, seed_packets.product_details FROM plant_histories, plant_list_plants, seed_packets, plants, varieties, suppliers WHERE plant_list_plants.plant_list_id = " . $this->get_given_plant_list_id() . " AND plant_histories.seed_packet_id = seed_packets.id AND plant_list_plant_id = plant_list_plants.id AND seed_packets.variety_id = varieties.id AND plant_list_plants.plant_id = plants.id AND seed_packets.supplier_id = suppliers.id ORDER BY plants.common_name, varieties.name;";

    } else if ($this->get_type() == "get_by_plant_list_plant_id") {
      // todo the following needs work
      $sql = "SELECT plant_histories.* FROM plant_histories WHERE plant_histories.plant_list_plant_id = " . $this->get_given_plant_list_plant_id() . " ORDER BY plant_histories.plant_list_plant_id;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_histories.* FROM plant_histories ORDER BY plant_histories.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // debug
    //print "debug plant_histories type = " . $this->get_type() . "<br />";
    //print "debug plant_histories sql = " . $sql . "<br />";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_list();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_by_plant_list_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant_history_obj = $this->make_plant_history();
        $plant_history_obj->set_id(pg_result($results, $lt, 0));
        $plant_history_obj->get_plant_list_plant_obj()->set_id(pg_result($results, $lt, 1));
        $plant_history_obj->get_seed_packet_obj()->set_id(pg_result($results, $lt, 2));
        $plant_history_obj->get_plant_list_plant_obj()->get_plant_obj()->set_id(pg_result($results, $lt, 3));
        $plant_history_obj->get_plant_list_plant_obj()->get_plant_obj()->set_common_name(pg_result($results, $lt, 4));
        $plant_history_obj->get_seed_packet_obj()->get_variety_obj()->set_id(pg_result($results, $lt, 5));
        $plant_history_obj->get_seed_packet_obj()->get_variety_obj()->set_name(pg_result($results, $lt, 6));
        $plant_history_obj->get_seed_packet_obj()->get_supplier_obj()->set_id(pg_result($results, $lt, 7));
        $plant_history_obj->get_seed_packet_obj()->get_supplier_obj()->set_name(pg_result($results, $lt, 8));
        $plant_history_obj->get_seed_packet_obj()->set_packed_for_year(pg_result($results, $lt, 9));
        $plant_history_obj->get_seed_packet_obj()->set_days_to_germination(pg_result($results, $lt, 10));
        $plant_history_obj->get_seed_packet_obj()->set_days_to_maturity(pg_result($results, $lt, 11));
        $plant_history_obj->get_seed_packet_obj()->set_product_details(pg_result($results, $lt, 12));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_history();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_list_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_seed_packet_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_by_plant_list_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_history();
        $user_obj = $this->get_user_obj();
        $obj->set_user_obj($user_obj);
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_list_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_seed_packet_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_by_seed_packet_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_history();
        $user_obj = $this->get_user_obj();
        $obj->set_user_obj($user_obj);
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_list_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_seed_packet_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_by_id" ||
              $this->get_type() == "get_by_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_history();
        $user_obj = $this->get_user_obj();
        $obj->set_user_obj($user_obj);
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_list_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_seed_packet_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_plant_list_plant_obj()->get_plant_obj()->set_id(pg_result($results, $lt, 3));
        $obj->get_plant_list_plant_obj()->get_plant_obj()->set_common_name(pg_result($results, $lt, 4));
      }
    } else {
      // may not need this hard-coded print if error system is working
      print "Error: " . get_class($this) . " does not know the type.";
      $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // todo move (this code is in the middle of nowhere)
  // } else if ($this->get_given_sort()) {
  // $markup .= $this->output_list_table_sort_by_date();

  // method
  protected function output_aggregate() {
    $markup = "";

    if ($this->get_type() == "get_all") {

      $markup = $this->output_table();

    } else if ($this->get_type() == "get_by_project_id") {

      // this lists helps the user select a particular list
      include_once("plant_lists.php");
      $plant_list_obj = new PlantLists($this->get_given_config());
      $list_obj = $this->get_list_bliss()->get_list();
      $type_url = "plant_histories";
      $type_string = "Plant Histories";
      $markup .= $plant_list_obj->output_plant_lists_table($type_url, $type_string, $list_obj);

    } else {
      // "get_by_plant_list_id"
      // "get_by_plant_list_plant_id"
      $markup .= $this->output_alternate();
    }

    return $markup;
  }

  // method
  public function output_alternate() {
    $markup = "";

    // todo fix this with respect to the user knowing the view presented
    $markup .= "<p>view: <em>alternate</em></p>";
   
    // preprocess 1 of 2
    // initialize this array for an error report
    // that shows plants on plant_list that do not have plant_histories yet 
    $no_histories_error_report_obj_list = new ListBliss();

    // preprocess 2 of 2
    // helps to remove plant common_name when the same as above row
    $flag_plant_common_name = "";

    // start list
    $markup .= "<table class=\"plants\" border=\"1\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_list\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant list plant id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_histories id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    seed packet\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("plant_history_events/plant_list/" . $this->get_given_plant_list_id());
    $markup .= "    <a href=\"" . $url . "\">plant history events</a>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $num_inside = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_history_obj) {
      $num++;

      // save id for error report
      $plant_list_plant_id = $plant_history_obj->get_plant_list_plant_obj()->get_id();
      $no_histories_error_report_obj_list->add_item($plant_list_plant_id);

      // start row
      $markup .= "<tr>\n";

      // #
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // plant_list_id
      $markup .= "  <td>\n";
      include_once("plant_lists.php");
      $plant_list_obj = new PlantLists($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_list_id = $plant_list_obj->get_plant_list_id_given_plant_history_id($plant_history_obj->get_id(), $user_obj);
      $markup .= "    " . $plant_list_id . " " . $plant_list_obj->get_plant_list_name_given_plant_list_id($plant_list_id, $user_obj) . "\n";
      $markup .= "  </td>\n";

      // plant
      $markup .= "  <td>\n";
      // todo clean up old kruft
      //if ($flag_plant_common_name == $plant_history_obj->get_plant_list_plant_obj()->get_plant_obj()->get_common_name()) {
      //  // duplicate, so skip
      //} else {
      //  $markup .= "    " . $plant_history_obj->get_plant_list_plant_obj()->get_plant_obj()->get_common_name_with_link() . "\n";
      //}
      include_once("plants.php");
      $plant_obj = new Plants($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $plant_obj->get_plant_given_plant_history_id($plant_history_obj->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      // plant_list_plant_id
      $markup .= "  <td>\n";
      if ($flag_plant_common_name == $plant_history_obj->get_plant_list_plant_obj()->get_plant_obj()->get_common_name_with_link()) {
        // duplicate, so skip
      } else {
        $markup .= "    " . $plant_history_obj->get_plant_list_plant_obj()->get_id() . "\n";
      }
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td class=\"header\">\n";
      $url = $this->url("plant_histories/" . $plant_history_obj->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $plant_history_obj->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      // seed_packet_id
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_history_obj->get_seed_packet_obj()->output_mini_table() . "\n";
      $markup .= "  </td>\n";

      // output count
      $url = $this->url("plant_history_events/plant_histories/" . $plant_history_obj->get_id());
      include_once("plant_history_events.php");
      $plant_history_events_obj = new PlantHistoryEvents($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_history_id = $plant_history_obj->get_id();
      $count = $plant_history_events_obj->get_count_given_plant_history_id($user_obj, $plant_history_id);
      if ($count > 0) {
        $markup .= "  <td style=\"background-color: #B4EEB4;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $markup .= "    <a href=\"" . $url . "\">" . $count . " plant history events</a>\n";
      // output "Add event" button
      // note: the plant_list_id needs to be passed so that user can return
      //$url = $this->url("plant_histories/plant_list/" . $this->get_given_plant_list_id());
      //$markup .= "  <div style=\"text-align: right;\"><a href=\"" . $url . "?command=add_an_event_to_this_plant_list&amp;plant_history_id=" . $plant_history_obj->get_id() . "&amp;request=process_submitted_form_data\">Add event</a></div>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // todo build lib/errors class
    // show error report
    //$plant_list_id = $this->get_given_plant_list_id();
    //if ($plant_list_id) {
      //include_once("lib/errors.php");
      //$errors_obj = new Errors();
      //$class = get_class($this);
      //$user_obj = $this->get_user_obj();
      //$markup .= $errors_obj->output_missing_error_report($no_histories_error_report_obj_list, $num_inside, $class, $plant_list_id, $user_obj);
    //}

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant list plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    seed packet\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $num_inside = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_history_obj) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_history_obj->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $url = $this->url("plant_list_plant/" . $plant_history_obj->get_plant_list_plant_obj()->get_id()) ;
      $markup .= "    <a href=\"" . $url . "\">" . $plant_history_obj->get_plant_list_plant_obj()->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_history_obj->get_seed_packet_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  // todo this should be moved!
  public function output_list_table_sort_by_date() {
    $markup = "";

    // todo store the frost dates in a more central user-changable location
    //$last_spring_frost_month = 5;
    //$last_spring_frost_date = 4;
    //$first_fall_frost_month = 10;
    //$first_fall_frost_date = 17;
    // calculation given variables
    //$markup .= "<div class=\"calculation_given_variables\">\n";
    //$markup .= "<em>Given:</em><br />\n";
    //$markup .= "last_spring_frost_month = " . $last_spring_frost_month . "<br />\n";
    //$markup .= "last_spring_frost_date = " . $last_spring_frost_date . "<br />\n";
    //$markup .= "first_fall_frost_month = " . $first_fall_frost_month . "<br />\n";
    //$markup .= "first_fall_frost_date = " . $first_fall_frost_date . "<br />\n";
    //$markup .= "</div><!-- calculation_given_variables -->\n";
    //$markup .= "<br />\n";

    // step 1 of 2
    // collect data and store in an array
    $special_date_array = array();
    foreach ($this->get_list_bliss()->get_list() as $plant_history_obj) {

      include_once("plant_history_events.php");
      $plant_history_events_obj = new PlantHistoryEvents($this->get_given_config());
      $plant_history_events_obj->set_user_obj($this->get_user_obj());
      $plant_history_events_obj->set_given_plant_history_id($plant_history_obj->get_id());
      $plant_history_events_obj->determine_type();
      $plant_history_events_obj->prepare_query();

      foreach ($plant_history_events_obj->get_list_bliss()->get_list() as $plant_history_event_obj) {

        // save object
        $plant_history_event_obj->set_plant_history_obj($plant_history_obj);

        // debug
        //print "debug plant_history_obj = " . $plant_history_event_obj->get_plant_history_obj()->get_id() . "<br />\n";

        // todo clean up the kruft below
        //$rank_date = $plant_history_event_obj->get_multifunction_date_obj()->output_date_given($last_spring_frost_month, $last_spring_frost_date, $first_fall_frost_month, $first_fall_frost_date);
        $rough_date = $plant_history_event_obj->get_rough_date();
        // todo need to convert date to real date object
        $rank_date = $rough_date;
        if (array_key_exists($rank_date, $special_date_array)) {
          // add to existing key's array
          $plant_history_event_obj_array = $special_date_array[$rank_date];
          array_push($plant_history_event_obj_array, $plant_history_event_obj);
          $special_date_array[$rank_date] = $plant_history_event_obj_array;
        } else {
          // first key of this value
          $plant_history_event_obj_array = array();
          array_push($plant_history_event_obj_array, $plant_history_event_obj);
          $special_date_array[$rank_date] = $plant_history_event_obj_array;
        }

        // debug
        //print "plant_history_event_id = " . $plant_history_event_obj->get_id() . "<br />";
      }
    }

    // step 2 of 2
    // output the data
    // the date determines the order

    // show crop plan plants error report
    $plant_id_list = array();

    // rows
    ksort($special_date_array);
    foreach ($special_date_array as $rank => $plant_history_event_obj_array) {

      $markup .= "  <h3>\n";
      $markup .= "    " . $rank . "\n";
      $markup .= "  </h3>\n";

      $first_flag = "yes";
      foreach ($plant_history_event_obj_array as $plant_history_event_obj) {

        // show crop plan plants error report
        // populate a list that is used after the table is output
        $plant_id = $plant_history_obj->get_plant_obj()->get_id();
        array_push($plant_id_list, $plant_id);

        if ($first_flag) {
          $markup .= "<table class=\"plants\">\n";

          $markup .= "<tr>\n";
          // column headings
          $markup .= "  <td class=\"header\">\n";
          $markup .= "    event date\n";
          $markup .= "  </td>\n";
          $markup .= "  <td class=\"header\">\n";
          $markup .= "    plant\n";
          $markup .= "  </td>\n";
          $markup .= "  <td class=\"header\">\n";
          $markup .= "    variety\n";
          $markup .= "  </td>\n";
          $markup .= "  <td class=\"header\">\n";
          $markup .= "    event name\n";
          $markup .= "  </td>\n";
          $markup .= "  <td class=\"header\">\n";
          $markup .= "    event count\n";
          $markup .= "  </td>\n";
          $markup .= "</tr>\n";
          $first_flag = "";
        }

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $plant_history_event_obj->get_rough_date() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $plant_history_event_obj->get_plant_history_obj()->get_plant_obj()->get_common_name_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $plant_history_event_obj->get_plant_history_obj()->get_plant_obj()->get_variety_obj()->get_name_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $plant_history_event_obj->get_name() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "   " . $plant_history_event_obj->get_count() . "<br />";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n"; 
      }
      $markup .= "</table>\n";
    }

    // todo fix the code below
    // show crop plan plants error report
    //include_once("class_list_control.php");
    //$list_control_obj = new ListControl();
    //$given_plant_list_id = $this->get_given_plant_list_id();
    //$user_obj = $this->get_user_obj();
    //$markup .= $list_control_obj->get_missing($plant_id_list, $given_plant_list_id, $user_obj);

    return $markup;
  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    // todo deal with abstract nature of this subsubmenu
    if ($this->get_given_plant_list_id()) {
      // other options

      $markup .= "<strong>See Also</strong>: \n";

      // plant_list
      if ($this->get_given_plant_list_id()) {
        $url = $this->url("plant_lists/" . $this->get_given_plant_list_id());
        $markup .= "<a href=\"" . $url . "\">Plant List " . $this->get_given_plant_list_id() . "</a>\n";
        $markup .= " | ";
      }

      // crop plans
      //if ($this->get_given_project_id()) {
      //  $url = $this->url("crop_plans/project/" . $this->get_given_project_id());
      //  $markup .= "<a href=\"" . $url . "\">Crop Plans</a><br />\n";
      //  $markup .= "<br />\n";
      //}

      if ($this->get_given_plant_list_id()) {
        $url = $this->url("plant_history_events/plant_list/" . $this->get_given_plant_list_id());
        $markup .= "<a href=\"" . $url . "\">Plant History Events for this plant list</a><br />\n";
        $markup .= "<br />\n";
      }

      // todo this should be moved
      //$markup .= "<strong>Sort</strong>:<br />\n";
      //// deal with sort
      //if ($this->get_given_sort() == "date") {
      //  $markup .= "<ul>\n";
      //  $url = $this->url("plant_histories/plant_list/" . $this->get_given_plant_list_id());
      //  $markup .= "  <li><a href=\"" . $url . "\">by <strong>plant</strong></a></li>\n";
      //  $markup .= "<li>by <strong>date</strong></li>\n";
      //  $markup .= "</ul>\n";
      //} else {
      //  $markup .= "<ul>\n";
      //  $markup .= "  <li>by <strong>plant</strong></li>\n";
      //  $url = $this->url("plant_histories/plant_list/" . $this->get_given_plant_list_id());
      //  $markup .= "  <li><a href=\"" . $url . "?sort=date\">by <strong>date</strong></a></li>\n";
      //  $markup .= "</ul>\n";
      //}

    }

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // todo
    //$markup .= $this->output_user_info_message();

    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    $output = "";

    // by given_variable
    // this checks to see if a "given" variable exists
    // todo the code could just loop through an array of given variable names
    if ($this->get_given_plant_list_plant_id()) {
      $output .= "<em>plant_list_plant_id</em> = " . $this->get_given_plant_list_plant_id() . "\n";
    }

    // by type (which is based upon given_varible)
    // this uses the sql situation (which is different but related)
    // so which is better? are they both needed?
    // what I am doing is telling the user the system status
    // this is a debugging issue, too, so perhaps it has reporting levels
    if ($this->get_type() == "get_by_project_id") {
      $output .= "To list a set of <strong>plant_histories</strong>, click on a <strong>plant_list</strong> name.";
    } else if ($this->get_type() == "get_by_plant_list_id") {
      include_once("plant_lists.php");
      $plant_lists_obj = new PlantLists($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_lists_obj->set_user_obj($user_obj);
      $given_plant_list_id = $this->get_given_plant_list_id();
      $output .= $plant_lists_obj->output_plant_list_name_given_id($given_plant_list_id);
    }

    // todo figure out what the following does
    //$markup .= $this->output_given_variables_given_id();

    // note if output, then wrap output in div element
    if ($output) {
      $markup .= "<div class=\"given-variables\">\n";
    }
    $markup .= $output;;
    if ($output) {
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // start list
    $markup .= "<table class=\"plants\" border=\"1\">\n";

    // rows
    $num = 0;
    $num_inside = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_history_obj) {
      $num++;

      // #
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $plant_history_obj->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // plant_list_plant_id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    plant list plant id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("plant_list_plants/" . $plant_history_obj->get_plant_list_plant_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $plant_history_obj->get_plant_list_plant_obj()->get_id() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // plant
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    plant\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_history_obj->get_plant_list_plant_obj()->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // seed_packet_id
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    seed packet\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      // todo fix the more info output
      //$markup .= "    " . $plant_history_obj->get_seed_packet_obj()->output_supplier_and_year_and_days() . "\n";
      // simple output for now
      $markup .= "    " . $plant_history_obj->get_seed_packet_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_pure_list($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // kludge so that the markup from the prepare_query() function is not lost
    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    // return the array
    $list_obj = $this->get_list_bliss()->get_list();
    return $list_obj;
  }

  // method
  public function get_count_given_plant_list_plant_id($given_plant_list_plant_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_plant_id($given_plant_list_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    // return count
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_plant_list_id_given_id($given_plant_history_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_plant_history_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    // note this is an extended search
    // note get this information and go fetch from the db again
    $plant_list_plant_id = $this->get_list_bliss()->get_first_element()->get_plant_list_plant_obj()->get_id();

    include_once("plant_list_plants.php");
    $plant_list_plant_obj = new PlantListPlants($this->get_given_config());
    $user_obj = $this->get_user_obj();
    return $plant_list_plant_obj->get_plant_list_id_given_id($plant_list_plant_id, $user_obj);
  }

  // method
  public function output_plant_history_count_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    $count = $this->get_list_bliss()->get_count();

    $markup .= "<p>" . $count . "</p>";

    return $markup;
  }

  // method
  public function output_seed_packet_info_given_plant_list_plant_id($given_plant_list_plant_id, $given_user_obj) {
    $markup = "";

    $this->get_list_bliss()->empty_list();

    // set
    $this->set_given_plant_list_plant_id($given_plant_list_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $plant_history_obj) {
      $seed_packet_id = $plant_history_obj->get_seed_packet_obj()->get_id();
      if ($seed_packet_id) {
        $markup .= "[";
        include_once("seed_packets.php");
        $seed_packet_obj = new SeedPackets($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $seed_packet_obj->output_info_given_id($seed_packet_id, $user_obj);
        $markup .= "]\n";
      }
    }

    return $markup;
  }

  // method
  public function get_count_with_link_given_seed_packet_id($given_seed_packet_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_seed_packet_id($given_seed_packet_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    if ($this->get_list_bliss()->get_count() == 0) {
      // no link
      $markup .= $this->get_list_bliss()->get_count();
    } else {
      // with link
      $url = $this->url("plant_histories/seed_packets/" . $given_seed_packet_id);
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= $this->get_list_bliss()->get_count();
      $markup .= "</a>";
    }

    return $markup;
  }
}
