<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lands.php

include_once("lib/socation.php");

class Lands extends Socation {

  // given
  private $given_agricultural_type_id;
  private $given_view = "online"; // default

  // given_agricultural_type_id
  public function set_given_agricultural_type_id($var) {
    $this->given_agricultural_type_id = $var;
  }
  public function get_given_agricultural_type_id() {
    return $this->given_agricultural_type_id;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attributes
  private $agricultural_type_obj;

  // agricultural_type_obj
  public function get_agricultural_type_obj() {
    if (! isset($this->agricultural_type_obj)) {
      include_once("agricultural_types.php");
      $this->agricultural_type_obj = new AgriculturalTypes($this->get_given_config());
    }
    return $this->agricultural_type_obj;
  }

  // method
  public function get_name_with_link_special() {
    // target of hyperlink is relative
    // target of hyperlink depends upon the class of the land
    // figure out the target_page
    $target_page = "";
    if ($this->get_class_obj()->get_id() == "1") {
      // skip
      // this is the "unknown"
    } else {
      // todo: fix the below so that is is not hard-coded
      // for example:
      //   id 2 is greenhouse.php
      // make filename based upon the name of the class
      // assume a php file, so add the php extension
      $target_page = strtolower($this->get_class_obj()->get_name()) . ".php";
    }
    // ok, now output the string
    if ($target_page) {
      return "<a href=\"" . $target_page . "?id=" . $this->get_instance_id() . "\" class=\"show\">" . $this->get_name() . "</a>";
    } else {
      // output just name
      // do not output as a hyperlink
      return $this->get_name();
    }
  }

  // method
  private function make_land() {
    $obj = new Lands($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_agricultural_type_id()) {
      $this->set_type("get_by_agricultural_type_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      // note: distinct
      $sql = "SELECT DISTINCT ON (lands.id) lands.*, agricultural_types.img_url FROM lands, projects, agricultural_types WHERE agricultural_types.id = lands.agricultural_type_id AND lands.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      // todo did not show all of the rows
      //$sql = "SELECT DISTINCT ON (lands.sort) lands.*, agricultural_types.id, agricultural_types.name, agricultural_types.img_url FROM lands, projects, agricultural_types WHERE lands.agricultural_type_id = agricultural_types.id ORDER BY lands.sort DESC;";
      // show all rows
      $sql = "SELECT lands.*, agricultural_types.name, agricultural_types.img_url FROM lands, agricultural_types WHERE lands.agricultural_type_id = agricultural_types.id ORDER BY lands.sort DESC;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      //$sql = "SELECT DISTINCT ON (lands.id) lands.*, agricultural_types.img_url FROM lands, agricultural_types, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'lands' AND lands.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY lands.id;";
      // from counter
      $sql = "SELECT DISTINCT ON (lands.sort, lands.id) lands.* FROM lands, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'lands' AND lands.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY lands.sort DESC, lands.id;";

    } else if ($this->get_type() == "get_by_agricultural_type_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT lands.*, agricultural_types.img_url FROM lands, agricultural_types WHERE agricultural_types.id = lands.agricultural_type_id AND lands.agricultural_type_id = " . $this->get_given_agricultural_type_id() . " ORDER BY lands.name;";

    } else if ($this->get_type() == "get_beds_given_field_id") {
      // beds
      //$sql = "SELECT beds.id, beds.name, beds.width, beds.length, beds.user_name, beds.field_id, fields.name FROM beds, fields WHERE beds.field_id = " . $this->get_given_field_id() . " AND beds.field_id = fields.id ORDER BY beds.id;";

    } else if ($this->get_type() == "get_places") {
      // places
      $database_name = "plantdot_principle";
      //$sql = "SELECT places_a.id, places_a.name, places_a.description, places_a.site_flag, places_a.parent_id, places_a.zone_flag, places_b.name, places_a.class_id, classes.name, places_a.instance_id, places_a.user_name from places as places_a, places as places_b, classes where places_a.class_id = classes.id and places_a.parent_id = places_b.id AND places_a.user_name = '" . $this->get_user_obj()->name . "' order by places_a.site_flag desc, classes.name, places_a.name";

    } else if ($this->get_type() == "get_garden_given_id") {
      // garden
      $database_name = "plantdot_principle";
      //$sql = "SELECT places_a.name, places_a.description, places_a.site_flag, places_a.parent_id, places_a.zone_flag, places_b.name FROM places as places_a, places as places_b WHERE places_a.id = " . $this->get_given_id()  . " and places_a.parent_id = places_b.id;";

    } else if ($this->get_type() == "get_field_given_id") {
      // field
      //$sql = "SELECT fields.id, fields.name, fields.description, fields.row_length, fields.bed_width, fields.number_of_beds FROM fields WHERE fields.id = " . $this->get_given_id() . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_land();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->get_agricultural_type_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_description(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->get_agricultural_type_obj()->set_img_url(pg_result($results, $lt, 7));

      }
    } else if ($this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_land();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->get_agricultural_type_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_description(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5)); 
        $obj->set_img_url(pg_result($results, $lt, 6));
        //$obj->set_temp_parent_land_id(pg_result($results, $lt, 7));
        //$obj->set_temp_layout_id(pg_result($results, $lt, 8));
        //$obj->set_temp_soil_area_id(pg_result($results, $lt, 9));

      }
    } else if ($this->get_type() == "get_by_agricultural_type_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_land();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->get_agricultural_type_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_description(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        //$obj->set_temp_parent_land_id(pg_result($results, $lt, 6));
        $obj->set_img_url(pg_result($results, $lt, 7));
        //$obj->set_temp_layout_id(pg_result($results, $lt, 8));
        //$obj->set_temp_soil_area_id(pg_result($results, $lt, 9));


      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_land();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->get_agricultural_type_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_description(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->get_agricultural_type_obj()->set_name(pg_result($results, $lt, 7));
        $obj->get_agricultural_type_obj()->set_img_url(pg_result($results, $lt, 8));

      }
    } else if ($this->get_type() == "get_land_list") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $land = $this->make_land();
        $land->set_id(pg_result($results, $lt, 0));
        $land->set_name(pg_result($results, $lt, 1));
        $land->set_supplier_id(pg_result($results, $lt, 2));
        $land->set_agricultural_type_id(pg_result($results, $lt, 3));
        $land->set_supplier_name(pg_result($results, $lt, 4));
        $land->set_agricultural_type_name(pg_result($results, $lt, 5));
      }
    } else if ($this->get_type() == "get_beds_by_field_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_name(pg_result($results, $lt, 1));
        $this->set_width(pg_result($results, $lt, 2));
        $this->set_user_name(pg_result($results, $lt, 4));
        $this->get_field_obj()->set_id(pg_result($results, $lt, 5));
        $this->get_field_obj()->set_name(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "get_beds_given_field_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $bed = $this->make_bed();
        $bed->set_id(pg_result($results, $lt, 0));
        $bed->set_name(pg_result($results, $lt, 1));
        $bed->set_width(pg_result($results, $lt, 2));
        $bed->set_user_name(pg_result($results, $lt, 4));
        $bed->get_field_obj()->set_id(pg_result($results, $lt, 5));
        $bed->get_field_obj()->set_name(pg_result($results, $lt, 6));
      }
    } else if ($type == "get_lands") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $place_obj = $this->make_place();
        $place_obj->set_id(pg_result($results, $lt, 0));
        $place_obj->set_name(pg_result($results, $lt, 1));
        $place_obj->set_description(pg_result($results, $lt, 2));
        $place_obj->set_site_flag(pg_result($results, $lt, 3));
        $place_obj->get_parent_obj()->set_id(pg_result($results, $lt, 4));
        $place_obj->set_zone_flag(pg_result($results, $lt, 5));
        $place_obj->get_parent_obj()->set_name(pg_result($results, $lt, 6));
        $place_obj->set_instance_id(pg_result($results, $lt, 9));
        $place_obj->set_user_name(pg_result($results, $lt, 10));
      }
    } else if ($type == "get_garden_given_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_name(pg_result($results, $lt, 0));
        $this->set_description(pg_result($results, $lt, 1));
        $this->set_site_flag(pg_result($results, $lt, 2));
        $this->set_parent_id(pg_result($results, $lt, 3));
        $this->set_zone_flag(pg_result($results, $lt, 4));
        $this->set_parent_name(pg_result($results, $lt, 5));
      }
    } else if ($type == "get_field_given_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_name(pg_result($results, $lt, 1));
        $this->set_description(pg_result($results, $lt, 2));
        $this->set_row_length(pg_result($results, $lt, 3));
        $this->set_bed_width(pg_result($results, $lt, 4));
        $this->set_number_of_beds(pg_result($results, $lt, 5));
      }
  } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // only authenticated users
    if ($this->get_user_obj()) {
      //if ($this->get_user_obj()->uid) {
      //}
    }

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // todo fix this because sometimes it does not know project_id
    //$markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("agricultural_types");
    $markup .= "    <a href=\"" . $url . "\">agricultural type</a>\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    parent_land_id\n";
    //$markup .= "  </td>\n";
    // todo figure out visits to land
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    visits\n";
    //$markup .= "  </td>\n"; 
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    soil_tests\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 140px;\">\n";
    $markup .= "    s.e.\n";
    $markup .= "  </td>\n";
    // todo clean up this kruft
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    temp_parent_land_id\n";
    //$markup .= "    temp_layout_id\n";
    //$markup .= "    temp_soil_area_id\n";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // set up a utility class
    include_once("lib/timekeeper.php");
    $timekeeper_obj = new Timekeeper();

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $land) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // project
      //$markup .= "  <td>\n";
      //$user_obj = $this->get_user_obj();
      //$markup .= $land->get_project_land_obj()->get_projects_given_land_id($user_obj, $land->get_id());
      //$markup .= "  </td>\n";

      // status
      $markup .= "  <td>\n";
      $markup .= "    " . $land->get_status();
      $markup .= "  </td>\n";

      // sort
      $sort = $land->get_sort();
      $column_name = "sort";
      $class_name_for_url = "lands";
      // todo this class does not have a given_view (need to design default)
      //$given_view = $this->get_given_view();
      $given_view = "none";
      // todo turn off because link was not working and too hard to fix
      $id  = $land->get_id();
      $markup .= $timekeeper_obj->get_cell_colorized_given_sort_date($sort, $column_name, $class_name_for_url, $id, $given_view, "");

      // id with link
      $markup .= "  <td>\n";
      $markup .= "    " . $land->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img_url
      $markup .= "  <td>\n";
      $padding = "4px";
      $float = "center";
      $width = "65px";
      $markup .= "    " . $land->get_img_as_img_element_with_link($padding, $float, $width) . "<br />\n";
      $markup .= "  </td>\n";

      // name with link
      $markup .= "  <td>\n";
      $markup .= "    " . $land->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // agricultural_type
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      // todo move to config system that cascades for value
      $width = "25";
      $markup .= "    " . $land->get_agricultural_type_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "<br />\n";
      $markup .= "    " . $land->get_agricultural_type_obj()->get_name() . "\n";
      $markup .= "  </td>\n";

      // parent_land_id
      //$markup .= "  <td>\n";
      //$markup .= "    [id]" . $land->get_parent_land_id() . "\n";
      //$markup .= "  </td>\n";

      // soil tests
      //$markup .= "  <td>\n";
      //$markup .= "    todo\n";
      //$markup .= "  </td>\n";

      // s.e. scene_elements
      $markup .= "  <td>\n";
      include_once("scene_elements.php");
      $scene_element_obj = new SceneElements($this->get_given_config());
      $class_name = get_class($this);
      $id = $land->get_id();
      $user_obj = $this->get_user_obj();
      $markup .= $scene_element_obj->get_scene_elements_list($class_name, $id, $user_obj);
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // get object
    $land_obj = $this->get_list_bliss()->get_first_element();

    // output

    // sort
    $markup .= "<p> sort = " . $land_obj->get_sort() . "</p>\n";

    // img
    $padding = "4px";
    $float = "left";
    $width = "40px";
    $markup .= "    " . $land_obj->get_img_as_img_element_with_link($padding, $float, $width) . "<br />\n";

    // name
    $markup .= "<h3>" . $land_obj->get_name() . "</h3>\n";

    // description
    $markup .= $land_obj->get_description() . "\n";

    // agriculture type
    $markup .= "<hr />\n";
    $markup .= "<h4 style=\"background-color: #CCCCCC;\">agricultural_type</h4>";
    $user_obj = $this->get_user_obj();
    $markup .= $land_obj->get_agricultural_type_obj()->output_img_url_as_img_element_with_name($user_obj);
    $markup .= "<br />\n";

    // land_traits
    $markup .= "<hr />\n";
    $markup .= "<h4 style=\"background-color: #CCCCCC;\">LandTraits</h4>";
    if ($land_obj->get_id()) {
      include_once("land_traits.php");
      $land_traits_obj = new LandTraits($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $land_traits_obj->get_table_given_land_id($land_obj->get_id(), $user_obj);
    } else {
      $markup .= "<p style=\"error\">No land_id. Unable to display LandTraits.</p>";
    }
    $markup .= "<br />\n";
    
    // soil_areas
    $markup .= "<hr />\n";
    $markup .= "<h4 style=\"background-color: #CCCCCC;\">Soil Areas</h4>";
    if ($land_obj->get_id()) {
      include_once("soil_areas.php");
      $soil_area_obj = new SoilAreas($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $soil_area_obj->get_list_given_land_id($land_obj->get_id(), $user_obj);
    } else {
      $markup .= "<p style=\"error\">No land_id. Unable to display SoilAreas.</p>";
    }
    $markup .= "<br />\n";

    // todo consider visits with respect to land
    $markup .= "<hr />\n";
    $markup .= "<h4>consider visits with respect to land</h4>\n";
    $markup .= "<p>";
    $markup .= "go to: <a href=\"visits?land_id=" . $this->get_id() . "\">visits</a>\n";
    $markup .= "</p>";

    // todo consider soil tests with respect to land
    $markup .= "<hr />\n";
    $markup .= "<h4>consider soil tests with respect to land</h4>\n";
    $markup .= "<p>";
    $markup .= "go to: <a href=\"soil_tests?land_id=" . $this->get_id() . "\">soil tests</a>\n";
    $markup .= "</p>";

    return $markup;
  }

  // method
  public function output_bed() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    width\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    length\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    field\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    user name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $bed) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $bed->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $bed->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $bed->get_width() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $bed->get_length() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $bed->get_field_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"user\">\n";
      $markup .= "    " . $bed->get_user_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_bed_single() {
    $markup = "";

    $markup .= "<p>" . $this->get_id() . "</em>\n";
    $markup .= "<h2>" . $this->get_name() . "</h2>\n";

    if ($this->get_width()) {
      $markup .= "<p>" . $this->get_width() . "</p>\n";
    } else {
      $markup .= "<p><em>no length<em></p>\n";
    }

    if ($this->get_length()) {
      $markup .= "<p>" . $this->get_length() . "</p>\n";
    } else {
      $markup .= "<p><em>no length<em></p>\n";
    }

    return $markup;
  }

  // method
  public function output_place() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    class\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    site flag\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    within this land\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    zone flag\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    user name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $place) {
      $markup .= $this->output_place_row($place);
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_place_row($place) {
    $markup = "";

    $markup .= "<tr>\n";

    $markup .= "  <!-- place id = " . $place->get_id() . " -->\n";

    $markup .= "  <td>\n";
    $markup .= "    " . $place->get_name_with_link() . "\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    " . $place->get_description() . "\n";
    $markup .= "  </td>\n";

    if ($place->get_site_flag() == "t") {
      $markup .= "  <td class=\"text-data\">\n";
    } else {
      $markup .= "  <td>\n";
    }
    $markup .= "    " . $place->get_site_flag() . "\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    <!-- id = " . $place->get_parent_obj()->get_id() . " -->\n";
    $markup .= "    " . $place->get_parent_obj()->get_name() . "\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    " . $place->get_zone_flag() . "\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"user\">\n";
    $markup .= "    " . $place->get_user_name() . "\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    return $markup;
  }

  // method
  public function output_field() {
    $markup = "";

    $markup .= "<h2>" . $this->get_name() . "</h2>\n";

    $markup .= "<p>" . $this->get_description() . "</p>\n";

    $markup .= "<h3>General Definitions</h3>\n";
    $markup .= "row length = " . $this->get_row_length() . "<br />\n";
    $markup .= "bed width = " . $this->get_bed_width() . "<br />\n";
    $markup .= "number of beds = " . $this->get_number_of_beds() . "<br />\n";

    $markup .= "<h3>Beds</h3>\n";
    include_once("beds.php");
    $beds_obj = new Beds($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $beds_obj->set_user_obj($user_obj);
    $field_id = $this->get_id();
    $beds_obj->set_given_field_id($field_id);
    $markup .= $beds_obj->output_table_given_field_id();

    return $markup;
  }

  // method
  public function get_project_id_given_land_id($given_land_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_land_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_project_obj()->get_id();
      $markup .= $project_id;

      // debug
      //print "debug lands project_id = " . $project_id . "<br />\n";
    }

    return $markup;
  }

  // method
  public function get_build_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug lands given_id " . $this->get_given_id() . "<br />\n";

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no lands were found.</p>\n";;
      return $markup;
    }

    $markup .= "<h3 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px;\">lands</h3>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $land) {
      $num++;

      // add space for breathing room
      $markup .= "<br />\n";

      // num
      //$markup .= "  <p>\n";
      //$markup .= "    " . $num . "<br />\n";
      //$markup .= "  </p>\n";

      // name
      $land->set_given_project_id($this->get_given_project_id());
      $markup .= "    <h1>" . $land->get_name() . "</h1>\n";

      // id
      $markup .= "  <p><em>This is land id " . $land->get_id() . "</em></p>\n";

      // description
      $markup .= "  <p>\n";
      $markup .= "    " . $land->get_description() . "\n";
      //$markup .= "    <div style=\"float: right;\"><em>sort = " . $land->get_sort() . "</em></div>\n";
      $markup .= "  </p>\n";

    }

    return $markup;
  }

  // method
  public function get_land_name_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $land) {
      $url = $this->url("lands/" . $land->get_id());
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= $land->get_name() . "\n";
      $markup .= "</a>\n";
    }

    return $markup;
  }

  // method
  public function get_lands_count_given_agricultural_type($given_agricultural_type_id, $given_user_obj) {
    $markup = "";

    $this->set_given_agricultural_type_id($given_agricultural_type_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">No lands were found.</p>\n";;
      return $markup;
    }

    $lands_count = $this->get_list_bliss()->get_count();

    return $lands_count;
  }

  // method
  public function get_lands_given_agricultural_type($given_agricultural_type_id, $given_user_obj) {
    $markup = "";

    $this->set_given_agricultural_type_id($given_agricultural_type_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 0) {
      $markup .= "<p style=\"error\">No lands were found.</p>\n";;
    } else {

      $markup .= "<table class=\"plants\">\n";
      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $land) {
        $num++;

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $land->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $land->get_name_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
    }

    return $markup;
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {

      $this_architectural_type_id = $this->get_agricultural_type_obj()->get_id();
      $given_architectural_type_id = $given_pm_obj->get_agricultural_type_obj()->get_id();
      $markup .= "debug lands: <strong>check for architectural_type (" . $this_architectural_type_id . " + " . $given_architectural_type_id . ")</strong><br />\n";
      $markup .= "debug lands: <strong>check for hierachy and scale</strong><br />\n";

    } else if (get_class($given_pm_obj) == "PlantLists") {
      $markup .= "debug lands: ignore, not related<br />\n";
    } else {
      $markup .= "debug lands: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug lands: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error lands: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $land) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $land->get_id() . " " . $land->get_name_with_link() . " rent " . $land->get_energy_flows_dollars("rent") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $land->get_energy_flows_dollars("rent");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error lands: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "lands: no lands found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // preprocess
    include_once("scene_elements.php");
    $scene_element_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    // todo note that this is hard-coded and should be a db issue
    $class_name = get_class($this);
    $id = $this->get_id();
    $count_of_matching_scene_elements = $scene_element_obj->get_count_of_matching_scene_elements($class_name, $id, $user_obj);
    
    // debug
    //$markup .= "debug lands: count_of_matching_scene_elements = " . $count_of_matching_scene_elements . "\n";

    if ($given_account_name == "rent") {
      // note extract XML from description field
      $field_string = $this->get_description();
      // debug
      //$markup .= $field_string;

      // get first line
      // extract string based upon substring
      $account_rule_string = substr($field_string, 0, strpos($field_string, "account-rule"));
      //$markup .= $account_rule_string;

      // narrow
      // extract string based upon substring
      $amount_string = substr($account_rule_string, 0, strpos($account_rule_string, "-dollars-per-month"));

      // note convert to portion of scene_elements
      $markup .= $this->convert_to_portion_of_scene_elements($amount_string, $count_of_matching_scene_elements);

    } else {
      $markup .= "error lands: given_account_type not known.<br />\n";
    }

    return $markup;
  }

  // method
  protected function output_preface_single() {
    $markup = "";

    $url = $this->url("lands");
    $markup .= "<a href=\"" . $url . "\">get all lands</a><br />\n";

    return $markup;
  }

}
