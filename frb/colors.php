<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2013-04-02
// version 1.6 2016-05-07

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/field_tests.php

include_once("class_standard.php");

class Colors extends Standard {

  // attributes
  private $id;
  private $tla;
  private $name;
  private $url;
  private $hex_code;
  private $full_name;

  // method
  private function make_color() {
    $obj = new Colors($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_color();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_tla(pg_result($results, $lt, 1));
        $obj->set_name(pg_result($results, $lt, 2));
        $obj->set_url(pg_result($results, $lt, 3));
        $obj->set_hex_code(pg_result($results, $lt, 4));
        $obj->set_full_name(pg_result($results, $lt, 5));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_process();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type: <kbd>" . $type . "</kbd>");
    }

  }

}
