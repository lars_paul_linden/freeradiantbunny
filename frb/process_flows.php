<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-17
// version 1.2 2015-01-04
// version 1.4 2015-06-19
// version 1.5 2015-10-16
// version 1.5 2016-03-12
// version 1.6 2016-03-25
// version 1.7 2017-04-22
// version 1.8 2017-04-28
// version 1.8 2017-11-01

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/process_flows.php

include_once("lib/scrubber.php");

class ProcessFlows extends Scrubber {

  // given
  private $given_id;
  private $given_process_id;
  private $given_project_id;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $parent_process_obj;
  private $child_process_obj;
  private $description;
  private $sort;

  // parent_process_obj
  public function get_parent_process_obj() {
    if (! isset($this->parent_process_obj)) {
      include_once("processes.php");
      $this->parent_process_obj = new Processes($this->get_given_config());
    }
    return $this->parent_process_obj;
  }

  // todo this might not be the best way to override this
  // todo perhaps this has the wrong inheritanced parent class
  public function get_id() {
    // note make something work
    return $this->parent_process_obj;
  }

  // child_process_obj
  public function get_child_process_obj() {
    if (! isset($this->child_process_obj)) {
      include_once("processes.php");
      $this->child_process_obj = new Processes($this->get_given_config());
    }
    return $this->child_process_obj;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // sort
  public function set_sort($var) {
    $this->sort = $var;
  }
  public function get_sort() {
    return $this->sort;
  }

  // method
  private function make_process_flow() {
    $obj = new ProcessFlows($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      // assumes parent
      // todo ask if this table should have an id (for convention sake)
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // debug
    //print "debug process_flows user_obj " . get_class($this->get_user_obj()) . "<br />\n";

    if ($this->get_type() == "get_all") {
      $sql = "SELECT process_flows.*, processes_parent.name, processes_child.name FROM process_flows, processes as processes_parent, processes as processes_child WHERE process_flows.parent_process_id = processes_parent.id AND process_flows.child_process_id = processes_child.id ORDER BY process_flows.sort, process_flows.parent_process_id, process_flows.child_process_id;";

    } else if ($this->get_type() == "get_by_process_id") {
      if ($this->get_given_process_id()) {
        // need to write this sql statement
        // warning: the following is a UNION statement
        $sql = "SELECT process_flows.*, processes_parent.name, processes_child.name FROM process_flows, processes as processes_parent, processes as processes_child WHERE process_flows.parent_process_id = " . $this->get_given_process_id() . " AND process_flows.parent_process_id = processes_parent.id AND process_flows.child_process_id = processes_child.id UNION SELECT process_flows.*, processes_parent.name, processes_child.name FROM process_flows, processes as processes_parent, processes as processes_child WHERE process_flows.child_process_id = " . $this->get_given_process_id() . " AND process_flows.parent_process_id = processes_parent.id AND process_flows.child_process_id = processes_child.id;";
       // debug
       //print "debug process_flows: sql = " . $sql . "<br />\n";
      }

    } else if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT process_flows.* FROM process_flows, processes, projects, goal_statements, business_plan_texts WHERE projects.id = " . $this->get_given_project_id() . " AND projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND processes.id = process_flows.parent_process_id UNION SELECT process_flows.* FROM process_flows, processes, projects, goal_statements, business_plan_texts WHERE projects.id = " . $this->get_given_project_id() . " AND projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND processes.id = process_flows.child_process_id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data." . " type = " . $this->get_type() . "<br />\n");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_process_flow();
        $obj->get_parent_process_obj()->set_id(pg_result($results, $lt, 0));
        $obj->get_child_process_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->get_parent_process_obj()->set_name(pg_result($results, $lt, 4));
        $obj->get_child_process_obj()->set_name(pg_result($results, $lt, 5));
      }
    } else if ($this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_process_flow();
        $obj->get_parent_process_obj()->set_id(pg_result($results, $lt, 0));
        $obj->get_child_process_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .=  "<table class=\"plants\">\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $process_flow) {

      // heading
      // project
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead-narrow\">\n";
      $markup .=  "    project\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td>\n";
      include_once("projects.php");
      $project_obj = new Projects($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $project_data = $project_obj->get_img_with_link_given_given_process_id($process_flow->get_process_obj()->get_id(), $user_obj);
      $markup .= "    " . $project_data . "\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // process
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead-narrow\">\n";
      $markup .=  "    process\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td>\n";
      $url = $this->url("processes/" . $process_flow->get_process_obj()->get_id());
      $markup .=  "    <a href=\"" . $url . "\">" . $process_flow->get_process_obj()->get_id() . "</a><br />\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // description
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead\">\n";
      $markup .=  "    description\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $process_flow->get_description() . "<br />\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // name
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead\">\n";
      $markup .=  "    name\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td>\n";
      $markup .=  "    " . $process_flow->get_name() . "<br />\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";
    }
    $markup .=  "</table>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // todo clean up the temp code

    $markup .=  "<div id=\"tenperday\">\n";
    $markup .=  "<table class=\"plants\">\n";

    // heading
    $markup .=  "<tr>\n";
    $markup .=  "  <td class=\"colhead\">\n";
    $markup .=  "    #\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead-narrow\" style=\"width: 120px; text-align: center;\">\n";
    $markup .=  "    sort\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead-narrow\">\n";
    $markup .=  "    project\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead-narrow\">\n";
    $markup .=  "    parent process\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead-narrow\">\n";
    $markup .=  "    project\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead-narrow\">\n";
    $markup .=  "    child process\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead\">\n";
    $markup .=  "    description\n";
    $markup .=  "  </td>\n";
    $markup .=  "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $process_flow) {

       $markup .=  "<tr>\n";

      // num
      $num++;
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $num . "\n";
      $markup .=  "  </td>\n";

      // sort
      $num++;
      $markup .=  "  <td class=\"mid\" style=\"text-align: center;\">\n";
      $markup .=  "    " . $process_flow->get_sort() . "\n";
      $markup .=  "  </td>\n";

      // project of parent_process
      $markup .=  "  <td>\n";
      include_once("projects.php");
      $project_obj = new Projects($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $project_name = $project_obj->get_name_given_process_id($process_flow->get_parent_process_obj()->get_id(), $user_obj);
      $markup .=  "    " . $project_name . "\n";
      $markup .=  "  </td>\n";

      // parent_process
      $markup .=  "  <td>\n";
      if ($this->get_type() == "get_by_project_id") {
        $markup .=  "fix here\n";
      } else {
        $url = $this->url("processes/" . $process_flow->get_parent_process_obj()->get_id());
        $markup .=  "    <a href=\"" . $url . "\">" . $process_flow->get_parent_process_obj()->get_name() . "</a><br />\n";
      }
      $markup .=  "  </td>\n";

      // project of child_process
      $markup .=  "  <td>\n";
      include_once("projects.php");
      $project_obj = new Projects($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $project_name = $project_obj->get_name_given_process_id($process_flow->get_child_process_obj()->get_id(), $user_obj);
      $markup .=  "    " . $project_name . "\n";
      $markup .=  "  </td>\n";

      // child_process
      $markup .=  "  <td>\n";
      if ($this->get_type() == "get_by_project_id") {
        $markup .=  "fix here\n";
      } else {
        $url = $this->url("processes/" . $process_flow->get_child_process_obj()->get_id());
        $markup .=  "    <a href=\"" . $url . "\">" . $process_flow->get_child_process_obj()->get_name() . "</a><br />\n";
      }
      $markup .=  "  </td>\n";

      // description
      $markup .= "  <td style=\"text-align: left;\">\n";
      $markup .=  "    " . $process_flow->get_description() . "\n";
      $markup .=  "  </td>\n";

      $markup .=  "</tr>\n";
    }
    $markup .=  "</table>\n";

    return $markup;
  }

  // method
  public function output_sidecar($given_process_id, $given_user_obj, $heading = "") {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_process_id");

    // debug
    //print "type " . $this->get_type() . "<br />\n";
    //print "scene_elements: user_name " . $this->get_user_obj()->name . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    // list heading
    if ($heading) {
      $markup .= "<h2>Process Flows</h2>\n";
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<div style=\"margin: 4px 4px 4px 0px; padding: 10px 6px 10px 6px; background-color: #E8C782; font-size: 100%;\">\n";
      $markup .= $this->output_aggregate();
      $markup .= "</div>\n";

    } else { 
      $markup .= "<div style=\"margin: 2px 2px 2px 2px; padding: 1px 1px 1px 1px; background-color: #E8C782; font-size: 100%;\">\n";
     $markup .= "<p>No process_flows found.</p>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_process_flows_in_given_process_id($given_process_id, $given_user_obj, $heading = "") {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_process_id");

    // load data from database
    $markup .= $this->prepare_query();

    // list heading
    if ($heading) {
      $markup .= "<h2>Process Flows</h2>\n";
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .=  "<table class=\"plants\" style=\"width: 100%; font-size: 70%;\">\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $process_flow) {

        if ($process_flow->get_child_process_obj()->get_id() == $given_process_id) {
          // num
          $num++;

          // parent_process
          $markup .=  "<tr>\n";
          $color = "#FFA07A";
          //$color = "#A4D3EE";
          $markup .=  "  <td style=\"background-color: " . $color . "; width: 100%;\">\n";
          $url = $this->url("processes/" . $process_flow->get_parent_process_obj()->get_id());
          $markup .=  "    <a href=\"" . $url . "\">" . $process_flow->get_parent_process_obj()->get_name() . "</a><br />\n";
          $markup .=  "  </td>\n";
          $markup .=  "</tr>\n";
        }
      }
      $markup .=  "</table>\n";

      // if no rows, then return null string
      if (! $num) {
        return "";
      }

    } else {
      $markup .= "<div style=\"margin: 2px 2px 2px 2px; padding: 1px 1px 1px 1px; background-color: #E8C782; font-size: 100%;\">\n";
      $markup .= "<p>No process _flows found.</p>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_count_given_project_id($given_project_id, $given_user_obj, $heading = "") {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_project_id");

    // debug
    //print "debug process_flows given_project_id = " . $this->get_given_project_id() . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    // debug
    //print "debug process_flows type = " . $this->get_type() . "<br />\n";

    // list heading
    if ($heading) {
      $markup .= "<h2>Process Flows</h2>\n";
    }

    // todo perhaps only output if there are items to output
    $count = $this->get_list_bliss()->get_count();
    $markup .= $count;

    return $markup;
  }

  // method
  public function get_process_flows_out_given_process_id($given_process_id, $given_user_obj, $heading = "") {
    $markup = "";

    // reset
    $this->get_list_bliss()->empty_list();

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    // debug
    //print "debug process_flows: given_process_id = " .  $given_process_id . "<br />\n";
    //print "debug process_flows: get_given_process_id() = " .  $this->get_given_process_id() . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    // list heading
    if ($heading) {
      $markup .= "<h2>Process Flows</h2>\n";
    }

    // debug
    //$markup .= "debug process_flows count = " . $this->get_list_bliss()->get_count() . "\n";

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .=  "<table class=\"plants\" style=\"width: 100%; font-size: 70%;\">\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $process_flow) {

        if ($process_flow->get_parent_process_obj()->get_id() == $given_process_id) {
          // num
          $num++;

          // child_process
          $markup .=  "<tr>\n";
          $color = "#A4D3EE";
          $markup .=  "  <td style=\"background-color: " . $color . "; width: 100%;\">\n";
          $url = $this->url("processes/" . $process_flow->get_child_process_obj()->get_id());
          $markup .=  "    <a href=\"" . $url . "\">" . $process_flow->get_child_process_obj()->get_name() . "</a><br />\n";
          $markup .=  "  </td>\n";
          $markup .=  "</tr>\n";
        }
      }
      $markup .=  "</table>\n";

      // if no rows, then return null string
      if (! $num) {
        return "";
      }

    } else {
      $markup .= "<div style=\"margin: 2px 2px 2px 2px; padding: 1px 1px 1px 1px; background-color: #E8C782; font-size: 100%;\">\n";
      $markup .= "<p>No process_flows found.</p>\n";
      $markup .= "</div>\n";
    }

     return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug process_flows: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error process_flows: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $process_flow) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " parent " . $process_flow->get_parent_process_obj()->get_id_with_link() . " child " . $process_flow->get_child_process_obj()->get_id_with_link() . " documentation " . $process_flow->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $process_flow->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error process_flows: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "process_flows: no process_flows found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

  // method
  public function get_table_given_project_id($given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error process_flows: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
        $markup .=  "<table class=\"plants\">\n";

        // heading
        $markup .=  "<tr>\n";
        $markup .=  "  <td class=\"colhead\">\n";
        $markup .=  "    #\n";
        $markup .=  "  </td>\n";
        $markup .=  "  <td class=\"colhead-narrow\">\n";
        $markup .=  "    parent process\n";
        $markup .=  "  </td>\n";
        $markup .=  "  <td class=\"colhead-narrow\">\n";
        $markup .=  "    child process\n";
        $markup .=  "  </td>\n";
        $markup .=  "</tr>\n";

        $num = 0;
        foreach ($this->get_list_bliss()->get_list() as $process_flow) {

            $markup .=  "<tr>\n";

            // num
            $num++;
            $markup .=  "  <td class=\"mid\">\n";
            $markup .=  "    " . $num . "\n";
            $markup .=  "  </td>\n";

            // parent_process
            $markup .=  "  <td>\n";
            $url = $this->url("processes/" . $process_flow->get_parent_process_obj()->get_id());
            $markup .=  "    <a href=\"" . $url . "\">" . $process_flow->get_parent_process_obj()->get_name() . "</a><br />\n";
            $markup .=  "  </td>\n";

            // child_process
            $markup .=  "  <td>\n";
            $url = $this->url("processes/" . $process_flow->get_child_process_obj()->get_id());
            $markup .=  "    <a href=\"" . $url . "\">" . $process_flow->get_child_process_obj()->get_name() . "</a><br />\n";
            $markup .=  "  </td>\n";

            $markup .=  "</tr>\n";
        }
        $markup .=  "</table>\n";
        
    } else {
      // todo fix this so that it goes through error system
      $markup .= "process_flows: no process_flows found<br />";
    }

    return $markup;
  }

  // method
  public function output_preface() {

  }

}
