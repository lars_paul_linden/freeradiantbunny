<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about
// http://freeradiantbunny.org/main/en/docs/frb/soil_areas.php

include_once("lib/standard.php");

class SoilAreas extends Standard {

  // given
  private $given_layout_id;
  private $given_land_id;
 
  // given_layout_id
  public function set_given_layout_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "layout_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_layout_id = $var;
    }
  }
  public function get_given_layout_id() {
    return $this->given_layout_id;
  }

  // given_land_id
  public function set_given_land_id($var) {
    $this->given_land_id = $var;
  }
  public function get_given_land_id() {
    return $this->given_land_id;
  }

  // attributes
  private $land_obj;

  // where this soil_area is relative to associated layout
  // private $position;

  // todo clean up the following
  //private $plant_obj;
  //private $row_num;
  //private $col_num;
  //private $block_number;
  //private $block_width;
  //private $block_length;

  // bed_num
  public function set_bed_num($var) {
    $this->bed_num = $var;
  }
  public function get_bed_num() {
    return $this->bed_num;
  }

  // width
  public function set_width($var) {
    $this->width = $var;
  }
  public function get_width() {
    return $this->width;
  }

  // length
  public function set_length($var) {
    $this->length = $var;
  }
  public function get_length() {
    return $this->length;
  }

  // farmer
  public function set_farmer($var) {
    $this->farmer = $var;
  }
  public function get_farmer() {
    return $this->farmer;
  }

  // height
  public function set_height($var) {
    $this->height = $var;
  }
  public function get_height() {
    return $this->height;
  }

  // position
  public function set_position($var) {
    $this->position = $var;
  }
  public function get_position() {
    return $this->position;
  }

  // plant_obj
  // no setter
  //public function get_plant_obj() {
  //  if (! isset($this->plant_obj)) {
  //    include_once("plants.php");
  //    $this->plant_obj = new Plants($this->get_given_config());
  //  }
  //  return $this->plant_obj;
  //}

  // land_obj
  public function get_land_obj() {
    if (! isset($this->land_obj)) {
      include_once("lands.php");
      $this->land_obj = new Lands($this->get_given_config());
    }
    return $this->land_obj;
  }

  // row_num
  //public function set_row_num($var) {
  //  $this->row_num = $var;
  //}
  //public function get_row_num() {
  //  return $this->row_num;
  //}

  // col_num
  //public function set_col_num($var) {
  //  $this->col_num = $var;
  //}
  //public function get_col_num() {
  //  return $this->col_num;
  //}

  // block_number
  //public function set_block_number($var) {
  //  $this->block_number = $var;
  //}
  //public function get_block_number() {
  //  return $this->block_number;
  //}

  // block_width
  //public function set_block_width($var) {
  //  $this->block_width = $var;
  //}
  //public function get_block_width() {
  //  return $this->block_width;
  //}

  // block_length
  //public function set_block_length($var) {
  //  $this->block_length = $var;
  //}
  //public function get_block_length() {
  //  return $this->block_length;
  //}

  // method
  // todo should this be here
  public function get_full_name() {
    // derived
    $full_name = $this->get_layout_obj()->get_name() . " bed #" . $this->get_bed_num();
    return $full_name;
  }

  // method
  private function make_soil_area() {
    $obj = new SoilAreas($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_land_id()) {
      // subset
      $this->set_type("get_by_land_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      // old
      // note: distinct below
      $sql = "SELECT soil_areas.*, lands.name FROM soil_areas, lands WHERE soil_areas.land_id = lands.id AND soil_areas.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // todo this example of get all rows sql that does not work
      $sql = "SELECT soil_areas.*, lands.name FROM soil_areas, lands WHERE soil_areas.land_id = lands.id ORDER BY soil_areas.sort DESC, soil_areas.name, soil_areas.id;";
      $sql = "SELECT soil_areas.* FROM soil_areas ORDER BY soil_areas.sort DESC, soil_areas.name, soil_areas.id;";

      // debug
      //print "debug soil_areas sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_land_id") {
      $sql = "SELECT soil_areas.*, lands.name FROM soil_areas, lands WHERE soil_areas.land_id = lands.id AND lands.id = " . $this->get_given_land_id() . " ORDER BY soil_areas.name, soil_areas.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_soil_area();
        $obj->get_land_obj()->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_status(pg_result($results, $lt, 3));
        $obj->set_description(pg_result($results, $lt, 4));
        $obj->set_id(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_land_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_soil_area();
        $obj->get_land_obj()->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_status(pg_result($results, $lt, 3));
        $obj->set_description(pg_result($results, $lt, 4));
        $obj->set_id(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->get_land_obj()->set_name(pg_result($results, $lt, 7));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "Widen View: <a href=\"seed_box?id=" . $this->get_given_seed_box() . "\">Seed Box</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";
    // only authenticated users
    //if ($this->get_user_obj()) {
    //  if ($this->get_user_obj()->uid) {
    //    $markup .= "<div class=\"subsubmenu-user\">\n";
    //    $markup .= "</div>\n";
    //  }
    //}
    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    // todo develop here

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 250px;\">\n";
    $markup .= "    land";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    description\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $soil_area) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_sort() . "\n";
      $markup .= "  </td>\n";

      // land_id
      $markup .= "  <td>\n";
      if ($soil_area->get_land_obj()->get_id()) {
          include_once("lands.php");
          $land_obj = new Lands($this->get_given_config());
          $land_id = $soil_area->get_land_obj()->get_id();
          $user_obj = $this->get_user_obj();
          $land_data = $land_obj->get_land_name_given_id($land_id, $user_obj);
          $markup .= $land_data . "\n";
      }
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_id() . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_name() . "\n";
      $markup .= "  </td>\n";

      // description
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_description() . "\n";
      //$markup .= "  </td>\n";

      // status
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_status() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_aggregate_special($width, $length) {
    $markup = "";

    // this is a sidecar called by layouts
    // this was sidecar by a function that draws a png image using GD image

    if ($this->get_list_bliss()->get_count() == 0) {
      // nothing in list, so return
      $markup .= "<p class=\"error\">No soil areas.</p>";
      return $markup;
    }

    // get list
    $list_obj = $this->get_list_bliss()->get_list();

    // place into array of arrays
    $array_of_array_obj = $this->place_into_array_of_arrays($list_obj);

    // add objects for "blanks"
    $sorted_list_obj = $this->add_objects_for_blanks($array_of_array_obj);
    
    // set up overall dimensions of HTML table
    $width_pixels = 900;
    $height_pixels = $width_pixels * ($width / $length);

    $height_max = 300;
    if ($height_pixels > $height_max) {
      $ratio = ($height_pixels - $height_max) / $height_pixels;
      $height_pixels = $height_pixels * $ratio;
      $width_pixels = $width_pixels * $ratio;
    }
    // todo build some code that checks for duplicate row,col numbers
    // todo consider that there may be rows that are not in array

    // loop through rows (which are the hash key)
    $row_keys = array_keys($sorted_list_obj);

    // sort rows
    asort($row_keys);

    // output table that looks like a layout
    $markup .= "<table class=\"plants\">\n";
    foreach ($row_keys as $row_key) {

      // output all the cells in this row
      $markup .= "<tr>\n";

      // get array of objects in the row
      $row_array = $sorted_list_obj[$row_key];

      // sort columns
      usort($row_array, array("SoilAreas", "cmp_obj"));

      foreach ($row_array as $soil_area_obj) {

        // calculate width for each cells
        //$cell_width_pixels = ($soil_area_obj->get_x_percent() / 100) * $width_pixels;
        //$cell_height_pixels = ($soil_area_obj->get_y_percent() / 100) * $height_pixels;
        // output cell
        $markup .= "  <td align=\"center\" valign=\"middle\" width=\"" . $cell_width_pixels . "\" height=\"" . $cell_height_pixels . "\">\n";
        if ($soil_area_obj->get_plant_obj()->get_id()) {
          $markup .= "    " . $soil_area_obj->get_plant_obj()->get_common_name_with_link() . "<br />\n";
        }
        $markup .= "  </td>\n";
      }
    }
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $soil_area) {

      $markup .= "<h2>Soil_Area</h2>\n";

      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    layout\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_layout_obj()->get_name_with_link() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    plant\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_plant_obj()->get_common_name_with_link() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    col num\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_col_num() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    row num\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_row_num() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    block number\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_block_number() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    block width\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_block_width() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    block length\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_block_length() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    farmer\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_farmer() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    layout\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_layout_obj()->get_name_with_link() . "\n";
      $markup .= "  [to be developed]\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      // todo fix the following so that it works
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    plant\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $soil_area->get_plant_obj()->get_common_name_with_link() . "\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    ben num\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_bed_num() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    width\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_width() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    length\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_length() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    height\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_height() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    position\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_position() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    farmer\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_farmer() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_sort() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    derived_shape\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_derived_shape() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "</table>\n";
    }

    return $markup;
  }

  // method
  public function get_derived_shape() {
    $markup = "";

    include_once("models.php");

    $anchor = 0;
    $length = $anchor + $this->get_length();
    $width = $anchor + $this->get_width();
    $height = $anchor + $this->get_height();

    // bottom square
    $point3D_obj_1 = new Point3D($anchor, $anchor, $anchor);
    $point3D_obj_2 = new Point3D($length, $anchor, $anchor);
    $point3D_obj_3 = new Point3D($anchor , $width, $anchor);
    $point3D_obj_4 = new Point3D($length, $width, $anchor);

    // upper square
    $point3D_obj_5 = new Point3D($anchor, $anchor, $height);
    $point3D_obj_6 = new Point3D($length, $anchor, $height);
    $point3D_obj_7 = new Point3D($anchor, $width, $height);
    $point3D_obj_8 = new Point3D($length, $width, $height);

    $cube = new Cube3D($point3D_obj_1, $point3D_obj_2, $point3D_obj_3, $point3D_obj_4, $point3D_obj_5, $point3D_obj_6, $point3D_obj_7, $point3D_obj_8);

    $markup .= $cube->print_data();

    return $markup;
  }

  // method
  private function place_into_array_of_arrays($list_obj) {
    // an array of arrays
    $sorted_list_obj = array();

    // start by grouping the rows
    foreach ($list_obj as $obj) {
      if (array_key_exists($obj->get_bed_num(), $sorted_list_obj)) {
        //print "debug soil_areas row " . $obj->get_bed_num() . "<br />";
        //print "debug soil_areas pid " . $obj->get_plant_obj()->get_id() . "<br />";
        // add to the array that exists
        $row_array = $sorted_list_obj[$obj->get_bed_num()];
        array_push($row_array, $obj);
        $sorted_list_obj[$obj->get_bed_num()] = $row_array;
      } else {
        //print "debug soil_areas row* " . $obj->get_bed_num() . "<br />";
        // this bed_num is not in array so create a new array
        $row_array = array();
        array_push($row_array, $obj);
        $sorted_list_obj[$obj->get_bed_num()] = $row_array;
      }
    }

    return $sorted_list_obj;
  }

  // method
  static function cmp_obj($a, $b) {
    $a_row_details = $a->get_row_details();
    $b_row_details = $b->get_row_details();
    if ($a_row_details == $b_row_details) {
        return 0;
    }
    return ($a_row_details > $b_row_details) ? +1 : -1;
  }

  // method
  public function add_objects_for_blanks($list_obj) {
    // first, add rows that are blank
    $row_keys = array_keys($list_obj);
    $max = max($row_keys);
    $row_id = 1;
    $found_max_col_count = 0;
    while ($row_id <= $max) {
      if (array_key_exists($row_id, $list_obj)) {
        // ok
        //print "debug row_id = " . $row_id . "<br />";
        $row_array = $list_obj[$row_id];
        if ($found_max_col_count < count($row_array)) {
          $found_max_col_count = count($row_array);
          //print "debug found max col count " . $found_max_col_count . "<br />";
        }
      } else {
        print "error soil areas: there is a blank row";
      }
      $row_id++;
    }
    // now go through each row and make sure it has the correct number of col
    $row_id_keys = array_keys($list_obj);
    asort($row_id_keys);
    foreach ($row_id_keys as $row_id) {
      $row_array = $list_obj[$row_id];
      //print "debug row_id \"" . $row_id . "\"<br />";
      if ($found_max_col_count == count($row_array)) {
        //print "debug ok row " . $row_id . "<br />";
        continue;
      }
      $num = 1;
      usort($row_array, array("SoilAreas", "cmp_obj"));
      while ($num <= $found_max_col_count) {
        // see if each num exists as row_details        
        $found = 0;
        foreach ($row_array as $obj) {
          if ($num == $obj->get_row_details()) {
            $found = 1;
          }
        }
        if (! $found) {
          //print "debug not found " . $row_id . " " . $num . "<br />";
          // add
          $blank_obj = new SoilAreas($this->get_given_config());
          $blank_obj->set_bed_num($row_id);
          $blank_obj->set_row_details($num);
          array_push($row_array, $blank_obj);
          $list_obj[$row_id] = $row_array;
        }
        $num++;
      }
      //usort($row_array, array("SoilAreas", "cmp_obj"));
    }
    return $list_obj;
  }

  // method
  public function get_garden_plot_image($width, $length) {
    $markup = "";

    if ($this->get_list_bliss()->get_count() == 0) {
      // nothing in list, so return
      $markup .= "<p class=\"error\">No soil areas.</p>";
      return $markup;
    }

    // get list
    $list_obj = $this->get_list_bliss()->get_list();

    // place into array of arrays
    $array_of_array_obj = $this->place_into_array_of_arrays($list_obj);

    // add objects for "blanks"
    $sorted_list_obj = $this->add_objects_for_blanks($array_of_array_obj);
    
    // set up overall dimensions of HTML table
    $width_pixels = 900;
    $height_pixels = $width_pixels * ($width / $length);

    $height_max = 300;
    if ($height_pixels > $height_max) {
      $ratio = ($height_pixels - $height_max) / $height_pixels;
      $height_pixels = $height_pixels * $ratio;
      $width_pixels = $width_pixels * $ratio;
    }
    // todo build some code that checks for duplicate row,col numbers
    // todo consider that there may be rows that are not in array

    // loop through rows (which are the hash key)
    $row_keys = array_keys($sorted_list_obj);

    // sort rows
    asort($row_keys);

    // output table that looks like a layout
    $markup .= "<table class=\"plants\">\n";
    foreach ($row_keys as $row_key) {

      // output all the cells in this row
      $markup .= "<tr>\n";

      // get array of objects in the row
      $row_array = $sorted_list_obj[$row_key];

      // sort columns
      usort($row_array, array("SoilAreas", "cmp_obj"));

      foreach ($row_array as $soil_area_obj) {

        // calculate width for each cells
        //$cell_width_pixels = ($soil_area_obj->get_x_percent() / 100) * $width_pixels;
        //$cell_height_pixels = ($soil_area_obj->get_y_percent() / 100) * $height_pixels;
        // output cell
        $markup .= "  <td align=\"center\" valign=\"middle\" width=\"" . $cell_width_pixels . "\" height=\"" . $cell_height_pixels . "\">\n";
        if ($soil_area_obj->get_plant_obj()->get_id()) {
          $markup .= "    " . $soil_area_obj->get_plant_obj()->get_common_name_with_link() . "<br />\n";
        }
        $markup .= "  </td>\n";
      }
    }
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    // create a pointer to a new true colour image
    $image = ImageCreateTrueColor($width_pixels, $height_pixels); 
 
    // switch on image antialising
    ImageAntiAlias($image, true);
 
    // sets background to white
    $white = ImageColorAllocate($image, 255, 255, 255); 
    ImageFillToBorder($image, 0, 0, $white, $white);
 
    // define black and blue colours
    $black = ImageColorAllocate($image, 0, 0, 0);
    $blue = ImageColorAllocate($image, 0, 0, 255);
 
    // define the dimensions of our rectangle
    $r_width = 150;
    $r_height = 100;
    $r_x = 100;
    $r_y = 50;
 
    imagefilledrectangle($image, $r_x, $r_y, $r_x+$r_width, $r_y+$r_height, $black);
 
    imagestring($image,5,50,160,"TEST",$black, 'courbi', 'The Courier TTF font');

    // send the new PNG image to the browser
    header("Content-type: image/jpeg");
    imagejpeg($image);
 
    // clear memory
    imagedestroy($image); 

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    soil area id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    bed num\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    farmer\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    width\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    length\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    height\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    position\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sowings\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $soil_area) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"center\">\n";
      $markup .= "    " . $soil_area->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"center\">\n";
      $markup .= "    " . $soil_area->get_bed_num() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"center\">\n";
      $markup .= "    <strong>" . $soil_area->get_name() . "</strong>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $soil_area->get_farmer() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $soil_area->get_width() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $soil_area->get_length() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $soil_area->get_height() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $soil_area->get_position() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $soil_area->get_sort() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $soil_area->get_status() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      include_once("sowings.php");
      $sowing_obj = new Sowings($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $sowing_obj->output_sidecar_given_soil_area_id($user_obj, $soil_area->get_id()) . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_sidecar($given_layout_id, $given_user_obj, $given_current_year) {
    $markup = "";

    // set
    $this->set_given_layout_id($given_layout_id);
    $this->set_user_obj($given_user_obj);

    // get data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // todo fix first attempt was a diagram out of HTML table
    //$markup .= $this->output_aggregate_special($this->get_width(), $this->get_length());

    // todo fix second attempt is a diagram using the GD image library
    //$markup .= $this->get_garden_plot_image($this->get_width(), $this->get_length());

    if (count($this->get_list_bliss()->get_list())) {
      $markup .= "<h3>Soil Areas of this Layout</h3>\n";
      // output by year
      $markup .= "<table class=\"plants\">\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    soil area id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    bed num\n";
      $markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    name\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    farmer\n";
      //$markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    length\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    width\n";
      $markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    height\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    position\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    sort\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    status\n";
      //$markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    sowings\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      //print "debug count before sort " . count($this->get_list_bliss()->get_list()) . "<br />\n";

      // sort
      $sorted = $this->sort_for_sidecar($this->get_list_bliss()->get_list());

      //print "debug count after sort" . count($sorted) . "<br />\n";


      // output rows
      $num = 0;
      foreach ($sorted as $key => $value) {
        $id = $key;
        foreach ($value as $soil_area) {

          $num++;

          $markup .= "<tr>\n";

          $markup .= "  <td class=\"header\">\n";
          $markup .= "    " . $num . "\n";
          $markup .= "  </td>\n";

          $markup .= "  <td align=\"center\">\n";
          $markup .= "    " . $soil_area->get_id_with_link() . "\n";
          $markup .= "  </td>\n";

          $markup .= "  <td align=\"center\">\n";
          $markup .= "    " . $soil_area->get_bed_num() . "\n";
          $markup .= "  </td>\n";

          //$markup .= "  <td align=\"center\">\n";
          //$markup .= "    <strong>" . $soil_area->get_name() . "</strong>\n";
          //$markup .= "  </td>\n";

        //$markup .= "  <td>\n";
        //$markup .= "    " . $soil_area->get_farmer() . "\n";
        //$markup .= "  </td>\n";

          $markup .= "  <td align=\"right\">\n";
          $markup .= "    " . $soil_area->get_length() . "\n";
          $markup .= "  </td>\n";

          $markup .= "  <td align=\"right\">\n";
          $markup .= "    " . $soil_area->get_width() . "\n";
          $markup .= "  </td>\n";

        //$markup .= "  <td align=\"right\">\n";
        //$markup .= "    " . $soil_area->get_height() . "\n";
        //$markup .= "  </td>\n";

        //$markup .= "  <td align=\"right\">\n";
        //$markup .= "    " . $soil_area->get_position() . "\n";
        //$markup .= "  </td>\n";

          //$markup .= "  <td align=\"right\">\n";
          //$markup .= "    " . $soil_area->get_sort() . "\n";
          //$markup .= "  </td>\n";

          //$markup .= "  <td align=\"right\">\n";
          //$markup .= "    " . $soil_area->get_status() . "\n";
          //$markup .= "  </td>\n";

          $markup .= "  <td>\n";
          if ($soil_area->get_name()) {
            $markup .= "    <em>" . $soil_area->get_name() . "</em><br />\n";
          }
          include_once("sowings.php");
          $sowing_obj = new Sowings($this->get_given_config());
          $user_obj = $this->get_user_obj();
          $markup .= "    " . $sowing_obj->output_sidecar_given_soil_area_id($user_obj, $soil_area->get_id(), $given_current_year) . "\n";
          $markup .= "  </td>\n";

          $markup .= "</tr>\n";
        }
      }
      $markup .= "</table>\n";
    } else {
      // list is empty, so...
      // provide something for markup, a "no" message
      $markup .= "<p>No soil_ares.</p>\n";
    }

    return $markup;
  }

  // method
  private function add_to_array($array, $obj, $key) {
    //print "debug key = " . $key . "<br />\n";
    if (array_key_exists($key, $array)) {
      // add to array that is value
      $value = $array[$key];
      array_push($value, $obj);
      $array[$key] = $value;
    } else {
      $array[$key] = array($obj);
    }
    return $array;
  }

  // method
  private function sort_for_sidecar($array_to_sort) {
    $flag = 1;
    $max = 250;
    while ($flag && $max) {
        $max--;
        $flag = 0;

        // sort by bed row
        $sorted = array();
        $last_soil_area = "";

        foreach ($array_to_sort as $soil_area) {
          $bed_num_int = (int) $soil_area->get_bed_num();

          if ($last_soil_area) {
            $last_soil_area_bed_num_int = (int) $last_soil_area->get_bed_num();
            // debug
            //print "debug soil_areas " . $last_soil_area_bed_num_int . " < " . $bed_num_int . "<br />\n";

            if ($bed_num_int < $last_soil_area_bed_num_int) {
              $flag = 1;
              // debug
              //print "debug soil_areas ***** not correct *****" . $last_soil_area_bed_num_int . " < " . $bed_num_int . "<br />\n";
              $sorted = $soil_area->add_to_array($sorted, $soil_area, $bed_num_int);
            } else {
              //print "correct <br />\n";
              $sorted = $soil_area->add_to_array($sorted, $last_soil_area, $last_soil_area_bed_num_int);
              // new last soil area
              $last_soil_area = $soil_area;
            }
          } else {
            // debug
            //print "debug soil_areas first object<br />\n";
            $last_soil_area = $soil_area;
          }
        }
        $last_soil_area_bed_num_int = (int) $last_soil_area->get_bed_num();
        $sorted = $soil_area->add_to_array($sorted, $last_soil_area, $last_soil_area_bed_num_int);
        // debug
        //print "debug $max done<br />\n";
        //print "debug <br />\n";
        // flatten array
        $flat = array();
        foreach ($sorted as $key => $value) {
          $id = $key;
          foreach ($value as $soil_area) {
            array_push($flat, $soil_area);
          }
        }
        $array_to_sort = $flat;
    }
    if (! $max) {
        print "debug soil_areas sort not completed!<br />\n";
    }
    return $sorted;
  }

  // todo add code that checks that all plants on plant_list are on layout

  // method
  public function get_list_given_land_id($given_land_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_land_id($given_land_id);
    $this->set_user_obj($given_user_obj);

    // get data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if (count($this->get_list_bliss()->get_list())) {
      $markup .= "<h3>Soil Areas</h3>\n";
      $markup .= "<table class=\"plants\">\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    status (as of date)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    planned harvests (line per week)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    est. hours to end\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // output rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $soil_area) {
        $num++;

        $markup .= "<tr>\n";

        $markup .= "  <td class=\"header\">\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td align=\"center\">\n";
        $markup .= "    " . $soil_area->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td align=\"left\">\n";
        $markup .= "    " . $soil_area->get_name_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
    } else {
      // list is empty, so...
      // provide something for markup, a "no" message
      $markup .= "<p>No soil_areas.</p>\n";
    }

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

}
