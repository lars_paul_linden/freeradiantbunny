<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 4-Feb-2012
// version 1.2 2015-01-18

// about
// http://freeradiantbunny.org/main/en/docs/frb/soil_tests.php

include_once("lib/scrubber.php");

class SoilTests extends Scrubber {

  // given
  private $given_project_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $id;
  private $name;
  //private $date;
  //private $supplier_obj;
  //private $area_represented;
  //private $collected_by;
  //private $results_filename;
  private $land_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // date
  //public function set_date($var) {
  //  $this->date = $var;
  //}
  //public function get_date() {
  //  return $this->date;
  //}

  // supplier_obj
  //public function get_supplier_obj() {
  //  if (! isset($this->supplier_obj)) {
  //    include_once("suppliers.php");
  //    $this->supplier_obj = new Suppliers();
  //  }
  //  return $this->supplier_obj;
  //}

  // area_represented
  //public function set_area_represented($var) {
  //  $this->area_represented = $var;
  //}
  //public function get_area_represented() {
  //  return $this->area_represented;
  //}

  // collected_by
  //public function set_collected_by($var) {
  //  $this->collected_by = $var;
  //}
  //public function get_collected_by() {
  //  return $this->collected_by;
  //}

  // results_filename
  //public function set_results_filename($var) {
  //  $this->results_filename = $var;
  //}
  //public function get_results_filename() {
  //  return $this->results_filename;
  //}

  // land_obj
  //public function get_land_obj() {
  //  if (! isset($this->land_obj)) {
  //    include_once("lands.php");
  //    $this->land_obj = new Lands();
  //  }
  //  return $this->land_obj;
  //}

  // method
  private function make_soil_test() {
    $obj = new SoilTests($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT soil_tests.* FROM soil_tests, lands, project_lands, projects WHERE soil_tests.land_id = lands.id AND lands.id = project_lands.land_id AND project_lands.project_id = project_id AND projects.user_name = '" . $this->get_user_obj()->name . "' AND soil_tests.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT soil_tests.* FROM soil_tests, lands, project_lands, projects WHERE soil_tests.land_id = lands.id AND lands.id = project_lands.land_id AND project_lands.project_id = project_id AND projects.user_name = '" . $this->get_user_obj()->name . "' AND projects.id = " . $this->get_land_obj()->get_project_land_obj()->get_project_obj()->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT DISTINCT soil_tests.* FROM soil_tests, lands, project_lands, projects WHERE soil_tests.land_id = lands.id AND lands.id = project_lands.land_id AND project_lands.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY soil_tests.name, soil_tests.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_id") { 
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_soil_test();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_date(pg_result($results, $lt, 2));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_area_represented(pg_result($results, $lt, 4));
        $obj->set_collected_by(pg_result($results, $lt, 5));
        $obj->set_results_filename(pg_result($results, $lt, 6));
        $obj->get_land_obj()->set_id(pg_result($results, $lt, 7));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_project_id()) {
      $markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sample name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    area_represeneted\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    collected_by\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    results_filename\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    land_id\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $soil_test) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "<!-- soil_tests.id = " . $soil_test->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $soil_test->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $soil_test->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $soil_test->get_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $soil_test->get_supplier_obj()->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $soil_test->get_area_represented() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $soil_test->get_collected_by() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $soil_test->get_results_filename() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $soil_test->get_land_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function get_project_id_given_soil_test_id($given_soil_test_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_soil_test_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_land_obj()->get_project_land_obj()->get_project_obj()->get_id();
      //print "debug plant list projects = " . $project_id . "<br />\n"; 
      $markup .= $project_id;
    }

    return $markup;
  }
}
