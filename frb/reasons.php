<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 29-Jan-2011 refactored
// version 29-Jan-2011 refactored
// version 19-Feb-2011 refactored
// version 16-Apr-2012 merged into Free Radiant Bunny
// version 1-May-2012 add to Free Radiant Bunny
// version 1.2 2016-03-22
// version 1.6 2017-01-28

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/reasons.php

// this class models a reason in which a reason pertains to the justification for a website being qualified

include_once("lib/standard.php");

class Reasons extends Standard {

  // method
  public function get_project_id() {
    return;
  }

  // given_hyperlink_id
  private $given_hyperlink_id;

  // given_hyperlink_id
  public function set_given_hyperlink_id($var) {
    $this->given_hyperlink_id = $var;
  }
  public function get_given_hyperlink_id() {
    return $this->given_hyperlink_id;
  }

  // todo move this (this is really lost)
  private $valid_names = array(); // holds all valid names from database

  // derived
  private $count;       // derived

  // count
  public function set_count($var) {
    $this->count = $var;
  }
  public function get_count() {
    return $this->count;
  }

  // special
  //public function get_name() {
  //  if (0) {
  //    if (! isset($this->name)) {
  //      if (isset($_GET['qualifier'])) {
  //        // aka "qualifier reason" (see about.php)
  //        $var = $this->sanitize_user_input($_GET['qualifier']);
  //        // check that qualifier is official qualifier
  //        // all but one of these strings are from "reasons" database table
  //        // "not_yet_qualified" (the exception) allows...
  //        // ... for the user to list what hyperlinks are not yet qualified
  //        if ($this->is_valid_reason_name($var)) {
  //          $this->name = $var;
  //        } else {
  //          $message = "Sorry, but that <em>reason name</em> is not known.";
  //          $this->get_db_dash()->print_error($message);
  //        }
  //      }
  //    }
  // }
  //}

  // method
  public function get_name_user_ready() {
    $search = "_";
    $replace = " ";
    return str_replace($search, $replace, $this->get_name());
  }

  // method
  public function is_valid_reason_name($var) {

    // database //
    $type = "get_names";
    $this->get_db_dash()->load($this, $type);

    // check
    if (in_array($var, $this->valid_names)) {
      // valid
      return 1;
    }
    // not valid
    return 0;
  }

  // method
  private function make_reason() {
    $obj = new Reasons($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {
    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // todo look over this
      $sql = "SELECT reasons.*, '' FROM reasons WHERE reasons.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT reasons.* FROM reasons ORDER BY reasons.sort;";

    } else if ($this->get_type() == "get_by_hyperlink_id") {
      $sql = "SELECT reasons.*, '' FROM reasons, hyperlink_reasons WHERE reasons.id = hyperlink_reasons.reason_id AND hyperlink_reasons.hyperlink_id = " . $this->get_given_hyperlink_id() . " ORDER BY reasons.sort;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_psites";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  public function transfer($results) {
    $markup = "";

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_hyperlink_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $reason = $this->make_reason();
        $reason->set_id(pg_result($results, $lt, 0));
        $reason->set_name(pg_result($results, $lt, 1));
        $reason->set_description(pg_result($results, $lt, 2));
        $reason->set_sort(pg_result($results, $lt, 3));
        $reason->set_status(pg_result($results, $lt, 4));
        $reason->set_img_url(pg_result($results, $lt, 5));
      }
    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

    return $markup;
  }

  // method
  public function get_domain_name() {
    foreach ($this->reasons_array as $reason) {
      if ($reason->get_name() == "domain_name") {
        return "domain name";
      }
      return "";
    }
  }

  // method
  public function get_definition() {
    foreach ($this->reasons_array as $reason) {
      if ($reason->get_name() == "definition") {
        return "definition";
      }
      return "";
    }
  }

  // method
  public function get_book() {
    foreach ($this->reasons_array as $reason) {
      if ($reason->get_name() == "book") {
        return "book";
      }
      return "";
    }
  }

  // method
  public function get_word() {
    foreach ($this->reasons_array as $reason) {
      if ($reason->get_name() == "word") {
        return "word";
      }
      return "";
    }
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    description\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    hyperlinks count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $reason) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $reason->get_id_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "65";
      $markup .= "    " . $reason->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $reason->get_name_with_link() . "\n";
      $markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    " . $reason->get_description() . "\n";
      //$markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $reason->get_sort() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $reason->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td align=\"right\">\n";
      include_once("hyperlink_reasons.php");
      $hyperlink_reason_obj = new HyperlinkReasons($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $count_data = $hyperlink_reason_obj->get_hyperlink_count_given_reason_id($reason->get_id(), $user_obj);
      $url = $this->url("hyperlinks/reasons/" . $reason->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $count_data . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    // extra at bottom
    $markup .= "<br />\n";
    $markup .= $this->output_hyperlinks_with_no_reasons();

    return $markup;
  }

  // method
  public function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    foreach ($this->get_list_bliss()->get_list() as $reason) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $reason->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $reason->get_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // icon
      $markup .= "</tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "65";
      $markup .= "    " . $reason->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // description
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $reason->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // sort
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $reason->get_sort() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // status
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $reason->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_reasons_given_id_as_list($given_user_obj, $given_hyperlink_id) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);
    $this->set_given_hyperlink_id($given_hyperlink_id);

    $this->set_type("get_by_hyperlink_id");
    $this->prepare_query();

    $found_array = array();
    foreach ($this->get_list_bliss()->get_list() as $reason) {
      $markup .= $reason->get_img_url_as_image_element_as_link();
    }

    return $markup;
  }

  // method
  public function output_hyperlinks_with_no_reasons() {
    $markup = "";

    // get all hyperlinks
    $hyperlink_id_all_array = array();
    // prepare
    include_once("hyperlinks.php");
    $hyperlink_obj = new Hyperlinks($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $hyperlinks_all_array = $hyperlink_obj->get_list_of_all_hyperlinks($user_obj);
    $count_hyperlinks_all = count($hyperlinks_all_array);
    $markup .= "<p>count of all hyperlinks = $count_hyperlinks_all</p>\n";

    // get hyperlinks with reasons
    $hyperlinks_with_reasons_array = array();
    include_once("hyperlink_reasons.php");
    $hyperlink_reason_obj = new HyperlinkReasons($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $hyperlinks_with_reasons_array = $hyperlink_reason_obj->get_list_of_hyperlinks($user_obj);
    $count_hyperlinks_with_reasons = count($hyperlinks_with_reasons_array);
    $markup .= "<p>count of hyperlinks with reasons = $count_hyperlinks_with_reasons</p>\n";

    // difference
    $count_with_no_reason = $count_hyperlinks_all - $count_hyperlinks_with_reasons;
    $markup .= "<p>count of hyperlinks with no reasons yet = $count_with_no_reason</p>\n"; 

    // list hyperlinks with no reason
    $markup .= "hyperlinks with no reason yet: ";
    foreach ($hyperlinks_all_array as $hyperlink_all_obj) {
      $flag = 0;
      foreach ($hyperlinks_with_reasons_array as $hyperlink_with_reason_obj) {
        if ($hyperlink_all_obj->get_id() == $hyperlink_with_reason_obj->get_id()) {
          // skip
          $flag = 1;
        }
      }
      if (! $flag) {
        $url = $this->url("hyperlinks/" . $hyperlink_all_obj->get_id());
        $markup .= "<a href=\"" . $url . "\">" . $hyperlink_all_obj->get_id() . "</a> ";
      }
    }

    return $markup;
  }

}
