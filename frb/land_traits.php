<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-11

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/land_traits.php

include_once("lib/standard.php");

class LandTraits extends Standard {

  // given
  private $given_land_id;

  // given_land_id
  public function set_given_land_id($var) {
    $this->given_land_id = $var;
  }
  public function get_given_land_id() {
    return $this->given_land_id;
  }

  // attributes
  private $value;
  private $land_obj;
  private $reference;

  // value
  public function set_value($var) {
    $this->value = $var;
  }
  public function get_value() {
    return $this->value;
  }

  // land_obj
  public function get_land_obj() {
    if (! isset($this->land_obj)) {
      include_once("lands.php");
      $this->land_obj = new Lands($this->get_given_config());
    }
    return $this->land_obj;
  }

  // reference
  public function set_reference($var) {
    $this->reference = $var;
  }
  public function get_reference() {
    return $this->reference;
  }

  // method
  public function get_img_url_default() {
    // todo move to config so that the user can set there
    return "http://mudia.com/dash/_images/land_traits_stamp.png";
  }

  // method
  private function make_land_trait() {
    $obj = new LandTraits($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_land_id()) {
      $this->set_type("get_by_land_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      // note: distinct
      $sql = "SELECT land_traits.* FROM land_traits WHERE land_traits.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug lands <em>get_by_id</em> sql " . $sql . "<br />";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT land_traits.* FROM land_traits ORDER BY land_traits.sort DESC, land_traits.name;";

    } else if ($this->get_type() == "get_by_land_id") {
      $sql = "SELECT land_traits.* FROM land_traits, lands WHERE land_traits.land_id = lands.id AND lands.id = " . $this->get_given_land_id() . " ORDER BY land_traits.sort DESC, land_traits.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_land_id" ||
        $this->get_type() == "get_all" ) {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_land_trait();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_value(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->get_land_obj()->set_id(pg_result($results, $lt, 5));

      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // todo fix this because sometimes it does not know project_id
    //$markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    value\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    land_id\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $land_trait) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $land_trait->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $land_trait->get_sort() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $land_trait->get_name() . "\n";
      $markup .= "  </td>\n";

      // value
      $markup .= "  <td>\n";
      $markup .= "    " . $land_trait->get_value() . "\n";
      $markup .= "  </td>\n";

      // status
      $markup .= "  <td>\n";
      $markup .= "    " . $land_trait->get_status() . "\n";
      $markup .= "  </td>\n";

      // land_id
      $markup .= "  <td>\n";
      $markup .= "    " . $land_trait->get_land_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function get_table_given_land_id($given_land_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_land_id($given_land_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_aggregate();
    } else {
      $markup .= "<p>No <em>land_traits</em> found.</p>";
    }

    return $markup;
  }

}
