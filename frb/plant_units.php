<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.1 2014-08-18
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_units.php

include_once("lib/scrubber.php");

class PlantUnits extends Scrubber {

  // given
  private $given_plant_id;

  // given_plant_id
  public function set_given_plant_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "plant_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_id = $var;
    }
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // attributes
  private $id;
  private $plant_obj;
  private $unit_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // unit_obj
  public function get_unit_obj() {
    if (! isset($this->unit_obj)) {
      include_once("units.php");
      $this->unit_obj = new Units($this->get_given_config());
    }
    return $this->unit_obj;
  }

  // method
  private function make_plant_unit() {
    $obj = new PlantUnits($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_id()) {
      // single
      $this->set_type("get_by_plant_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT plant_units.id, plant_units.plant_id, plant_units.unit_id, units.name FROM plant_units, units WHERE plant_units.unit_id = units.id AND plant_units.plant_id = " . $this->get_given_plant_id() . ";";
      //print "debug sql " . $sql;

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_units.id, plant_units.plant_id, plant_units.unit_id, units.name, plants.common_name FROM plant_units, units, plants WHERE plants.id = plant_units.plant_id AND plant_units.unit_id = units.id ORDER BY plants.common_name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $this->get_unit_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_unit();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_unit_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_unit_obj()->set_name(pg_result($results, $lt, 3));
        $obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "get_by_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_unit();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_unit_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_unit_obj()->set_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    if ($this->get_given_id()) {
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("varieties");
      $markup .= "  Widen view: <a href=\"" . $url . "\">All varities</a><br />\n";
      $markup .= "  <br />\n";
      $url = $this->url("plants/variety/" . $this->get_plant_id_of_this_variety());
      $markup .= "  Widen view: <a href=\"" . $url . "\">Varieties of the this same plant</a><br />\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // note: all varieties are public

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_id() != "") {
      // for single
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "  <em>This is <strong>" . get_class($this) . "</strong> id = " . $this->get_given_id() . "</em>\n";
      $markup .= "</div>\n";
    }
    if ($this->get_given_plant_id() != "") {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "    <em>These " . get_class($this) . " are of the plant <strong>" . $this->get_plant_obj()->get_common_name_with_link() . "</strong>.\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function output_data_set_table() {
    $markup = "";

    if ($this->get_error_message()) {
      $markup .= $this->get_error_message();
    } else {
      if ($this->get_type() == "get_all") {
        // specialized
        $markup .= $this->output_aggregate_with_plant_name();
      } else {
        $markup .= parent::output_data_set_table();
      }
    }

    return $markup;
  }

  // method
  public function output_aggregate_with_plant_name() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    unit\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";
    foreach ($this->get_list_bliss()->get_list() as $plant_unit) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- plant_unit id=" . $plant_unit->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_unit->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_unit->get_unit_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    foreach ($this->get_list_bliss()->get_list() as $plant_unit) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- plant_unit id=" . $plant_unit->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_unit->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<h2>" . $this->get_name() . "</h2>\n";

    $markup .= "<p>" . $this->get_description() . "</p>\n";

    //$markup .= "<p>id = " . $this->get_id() . "</p>\n";
    //$markup .= "<p><em>id = " . $this->get_id() . "</em></p>\n";

    $markup .= "<p>This is a " . get_class($this) . " of <strong>" . $this->get_plant_obj()->get_common_name_with_link() . "</strong>.</p>\n";

    return $markup;
  }

  // method
  public function get_unit_name_given_plant_id($given_plant_id) {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);

    // get data
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    // return data
    if ($this->get_list_bliss()->get_count() > 0) {
      $list_obj = $this->get_list_bliss()->get_list();
      $plant_unit_obj = $list_obj[0];
      if ($plant_unit_obj) {
        //print "debug " . get_class($plant_unit_obj) . "<br />";
        $unit_obj = $plant_unit_obj->get_unit_obj();
        //print "debug " . get_class($unit_obj) . "<br />";
        //print "debug " . $unit_obj->get_name() . "<br />";
        $markup .= $unit_obj->get_name();
        //print "debug " . $markup . "<br />";
      }
    }

    return $markup;
  }
}
