<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-28
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/payments.php

include_once("lib/standard.php");

class Payments extends Standard {

  // given
  private $given_invoice_id;

  // given_invoice_id
  public function set_given_invoice_id($var) {
    $this->given_invoice_id = $var;
  }
  public function get_given_invoice_id() {
    return $this->given_invoice_id;
  }

  // attribute
  private $id;
  private $date;
  private $invoice_obj;
  private $description;
  private $amount;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // invoice_obj
  public function get_invoice_obj() {
    if (! isset($this->invoice_obj)) {
      include_once("invoices.php");
      $this->invoice_obj = new Invoices($this->get_given_config());
    }
    return $this->invoice_obj;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // amount
  public function set_amount($var) {
    $this->amount = $var;
  }
  public function get_amount() {
    return $this->amount;
  }

  // method
  private function make_payment() {
    $obj = new Payments($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_invoice_id()) {
      $this->set_type("get_by_invoice_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT payments.* FROM payments WHERE payments.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT payments.* FROM payments ORDER BY payments.date;";

    } else if ($this->get_type() == "get_by_invoice_id") {
      $sql = "SELECT payments.* FROM payments WHERE payments.invoice_id = " . $this->get_given_invoice_id() . " ORDER BY payments.date;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_invoice_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_payment();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_date(pg_result($results, $lt, 1));
        $obj->get_invoice_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_description(pg_result($results, $lt, 3));
        $obj->set_amount(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    invoice\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    amount\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $payment) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $payment->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // date
      $markup .= "  <td>\n";
      $markup .= "    " . $payment->get_date() . "\n";
      $markup .= "  </td>\n";

      // invoice
      $markup .= "  <td>\n";
      $markup .= "    " . $payment->get_invoice_obj()->get_id() . "\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $payment->get_description() . "\n";
      $markup .= "  </td>\n";

      // amount
      $markup .= "  <td>\n";
      $markup .= "    " . $payment->get_amount() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $payment) {

      // project
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    project\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $payment->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $payment->get_id_with_link() . "\n";
      $markup .= "</tr>\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $payment->get_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // invoices
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoices\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      include_once("invoices.php");
      $invoice_obj = new Invoices($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $invoice_obj->get_invoices_given_customer_id($payment->get_id(), $user_obj);
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";


    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_payments_given_invoice_id($given_invoice_id, $given_user_obj) {
    $markup = "";

    $this->set_given_invoice_id($given_invoice_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_aggregate();
    } else {
      $markup .= "No payments found.<br />\n";
    }

    return $markup;
  }

  // method
  public function get_payments_total_given_invoice_id($given_invoice_id, $given_user_obj) {
    $markup = "";

    $this->set_given_invoice_id($given_invoice_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {

      $payments_total = 0;

      foreach ($this->get_list_bliss()->get_list() as $payment) {
        // debug
        //print "debug payments payment amount = " . $payment->get_amount() . "<br />\n";
        $payments_total = $payments_total + $payment->get_amount();
      }

      $markup .= $payments_total;

    } else {
      $markup .= "0";
    }

    return $markup;
  }

}
