<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-20
// version 1.2 2015-01-04
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/databases.php

include_once("lib/socation.php");

class Databases extends Socation {

  // given
  private $given_domain_tli;
  private $given_view = "online"; // default

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attributes
  private $management_system;
  private $date_last_backup;
  private $schema_version;

  // management_system
  public function set_management_system($var) {
    $this->management_system = $var;
  }
  public function get_management_system() {
    return $this->management_system;
  }

  // date_last_backup
  public function set_date_last_backup($var) {
    $this->date_last_backup = $var;
  }
  public function get_date_last_backup() {
    return $this->date_last_backup;
  }

  // schema_version
  public function set_schema_version($var) {
    $this->schema_version = $var;
  }
  public function get_schema_version() {
    return $this->schema_version;
  }

  // method
  public function get_machines_list() {
      include_once("host_databases.php");
      $host_database_obj = new HostDatabases($this->get_given_config());
      $database_id = $this->get_id();
      $usr_obj = $this->get_user_obj();
      return $host_database_obj->get_machines_list_given_database_id($database_id, $usr_obj);
  } 

  // print machines_list
  public function print_machines_list() {
    $markup = "";

    // loop to create string
    $count = count($this->get_machines_list());
    $num = 0;
    foreach ($this->get_machines_list() as $machine) {
      $num++;
      //on hold
      //$markup .= $machine->get_name_with_link();
      // comma after all but last
      if ($num < $count) {
        $markup .= ", ";
        $markup .= "<br />\n";
      }
    }
    $markup .= "\n";

    return $markup;
  }

  // application_id_list
  public function set_applications_id_list($var) {
    $this->applications_id_list = $var;
  }
  public function get_applications_id_list() {
      include_once("host_applications.php");
      $host_application_obj = new HostApplications($this->get_given_config());
      $database_id = $this->get_id();
      $usr_obj = $this->get_user_obj();
      $this->applications_id_list = $host_application_obj->get_applications_id_list_given_database_id($database_id, $usr_obj);
      return $this->applications_id_list;
  } 
  public function get_applications_list() {
      include_once("host_applications.php");
      $host_application_obj = new HostApplications($this->get_given_config());
      $database_id = $this->get_id();
      $usr_obj = $this->get_user_obj();
      $this->applications_list = $host_application_obj->get_applications_list_given_database_id($database_id, $usr_obj);
      return $this->applications_list;
  } 


  // print applications_id_list
  public function print_applications_id_list() {
    $markup = "";

    // loop to create string
    $count = count($this->get_applications_id_list());

    $num = 0;
    //foreach ($this->get_applications_id_list() as $application_id) {
    //  $num++;
    //  $markup .= $application_id;
    //  // comma after all but last
    //  if ($num < $count) {
    //    $markup .= ", ";
    //  }
    //}
    return $markup;
  }

  // method
  public function print_applications_name_list() {
    $markup = "";

    // load from db
    $list = $this->get_applications_list();

    if ($list) {
      // loop to create string based on the list's elements
      $count = count($list);
      $num = 0;
      foreach ($list as $application) {
        $num++;
        $markup .= $application->get_name_with_link();
        // comma after all but last
        if ($num < $count) {
          $markup .= ", ";
        }
      }
    }

    return $markup;
  }

  // img_url
  //public function get_img_url() {
   // todo move this config so tha the user can modify
   // todo although this does show a good use of a public_default
   // todo so, in that case, move it to freeradiantbunnu.org/freeradiantbunny/_images/database_icon_drawn.png
   //return "http://mudia.com/dash/_images/database_icon_drawn.png";
  //}

  // method
  private function make_database() {
    $obj = new Databases($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug databases type = " . $this->get_type() . "<br />\n";
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "select databases.* from databases WHERe databases.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "select databases.* from databases ORDER BY databases.sort DESC, databases.id;";

    //} else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      // todo: cannot span 2 databases to get this data
      //$sql = "";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "select databases.* from databases, host_databases WHERE databases.id = host_databases.database_id AND host_databases.domain_tli = '" . $this->get_given_domain_tli() . "' ORDER BY databases.sort DESC, databases.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_database();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_management_system(pg_result($results, $lt, 1));
        $obj->set_name(pg_result($results, $lt, 2));
        $obj->set_date_last_backup(pg_result($results, $lt, 3));
        $obj->set_schema_version(pg_result($results, $lt, 4));
        $obj->set_description(pg_result($results, $lt, 5));
        $obj->set_sort(pg_result($results, $lt, 6));
        $obj->set_status(pg_result($results, $lt, 7));
        $obj->set_img_url(pg_result($results, $lt, 8));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  public function extra_function() {

    $markup .= "<div class=\"data_dictionary\">\n";
    $markup .= "    <h2><a href=\"data_dictionary.php\">data dictionary</a></h2>\n";
    $markup .= "</div><!-- end data_dictionary -->\n";
    $markup .= "<br />\n";
  }

  // method
  public function output_live_given_name($name) {

    if (! $name) {
      $markup .= "<p>Error: no name</p>\n";
      return;
    }

    // get data
    $table = "databases";
    $one = $name;
    $this->get_db_dash()->craft_sql_statement($table, $one, $two, $three);
    $this->get_db_dash()->execute();
    $this->transfer();
    $this->output_table();

    // live table_names
    $markup .= "<h2>live table_names</h2>\n";
    include_once("lib/metadata.php");
    $metadata_obj = new MetaData();
    $metadata_obj->output_table_names($this->get_db_dash(), $name);

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // may be a subset
    if ($this->get_given_domain_tli()) {
      $url = $this->url("domains/" . $this->get_given_domain_tli());
      $markup .= "<p><a href=\"" . $url . "\">" . $this->get_given_domain_tli() . "</a></p>\n";
    }

      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    #\n";
      $markup .= "  <td class=\"colhead\" width=\"120\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\" width=\"100\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    mgmt.sys.\n";
      $markup .= "  </td>\n";
      //$markup .= "  <td class=\"colhead\">\n";
      //$markup .= "    description\n";
      //$markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    machines\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    application\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    s.e.\n";
      $markup .= "  </td>\n";
      //$markup .= "  <td class=\"colhead\">\n";
      //$markup .= "    schema_version\n";
      //$markup .= "  </td>\n";
      if (0) {
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    date_last_backup\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    host_databases\n";
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";

      // set up a utility class
      include_once("lib/timekeeper.php");
      $timekeeper_obj = new Timekeeper();

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $database) {
        $num++;

        $markup .= "<tr>\n";

        // num
        $markup .= "  <td>\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        // very old way of sort
        //// status
        //if ($database->get_status() == "in progress") {
        //  $markup .=  "  <td style=\"background-color: #003399;\">\n";
        //} else {
        //  if ($database->get_status() == "") {
        //    // blank
        //    $markup .=  "  <td style=\"background-color: #999999;\">\n";
        //  } else {
        //    $markup .=  "  <td>\n";
        //  }
        //}
        //$markup .= "    " . $database->get_status() . "<br />\n";
        //$markup .= "  </td>\n";

        // status
        // todo problem is that the font-size is a poor fix for big string issue
        $markup .= "  <td style=\"text-align: center; background-color: " . $database->get_status_background_color() . "; font-size: 50%;\">\n";
        $markup .= "    " . $database->get_status() . "\n";
        $markup .= "  </td>\n";

        // sort
        $sort = $database->get_sort();
        $column_name = "sort";
        $class_name_for_url = "databases";
        $id = $database->get_id();
        // this class does not have a given_view (yet)
        // $given_view = $this->get_given_view();
        $given_view = "";
        $markup .= $timekeeper_obj->get_cell_colorized_given_sort_date($sort, $column_name, $class_name_for_url, $id, $given_view, "");

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $database->get_id_with_link() . "<br />\n";
        $markup .= "  </td>\n";

        // img_element
        $markup .= "  <td>\n";
        $markup .= "    " . $database->get_img_url_as_img_element() . "<br />\n";
        $markup .= "  </td>\n";

        // name
        $markup .= "  <td>\n";
        $url = $this->url("databases/" . $database->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $database->get_name() . "</a>\n";
        $markup .= "  </td>\n";
 
        // management_system
        $markup .= "  <td>\n";
        $markup .= "    <a href=\"http://mudia.com/link/index.php?" . $database->get_management_system() . "\">" . $database->get_management_system() . "</a><br />\n";
        $markup .= "  </td>\n";

        // machines_id_list
        if ($database->print_machines_list()) {
          $markup .=  "  <td>\n";
        } else {
          $markup .=  "  <td style=\"background-color: #999999;\">\n";
        }
        // on hold due to lack of database table
        //$markup .= "    " . $database->print_machines_list() . "<br />\n";
        $markup .= "[on hold]";
        $markup .= "  </td>\n";

        // applications_id_list
        if ($database->print_applications_id_list()) {
          $markup .=  "  <td>\n";
        } else {
          $markup .=  "  <td style=\"background-color: #999999;\">\n";
        }

        // todo this does not make sense and probably should be deleted
        //$markup .= "    " . $database->print_applications_id_list() . "<br />\n";
        //$markup .= "    " . $database->print_applications_name_list() . "<br />\n";

        // todo the above is disruptive
        $markup .= "[on hold]";
        $markup .= "  </td>\n";

        // scene_elements
        // note select from scene_elements obj all where given_id
        include_once("scene_elements.php");
        $scene_element_obj = new SceneElements($this->get_given_config());
        $user_obj = $this->get_user_obj();
        // on hold due to lack of database table
        //$scene_element_data = $scene_element_obj->get_scene_elements_list_given_database_id($user_obj, $database->get_id());
        $scene_element_data = "[on hold]";
        if ($scene_element_data) {
          $markup .=  "  <td>\n";
        } else {
          $markup .=  "  <td style=\"background-color: #999999;\">\n";
        }
        $markup .= "    " .  $scene_element_data . "<br />\n";
        $markup .= "  </td>\n";

        // description
        //$markup .= "  <td>\n";
        //$markup .= "    " . $database->get_description() . "<br />\n";
        //$markup .= "  </td>\n";

        // schema_version
        //$markup .= "  <td>\n";
        //$markup .= "    " . $database->get_schema_version() . "<br />\n";
        //$markup .= "  </td>\n";

        // date_last_backup
        //$date = $database->get_date_last_backup();
        //if (strstr($date, "2012") or $date == "") {
        //  $markup .= "  <td style=\"background-color: red;\">\n";
        //} else {
        //  $markup .= "  <td>\n";
        //}
        //$markup .= "    " . $date . "<br />\n";
        //$markup .= "  </td>\n";

        // h.d.
        //$markup .= "  <td>\n";
        // todo code: select from host_databases all where given_id
        //$markup .= "    [h.d.]<br />\n";
        //$markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";
      $markup .= "<br />\n";

      //$markup .= "<p class=\"error\">[No databases found.]</p>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

      $markup .= "<table class=\"plants\">\n";


      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $database) {
        $num++;

        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    #\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    id\n";
        $markup .= "  </td>\n";
     	$markup .= "  <td>\n";
        $markup .= "    " . $database->get_id_with_link() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    img_url\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "    " . $database->get_img_url_as_img_element() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    sort\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "    " . $database->get_sort() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    mgmt.sys.\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "    <a href=\"http://mudia.com/link/index.php?" . $database->get_management_system() . "\">" . $database->get_management_system() . "</a><br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    machine\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        // note only get data if id exists
        //if ($database->get_machine_id()) {
        //  include_once("machines.php");
        //  $machine_obj = new Machines($this->get_given_config());
        //  $user_obj = $this->get_user_obj();
        //  $machine_data = $machine_obj->get_name_and_link_given_id($user_obj, $database->get_machine_id());
        //  $markup .= "    " .  $machine_data . "<br />\n";
        //}
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    description\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "    " . $database->get_description() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    status\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "    " . $database->get_status() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    name\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $url = $this->url("databases/" . $database->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $database->get_name() . "</a>\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
        $markup .= "<tr>\n";

        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    app\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "    " . $database->get_schema_version() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

        if (0) {
          $markup .= "<tr>\n";

          $markup .= "  <td class=\"colhead\">\n";
          $markup .= "    data_last_backup\n";
          $markup .= "  </td>\n";
          $date = $database->get_date_last_backup();
          if (strstr($date, "2012") or $date == "") {
            $markup .= "  <td style=\"background-color: #336699;\">\n";
          } else {
            $markup .= "  <td>\n";
          }
          $markup .= "    " . $date . "<br />\n";
          $markup .= "  </td>\n";
          $markup .= "</tr>\n";
        }
      }
      $markup .= "</table>\n";

    return $markup;
  }

  // $markup .= "<p>This is a database dictionary, class library, and dcoumentation to the mudia object repository (MOR).</p>\n";
  
  // $markup .= "<h2 style=\"padding-left: 10px;\">Databases Installed</h2>\n";

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // special
  public function get_status_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_status();
    }

    return $markup;
  }

  // method
  public function get_databases_given_domain_tli($given_domain_tli, $given_user_obj, $given_output_flag = "") {
    $markup = "";

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      if ($given_output_flag == "list") {
        $markup .= "\n";
        $markup .= "<table class=\"plants\">\n";
        foreach ($this->get_list_bliss()->get_list() as $database) {
          $markup .= "<tr>\n";
          if ($database->get_sort()) {
            $markup .= "  <td style=\"background-color: #A6D785;\">\n";
          } else {
            $markup .= "  <td style=\"background-color: #ECC3BF;\">\n";
          }
          $url = $this->url("databases/" . $database->get_id());
          $markup .= "      <a href=\"" . $url . "\">" . $database->get_name() . "</a>\n";
          $markup .= "    </td>\n";
          $markup .= "</tr>\n";
        }
        $markup .= "</table>\n";
        $markup .= "\n";
      } else {
        $markup .= $this->output_aggregate();
      }
    } else {
      $markup .= "<p>[No databases found.]</p>\n";
    }

    return $markup;
  }

  // method
  public function get_name_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $database = $this->get_list_bliss()->get_first_element();
      $markup .= $database->get_name();
    } else {
      $markup .= "<p>[No databases found.]</p>\n";
    }

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug databases: ignore, not related<br />\n";
    } else {
      $markup .= "debug databases: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "databases";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $databases_obj = new Databases($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $databases_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug processes target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
        }
      }
    }
    return $markup;
  }

    // todo best to clean up all of this stuff below (remember function idea)
    // define database based upon context (domain name, aka host)
    //print "debug host: " . $_SERVER['HTTP_HOST'] . "\n";
    //if (strstr($_SERVER['HTTP_HOST'], "permaculturewebsites.org")) {

}
