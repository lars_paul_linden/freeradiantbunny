<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-28
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/customers.php

include_once("lib/standard.php");

class Customers extends Standard {

  // given
  private $given_project_id;
  private $given_invoice_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_invoice_id
  public function set_given_invoice_id($var) {
    $this->given_invoice_id = $var;
  }
  public function get_given_invoice_id() {
    return $this->given_invoice_id;
  }

  // attribute
  private $id;
  private $name;
  private $project_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      include_once("projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

  // method
  private function make_customer() {
    $obj = new Customers($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT customers.*, projects.name, projects.img_url FROM customers, projects WHERE customers.project_id = projects.id AND customers.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT customers.*, projects.name, projects.img_url FROM customers, projects WHERE customers.project_id = projects.id ORDER BY customers.id;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT customers.*, projects.name, projects.img_url FROM customers, projects WHERE customers.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY customers.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_customer();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_project_obj()->set_name(pg_result($results, $lt, 3));
        $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("invoices");
    $markup .= "    <a href=\"" . $url . "\">invoices</a>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $customer) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // projects
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $customer->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $customer->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "    " . $customer->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // invoices
      $markup .= "  <td>\n";
      include_once("invoices.php");
      $invoice_obj = new Invoices($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $invoice_obj->get_invoices_given_customer_id($customer->get_id(), $user_obj);
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $customer) {

      // project
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    project\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $customer->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $customer->get_id_with_link() . "\n";
      $markup .= "</tr>\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $customer->get_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // invoices
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoices\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      include_once("invoices.php");
      $invoice_obj = new Invoices($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $invoice_obj->get_invoices_given_customer_id($customer->get_id(), $user_obj);
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";


    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_customer_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      foreach ($this->get_list_bliss()->get_list() as $customer) {

        $markup .= "<tr>\n";

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $customer->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // name
        $markup .= "  <td>\n";
        $markup .= "    " . $customer->get_name() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";

    } else {
      $markup .= "No customers found.<br />\n";
    }

    return $markup;
  }

}
