<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.1 2014-08-20
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/host_databases.php

include_once("lib/sproject.php");

class HostDatabases extends Sproject {

  // given
  private $given_database_id;
  private $given_machine_id;
  private $given_domain_tli;

  // given_database_id
  public function set_given_database_id($var) {
    $this->given_database_id = $var;
  }
  public function get_given_database_id() {
    return $this->given_database_id;
  }

  // given_machine_id
  public function set_given_machine_id($var) {
    $this->given_machine_id = $var;
  }
  public function get_given_machine_id() {
    return $this->given_machine_id;
  }

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // attribute
  private $domain_obj;
  private $machine_obj;
  private $database_obj;

  // domain_obj
  public function get_domain_obj() {
    if (! isset($this->domain_obj)) {
      include_once("domains.php");
      $this->domain_obj = new Domains($this->get_given_config());
    }
    return $this->domain_obj;
  }

  // machine_obj
  public function get_machine_obj() {
    if (! isset($this->machine_obj)) {
      include_once("machines.php");
      $this->machine_obj = new Machines($this->get_given_config());
    }
    return $this->machine_obj;
  }

  // database_obj
  public function get_database_obj() {
    if (! isset($this->database_obj)) {
      include_once("databases.php");
      $this->database_obj = new Databases($this->get_given_config());
    }
    return $this->database_obj;
  }

  // method
  private function make_host_database() {
    $obj = new HostDatabases($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");
      
    } else if ($this->get_given_database_id()) {
      $this->set_type("get_by_database_id");
      
    } else if ($this->get_given_machine_id()) {
      $this->set_type("get_by_machine_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");
      
    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND goal_statements.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";
      
    } else if ($this->get_type() == "get_all") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else if ($this->get_type() == "get_by_database_id") {
      $sql = "SELECT * FROM host_databases WHERE database_id = " . $this->get_given_database_id() . " ORDER BY domain_tli, machine_id, database_id;";
      
    } else if ($this->get_type() == "get_by_machine_id") {
      $sql = "SELECT * FROM host_databases WHERE machine_id = " . $this->get_given_machine_id() . " ORDER BY domain_tli, machine_id, database_id;";
      
    } else if ($this->get_type() == "get_by_domain_tli") {
      $sql = "SELECT * FROM host_databases WHERE domain_tli = '" . $this->get_given_domain_tli() . "' ORDER BY domain_tli, machine_id, database_id;";
      
    } else if ($this->get_type() == "get_by_project_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_database_id" ||
        $this->get_type() == "get_by_machine_id" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_host_database();
        $obj->get_domain_obj()->set_tli(pg_result($results, $lt, 0));
        $obj->get_machine_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_database_obj()->set_id(pg_result($results, $lt, 2));

      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

  // method
  public function get_sidecar_given_database_id($given_database_id, $given_user_obj) {
    $markup = "";

    $this->set_given_database_id($given_database_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug host_databases given_database_id = " . $this->get_given_database_id() . "<br />\n";

    $this->determine_type();

    $markup .= $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $markup .= "<table class=\"scene-elements-list\">\n";
      // heading
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    #<br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    domain_tli<br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    machine_id<br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    database_id<br />\n";
      $markup .= "  </td>\n";
      $markup .= "<tr>\n";
      // rows
      foreach ($this->get_list_bliss()->get_list() as $host_database) {
        $num++;

        $markup .= "<tr>\n";

        // num
        $markup .= "  <td>\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        // domain_tli
        $markup .= "  <td>\n";
        $markup .= "    " . $host_database->get_domain_obj()->get_tli() . "<br />\n";
        $markup .= "  </td>\n";

        // machine_id
        $markup .= "  <td>\n";
        $markup .= "    " . $host_database->get_machine_obj()->get_id() . "<br />\n";
        $markup .= "  </td>\n";

        // database_id
        $markup .= "  <td>\n";
        $markup .= "    " . $host_database->get_database_obj()->get_id() . "<br />\n";
        $markup .= "  </td>\n";

      }
      $markup .= "</table>\n";
    } else {
      $markup .= "<p>No host_databases found.</p>\n";
    }

    return $markup;
  }

  // method
  public function get_machines_list_given_database_id($given_database_id, $given_user_obj) {

    // declare container and what to return
    $machines_list = array();

    // code here
    $this->set_given_database_id($given_database_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug host_databases given_database_id = " . $this->get_given_database_id() . "<br />\n";

    $this->determine_type();

    $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
        foreach ($this->get_list_bliss()->get_list() as $host_database) {
          // machine_id
          $machine_id = $host_database->get_machine_obj()->get_id();
          // machine_obj
          include_once("machines.php");
          $machine_obj = new Machines($this->get_given_config());
          $user_obj = $this->get_user_obj();
          // process it but do not anything with the data
          $found_obj = $machine_obj->get_obj_given_id($machine_id, $user_obj);
          array_push($machines_list, $found_obj);
        }
    }

    return $machines_list;
  }
 
  // method
  public function get_databases_given_machine_id($given_machine_id, $given_user_obj) {
    $markup = "";

    // code here
    $this->set_given_machine_id($given_machine_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug host_databases given_machine_id = " . $this->get_given_machine_id() . "<br />\n";

    $this->determine_type();

    $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
        foreach ($this->get_list_bliss()->get_list() as $host_database) {
          $markup .= "<p>";
          $markup .= $host_database->get_database_obj()->get_id() . "\n";
          $markup .= $host_database->get_database_obj()->get_name() . "\n";
          $markup .= "</p>\n";
        }
    }

    return $markup;
  }

  // method
  public function get_databases_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // code here
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
        foreach ($this->get_list_bliss()->get_list() as $host_database) {
          $markup .= $host_database->get_database_obj()->get_id() . " ";
          $markup .= $host_database->get_database_obj()->get_name() . "<br />\n";
        }
    }

    return $markup;
  }
 
  // method
  public function get_databases_count_given_domain_tli($given_domain_tli, $given_user_obj, $given_status_filter_string = "") {
    $markup = "";

    // code here
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
        $num = 0;
        foreach ($this->get_list_bliss()->get_list() as $host_database) {
          include_once("databases.php");
          $database_obj = new Databases($this->get_given_config());
          $user_obj = $this->get_user_obj();
          $status = $database_obj->get_status_given_id($host_database->get_database_obj()->get_id(), $user_obj);
          if ($status == $given_status_filter_string) {
             $num++;
          }    
        }
        return $num;
    }

    return $markup;
  }
 
  // method
  protected function output_preface() {
    $markup = "";

    $markup = "<br />\n";

    return $markup;
  }

}
