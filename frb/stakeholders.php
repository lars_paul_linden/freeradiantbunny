<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/stakeholders.php

include_once("lib/standard.php");

class Stakeholders extends Standard {

  // given
  private $given_id;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // method
  private function make_stakeholder() {
    $obj = new Stakeholders($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_class_name_string()) {
      $this->set_type("get_by_class_name_string");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT accounts.* FROM accounts WHERE accounts.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug accounts sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT accounts.* FROM accounts ORDER BY accounts.name;";

    } else if ($this->get_type() == "get_by_class_name_string") {
      $sql = "SELECT accounts.* FROM accounts WHERE accounts.class_name_string = '" . $this->get_given_class_name_string() . "';";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_class_name_string") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $account = $this->make_stakeholder();
        $account->set_id(pg_result($results, $lt, 0));
        $account->set_name(pg_result($results, $lt, 1));
        $account->set_description(pg_result($results, $lt, 2));
        $account->set_sort(pg_result($results, $lt, 3));
        $account->set_status(pg_result($results, $lt, 4));
        $account->set_img_url(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $stakeholder) {
      $markup .= "<tr>\n";

      // sort
      $column_name = "sort";
      include_once("lib/timekeeper.php");
      $timekeeper_obj = new Timekeeper();
      $color = $timekeeper_obj->calculate_cell_color($column_name, $stakeholder->get_sort());
      $markup .= "  <td valign=\"top\" style=\"background: $color;\">\n";
      $markup .= "    " . $stakeholder->get_sort() . "<br />\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $stakeholder->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "    " . $stakeholder->get_name() . "\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $stakeholder->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    // todo add something interesting here

    return $markup;
  }

}
