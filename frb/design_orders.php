<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/design_orders.php

include_once("lib/scrubber.php");

class DesignOrders extends Scrubber {

  // attribute
  private $id;
  private $date;
  private $note;
  private $supplier_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // method
  public function get_id_with_link() {
    $url = url("design_orders/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_id() . "</a>";
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // note
  public function set_note($var) {
    $this->note = $var;
  }
  public function get_note() {
    return $this->note;
  }

  // supplier_obj
  public function get_supplier_obj() {
    if (! isset($this->supplier_obj)) {
      include_once("suppliers.php");
      $this->supplier_obj = new Suppliers($this->get_given_config());
    }
    return $this->supplier_obj;
  }

  // method
  private function make_design_order() {
    $obj = new DesignOrders($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT design_orders.id, design_orders.supplier_id, design_orders.date, design_orders.note, suppliers.name FROM design_orders, suppliers WHERE design_orders.id = " . $this->get_given_id() . " AND design_orders.supplier_id = suppliers.id;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT design_orders.id, design_orders.supplier_id, design_orders.date, design_orders.note, suppliers.name FROM design_orders, suppliers WHERE design_orders.supplier_id = suppliers.id ORDER BY design_orders.date, suppliers.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_design_order();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_date(pg_result($results, $lt, 2));
        $obj->set_note(pg_result($results, $lt, 3));
        $obj->get_supplier_obj()->set_name(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_design_order();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_date(pg_result($results, $lt, 2));
        $obj->set_note(pg_result($results, $lt, 3));
        $obj->get_supplier_obj()->set_name(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // heading
    $markup .= "<table class=\"plants\">\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    note\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    design order items\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $design_order) {
      $num++;

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "  " . $num;
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $design_order->get_id();
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $design_order->get_date();
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $design_order->get_supplier_obj()->get_name_with_link();
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $design_order->get_note();
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = url("design_orders/" . $design_order->get_id());
      $markup .= "    <a href=\"" . $url . "\">design order items</a>\n";;
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "  </table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // heading
    $markup .= "<table class=\"plants\">\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    note\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "  " . $this->get_id();
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "  " . $this->get_supplier_obj()->get_name_with_link();
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "  " . $this->get_date();
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "  " . $this->get_note();
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "  </table>\n";

    include_once("design_order_items.php");
    $design_order_item_obj = new DesignOrderItems();
    $markup .= $design_order_item_obj->output_sidecar($this->get_id());

    return $markup;
  }

}
