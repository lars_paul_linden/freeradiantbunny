<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-07
// version 1.2 2015-01-11

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/domain_measurements.php

include_once("lib/scrubber.php");

class DomainMeasurements extends Scrubber {

  // todo fix the following oddness
  public function get_project_id() {
    return "todo FIX";
  }

  // given
  private $given_domain_tli;
  private $given_domain_field_string;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_domain_field_string
  public function set_given_domain_field_string($var) {
    $this->given_domain_field_string = $var;
  }
  public function get_given_domain_field_string() {
    return $this->given_domain_field_string;
  }

  // atttributes
  private $id;
  private $domain_tli;
  private $domain_field_string;
  private $webmaster_responsibility;
  private $sort;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }
  public function get_id_with_link() {
    $markup = "";

    // todo it is suprising that scrubber does not have this method
    $url = $this->url("domain_measurements/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    $markup .= $this->get_id();
    $markup .= "</a>";

    return $markup;
  }


  // domain_tli
  public function set_domain_tli($var) {
    $this->domain_tli = $var;
  }
  public function get_domain_tli() {
    return $this->domain_tli;
  }

  // domain_field_string
  public function set_domain_field_string($var) {
    $this->domain_field_string = $var;
  }
  public function get_domain_field_string() {
    return $this->domain_field_string;
  }

  // webmaster_responsibility
  public function set_webmaster_responsibility($var) {
    $this->webmaster_responsibility = $var;
  }
  public function get_webmaster_responsibility() {
    return $this->webmaster_responsibility;
  }

  // sort
  public function set_sort($var) {
    $this->sort = $var;
  }
  public function get_sort() {
    return $this->sort;
  }

  // img_url
  public function get_img_url() {
    return "http://mudia.com/dash/_images/not_defined.png";
  }

  // method
  private function make_domain_measurement() {
    $obj = new DomainMeasurements($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
    }

    // reset
    $this->get_list_bliss()->empty_list();

    // debug
    //print "debug profiles: determine_type() type = " . $this->get_type() . "<br />\n";
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT domain_measurements.* FROM domain_measurements WHERE domain_measurements.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      // todo move this to the view code object
      // todo for now however just allow for subset of all
      $sql = "SELECT domain_measurements.* FROM domain_measurements ORDER BY domain_measurements.sort DESC, domain_measurements.domain_field_string;";

    } else if ($this->get_type() == "get_by_domain_tli") {
      if ($this->get_given_domain_field_string()) {
        $sql = "SELECT domain_measurements.*, domains.name FROM domain_measurements, domains WHERE domain_measurements.domain_tli = domains.tli AND domain_measurements.domain_tli = '" . $this->get_given_domain_tli() . "' AND domain_measurements.domain_field_string = '" . $this->get_given_domain_field_string() . "';";
      } else {
        $sql = "SELECT domain_measurements.*, domains.name FROM domain_measurements, domains WHERE domain_measurements.domain_tli = domains.tli AND domain_measurements.domain_tli = '" . $this->get_given_domain_tli() . "' ORDER BY domain_measurements.domain_field_string;";
      }

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
    	$this->get_type() == "get_by_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_domain_measurement();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_domain_tli(pg_result($results, $lt, 1));
        $obj->set_domain_field_string(pg_result($results, $lt, 2));
        $obj->set_webmaster_responsibility(pg_result($results, $lt, 3));
        $obj->set_sort(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 80px;\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 80px;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
    $markup .= "    domain_tli\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
    $markup .= "    domain_field_string\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain_measurement) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= "  <td>\n";
      $markup .= "    " . $domain_measurement->get_sort() . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $domain_measurement->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // domain_tli
      $markup .= "  <td>\n";
      $markup .= "    " . $domain_measurement->get_domain_tli() . "\n";
      $markup .= "  </td>\n";

      // domain_field_string
      $markup .= "  <td>\n";
      $markup .= "    " . $domain_measurement->get_domain_field_string() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain_measurement) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\" style=\"width: 80px;\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $domain_measurement->get_id_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // domain_tli
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
      $markup .= "    domain_tli\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    <h2>" . $domain_measurement->get_domain_tli() . "</h2>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // domain_field_string
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
      $markup .= "    domain_field_string\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    <h2>" . $domain_measurement->get_domain_field_string() . "</h2>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // sort
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    <h2>" . $domain_measurement->get_sort() . "</h2>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  //method
  public function get_output_given_tli_and_fieldname($given_domain_tli, $given_domain_field_string) {
    $markup = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_given_domain_field_string($given_domain_field_string);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    // default color
    $color = "#CC0000";
    if ($this->get_list_bliss()->get_count() > 0) {

      foreach ($this->get_list_bliss()->get_list() as $domain_measurement) { 
        // debug the following is the reference and is used for debugging purposes
        //$markup .= "<p>". $domain_measurement->get_id() . "-";
        //$markup .= $domain_measurement->get_domain_tli() . "-";
        //$markup .= $domain_measurement->get_domain_field_string() . "</p>\n";
        if ($domain_measurement->get_webmaster_responsibility() == "ok") {
          $color = "#23AFF3";
        }
        $markup .= "<div style=\";padding: 10px; background-color: " . $color . ";\">\n";
        $markup .= $domain_measurement->get_webmaster_responsibility();
        $markup .= "</div>\n";
      }

    } else {
      $markup .= "<div style=\";padding: 10px; background-color: " . $color . ";\">\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  //method
  public function get_output_given_tli($given_domain_tli) {
    $markup = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $ok_strings = "";
      $ok_count = 0;
      $markup .= "<div style=\";padding: 10px; background-color: #23AFF3;\">\n";
      foreach ($this->get_list_bliss()->get_list() as $domain_measurement) { 
        if ($domain_measurement->get_webmaster_responsibility() == "ok") {
          $ok_strings .= $domain_measurement->get_domain_field_string() . " ";
          $ok_count++;
        } else {
          $markup .= "<strong>";
          $markup .= $domain_measurement->get_domain_field_string();
          $markup .= ":</strong> ";
          $markup .= $domain_measurement->get_webmaster_responsibility();
          $markup .= "<br/>\n";
        }
      }
      $markup .= "<em>ok (" . $ok_count . "):</em> " . $ok_strings . "<br />\n";
      $markup .= "</div>\n";

    } else {
      $markup .= "<div style=\";padding: 10px; background-color: red;\">\n";
      $markup .= "[none]\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  //method
  public function get_output_cell_given_tli($given_domain_tli) {
    $markup = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      foreach ($this->get_list_bliss()->get_list() as $domain_measurement) { 
        $markup .= "<td class=\"mid\">";
        $markup .= $domain_measurement->get_domain_field_string() . "<br />";
        $markup .= $domain_measurement->get_webmaster_responsibility();
        $markup .= "</td>\n";
      }

    } else {
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    [no domain_measurements found]\n";
      $markup .= "  </td>\n";
    }

    return $markup;
  }

  //method
  public function get_count_given_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    $ok_count = 0;

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $domain_measurement) { 
        if ($domain_measurement->get_webmaster_responsibility() == "ok") {
          $ok_count++;
        }
      }
    }
    // after loop, now set
    $markup .= $ok_count;

    return $markup;
  }

  // method
  public function get_given_project_id() {
    // todo seems like bad design
    return;
  }

  // method
  protected function output_preface() {
    $markup = "";

    $markup = "<br />\n";

    return $markup;
  }

}
