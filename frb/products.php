<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-28
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/products.php

include_once("lib/standard.php");

class Products extends Standard {

  // method
  private function make_product() {
    $obj = new Products($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT products.* FROM products WHERE products.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT products.* FROM products ORDER BY products.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_product();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->set_img_url(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $product) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $product->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "    " . $product->get_name() . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= "  <td>\n";
      $markup .= "    " . $product->get_sort() . "\n";
      $markup .= "  </td>\n";

      // status
      $markup .= "  <td>\n";
      $markup .= "    " . $product->get_status() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $product) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $product->get_id_with_link() . "\n";
      $markup .= "</tr>\n";
      $markup .= "  </td>\n";

      // date
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $product->get_date() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // patrons
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    patrons\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";

      $markup .= "  </td>\n";
      $markup .= "</tr>\n";


    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_invoices_given_patron_id($given_patron_id, $given_user_obj) {
    $markup = "";

    $this->set_given_patron_id($given_patron_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $product) {
        ++$num;

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $product->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // date
        $markup .= "  <td>\n";
        $markup .= "    " . $product->get_date() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";

    } else {
      $markup .= "No invoices found.<br />\n";
    }

    return $markup;
  }

}
