<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2013-04-02
// version 1.6 2017-01-28

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/hyperlink_reasons.php

include_once("lib/standard.php");

class HyperlinkReasons extends Standard {

  // given
  private $given_hyperlink_id;
  private $given_reason_id;

  // given_hyperlink_id
  public function set_given_hyperlink_id($var) {
    $this->given_hyperlink_id = $var;
  }
  public function get_given_hyperlink_id() {
    return $this->given_hyperlink_id;
  }

  // given_reason_id
  public function set_given_reason_id($var) {
    $this->given_reason_id = $var;
  }
  public function get_given_reason_id() {
    return $this->given_reason_id;
  }

  // attribute
  private $id;
  private $hyperlink_obj;
  private $reason_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // hyperlink_obj
  public function get_hyperlink_obj() {
    if (! isset($this->hyperlink_obj)) {
      include_once("hyperlinks.php");
      $this->hyperlink_obj = new Hyperlinks($this->get_given_config());
    }
    return $this->hyperlink_obj;
  }

  // reason_obj
  public function get_reason_obj() {
    if (! isset($this->reason_obj)) {
      include_once("reasons.php");
      $this->reason_obj = new Reasons($this->get_given_config());
    }
    return $this->reason_obj;
  }

  // method
  private function make_hyperlink_reason() {
    $obj = new HyperlinkReasons($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_hyperlink_id()) {
      $this->set_type("get_by_hyperlink_id");

    } else if ($this->get_given_reason_id()) {
      $this->set_type("get_by_reason_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT hyperlink_reasons.* FROM hyperlink_reasons WHERE hyperlink_reasons.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug tags sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT hyperlink_reasons.* FROM hyperlink_reasons ORDER BY hyperlink_reasons.reason_id, hyperlink_reasons.hyperlink_id;";

    } else if ($this->get_type() == "get_by_hyperlink_id") {
      $sql = "select hyperlink_reasons.* FROM hyperlink_reasons WHERE hyperlink_reasons.hyperlink_id = " . $this->get_given_hyperlink_id() . " ORDER BY hyperlink_reasons.reason_id;";

    } else if ($this->get_type() == "get_by_reason_id") {
      $sql = "select hyperlink_reasons.* FROM hyperlink_reasons WHERE hyperlink_reasons.reason_id = " . $this->get_given_reason_id() . " ORDER BY hyperlink_reasons.hyperlink_id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_hyperlink_id" ||
        $this->get_type() == "get_by_reason_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $hyperlink_reason = $this->make_hyperlink_reason();
        $hyperlink_reason->set_id(pg_result($results, $lt, 0));
        $hyperlink_reason->get_hyperlink_obj()->set_id(pg_result($results, $lt, 1));
        $hyperlink_reason->get_reason_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type = " . $this->get_type());
    }

  }

  // method
  public function get_reasons_given_hyperlink_id($given_hyperlink_id, $given_user_obj) { 
    $markup = "";

    // set
    $this->set_given_hyperlink_id($given_hyperlink_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error hyperlink_reasons: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $hyperlink_reason) {
          $markup .= $hyperlink_reason->get_reason_obj()->get_id_with_link() . " ";
      }
    }

    return $markup;
  }

  // method
  public function get_hyperlink_count_given_reason_id($given_reason_id, $given_user_obj) { 
    $markup = "";

    // set
    $this->set_given_reason_id($given_reason_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error hyperlink_reasons: results = " . $results . "<br />\n";
    }

    $markup .= $this->get_list_bliss()->get_count();

    return $markup;
  }

  // method
  public function get_list_of_hyperlinks($given_user_obj) {
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_all");
    $this->prepare_query();
    $hyperlinks_array = array();

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $hyperlink_reason) {
          $hyperlink_id = $hyperlink_reason->get_hyperlink_obj()->get_id();
          include_once("hyperlinks.php");
          $hyperlink_obj = new Hyperlinks($this->get_given_config());
          $hyperlink_obj->set_id($hyperlink_id);
          array_push($hyperlinks_array, $hyperlink_obj);
      }
    }

    return $hyperlinks_array;
  }

}
