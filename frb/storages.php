<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/storages.php

include_once("lib/scrubber.php");

class Storages extends Scrubber {

  // attributes
  private $id;
  private $plant_obj;
  private $instructions;
  private $source;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // instructions
  public function set_instructions($var) {
    $this->instructions = $var;
  }
  public function get_instructions() {
    return $this->instructions;
  }

  // source
  public function set_source($var) {
    $this->source = $var;
  }
  public function get_source() {
    return $this->source;
  }

  // method
  private function make_storage() {
    $obj = new Storages($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    //print "debug type " . $this->get_type() . "<br />";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT storages.id, storages.plant_id, storages.instructions, storages.source, plants.common_name FROM storages, plants WHERE storages.plant_id = plants.id ORDER BY plants.common_name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_storage();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_instructions(pg_result($results, $lt, 2));
        $obj->set_source(pg_result($results, $lt, 3));
        $obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    instructions\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    source\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $storage) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $storage->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $storage->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $storage->get_instructions() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $storage->get_source() . "\n";
      $markup .= "  </td>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<h2>" . $this->get_plant_obj()->get_common_name() . "</h2>\n";

    $markup .= "<p>" . $this->get_instructions() . "</p>\n";
    $markup .= "<p>" . $this->get_source() . "</p>\n";

    return $markup;
  }

}
