<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-17
// version 1.2 2015-01-04
// version 1.4 2015-06-19
// version 1.5 2015-10-16
// version 1.6 2016-03-25

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/goal_statements.php

include_once("lib/standard.php");

class GoalStatements extends Standard {

  // given
  // todo should the following be scoped as private so test this
  public $given_business_plan_text_id;

  // given_business_plan_text_id;
  public function set_given_business_plan_text_id($given_business_plan_text_id) {
    $this->given_business_plan_text_id = $given_business_plan_text_id;
  }
  public function get_given_business_plan_text_id() {
    return $this->given_business_plan_text_id;
  }

  // attributes
  private $project_obj;

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      include_once("projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

  // img_url default
  public function get_img_url_default() {
    // todo move the hard-coded URL below to config file
    return "http://mudia.com/dash/_images/attention.png";
  }

  // method
  private function make_goal_statement() {
    $obj = new GoalStatements($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");
      
    } else if ($this->get_given_business_plan_text_id()) {
      $this->set_type("get_by_business_plan_text_id");
      
    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // set order_by
    $order_by = " ORDER BY goal_statements.sort DESC, goal_statements.id";
    if ($this->get_given_view() == "printable") {
      // group  by project so that it is easier to comprehend project level
      $order_by = " ORDER BY goal_statements.name, goal_statements.id";
    }

    // debug
    //print "debug goal_statements type = " . $this->get_type() . "<br />\n";

    // username_sql
    // this insures that a user may only get their own rows
    $username_sql = " projects.user_name = '" . $this->get_user_obj()->name . "' ";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND goal_statements.id = " . $this->get_given_id() . " AND " . $username_sql . ";";
     
      // debug
      //print "debug goal_statements sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      if ($this->get_given_view() == "all") {
        #$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND " . $username_sql . $order_by . ";";
        $sql = "SELECT goal_statements.* FROM goal_statements " . $order_by . ";";

      } else if ($this->get_given_view() == "offline") {
        $sql = "SELECT goal_statements.* FROM goal_statements, projects WHERE goal_statements.status = 'offline' AND goal_statements.project_id = projects.id AND " . $username_sql . $order_by . ";";

      } else if ($this->get_given_view() == "zoneline") {
        $sql = "SELECT goal_statements.* FROM goal_statements, projects WHERE goal_statements.status = 'zoneline' AND goal_statements.project_id = projects.id AND " . $username_sql . $order_by . ";";

      } else {
        // online (also printable)
        // todo actually (idea that printable is a type of online is here)
        //// todo so perhaps refactor this into printable is a type of view 
        $sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE projects.status = 'in progress' AND goal_statements.project_id = projects.id AND " . $username_sql . $order_by . ";";
        $sql = "SELECT goal_statements.* FROM goal_statements " . $order_by . ";";
     }

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $order_by = " ORDER BY goal_statements.sort DESC, goal_statements.status, goal_statements.name";
      $sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND " . $username_sql . $order_by . ";";

    } else if ($this->get_type() == "get_by_business_plan_text_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT goal_statements.*, projects.name, projects.img_url, business_plan_texts.id, business_plan_texts.name FROM goal_statements, projects, business_plan_texts WHERE goal_statements.id = business_plan_texts.goal_statement_id AND goal_statements.project_id = projects.id AND business_plan_texts.id = " . $this->get_given_business_plan_text_id() . " AND " . $username_sql . " ORDER BY goal_statements.id;";

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM goal_statements WHERE " . $username_sql . ";";

    } else if ($this->get_type() == "get_oldest_sort_obj") {
      $sql = "SELECT id, name, sort FROM goal_statements WHERE " . $username_sql . " ORDER BY sort, name LIMIT 1;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    // debug
    //print "debug goaL_statements type = " . $this->get_type() . "<br />\n";
    //print "debug goal_statements sql = " . $sql . "<br />\n";

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_business_plan_text_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_goal_statement();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_description(pg_result($results, $lt, 1));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->set_name(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->get_project_obj()->set_name(pg_result($results, $lt, 7));
        $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 8));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_goal_statement();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_description(pg_result($results, $lt, 1));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->set_name(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        #if ($this->get_given_view() == "offline") {
        #  // skip
        #} else {
        #  $obj->get_project_obj()->set_name(pg_result($results, $lt, 7));
        #  $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 8));
        #}
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_goal_statement();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_goal_statement();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_oldest_sort_obj") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_goal_statement();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_oldest_sort_date(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_given_variables() {
    $markup = "";

    $markup .= parent::output_given_variables();

    // note: these are special to this class
    if ($this->get_given_business_plan_text_id()) {
      // deal with plural
      if ($this->get_list_bliss()->get_count() == 1) {
        // todo remove the "s" at the end of the class name
        $class_type = "This is a " . get_class($this) . " of ";
      } else {
        $class_type = "These are " . get_class($this) . " of ";
      }
      $given_variable = "business_plan_text";
      include_once("business_plan_texts.php");
      $obj = new BusinessPlanTexts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $name_with_link = $obj->get_name_and_link_given_id($this->get_given_business_plan_text_id(), $user_obj);
      $markup_being_sent = "";
      $markup .= $this->output_given_variables_boilerplate($class_type, $given_variable, $name_with_link, $markup_being_sent);
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table($style_flag = "") {
    $markup = "";

    // build table
    include_once("lib/html_table.php");
    $table_obj = new HtmlTable();
    $table_attribute_class = "plants";
    $table_type = "";

    $url = $this->url("business_plan_texts");
    $business_plan_texts_string = "<a href=\"" . $url . "\">business_plan_texts</a>";

    // make header cells
    $row_matrix =  array();
    if ($style_flag == "standard_flag") {
      $row_matrix =  array(
        array("header-derived", "#", "", ""),
        array("header", "project", "", ""),
        array("header", "status", "simple", ""),
        array("header", "sort", "", ""),
        array("header", "id", "simple", ""),
        array("header-derived", "img_url", "", ""),
        array("header", "name", "simple", ""),
        array("header-derived", $business_plan_texts_string, "", ""),
        //array("header-derived", "timecards total hours", "", ""),
        //array("header", "status", "", ""),
        //array("header-derived", "edit", "", "")
      );
    } else if ($style_flag == "hint_flag") {
      $row_matrix =  array(
        array("header-derived", "#", "", ""),
        array("header", "id", "simple", ""),
        array("header-derived", "img_url", "", ""),
        array("header", "name", "simple", ""),
      );
    }
    $table_obj->make_row($row_matrix);

    // loop to make data cells
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $obj) {

      // pre-process data
      $num++;

      // pre-process data
      $padding = "";
      $float = "";
      $width = "65";
      $project_img_element_data = $obj->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width);

      // pre-process data
      $obj_img_element_data = $obj->get_img_as_img_element_with_link($padding, $float, $width);

      // pre-process data
      //$edit_button = $this->get_create_obj()->output_edit_link($obj);

      // pre-process data
      $business_plan_texts_data = "";
      include_once("business_plan_texts.php");
      $business_plan_text_obj = new BusinessPlanTexts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $style_flag = "";
      $business_plan_texts_data = "<td style=\"font-size: 80%;\">" . $business_plan_text_obj->get_sidecar_given_goal_statement_id($obj->get_id(), $user_obj, $style_flag) . "</td>\n";

      // pre-process data
      $sort_width = "120px";
      $column_name = "sort";
      $sort_color = $this->get_timekeeper_obj()->calculate_cell_color($column_name, $obj->get_sort());
      $sort_styles = array();
      array_push($sort_styles, "width: $sort_width; ");
      array_push($sort_styles, "text-align: center; ");
      array_push($sort_styles, "background-color: " . $sort_color . ";");
 
     $bpt_styles = array();
      array_push($bpt_styles, "width: 200px; ");
      // todo clean up because this is not needed later in the code
      // $timecards_total_hours_styles = array();
      // array_push($timecards_total_hours_styles, "text-align: right; padding: 6px;");

      // pre-process sort
      // old
      //$sort = $obj->get_sort();
      // new (whole button)
      $sort_cell = $obj->get_sort_cell();

      // status
      // todo this is probably totally out of place
      $status_styles = array();
      if ($obj->get_status() == "zoneline") {
        array_push($status_styles, "background-color: #C0509F;");
      }

      // debug
      //print "debug goal_statements sort cell = " . $sort_cell . "\n<br />\n";

      // pre-process timecards_total_hours
      // todo clean up because it is too much information displayed
      if (0) {
        include_once("timecards.php");
        $timecard_obj = new Timecards($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $timecards_total_hours = $timecard_obj->get_total_hours_given_goal_statement_id($obj->get_id(), $user_obj);
      }

      // make data cells
      if ($style_flag == "" || $style_flag == "standard_flag") {
        $row_matrix = array(
          array("", $num, "", ""),
          array("", $project_img_element_data, "", ""),
          array("", $obj->get_status(), "simple", $status_styles),
          array("", $sort_cell, "", ""),
          array("", $obj->get_id_with_link(), "simple", ""),
          array("", $obj_img_element_data, "", ""),
          array("", $obj->get_name(), "simple", ""),
          array("", $business_plan_texts_data, "", $bpt_styles),
          //array("", $timecards_total_hours, "", $timecards_total_hours_styles),
          //array("", $obj->get_status(), "", ""),
          //array("", $edit_button, "", "")
        );
      } else if ($style_flag == "hint_flag") {
        $row_matrix = array(
          array("", $num, "", ""),
          array("", $obj->get_id_with_link(), "simple", ""),
          array("", $obj_img_element_data, "", ""),
          array("", $obj->get_name(), "simple", ""),
        );
      } else {
        return "Error: " . get_class($this) . " style_flag is not known.<br />\n";
      }
      $table_obj->make_row($row_matrix);
    }

    $markup .=  $table_obj->craft_table($table_attribute_class, $table_type);

    return $markup;
  }

  // method
  protected function output_single_nonstandard($given_parent_view = "") {
    $markup = "";

    // default
    $style_flag = "standard_flag";
    if ($this->get_given_view() == "printable") {
      // sidecar (hint reference style)
      // note intended to be a quick "hint reference" as to what is next
      $style_flag = "hint_flag";
    }

    // todo work on the following because it is not working
    if ($given_parent_view == "printable") {
      // skip
    } else {
      // pre-process data
      //$edit_button = $this->get_create_obj()->output_edit_link($this);
      $markup .= "<br />\n";
      $edit_button = "[edit button is offline]";
      $markup .= $edit_button;
    }

    // list related business_text_plans
    $heading = "true";
    include_once("business_plan_texts.php");
    $bpt_obj = new BusinessPlanTexts($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $bpt_obj->get_sidecar_given_goal_statement_id($this->get_id(), $user_obj, $style_flag, $heading);

    // todo clean up the following
    //$markup .= "[todo: list related business_text_plans]<br />\n";

    $markup .= "<br />\n";

    return $markup;
  }

  // method
  protected function output_preface_single() {
    $markup = "";

    // associated
    // name with link
    $padding = "";
    $float = "";
    $width = "32";
    $markup .= $this->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width);
    $markup .= " ";
    $markup .= $this->get_project_obj()->get_id();
    $markup .= " ";
    $markup .= $this->get_project_obj()->get_name() . "<br />\n";

    return $markup;
  }

  // method
  public function update() {

    // fix for database
    $description = addslashes($this->get_description());

    $sql = "UPDATE goal_statements set name = '" . $this->get_name() . "', description = '" . $description . "', project_id = '" . $this->get_project_obj()->get_id() . "', sort = '" . $this->get_sort() . "', status = '" . $this->get_status() . "' WHERE id = " . $this->get_given_id() . ";";
    $database_name = "mudiacom_soiltoil";
    $error_message = $this->get_db_dash()->new_update($database_name, $sql);

    // debug
    //print "debug goal_statements update sql = " . $sql . "<br />\n";

    return $error_message;
  }

  // method
  public function get_project_id() {
    $markup = "";

    // make sure that the given_id exists
    if (! $this->get_given_id()) {
      return "Error: goal_statements given_id does not exist.<br />\n";
    }

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there is 1 item
    if ($this->get_list_bliss()->get_count() == 1) {
      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    } else {
      print $this->get_db_dash()->output_error("Error " . get_class($this) . ": expect 1 count and got a count = " . $this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  public function get_build_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no goal_statement(s) were found.</p>\n";
      return $markup;
    }

    // header
    $markup .= "<h3 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px;\">goal statements</h3>\n";
    $markup .= "<p>There are " . $this->get_list_bliss()->get_count() . " <em>goal statements</em> listed in the following table.</p>\n";

    // build table
    include_once("lib/html_table.php");
    $table_obj = new HtmlTable();
    $table_attribute_class = "plants";
    $table_type = "";

    // todo note that the header was not needed
    // make header cells
    //$row_matrix =  array(
    //  array("header", "# img id name description", "", ""),
    //  array("header-derived", "business_plan_texts", "", ""),
    //);
    //$table_obj->make_row($row_matrix);

    // loop to make data cells
    $num = 0;
    $total_count = $this->get_list_bliss()->get_count();
    foreach ($this->get_list_bliss()->get_list() as $obj) {

      // pre-process data
      $num++;

      // pre-process data
      $padding = "0px 10px 0px 0px";
      $float = "left";
      $width = "65";
      $obj_img_element_data = $obj->get_img_as_img_element_with_link($padding, $float, $width);
      $num_name_and_description = "#" . $num . " of " . $total_count . " goal&nbsp;statements<br />\n" . $obj_img_element_data . "<br />\n" . "<em>This is goal statements id " . $obj->get_id_with_link() . ".</em><br />\n" . "<span style=\"font-size: 160%; background-color: #EFEFEF; font-weight: 600; margin: 0x; padding: 0x; border: 3px;\">" . $obj->get_name() . "</span><br />\n" . $obj->get_description();

      // pre-process data
      $given_variable = "business_plan_text";
      include_once("business_plan_texts.php");
      $bpt_obj = new BusinessPlanTexts($this->get_given_config());
      $user_obj = $obj->get_user_obj();
      $goal_statement_id = $obj->get_id();
      $goal_bpts_data = $bpt_obj->get_sidecar_given_goal_statement_id($goal_statement_id, $user_obj);
      $num_name_and_description .= "<h4>Business Plan Texts of this Goal Statement</h4>\n";
      $num_name_and_description .= $goal_bpts_data;

      // make datar cells
      $row_matrix = array(
        array("", $num_name_and_description, "", ""),
      );
      $table_obj->make_row($row_matrix);
    }

    $markup .= $table_obj->craft_table($table_attribute_class, $table_type);

    return $markup;
  }

  // method
  public function get_row_count_from_db($given_user_obj) {
    $markup = "";

    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->set_type("get_row_count");
    $markup .= $this->prepare_query();

    // the same code is somewhere else, so refactor
    if (! defined($this->get_list_bliss()->get_first_element()) ) {
      return 0;
    }
    return $this->get_list_bliss()->get_first_element()->get_row_count();
  }

  // method
  public function get_oldest_sort_object_from_db() {
    $markup = "";

    // load data from database
    $this->set_type("get_oldest_sort_obj");
    $markup .= $this->prepare_query();

    if ($markup) {
      $markup .= "<p>error get_oldest_sort_object_from_db()</p>\n";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) {
      // debug
      //print "debug  " . get_class($this)  . " oldest obj name = " . $obj->get_name() . "<br />\n";
      return $obj;
    }
  }

  // method
  public function get_goal_statement_given_bpt_id($given_bpt_id, $given_user_obj) {
    $markup = "";

    $this->set_given_business_plan_text_id($given_bpt_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // todo move all of the error statements to global section of class
      $markup .= "<p style=\"error\">no goal_statement(s) were found.</p>\n";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= $obj->get_name();
    }

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    // output meta data header
    $class_type = "This is a " . get_class($this) . " that ";
    $markup .= $class_type;
    $views = array("online", "all", "offline", "zoneline", "printable");
    $markup .= " has " . count($views) . " views: ";

    $last_pos = count($views) - 1;
    foreach ($views as $view) {
      $markup .= $this->get_menu_item($view, "view");
      if ($view != $views[$last_pos]) {
        $markup .= " | ";
      }
    }
    $markup .= ".<br />\n";

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // view
    $parameter_a = new Parameter();
    $parameter_a->set_name("view");
    $parameter_a->set_validation_type_as_view();
    array_push($parameters, $parameter_a);

    // make-sort-today
    $parameter_b = new Parameter();
    $parameter_b->set_name("make-sort-today"); 
    $parameter_b->set_validation_type_as_id();
    array_push($parameters, $parameter_b);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
            // debug
            //print "debug goal_statements view = " . $this->get_given_view() . "<br />\n";
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "goal_statements";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $goal_statement_obj = new GoalStatements($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $goal_statement_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function get_sidecar_given_project_id($given_project_id, $given_user_obj, $style_flag) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    $plural_string = "";
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">No <em>goal_statements</em> were found.</p>\n";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() == 1) {
      $plural_string = "";
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $plural_string = "s";
    }

    if ($style_flag == "standard_flag") {
      $markup .= "<h3>goal_statements</h3>\n";
    } else if ($style_flag == "hint_flag") {
      $markup .= "<p>This project has the following goal_statement" . $plural_string . ":</p>\n";
    }

    $markup .= $this->output_table($style_flag);

    return $markup;
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT goal_statements.id, goal_statements.name FROM goal_statements WHERE goal_statements.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug goal_statements: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "GoalStatements ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug goal_statements: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error goal_statements: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $goal_statement) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $goal_statement->get_id() . " " . $goal_statement->get_name_with_link() . " documentation " . $goal_statement->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $goal_statement->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error goal_statements: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "goal_statements: no goal_statements found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

}
