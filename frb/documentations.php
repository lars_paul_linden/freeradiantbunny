<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/documentationss.php

include_once("lib/standard.php");

class Documentations extends Standard {

  // attribute
  private $user_name;
  private $categorization;
  private $table_name;
  private $how_to_measure;

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // categorization
  public function set_categorization($var) {
    $this->categorization = $var;
  }
  public function get_categorization() {
    return $this->categorization;
  }

  // table_name
  public function set_table_name($var) {
    $this->table_name = $var;
  }
  public function get_table_name() {
    return $this->table_name;
  }

  // how_to_measure
  public function set_how_to_measure($var) {
    $this->how_to_measure = $var;
  }
  public function get_how_to_measure() {
    return $this->how_to_measure;
  }

  // method
  private function make_documentation() {
    $obj = new Documentations($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT documentations.* FROM documentatinos where documentations.id = " . $this->get_given_id() . ";";
      
    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT documentations.* FROM documentatinos ORDER BY documentations.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_goal_statement();
        $obj->set_id(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= $this->output_view();
    //$markup .= $this->output_table();
    $markup .= "debug documentation output_aggregate()<br />\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "debug documentation output_single()<br />\n";

    return $markup;
  }

}
