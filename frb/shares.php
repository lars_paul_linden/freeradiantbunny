<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/shares.php

include_once("lib/scrubber.php");

class Shares extends Scrubber {

  // given
  private $given_harvest_id;
  private $given_project_id;
  private $given_pickup_id;
  private $given_pickup_subview;

  // given_harvest_id
  public function set_given_harvest_id($var) {
    $this->given_harvest_id = $var;
  }
  public function get_given_harvest_id() {
    return $this->given_harvest_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_pickup_id
  public function set_given_pickup_id($var) {
    $this->given_pickup_id = $var;
  }
  public function get_given_pickup_id() {
    return $this->given_pickup_id;
  }

  // given_pickup_subview
  public function set_given_pickup_subview($var) {
    $this->given_pickup_subview = $var;
  }
  public function get_given_pickup_subview() {
    return $this->given_pickup_subview;
  }

  // attributes
  private $id;
  private $harvest_obj;
  private $owner;
  private $price;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // harvest_obj
  public function get_harvest_obj() {
    if (! isset($this->harvest_obj)) {
      include_once("harvests.php");
      $this->harvest_obj = new Harvests($this->get_given_config());
    }
    return $this->harvest_obj;
  }

  // owner
  public function set_owner($var) {
    $this->owner = $var;
  }
  public function get_owner() {
    return $this->owner;
  }

  // price
  public function set_price($var) {
    $this->price = $var;
  }
  public function get_price() {
    return $this->price;
  }

  // method
  private function make_share() {
    $obj = new Shares($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  public function make_pickup() {
    include_once("pickups.php");
    $obj = new Pickups;
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  public function make_pickup_plant() {
    include_once("pickup_plants.php");
    $obj = new PickupPlants;
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_subview_class() == "pickups") {
      // subset
      $this->set_type("get_pickups");

    } else if ($this->get_given_subview_class() == "pickup") {
      // subset
      $this->set_type("get_pickup_by_id");

    } else if ($this->get_given_harvest_id()) {
      // subset
      $this->set_type("get_by_harvest_id");

    } else if ($this->get_given_project_id()) {
      // subset
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_harvest_id") {
      $sql = "SELECT shares.id, shares.harvest_id, shares.owner, shares.price, harvests.name FROM shares, harvests WHERE shares.harvest_id = harvests.id AND shares.harvest_id = " . $this->get_given_harvest_id() . " ORDER BY shares.owner, shares.id;";

    } else if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT shares.id, shares.harvest_id, shares.owner, shares.price, harvests.name FROM shares, harvests WHERE shares.harvest_id = harvests.id AND harvests.project_id = " . $this->get_given_project_id() . " ORDER BY harvests.sort, shares.owner, shares.id;";

    } else if ($this->get_type() == "get_pickups") {
      $sql = "SELECT pickups.id, pickups.harvest_id, pickups.date, harvests.name FROM pickups, harvests WHERE pickups.harvest_id = " . $this->get_given_harvest_id() . " AND pickups.harvest_id = harvests.id ORDER BY pickups.date;";

    } else if ($this->get_type() == "get_pickup_by_id") {
      $sql = "SELECT pickups.id, pickups.harvest_id, pickups.date, harvests.name FROM pickups, harvests WHERE pickups.harvest_id = " . $this->get_given_harvest_id() . " AND pickups.harvest_id = harvsts.id AND pickups.id = " . $this->get_given_pickup_id() . " ORDER BY pickups.date;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT shares.id FROM shares, harvests, projects WHERe shares.harvest_id = harvests.id AND harvests.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "';";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_harvest_id" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $share = $this->make_share();
        $share->set_id(pg_result($results, $lt, 0));
        $share->get_harvest_obj()->set_id(pg_result($results, $lt, 1));
        $share->set_owner(pg_result($results, $lt, 2));
        $share->set_price(pg_result($results, $lt, 3));
        $share->get_harvest_obj()->set_name(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "get_pickups" ||
               $this->get_type() == "get_pickup_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $pickup = $this->make_pickup();
        $pickup->set_id(pg_result($results, $lt, 0));
        $pickup->get_harvest_obj()->set_id(pg_result($results, $lt, 1));
        $pickup->set_date(pg_result($results, $lt, 2));
        $pickup->get_harvest_obj()->set_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    $markup .= "<div class=\"subsubmenu\">\n";

    $url = $this->url("harvests/" . $this->get_given_harvest_id());
    $markup .= "To:  <a href=\"" . $url . "\">harvest</a>\n";
    $markup .= "<br />\n";
    $markup .= "<br />\n";

    if ($this->get_type() != "get_by_harvest_id" &&
        $this->get_type() != "get_pickups") {
      $url = $this->url("shares/harvest/" . $this->get_given_harvest_id());
      $markup .= "See also:  <a href=\"" . $url . "\">Shares</a>\n";
    } else {
      if ($this->get_type() == "get_pickups") {
        $url = $this->url("shares/harvest/" . $this->get_given_harvest_id());
        $markup .= "See also:  <a href=\"" . $url . "\">Shares</a>\n";
      } else {
        $markup .= "You are here: Shares\n";
      }
    }
    $markup .= "<br />\n";
    $markup .= "<br />\n";

    if ($this->get_type() != "get_by_harvest_id") {
      if ($this->get_type() != "get_pickups") {
        $url = $this->url("shares/harvest/" . $this->get_given_harvest_id() . "/pickups");
        $markup .= "See also:  <a href=\"" . $url . "\">Pickups</a>\n";
      } else {
        $markup .= "You are here: Pickups\n";
      }
    } else {
        $url = $this->url("shares/harvest/" . $this->get_given_harvest_id() . "/pickups");
        $markup .= "See also:  <a href=\"" . $url . "\">Pickups</a>\n";
    }
    $markup .= "<br />\n";

    $markup .= "</div>\n";

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";
    // only authenticated users
    if ($this->get_user_obj()) {
      //if ($this->get_user_obj()->uid) {
      //  $markup .= "<div class=\"subsubmenu-user\">\n";
      //  $markup .= "<strong>These shares were created by</strong> " . $this->get_user_obj()->name . "<br />\n";
      //  $markup .= "</div>\n";
      //}
    }
    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_harvest_id()) {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "    These shares are of harvest id = " . $this->get_given_harvest_id() . "\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // subview
    if ($this->get_type()  == "get_pickups" ||
        $this->get_type()  == "get_pickup_by_id") {
      $markup .= $this->output_pickups();
      return $markup;
    }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    harvest\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    owner\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    price\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickups<br />count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickups logged<br />count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $flag_harvest_name = "";

    // rows
    $num = 0;
    $sum_price = 0;
    foreach ($this->get_list_bliss()->get_list() as $share) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "<!--  shares.id = " . $share->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= $share->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      if ($flag_harvest_name == $share->get_harvest_obj()->get_name()) {
        // skip because it is same as the last row
      } else {
        $markup .= $share->get_harvest_obj()->get_name_with_link() . "\n";
      }
      $flag_harvest_name= $share->get_harvest_obj()->get_name();
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $share->get_owner() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $sum_price += $share->get_price();
      $markup .= $share->get_price() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"center\">\n";
      if ($this->get_given_harvest_id()) {
        $url = $this->url("pickups/harvest/" . $this->get_given_harvest_id());
        $markup .= "<a href=\"" . $url . "\">\n";
        // todo
        $markup .= $this->get_pickups_count();
        $markup .= "</a>\n";
      }
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    // totals row
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"4\" align=\"right\">\n";
    $markup .= "  <strong>price sum</strong>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= $sum_price . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td colspan=\"2\">\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_pickups() {
    $markup = "";

    // see if this is Pickups or Pickup (by id and subview)
    if (! $this->get_given_pickup_subview()) {
      $markup .= "<h2>Pickups</h2>\n";
    }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    harvest\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    shares<br /> count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup price<br />(derived)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    market value<br />of pickup basket<br />(derived)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    item count<br />of weekly share list\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $pickup) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "<!--  pickup.id = " . $pickup->get_id() . " -->\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $pickup->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $pickup->get_harvest_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"center\">\n";
      $url = $this->url("shares/harvest/" . $this->get_given_harvest_id());
      $markup .= "  <a href=\"" . $url . "\">";
      $markup .= $this->get_shares_count();
      $markup .= "</a>";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $pickup->get_date() . "\n";
      $markup .= "  </td>\n";

      $share_price = $this->get_share_price();
      // the number of pickup dates is the same as the number of rows
      $pickup_date_sum = $this->get_list_bliss()->get_count();
      $pickup_price = $share_price / $pickup_date_sum;
      $pickup_price = number_format($pickup_price, 2);

      $markup .= "  <td align=\"right\">\n";
      $markup .= $pickup_price . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"center\">\n";
      $url = $this->url("shares/harvest/" . $this->get_given_harvest_id() . "/pickup/" . $pickup->get_id() . "/list");
      $markup .= "    <a href=\"" . $url . "\">";
      include_once("lib/counter.php");
      $counter_obj = new Counter();
      $counter_obj->set_given_pickup_id($pickup->get_id());
      $counter_obj->set_user_obj($this->get_user_obj());
      // count of items on weekly share list
      $markup .= $counter_obj->get_pickup_plants_count_by_pickup_id();
      $markup .= "</a>";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // see if this is Pickups or Pickup (by id and subview)
    if ($this->get_given_pickup_subview()) {
      $list = $this->get_list_bliss()->get_list();
      $obj = $list[0];
      $date = $obj->get_date();
      $markup .= "<h2>Pickup " . $date . "</h2>\n";
      
      include_once("pickup_plants.php");
      $pickup_plant_obj = new PickupPlants();
      $markup .= $pickup_plant_obj->output_weekly_share_list($this->get_given_pickup_id(), $this->get_user_obj());
    }


    return $markup;
  }

  // method
  private function get_share_price() {
    $markup = "";

    include_once("shares.php");
    $share_obj = new Shares();
    $share_obj->set_given_harvest_id($this->get_given_harvest_id());
    $share_obj->set_user_obj($this->get_user_obj());

    // load data from database
    $share_obj->determine_type();
    $markup .= $share_obj->prepare_query();

    // only output if there are items to output
    if ($share_obj->get_list_bliss()->get_count() > 0) {
      $list = $share_obj->get_list_bliss()->get_list();
      $first_share_obj = $list[0];
      // note: this assumes that the price of the first element is same as all
      return $first_share_obj->get_price();
      
    }
    return $markup;
  }

  // method
  private function get_shares_count() {
    $markup = "";

    include_once("shares.php");
    $share_obj = new Shares();
    $share_obj->set_given_harvest_id($this->get_given_harvest_id());
    $share_obj->set_user_obj($this->get_user_obj());

    // load data from database
    $share_obj->determine_type();
    $markup .= $share_obj->prepare_query();

    // only output if there are items to output
    if (! $markup) {
      return $share_obj->get_list_bliss()->get_count();
    }

    return $markup;
  }

  // method
  public function output_table_given_harvest_id($given_harvest_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_harvest_id($given_harvest_id);
    $this->set_user_obj($given_user_obj);

    // get data
    $this->determine_type();
    $this->prepare_query();

    $markup = "<h2>Shares of this Harvest</h2>\n";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  private function get_pickups_count() {
    $markup = "";

    include_once("class_counter.php");
    $counter_obj = new Counter();
    if ($this->get_given_harvest_id()) {
      $counter_obj->set_given_harvest_id($this->get_given_harvest_id());
    }
    if ($this->get_given_project_id()) {
      $counter_obj->set_given_project_id($this->get_given_project_id());
    }
    $counter_obj->set_user_obj($this->get_user_obj());
    return $counter_obj->get_pickups_count();
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "debug shares output_single()<br />\n";

    return $markup;
  }

}
