<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/observations.php

include_once("lib/scrubber.php");

class Observations extends Scrubber {

  // given
  private $given_project_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attribute
  private $id;
  private $land_obj;
  private $unit_obj;
  private $ts;
  private $notes;
  private $design_instance_obj;
  private $raw;
  private $raw_read;
  private $measurement;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // land_obj
  public function get_land_obj() {
    if (! isset($this->land_obj)) {
      include_once("lands.php");
      $this->land_obj = new Lands($this->get_given_config());
    }
    return $this->land_obj;
  }

  // unit_obj
  public function get_unit_obj() {
    if (! isset($this->unit_obj)) {
      include_once("units.php");
      $this->unit_obj = new Units($this->get_given_config());
    }
    return $this->unit_obj;
  }

  // ts
  public function set_ts($var) {
    $this->ts = $var;
  }
  public function get_ts() {
    return $this->ts;
  }

  public function get_ts_as_string() {
    if ($this->ts) {
      include_once("lib/dates.php");
      $dates_obj = new Dates($this->get_given_config());
      $ts_as_string = $dates_obj->convert_from_timestamp_to_date_as_first_year_style_with_time($this->ts);
      return $ts_as_string;
    }
    // not set, so return null string
    return "";
  }

  // notes
  public function set_notes($var) {
    $this->notes = $var;
  }
  public function get_notes() {
    return $this->notes;
  }

  // design_instance_obj
  public function get_design_instance_obj() {
    if (! isset($this->design_instance_obj)) {
      include_once("design_instances.php");
      $this->design_instance_obj = new DesignInstances($this->get_given_config());
    }
    return $this->design_instance_obj;
  }

  // raw
  public function set_raw($var) {
    $this->raw = $var;
  }
  public function get_raw() {
    return $this->raw;
  }

  // method
  public function get_raw_with_htmlentities() {
    // convert \n to <br />
    $raw_with_entities = $this->raw;
    $raw_with_entities = htmlentities($raw_with_entities);
    $raw_with_entities = nl2br($raw_with_entities);
    //$raw_with_entities = str_replace(' ','&nbsp;',$raw_with_entities);
    return $raw_with_entities;
  }

  // raw_read
  public function set_raw_read($var) {
    $this->raw_read = $var;
  }
  public function get_raw_read() {
    return $this->raw_read;
  }

  // measurement
  public function set_measurement($var) {
    $this->measurement = $var;
  }
  public function get_measurement() {
    return $this->measurement;
  }

  // method
  public function get_measurement_dynamic() {
    $haystack = $this->measurement;
    $needle = "png";
    $pos = strpos($haystack, $needle);
    if ($pos !== false) {
      // png file, so output as img tag
      return "<img src=\"http://eatlocalfreshfood.org/lff/webcam/" . $this->measurement . "\" width=\"400\" alt=\"webcam\" />\n";;
    }
    return $this->measurement;
  }

  // method
  private function make_observation() {
    $obj = new Observations($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT observations.id, observations.land_id, observations.unit_id, observations.ts, observations.notes, observations.design_instance_id, observations.raw, observations.raw_read, observations.measurement, units.name FROM observations, design_instances, designs, units, processes, business_plan_texts WHERE units.id = observations.unit_id AND observations.design_instance_id = design_instances.id AND design_instances.design_id = designs.id AND designs.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.id = " . $this->get_given_project_id() . " ORDER BY observations.ts DESC, observations.unit_id;";

    } else if ($this->get_type() == "get_by_id") {

    } else if ($this->get_type() == "get_all") {
      // todo write sql (must WHERE projects.user_name = $this->get_user_obj()->user_name
    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_project_id") {
       for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_observation();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_land_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_unit_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_ts(pg_result($results, $lt, 3));
        $obj->set_notes(pg_result($results, $lt, 4));
        $obj->get_design_instance_obj()->set_id(pg_result($results, $lt, 5));
        $obj->set_raw(pg_result($results, $lt, 6));
        $obj->set_raw_read(pg_result($results, $lt, 7));
        $obj->set_measurement(pg_result($results, $lt, 8));
        $obj->get_unit_obj()->set_name(pg_result($results, $lt, 9));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  public function output_given_variables() {
    $markup = "";

    // todo

    return $markup;
  }

  // method
  public function print_table_given_design_instance_id($design_instance_id) {
    $markup = "";

    // parameters //
    $this->set_given_design_instance_id($design_instance_id); 

    // database //
    $this->determine_type();
    $this->prepare_query();

    $markup .= "<h3>Observations for this Design Instance</h3>\n";

    if ($this->get_list_bliss()->get_count() == 0) {
      $markup .= $this->output_aggregate();
    }
 
    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // input observations
    $markup .= $this->input_observations();

    // blank list and redo
    $this->get_list_bliss()->empty_list();
    $this->prepare_query();

    // add refresh javascript
    $markup .= $this->output_refresh_javascript();

    // heading
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    land id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    measurement\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    unit id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"220px\">\n";
    $markup .= "    ts\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    notes\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    design instance\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    raw\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    raw_read\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    // todo install pagination instead of hard-coded max
    $max_rows = 20;
    foreach ($this->get_list_bliss()->get_list() as $observation) {
      $num++;

      // only show so many rows
      if ($num > $max_rows) {
        break;
      }

      $markup .= "<tr>\n";

      $markup .= "<td class=\"header\">\n";
      $markup .= "  " . $num . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $observation->get_id() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $observation->get_land_obj()->get_id() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td align=\"center\">\n";
      $markup .= "  <span style=\"font-weight: bold; font-size: 120%;\">" . $observation->get_measurement_dynamic() . "</span>\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $observation->get_unit_obj()->get_name_with_link() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $observation->get_ts_as_string() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $observation->get_notes() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $observation->get_design_instance_obj()->get_id() . "\n";
      $markup .= "</td>\n";

      //$markup .= "<td>\n";
      //$markup .= "  " . $observation->get_raw_with_htmlentities() . "\n";
      //$markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $observation->get_raw_read() . "\n";
      $markup .= "</td>\n";

      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";


    return $markup;
  }

  // method
  private function input_observations() {
    $markup = "";

    // about this function

    // here is an overview of this function
    // this is part of the Garden InfoSys project
    // the idea is to have sensors in the greenhouses (or garden perhaps)
    // that provide data to the database upon which decisions can be made

    // when data is uploaded it is placed...
    // as XML in the "raw" field of the "observations" table

    // so, given all that, the purpose of this function is:
    // to convert that raw XML data into the data that populates the fields
    // of the "observations" table

    // there are 5 steps in the input_observations processs
    // here we go...

    // 1 of 5 reverse the array so that the oldest are processesd first
    $list = $this->get_list_bliss()->get_list();
    arsort($list);

    // 2 of 5 get the "raw" XML data from the table's "raw" field
    foreach ($list as $observation) {

      // one the raw field is input the "raw_read" flag is set to TRUE
      // so, see if the raw has been read
      if ($observation->get_raw_read() == "f") {

        // debug
        //print "debug raw_read = " . $observation->get_raw_read() . "<br />";

        // here is the XML, now in a handy variable
        $xml = $observation->get_raw();

        // 3 of 5 given this XML, parse it!

        // for more about php xml parsing see:
        // http://php.net/manual/en/function.xml-parse-into-struct.php

        // instantiate the parser
        $parser = xml_parser_create();

        // debug: test parser with simple string of XML
        //$simple = "<para><note>simple note</note></para>";

        // parse
        $status = xml_parse_into_struct($parser, $xml, $values, $tags);

        // free parser resources
        xml_parser_free($parser);

        // check for errors
        if ($status) {

          // debug
          //print "debug status = " . $status . "<br />";

          // debug to show the raw structures
          //print "debug tags array<br />\n";
          //print "<pre>\n";
          //print_r($tags);        
          //print "</pre>\n";
          //print "<br />\n";
          //print "debug values array<br />\n";
          //print "<pre>\n";
          //print_r($values);
          //print "</pre>\n";
          //print "<br />\n";

          // store what is found
          // hash where (observation_index => observation_obj)
          // then I can use the observation_index to know what belongs to
          // the respective observation_obj
          $new_observation_obj_array = array();
          $first_obj;

          // loop through the found structures
          foreach ($tags as $key=>$val) {

            //print "<br />debug tag<br />\n";

            // solve for observation
            // each observation element is an object
            if ($key == "OBSERVATION") {
              //print "debug observation<br />\n";
              foreach ($val as $index => $values_index) {

                //print "debug index = " . $index . "<br />\n";

                $type = $values[$values_index]["type"];

                if ($type == "open") {

                  // start tag
                  //print "debug observation open<br />\n";
                  // first object is object of loop iteration
                  if (count($new_observation_obj_array) == 0) {

                    //print "debug first observation<br />\n";

                    // store in hash
                    $new_observation_obj_array[$values_index] = $observation;
		    $first_obj = $observation;

                  } else {

                    //print "debug not first observation<br />\n";
                    $observation_obj = new Observations($this->get_given_config());

                    // add to list
                    $this->get_list_bliss()->add_item($observation_obj);

                    // link the child to the parent via the "raw" field
                    $observation_obj->set_raw("parent_id=" . $first_obj->get_id());

                    // store in hash
                    $new_observation_obj_array[$values_index] = $observation_obj;

                  }
                } else if ($type == "close") {
                  // end tag
                  //print "debug observation end<br />\n";
                  // skip
                }
              }
            }

            // todo clean up old code
            // get data in case it is needed
            //$values_index = $val[0];
            //$value = "";
            //if (isset($values[$values_index]["value"])) {
            //  $value = $values[$values_index]["value"];
            //}
            //print "debug values_index = " . $values_index . "<br />\n";
            //if (! $value) {
            //  print "debug value = " . $value . "<br />\n";
            //  continue;
            //}

            if ($key == "DESIGN-INSTANCE-ID") {
              // now answer: which object in the array of observations obj?
              // loop comparing indexes
              // the index must be more than current and less than next
              foreach ($val as $index => $values_index) {
                foreach ($new_observation_obj_array as $akey => $avalue) {
                  if ($values_index > $akey) {
                    $value = $values[$values_index]["value"];
                    $avalue->get_design_instance_obj()->set_id($value);
                  }
                }
              }
            } else if ($key == "LAND-ID") {
              // loop and store what is found in the respective object
              foreach ($val as $index => $values_index) {

  	        //print "debug LAND-ID values_index = " . $values_index . "<br />";
                $save_this_value = $values[$values_index]["value"];
  	        //print "debug save this value = " . $save_this_value . "<br />";

                // but in which object?
                $obj_with_highest_value_index;
                foreach ($new_observation_obj_array as $akey => $avalue) {
                  if ($values_index > $akey) {
    		    //print "debug store compare $values_index $akey<br />";
                    $obj_with_highest_value_index = $avalue;
                  }
                }
                $obj_with_highest_value_index->get_land_obj()->set_id($save_this_value);
              }

            } else if ($key == "UNIT-ID") {
              // loop and store what is found in the respective object
              foreach ($val as $index => $values_index) {

  	        //print "debug UNIT-ID values_index = " . $values_index . "<br />";
                $save_this_value = $values[$values_index]["value"];
  	        //print "debug save this value = " . $save_this_value . "<br />";

                // but in which object?
                $obj_with_highest_value_index;
                foreach ($new_observation_obj_array as $akey => $avalue) {
                  if ($values_index > $akey) {
    		    //print "debug store compare $values_index $akey<br />";
                    $obj_with_highest_value_index = $avalue;
                  }
                }
                $obj_with_highest_value_index->get_unit_obj()->set_id($save_this_value);
                // add the units name
                if ($save_this_value == 10) {
                  $obj_with_highest_value_index->get_unit_obj()->set_name("degrees F");
                } else if ($save_this_value == 22) {
                  $obj_with_highest_value_index->get_unit_obj()->set_name("filename of image showing time, inside-temperature, and outside-temperature");
                }
              }

            } else if ($key == "MEASUREMENT") 
{              // loop and store what is found in the respective object
              foreach ($val as $index => $values_index) {

  	        //print "debug MEASUREMENT values_index = " . $values_index . "<br />";
                $save_this_value = $values[$values_index]["value"];
  	        //print "debug save this value = " . $save_this_value . "<br />";

                // but in which object?
                $obj_with_highest_value_index;
                foreach ($new_observation_obj_array as $akey => $avalue) {
                  if ($values_index > $akey) {
    		    //print "debug store compare $values_index $akey<br />";
                    $obj_with_highest_value_index = $avalue;
                  }
                }
                $obj_with_highest_value_index->set_measurement($save_this_value);
              }

            } else if ($key == "TS") {
              // now answer: which object in the array of observations obj?
              // loop comparing indexes
              // the index must be more than current and less than next
              foreach ($val as $index => $values_index) {
                foreach ($new_observation_obj_array as $akey => $avalue) {
                  if ($values_index > $akey) {
                    $value = $values[$values_index]["value"];
                    $avalue->set_ts($value);
                  }
                }
              }

            } else if ($key == "SERIAL-NUMBER") {
              // now answer: which object in the array of observations obj?
              // loop comparing indexes
              // the index must be more than current and less than next
              foreach ($val as $index => $values_index) {
                foreach ($new_observation_obj_array as $akey => $avalue) {
                  if ($values_index > $akey) {
                    $value = $values[$values_index]["value"];
                    // store this in the notes field
                    $notes = "serial_number=" . $value;
                    $avalue->set_notes($notes);
                  }
                }
              }
            }
          }
        } else {
          print "<p error=\"error\">Error: unable to to parse XML of raw Observations<br />XML data:<br />" . htmlentities($xml) . "</p>\n";
        }

        // 4 of 5 change the flag on "raw_read" field
        foreach ($new_observation_obj_array as $observation) {
          $observation->set_raw_read('TRUE');
        }

        // 5 of 5 store the data that was found in database
        // formulate the SQL statement
        $database_name = "plantdot_soiltoil";
        $first_flag = 1;
        foreach ($new_observation_obj_array as $observation) {
          // first element is an update
          if ($first_flag) {
            $first_flag = 0;
            $sql = "UPDATE observations SET ";
            $first_flag = 1;
            $found_flag = 0;
            if ($observation->get_design_instance_obj()->get_id()) {
              $sql .= " design_instance_id=" . $observation->get_design_instance_obj()->get_id();
              $first_flag = 0;
              $found_flag = 1;
            }
            if ($found_flag && ! $first_flag) {
              $sql .= ", ";
              $first_flag = 0;
            }
            $found_flag = 0;
            if ($observation->get_land_obj()->get_id()) {
              $sql .= " land_id=" . $observation->get_land_obj()->get_id();
              $first_flag = 0;
              $found_flag = 1;
            }
            if ($found_flag && ! $first_flag) {
              $sql .= ", ";
              $first_flag = 0;
            }
            $found_flag = 0;
            if ($observation->get_unit_obj()->get_id()) {
              $sql .= "unit_id=" . $observation->get_unit_obj()->get_id();
              $first_flag = 0;
              $found_flag = 1;
            }
            if ($found_flag && ! $first_flag) {
              $sql .= ", ";
              $first_flag = 0;
            }
            $found_flag = 0;
            if ($observation->get_ts()) {
              $sql .= "ts='" . $observation->get_ts() . "'";
              $first_flag = 0;
              $found_flag = 1;
            }
            if ($found_flag && ! $first_flag) {
              $sql .= ", ";
              $first_flag = 0;
            }
            $found_flag = 0;
            if ($observation->get_notes()) {
              $sql .= "notes='" . $observation->get_notes() . "'";
              $first_flag = 0;
              $found_flag = 1;
            }
            if ($found_flag && ! $first_flag) {
              $sql .= ", ";
              $first_flag = 0;
            }
            $found_flag = 0;
            if ($observation->get_raw_read()) {
              $sql .= "raw_read='" . $observation->get_raw_read() . "'";
              $first_flag = 0;
              $found_flag = 1;
            }
            if ($found_flag && ! $first_flag && $observation->get_measurement()) {
              $sql .= ", ";
              $first_flag = 0;
            }
            $found_flag = 0;
            if ($observation->get_measurement()) {
              $sql .= "measurement='" . $observation->get_measurement() . "'";
              $first_flag = 0;
              $found_flag = 1;
            }
            $sql .= " WHERE id = " . $observation->get_id() . ";";

            // todo the following lines to the class_databasedashboard
            $markup .= $this->get_db_dash()->new_update($database_name, $sql);

          } else {
            $sql = "INSERT INTO observations (";
            if ($observation->get_design_instance_obj()->get_id()) {
              $sql .= "design_instance_id, ";
            }
            if ($observation->get_land_obj()->get_id()) {
              $sql .= "land_id, ";
            }
            if ($observation->get_unit_obj()->get_id()) {
              $sql .= "unit_id, ";
            }
            $sql .= "ts, ";
            $sql .= "notes, ";
            $sql .= "raw_read, ";
            $sql .= "raw";
	    if ($observation->get_measurement()) {
              $sql .= ", ";
              $sql .= "measurement";
            }
            $sql .= ") VALUES (";
            if ($observation->get_design_instance_obj()->get_id()) {
              $sql .= $observation->get_design_instance_obj()->get_id();
              $sql .= ", ";
            }
            if ($observation->get_land_obj()->get_id()) {
              $sql .= $observation->get_land_obj()->get_id();
              $sql .= ", ";
            }
            if ($observation->get_unit_obj()->get_id()) {
              $sql .= $observation->get_unit_obj()->get_id();
              $sql .= ", ";
            }
            $sql .= "'" . $observation->get_ts() . "'";
            $sql .= ", ";
            $sql .= "'" . $observation->get_notes() . "'";
            $sql .= ", ";
            $sql .= "'" . $observation->get_raw_read() . "'";
            $sql .= ", ";
            $sql .= "'" . $observation->get_raw() . "'";
            if ($observation->get_measurement()) {
              $sql .= ", ";
              $sql .= "'" . $observation->get_measurement() . "'";
            }
            $sql .= ");";

            // todo the following lines to the class_databasedashboard
            $markup .= $this->get_db_dash()->new_insert($database_name, $sql);
          }

          // debug
          //print "debug sql = " . $sql . "<br /><br />";
        }
      } else {
        //print "debug observations raw_read = " . $observation->get_raw_read() . "<br />\n";
      }
    }


    return $markup;
  }

  // method
  private function get_contents($given_file) {
   $xml = "";
   $handle = fopen($given_file, 'r');  
   $xml = fread($handle, filesize($given_file));
   fclose($handle);
   return $xml;
  }

  // method
  public function load_observations_into_datbase($given_xml) {
    $markup = "";

    $markup .= "<pre>\n";
    $contents_entities = htmlentities($given_xml);
    $markup .=  $contents_entities;
    $markup .= "</pre>\n";

    return $markup;
  }

  // method
  public function output_refresh_javascript() {
    $markup = "";
    $markup .= "<script language=\"JavaScript\">
    <!--
    var sURL = unescape(window.location.pathname);
    setTimeout( \"refresh()\", 120*1000 );
    function refresh()
    {
        window.location.href = sURL;
    }
    //-->
    </script>";

    return $markup;
  }
}
