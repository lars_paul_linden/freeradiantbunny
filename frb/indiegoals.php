<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/indiegoals.php

include_once("lib/scrubber.php");

class Indiegoals extends Scrubber {

  public $todo_dynamic_array = array(); // array

  // attributes
  private $id;
  private $name;
  private $description;
  private $date;
  private $reading;
  private $yawp_agent_type;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // reading
  public function set_reading($var) {
    $this->reading = $var;
  }
  public function get_reading() {
    return $this->reading;
  }

  // yawp_agent_type
  public function set_yawp_agent_type($var) {
    $this->yawp_agent_type = $var;
  }
  public function get_yawp_agent_type() {
    return $this->yawp_agent_type;
  }

  // method
  private function make_indiegoal() {
    $obj = new IndieGoals($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");
    } else {
      $this->set_type("get_all");
    }

  }

  // method
  protected function get_given_project_id() {
    return "project_id is not know by indiegoals";
  }


  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      $sql = "";

    } else if ($this->get_type() == "get_all") {
      $sql = "select * from indiegoals order by name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_tenperday";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_indiegoal();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_date(pg_result($results, $lt, 3));
        $obj->set_reading(pg_result($results, $lt, 4));
        $obj->set_yawp_agent_type(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->print_table();

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function print_table() {
    $markup = "";

      $markup .= "<table class=\"plants\">\n";

      // heading
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    name\n";
      $markup .= "    <span style=\"color: #EE7600;\">description</span>\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    yawp agent type\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    reading\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $indiegoal) {
        $num++;

        $markup .= "<tr>\n";

        $markup .= "  <td class=\"ig\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        // date
        // if date is today, then change color of cell
        include_once("lib/timekeeper.php");
        $timekeeper_obj = new Timekeeper();
        $today = $timekeeper_obj->get_today_date_as_first_year_style();
        if ($indiegoal->get_date() == $today) {
          $markup .= "  <td class=\"mid-bright\" width=\"120\">\n";
        } else {
          $markup .= "  <td class=\"ig\" width=\"120\">\n";
        }
        $markup .= "    " . $indiegoal->get_date() . "<br />\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td class=\"ig\">\n";
        $markup .= "    " . $indiegoal->get_id() . "<br />\n";
        $markup .= "  </td>\n";

        //
        $markup .= "  <td class=\"ig-row\" width=\"320\" style=\"background-color: #99FFAA\">\n";
        $markup .= "    <strong style=\"font-size: 120%;\">" . $indiegoal->get_name() . "</strong><br />\n";
        $markup .= "    <p><span style=\"color: #EE7600;\">" . $indiegoal->get_description() . "</span></p>\n";
        $markup .= "  </td>\n";

        // yawp_agent_type
        $markup .= "  <td class=\"ig\" style=\"background-color: #99FFAA\">\n";
        $markup .= "    <span style=\"font-size: 120%;\">" . $indiegoal->get_yawp_agent_type() . "</span>\n";
        $markup .= "  </td>\n";

        //
        if ($indiegoal->get_reading() == "OK") {
          $markup .= "  <td class=\"ig\" width=\"350\" style=\"background-color: #339966\">\n";
        } else {
          $markup .= "  <td class=\"ig\" width=\"350\" style=\"background-color: #9999AA\">\n";
        }
        $markup .= "    " . $indiegoal->get_reading() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function print_todo_dynamic_table() {
    $markup = "";

    $markup .= "<h1>list of indiegoal todo dynamic (hard-coded in php)</h1>\n";

    // once it is design, create the table
    include_once("todo.php");
    $todo_obj = new Todo($this->get_given_config());
    $todo_obj->set_id("1");
    $todo_obj->set_icon("<a href=\"http://mudia.com/dash/indiegoal_by_hand/email.php\"><img src=\"http://mudia.com/dash/_images/email.jpg\" alt=\"\" /></a>");
    $todo_obj->set_ig("email");
    $todo_obj->set_todo("check all accounts once per day");
    $todo_obj->set_priority("high");
    array_push($this->todo_dynamic_array, $todo_obj);

    $todo_obj = new Todo($this->get_given_config());
    $todo_obj->set_id("2");
    $todo_obj->set_icon("<a href=\"http://mudia.com/dash/indiegoal_by_hand/backup.php\"><img src=\"http://mudia.com/dash/_images/backup_donut.png\" width=\"65\" alt=\"backup\" /></a>");
    $todo_obj->set_ig("backup");
    $todo_obj->set_todo("backup all backupable");
    $todo_obj->set_priority("high (last=9-Feb-2012)");
    array_push($this->todo_dynamic_array, $todo_obj);

    $todo_obj = new Todo($this->get_given_config());
    $todo_obj->set_id("3");
    $todo_obj->set_icon("<a href=\"https://mudia.com/dash/designdoc/app_gnucash.php\"><img src=\"http://mudia.com/dash/_images/sisu_stamp.jpeg\" width=\"65\" alt=\"backup\" /></a>");
    $todo_obj->set_ig("gnucash");
    $todo_obj->set_todo("bookkeeping");
    $todo_obj->set_priority("high");
    array_push($this->todo_dynamic_array, $todo_obj);

    $count = count($this->todo_dynamic_array);

    // debug
    $markup .= "<p>count = $count</p>\n";

    $markup .= "<div id=\"tenperday\">\n";

    if ($count) {

      $markup .= "<table class=\"domains\">\n";

      // heading
      $markup .= "<tr>\n";
      $markup .= "  <td colspan=\"6\" class=\"tiptop\">\n";
      $markup .= "    tenperday indiegoal todo dynamic\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    icon\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead-narrow\">\n";
      $markup .= "    ig\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead\">\n";
      $markup .= "    todo\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"colhead-narrow\">\n";
      $markup .= "    priority\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

     foreach ($this->todo_dynamic_array as $todo) {

        $markup .= "<tr>\n";

        $markup .= "  <td class=\"ig\">\n";
        $markup .= "    " . $todo->get_sort() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"ig\">\n";
        $markup .= "    " . $todo->get_icon() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"ig\">\n";
        $markup .= "    " . $todo->get_ig() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"ig\">\n";
        $markup .= "    " . $todo->get_todo() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"ig\">\n";
        $markup .= "    " . $todo->get_priority() . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
      $markup .= "</div><!-- end tenperday -->\n";
    } else {
      $markup .= "<p class=\"error\">No todo dymanics found.</p>\n";
    }
    return $markup;
  }

  // method
  protected function output_preface() {
    $markup = "";

    return $markup;
  }
  
}
