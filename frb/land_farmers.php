<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-20
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/land_farmers.php

include_once("lib/scrubber.php");

class LandFarmers extends Scrubber {

  // attributes
  private $id;
  private $farmer;
  private $soil_area_id;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // farmer
  public function set_farmer($var) {
    $this->farmer = $var;
  }
  public function get_farmer() {
    return $this->farmer;
  }

  // soil_area_id
  public function set_soil_area_id($var) {
    $this->soil_area_id = $var;
  }
  public function get_soil_area_id() {
    return $this->soil_area_id;
  }

  // method
  private function make_land_farmer() {
    $obj = new LandFarmers($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // hierarchy
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT * FROM land_farmers WHERE land_farmers.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      // special: only display projects that have no parents (subquery below)

        $sql = "SELECT * FROM projects WHERE projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY projects.sort DESC, projects.name;";

      } else if ($this->get_given_view() == "online") {
        // all
        $sql = "SELECT * FROM land_farmers;

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_project();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_famer(pg_result($results, $lt, 1));
        $obj->get_soil_areas_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }
  }
  
  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "todo";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "todo";

    return $markup;
  }

}
