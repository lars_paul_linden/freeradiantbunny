<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-11-26
// version 1.2 2015-01-17

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/builds.php

include_once("lib/standard.php");

class Builds extends Standard {

  // defaults
  // img_url
  public function get_img_url_default() {
    // todo place this string in the config file of the user
    return "http://mudia.com/dash/_images/sisu_stamp.jpeg";
  }

  // attributes
  private $project_obj;
  private $client_supplier_obj;
  private $versions;
  private $ranking;

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      include_once("projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

  // client_supplier_obj
  public function get_client_supplier_obj() {
    if (! isset($this->client_supplier_obj)) {
      include_once("suppliers.php");
      $this->client_supplier_obj = new Suppliers($this->get_given_config());
    }
    return $this->client_supplier_obj;
  }

  // versions
  public function set_versions($var) {
    $this->versions = $var;
  }
  public function get_versions() {
    return $this->versions;
  }

  // ranking
  public function set_ranking($var) {
    $this->ranking = $var;
  }
  public function get_ranking() {
    return $this->ranking;
  }

  // method
  private function make_build() {
    $obj = new Builds($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");
      
    } else if ($this->get_row_count()) {
      $this->set_type("get_row_count");
      
    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug builds type = " . $this->get_type() . "<br />\n";

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // set order_by
    $order_by = " ORDER BY builds.ranking, builds.name";

    // debug
    //print "debug goal_statements type = " . $this->get_type() . "<br />\n";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT builds.*, projects.name, projects.img_url FROM builds, projects WHERE builds.project_id = projects.id AND builds.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";
      
      // debug
      //print "debug goal_statements sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
     $sql = "SELECT builds.*, projects.name, projects.img_url FROM builds, projects WHERE builds.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT builds.*, projects.name, projects.img_url FROM builds, projects WHERE builds.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else if ($this->get_type() == "get_row_count") {
      // security: only get the rows owned by the user
      $sql = "SELECT count(*) from builds;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // debug
    //print "debug builds sql = " . $sql . "<br />\n";
    //print "debug builds given_project_id = " . $this->get_given_project_id() . "<br />\n";

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_build();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_status(pg_result($results, $lt, 2));
        $obj->set_description(pg_result($results, $lt, 3));
        $obj->set_sort(pg_result($results, $lt, 4));
        $obj->set_name(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->get_client_supplier_obj()->set_id(pg_result($results, $lt, 7));
        $obj->set_versions(pg_result($results, $lt, 8));
        $obj->set_ranking(pg_result($results, $lt, 9));
        $obj->get_project_obj()->set_name(pg_result($results, $lt, 10));
        $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 11));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_given_variables() {
    $markup = "";

    $markup .= parent::output_given_variables();

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_table() {
    $markup = "";

    // build table
    //include_once("lib/html_table.php");
    //$table_obj = new HtmlTable();
    //$table_attribute_class = "plants";
    //$table_type = "";

    // make header cells
    //$row_matrix =  array(
    //  array("header", "#", "", ""),
    //  array("header", "project", "", ""),
    //  array("header", "sort", "", ""),
    //  array("header", "id", "simple", ""),
    //  //array("header", "img_url", "", ""),
    //  array("header", "name", "simple", ""),
    //  array("header", "status", "", ""),
    //  array("header", "database", "", ""),
    //  array("header", "client", "", ""),
    //  array("header", "versions", "", ""),
    //);
    //$table_obj->make_row($row_matrix);

    // loop
    //$num = 0;
    //foreach ($this->get_list_bliss()->get_list() as $obj) {

      // pre-process data
      //$num++;

      // todo clean up the old table making functions (too hard to change)

      // pre-process data
      //$sort_width = "120px";
      //$sort_color = "#FFFFFF";
      //if (preg_match("/^Z/", $obj->get_status())) {
      //  $sort_color = "#63AB62";
      //}

      // pre-process sort_styles
      //$sort_width = "120px";
      //$column_name = "sort";
      //$sort_color = $this->get_timekeeper_obj()->calculate_cell_color($column_name, $obj->get_sort());
      //$sort_styles = array();
      //array_push($sort_styles, "width: $sort_width; ");
      //array_push($sort_styles, "text-align: center; ");
      //array_push($sort_styles, "background-color: " . $sort_color . ";");

      // pre-process data
      //include_once("projects.php");
      //$project_obj = new Projects($this->get_given_config());
      //$user_obj = $this->get_user_obj();

      // make data cells
      //$row_matrix = array(
      //  array("", $num, "", ""),
      //  array("", $project_img_element_data, "", ""),
      //  array("", $obj->get_sort(), "", $sort_styles),
      //  array("", $obj->get_id_with_link(), "simple", ""),
      //  //array("", $obj_img_element_data, "", ""),
      //  array("", $obj->get_name(), "simple", ""),
      //  array("", $obj->get_status(), "", ""),
      //  array("", $obj->get_project_obj()->get_database_string_given_id($obj->get_project_obj()->get_id(), $user_obj), "", ""),
      //  array("", $obj->get_client_supplier_obj()->get_name_with_link_given_id($obj->get_client_supplier_obj()->get_id(), $user_obj), "", ""),
      //  array("", $obj->get_versions(), "", ""),
      //);
      //$table_obj->make_row($row_matrix);
    //}

    //$markup = $table_obj->craft_table($table_attribute_class, $table_type);

    $markup = "<p>project portfolio</p>\n";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $short_table = 0;

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 40px;\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    ranking\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    client supplier id\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    versions\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $build) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td style=\"text-align: center;\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // status (version)
      $markup .= "  <td style=\"text-align: center;\">\n";
      $markup .= "    " . $build->get_status() . "\n";
      $markup .= "  </td>\n";

      // project
      $padding = "";
      $float = "";
      $width = "65";
      $project_img_element_data = $build->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width);
      $markup .= "  <td style=\"text-align: center;\">\n";
      $markup .= "    " . $project_img_element_data . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $build->get_sort_cell();

      // ranking
      $markup .= "  <td style=\"text-align: center;\">\n";
      $markup .= "    " . $build->get_ranking() . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td style=\"text-align: right; padding: 2px;\">\n";
      $url = $this->url("builds/" . $build->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $build->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td style=\"text-align: left; padding: 2px;\">\n";
      // todo go through all classes and where class name refactor to function
      $url = $this->url(strtolower(get_class($this)) . "/" . $build->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $build->get_name() . "</a>\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $build->get_description() . "\n";
      $markup .= "  </td>\n";

      // client supplier id
      $markup .= "  <td>\n";
      $markup .= "    " . $build->get_client_supplier_obj()->get_id() . "\n";
      $markup .= "  </td>\n";

      // versions
      $markup .= "  <td>\n";
      $markup .= "    " . $build->get_versions() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    $markup .= "<p>project_id: " . $this->get_project_obj()->get_id() . "</p>\n";
    $markup .= "<p>client_supplier_id: " . $this->get_client_supplier_obj()->get_id() . "</p>\n";
    $markup .= "<p>versions: " . $this->get_versions() . "</p>\n";

    // build name
    $markup .= "<hr /><br />\n";
    $markup .= "<!-- build starts here -->\n";
    $markup .= "<h1>" . $this->get_name() . "</h1>\n";

    // project img
    include_once("projects.php");
    $obj = new Projects($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $obj->get_build_given_id($this->get_project_obj()->get_id(), $user_obj);

    $markup .= "<!-- build ends here -->\n";

    return $markup;
  }

  // method
  public function update() {

    // todo this function might not be for this class
    // fix for database
    $description = addslashes($this->get_description());

    $sql = "UPDATE goal_statements set name = '" . $this->get_name() . "', description = '" . $description . "', project_id = '" . $this->get_project_obj()->get_id() . "', sort = '" . $this->get_sort() . "', status = '" . $this->get_status() . "' WHERE id = " . $this->get_given_id() . ";";
    $database_name = "mudiacom_soiltoil";
    $error_message = $this->get_db_dash()->new_update($database_name, $sql);

    // debug
    //print "debug goal_statements update sql = " . $sql . "<br />\n";

    return $error_message;
  }

  // method
  public function get_project_id() {
    $markup = "";

    // make sure that the given_id exists
    if (! $this->get_given_id()) {
      return "Error: goal_statements given_id does not exist.<br />\n";
    }

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there is 1 item
    if ($this->get_list_bliss()->get_count() == 1) {
      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    } else {
      print $this->get_db_dash()->output_error("Error " . get_class($this) . ": expect 1 count and got a count = " . $this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validator_function_string("id");
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "builds";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $build_obj = new Builds($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $build_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug builds target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    return $markup;
  }

}
