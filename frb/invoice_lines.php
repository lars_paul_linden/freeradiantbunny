<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-28
// version 1.2 2015-01-17

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/invoice_lines.php

include_once("lib/standard.php");

class InvoiceLines extends Standard {

  // given
  private $given_invoice_id;
  private $given_pickup_detail_id;
  private $given_product_id;

  // given_invoice_id
  public function set_given_invoice_id($var) {
    $this->given_invoice_id = $var;
  }
  public function get_given_invoice_id() {
    return $this->given_invoice_id;
  }

  // given_pickup_detail_id
  public function set_given_pickup_detail_id($var) {
    $this->given_pickup_detail_id = $var;
  }
  public function get_given_pickup_detail_id() {
    return $this->given_pickup_detail_id;
  }

  // given_product_id
  public function set_given_product_id($var) {
    $this->given_product_id = $var;
  }
  public function get_given_product_id() {
    return $this->given_product_id;
  }

  // attribute
  private $id;
  private $invoice_obj;
  private $product_obj;
  private $quantity;
  private $price;
  private $note;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // invoice_obj
  public function get_invoice_obj() {
    if (! isset($this->invoice_obj)) {
      include_once("invoices.php");
      $this->invoice_obj = new Invoices($this->get_given_config());
    }
    return $this->invoice_obj;
  }

  // product_obj
  public function get_product_obj() {
    if (! isset($this->product_obj)) {
      include_once("products.php");
      $this->product_obj = new Products($this->get_given_config());
    }
    return $this->product_obj;
  }

  // quantity
  public function set_quantity($var) {
    $this->quantity = $var;
  }
  public function get_quantity() {
    return $this->quantity;
  }

  // price
  public function set_price($var) {
    $this->price = $var;
  }
  public function get_price() {
    return $this->price;
  }

  // note
  public function set_note($var) {
    $this->note = $var;
  }
  public function get_note() {
    return $this->note;
  }

  // method
  private function make_invoice_line() {
    $obj = new InvoiceLines($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_invoice_id()) {
      $this->set_type("get_by_invoice_id");

    } else if ($this->get_given_product_id()) {
      $this->set_type("get_by_product_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT invoice_lines.*, products.name FROM invoice_lines, products WHERE invoice_lines.product_id = products.id AND invoice_lines.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT invoice_lines.*, products.name FROM invoice_lines, products WHERE invoice_lines.product_id = products.is AND invoice_lines.id = " . $this->get_given_id() . " ORDER BY invoice_lines.id;";

    } else if ($this->get_type() == "get_by_invoice_id") {
      $sql = "SELECT invoice_lines.*, products.name FROM invoice_lines, invoices, products WHERE invoice_lines.invoice_id = invoices.id AND invoice_lines.product_id = products.id AND invoice_lines.invoice_id = " . $this->get_given_invoice_id() . " ORDER BY invoice_lines.id;";

    } else if ($this->get_type() == "get_by_product_id") {
      $sql = "SELECT invoice_lines.*, products.name FROM invoice_lines, products WHERE invoice_lines.product_id = products.id ORDER BY invoice_lines.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_invoice_id" ||
        $this->get_type() == "get_by_product_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_invoice_line();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_invoice_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_product_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_quantity(pg_result($results, $lt, 3));
        $obj->set_price(pg_result($results, $lt, 4));
        $obj->set_note(pg_result($results, $lt, 5));
        $obj->get_product_obj()->set_name(pg_result($results, $lt, 6));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    invoice\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    product\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    quatity\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    price\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    line total\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    note\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $invoice_line) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice_line->get_id() . "\n";
      $markup .= "  </td>\n";

      // invoice
      $markup .= "  <td>\n";
      include_once("invoices.php");
      $invoice_obj = new Invoices($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $invoice_obj->get_invoice_given_id($invoice_line->get_invoice_obj()->get_id(), $user_obj);
      $markup .= "  </td>\n";

      // product
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice_line->get_project_obj()->get_name() . "\n";
      $markup .= "  </td>\n";

      // quantity
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice_line->get_quantity() . "\n";
      $markup .= "  </td>\n";

      // price
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice_line->get_price() . "\n";
      $markup .= "  </td>\n";

      // line_total
      $markup .= "  <td>\n";
      //$markup .= "    " . $invoice_line->get_line_total() . "\n";
      $markup .= "  </td>\n";

      // note
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice_line->get_note() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  private function get_line_total() {
    $markup = "";

    $line_total = $this->get_quantity() * $this->get_price();
    $markup = $line_total;

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $invoice) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice->get_id_with_link() . "\n";
      $markup .= "</tr>\n";
      $markup .= "  </td>\n";

      // date
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $invoice->get_date() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // customers
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    customers\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";

      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // note
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    note\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $invoice->get_note() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_invoices_given_customer_id($given_customer_id, $given_user_obj) {
    $markup = "";

    $this->set_given_customer_id($given_customer_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $invoice) {
        ++$num;

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // date
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice->get_date() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";

    } else {
      $markup .= "No invoice_lines found.<br />\n";
    }

    return $markup;
  }

  // method
  public function get_invoice_lines_given_invoice_id($given_invoice_id, $given_user_obj) {
    $markup = "";

    $this->set_given_invoice_id($given_invoice_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    product\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    quantity\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    price\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    line_total\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    note\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 600px;\">\n";
      $markup .= "    <em>pickups</em> (if any)\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $invoice_line) {
        ++$num;

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // product
        $markup .= "  <td>\n";
        $url = $this->url("products/" . $invoice_line->get_product_obj()->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $invoice_line->get_product_obj()->get_name() . "</a>\n";
        $markup .= "  </td>\n";

        // quantity
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_quantity() . "\n";
        $markup .= "  </td>\n";

        // price
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_price() . "\n";
        $markup .= "  </td>\n";

        // line_total
        $markup .= "  <td style=\"text-align: right;\">\n";
        $markup .= "    " . $invoice_line->get_line_total() . "\n";
        $markup .= "  </td>\n";

        // note
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_note() . "\n";
        $markup .= "  </td>\n";

        // pickups
        $markup .= "  <td>\n";
        include_once("pickups.php");
        $pickup_obj = new Pickups($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $pickup_obj->get_pickups_given_invoice_line_id($invoice_line->get_id(), $user_obj);
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";

    } else {
      $markup .= "No invoice_lines found.<br />\n";
    }

    return $markup;
  }

  // method
  public function get_total_given_invoice_id($given_invoice_id, $given_user_obj) {
    $markup = "";

    $this->set_given_invoice_id($given_invoice_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      $total = 0;
      foreach ($this->get_list_bliss()->get_list() as $invoice_line) {
        $total += $invoice_line->get_line_total();
      }
      return $total;
    }

    return "";
  }

  // method
  public function get_invoice_lines_given_pickup_detail_id($given_pickup_detail_id, $given_user_obj) {
    $markup = "";

    $this->set_given_pickup_detail_id($given_pickup_detail_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    customer\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoice id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoice line id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    product\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    pickup_detail\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    quantity\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    price\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    line_total\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $invoice_line) {
        ++$num;

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // customer
        $markup .= "  <td>\n";
        $invoice_id = $invoice_line->get_invoice_obj()->get_id();
        $user_obj = $this->get_user_obj();
        $markup .= "    " . $invoice_line->get_invoice_obj()->get_customer_link_given_invoice_id($invoice_id, $user_obj) . "\n";
        $markup .= "  </td>\n";

        // invoice id
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_invoice_obj()->get_id() . "\n";
        $markup .= "  </td>\n";

        // invoice line id
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // product
        $markup .= "  <td>\n";
        $url = $this->url("products/" . $invoice_line->get_product_obj()->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $invoice_line->get_product_obj()->get_name() . "</a>\n";
        $markup .= "  </td>\n";

        // pickup_detail
        // todo refactor and use existing pickup_detail_obj instead of new one
        $markup .= "  <td>\n";
        include_once("pickup_details.php");
        $pickup_detail_obj = new PickupDetails($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $pickup_detail_obj->get_pickup_detail_given_id($invoice_line->get_pickup_detail_obj()->get_id(), $user_obj);
        $markup .= "  </td>\n";

        // quantity
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_quantity() . "\n";
        $markup .= "  </td>\n";

        // price
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_price() . "\n";
        $markup .= "  </td>\n";

        // line_total
        $markup .= "  <td style=\"text-align: right;\">\n";
        $markup .= "    " . $invoice_line->get_line_total() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";

    } else {
      $markup .= "No invoice_lines found.<br />\n";
    }

    return $markup;
  }

  // method
  public function get_quick_invoice() {
    $markup = "";

    $this->set_given_id($this->get_id());

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    customer\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoice id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoice line id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    product\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    quantity\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    price\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    line_total\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $invoice_line) {
        ++$num;

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // customer
        $markup .= "  <td>\n";
        $invoice_id = $invoice_line->get_invoice_obj()->get_id();
        $user_obj = $this->get_user_obj();
        $markup .= "    " . $invoice_line->get_invoice_obj()->get_customer_link_given_invoice_id($invoice_id, $user_obj) . "\n";
        $markup .= "  </td>\n";

        // invoice id
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_invoice_obj()->get_id() . "\n";
        $markup .= "  </td>\n";

        // invoice line id
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // product
        $markup .= "  <td>\n";
        $url = $this->url("products/" . $invoice_line->get_product_obj()->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $invoice_line->get_product_obj()->get_name() . "</a>\n";
        $markup .= "  </td>\n";

        // quantity
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_quantity() . "\n";
        $markup .= "  </td>\n";

        // price
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice_line->get_price() . "\n";
        $markup .= "  </td>\n";

        // line_total
        $markup .= "  <td style=\"text-align: right;\">\n";
        $markup .= "    " . $invoice_line->get_line_total() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";

    } else {
      $markup .= "No invoice_lines found.<br />\n";
    }

    return $markup;
  }

}
