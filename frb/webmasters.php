<?php
// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/webmasters.php

include_once("lib/standard.php");

class Webmasters extends Standard {

  // attribute
  private $user_name;

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // method
  private function make_webmaster() {
    $obj = new Webmasters($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webmasters.* FROM webmasters WHERE id = " . $this->get_given_id() . " AND webmasters.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT webmasters.* FROM webmasters WHERE webmasters.user_name = '" . $this->get_user_obj()->name . "' ORDER BY webmasters.sort DESC, webmasters.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webmaster();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->set_img_url(pg_result($results, $lt, 5));
        $obj->set_user_name(pg_result($results, $lt, 6));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // todo this code is based upon goal_statements.php code
    // build the table using objects
    include_once("class_html_table.php");
    $table_obj = new HtmlTable();
    $table_type = "";
    $table_attribute_class = "plants";

    // make header cells
    $row_matrix =  array(
      array("header-derived", "#", "", ""),
      array("header", "sort", "", ""),
      array("header", "id", "simple", ""),
      array("header-derived", "img_url", "", ""),
      array("header", "name", "simple", ""),
      array("header", "description", "", ""),
      array("header", "status", "", ""),
    );
    $table_obj->make_row($row_matrix);

    // start data
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $num++;

      // pre-process for data
      // set variables
      $padding = "";
      $float = "";
      $width = "65";
      $obj_img_element_data = $obj->get_img_as_img_element_with_link($padding, $float, $width);

      // make header cells
      $row_matrix = array(
        array("", $num, "", ""),
        array("", $obj->get_sort(), "", ""),
        array("", $obj->get_id_with_link(), "simple", ""),
        array("", $obj_img_element_data, "", ""),
        array("", $obj->get_name(), "simple", ""),
        array("", $obj->get_description(), "", ""),
        array("", $obj->get_status(), "", ""),
      );
      $table_obj->make_row($row_matrix);
    }

    $markup .= $table_obj->craft_table($table_attribute_class, $table_type);

    return $markup;
  }

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

}
