<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-17

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/albums.php

include_once("lib/scrubber.php");

class Albums extends Scrubber {

  // given
  private $given_project_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $id;
  private $name;
  private $year;
  private $cover_front_url;
  private $cover_back_url;
  private $notes;
  private $album_url;
  private $project_id;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // year
  public function set_year($var) {
    $this->year = $var;
  }
  public function get_year() {
    return $this->year;
  }

  // cover_front_url
  public function set_cover_front_url($var) {
    $this->cover_front_url = $var;
  }
  public function get_cover_front_url() {
    return $this->cover_front_url;
  }

  // cover_back_url
  public function set_cover_back_url($var) {
    $this->cover_back_url = $var;
  }
  public function get_cover_back_url() {
    return $this->cover_back_url;
  }

  // notes
  public function set_notes($var) {
    $this->notes = $var;
  }
  public function get_notes() {
    return $this->notes;
  }

  // album_url
  public function set_album_url($var) {
    $this->album_url = $var;
  }
  public function get_album_url() {
    return $this->album_url;
  }

  // project_id
  public function set_project_id($var) {
    $this->project_id = $var;
  }
  public function get_project_id() {
    return $this->project_id;
  }

  // method
  private function make_album() {
    $obj = new Albums($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
        $sql = "select albums.* from albums where albums.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
        $sql = "select albums.*  from albums ORDER BY albums.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_album();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_year(pg_result($results, $lt, 2));
        $obj->set_cover_front_url(pg_result($results, $lt, 3));
        $obj->set_cover_back_url(pg_result($results, $lt, 4));
        $obj->set_notes(pg_result($results, $lt, 5));
        $obj->set_album_url(pg_result($results, $lt, 6));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";

      $markup .= "<table class=\"plants\">\n";

      // heading
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";

      // year
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    year\n";
      $markup .= "  </td>\n";

      // cover_front_url
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    cover front URL\n";
      $markup .= "  </td>\n";

      // cover_back_url
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    cover back URL\n";
      $markup .= "  </td>\n";

      // notes
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    notes\n";
      $markup .= "  </td>\n";

      // album_url
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    album_url\n";
      $markup .= "  </td>\n";

      // mp3
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    mp3\n";
      $markup .= "  </td>\n";

      // lyrics
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    lyrics\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $album) {

        $markup .= "<tr>\n";

        $num++;
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $url = $this->url("albums/" . $album->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $album->get_id() . "</a><br />\n";
        $markup .= "  </td>\n";

        // name
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . strtoupper($album->get_name()) . "<br />\n";
        $markup .= "  </td>\n";

        // year
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $album->get_year() . "<br />\n";
        $markup .= "  </td>\n";

        // cover_front_url
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $album->get_cover_front_url() . "<br />\n";
        $markup .= "    <a href=\"" . $album->get_album_url() . "\"><img src=\"" . $album->get_cover_front_url() . "\" alt=\"" . $album->get_id() . "\" width=\"66\" /></a><br />\n";
        $markup .= "  </td>\n";

        // cover_back_url
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $album->get_cover_back_url() . "<br />\n";
        if ($album->get_cover_back_url()) {
          $markup .= "    <a href=\"" . $album->get_album_url() . "\"><img src=\"" . $album->get_cover_back_url() . "\" alt=\"" . $album->get_id() . "\" width=\"66\" /></a><br />\n";
        }
        $markup .= "  </td>\n";

        // notes
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $album->get_notes() . "<br />\n";
        $markup .= "  </td>\n";

        // album_url
        $markup .= "  <td class=\"mid\">\n";
        if ($album->get_album_url()) {
          $markup .= "    <a href=\"" . $album->get_album_url() . "\">" . $album->get_album_url() . "</a><br />\n";
        }
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    <br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    <br />\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    /**
     * @todo move this to menu function
     */
    $url = $this->url("albums");
    $markup .= "<p><a href=\"" . $url . "/\">all albums</a></p>";

    $markup .= "<table class=\"plants\">\n";

    foreach ($this->get_list_bliss()->get_list() as $album) {

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $album->get_id() . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";


      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    <a href=\"" . $album->get_album_url() . "\">" . $album->get_name() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    year\n";
      $markup .= "  </td>\n";
      if (strstr($album->get_year(), "<pre>")) {
        $markup .= "  <td style=\"background-color: #339955;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $markup .= "    " . $album->get_year() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    cover_front_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "    " . $album->get_cover_front_url() . "<br />\n";
      $markup .= "    <a href=\"" . $album->get_album_url() . "\"><img src=\"" . $album->get_cover_front_url() . "\" alt=\"" . $album->get_id() . "\" width=\"66\" /></a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    cover_back_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $album->get_cover_back_url() . "<br />\n";
      $markup .= "    <a href=\"" . $album->get_album_url() . "\"><img src=\"" . $album->get_cover_back_url() . "\" alt=\"" . $album->get_id() . "\" width=\"66\" /></a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    notes\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $album->get_notes() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    album_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $album->get_album_url();
      $markup .= "    <a href=\"" . $url . "\">" . $album->get_album_url() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    // list songs of this album
    include_once("songs.php");
    $song_obj = new Songs($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $header = 1;
    $song_data = $song_obj->get_song_list_given_album_id($album->get_id(), $user_obj, $header);
    $markup .= $song_data . "\n";

    return $markup;
  }

  // method
  private function get_list_of_domain_id() {
    $album_id_array = array();

    $this->set_type("get_all");
    $this->determine_type();
    $markup = $this->prepare_query();
    if ($markup) {
      //error
      print "error domains.php: " . $markup;
    }
    foreach ($this->get_list_bliss()->get_list() as $album) {
      array_push($album_id_array, $album->get_id());
    }

    return $album_id_array;
  }

  // method
  public function output_preface() {

  }

}
