<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-20
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/agricultural_types.php

include_once("lib/scrubber.php");

class AgriculturalTypes extends Scrubber {

  // given
  private $given_id;

  // instance variables
  private $id;
  private $name;
  private $description;
  private $img_url;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  public function get_name_with_link() {
    $url = $this->url("agricultural_type/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // img_url
  public function set_img_url($var) {
    $this->img_url = $var;
  }
  public function get_img_url() {
    return $this->img_url;
  }

  // img_url
  //public function get_img_url_default() {
    /**
     * @todo deal with default images in a centralized way
     */
    /**
     * @todo place this string in the config file of the user
     */
    /**
     * @todo need to only try to get the commons if online
     */
    //return "http://freeradiantbunny.org/main/en/docs/_images/commons/img_url/agricultural_types/agricultural_type_default.png";
    /**
     * @todo have a switch in the config for local images
     */
    //return "http://localhost/~blaireric/dev/domains/frb/frb_up/frb_v01/frb_base/frb_zone/frb_to_sift/agricultural_type_default.png";
  //}

  // method
  public function get_img_url_as_img_element() {
    $markup = "";

    $markup .= "<img src=\"" . $this->get_img_url() . "\" alt=\"icon\" width=\"66\" />";

    return $markup;
  }

  // method
  public function get_img_as_img_element_with_link($padding, $float, $width) {
    $markup = "";

    $url = $this->url("agricultural_types/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\">";
    $markup .= $this->get_img_url_as_img_element($padding, $float, $width);
    $markup .= "</a>";

    return $markup;
  }

  // method
  private function make_agricultural_type() {
    $obj = new AgriculturalTypes($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT agricultural_types.id, agricultural_types.name, agricultural_types.description, agricultural_types.img_url FROM agricultural_types WHERE agricultural_types.id = " . $this->get_given_id() . " ORDER BY agricultural_types.name;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT agricultural_types.* FROM agricultural_types ORDER BY agricultural_types.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_name(pg_result($results, $lt, 1));
        $this->set_description(pg_result($results, $lt, 2));
        $this->set_img_url(pg_result($results, $lt, 3));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_agricultural_type();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_img_url(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // skip

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // note: all varieties are public

    return $markup;
  }

  // method menu 3
  public function output_given_variables() {
    $markup = "";

    // skip

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<p><em>This is a look-up table.</em></p>\n";
    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    lands count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    # output for individual items
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $num++;

      $markup .= "<tr>\n";

      // num
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $obj->get_id() . "<br />\n";
      $markup .= "  </td>\n";

      // img_url
      $markup .= "  <td>\n";
      $markup .= "    " . $obj->get_img_url_as_img_element() . "<br />\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $url = $this->url("agricultural_types/" . $obj->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $obj->get_name() . "</a><br />\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $obj->get_description() . "<br />\n";
      $markup .= "  </td>\n";

      // lands count
      $markup .= "  <td>\n";
      include_once("lands.php");
      $land_obj = new Lands($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $lands_count = $land_obj->get_lands_count_given_agricultural_type($obj->get_id(), $user_obj);

      $markup .= "    " . $lands_count . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // column headings

    # output for individual items
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $num++;

      // num
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $obj->get_id() . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // img_url
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $obj->get_img_url_as_img_element() . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("agricultural_types/" . $obj->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $obj->get_name() . "</a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // description
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $obj->get_description() . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // lands
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    lands\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      include_once("lands.php");
      $land_obj = new Lands($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $lands_count = $land_obj->get_lands_given_agricultural_type($obj->get_id(), $user_obj);

      $markup .= "    " . $lands_count . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_img_url_as_img_element_with_name($given_user_obj) {
    $markup = "";

    $this->set_given_id($this->get_id());
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // outptu
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $obj) {
        // img_url
        $padding = "4px";
        $float = "left";
        $width = "40px";
        $markup .= "    " . $obj->get_img_as_img_element_with_link($padding, $float, $width) . "<br />\n";
        $markup .= "    " . $obj->get_name_with_link() . "<br />\n";
      }
    } else {
      $markup .= "No agricultural_types found.<br />\n";
    }

    return $markup;
  }

  // method
  public function output_preface() {

  }

}
