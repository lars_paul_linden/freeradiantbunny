<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about
// http://freeradiantbunny.org/main/en/docs/frb/guest_passes.php

include_once("lib/scrubber.php");

class GuestPasses extends Scrubber {

  // given
  private $given_guest_name;

  public function set_given_guest_name($var) {
    $this->given_guest_name = $var;
  }
  public function get_given_guest_name() {
    return $this->given_guest_name;
  }

  // attributes
  private $id;
  private $owner_name;
  private $url;
  private $guest_name;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // owner_name
  public function set_owner_name($var) {
    $this->owner_name = $var;
  }
  public function get_owner_name() {
    return $this->owner_name;
  }

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // guest_name
  public function set_guest_name($var) {
    $this->guest_name = $var;
  }
  public function get_guest_name() {
    return $this->guest_name;
  }

  // method
  private function make_guest_pass() {
    $obj = new GuestPasses($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_guest_name()) {
      $this->set_type("get_by_guest_name");

    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_guest_name") {
      $sql = "SELECT guest_passes.id, guest_passes.owner_name, guest_passes.url, guest_passes.guest_name from guest_passes WHERE guest_passes.guest_name = '" . $this->get_given_guest_name() . "';";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_guest_name") {
       for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_guest_pass();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_owner_name(pg_result($results, $lt, 1));
        $obj->set_url(pg_result($results, $lt, 2));
        $obj->set_guest_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  public function output_given_variables() {
    $markup = "";

    // todo

    return $markup;
  }

  // method
  public function get_access_flag($given_user_obj, $given_project_id, $given_class_name) {

    // debug
    //print "debug given_project_id = " . $given_project_id . "<br />\n";
    //print "debug given_class_name = " . $given_class_name . "<br />\n";

    // set
    $this->set_user_obj($given_user_obj);
    $given_user_name = $given_user_obj->name;
    $this->set_given_guest_name($given_user_name);
    //print "debug given_user_name = " . $given_user_name . "<br />\n";
    //print "debug given_guest_name = " . $this->get_given_guest_name() . "<br />\n";

    // database //
    $this->determine_type();
    $this->prepare_query();

    // initialize
    $access_flag = 0;

    // construct url
    $this_url = strtolower($given_class_name) . "/project/" . $given_project_id;
    //print "debug this_url = " . $this_url . "<br />\n";

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $guest_pass) {
        $url = $guest_pass->get_url();
        //print "debug url = " . $url . "<br />\n";
        if ($url == $this_url) {
          //print "debug guest pass FOUND<br />\n";
          $access_flag = 1;
        }
      }
    }

    return $access_flag;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // view
    $markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";

    return $markup;
  } 

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

}
