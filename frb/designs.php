<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/designs.php

include_once("lib/socation.php");

class Designs extends Socation {

  // given
  private $given_id;
  private $given_domain_tli;
  private $given_view = "online"; // default

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_domain_tli;
  public function set_given_domain_tli($given_domain_tli) {
    $this->given_domain_tli = $given_domain_tli;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attribute
  private $domain_tli;
  private $project_obj;

  // domain_tli
  public function set_domain_tli($var) {
    $this->domain_tli = $var;
  }
  public function get_domain_tli() {
    return $this->domain_tli;
  }

  // img_url
  public function get_img_url() {
  
      // todo figure out why this was not defined
      //if (! $this->img_url) {
  
      // get global variables
      $config = $this->get_given_config();
      $base_url = $config->get_base_url();

      // target examples and testing examples: 
      // todo try to a more flexible and less hard-coding; central repository
      // todo or a series of places that are checked (online, offline)
      //return $base_url . "http://basecamp/~blaireric/dev/public_html_dev/frb-clone4/freeradiantbunny-demo/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html/dash/_images/machine_stamp.png";
      // target example:
      // http://basecamp/~blaireric/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html/dash/_images/machine_stamp.png
      // target example:
      //  return "http://mudia.com/dash/_images/phd.gif";

    return "http://basecamp/~blaireric/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html/dash/_images/phd.gif";
  }

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      include_once("projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

  // method
  private function make_design() {
    $obj = new Designs($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // debug
    //print "debug designs type = " . $this->get_type() . "<br />\n";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT designs.* FROM designs WHERE designs.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT designs.* FROM designs ORDER BY designs.sort DESC, designs.name;";

      // debug
      //print "debug designs sql = " . $sql . "<br />\n";

#    } else if ($this->get_type() == "get_by_project_id") {
#      // security: only get the rows owned by the user
#      $sql = "SELECT designs.*, projects.name, projects.img_url FROM designs, #projects WHERE projects.id = " . $this->g#et_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()-#>name . "';";

      // debug
      //print "debug designs sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT designs.*, projects.name, projects.img_url FROM designs, projects WHERE designs.domain_tli = '" . $this->get_given_domain_tli() . "' AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY designs.sort DESC, designs.domain_tli, designs.name;";

      // debug
      //print "debug designs sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_row_count") {
      // todo fix this broken sql statement
      //$sql = "SELECT count(*) FROM designs WHERE projects.user_name = '" . $this->get_user_obj()->name . "';";
      // todo remove this temporary sql statement after the replacement
      $sql = "SELECT count(*) FROM designs;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $design = $this->make_design();
        $design->set_id(pg_result($results, $lt, 0));
        $design->set_name(pg_result($results, $lt, 1));
        $design->set_description(pg_result($results, $lt, 2));
        $design->set_sort(pg_result($results, $lt, 3));
        $design->set_status(pg_result($results, $lt, 4));
        $design->get_project_obj()->set_id(pg_result($results, $lt, 5));
        $design->set_domain_tli(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "get_by_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $design = $this->make_design();
        $design->set_id(pg_result($results, $lt, 0));
        $design->set_name(pg_result($results, $lt, 1));
        $design->set_description(pg_result($results, $lt, 2));
        $design->set_sort(pg_result($results, $lt, 3));
        $design->set_status(pg_result($results, $lt, 4));
        $design->get_project_obj()->set_id(pg_result($results, $lt, 5));
        $design->set_domain_tli(pg_result($results, $lt, 6));
        $design->get_project_obj()->set_name(pg_result($results, $lt, 7));
        $design->get_project_obj()->set_img_url(pg_result($results, $lt, 8));
      }
    } else if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $design = $this->make_design();
        $design->set_id(pg_result($results, $lt, 0));
        $design->set_name(pg_result($results, $lt, 1));
        $design->set_description(pg_result($results, $lt, 2));
        $design->set_sort(pg_result($results, $lt, 3));
        $design->set_status(pg_result($results, $lt, 4));
        $design->get_project_obj()->set_id(pg_result($results, $lt, 5));
        $design->set_domain_tli(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_design();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<a href=\"designs.php\" class=\"show\">All Designs</a> | ";

    $markup .= "<div class=\"subsubmenu\">\n";
    $markup .= "To project<br />\n";
    $markup .= "<br />\n";
    #if ($this->get_given_project_id()) {
    #  $url = $this->url("design_instances/project/" . $this->get_given_project_id());
    #  $markup .= "To <a href=\"" . $url . "\">design instances of this project page</a>\n";
    #}
    if ($this->get_given_id()) {
      $url = $this->url("designs");
      $markup .= "Widen view: <a href=\"" . $url . "\">All&nbsp;designs</a>\n";
    }
    $markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // only authenticated users
    if ($this->get_user_obj()) {
      //if ($this->get_user_obj()->uid) {
      //  // todo this is on hold because it takes up too much room
      //  //$markup .= "<div class=\"subsubmenu-user\">\n";
      //  //$markup .= "<strong>These design were created by</strong> " . $this->get_user_obj()->name . "<br />\n";
      //  //$markup .= "</div>\n";
      //}
    }
    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<img src=\"http://mudia.com/_images/designdoc_icon.png\" alt=\"design\" />\n";

    // heading
    // links in heading

    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    domain_tli\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    design&nbsp;instances\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $design) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // project
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $design->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $design->get_sort() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    <img src=\"http://mudia.com/_images/designdoc_icon.png\" />\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $design->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td valign=\"top\">\n";
      $markup .= "    " . $design->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td valign=\"top\">\n";
      $markup .= "    " . $design->get_domain_tli() . "\n";
      $markup .= "  </td>\n";

      // old      
      // debug just a simple link to everything
      //$url = $this->url("design_instances/design/" . $design->get_id());
      //$markup .= "    <a href=\"" . $url . "\">design instances</a>\n";

      // design instances sidecar
      include_once("design_instances.php");
      $design_instances_obj = new DesignInstances($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "  <td>\n";
      $markup .= $design_instances_obj->output_given_design_id($design->get_id(), $user_obj);
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    // name with link
    $markup .= "<p>project = " . $this->get_project_obj()->get_name_with_link() . "</p>\n";
    $markup .= "<br />\n";

    $markup .= "<h3>Design Instances</h3>\n";
    include_once("design_instances.php");
    $design_instances_obj = new DesignInstances($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $design_instances_obj->output_given_design_id($this->get_id(), $user_obj);
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function get_quick_design_instances_link() {
    $markup = "";

    include_once("design_instances.php");
    $design_instances_obj = new DesignInstances($this->get_given_config());
    $markup .= $design_instances_obj->print_simple_list($this->get_id());

    return $markup;
  }

  // method
  public function xget_project_id_given_design_id($given_design_id, $given_user_obj) {
    $markup = "";

    return  "debug designs get_project_id_given_design_id()<br />\n";

    // set
    $this->set_given_id($given_design_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      #$project_id = $obj->get_project_obj()->get_id();
      #//print "debug plant list projects = " . $project_id . "<br />\n"; 
      #$markup .= $project_id;
    }

    return $markup;
  }

  // method
  public function get_count_given_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();
    // check for errors
    if ($markup) {
      print "debug designs errors = " . $markup . "<br />\n";
      return $markup;
    }

    // only output if there is 1 item
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_img_url_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // return default image
      $markup .= "http://mudia.com/dash/_images/swept_ins_icon_small_blue.png";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) {

      // pre-process data
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= $obj->get_img_url($padding, $float, $width);
    }

    return $markup;
  }

  // method
  public function get_name_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $obj = $this->get_list_bliss()->get_first_element();
      $markup .= $obj->get_name();
    }

    return $markup;
  }

  // method
  public function get_name_with_link_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $obj = $this->get_list_bliss()->get_first_element();
      include_once("lib/factory.php");
      $factory = new Factory($this->get_given_config());
      $url = $this->url($factory->get_class_name_given_object($obj) . "/" . $obj->get_id());
      $markup .= "<a href=\"" . $url . "\" class=\"show\">";
      // replace all spaces with no-break-space entity
      $markup .= str_replace(" ", "&nbsp;", $obj->get_name());
      $markup .= "</a>";
    }

    return $markup;
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug designs: ignore, not related<br />\n";
    } else {
      $markup .= "debug designs: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function xget_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug designs: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error designs: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $design) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $design->get_id() . " " . $design->get_name_with_link() . " documentation " . $design->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $design->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error designs: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "designs: no designs found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

}
