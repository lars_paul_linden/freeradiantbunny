<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-22
// version 1.2 2015-01-21

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/calendars.php

class Calendars extends Scrubber {

  // given
  private $given_project_id;

  // given_project_id;
  public function set_given_project_id($given_project_id) {
    $this->given_project_id = $given_project_id;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $id;
  private $name;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  private function make_calendar() {
    $obj = new Calendars($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function process_command() {
    // todo no commands yet
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("parameter.php");
    
    // plant_id
    $parameter_a = new Parameter();
    $parameter_a->set_name("project_id");
    $parameter_a->set_validation_type("project_id");
    array_push($parameters, $parameter_a);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "project_id") {
            $this->set_given_project_id($parameter->get_value());
          }
          if ($parameter->get_name() == "id") {
            $this->set_given_id($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_project_id() != "") {
      $this->set_type("get_calendars_given_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_calendars_given_project_id") {
      $sql = "SELECT calendars.id, calendars.entry, calendars.project_id, projects.name FROM calendars, projects WHERE calendars.project_id = " . $this->get_given_project_id() . " AND calendars.project_id = projects.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_calendars_given_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $calendar = $this->make_calendar();
        $calendar->set_id(pg_result($results, $lt, 0));
        $calendar->set_entry(pg_result($results, $lt, 1));
        $calendar->get_project_obj()->set_id(pg_result($results, $lt, 2));
        $calendar->get_project_obj()->set_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_user_info() {
    $markup = "";

    // todo perhaps a fix for all... box should not print out when nothing in list
    // only authenticated users
    if ($this->get_user_obj()) {
      if ($this->get_user_obj()->uid) {
        $markup .= "<div class=\"subsubmenu-user\">\n";
        $markup .= "<strong>These calendars were created by</strong> " . $this->get_user_obj()->name . "<br />\n";
        $markup .= "</div>\n";
      }
    }
    return $markup;
  }

  // method
  public function output_data_set_table() {
    $markup = "";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    entry\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $calendar) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $calendar->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $calendar->get_entry() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $calendar->get_project_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    print "<br />\n";
    print "<a href=\"http://mudia.com/schemas/xml/google_gadget_spec.xml\">gadget</a></p>\n";
    print "https://www.google.com/calendar/render?gadgeturl=http://mudia.com/schemas/xml/google_gadget_spec.xml\n";
    return $markup;
  }

// todo store the frost dates in a more central user-changable location

//$last_spring_frost_month = 5;
//$last_spring_frost_date = 4;
//$first_fall_frost_month = 10;
//$first_fall_frost_date = 17;
// calculation given variables
//$markup .= "<div class=\"calculation_given_variables\">\n";
//$markup .= "<em>Given:</em><br />\n";
    //$markup .= "last_spring_frost_month = " . $last_spring_frost_month . "<br />\n";
    //$markup .= "last_spring_frost_date = " . $last_spring_frost_date . "<br />\n";
    //$markup .= "first_fall_frost_month = " . $first_fall_frost_month . "<br />\n";
    //$markup .= "first_fall_frost_date = " . $first_fall_frost_date . "<br />\n";
//$markup .= "</div><!-- calculation_given_variables -->\n";
//$markup .= "<br />\n";

// todo clean up the kruft below
//$rank_date = $plant_history_event_obj->get_multifunction_date_obj()->output_date_given($last_spring_frost_month, $last_spring_frost_date, $first_fall_frost_month, $first_fall_frost_date);

  // method
  protected function output_aggregate() {
    $markup = "";

    // view
    //$markup .= $this->output_view();
    //$markup .= $this->output_table();

    return $markup;
  }


  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

}
