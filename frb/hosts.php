<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-20
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/hosts.php

include_once("lib/sproject.php");

class Hosts extends Sproject {

  // parameters
  private $given_parent_class_name_string;
  private $given_parent_class_primary_key_string;

  // given_parent_class_name_string
  public function set_given_parent_class_name_string($var) {
    $this->given_parent_class_name_string = strtolower($var);
  }
  public function get_given_parent_class_name_string() {
    return $this->given_parent_class_name_string;
  }

  // given_parent_class_primary_key_string
  public function set_given_parent_class_primary_key_string($var) {
    $this->given_parent_class_primary_key_string = $var;
  }
  public function get_given_parent_class_primary_key_string() {
    return $this->given_parent_class_primary_key_string;
  }

  # todo clean up old attributes
  #private $domain_obj;
  #private $machine_obj;
  #private $account;
  #private $development_role;

  // attribute
  private $parent_class_name_string;
  private $parent_class_primary_key_string;
  private $child_class_name_string;
  private $child_class_primary_key_string;

  // parent_class_name_string
  public function set_parent_class_name_string($var) {
    $this->parent_class_name_string = $var;
  }
  public function get_parent_class_name_string() {
    return $this->parent_class_name_string;
  }

  // parent_class_primary_key_string
  public function set_parent_class_primary_key_string($var) {
    $this->parent_class_primary_key_string = $var;
  }
  public function get_parent_class_primary_key_string() {
    return $this->parent_class_primary_key_string;
  }

  // child_class_name_string
  public function set_child_class_name_string($var) {
    $this->child_class_name_string = $var;
  }
  public function get_child_class_name_string() {
    return $this->child_class_name_string;
  }

  // child_class_primary_key_string
  public function set_child_class_primary_key_string($var) {
    $this->child_class_primary_key_string = $var;
  }
  public function get_child_class_primary_key_string() {
    return $this->child_class_primary_key_string;
  }

  // method
  private function make_host() {
    $obj = new Hosts($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");
      
    } else if ($this->get_given_parent_class_name_string() && $this->get_given_parent_class_primary_key_string()) {
      $this->set_type("get_by_parent_class_name_and_primary_key");
      
    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND goal_statements.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";
      
    } else if ($this->get_type() == "get_all") {
      $order_by = "ORDER BY hosts.parent_class_name_string,  hosts.parent_class_primary_key_string, hosts.child_class_name_string,  hosts.child_class_primary_key_string";
      $sql = "SELECT hosts.* FROM hosts " . $order_by . ";";

    } else if ($this->get_type() == "get_by_project_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else if ($this->get_type() == "get_by_parent_class_name_and_primary_key") {
      $sql = "SELECT hosts.* FROM hosts WHERE parent_class_name_string = '" . $this->get_given_parent_class_name_string() . "' AND parent_class_primary_key_string = '" . $this->get_given_parent_class_primary_key_string() . "';";

      // debug
      //print "debug hosts sql = " . $sql . "\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }
  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_parent_class_name_and_primary_key") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_host();
        $obj->set_parent_class_name_string(pg_result($results, $lt, 0));
        $obj->set_parent_class_primary_key_string(pg_result($results, $lt, 1));
        $obj->set_child_class_name_string(pg_result($results, $lt, 2));
        $obj->set_child_class_primary_key_string(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 180px;\">\n";
    $markup .= "    parent\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 180px;\">\n";
    $markup .= "    child\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $host) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // parent_class_name_string
      $status_color = "#CCCCCC";
      $markup .= "  <td style=\"text-align: center; background-color: " . $status_color . "; font-size: 120%;\">\n";
      $markup .= "    " . $host->get_parent_class_name_string() . "\n";
      $markup .= "    " . $host->get_parent_class_primary_key_string() . "\n";
      $markup .= "  </td>\n";

      // child_class_name_string
      $status_color = "#CCCCCC";
      $markup .= "  <td style=\"text-align: center; background-color: " . $status_color . "; font-size: 120%;\">\n";
      $markup .= "    " . $host->get_child_class_name_string() . "\n";
      $markup .= "    " . $host->get_child_class_primary_key_string() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    return $markup;
  }

  // method
  public function output_preface() {
    $markup = "";

    return $markup;
  }

  // method
  public function get_count_given_machine_id($given_machine_id, $given_user_obj) {

    // set
    $this->set_given_parent_class_name_string("machines");
    $this->set_given_parent_class_primary_key_string($given_machine_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    // output count
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_hosts_given_machine_id($given_machine_id, $given_user_obj) {
   
    // set
    $this->set_given_parent_class_name_string("machines");
    $this->set_given_parent_class_primary_key_string($given_machine_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }


    if ($this->get_list_bliss()->get_count() > 0) {
        foreach ($this->get_list_bliss()->get_list() as $host) {
                $markup .= $host->get_child_class_name_string() . " ";
                $markup .= $host->get_child_class_primary_key_string() . "<br />\n";
        }
    }

    // output
    return $markup;
  }


}
