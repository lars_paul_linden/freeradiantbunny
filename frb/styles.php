<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 30-Apr-2011
// version 13-Sep-2011 moved from tenperday to free radiant bunny
// version 04-Dec-2012 created styles class file (for patagonia fandom)
// version 1.4 2015-12-20
// version 1.6 2016-05-07

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/styles.php

include_once("lib/standard.php");

class Styles extends Standard {

  // given
  private $given_id;
  private $given_sort;
  private $given_project_id;

  // given_id
  public function set_given_id($var) {
    include_once("class_validation.php");
    $variable_name = "id";
    $obj = new Validation();
    $markup = "";
    //$markup = $obj->validate_id($var, $variable_name);
    if ($markup) {
      print $markup;
    } else {
      $this->given_id = $var;
    }
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_sort
  public function set_given_sort($var) {
    include_once("class_validation.php");
    $variable_name = "sort";
    $obj = new Validation();
    $markup = "";
    //$markup = $obj->validate_sort($var, $variable_name);
    if ($markup) {
      print $markup;
    } else {
      $this->given_sort = $var;
    }
  }
  public function get_given_sort() {
    return $this->given_sort;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $id;
  private $number;
  private $description;
  private $url;
  private $season;
  private $color_id;
  private $puppy;
  private $owner;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // number
  public function set_number($var) {
    $this->number = $var;
  }
  public function get_number() {
    return $this->number;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // season
  public function set_season($var) {
    $this->season = $var;
  }
  public function get_season() {
    return $this->season;
  }

  // color_id
  public function set_color_id($var) {
    $this->color_id = $var;
  }
  public function get_color_id() {
    return $this->color_id;
  }

  // puppy
  public function set_puppy($var) {
    $this->puppy = $var;
  }
  public function get_puppy() {
    return $this->puppy;
  }

  // owner
  public function set_owner($var) {
    $this->owner = $var;
  }
  public function get_owner() {
    return $this->owner;
  }

  // method
  private function make_style() {
    $obj = new Styles($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_string()) {
      $this->set_type("get_by_string");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      $sql = "select * from styles WHERE styles.id = '" . $this->get_given_id() . "' AND owner = '" . $this->get_user_obj()->name . "';";
      // debug
      //print "debug domains: $sql\n";

    } else if ($this->get_type() == "get_all") {
      // todo the if-statement seems extra but it was not finding user_obj
      if ($this->get_user_obj() AND property_exists($this->get_user_obj(), "name")) {
        // set default
        $sort_order = "number";
        if ($this->get_given_sort()) {
          $sort_order = $this->get_given_sort();
        }
        // this is a database-drive sort
        //$sql = "select * from styles where owner = '" . $this->get_user_obj()->name . "' order by " . $sort_order . ";";
        // this is an alphabetical order sort
        $sql = "select * from styles where owner = '" . $this->get_user_obj()->name . "' order by description;";
        // debug
        //print "debug domains: sql = $sql\n";

    } else if ($this->get_type() == "get_by_string") {
      $sql = "select categories.id, categories.name FROM categories WHERE categories.name = '" . $this->get_given_string() . "' ORDER BY categories.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // todo best to clean up all of this stuff below (remember function idea)
    // define database based upon context (domain name, aka host)
    //print "debug host: " . $_SERVER['HTTP_HOST'] . "\n";
    //if (strstr($_SERVER['HTTP_HOST'], "permaculturewebsites.org")) {

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
    }
  }

  // method
  public function transfer($results) {
    $markup = "";

    if ($this->get_type() == "get_all" ||
      $this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_style();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_number(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_url(pg_result($results, $lt, 3));
        $obj->set_season(pg_result($results, $lt, 4));
        $obj->set_color_id(pg_result($results, $lt, 5));
        $obj->set_puppy(pg_result($results, $lt, 6));
        $obj->set_owner(pg_result($results, $lt, 7));
      }
    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

    return $markup;
  }

  // method
  public function output_aggregate() {
    $markup = "";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // declare variable for view
    $short = ""; # toggle true and false

    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "  #\n";
    $markup .= "  </td>\n";
  
    if ($short) {
      $markup .= "  <td class=\"header\">\n";
      $markup .= "  id\n";
      $markup .= "  </td>\n";
    }
 
    $markup .= "  <td class=\"header\" styles=\"width: 200px;\">\n";
    $markup .= "  number\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "  style to be tested in the field\n";
    if ($short) {
      //$markup .= "  description\n";
      //$markup .= "  url\n";
    }
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "  season\n";
    $markup .= "  </td>\n";
    // set up hyperlink so that user can sort rows
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("styles/");
    $parameter = "?sort=color_id";
    $markup .= "  <a href=\"" . $url . $parameter . "\">color_id</a>\n";
    $markup .= "  </td>\n";
    if ($this->get_given_sort() == "puppy") {
      $markup .= "  <td class=\"header\">\n";
      $markup .= "  puppy\n";
      $markup .= "  </td>\n";
    } 
    // digital
    // set up hyperlink so that user can sort rows
    $row_name = "puppy\n";
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("styles/");
    $parameter = "?sort=" . $row_name;
    $markup .= "  <a href=\"" . $url . $parameter . "\">" . $row_name . "</a>\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {

      $markup .= "<tr>\n";

      // number that increases as it goes along
      $num++;
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "  " . $num . "<br />\n";
      $markup .= "  </td>\n";

      if ($short) {
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $url = $this->url("styles/" . $domain->get_id());
        $markup .= "  <a href=\"" . $url . "\">" . $domain->get_id() . "</a><br />\n";
        $markup .= "  </td>\n";
      }

      // number
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "  " . $domain->get_number() . "<br />\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "  <a href=\"" . $domain->get_url() . "\">" . $domain->get_description() . "</a>\n";
      $markup .= "  </td>\n";

      // season
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "  " . $domain->get_season() . "<br />\n";
      $markup .= "  </td>\n";

      // color
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "  " . $domain->get_color_id() . "<br />\n";
      $markup .= "  </td>\n";

      //puppy_status
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "  " . $domain->get_puppy() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_single() {
    $markup = "";

    // todo move this to menu function
    $url = $this->url("styles");
    $markup .= "<p><a href=\"" . $url . "/\">all styles</a></p>";

    $markup .= "<table class=\"plants\">\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {

      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "  #\n";
      $markup .= "  </td>\n";
      $num++;
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "  " . $num . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "  id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $domain->get_id() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "  number\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $domain->get_number() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "  description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $domain->get_description() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "  url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $domain->get_url() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "  season\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $domain->get_season() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "  color_id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "  " . $domain->get_color_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "  puppy\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $domain->get_puppy() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "  owner\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $domain->get_owner() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function get_list_of_domain_tli() {
    $domain_tli_array = array();

    $this->set_type("get_all");
    $this->determine_type();
    $markup = $this->prepare_query();
    if ($markup) {
      //error
      print "error domains.php: " . $markup;
    }
    foreach ($this->get_list_bliss()->get_list() as $domain) {
      array_push($domain_tli_array, $domain->get_tli());
    }

    return $domain_tli_array;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
  
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    // todo clean up the following which was not found
    //$parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // given
  private $given_string;

  // given_string
  public function set_given_string($var) {
    $this->given_string = $var;
  }
  public function get_given_string() {
    return $this->given_string;
  }

  // attributes
  private $parent_category_obj;
  private $project_obj;
  private $permaculture_topic_obj;

  // parent_category_obj
  public function get_parent_category_obj() {
    if (! isset($this->parent_category_obj)) {
      include_once("categories.php");
      $this->parent_category_obj = new Categories($this->get_given_config());
    }
    return $this->parent_category_obj;
  }

  // permaculture_topic_obj
  public function get_permaculture_topic_obj() {
    if (! isset($this->permaculture_topic_obj)) {
      include_once("permaculture_topics.php");
      $this->permaculture_topic_obj = new PermacultureTopics($this->get_given_config());
    }
    return $this->permaculture_topic_obj;
  }

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      include_once("projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // todo figure out public and private
    //$markup .= $this->output_user_info_message();

    return $markup;
  }

}
