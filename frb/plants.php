<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plants.php

include_once("lib/scrubber.php");

class Plants extends Scrubber {

  // given
  private $given_plant_category_id;
  private $given_plant_family_id;
  private $given_variety_id;
  private $given_plant_history_id;

  // given_plant_category_id
  public function set_given_plant_category_id($var) {
    $error_message = $this->get_validator_obj()->validate_id($var, "plant_category_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_category_id = $var;
    }
  }
  public function get_given_plant_category_id() {
    return $this->given_plant_category_id;
  }

  // given_plant_family_id
  public function set_given_plant_family_id($var) {
    $this->given_plant_family_id = $var;
  }
  public function get_given_plant_family_id() {
    return $this->given_plant_family_id;
  }

  // given_variety_id
  public function set_given_variety_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "variety_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_variety_id = $var;
    }
  }
  public function get_given_variety_id() {
    return $this->given_variety_id;
  }

  // given_plant_history_id
  public function set_given_plant_history_id($var) {
    $this->given_plant_history_id = $var;
  }
  public function get_given_plant_history_id() {
    return $this->given_plant_history_id;
  }

  // attributes
  private $id;
  private $common_name;
  private $scientific_name;
  private $plant_category_obj;
  private $plant_family_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // common_name
  public function set_common_name($var) {
    $this->common_name = $var;
  }
  public function get_common_name() {
    return $this->common_name;
  }

  // method
  public function get_common_name_with_link() {
    $url = $this->url("plants/" . $this->get_id());
    return "<a href=\"" . $url ."\">" . $this->common_name . "</a>";
  }

  // scientific_name
  public function set_scientific_name($var) {
    $this->scientific_name = $var;
  }
  public function get_scientific_name() {
    return $this->scientific_name;
  }

  // plant_category_obj
  public function get_plant_category_obj() {
    if (! isset($this->plant_category_obj)) {
      include_once("plant_categories.php");
      $this->plant_category_obj = new PlantCategories($this->get_given_config());
    }
    return $this->plant_category_obj;
  }

  // plant_family_obj
  public function get_plant_family_obj() {
    if (! isset($this->plant_family_obj)) {
      include_once("plant_families.php");
      $this->plant_family_obj = new PlantFamilies($this->get_given_config());
    }
    return $this->plant_family_obj;
  }

  // method
  private function make_plant() {
    $obj = new Plants($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_category_id()) {
      $this->set_type("get_by_plant_category_id");

    } else if ($this->get_given_plant_family_id()) {
      $this->set_type("get_by_plant_family_id");

    } else if ($this->get_given_variety_id()) {
      $this->set_type("get_by_variety_id");

    } else if ($this->get_given_plant_history_id()) {
      $this->set_type("get_by_plant_history_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // note the union below is because the foreign keys may be null
      $sql = "SELECT plants.* FROM plants WHERE plants.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_by_plant_category_id") {
      $sql = "SELECT plants.id, plants.common_name, plants.scientific_name, plants.plant_category_id, plant_categories.name, plants.plant_family_id, plant_families.name FROM plants, plant_categories, plant_families WHERE plants.plant_category_id = plant_categories.id AND plant_categories.id = " . $this->get_given_plant_category_id() . " AND plants.plant_family_id = plant_families.id ORDER BY common_name;";

    } else if ($this->get_type() == "get_by_plant_family_id") {
      $sql = "SELECT plants.id, plants.common_name, plants.scientific_name, plants.plant_category_id, plant_categories.name, plants.plant_family_id, plant_families.name FROM plants, plant_categories, plant_families WHERE plants.plant_category_id = plant_categories.id AND plants.plant_family_id = " . $this->get_given_plant_family_id() . " AND plants.plant_family_id = plant_families.id ORDER BY common_name;";

    } else if ($this->get_type() == "get_by_variety_id") {
      $sql = "SELECT plants.id, plants.common_name, plants.scientific_name, plants.plant_category_id, plant_categories.name, plants.plant_family_id, plant_families.name FROM plants, plant_categories, plant_families WHERE plants.plant_category_id = plant_categories.id AND varieties.plant_id = plants.id AND varieties.id = " . $this->get_given_variety_id() . " AND plants.plant_family_id = plant_families.id ORDER BY common_name;";

    } else if ($this->get_type() == "get_by_plant_history_id") {
      $sql = "SELECT plants.* FROM plants, plant_histories, plant_list_plants WHERE plant_list_plants.plant_id = plants.id AND plant_list_plants.id = plant_histories.plant_list_plant_id AND plant_histories.id = " . $this->get_given_plant_history_id() . " ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_all") {
      // todo the following sql may be more efficient but it neglects NULLS
      // need to add select statement that gets union
      $sql = "SELECT plants.id, plants.common_name, plants.scientific_name, plants.plant_category_id, plant_categories.name, plants.plant_family_id, plant_families.name FROM plants, plant_categories, plant_families WHERE plants.plant_category_id = plant_categories.id AND plants.plant_family_id = plant_families.id UNION SELECT plants.id, plants.common_name, plants.scientific_name, plants.plant_category_id, '', plants.plant_family_id, '' FROM plants WHERE plants.plant_category_id IS NULL OR plants.plant_family_id IS NULL ORDER BY common_name;";

    // debug
    //print "debug plants.php sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_plant_category_id" ||
        $this->get_type() == "get_by_plant_family_id" ||
        $this->get_type() == "get_by_variety_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant = $this->make_plant();
        $plant->set_id(pg_result($results, $lt, 0));
        $plant->set_common_name(pg_result($results, $lt, 1));
        $plant->set_scientific_name(pg_result($results, $lt, 2));
        $plant->get_plant_category_obj()->set_id(pg_result($results, $lt, 3));
        $plant->get_plant_category_obj()->set_name(pg_result($results, $lt, 4));
        $plant->get_plant_family_obj()->set_id(pg_result($results, $lt, 5));
        $plant->get_plant_family_obj()->set_name(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "get_by_plant_history_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant = $this->make_plant();
        $plant->set_id(pg_result($results, $lt, 0));
        $plant->set_common_name(pg_result($results, $lt, 1));
        $plant->set_scientific_name(pg_result($results, $lt, 2));
        $plant->get_plant_category_obj()->set_id(pg_result($results, $lt, 3));
        $plant->get_plant_family_obj()->set_id(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->get_list_bliss()->add_item($this);
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_common_name(pg_result($results, $lt, 1));
        $this->set_scientific_name(pg_result($results, $lt, 2));
        $this->get_plant_category_obj()->set_id(pg_result($results, $lt, 3));
        $this->get_plant_family_obj()->set_id(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    if ($this->get_given_id()) {
      // navigate to aggregate from single
      $url = $this->url("plants");
      $markup .= "<strong>Widen View</strong>: <a href=\"" . $url . "\">All&nbsp;Plants</a>\n";
    } else {
      if (0) {
        // navigate by criteria
        $markup .= "<strong>Select a Plant List</strong>:<br />\n";
        $markup .= "<br />\n";

        //if ($this->get_given_plant_category_id() == "" &&
          //$this->get_given_plant_family_id() == "") {
          //$markup .= "all<br />\n";
        //} else {
        //  $url = $this->url("plants");
        //  $markup .= "<a href=\"" . $url . "\">all</a><br />\n";
        //}
        $markup .= "<br />\n";
      }

      if (1) {
        $markup .= "<strong>Plant Categories</strong>: \n";
        if ($this->get_given_plant_category_id() == "1") {
          $markup .= "vegetables \n";
        } else {
          $url = $this->url("plants/plant_categories/1");
          $markup .= "<a href=\"" . $url . "\">vegetables</a> \n";
        }
        if ($this->get_given_plant_category_id() == "2") {
          $markup .= "herbs \n";
        } else {
          $url = $this->url("plants/plant_categories/2");
          $markup .= "<a href=\"" . $url . "\">herbs</a> \n";
        }
        if ($this->get_given_plant_category_id() == "3") {
          $markup .= "flowers \n";
        } else {
          $url = $this->url("plants/plant_categories/3");
          $markup .= "<a href=\"" . $url . "\">flowers</a> \n";
        }
        if ($this->get_given_plant_category_id() == "4") {
          $markup .= "fruits \n";
        } else {
          $url = $this->url("plants/plant_categories/4");
          $markup .= "<a href=\"" . $url . "\">fruits</a> \n";
        }
        if ($this->get_given_plant_category_id() == "5") {
          $markup .= "trees \n";
        } else {
          $url = $this->url("plants/plant_categories/5");
          $markup .= "<a href=\"" . $url . "\">trees</a> \n";
        }
        if (! $this->get_given_plant_category_id()) {
          $markup .= "all \n";
        } else {
          $url = $this->url("plants");
          $markup .= "<a href=\"" . $url . "\">all</a> \n";
        }
        $markup .= "<br />\n";
      }

      // display given_plant_family_id
      if ($this->get_given_plant_family_id()) {
        $markup .= "<strong>Plant Family</strong>: \n";
        $markup .= $this->get_given_plant_family_id() . "\n";
      }

      // get data
      //$plant_families_obj = $this->get_plant_family_obj();
      //$markup .= $plant_families_obj->output_list_with_heading($this->get_given_plant_family_id());

    }
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo should the plant_lists be shown on the plant page?
    // only authenticated users
    // if ($this->get_user_obj() && $this->get_user_obj()->uid) {
    //   $markup .= "<div class=\"subsubmenu-user\">\n";
    //   include_once("plant_lists.php");
    //   $plant_lists_obj = new PlantLists($this->get_given_config());
    //   $markup .= $plant_lists_obj->output_submenu_user_plant_lists("", $this->get_user_obj());
    //   $markup .= "</div><!-- end subsubmenu-user -->\n";
    // }

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    $markup .= "<div class=\"given-variables\"><em>\n";
    if ($this->get_type() == "get_by_id") {
      $markup .= "This is plant id <strong>" . $this->get_given_id() . "</strong>.\n";

    } else if ($this->get_type() == "get_by_plant_category_id") {
      $markup .= "Only plants that are in the <strong>" . $this->get_plant_category_obj()->get_plant_category_name_given_id($this->get_given_plant_category_id()) . "</strong> plant category are listed.\n";

    } else if ($this->get_type() == "get_by_plant_family_id") {
      // todo clean up old message that output the name
      //$markup .= "Only plants that are in the <strong>" . $this->get_plant_family_obj()->get_family_name_given_id($this->get_given_plant_family_id()) . "</strong> family are listed.\n";
      // output id
      $markup .= "Only plants that are in plant_family_id = <strong>" . $this->get_given_plant_family_id() . "</strong> are listed.\n";

    } else if ($this->get_type() == "get_by_varieties_id") {
      $markup .= "Only plants that are variety id  <strong>" . $this->get_given_variety_id() . "</strong> are listed.\n";

    } else if ($this->get_type() == "get_all") {
      $markup .= "All plants are listed.\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to display \"given\" variables.");

    }
    $markup .= "</em></div><!-- given-variables -->\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    common name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    category\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("plant_families");
    $markup .= "    <a href=\"" . $url . "\" class=\"noshow\">plant_families</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_lists count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $plant) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- id=" . $plant->get_id() . " -->\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $url = $this->url("plants/plant_categories/" . $plant->get_plant_category_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\" class=\"noshow\">" . $plant->get_plant_category_obj()->get_name() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      if ($plant->get_plant_family_obj()->get_name() == "unknown") {
        // skip
      } else {
        $url = $this->url("plants/family/" . $plant->get_plant_family_obj()->get_id());
        $markup .= "    <a href=\"" . $url . "\" class=\"noshow\">" . $plant->get_plant_family_obj()->get_name() . "</a>\n";
      }
      $markup .= "  </td>\n";

      // plant_lists count
      // this plant is in this number of plant_lists
      $markup .= "  <td>\n";
      include_once("plant_list_plants.php");
      $plant_list_plant_obj = new PlantListPlants($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_list_count = $plant_list_plant_obj->get_count_in_given_plant_id($plant->get_id(), $user_obj);
      $markup .= "    ";
      if ($plant_list_count) {
        $url = $this->url("plant_lists/plant/" . $plant->get_id());
        $markup .= "<a href=\"" . $url . "\">\n";
      }
      $markup .= $plant_list_count;
      if ($plant_list_count) {
        $markup .= "</a>\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // todo move styles to CSS
    $markup .= "<h2 style=\"font-size: 260%; letter-spacing: 1px;\">" . $this->get_common_name() . "</h2>\n";

    // scientific name
    if ($this->get_scientific_name()) {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\"><em>" . $this->get_scientific_name() . "</em></p>\n";
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\">[scientific name not known.]</p>\n";
    }

    // plant_category
    if ($this->get_plant_category_obj()->get_id()) {
      include_once("plant_categories.php");
      $plant_category_obj = new PlantCategories($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_category_data = $plant_category_obj->get_plant_category_name_given_id($this->get_plant_category_obj()->get_id(), $user_obj);
      if ($plant_category_data) {
        $url = $this->url("plant_categories/" . $this->get_plant_category_obj()->get_id());
        $markup .= "<p>plant category = <strong><a href=\"" . $url . "\">" . $plant_category_data . "</a></strong></p>\n";
        
      } else {
        $markup .= "<p>plant category = <strong>" . $this->get_plant_category_obj()->get_id() . "</strong></p>\n";
      }
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\">[plant category name not known.]</p>\n";
    }

    // plant_family
    if ($this->get_plant_family_obj()->get_id() && $this->get_plant_family_obj()->get_name() != "unknown") {
      include_once("plant_families.php");
      $plant_family_obj = new PlantFamilies($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_family_data = $plant_family_obj->get_plant_family_name_given_id($this->get_plant_family_obj()->get_id(), $user_obj);
      if ($plant_family_data) {
        $url = $this->url("plant_families/" . $this->get_plant_family_obj()->get_id());
        $markup .= "<p>family name = <strong><a href=\"" . $url . "\"><em>" . $plant_family_data . "</em></a></strong></p>\n";
      } else {
        $url = $this->url("plant_families/" . $this->get_plant_families_obj()->get_id());
        $markup .= "<p>family name = <strong><a href=\"" . $url . "\"><em>" . $this->get_plant_family_obj()->get_id() . "</em></a></strong></p>\n";
      }
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\">[plant family not known.]</p>\n";
    }

    // varieties
    include_once("varieties.php");
    $varieties_obj = new Varieties($this->get_given_config());
    // todo declaring this variable has a cost so what if object formats cell
    // todo would change the whole application object formats per context
    $varieties_data = $varieties_obj->output_sidecar($this->get_id());
    if ($varieties_data) {
      $url = $this->url("varieties/plants/" . $this->get_id());
      $markup .= "<p><a href=\"" . $url . "\">varieties</a>:<br />" . $varieties_data . "</p>\n";
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 100%; letter-spacing: 1px;\">No varieties.</p><br />\n";
    }

    // plant_aliases
    include_once("plant_aliases.php");
    $plant_alias_obj = new PlantAliases($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $plant_alias_data = $plant_alias_obj->output_sidecar($this->get_id(), $user_obj);
    if ($plant_alias_data) {
      $markup .= $plant_alias_data;
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 100%; letter-spacing: 1px;\">No plant_aliases.</p><br />\n";
    }

    // image
    include_once("object_images.php");
    $object_image_obj = new ObjectImages($this->get_given_config());
    $show_caption = "";
    $object_images_data = $object_image_obj->output_images("plants", $this->get_id(), $show_caption);
    if ($object_images_data) {
      $markup .= $object_images_data;
      // todo clean up breaks to css
      $markup .= "<br />\n";
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\">[object_images not known.]</p>\n";
    }

    // plant_lists
    include_once("plant_lists.php");
    $plant_list_obj = new PlantLists($this->get_given_config());
    $plant_list_data = $plant_list_obj->output_plant_lists_given_plant_id($this->get_id());
    if ($plant_list_data) {
      $markup .= $plant_list_data;
      // todo clean up breaks to css
      $markup .= "<br />\n";
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\">[plant lists not known.]</p>\n";
    }

    // hyperlinks
    include_once("hyperlinks.php");
    $hyperlink_obj = new Hyperlinks($this->get_given_config());
    $hyperlinks_data = $hyperlink_obj->output_sidecar_given_class_name_and_id("plants", $this->get_id());
    if ($hyperlinks_data) {
      $markup .= $hyperlinks_data . "<br /><br />\n";
    } else {
      $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 100%; letter-spacing: 1px;\">No hyperlinks.</p>\n";
    }

    // plant_attributes
    $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\">[plant_attributes not coded yet.]</p><br />\n";
    
    // plant_units
    $markup .= "<p style=\"padding: 0px; margin: 0px; font-size: 140%; letter-spacing: 1px;\">[plant_units not coded yet.]</p><br />\n";
    
    return $markup;
  }

  //  method
  public function get_name_with_link_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "no plant";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $markup .= $this->get_list_bliss()->get_count() . " plants";
      return $markup;
    }

    return $this->get_list_bliss()->get_first_element()->get_common_name_with_link();
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_plant_given_plant_history_id($given_plant_history_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_history_id($given_plant_history_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }   

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $plant) {
        $markup .= $plant->get_common_name_with_link() . "<br />\n";
      }
    }

    return $markup;
  }

  // method
  protected function output_preface() {
    $markup = "";
    return $markup;
  }

}
