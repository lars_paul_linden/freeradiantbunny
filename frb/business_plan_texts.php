<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-17
// version 1.2 2015-01-04
// version 1.4 2015-06-19
// version 1.5 2015-10-16
// version 1.6 2016-03-25

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/business_plan_texts.php

include_once("lib/standard.php");

class BusinessPlanTexts extends Standard {

  // given
  private $given_goal_statement_id;

  // given_goal_statement_id;
  public function set_given_goal_statement_id($given_goal_statement_id) {
    $this->given_goal_statement_id = $given_goal_statement_id;
  }
  public function get_given_goal_statement_id() {
    return $this->given_goal_statement_id;
  }

  // attributes
  private $goal_statement_obj;
  private $order;

  // goal_statement_obj
  public function get_goal_statement_obj() {
    if (! isset($this->goal_statement_obj)) {
      include_once("goal_statements.php");
      $this->goal_statement_obj = new GoalStatements($this->get_given_config());
    }
    return $this->goal_statement_obj;
  }

  // order
  public function set_order($var) {
    $this->order = $var;
  }
  public function get_order() {
    return $this->order;
  }

  // img_url
  // todo this was used to over-ride (how would I know to do that? docs?)
  // todo this is not working (it should be used only if table img_url is null
  //public function get_img_url_default() {
  //  return "http://mudia.com/dash/_images/business_plan_stamp.png";
  //}

  // method
  private function make_business_plan_text() {
    $obj = new BusinessPlanTexts($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function process_command() {
    // todo no commands yet
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // id
    $parameter_a = new Parameter();
    $parameter_a->set_name("id");
    $parameter_a->set_validation_type_as_id();
    array_push($parameters, $parameter_a);

    // project_id
    $parameter_b = new Parameter();
    $parameter_b->set_name("project_id");
    // todo this seems odd why id and not project_id
    $parameter_b->set_validation_type_as_id();
    array_push($parameters, $parameter_b);

    // view
    $parameter_c= new Parameter();
    $parameter_c->set_name("view");
    $parameter_c->set_validation_type_as_view();
    array_push($parameters, $parameter_c);

    // make-sort-today
    $parameter_d = new Parameter();
    $parameter_d->set_name("make-sort-today");
    $parameter_d->set_validation_type_as_id();
    array_push($parameters, $parameter_d);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "id") {
            $this->set_given_id($parameter->get_value());
          }
          if ($parameter->get_name() == "project_id") {
            $this->set_given_project_id($parameter->get_value());
          }
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
            // debug
            //print "debug business_plan_texts view = " . $this->get_given_view() . "<br />\n";
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "business_plan_texts";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $business_plan_text_obj = new BusinessPlanTexts($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $business_plan_text_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }
    return $markup;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_goal_statement_id()) {
      $this->set_type("get_by_goal_statement_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // username_sql
    // this insures that a user may only get their own rows
    $username_sql = " projects.user_name = '" . $this->get_user_obj()->name . "' ";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM business_plan_texts, goal_statements, projects WHERE business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND business_plan_texts.id = " . $this->get_given_id() . " AND " . $username_sql . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user

      if ($this->get_given_view() == "all") {
        // debug
        //print "debug business_plan_texts all<br />\n";
        $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM business_plan_texts, goal_statements, projects WHERE business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND " . $username_sql . " ORDER BY business_plan_texts.order, business_plan_texts.sort DESC, business_plan_texts.name;";
      } else if ($this->get_given_view() == "offline") {
        // debug
        //print "debug business_plan_texts offline<br />\n";
        $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM business_plan_texts, goal_statements, projects WHERE business_plan_texts.status = 'offline' AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND " . $username_sql . " ORDER BY business_plan_texts.order, business_plan_texts.sort DESC, business_plan_texts.name;";
      } else if ($this->get_given_view() == "zoneline") {
        // todo zoneline refactor for view sort filter
        $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM business_plan_texts, goal_statements, projects WHERE business_plan_texts.status = 'zoneline' AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND " . $username_sql . " ORDER BY projects.name, business_plan_texts.order, business_plan_texts.sort DESC, business_plan_texts.name;";
      } else {
        // debug
        //print "debug business_plan_texts online<br />\n";
        // online
        $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM business_plan_texts, goal_statements, projects WHERE projects.status != 'offline' AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND " . $username_sql . " ORDER BY business_plan_texts.order, business_plan_texts.sort DESC, business_plan_texts.name;";
      }

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM business_plan_texts, goal_statements, projects WHERE business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND " . $username_sql . " ORDER BY business_plan_texts.order, business_plan_texts.sort DESC, goal_statements.status, business_plan_texts.status, business_plan_texts.name;";

    } else if ($this->get_type() == "get_by_goal_statement_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM business_plan_texts, goal_statements, projects WHERE business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND business_plan_texts.goal_statement_id = " . $this->get_given_goal_statement_id() . " AND " . $username_sql . " ORDER BY business_plan_texts.order, business_plan_texts.status, business_plan_texts.sort DESC, business_plan_texts.name;";

    } else if ($this->get_type() == "check_relationships") {
      // security: only get the rows owned by the user
      $sql = "SELECT business_plan_texts.*, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url, goal_statements.id, goal_statements.name FROM business_plan_texts, projects, goal_statements WHERE business_plan_texts.goal_statement_id = goal_statements.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND goal_statements.project_id != projects.id AND " . $username_sql . " ORDER BY business_plan_texts.status, business_plan_texts.order, business_plan_texts.sort DESC, business_plan_texts.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_goal_statement_id" ||
        $this->get_type() == "check_relationships") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_business_plan_text();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_description(pg_result($results, $lt, 1));
        $obj->set_name(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->set_img_url(pg_result($results, $lt, 5));
        $obj->get_goal_statement_obj()->set_id(pg_result($results, $lt, 6));
        $obj->set_order(pg_result($results, $lt, 7));
        $obj->get_goal_statement_obj()->set_name(pg_result($results, $lt, 8));
        $obj->get_goal_statement_obj()->set_img_url(pg_result($results, $lt, 9));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 10));
        $obj->get_project_obj()->set_name(pg_result($results, $lt, 11));
        $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 12));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_business_plan_text();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->print_error("Sorry, business_plans does not know the type.");
    }

  }

  // method menu 4
  public function output_user_info() {
    $markup = "";

    // only authenticated users
    //if ($this->get_user_obj()) {
    //  if ($this->get_user_obj()->uid) {
    //    $markup .= "<div class=\"subsubmenu-user\">\n";
    //    if ($this->get_given_id()) {
    //      $markup .= "<strong>This business plan was created by</strong> " . $this->get_user_obj()->name . "<br />\n";
    //    } else {
    //      $markup .= "<strong>These " . get_class($this) . " were written by</strong> " . $this->get_user_obj()->name . "<br />\n";
    //    }
    //    $markup .= "</div>\n";
    //  }
    //}

    return $markup;
  }

  // method
  public function get_business_plan_texts_count() {
    $message = array("", "");

    $markup = $this->load_data();

    if ($markup) {
      $message[0] = "error";
      $message[1] = $markup;
    } else {
      $message[0] = "";
      $message[1] = $this->get_list_bliss()->get_count();
    }

    return $message;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";

    // check for warnings
    // check integrity of database
    if ($this->get_given_project_id()) {
      $markup .= "<br />\n";
      $user_obj = $this->get_user_obj();
      $markup .= $this->check_relationships($this->get_given_project_id(), $user_obj) . "\n";
      //$markup .= "<br />\n";
    }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 100px;\">\n";
    $markup .= "    <a href=\"goal_statements\">goal statements</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"background-color: #EF9900;\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";   if ($this->get_given_project_id()) {
      $project_id = $this->get_given_project_id(); 
      $url = $this->url("processes/projects/" . $project_id);
    } else {
      $url = $this->url("processes/");
    }
    $markup .= "<a href=\"" . $url . "\">processes</a>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $business_plan_text) {
      $num++;

      $markup .= "<tr>\n";

      // num
      $markup .= "  <td valign=\"top\">\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      // project
      $markup .= "  <td valign=\"top\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $business_plan_text->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      // debug below
      //$markup .= "    " . $business_plan_text->get_project_obj()->get_img_url() . "\n";
      $markup .= "  </td>\n";

      // goal_statements
      $markup .= "  <td class=\"header\" style=\"width: 100px;\">\n";
      $markup .= "    " . $business_plan_text->get_goal_statement_obj()->get_name_with_link() . "\n";
      // todo clean up the old way below
      //$business_plan_text_id = $business_plan_text->get_id();
      //$markup .= "    " . $goal_bpts_obj->get_bpts_given_business_plan_text_id($business_plan_text_id, $user_obj) . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $business_plan_text->get_sort_cell();

      // id
      $markup .= "  <td valign=\"top\">\n";
      $markup .= "    " . $business_plan_text->get_id_with_link() . "<br />\n";
      $markup .= "  </td>\n";

      // img
      $markup .= "  <td valign=\"top\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $business_plan_text->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td valign=\"top\">\n";
      $business_plan_text->set_given_project_id($this->get_given_project_id());
      $markup .= "    <p>" . $business_plan_text->get_name_with_link() . "</p>\n";
      $markup .= "  </td>\n";

      // description
      //$markup .= "  <td valign=\"top\">\n";
      //$markup .= "    " . $business_plan_text->get_description() . "\n";
      //$markup .= "    <div style=\"float: right;\"><em>sort = " . $business_plan_text->get_sort() . "</em></div>\n";
      //$markup .= "  </td>\n";

      // status
      if ($this->get_given_view() == "zoneline") {
        $markup .= "  <td valign=\"top\" style=\"background-color: #C0509F;\">\n";
      } else {
        $markup .= "  <td valign=\"top\" style=\"background-color: #EF9900;\">\n";
      }
      $markup .= "    " . $business_plan_text->get_status() . "<br />\n";
      $markup .= "  </td>\n";

      // processes
      $markup .= "  <td style=\"vertical-align: top;\">\n";
      include_once("processes.php");
      $process_obj = new Processes($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $business_plan_text_id = $business_plan_text->get_id();
      $markup .= "    " . $process_obj->get_processes_given_business_plan_text_id($business_plan_text_id, $user_obj) . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function display_processes_table_simple($given_bpt_id, $given_user_obj, $display_stats_toggle = "") {
    $markup = "";

    include_once("processes.php");
    $process_obj = new Processes($this->get_given_config());
    $markup .= $process_obj->get_processes_table_list_given_bpt_id($given_user_obj, $given_bpt_id, $display_stats_toggle);

    return $markup;
  }

  // method
  public function get_project_id_given_business_plan_text_id($given_business_plan_text_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_business_plan_text_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_project_obj()->get_id();
      $markup .= $project_id;
    }

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    // goal_statement_id
    $markup .= "<p>goal_statement_id = " . $this->get_goal_statement_obj()->get_id() . "</p>\n";
    
    // order
    $markup .= "<p>order = " . $this->get_order() . "</p>\n";
    
    // 
    $style_flag = "standard_flag";
    if ($this->get_given_view() == "printable") {
      // sidecar (hint reference style)
      // note intended to be a quick "hint reference" as to what is next
      $style_flag = "hint_flag";
    }

    if ($this->get_given_view() == "printable") {
      //skip
    } else {
      $markup .= "<div style=\"border: 2px solid; border-radius:25px; background-color: #EFEFEF; padding: 6px 6px 6px 20px; margin: 20px 0px 20px 0px;\">\n";
      $markup .= "<h3 style=\"border: 1px dashed; border-radius:25px; background-color: DarkMagenta; color: LightBlue; padding: 4px 4px 4px 8px;\">Nonstandard</h3>\n";
    }

    // processes
    include_once("processes.php");
    $process_obj = new Processes($this->get_given_config());
    $user_obj = $this->get_user_obj(); 
    $bpt_id = $this->get_id();
    $display_stats_toggle = "";
    $markup .= $process_obj->get_processes_table_given_bpt_id($user_obj, $bpt_id, $style_flag, $display_stats_toggle);

    if ($this->get_given_view() == "printable") {
      //skip
    } else {
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_preface_single() {
    $markup = "";

    // associated
    // name with link
    $padding = "";
    $float = "";
    $width = "32";
    $markup .= $this->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width);
    $markup .= " ";
    $markup .= $this->get_project_obj()->get_id();
    $markup .= " ";
    $markup .= $this->get_project_obj()->get_name() . "<br />\n";

    // associated
    // list related goal_statemensts
    // todo figure out where this next line goes, it is lost
    //$heading = "true";
    $padding = "";
    $float = "";
    $width = "32";
    $markup .= $this->get_goal_statement_obj()->get_img_as_img_element_with_link($padding, $float, $width);
    $markup .= " ";
    $markup .= $this->get_goal_statement_obj()->get_id() . " ";
    // todo note sure that the following object needs to be called
    include_once("goal_statements.php");
    $goal_statement_obj = new GoalStatements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $bpt_id = $this->get_id();
    $markup .= $goal_statement_obj->get_goal_statement_given_bpt_id($bpt_id, $user_obj);
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  public function output_single_associated() {
    $markup = "";

    // name with link
    $markup .= "<p>project = " . $this->get_project_obj()->get_name_with_link() . "</p>\n";
    $markup .= "<br />\n";

    // more
    $markup .= "<h3>Processes</h3>\n";
    $given_bpt_id = $this->get_id();
    $markup .= $this->display_processes_table($given_bpt_id);
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function get_name_and_link_given_id($given_given_business_plan_text_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_given_business_plan_text_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug business_plan_texts given_given_bpt_id = " . $this->get_given_id() . "<br />\n";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there is 1 item
    if ($this->get_list_bliss()->get_count() > 0) {
      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_name_with_link();
    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": expect 1 count and got a count = " . $this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  private function check_relationships($given_project_id, $given_user_obj) {
    $markup = "";

    // get instances where business_plan_texts are related to goal_statements

    // do all of this in an independent instance
    $obj = new BusinessPlanTexts($this->get_given_config());

    // set
    $obj->set_given_project_id($given_project_id);
    $obj->set_user_obj($given_user_obj);

    // load data from database
    $obj->set_type("check_relationships");
    $markup .= $obj->prepare_query();

    // only output if there are items to output
    if ($obj->get_list_bliss()->get_count() > 0) {
      $markup .= "<div style=\"background-color: red;\">\n";
      $markup .= "<h3>BusinessPlanTexts with Extra Relationships</h3>\n";
      foreach ($obj->get_list_bliss()->get_list() as $business_plan_text) {
        $markup .= "goal_statement = " . $business_plan_text->get_goal_statement_obj()->get_name_with_link() . " &amp; ";
        $markup .= "business_plan_text = " . $business_plan_text->get_name_with_link() . "<br />\n";
      }
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_build_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // todo remove the following kludge and learn how to write SQL
    //$this->remove_duplicates();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no business_plan_texts were found.</p>\n";;
      return $markup;
    }

    $markup .= "<h3 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px;\">business plan texts</h3>\n";

    // rows
    $num = 0;
    $total_count = $this->get_list_bliss()->get_count();
    foreach ($this->get_list_bliss()->get_list() as $business_plan_text) {
      $num++;

      // add space for breathing room
      $markup .= "<br />\n";
      $markup .= "#" . $num . " of " . $total_count . " business&nbsp;plan&nbsp;texts<br />\n";

      // img
      $padding = "";
      $float = "left";
      $width = "65";
      $markup .= "    " . $business_plan_text->get_img_as_img_element_with_link($padding, $float, $width);

      // id
      $markup .= "  <br />";
      $markup .= "  <em>This is business plan texts id " . $business_plan_text->get_id() . "</em><br />\n";

      // name
      $business_plan_text->set_given_project_id($this->get_given_project_id());
      $markup .= "    <h2 style=\"background-color: #EFEFEF; padding: 3px;\">" . $business_plan_text->get_name() . "</h2>\n";

      // description
      if ($business_plan_text->get_description() == "pushed") {
        $markup .= "  <p style=\"background-color: #343434;\">";
      } else {
        $markup .= "  <p>";
      }
      $markup .= $business_plan_text->get_description() . "</p>\n";

      // processes
      //$markup .= "<br />\n";
      $markup .= "  <h3>processes of this business plan text</h3>\n";
      $markup .= "  <p>\n";
      $project_id = $business_plan_text->get_project_obj()->get_id();
      $given_bpt_id = $business_plan_text->get_id();
      $markup .= "    " . $business_plan_text->display_processes_table_simple($given_bpt_id, $given_user_obj, "do-not-display-stats") . "\n";
      $markup .= "  </p>\n";
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  public function remove_duplicates() {
    
    $this->get_list_bliss()->make_array_unique();

  }

  // method
  public function get_sidecar_given_goal_statement_id($given_goal_statement_id, $given_user_obj, $style_flag = "", $given_heading = "") {
    $markup = "";

    $this->set_given_goal_statement_id($given_goal_statement_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    $plural_string = "";
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">No business_plan_texts were found.</p>\n";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() == 1) {
      $plural_string = "";
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $plural_string = "s";
    }

    if ($given_heading) {
      if ($style_flag == "standard_flag") {
        // todo this string is based upon class name and should be refactored
        // todo this string should be created dynamically and in parent class
        $markup .= "<h3>business_plan_texts</h3>\n";
      } else if ($style_flag == "hint_flag") {
        $markup .= "<p>This goal_statement has the following business_plan_text" . $plural_string . ":</p>\n";
      }
    }

    if ($style_flag == "" || $style_flag == "standard_flag") {
      // guts of the list in the form of a table
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";

      // column headings
      $markup .= "  <td class=\"header\" style=\"\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 110px;\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 110px;\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $business_plan_text) {
        $num++;

        $markup .= "<tr>\n";

        // num
        $markup .= "  <td valign=\"top\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        // status
        $markup .= "  <td style=\"background-color: " . $business_plan_text->get_status_background_color() . "\">\n";
        if ($this->get_user_obj()) {
          $user_obj = $this->get_user_obj();
          $business_plan_text->set_user_obj($user_obj);
          $markup .= "    " . $business_plan_text->get_status() . "\n";
        } else {
          $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": user is not known in <em>business_plan_text</em> context.");
        }
        $markup .= "  </td>\n";

        // sort
        $markup .= $business_plan_text->get_sort_cell();

        // id
        $markup .= "  <td valign=\"top\">\n";
        $markup .= "    " . $business_plan_text->get_id_with_link() . "<br />\n";
        $markup .= "  </td>\n";

        // name
        $markup .= "  <td valign=\"top\">\n";
        $business_plan_text->set_given_project_id($this->get_given_project_id());
        $markup .= "    " . $business_plan_text->get_name_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
    } else if ($style_flag == "hint_flag") {
      // guts of the list in the form of a table
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";

      // column headings
      $markup .= "  <td class=\"header\" style=\"\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $business_plan_text) {
        $num++;

        $markup .= "<tr>\n";

        // num
        $markup .= "  <td valign=\"top\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td valign=\"top\">\n";
        $markup .= "    " . $business_plan_text->get_id_with_link() . "<br />\n";
        $markup .= "  </td>\n";

        // name
        $markup .= "  <td valign=\"top\">\n";
        $business_plan_text->set_given_project_id($this->get_given_project_id());
        $markup .= "    " . $business_plan_text->get_name_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
    }

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    $markup .= "<p>view: ";
    $views = array("online", "all", "offline", "printable", "zoneline");
    $last_pos = count($views) - 1;
    foreach ($views as $view) {
      $markup .= $this->get_menu_item($view, "view");
      if ($view != $views[$last_pos]) {
        $markup .= " | ";
      }
    }
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT business_plan_texts.id, business_plan_texts.name FROM business_plan_texts WHERE business_plan_texts.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug business_plan_texts: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "BusinessPlanTexts ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug business_plan_texts: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error business_plan_texts: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $business_plan_text) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $business_plan_text->get_id() . " " . $business_plan_text->get_name_with_link() . " documentation " . $business_plan_text->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $business_plan_text->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error business_plan_texts: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "business_plan_texts: no business_plan_texts found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

  // method
  public function get_three_part_given_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $results = $this->prepare_query();

    $padding = "";
    $float = "";
    $width = "32";

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $business_plan_text) {
        $markup .= $business_plan_text->get_img_as_img_element_with_link($padding, $float, $width);
        $markup .= " " . $business_plan_text->get_id() . "\n";
        $markup .= " " . $business_plan_text->get_name() . "\n";
      }
    }

    return $markup;
  }

}
