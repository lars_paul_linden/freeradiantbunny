<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-21
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/postings.php

include_once("lib/standard.php");

class Postings extends Standard {

  // given
  private $given_budget_id;
  private $given_scene_element_id;
  private $given_account_id;

  // given_budget_id
  public function set_given_budget_id($budget_id) {
    $this->given_budget_id = $budget_id;
  }
  public function get_given_budget_id() {
    return $this->given_budget_id;
  }

  // given_scene_element_id
  public function set_given_scene_element_id($var) {
    $this->given_scene_element_id = $var;
  }
  public function get_given_scene_element_id() {
    return $this->given_scene_element_id;
  }
 
  // given_account_id
  public function set_given_account_id($account_id) {
    $this->given_account_id = $account_id;
  }
  public function get_given_account_id() {
    return $this->given_account_id;
  }

  // attributes
  private $transaction_date;
  private $account_obj;
  private $journal_obj;
  private $asset_type_obj;
  private $amount;
  private $budget_obj;
  private $scene_element_obj;
  private $transfer_account_obj;
  private $supplier_obj;
  private $due_date;
 
  // account_obj
  public function get_account_obj() {
    if (! isset($this->account_obj)) {
      include_once("accounts.php");
      $this->account_obj = new Accounts($this->get_given_config());
    }
    return $this->account_obj;
  }

  // journal_obj
  public function get_journal_obj() {
    if (! isset($this->journal_obj)) {
      include_once("journals.php");
      $this->journal_obj = new Journals($this->get_given_config());
    }
    return $this->journal_obj;
  }

  // asset_type_obj
  public function get_asset_type_obj() {
    if (! isset($this->asset_type_obj)) {
      include_once("asset_types.php");
      $this->asset_type_obj = new AssetTypes($this->get_given_config());
    }
    return $this->asset_type_obj;
  }

  // amount
  public function set_amount($var) {
    $this->amount = $var;
  }
  public function get_amount() {
    return $this->amount;
  }

  // budget_obj
  public function get_budget_obj() {
    if (! isset($this->budget_obj)) {
      include_once("budgets.php");
      $this->budget_obj = new Budgets($this->get_given_config());
    }
    return $this->budget_obj;
  }

  // scene_element_obj
  public function get_scene_element_obj() {
    if (! isset($this->scene_element_obj)) {
      include_once("scene_elements.php");
      $this->scene_element_obj = new SceneElements($this->get_given_config());
    }
    return $this->scene_element_obj;
  }

  // transaction_date
  public function set_transaction_date($var) {
    $this->transaction_date = $var;
  }
  public function get_transaction_date() {
    return $this->transaction_date;
  }

  // transfer_account_obj
  public function get_transfer_account_obj() {
    if (! isset($this->transfer_account_obj)) {
      include_once("accounts.php");
      $this->transfer_account_obj = new Accounts($this->get_given_config());
    }
    return $this->transfer_account_obj;
  }

  // supplier_obj
  public function get_supplier_obj() {
    if (! isset($this->supplier_obj)) {
      include_once("suppliers.php");
      $this->supplier_obj = new Suppliers($this->get_given_config());
    }
    return $this->supplier_obj;
  }

  // due_date
  public function set_due_date($var) {
    $this->due_date = $var;
  }
  public function get_due_date() {
    return $this->due_date;
  }

  // img_url
  public function get_img_url() {
    return "http://mudia.com/dash/_images/posting_lamp_logo.png";
  }

  // method
  private function make_posting() {
    $obj = new Postings($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_budget_id()) {
      $this->set_type("get_by_budget_id");

    } else if ($this->get_given_scene_element_id() != "") {
      $this->set_type("get_by_scene_element_id");

    } else if ($this->get_given_account_id()) {
      $this->set_type("get_by_account_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT postings.*, budgets.name, projects.id, projects.name, projects.img_url FROM postings, accounts, budgets, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE postings.budget_id = budgets.id AND budgets.scene_element_id = scene_elements.id AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id  AND postings.id = " . $this->get_given_id() . " AND postings.account_id = accounts.id  AND projects.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug postings sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT postings.*, budgets.name, projects.id, projects.name, projects.img_url FROM postings, accounts, budgets, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE postings.budget_id = budgets.id AND budgets.scene_element_id = scene_elements.id AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND postings.account_id = accounts.id  AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY postings.transaction_date, postings.supplier_id, accounts.name;";

    } else if ($this->get_type() == "get_by_budget_id") {
      // debug
      //print "debug postings hard-coded for postings from 2015 range only<br />\n";
      $sql = "SELECT postings.*, budgets.name, projects.id, projects.name, projects.img_url FROM postings, accounts, budgets, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE postings.budget_id = budgets.id AND budgets.scene_element_id = scene_elements.id AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id  AND budgets.id = " . $this->get_given_budget_id() . " AND postings.account_id = accounts.id  AND projects.user_name = '" . $this->get_user_obj()->name . "' AND postings.transaction_date like '2015%' UNION SELECT postings.*, budgets.name, projects.id, projects.name, projects.img_url FROM postings, accounts, budgets, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE postings.budget_id = budgets.id AND budgets.scene_element_id = scene_elements.id AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id  AND budgets.id = " . $this->get_given_budget_id() . " AND postings.account_id = accounts.id  AND projects.user_name = '" . $this->get_user_obj()->name . "' AND postings.due_date like '2015%'";

      // debug
      //print "debug postings sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_scene_element_id") {
      $sql = "SELECT postings.* FROM postings WHERE postings.scene_element_id = " . $this->get_given_scene_element_id() . ";";

    } else if ($this->get_type() == "get_posting_given_id") {
      // todo fix the line below that has sql
      $sql = "select * from postings;";

    } else if ($this->get_type() == "get_by_account_id") {
      // debug
      print "debug postings hard-coded for postings from 2015 range only<br />\n";
      $sql = "SELECT postings.* FROM postings, accounts WHERE postings.account_id = " . $this->get_given_account_id() . " AND postings.account_id = accounts.id AND postings.transaction-date like '2015%' ORDER BY postings.journal_id, postings.transaction_date, postings.due_date, postings.id;";

      // debug
      //print "debug postings sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_budget_id" ||
        $this->get_type() == "get_by_scene_element_id" ||
        $this->get_type() == "get_by_account_id" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $posting = $this->make_posting();
        $posting->set_id(pg_result($results, $lt, 0));
        $posting->get_account_obj()->set_id(pg_result($results, $lt, 1));
        $posting->get_journal_obj()->set_id(pg_result($results, $lt, 2));
        $posting->get_asset_type_obj()->set_id(pg_result($results, $lt, 3));
        $posting->set_amount(pg_result($results, $lt, 4));
        $posting->get_budget_obj()->set_id(pg_result($results, $lt, 5));
        $posting->set_sort(pg_result($results, $lt, 6));
        $posting->set_description(pg_result($results, $lt, 7));
        $posting->set_status(pg_result($results, $lt, 8));
        $posting->set_transaction_date(pg_result($results, $lt, 9));
        $posting->get_transfer_account_obj()->set_id(pg_result($results, $lt, 10));
        $posting->get_supplier_obj()->set_id(pg_result($results, $lt, 11));
        $posting->set_due_date(pg_result($results, $lt, 12));
        if ($this->get_type() != "get_by_scene_element_id" &&
            $this->get_type() != "get_by_account_id") {
          $posting->get_budget_obj()->set_name(pg_result($results, $lt, 13));
          $posting->get_budget_obj()->get_project_obj()->set_id(pg_result($results, $lt, 14));
          $posting->get_budget_obj()->get_project_obj()->set_name(pg_result($results, $lt, 15));
          $posting->get_budget_obj()->get_project_obj()->set_img_url(pg_result($results, $lt, 16));
        }
      }
    } else {
      return $markup .= $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "Widen View: <a href=\"crop_plans?project_id=" . $this->get_given_project_id() . "&amp;project_season_id=" . $this->get_given_project_season_id() . "\">Crop Plans</a>\n";
    //$markup .= "</div>\n";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "Widen View: <a href=\"crop_plans?project_id=" . $this->get_given_project_id() . "&amp;project_season_id=" . $this->get_given_project_season_id() . "\">Crop Plans</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    // output given
    //$markup .= "<div class=\"given-variables\">\n";
    //$markup .= "    <em>This crop plan is of <strong>project_id = </em>" . $this->get_given_project_id() . "</strong> and <strong>project_season_id = </em>" . $this->get_given_project_season_id() . "</strong>\n";
    //$markup .= "</div>\n";

    // output given
    //$markup .= "<div class=\"given-variables\">\n";
    //$markup .= "    <em>This crop plan is of <strong>project_id = </em>" . $this->get_given_project_id() . "</strong> and <strong>project_season_id = </em>" . $this->get_given_project_season_id() . "</strong>\n";
    //$markup .= "</div>\n";

    // given_account_id
    if ($this->get_given_account_id()) {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "    <em>These are the postings for <strong>account_id = </em>" . $this->get_given_account_id() . "</strong>.\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // todo clean up what looks like an old view
    //$markup .= "<p><strong>postings</strong>\n";
    //$markup .= " | " . $this->print_add_posting();
    //$markup .= "</p>\n";

    // if subset, add heading
    // todo following was commented out because it has unidentified method
    //if ($this->get_given_account_id()) {
    //  $markup .= "<h3>when account id = " . $this->get_given_account_id() . "</h3>\n";
    //}

    // set tally variables
    $deposits = 0;
    $withdrawals = 0;

    // output table
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";

    // budget
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    budget\n";
    $markup .= "  </td>\n";

    // sort
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";

    // id
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";

    // account
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    account\n";
    $markup .= "  </td>\n";

    // transaction_date
    // due_date
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    transaction_date<br />\n";
    $markup .= "    or <em>due_date</em>\n";
    $markup .= "  </td>\n";

    // description
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";

    // amount
    $markup .= "  <td class=\"header\" colspan=\"2\">\n";
    $markup .= "    amount\n";
    $markup .= "  </td>\n";

    // asset_type
    //$markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    //$markup .= "    asset type\n";
    //$markup .= "  </td>\n";

    // transfer_account
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    transfer_account\n";
    $markup .= "  </td>\n";

    // supplier
    $markup .= "  <td class=\"header\" rowspan=\"2\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";

    // withdrawal
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    withdrawal\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    deposit\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</tr>\n";

    // set up tally variables
    $num = 0;

    foreach ($this->get_list_bliss()->get_list() as $posting) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // budget
      $markup .= "  <td>\n";
      include_once("budgets.php");
      $budget_obj = new Budgets($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $budget_list_string = $budget_obj->get_budget_list_given_posting_id($posting->get_id(), $user_obj);
      $markup .= "    " . $budget_list_string . "\n";
      $markup .= "  </td>\n";

      // project
      //$markup .= "  <td>\n";
      //$padding = "";
      //$float = "";
      //$width = "65";
      //$markup .= "    " . $posting->get_budget_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      //$markup .= "  </td>\n";

      // sort
      $column_name = "sort";
      include_once("lib/timekeeper.php");
      $timekeeper_obj = new Timekeeper($this->get_given_config());
      $color = $timekeeper_obj->calculate_cell_color($column_name, $posting->get_sort());
      $markup .= "  <td valign=\"middle\" style=\"background: $color;\">\n";
      $markup .= "    " . $posting->get_sort() . "<br />\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $posting->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // account
      $markup .= "  <td class=\"specify-width\">\n";
      include_once("accounts.php");
      $accounts_obj = new Accounts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $accounts_obj->get_name_with_link_given_id($posting->get_account_obj()->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      // transaction_date
      // due_date
      if ($posting->get_due_date()) {
        $color = "green";
        $markup .= "  <td valign=\"middle\" style=\"background: $color;text-align: center; font-size: 120%;\">\n";
      } else {
        $color = "orange";
        $markup .= "  <td valign=\"middle\" style=\"background: $color;text-align: center;\">\n";
      }
      $markup .= "    " . $posting->get_transaction_date() . "\n";
      $markup .= "    <em>" . $posting->get_due_date() . "</em>\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $posting->get_description() . "\n";
      $markup .= "  </td>\n";

      // amount
      // one of two columns as based upon journal_id
      //$markup .= "    <!-- journal_id = " . $posting->get_journal_id() . " -->\n";
      // get journal name
      include_once("journals.php");
      $journal_obj = new Journals($this->get_given_config());
      $journal_name = $journal_obj->get_name_given_id($posting->get_journal_obj()->get_id());
      if ($journal_name == "withdrawal") {
        $markup .= "  <td class=\"specify-width\" align=\"right\">\n";
        $markup .= "    " . number_format($posting->get_amount(), 2) . "\n";
        $withdrawals += $posting->get_amount();
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "  </td>\n";
      } else if ($journal_name == "deposit") {
        $markup .= "  <td>\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"specify-width\" align=\"right\">\n";
        $markup .= "    " . number_format($posting->get_amount(), 2) . "\n";
        $deposits += $posting->get_amount();
        $markup .= "  </td>\n";
      } else {
        $markup .= "  <td>\n";
        $markup .= "  " . $posting->get_journal_obj()->get_id() . "?\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "  ?\n";
        $markup .= "  </td>\n";
      }

      // asset_type_id
      //$markup .= "  <td class=\"specify-width\" align=\"center\">\n";
      //$markup .= "    <!-- asset_type_id = " . $posting->get_asset_type_id() . " -->\n";
      //include_once("asset_types.php");
      //$asset_types_obj = new AssetTypes($this->get_given_config());
      //$asset_type_name = $asset_types_obj->get_name_given_id($posting->get_asset_type_id());
      //$markup .= "    " . $asset_type_name . "\n";
      //$markup .= "  </td>\n";

      // transsfer_account name
      $markup .= "  <td>\n";
      include_once("accounts.php");
      $accounts_obj = new Accounts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $accounts_obj->get_name_with_link_given_id($posting->get_transfer_account_obj()->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      // supplier name
      $markup .= "  <td>\n";
      include_once("suppliers.php");
      $supplier_obj = new Suppliers($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $supplier_obj->get_name_with_link_given_id($posting->get_supplier_obj()->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
 
    // totals
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\" colspan=\"7\" style=\"text-align: left;\">\n";
    $markup .= "    subtotals\n";
    $markup .= "  </td>\n";
    $markup .= "  <td align=\"right\" class=\"header\">\n";
    $markup .= "    " . number_format($withdrawals, 2) . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td align=\"right\" class=\"header\">\n";
    $markup .= "    " . number_format($deposits, 2) . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    dollars\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $difference = $deposits - $withdrawals;
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\" colspan=\"8\" style=\"text-align: left;\">\n";
    $markup .= "    net income\n";
    $markup .= "  </td>\n";
    $markup .= "  <td align=\"right\" class=\"header\">\n";
    $markup .= "    " . number_format($difference, 2) . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    dollars\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // todo clean up the command line below
    //if (isset($_GET['command'])) {
    //  $command = $this->sanitize_user_input($_GET['command']);
    //  $this->set_command($command);
    //}
    //if (isset($_POST['command'])) {
    //  $command = $this->sanitize_user_input($_POST['command']);
    //  $this->set_command($command);
    //}
    //if ($this->get_command() == "record") {
    //  $this->output_form();
    //} else {
    //  $this->get_db_dash()->print_error("Command is not known.\n");
    //}

    foreach ($this->get_list_bliss()->get_list() as $posting) {
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";

      // id
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $posting->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // account
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    account (id and name)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      // account id
      $markup .= "    " . $posting->get_account_id() . "<br />\n";
      // account_name
      include_once("accounts.php");
      $accounts_obj = new Accounts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $accounts_obj->get_name_with_link_given_id($posting->get_account_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // journal
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    journal (id and name)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\" align=\"left\">\n";
      // journal id
      $markup .= "    " . $posting->get_journal_id() . "<br />\n";
      // journal name
      include_once("journals.php");
      $journal_obj = new Journals($this->get_given_config());
      $journal_name = $journal_obj->get_name_with_link_given_id($posting->get_journal_id());
      $markup .= "    " . $journal_name . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // account
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    asset_type (id and name)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\" align=\"left\">\n";
      // asset_type id
      $markup .= "    " . $posting->get_asset_type_id() . "<br />\n";
      // asset_type name
      include_once("asset_types.php");
      $asset_types_obj = new AssetTypes($this->get_given_config());
      $asset_type_name = $asset_types_obj->get_name_with_link_given_id($posting->get_asset_type_id());
      $markup .= "    " . $asset_type_name . "\n";

      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // amount
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    amount\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\" align=\"left\">\n";
      $markup .= "    " . $posting->get_amount() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // due_date
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    due_date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\" align=\"left\">\n";
      $markup .= "    " . $posting->get_due_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // budget
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    budget (id and name)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $posting->get_budget_obj()->get_id() . "<br />\n";
      $markup .= "    " . $posting->get_budget_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // sort
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $posting->get_sort() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // description
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $posting->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // status
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $posting->get_status() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // transaction_date
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    transaction_date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $posting->get_transaction_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // transfer_account_obj
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    transfer_account (id and name)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\" align=\"left\">\n";

      // transfer_account id
      $markup .= "    " . $posting->get_transfer_account_obj()->get_id() . "<br />\n";

      // transfer_account name
      include_once("accounts.php");
      $accounts_obj = new Accounts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $accounts_obj->get_name_with_link_given_id($posting->get_transfer_account_obj()->get_id(), $user_obj) . "\n";

      $markup .= "  </td>\n";

      $markup .= "  </tr>\n";
      $markup .= "<tr>\n";

      // supplier_obj
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    supplier (id and name)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\" align=\"left\">\n";

      // supplier_obj id
      $markup .= "    " . $posting->get_supplier_obj()->get_id() . "<br />\n";

      // supplier_obj name
      include_once("suppliers.php");
      $supplier_obj = new Suppliers($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $supplier_obj->get_name_with_link_given_id($posting->get_supplier_obj()->get_id(), $user_obj) . "\n";

      $markup .= "  </td>\n";

      $markup .= "  </tr>\n";
      $markup .= "</table>\n";

    }

    return $markup;
  }

  // method
  public function output_sidecar($given_budget_id, $given_user_obj, $given_total_only_flag = "") {
    $markup = "";

    // set
    $this->set_given_budget_id($given_budget_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug postings user_obj name = " . $given_user_obj->name . "<br />\n";

    // get data
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    // output
    if ($given_total_only_flag) {
      // output only the total from the table
      // set tally variables
      $deposits = 0;
      $withdrawals = 0;
      // loop
      foreach ($this->get_list_bliss()->get_list() as $posting) {
        // amount
        // one of two columns as based upon journal_id
        // get journal name
        include_once("journals.php");
        $journal_obj = new Journals($this->get_given_config());
        $journal_name = $journal_obj->get_name_given_id($posting->get_journal_obj()->get_id());
        if ($journal_name == "withdrawal") {
          $withdrawals += $posting->get_amount();
        } else if ($journal_name == "deposit") {
          $deposits += $posting->get_amount();
        } else {
          $markup .= "postings error journal_name is not known.\n";
        }
      }
      // totals
      $difference = $deposits - $withdrawals;
      $markup .= number_format($difference, 2) . "\n";
    } else {
      // normal
      $markup .= $this->output_aggregate();
    }

    return $markup;
  }

  // method
  public function get_build($given_budget_id, $given_user_obj) {
    $markup = "";

    $markup .= $this->get_simple_table($given_budget_id, $given_user_obj);

    return $markup;
  }

  // method
  public function get_simple_table($given_budget_id, $given_user_obj, $given_start_date = "") {
    $markup = "";

    // set
    $this->set_given_budget_id($given_budget_id);
    $this->set_user_obj($given_user_obj);

    // get data
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    $deposits = 0;
    $withdrawals = 0;
    $markup .= "<table class=\"plants\" style=\"color: black;\">\n";

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"\" colspan=\"5\" style=\"text-align: center; background-color: #BBBBBB;\">\n";
    $markup .= "    start_date = $given_start_date\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    if (0) {
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    account\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" colspan=\"2\" style=\"text-align: center;\">\n";
      $markup .= "    amount\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    foreach ($this->get_list_bliss()->get_list() as $posting) {
      if (! $posting->get_transaction_date()) {
        // must be due, which should not be skipped
      } else {
        if ($given_start_date) {
          if ($posting->get_transaction_date() < $given_start_date) {
            // before start_date so skip
            continue;
          }
        }
      }

      $markup .= "<tr>\n";
  
      $user_obj = $this->get_user_obj();
      $posting->set_user_obj($user_obj);

      // id
      if ($posting->get_transaction_date()) {
        $markup .= "  <td style=\"background-color: #D8BFD8;\">\n";
      } else {
        $markup .= "  <td style=\"background-color: #FFC1C1;\">\n";
      }
      $markup .= "    " . $posting->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // transaction_date
      if ($posting->get_transaction_date()) {
        $markup .= "  <td style=\"background-color: #D8BFD8;\">\n";
        $markup .= "    " . $posting->get_transaction_date() . "\n";
      } else {
        // for postings that are due
        $markup .= "  <td style=\"background-color: #FFC1C1;\">\n";
        $markup .= "    " . $posting->get_due_date() . "\n";
      }
      $markup .= "  </td>\n";

      // account_id
      if ($posting->get_transaction_date()) {
        $markup .= "  <td style=\"background-color: #D8BFD8;\">\n";
      } else {
        $markup .= "  <td style=\"background-color: #FFC1C1;\">\n";
      }
      //$markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    <!-- account_id = " . $posting->get_account_obj()->get_id() . " -->\n";
      include_once("accounts.php");
      $accounts_obj = new Accounts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $accounts_obj->get_name_with_link_given_id($posting->get_account_obj()->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      // journal_id
      $markup .= "    <!-- journal_id = " . $posting->get_journal_obj()->get_id() . " -->\n";
      include_once("journals.php");
      $journal_obj = new Journals($this->get_given_config());
      $journal_name = $journal_obj->get_name_given_id($posting->get_journal_obj()->get_id());

      // amount
      if ($posting->get_transaction_date()) {
        $markup .= "  <td style=\"background-color: #D8BFD8;\" class=\"specify-width\" align=\"right\">\n";
      } else {
        $markup .= "  <td style=\"background-color: #FFC1C1;\" class=\"specify-width\" align=\"right\">\n";
      }
      if ($journal_name == "withdrawal") {
        $markup .= "    " . number_format($posting->get_amount(), 2) . "\n";
        $withdrawals += $posting->get_amount();
      }
      $markup .= "  </td>\n";
      if ($posting->get_transaction_date()) {
        $markup .= "  <td style=\"background-color: #D8BFD8;\" class=\"specify-width\" align=\"right\">\n";
      } else {
        $markup .= "  <td style=\"background-color: #FFC1C1;\" class=\"specify-width\" align=\"right\">\n";
      }
      if ($journal_name == "deposit") {
        $markup .= "    " . number_format($posting->get_amount(), 2) . "\n";
        $deposits += $posting->get_amount();
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    // totals
    if (0) { 
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\" colspan=\"3\">\n";
      $markup .= "    totals\n";
      $markup .= "  </td>\n";
      $markup .= "  <td align=\"right\" class=\"header\">\n";
      $markup .= "    " . number_format($withdrawals, 2) . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td align=\"right\" class=\"header\">\n";
      $markup .= "    " . number_format($deposits, 2) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      // bottom row
      $difference = $deposits - $withdrawals;
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\" colspan=\"3\">\n";
      $markup .= "    net\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    \n";
      $markup .= "  </td>\n";
      $markup .= "  <td align=\"right\" class=\"header\">\n";
      $markup .= "    " . number_format($difference, 2) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_count_given_scene_element_id($given_scene_element_id, $given_user_obj) {

    // set
    $this->set_given_scene_element_id($given_scene_element_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error postings " . $markup . "</p>\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_count_given_account_id($given_account_id, $given_user_obj) {

    // set
    $this->set_given_account_id($given_account_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error postings " . $markup . "</p>\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_postings_count_given_budget_id($given_budget_id, $given_user_obj) {
    $markup = "test";

    // set
    $this->set_given_budget_id($given_budget_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug postings given_budget_id = " . $this->get_given_budget_id() . "<br />\n";

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return $markup;
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();

  }

}
