<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_aliases.php

include_once("lib/scrubber.php");

class PlantAliases extends Scrubber {

  // given
  private $given_plant_id;
 
  // given_plant_id
  public function set_given_plant_id($var) {
    $this->given_plant_id = $var;
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // attributes
  private $id;
  private $plant_obj;
  private $name;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  private function make_plant_alias() {
    $obj = new PlantAliases($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_id()) {
      $this->set_type("get_by_plant_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_id") {
      $sql = "SELECT plant_aliases.*id, plants.common_name FROM plant_aliases, plants WHERE plant_aliases.plant_id = plants.id AND plant_aliases.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT plant_aliases.*id, plants.common_name FROM plant_aliases, plants WHERE plant_aliases.plant_id = plants.id AND plants.id = " . $this->get_given_plant_id() . ";";

      // debug
      //print "debug plant_aliases sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_aliases.*id, plants.common_name FROM plant_aliases, plants WHERE plant_aliases.plant_id = plants.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_alias();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_name(pg_result($results, $lt, 2));
        $obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_alias) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_alias->get_id() . "\n";
      $markup .= "  </td>\n";
      
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_alias->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_alias->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function output_sidecar($given_plant_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);
    $this->set_user_obj($given_user_obj);

    // database
    $markup .= $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<div style=\"margin: 4px 0px 4px 0px; padding: 8px 6px 10px 6px; background-color: #E8C782; font-size: 95%; width: 100%;\">\n";
      $markup .= "<ul>\n";
      foreach ($this->get_list_bliss()->get_list() as $plant_alias) {
        $markup .= "<li>\n";
        $markup .= $plant_alias->get_name();
        $markup .= "</li>\n";
      }
      $markup .= "</ul>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

}
