<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-28
// version 1.2 2015-01-17

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/invoices.php

include_once("lib/standard.php");

class Invoices extends Standard {

  // given
  private $given_customer_id;

  // given_customer_id
  public function set_given_customer_id($var) {
    $this->given_customer_id = $var;
  }
  public function get_given_customer_id() {
    return $this->given_customer_id;
  }

  // attribute
  private $id;
  private $customer_obj;
  private $date;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // customer_obj
  public function get_customer_obj() {
    if (! isset($this->customer_obj)) {
      include_once("customers.php");
      $this->customer_obj = new Customers($this->get_given_config());
    }
    return $this->customer_obj;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // method
  private function make_invoice() {
    $obj = new Invoices($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_customer_id()) {
      $this->set_type("get_by_customer_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT invoices.*, customers.name FROM invoices, customers WHERE invoices.customer_id = customers.id AND invoices.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT invoices.*, customers.name FROM invoices, customers WHERE invoices.customer_id = customers.id ORDER BY invoices.date;";

    } else if ($this->get_type() == "get_by_customer_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT invoices.*, customers.name FROM invoices, customers WHERE invoices.customer_id = customers.id AND customers.id = " . $this->get_given_customer_id() . " ORDER BY invoices.date;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_customer_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_invoice();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_customer_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_date(pg_result($results, $lt, 2));
        $obj->get_customer_obj()->set_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    customer\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    invoice_lines\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    invoice total\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $invoice_total_tally = 0;
    foreach ($this->get_list_bliss()->get_list() as $invoice) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // date
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice->get_date() . "\n";
      $markup .= "  </td>\n";

      // customers
      $markup .= "  <td>\n";
      include_once("customers.php");
      $customer_obj = new Customers($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $customer_obj->get_customer_given_id($invoice->get_customer_obj()->get_id(), $user_obj);
      $markup .= "  </td>\n";

      // invoice_lines
      if (0) {
        $markup .= "  <td>\n";
        include_once("invoice_lines.php");
        $invoice_line_obj = new InvoiceLines($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $invoice_line_obj->get_invoice_lines_given_invoice_id($invoice->get_id(), $user_obj);
        $markup .= "  </td>\n";
      }

      // invoice_lines total
      $markup .= "  <td style=\"text-align: right;\">\n";
      include_once("invoice_lines.php");
      $invoice_line_obj = new InvoiceLines($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $invoice_total = $invoice_line_obj->get_total_given_invoice_id($invoice->get_id(), $user_obj);
      $markup .= $invoice_total; 
      $invoice_total_tally += $invoice_total;
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    $markup .= "<p>invoice total (grand) = " . $invoice_total_tally . "</p>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    $line_total_tally = 0;
    foreach ($this->get_list_bliss()->get_list() as $invoice) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $invoice->get_id_with_link() . "\n";
      $markup .= "</tr>\n";
      $markup .= "  </td>\n";

      // date
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $invoice->get_date() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // customers
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    customers\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      include_once("customers.php");
      $customer_obj = new Customers($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $customer_obj->get_customer_given_id($invoice->get_customer_obj()->get_id(), $user_obj);
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // invoice_lines
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoice_lines\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"width: 700px;\">\n";
      include_once("invoice_lines.php");
      $invoice_line_obj = new InvoiceLines($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $invoice_line_obj->get_invoice_lines_given_invoice_id($invoice->get_id(), $user_obj);
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // invoice total
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    invoice total\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"text-align: right;\">\n";
      include_once("invoice_lines.php");
      $invoice_line_obj = new InvoiceLines($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $invoice_total = $invoice_line_obj->get_total_given_invoice_id($invoice->get_id(), $user_obj);
      $markup .= $invoice_total;
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $line_total_tally = 0;

      // payments
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    payments\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      include_once("payments.php");
      $payment_obj = new Payments($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $payment_obj->get_payments_given_invoice_id($invoice->get_id(), $user_obj);
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // payments total
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    payments total\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"text-align: right;\">\n";
      include_once("payments.php");
      $payment_obj = new Payments($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $payments_total = $payment_obj->get_payments_total_given_invoice_id($invoice->get_id(), $user_obj);
      $markup .= "  " . $payments_total . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // balance
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    balance\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"text-align: right;\">\n";
      $balance = $invoice_total - $payments_total;
      $markup .= "  " . $balance . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_invoices_given_customer_id($given_customer_id, $given_user_obj) {
    $markup = "";

    $this->set_given_customer_id($given_customer_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      // guts of the list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $invoice) {
        ++$num;

        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // date
        $markup .= "  <td>\n";
        $markup .= "    " . $invoice->get_date() . "\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";

    } else {
      $markup .= "No invoices found.<br />\n";
    }

    return $markup;
  }

  // method
  public function get_customer_link_given_invoice_id($given_invoice_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_invoice_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {

      foreach ($this->get_list_bliss()->get_list() as $invoice) {
        $markup .= "    " . $invoice->get_customer_obj()->get_name_with_link() . "\n";
      }
    }

    return $markup;
  }

}
