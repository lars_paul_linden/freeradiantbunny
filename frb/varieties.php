<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/varieties.php

include_once("lib/standard.php");

class Varieties extends Standard {

  // given
  private $given_plant_id;

  // given_plant_id
  public function set_given_plant_id($var) {
    $error_message = $this->get_validator_obj()->validate_id($var, "plant_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_id = $var;
    }
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // attributes
  private $id;
  private $name;
  private $description;
  private $plant_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // method
  public function get_name_with_link() {
    $url = $this->url("varieties/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // method
  private function make_variety() {
    $obj = new Varieties($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_id()) {
      // subset
      $this->set_type("get_by_plant_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // todo this produced too many results
      //$sql = "SELECT varieties.id, varieties.plant_id, varieties.name, varieties.description, plants.common_name FROM varieties, plants WHERE varieties.id = " . $this->get_given_id() . " AND varieties.plant_id = plants.id;";
      $sql = "SELECT varieties.* FROM varieties WHERE varieties.id = " . $this->get_given_id() . ";";

      // debug 
      //print "debug varieties sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT varieties.id, varieties.plant_id, varieties.name, varieties.description, plants.common_name FROM varieties, plants WHERE varieties.plant_id = plants.id AND varieties.plant_id = " . $this->get_given_plant_id() . " ORDER BY varieties.name;";

    } else if ($this->get_type() == "get_plant_id") {
      $sql = "SELECT varieties.plant_id FROM varieties WHERE varieties.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT varieties.id, varieties.plant_id, varieties.name, varieties.description, plants.common_name FROM varieties, plants WHERE varieties.plant_id = plants.id ORDER BY plants.common_name, varieties.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $this->set_name(pg_result($results, $lt, 2));
        $this->set_description(pg_result($results, $lt, 3));
        //$this->get_plant_obj()->set_common_name(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "get_varieties" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $variety = $this->make_variety();
        $variety->set_id(pg_result($results, $lt, 0));
        $variety->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $variety->set_name(pg_result($results, $lt, 2));
        $variety->set_description(pg_result($results, $lt, 3));
        $variety->get_plant_obj()->set_common_name(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "get_by_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $variety = $this->make_variety();
        $variety->set_id(pg_result($results, $lt, 0));
        $variety->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $variety->set_name(pg_result($results, $lt, 2));
        $variety->set_description(pg_result($results, $lt, 3));
        $variety->get_plant_obj()->set_common_name(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->print_error("Error " . get_class($this) . ": does not know the type.");
    }
  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    // todo deal with abstract nature of this subsubmenu
    if ($this->get_given_id()) {
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("varieties");
      $markup .= "  Widen view: <a href=\"" . $url . "\">All&nbsp;Varities</a><br />\n";
      $markup .= "  <br />\n";
      // todo fix the destination of this link
      $url = $this->url("plants/variety/" . $this->get_plant_id_of_this_variety());
      $markup .= "  Widen view: <a href=\"" . $url . "\">Varieties of the this same plant</a><br />\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function output_given_variables() {
    $markup = "";

    $markup .= $this->output_given_variables_given_id();

    if ($this->get_given_plant_id() != "") {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "    <em>These " . get_class($this) . " are of the plant <strong>" . $this->get_plant_obj()->get_common_name_with_link() . "</strong>.\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function output_data_set_table() {
    $markup = "";

    if ($this->get_error_message()) {
      $markup .= $this->get_error_message();
    } else {
      if ($this->get_type() == "get_all") {
        // specialized
        $markup .= $this->output_aggregate_with_plant_name();
      } else {
        $markup .= parent::output_data_set_table();
      }
    }

    return $markup;
  }

  // method
  public function output_aggregate_with_plant_name() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    varieties\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";
    $sorted_list_obj = array();
    foreach ($this->get_list_bliss()->get_list() as $variety) {
      $plant_id = $variety->get_plant_obj()->get_id();
      if (array_key_exists($plant_id, $sorted_list_obj)) {
        // debug
        //print "debug plant_id " . $plant_id . " exists<br />";
        // exists, so add to array
        $varities_array = $sorted_list_obj[$plant_id];
        array_push($varities_array, $variety);
        // debug
        //print "debug variety " . $variety->get_id() . " exists<br />";
        $sorted_list_obj[$plant_id] = $varities_array;
      } else {
        // does not exist, so create new array
        $varities_array = array();
        array_push($varities_array, $variety);
        $sorted_list_obj[$plant_id] = $varities_array;
      }
    }

    foreach ($sorted_list_obj as $plant_id => $variety_array) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- variety id=" . $variety->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $variety_array[0]->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $count = count($variety_array);
      $num = 0;
      while ($num < $count) {
        $markup .= $variety_array[$num]->get_name_with_link();
        // add comma to all except last
        if ($num < ($count - 1)) {
          $markup .= ", ";
        }
        $num++;
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    foreach ($this->get_list_bliss()->get_list() as $variety) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- variety id=" . $variety->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $variety->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $variety) {
      $markup .= "<h2>" . $variety->get_name() . "</h2>\n";
      $markup .= "<p>" . $variety->get_description() . "</p>\n";
      //$markup .= "<p>id = " . $variety->get_id() . "</p>\n";
      //$markup .= "<p><em>id = " . $variety->get_id() . "</em></p>\n";
      $markup .= "<p>This is a variety of <strong>" . $variety->get_plant_obj()->get_common_name_with_link() . "</strong>.</p>\n";
      include_once("seed_packets.php");
      $seed_packets_obj = new SeedPackets($this->get_given_config());
      $markup .= $seed_packets_obj->output_sidecar($variety->get_id());
    }

    return $markup;
  }

  // method form
  public function print_radio_button_list($parameter_name, $given_plant_id) {
    $markup = "";

    if (! $given_plant_id) {
      $markup .= $this->get_db_dash()->output_error("Error unable to to display varieties.");
      return $markup;
    }
    $this->set_given_plant_id($given_plant_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<table border=\"1\" cellpadding=\"6\">\n";
      $markup .= "<tr>\n";
      $markup .= "  <td valign=\"top\">\n";
      $count = -1;
      foreach ($this->get_list_bliss()->get_list() as $variety) {
        $count++;
        if ($count == 16) {
          $count = 0;
          $markup .= "  </td>\n";
          $markup .= "  <td valign=\"top\">\n";
        }
        $markup .= "    <input type=\"radio\" name=\"" . $parameter_name . "\" value=\"" . $variety->get_id() . "\" />" . $variety->get_name() . "<br />\n";
      }
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "</table>\n";
    }

    return $markup;
  }

  // method form
  public function print_select_list($name, $default_variety_id) {
    $markup = "";

    // parameter "name" is the attribute value for the input element
    $type = "get_variety_given_plant_id";
    $this->get_db_dash()->load($this, $type);
    $markup .= "    <select name=\"" . $name . "\">\n";
    foreach ($this->get_list_bliss()->get_list() as $variety) {
      $markup .= "      <option value=\"" . $variety->get_id() . "\"";
      if ($default_variety_id) {
        if ($default_variety_id == $variety->get_id()) {
          $markup .= " selected=\"selected\"";
        }
      }
      $markup .= " />" . $variety->get_name() . "</option>\n";
    }
    $markup .= "    </select>\n";

    return $markup;
  }

  // method
  private function output_list_as_text_line() {
    $markup = "";

    // guts of the list
    // control commas
    $last = $this->get_list_bliss()->get_count();
    $penultamate = "";
    if ($last >= 2) {
      $penultamate = $last - 1;
    }
    $num = 0;
    $markup .= "<p style=\"margin: 2px 0px 0px 0px; padding: 2px 0px 12px 4px;\">\n";
    foreach ($this->get_list_bliss()->get_list() as $variety) {
      $num++;
      $markup .= $variety->get_name_with_link();

      // deal with various comma situations
      if ($num == $last) {
        $markup .= ".\n";
      } else if ($num == $penultamate) {
        if ($penultamate == 1) {
          $markup .= " and ";
        } else {
          $markup .= ", and ";
        }
      } else {
        $markup .= ", ";
      }
      $markup .= "<br />\n";
    }
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  public function output_sidecar($given_plant_id) {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<div style=\"margin: 4px 50% 4px 0px; padding: 6px 6px 0px 6px; background-color: #E8C7FF;\">\n";
      $markup .= "<h4 style=\"margin: 4px 0px 0px 4px; padding: 4px 0px 6px 1px;\">Varieties</h4>\n";
      $markup .= $this->output_list_as_text_line();
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_plant_id_of_this_variety() {
    $markup = "";

    // set
    $this->set_type("get_plant_id");

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    $id = $this->get_plant_obj()->get_id();

    return $id;
  }

  //  method
  public function get_name_with_link_given_id($given_id, $given_user_obj) {
    $markup = "";

    // todo the need to reset here probably indicates poor design elsewhere
    // reset
    $this->get_list_bliss()->empty_list();

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "no variety";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $markup .= "more than one variety";
      return $markup;
    }

    return $this->get_list_bliss()->get_first_element()->get_name_with_link();
  }

  //  method
  public function get_plant_id_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug varieties given_id " . $given_id . "<br />\n";

    // todo should this be elsewhere too (or is this being here
    // todo an admission of defeat?
    $this->get_list_bliss()->empty_list();

    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "error: no variety";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $markup .= "error: more than 1 variety";
      return $markup;
    }

    return $this->get_list_bliss()->get_first_element()->get_plant_obj()->get_id();
  }

}
