<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/design_order_items.php

include_once("lib/scrubber.php");

class DesignOrderItems extends Scrubber {

  // given
  private $given_design_order_id;
  private $given_design_instance_id;

  // given_design_order_id
  public function set_given_design_order_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "order_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_design_order_id = $var;
    }
  }
  public function get_given_design_order_id() {
    return $this->given_design_order_id;
  }

  // given_design_instance_id
  public function set_given_design_instance_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "design_part_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_design_instance_id = $var;
    }
  }
  public function get_given_design_instance_id() {
    return $this->given_design_instance_id;
  }

  // attributes
  private $id;
  private $name;
  private $description;
  private $design_order_obj;
  private $cost;
  private $shipping_cost;
  private $design_instance_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // design_order_obj
  public function get_design_order_obj() {
    if (! isset($this->design_order_obj)) {
      include_once("design_orders.php");
      $this->design_order_obj = new DesignOrders($this->get_given_config());
    }
    return $this->design_order_obj;
  }

  // cost
  public function set_cost($var) {
    $this->cost = $var;
  }
  public function get_cost() {
    return $this->cost;
  }

  // shipping_cost
  public function set_shipping_cost($var) {
    $this->shipping_cost = $var;
  }
  public function get_shipping_cost() {
    return $this->shipping_cost;
  }

  // design_instance_obj
  public function get_design_instance_obj() {
    if (! isset($this->design_instance_obj)) {
      include_once("design_instances.php");
      $this->design_instance_obj = new DesignInstances($this->get_given_config());
    }
    return $this->design_instance_obj;
  }

  // method
  private function make_design_order_item() {
    $obj = new DesignOrderItems($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_design_order_id()) {
      // subset
      $this->set_type("get_by_design_order_id");

    } else if ($this->get_given_design_instance_id()) {
      // subset
      $this->set_type("get_by_design_instance_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT design_order_items.id, design_order_items.name, design_order_items.description, design_order_items.design_order_id, design_order_items.cost, design_order_items.shipping_cost, design_order_items.design_instance_id FROM design_order_items WHERE design_order_items.id = " . $this->get_given_id() . " ORDER BY design_order_items.name;";

    } else if ($this->get_type() == "get_by_design_instance_id") {
      $sql = "SELECT design_order_items.id, design_order_items.name, design_order_items.description, design_order_items.design_order_id, design_order_items.cost, design_order_items.shipping_cost, design_order_items.design_instance_id FROM design_order_items, design_instances WHERE design_instances.id = design_order_items.design_instance_id AND design_instances.id = " . $this->get_given_design_instance_id() . " ORDER BY design_order_items.name;";

    } else if ($this->get_type() == "get_by_design_order_id") {
      $sql = "SELECT design_order_items.id, design_order_items.name, design_order_items.description, design_order_items.design_order_id, design_order_items.cost, design_order_items.shipping_cost, design_order_items.design_instance_id FROM design_order_items, design_orders WHERE design_orders.id = design_order_items.design_order_id AND design_orders.id = " . $this->get_given_design_order_id() . " ORDER BY design_order_items.name;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT design_order_items.id, design_order_items.name, design_order_items.description, design_order_items.design_order_id, design_order_items.cost, design_order_items.shipping_cost, design_order_items.design_instance_id FROM design_order_items ORDER BY design_order_items.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_design_order_item();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->get_design_order_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_cost(pg_result($results, $lt, 4));
        $obj->set_shipping_cost(pg_result($results, $lt, 5));
        $obj->get_design_instance_obj()->set_id(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "get_by_design_order_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_design_order_item();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->get_design_order_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_cost(pg_result($results, $lt, 4));
        $obj->set_shipping_cost(pg_result($results, $lt, 5));
        $obj->get_design_instance_obj()->set_id(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "get_by_design_instance_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_design_order_item();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->get_design_order_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_cost(pg_result($results, $lt, 4));
        $obj->set_shipping_cost(pg_result($results, $lt, 5));
        $obj->get_design_instance_obj()->set_id(pg_result($results, $lt, 6));
      }
    } else {
      return $this->get_db_dash()->print_error("Error " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  public function get_costs_given($given_design_order_id, $given_design_instance_id, $given_user_obj) {
    $costs = array();
    // cost
    $costs[0] = "?";
    // shipping_cost
    $costs[1] = "?";

    // set
    $this->set_given_design_order_id($given_design_order_id);
    $this->set_given_design_instance_id($given_design_instance_id);
    $this->set_user_obj($given_user_obj);

    // get data
    $this->determine_type();
    $this->prepare_query();

    if ($this->get_list_bliss()->get_count() == 1) {
      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      if ($obj->get_cost()) {
        $costs[0] = $obj->get_cost();
        $costs[1] = $obj->get_shipping_cost();
      }
    }

    return $costs;
  }

  // method
  public function output_sidecar($given_design_order_id) {
    $markup = "";

    // set
    $this->set_given_design_order_id($given_design_order_id);

    // get data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // heading
    $markup .= "<h2>Design Order Items of this Design Order</h2>\n";

    // data table
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";

    // heading
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  #\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  id\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  name\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  description\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  design order id\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  cost\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  shipping cost\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  design instance id\n";
    $markup .= "</td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    $total_cost = 0;
    $total_shipping_cost = 0;
    foreach ($this->get_list_bliss()->get_list() as $design_order_item) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "<td class=\"header\">\n";
      $markup .= "  " . $num . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $design_order_item->get_id() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $design_order_item->get_name() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $markup .= "  " . $design_order_item->get_description() . "\n";
      $markup .= "</td>\n";

      $markup .= "<td>\n";
      $url = $this-.url("design_orders/" . $design_order_item->get_design_order_obj()->get_id());
      $markup .= "  <a href=\"" . $url . "\">" . $design_order_item->get_design_order_obj()->get_id() . "</a>\n";
      $markup .= "</td>\n";

      /**
       * @todo figure out this old was of obtaining the "cost"
       */
//      //include_once("design_order_items.php");
//      //$design_order_items_obj = new DesignOrderItems($this->get_given_config());
//      //$design_order_items_obj;
//      //$user_obj = $this->get_user_obj();
//      //$order_id = $design_order_item->get_design_order_obj()->get_id();
//      //$design_order_item_id = $design_order_item->get_id();
//      //$costs = array();
//      //$costs = $design_order_items_obj->get_costs_given($order_id, $design_order_item_id, $user_obj);
//      //$cost = $costs[0];
//      //$shipping_cost = $costs[1];
//      //$markup .= "<td style=\"text-align: right;\">\n";
//      //$markup .= "  " . number_format($cost, 2) . "\n";
//      //$total_cost += $cost;
//      //$markup .= "</td>\n";

      $markup .= "<td style=\"text-align: right;\">\n";
      $cost = $design_order_item->get_cost();
      if (is_numeric($cost)) {
        $markup .= "  " . number_format($cost, 2) . "\n";
      }
      $markup .= "</td>\n";
      $total_cost += $cost;

      $markup .= "<td style=\"text-align: right;\">\n";
      $shipping_cost = $design_order_item->get_shipping_cost();
      if (is_numeric($shipping_cost)) {
        $markup .= "  " . number_format($shipping_cost, 2) . "\n";
      }
      $markup .= "</td>\n";
      $total_shipping_cost += $shipping_cost;

      $markup .= "<td style=\"text-align: right;\">\n";
      $url = $this->url("design_instances/" . $design_order_item->get_design_instance_obj()->get_id());
      $markup .= "  <a href=\"" . $url . "\">" . $design_order_item->get_design_instance_obj()->get_id() . "</a>\n";
      $markup .= "</td>\n";

      $markup .= "</tr>\n";
    }

    // totals
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"5\" align=\"right\" style=\"background-color: #EFEFEF;\">\n";
    $markup .= "    totals\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"specify-right-justify\">\n";
    $markup .= "    " . number_format($total_cost, 2);
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"specify-right-justify\">\n";
    $markup .= "    " . number_format($total_shipping_cost, 2);
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $grand_total = $total_cost + $total_shipping_cost;
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"5\" align=\"right\" style=\"background-color: #EFEFEF;\">\n";
    $markup .= "    grand total\n";
    $markup .= "  </td>\n";
    $markup .= "  <td colspan=\"2\" class=\"specify-right-justify\">\n";
    $markup .= "    " . number_format($grand_total, 2);
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_table_given_design_instance_id($given_design_instance_id) {
    $markup = "";

    // set
    $this->set_given_design_instance_id($given_design_instance_id);

    // get data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // heading
    $markup .= "<h2>Design Order Items of this Design Instance</h2>\n";

    // data table
    $markup .= $this->output_table();

    return $markup;
  }
}
