<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/tickets.php

include_once("lib/standard.php");

class Tickets extends Standard {

  // given
  private $given_id;
  private $given_process_id;
  private $given_project_id;
  private $given_status;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_status
  public function set_given_status($var) {
    $this->given_status = $var;
  }
  public function get_given_status() {
    return $this->given_status;
  }

  // attributes
  private $action_to_take;
  private $process_obj;

  // action_to_take
  public function set_action_to_take($var) {
    $this->action_to_take = $var;
  }
  public function get_action_to_take() {
    return $this->action_to_take;
  }

  // process_obj
  public function get_process_obj() {
    if (! isset($this->process_obj)) {
      include_once("processes.php");
      $this->process_obj = new Processes($this->get_given_config());
    }
    return $this->process_obj;
  }

  // method
  public function get_primary_key() {
    return $this->get_id();
  }

  // method
  public function get_name_with_link($given_target_directory = "") {
    $url = $this->get_url();
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }
  public function get_name_with_link_natural() {
    $url = $this->url("tickets/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // method
  private function make_ticket() {
    $obj = new Tickets($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // debug
    //print "debug tickets user_obj " . get_class($this->get_user_obj()) . "<br />\n";

    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT tickets.* FROM tickets WHERE tickets.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT tickets.* FROM tickets ORDER BY tickets.sort DESC, tickets.name;";

    } else if ($this->get_type() == "get_by_process_id") {
      if ($this->get_given_process_id()) {
        $sql = "SELECT tickets.* FROM tickets WHERE tickets.process_id = " . $this->get_given_process_id() . " ORDER BY tickets.sort DESC, tickets.status DESC, tickets.name;";
      } else {
        print "Error: tickets given_process_id is not known<br />\n";
      }

      // debug
      //print "debug: sql = $sql<br />\n";

    } else if ($this->get_type() == "get_by_project_id") {
      if ($this->get_given_project_id()) {
        $sql = "SELECT tickets.* FROM tickets, processes, business_plan_texts, goal_statements, projects WHERE projects.id = " . $this->get_given_project_id() . " AND tickets.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id ANd business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id ORDER BY tickets.sort, tickets.name;";
      } else {
        print "Error: tickets given_project_id is not known<br />\n";
      }

    } else if ($this->get_type() == "get_count") {
      if ($this->get_given_status() == "open") {
        $sql = "SELECT * FROM tickets, projects WHERE tickets.process_id = processes.id AND processes.id = " . $this->get_given_process_id() . " AND tickets.status = 'open';";
      } else {
        $sql = "SELECT * FROM tickets, processes WHERE tickets.process_id = processes.id AND processes.id = " . $this->get_given_process_id() . ";";
      }

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    // might have to define below
    $database_name = "";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_ticket();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_status(pg_result($results, $lt, 3));
        $obj->get_process_obj()->set_id(pg_result($results, $lt, 4));
        $obj->set_description(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->set_action_to_take(pg_result($results, $lt, 7));
      }
    } else if ($this->get_type() == "get_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_ticket();
        $obj->set_name(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .=  "<table class=\"plants\">\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $ticket) {

      // heading
      // project
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead-narrow\">\n";
      $markup .=  "    project\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td>\n";
      include_once("projects.php");
      $project_obj = new Projects($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $project_data = $project_obj->get_img_with_link_given_given_process_id($ticket->get_process_obj()->get_id(), $user_obj);
      $markup .= "    " . $project_data . "\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // process
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead-narrow\">\n";
      $markup .=  "    process\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td>\n";
      $url = $this->url("processes/" . $ticket->get_process_obj()->get_id());
      $markup .=  "    <a href=\"" . $url . "\">" . $ticket->get_process_obj()->get_id() . "</a><br />\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // name
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead\">\n";
      $markup .=  "    name\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td>\n";
      $markup .=  "    <h2>" . $ticket->get_name() . "</h2>\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // description
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead-narrow\">\n";
      $markup .=  "    description\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td>\n";
      $markup .=  "    " . $ticket->get_description() . "\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // sort
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead\" width=\"150px\">\n";
      $markup .=  "    sort\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $ticket->get_sort() . "<br />\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // id
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead\">\n";
      $markup .=  "    id\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $ticket->get_id() . "\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // status
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead\">\n";
      $markup .=  "    status\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $ticket->get_status() . "<br />\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

      // action_to_take
      $markup .=  "<tr>\n";
      $markup .=  "  <td class=\"colhead\" width=\"150px\">\n";
      $markup .=  "    action_to_take\n";
      $markup .=  "  </td>\n";
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $ticket->get_action_to_take() . "<br />\n";
      $markup .=  "  </td>\n";
      $markup .=  "</tr>\n";

    }
    $markup .=  "</table>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .=  "<div id=\"tenperday\">\n";
    $markup .=  "<table class=\"plants\">\n";

    // heading
    $markup .=  "<tr>\n";
    $markup .=  "  <td class=\"colhead\">\n";
    $markup .=  "    #\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead-narrow\">\n";
    $markup .=  "    project\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead-narrow\">\n";
    $markup .=  "    process\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead\">\n";
    $markup .=  "    sort\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead\">\n";
    $markup .=  "    id\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead\">\n";
    $markup .=  "    name\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead\" width=\"250px\">\n";
    $markup .=  "    action to take\n";
    $markup .=  "  </td>\n";
    $markup .=  "  <td class=\"colhead\">\n";
    $markup .=  "    status\n";
    $markup .=  "  </td>\n";
    $markup .=  "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $ticket) {

      $markup .=  "<tr>\n";

      // num
      $num++;
      $markup .=  "  <td class=\"mid\">\n";
      $markup .=  "    " . $num . "\n";
      $markup .=  "  </td>\n";

      // project
      $markup .=  "  <td>\n";
      include_once("projects.php");
      $project_obj = new Projects($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $project_data = $project_obj->get_img_with_link_given_given_process_id($ticket->get_process_obj()->get_id(), $user_obj);
      $markup .= "    " . $project_data . "\n";
      $markup .=  "  </td>\n";

      // process
      $markup .=  "  <td>\n";
      include_once("processes.php");
      $process_obj = new Processes($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $process_data = $process_obj->get_name_with_link_given_process_id($ticket->get_process_obj()->get_id(), $user_obj);
      $markup .= "    " . $process_data . "\n";
      $markup .=  "  </td>\n";

      // sort
      $markup .= $ticket->get_sort_cell();

      // id
      $markup .=  "  <td class=\"mid\">\n";
      $url = $this->url("tickets/" . $ticket->get_id());
      $markup .=  "    " . "<a href=\"" . $url . "\" class=\"show\">" . $ticket->get_id() . "</a>\n";
      $markup .=  "  </td>\n";

      // name with link
      $markup .=  "  <td>\n";
      $markup .=  "    " . $ticket->get_name_with_link_natural() . "<br />\n";
      $markup .=  "  </td>\n";

      // action_to_take
      if ($ticket->get_action_to_take()) {
        $markup .= "  <td style=\"text-align: left; background-color: #22de8a;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: left; background-color: #CCCCCC;\">\n";
      }
      $markup .=  "    " . $ticket->get_action_to_take() . "<br />\n";
      $markup .=  "  </td>\n";

      // status
      if ($ticket->get_status() == "" || $ticket->get_status() == "closed") {
        // color red
        $markup .= "  <td style=\"text-align: center; background-color: #CD5555;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center;\">\n";
      }
      $markup .=  "    " . $ticket->get_status() . "<br />\n";
      $markup .=  "  </td>\n";

      $markup .=  "</tr>\n";

    }
    $markup .=  "</table>\n";

    return $markup;
  }

  // method
  public function get_ticket_count_given_process_id($given_process_id, $given_user_obj, $given_status = "") {
    $count_tickets = "unknown";

    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    if ($given_status) {
      $this->set_given_status($given_status);
    }

    // debug
    //print "debug tickets sql = " . $this->get_given_status() . "<br />\n";

    // load data from database
    $this->set_type("get_count");
    $markup = $this->prepare_query();
    // todo do something with the markup returned above

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $obj) {
        $count_tickets += 1;
        // $count_tickets .= $obj->get_name() . "<br />\n";
      }
    } else {
      // no rows found
      $count_tickets = 0;
    }

    return $count_tickets;
  }

  // method
  public function get_tickets_given_process_id($given_process_id, $given_user_obj, $given_status = "") {
    $markup = "";

    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // note default behavior assumes open only
    if (! $given_status) {
      $this->set_given_status("open");
    }

    // load data from database
    $markup = $this->determine_type();
    $markup = $this->prepare_query();
    // todo do something with the markup returned above

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $obj) {
        $markup .= $obj->get_id_with_link() . " " . $obj->get_name() . "<br />\n";
      }
    }

    return $markup;
  }

  // method
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there is 1 item
    if ($this->get_list_bliss()->get_count() == 1) {
      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    } else {
      print $this->get_db_dash()->output_error("Error " . get_class($this) . ": expect 1 count and got a count = " . $this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  public function output_sidecar($given_process_id, $given_user_obj, $heading = "") {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_process_id");

    // debug
    //print "type " . $this->get_type() . "<br />\n";
    //print "scene_elements: user_name " . $this->get_user_obj()->name . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    // list heading
    if ($heading) {
      $markup .= "<h2>Tickets</h2>\n";
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<div style=\"margin: 4px 4px 4px 0px; padding: 10px 6px 10px 6px; background-color: #E8C782; font-size: 100%;\">\n";
      $markup .= $this->output_aggregate();
      $markup .= "</div>\n";

    } else {
      $markup .= "<div style=\"margin: 4px 4px 4px 0px; padding: 10px 6px 10px 6px; background-color: #E8C782; font-size: 100%;\">\n";
      $markup .= "<p>No tickets found.</p>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validator_function_string("sort");
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validator_function_string("view");
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validator_function_string("id");
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            // todo refort this function in lib and then customize here
            $table_name = "tickets";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $obj = new Tickets($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
          // 3
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

}
