<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/timecards.php

include_once("lib/scrubber.php");

class Timecards extends Scrubber {

  // given
  private $given_project_id;
  private $given_responsibility_id;
  private $given_goal_statement_id;
  private $given_process_id;
  private $given_scene_element_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_responsibility_id
  public function set_given_responsibility_id($var) {
    $this->given_responsibility_id = $var;
  }
  public function get_given_responsibility_id() {
    return $this->given_responsibility_id;
  }

  // given_goal_statement_id
  public function set_given_goal_statement_id($var) {
    $this->given_goal_statement_id = $var;
  }
  public function get_given_goal_statement_id() {
    return $this->given_goal_statement_id;
  }

    // given_process_id
  public function set_given_process_id($var) {
    // debug
    //print "debug timecards set_given_process_id = " . $var . "<br />\n";
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_scene_element_id
  public function set_given_scene_element_id($var) {
    $this->given_scene_element_id = $var;
  }
  public function get_given_scene_element_id() {
    return $this->given_scene_element_id;
  }

  // attributes
  private $id;
  private $doer_user_name;
  private $date;
  private $time_in;
  private $time_out;
  private $description;
  private $project_obj;
  private $process_obj;
  private $shift_obj;
  private $scene_element_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // doer_user_name
  public function set_doer_user_name($var) {
    $this->doer_user_name = $var;
  }
  public function get_doer_user_name() {
    return $this->doer_user_name;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // time_in
  public function set_time_in($var) {
    $this->time_in = $var;
  }
  public function get_time_in() {
    return $this->time_in;
  }

  // time_out
  public function set_time_out($var) {
    $this->time_out = $var;
  }
  public function get_time_out() {
    return $this->time_out;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      include_once("projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

  // process_obj
  public function get_process_obj() {
    if (! isset($this->process_obj)) {
      include_once("processes.php");
      $this->process_obj = new Processes($this->get_given_config());
    }
    return $this->process_obj;
  }

  // shift_obj
  public function get_shift_obj() {
    if (! isset($this->shift_obj)) {
      include_once("shifts.php");
      $this->shift_obj = new Shifts($this->get_given_config());
    }
    return $this->shift_obj;
  }

  // scene_element_obj
  public function get_scene_element_obj() {
    if (! isset($this->scene_element_obj)) {
      include_once("scene_elements.php");
      $this->scene_element_obj = new SceneElements($this->get_given_config());
    }
    return $this->scene_element_obj;
  }

  // method
  private function make_timecard() {
    $obj = new Timecards($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id() != "") {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id() != "") {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_responsibility_id() != "") {
      $this->set_type("get_by_responsibility_id");

    } else if ($this->get_given_goal_statement_id() != "") {
      $this->set_type("get_by_goal_statement_id");

    } else if ($this->get_given_process_id() != "") {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_scene_element_id() != "") {
      $this->set_type("get_by_scene_element_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // debug
    //print "debug timecards type = " . $this->get_type() . "<br />\n";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT timecards.*, projects.name, projects.img_url FROM timecards, projects WHERE timecards.id = " . $this->get_given_id() . " AND projects.id = timecards.project_id AND projects.user_name = '" . $this->get_user_obj()->name . "' ;";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT timecards.*, projects.name, projects.img_url FROM timecards, projects WHERE timecards.id = " . $this->get_given_id() . " AND projects.id = timecards.project_id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY timecards.date, timecards.time_in;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT timecards.*, projects.name, projects.img_url FROM timecards, projects WHERE projects.id = timecards.project_id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY timecards.date, timecards.time_in;";

      // debug
      //print "debug timecards sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "shifts_only") {
      // security: only get the rows owned by the user
      $sql = "SELECT timecards.*, projects.name, projects.img_url FROM timecards, projects, shifts WHERE shifts.id = timecards.shift_id AND projects.id = timecards.project_id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY timecards.date, timecards.time_in;";

    } else if ($this->get_type() == "get_by_responsibility_id") {
      // note: this goes through business_text_plans and not shifts
      // todo should use the projects and check the user_name ?
      $sql = "SELECT timecards.* FROM timecards, scene_elements, scene_element_responsibilities, responsibilities WHERE timecards.scene_element_id = scene_elements.id AND scene_elements.id = scene_element_responsibilities.scene_element_id AND scene_element_responsibilities.responsibility_id = responsibilities.id AND responsibilities.id = " . $this->get_given_responsibility_id() . " ORDER BY timecards.date, timecards.time_in;";

      // debug
      //print "debug timecards sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_goal_statement_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT timecards.* FROM timecards, processes, business_plan_texts, goal_statements WHERE processes.id = timecards.process_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statement_id = " . $this->get_given_goal_statement_id() . " AND timecards.doer_user_name = '" . $this->get_user_obj()->name . "' ORDER BY timecards.date, timecards.time_in;";

    } else if ($this->get_type() == "get_by_process_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT timecards.* FROM timecards, processes WHERE processes.id = timecards.process_id AND processes.id = " . $this->get_given_process_id() . " AND timecards.doer_user_name = '" . $this->get_user_obj()->name . "' ORDER BY timecards.date, timecards.time_in;";

    } else if ($this->get_type() == "get_by_scene_element_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT timecards.* FROM timecards, scene_elements WHERE scene_elements.id = timecards.scene_element_id AND scene_elements.id = " . $this->get_given_scene_element_id() . " AND timecards.doer_user_name = '" . $this->get_user_obj()->name . "' ORDER BY timecards.date, timecards.time_in;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_date = "plantdot_soiltoil";

    // execute
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_date);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "shifts_only") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_timecard();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_doer_user_name(pg_result($results, $lt, 1));
        $obj->set_date(pg_result($results, $lt, 2));
        $obj->set_time_in(pg_result($results, $lt, 3));
        $obj->set_time_out(pg_result($results, $lt, 4));
        $obj->set_description(pg_result($results, $lt, 5));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 6));
        $obj->get_process_obj()->set_id(pg_result($results, $lt, 7));
        $obj->get_shift_obj()->set_id(pg_result($results, $lt, 8));
        $obj->get_scene_element_obj()->set_id(pg_result($results, $lt, 9));
        $obj->get_project_obj()->set_name(pg_result($results, $lt, 10));
        $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 11));
      }
    } else if ($this->get_type() == "get_by_responsibility_id" ||
              $this->get_type() == "get_by_goal_statement_id" ||
              $this->get_type() == "get_by_process_id" ||
              $this->get_type() == "get_by_scene_element_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_timecard();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_doer_user_name(pg_result($results, $lt, 1));
        $obj->set_date(pg_result($results, $lt, 2));
        $obj->set_time_in(pg_result($results, $lt, 3));
        $obj->set_time_out(pg_result($results, $lt, 4));
        $obj->set_description(pg_result($results, $lt, 5));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 6));
        $obj->get_process_obj()->set_id(pg_result($results, $lt, 7));
        $obj->get_shift_obj()->set_id(pg_result($results, $lt, 8));
        $obj->get_scene_element_obj()->set_id(pg_result($results, $lt, 9));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // set up (get ready for loop by initializing an object)
    include_once("lib/time_measurements.php");
    $time_measurements_obj = new TimeMeasurements();
    $grand_total_decimal_time = 0;

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    doer user name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    time in\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    time out\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"background-color: #E6E6FA;\">\n";
    $markup .= "    hours (derived)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    process\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    shift\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    scene_element\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $timecard) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $timecard->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $timecard->get_doer_user_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $timecard->get_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $timecard->get_time_in() . "\n";
      $markup .= "  </td>\n";
  
      $markup .= "  <td>\n";
      $markup .= "    " . $timecard->get_time_out() . "\n";
      $markup .= "  </td>\n";

      // hours (derived)
      $markup .= "  <td style=\"text-align: right; background-color: #E6E6FA;\">\n";
      // todo need to make the following in the database, so user can set it
      // hard-coded
      $given_subtotal_string = "Frank";
      //print $timecard->get_description() . "<br />\n";
      $total_decimal_time = $time_measurements_obj->measure_time($timecard->get_id(), $timecard->get_date(), $timecard->get_time_in(), $timecard->get_time_out(), $given_subtotal_string, $timecard->get_description());
      $grand_total_decimal_time += $total_decimal_time;
      $markup .= "    " . $total_decimal_time . "\n";

      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $timecard->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $timecard->get_project_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      // old way
      //$markup .= "    " . $timecard->get_process_obj()->get_id() . "\n";
      // new fancy way
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $timecard->get_process_obj()->get_name_with_link_given_id($timecard->get_process_obj()->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      // todo the following is where because it is calling an object...
      // and passing a parameter with info to it at the same time...
      // info that is provided by itself
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $timecard->get_shift_obj()->get_name_with_link_given_id($timecard->get_shift_obj()->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $timecard->get_scene_element_obj()->get_name_with_link_given_id($timecard->get_scene_element_obj()->get_id(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    // totals
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"6\">\n";
    $markup .= "    grand_total_decimal_time\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"text-align: right; background-color: #BCD2EE;\">\n";
    $markup .= "    " . $grand_total_decimal_time . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td colspan=\"6\">\n";
    $markup .= "    \n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"6\">\n";
    $markup .= "    grand_subtotal_string hours\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"text-align: right; background-color: #BCD2EE;\">\n";
    $grand_subtotal_string_hours = $time_measurements_obj->get_grand_subtotal_string_hours();
    $markup .= "    " . $grand_subtotal_string_hours . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td colspan=\"5\">\n";
    $markup .= "    \n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"6\">\n";
    $markup .= "    week_count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"text-align: right; background-color: #BCD2EE;\">\n";
    $week_count = $time_measurements_obj->get_week_count();
    $markup .= "    " . $week_count . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td colspan=\"5\">\n";
    $markup .= "    \n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"6\">\n";
    $markup .= "    average hours per week\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"text-align: right; background-color: #BCD2EE;\">\n";
    if ($week_count != 0) {
      $average_decimal_time_per_week = $grand_total_decimal_time / $week_count;
    } else {
      $average_decimal_time_per_week = "error: cannot divide by zero";
    }
    $markup .= "    " . round($average_decimal_time_per_week) . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td colspan=\"5\">\n";
    $markup .= "    \n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<p><strong>" . $this->get_date() . "</strong></p>\n";

    $markup .= "<p>";
    if ($this->get_description()) {
      $markup .= $this->get_description();
    }
    $markup .= "</p>";

    return $markup;
  }

  // method
  public function get_project_id_given_timecard_id($given_timecard_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_timecard_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_project_obj()->get_id();
      // debug
      //print "debug timecards pproject_id = " . $project_id . "<br />\n"; 
      $markup .= $project_id;
    }

    return $markup;
  }

  // method
  public function get_total_hours_from_timecards_given_project_id($given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();
    // note: check for errors
    if ($markup) {
      return $markup;
    }

    // declare variables for tally-taking
    $total_hours = 0;
    $grand_total_hours = 0;

    // only load object if objects where found
    if ($this->get_list_bliss()->get_count() > 0) {

      // set up (get ready for loop by initializing an object)
      include_once("time_measurements.php");
      $time_measurements_obj = new TimeMeasurements();
      foreach ($this->get_list_bliss()->get_list() as $timecard) {
        // sum time here
        $given_subtotal_string = "";
        $total_hours = $time_measurements_obj->measure_time($timecard->get_id(), $timecard->get_date(), $timecard->get_time_in(), $timecard->get_time_out(), $given_subtotal_string, $timecard->get_description());
        $grand_total_hours += $total_hours;
      }
    }

    // note: format the number of hours so that it is expressed in tenths
    $markup .= number_format($grand_total_hours, 1);

    // convert 0.0 to just 0 
    if ($markup) {
      $markup = "0";
    }

    return $markup;
  }

  // method
  public function get_total_hours_from_timecards_with_shifts_given_project_id($given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->set_type("shifts_only");
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    $total_hours = 0;
    $grand_total_hours = 0;
    if ($this->get_list_bliss()->get_count() > 0) {

      // set up (get ready for loop by initializing an object)
      include_once("time_measurements.php");
      $time_measurements_obj = new TimeMeasurements();
      foreach ($this->get_list_bliss()->get_list() as $timecard) {
        // sum time here
        $given_subtotal_string = "";
        $total_hours = $time_measurements_obj->measure_time($timecard->get_id(), $timecard->get_date(), $timecard->get_time_in(), $timecard->get_time_out(), $given_subtotal_string, $timecard->get_description());
        $grand_total_hours += $total_hours;
      }
    }

    // note: format the number of hours so that it is expressed in tenths
    $markup .= number_format($grand_total_hours, 1);

    return $markup;
  }

  // method
  private function get_total_time() {
    $total_time = 0;

    // note: do the arithmetic onlyif the data exists
    if ($this->get_time_in() && $this->get_time_out()) {
      // note: here is the forumula for "total_time"
      $total_time = $this->get_time_out() - $this->get_time_in();
    }

    return $total_time;
  } 

  // method
  public function insert() {
    $sql = "INSERT INTO timecards (doer_user_name, date, time_in, time_out, description, project_id, process_id) VALUES ('" . $this->get_doer_user_name() . "', '" . $this->get_date() . "', '" . $this->get_time_in() . "', '" . $this->get_time_out() . "', '" . $this->get_description() . "', " . $this->get_project_obj()->get_id() . ", " . $this->get_process_obj()->get_id() .");";
    print "sql = " . $sql . "<br />\n";
    $database_name = "plantdot_soiltoil";
    $error_message = $this->get_db_dash()->new_insert($database_name, $sql);
    return $error_message;
  }

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function get_timecard_array_given_responsibility_id($given_responsibility_id, $given_user_obj) {

    // set
    $this->set_given_responsibility_id($given_responsibility_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      print "error timecards " . $markup . "<br />\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_list();
  }

  // method
  public function get_count_given_process_id($given_process_id, $given_user_obj) {

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error timecards " . $markup . "</p>\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_count_given_scene_element_id($given_scene_element_id, $given_user_obj) {

    // set
    $this->set_given_scene_element_id($given_scene_element_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error timecards " . $markup . "</p>\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_count_given_project_id($given_project_id, $given_user_obj) {

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error timecards " . $markup . "</p>\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_total_hours_given_goal_statement_id($given_goal_statement_id, $given_user_obj) {
    $markup = "";

    // todo one day may want to limit the time period

    // set
    $this->set_given_goal_statement_id($given_goal_statement_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error timecards " . $markup . "</p>\n";
    }

    // set up (get ready for loop by initializing an object)
    include_once("lib/time_measurements.php");
    $time_measurements_obj = new TimeMeasurements();

    $total_time = 0;
    // only output if there are items to output
    foreach ($this->get_list_bliss()->get_list() as $timecard) {
      $given_subtotal_string = "";
      $total_time += $time_measurements_obj->measure_time($timecard->get_id(), $timecard->get_date(), $timecard->get_time_in(), $timecard->get_time_out(), $given_subtotal_string, $timecard->get_description());
    }

    return $total_time;
  }

}
