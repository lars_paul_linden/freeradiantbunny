<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/object_images.php

include_once("lib/scrubber.php");

class ObjectImages extends Scrubber {

  // given
  private $given_class_name_string;
  private $given_primary_key_string;
  private $given_image_id;
 
  // given_class_name_string
  public function set_given_class_name_string($var) {
    $this->given_class_name_string = $var;
  }
  public function get_given_class_name_string() {
    return $this->given_class_name_string;
  }

  // given_primary_key_string
  public function set_given_primary_key_string($var) {
    $this->given_primary_key_string = $var;
  }
  public function get_given_primary_key_string() {
    return $this->given_primary_key_string;
  }

  // given_image_id
  public function set_given_image_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "image_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_image_id = $var;
    }
  }
  public function get_given_image_id() {
    return $this->given_image_id;
  }

  // attributes
  private $id;
  private $class_primary_key_string;
  private $class_name_string;
  private $image_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // class_primary_key_string
  public function set_class_primary_key_string($var) {
    $this->class_primary_key_string = $var;
  }
  public function get_class_primary_key_string() {
    return $this->class_primary_key_string;
  }

  // class_name_string
  public function set_class_name_string($var) {
    $this->class_name_string = $var;
  }
  public function get_class_name_string() {
    return $this->class_name_string;
  }

  // image_obj
  public function get_image_obj() {
    if (! isset($this->image_obj)) {
      include_once("images.php");
      $this->image_obj = new Images($this->get_given_config());
    }
    return $this->image_obj;
  }

  // method
  private function make_object_image() {
    $obj = new ObjectImages($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      // single
      $this->set_type("get_given_id");

    } else if ($this->get_given_primary_key_string()) {
      $this->set_type("get_by_primary_key_string");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

//      $sql = "SELECT plant_images.id, plant_images.plant_id, plant_images.image_id, images.caption, images.description, images.photographer, images.url, images.license FROM plant_images, images WHERE plant_images.image_id = images.id AND plant_images.plant_id = " . $this->get_given_plant_id() . ";";

//      $sql = "SELECT plant_images.id, plant_images.plant_id, plant_images.image_id, images.caption, images.description, images.photographer, images.url, images.license, plants.common_name FROM plant_images, images, plants WHERE plants.id = plant_images.plant_id AND plant_images.image_id = images.id;";

    // figure out what to load
    if ($this->get_type() == "get_given_id") {
      $sql = "";

    } else if ($this->get_type() == "get_by_primary_key_string") {
      $sql = "SELECT object_images.* FROM object_images WHERE object_images.class_name_string = '" . $this->get_given_class_name_string() . "' AND object_images.class_primary_key_string = '" . $this->get_given_primary_key_string() . "';";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT object_images.id, object_images.primary_key_string, object_images.image_id, images.caption, images.description, images.photographer, images.url, images.license, plants.common_name FROM object_images, images, plants WHERE plants.id = object_images.primary_key_string AND object_images.image_id = images.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }


  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_object_image();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_image_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_class_primary_key_string(pg_result($results, $lt, 2));
        $obj->set_class_name_string(pg_result($results, $lt, 3));
        $obj->get_image_obj()->set_caption(pg_result($results, $lt, 4));
        $obj->get_image_obj()->set_description(pg_result($results, $lt, 5));
        $obj->get_image_obj()->set_photographer(pg_result($results, $lt, 6));
        $obj->get_image_obj()->set_url(pg_result($results, $lt, 7));
        $obj->get_image_obj()->set_license(pg_result($results, $lt, 8));
        //$obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 9));
      }
    } else if ($this->get_type() == "get_by_primary_key_string") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_object_image();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_image_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_class_primary_key_string(pg_result($results, $lt, 2));
        $obj->set_class_name_string(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method     
  public function output_subsubmenu_get_all() {
      $markup = "";

    if ($this->get_type() == "get_all") {
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("images");
      $markup .= "  See Also: <a href=\"" . $url . "\">Images</a><br />\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    image\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // show crop plan plants error report
    $primary_key_string_list = array();

    // rows
    foreach ($this->get_list_bliss()->get_list() as $plant_image) {

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_image->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("images/" . $plant_image->get_image_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $plant_image->get_image_obj()->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $url = $this->url("plants" . $plant_image->get_plant_obj()->get_id());
    $markup .= "    <a href=\"" . $url . "\">" . $plant_image->get_plant_obj()->get_id() . "</a><br />\n";
    $markup .= "  <br />\n";
    $url = $this->url("images/" . $plant_image->get_image_obj()->get_id());
    $markup .= "    <a href=\"" . $url . "</a><br />\n";

    return $markup;
  }

  // method
  public function output_images($given_class_name_string, $given_primary_key_string, $caption_flag) {
    $markup = "";

    // set
    $this->set_given_class_name_string($given_class_name_string);
    $this->set_given_primary_key_string($given_primary_key_string);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $plant_image) {
      $markup .= $plant_image->get_image_obj()->output_image_html($caption_flag);
    }

    return $markup;
  }

// todo move to menu function
//    if ($this->get_type() == "get_all") {
//      $markup .= "<div class=\"subsubmenu\">\n";
//      $url = $this->url("plants");
//      $markup .= "  See Also: <a href=\"" . $url . "\">Plants</a><br />\n";
//      $url = $this->url("images");
//      $markup .= "  See Also: <a href=\"" . $url . "\">Images</a><br />\n";
//      $markup .= "</div>\n";
//    }

  // method
  protected function x_output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    image\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // show crop plan plants error report
    $plant_id_list = array();

    // rows
    foreach ($this->get_list_bliss()->get_list() as $plant_image) {

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_image->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("images/" . $plant_image->get_image_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $plant_image->get_image_obj()->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function x_output_single() {
    $markup = "";

    $url = $this->url("plants" . $plant_image->get_plant_obj()->get_id());
    $markup .= "    <a href=\"" . $url . "\">" . $plant_image->get_plant_obj()->get_id() . "</a><br />\n";
    $markup .= "  <br />\n";
    $url = $this->url("images/" . $plant_image->get_image_obj()->get_id());
    $markup .= "    <a href=\"" . $url . "</a><br />\n";

    return $markup;
  }

}
