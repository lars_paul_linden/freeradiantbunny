<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-20
// version 1.2 2015-01-04
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/email_addresses.php

include_once("lib/socation.php");

class EmailAddresses extends Socation {

  // given
  private $given_domain_tli;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // attributes
  private $address;
  private $user_name;

  // address
  public function set_address($var) {
    $this->address = $var;
  }
  public function get_address() {
    return $this->address;
  }

  public function get_sort_no_z() {
    // assumes "Z " is at the beginning of string
    return substr($this->sort, 2);
  }

  // primary_key
  public function get_primary_key() {
    return $this->get_id();
  }
  public function get_class_primary_key_string() {
    return $this->get_id();
  }

  public function get_name_with_link_natural() {
    $markup = "";

    if ($this->get_name()) {
      $name = $this->get_name();
    } else {
      // no name, so try id
      if ($this->get_id()) {
        $name = "id=" . $this->get_id();
      } else {
        // no id, so try question mark
        $name = "?";
      }      
    }
    include_once("lib/factory.php");
    $factory = new Factory($this->get_given_config());
    $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    $markup .= $name;
    $markup .= "</a>";
    return $markup;
  }

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // img_url
  public function get_img_url() {
    // todo move to config file
    return "http://mudia.com/dash/_images/email_stamp2.png";
  }

  public function get_img_url_as_img_element($given_padding = "0px 0px 0px 0px", $float = "", $width = "66", $height = "99") {
    $markup = "";

    if (! $this->get_img_url()) {
      return "[img]";
    }
    $alt = "icon";
    $markup .= "<img src=\"" . $this->get_img_url() . "\" alt=\"" . $alt . "\" width=\"66\" />";

    return $markup;
  }

  // method
  private function make_email_address() {
    $obj = new EmailAddresses($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT email_addresses.* FROM email_addresses WHERE email_addresses.id = " . $this->get_given_id() . " AND email_addresses.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug email_addresses get_by_id sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT email_addresses.* FROM email_addresses WHERE email_addresses.user_name = '" . $this->get_user_obj()->name . "' ORDER BY email_addresses.name, email_addresses.sort DESC, email_addresses.id;";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT email_addresses.* FROM email_addresses, host_email_addresses WHERE email_addresses.id = host_email_addresses.email_address_id AND host_email_addresses.domain_tli = '" . $this->get_given_domain_tli() . "';";

      // debug
      //print "debug email_addresses sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_email_address();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_address(pg_result($results, $lt, 1));
        $obj->set_name(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_user_name(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->set_description(pg_result($results, $lt, 7));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // may be a subset
    if ($this->get_given_domain_tli()) {
      $url = $this->url("domains/" . $this->get_given_domain_tli());
      $markup .= "<p><a href=\"" . $url . "\">" . $this->get_given_domain_tli() . "</a></p>\n";
    }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    address\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    user_name\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $email_address) {

      $markup .= "<tr>\n";

      // num
      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= "  <td>\n";
      $markup .= "    " . $email_address->get_sort() . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $email_address->get_id() . "\n";
      $markup .= "  </td>\n";

      // img_url
      $markup .= "  <td>\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      // todo be able to configure the width of the process img_element
      $width = "20";
      $height = "20";
      $markup .= "    " . $email_address->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "    " . $email_address->get_name() . "\n";
      $markup .= "  </td>\n";

      // address
      $markup .= "  <td>\n";
      $markup .= "    " . $email_address->get_address() . "\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $email_address->get_description() . "\n";
      $markup .= "  </td>\n";

      // user_name
      $markup .= "  <td>\n";
      $markup .= "    " . $email_address->get_user_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";


    return $markup;
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $email_address) {
      $markup .= "<p>id = " . $email_address->get_id() . "</p>\n";
      $markup .= "<h2>" . $email_address->get_name() . "</h2>\n";
      $markup .= "<p>" . $email_address->get_address() . "</p>\n";
      $markup .= "<br />\n";
      $markup .= "<p>sort = " . $email_address->get_sort() . "</p>\n";
      $markup .= "<p>user_name = " . $email_address->get_user_name() . "</p>\n";
    }

    return $markup;
  }

  // method
  public function get_email_addresses_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $email_address) {
        $url = $this->url("email_addresses/" . $email_address->get_id());
        $markup .= "<a href=\"" . $url . "\" class=\"show\">";
        $markup .= $email_address->get_address();
        $markup .= "</a><br />";
      }
    } else {
      $markup .= "<p>[No email_addresses found.]</p>\n";
    }

    return $markup;
  }

  // method
  public function get_email_address_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $email_address) {
        $url = $this->url("email_addresses/" . $email_address->get_id());
        $markup .= "<a href=\"" . $url . "\" class=\"show\">";
        $markup .= $email_address->get_address();
        $markup .= "</a><br />";
      }
    } else {
      $markup .= "No email_address found.\n";
    }

    return $markup;
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug email_addresses: ignore, not related<br />\n";
    } else {
      $markup .= "debug email_addresses: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

}
