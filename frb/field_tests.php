<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2013-04-02
// version 1.6 2016-05-07

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/field_tests.php

include_once("class_standard.php");

class FieldTests extends Standard {

  // attributes
  private $id;
  private $date;
  private $farmer;
  private $garment;
  private $farm_task;
  private $field_report;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // farmer
  private function set_farmer($var) {
    $this->farmer = $var;
  }
  private function get_farmer() {
    return $this->farmer;
  }

  // garment
  private function set_garment($var) {
    $this->garment = $var;
  }
  private function get_garment() {
    return $this->garment;
  }

  // farm_task
  private function set_farm_task($var) {
    $this->farm_task = $var;
  }
  private function get_farm_task() {
    return $this->farm_task;
  }

  // field_report
  private function set_field_report($var) {
    $this->field_report = $var;
  }
  private function get_field_report() {
    return $this->field_report;
  }

  // method
  private function make_field_test() {
    $obj = new FieldTests();
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "select * from field_tests;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT field_tests.* from field_tests ORDER BY id";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $yield = $this->make_field_test();
        $yield->set_id(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

}
