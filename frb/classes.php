<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04
// version 1.5 2015-10-16
// version 1.6 2017-01-28
// version 1.7 2017-04-22

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/classes.php

include_once("lib/socation.php");

class Classes extends Socation {

  // parameters
  // todo note the previous defaults as a history
  private $given_view = "default";
  private $given_sort = "sort";
  private $given_table_name;
  private $given_name;

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // given_sort
  public function set_given_sort($var) {
    $this->given_sort = $var;
  }
  public function get_given_sort() {
    return $this->given_sort;
  }

  // given_table_name
  public function set_given_table_name($var) {
    $this->given_table_name = $var;
  }
  public function get_given_table_name() {
    return $this->given_table_name;
  }

  // given_name
  public function set_given_name($var) {
    $this->given_name = $var;
  }
  public function get_given_name() {
    return $this->given_name;
  }

  // attributes
  private $ownership;
  private $notes;
  private $db_mysql;
  private $user_name;
  private $zachman_id;
  private $fields;
  private $relationship;
  private $db_postgres;
  private $extends_class_id;
  private $rank;
  private $subsystem;
  private $spare_code;

  // special
  private function get_name_as_meta_link() {
    $markup = "";

    $url = ("../" . $this->get_name());
    $markup .= "<a href=\"" . $url . "\">" . $this->get_name() . "</a>";

    return $markup;
  }

  // ownership
  public function set_ownership($var) {
    $this->ownership = $var;
  }
  public function get_ownership() {
    return $this->ownership;
  }

  // notes
  public function set_notes($var) {
    $this->notes = $var;
  }
  public function get_notes() {
    return $this->notes;
  }

  // db_mysql
  public function set_db_mysql($var) {
    $this->db_mysql = $var;
  }
  public function get_db_mysql() {
    return $this->db_mysql;
  }

  // db_postgres
  public function set_db_postgres($var) {
    $this->db_postgres = $var;
  }
  public function get_db_postgres() {
    return $this->db_postgres;
  }

  // fields
  public function set_fields($var) {
    $this->fields = $var;
  }
  public function get_fields() {
    return $this->fields;
  }

  // zachman_id
  public function set_zachman_id($var) {
    $this->zachman_id = $var;
  }
  public function get_zachman_id() {
    return $this->zachman_id;
  }

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // relationship
  public function set_relationship($var) {
    $this->relationship = $var;
  }
  public function get_relationship() {
    return $this->relationship;
  }

  // extends_class_id
  public function set_extends_class_id($var) {
    $this->extends_class_id = $var;
  }
  public function get_extends_class_id() {
    return $this->extends_class_id;
  }

  // rank
  public function set_rank($var) {
    $this->rank = $var;
  }
  public function get_rank() {
    return $this->rank;
  }

  // subsystem
  public function set_subsystem($var) {
    $this->subsystem = $var;
  }
  public function get_subsystem() {
    return $this->subsystem;
  }

  // spare_code
  public function set_spare_code($var) {
    $this->spare_code = $var;
  }
  public function get_spare_code() {
    return $this->spare_code;
  }

  // method
  private function make_class() {
    $obj = new Classes($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_table_name()) {
        $this->set_type("get_by_table_name");

    } else if ($this->get_given_name()) {
        $this->set_type("get_by_given_name");

    } else if ($this->get_given_project_id()) {
        $this->set_type("get_by_project_id");

    } else {
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      if ($this->get_given_id()) {
        $sql = "SELECT classes.* FROM classes WHERE classes.id = " . $this->get_given_id() . " AND classes.user_name = '" . $this->get_user_obj()->name . "';";
      } else {
        $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": given_id is not known. Unable to load data.");
      }

    } else if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT DISTINCT ON (classes.sort, classes.id) classes.* FROM classes, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'classes' AND classes.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY classes.sort DESC, classes.id;";

    } else if ($this->get_type() == "get_by_name") {
      // security: only get the rows owned by the user
      $sql = "SELECT classes.* FROM classes WHERE classes.name = '" . $this->get_given_name() . "' AND classes.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_by_table_name") {
      // security: only get the rows owned by the user
      $sql = "SELECT classes.* FROM classes WHERE classes.name = '" . $this->get_given_table_name() . "' AND classes.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_all") {
      if ($this->get_given_view() == "db_postgres") {
        // security: only get the rows owned by the user
        $sql = "SELECT classes.* FROM classes WHERE classes.user_name = '" . $this->get_user_obj()->name . "' AND classes.status = '1.6' ORDER BY classes.notes, classes.notes, classes.relationship, classes.zachman_id, classes.name;";
      } else {
        // name (default)
        $order_by = "ORDER BY classes.name, classes.subsystem, classes.extends_class_id, classes.notes";
        if ($this->get_given_sort() == "sort") {
          $order_by = "ORDER BY classes.sort DESC, classes.subsystem, classes.notes, classes.extends_class_id, classes.name";
        } else if ($this->get_given_sort() == "status") {
          $order_by = "ORDER BY classes.status DESC, classes.subsystem, classes.extends_class_id, classes.notes";
        } else if ($this->get_given_sort() == "subsystem") {
          $order_by = "ORDER BY classes.subsystem, classes.notes, classes.extends_class_id, classes.status DESC";
        } else if ($this->get_given_sort() == "extends") {
          $order_by = "ORDER BY classes.extends_class_id, classes.status DESC, classes.subsystem, classes.id";
        } else if ($this->get_given_sort() == "notes") {
          $order_by = "ORDER BY classes.notes, classes.status DESC, classes.subsystem, classes.extends_class_id";
        } else if ($this->get_given_sort() == "rank") {
          $order_by = "ORDER BY classes.rank, classes.notes";
        }
        // todo know that this is where info about the sort changes things
        // todo create a function here perhaps

        // no-lib
        // sort by filter and name (see details in note below)
        // note filtering out status=0 and name=lib/% then sort by name 
        if ($this->get_given_view() == "no-lib") {
          $sql = "SELECT classes.* FROM classes WHERE NOT classes.status = '0' AND NOT classes.name like 'lib/%' " . $order_by . ";";
        } else {
          // default view get_all
          // this does not get the offline classes but it was too restrictive
          //$sql = "SELECT classes.* FROM classes WHERE NOT classes.status = 'offline' " . $order_by . ";";
          $sql = "SELECT classes.* FROM classes " . $order_by . ";";
        }

        // sort by name (heads-up this shows the '0')
        //$sql = "SELECT classes.* FROM classes WHERE classes.user_name = '" . $this->get_user_obj()->name . "' ORDER BY classes.name, classes.subsystem, classes.extends_class_id, classes.notes;";


        // sort by status special alpha sort
        //$sql = "SELECT classes.* FROM classes WHERE NOT classes.status = '0' ORDER BY classes.status DESC, classes.name, classes.subsystem, classes.extends_class_id, classes.notes;";

        // sort by status and subsystems special
        //$sql = "SELECT classes.* FROM classes WHERE NOT classes.status = '0' ORDER BY classes.name, classes.status DESC, classes.subsystem, classes.extends_class_id, classes.notes;";

      }

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM classes;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_name" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_table_name" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_class();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_ownership(pg_result($results, $lt, 3));
        $obj->set_notes(pg_result($results, $lt, 4));
        $obj->set_db_mysql(pg_result($results, $lt, 5));
        $obj->set_user_name(pg_result($results, $lt, 6));
        $obj->set_zachman_id(pg_result($results, $lt, 7));
        $obj->set_fields(pg_result($results, $lt, 8));
        $obj->set_sort(pg_result($results, $lt, 9));
        $obj->set_status(pg_result($results, $lt, 10));
        $obj->set_img_url(pg_result($results, $lt, 11));
        $obj->set_relationship(pg_result($results, $lt, 12));
        $obj->set_db_postgres(pg_result($results, $lt, 13));
        $obj->set_extends_class_id(pg_result($results, $lt, 14));
        $obj->set_spare_code(pg_result($results, $lt, 15));
        $obj->set_rank(pg_result($results, $lt, 16));
        $obj->set_subsystem(pg_result($results, $lt, 17));
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_class();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_class();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo develop output_subsubmenu()

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // todo develop output_given_variables()

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_view();
    $markup .= $this->output_sort();

    if ($this->get_given_view() == "default") {

      $markup .= $this->output_default_display();

      // todo add the following to the view menu
      //$markup .= $this->output_website_simple_list_display

    } else if ($this->get_given_view() == "db_postgres") {
      $markup .= $this->output_db_postgres_display();
    }

    return $markup;
  }

  // method
  protected function output_default_display() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $short_table = 0;

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 40px;\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    subsystem\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    extends\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    rank\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    rows\n";
    $markup .= "  </td>\n";

    $short_table = 0;
    if ($short_table) {
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    z.\n";
      $markup .= "  </td>\n";
    }
    if ($short_table) {
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    relationship\n";
      $markup .= "  </td>\n";
    }
    if ($short_table) {
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    db_postgres\n";
      $markup .= "  </td>\n";
    }

    // notes
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    notes\n";
    $markup .= "  </td>\n";

    // s.e.
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    s.e.\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $class) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td style=\"text-align: center;\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // old
      //$column_name = "sort";
      //include_once("lib/timekeeper.php");
      //$timekeeper_obj = new Timekeeper();
      //$color = $timekeeper_obj->calculate_cell_color($column_name, $class->get_sort());
      //$markup .= "  <td valign=\"top\" style=\"background: $color;\">\n";
      //$markup .= "    " . $class->get_sort() . "\n";
      //$markup .= "  </td>\n";
      // new
      // sort
      $markup .= $class->get_sort_cell();

      // status (version)
      if ($class->get_status() == "1.0") {
        $markup .= "  <td align=\"left\" style=\"background-color: green;\">\n";
      } else if ($class->get_status() == "zoneline") {
        // purple
        $markup .= "  <td align=\"left\" style=\"background-color: #C0509F;\">\n";
      } else {
         $markup .= "  <td align=\"left\" style=\"background-color: #EFEFEF;\">\n";
      }
      $markup .= "    " . $class->get_status() . "\n";
      $markup .= "  </td>\n";

      // subsystem
      // colorize the groups
      $color_1 = "#84BEFA"; # green
      $color_2 = "#33811D"; # dark green
      $color_3 = "#3D77EF"; # blue
      $color_4 = "#7F49D0"; # purple
      $color_5 = "#E28D31"; # orange
      $color_6 = "#CD5555"; # red
      $color_text_light = "#3BF965"; # yellow-green 
      $color_text_dark = "#000000"; # dark grey
   
      // default
      $color = "#CCCCCC";
      $color_text = $color_text_dark;

      // polymorphic subsystem

      // subsystem defined
      $pattern = '/Meta/';
      if (preg_match($pattern, $class->get_subsystem())) {
        $color = $color_3;
        $color_text = $color_text_light;
      }

      // subsystem defined
      // inquiring systems subsystem
      $pattern = '/Inquiring Systems/';
      if (preg_match($pattern, $class->get_subsystem())) {
        $color = $color_1;
        $color_text = $color_text_dark;
      }

      // subsystem defined
      // aggregator subsystem
      $pattern = '/Aggregator/';
      if (preg_match($pattern, $class->get_subsystem())) {
        $color = $color_1;
        $color_text = $color_text_dark;
      }

      $markup .= "  <td style=\"background-color: " . $color . "; color: " . $color_text . "; text-align: center; padding: 2px;\">\n";
      $markup .= "    " . $class->get_subsystem() . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td style=\"text-align: right; padding: 2px;\">\n";
      $url = $this->url("classes/" . $class->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $class->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td style=\"text-align: left; padding: 2px;\">\n";
      $url = $this->url("classes/" . $class->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $class->get_name() . "</a>\n";
      $markup .= "  </td>\n";

      // extends
      if (1) {
        // todo associate colors with class and store hash in system config
        if ($class->get_extends_class_id() == "142") {
          $markup .= "  <td align=\"right\" style=\"background-color: green;\">\n";
        } else if ($class->get_extends_class_id() == "143") {
          $markup .= "  <td align=\"right\" style=\"background-color: orange;\">\n";
        } else if ($class->get_extends_class_id() == "144") {
          $markup .= "  <td align=\"right\" style=\"background-color: yellow;\">\n";
        } else if ($class->get_extends_class_id() == "163") {
          $markup .= "  <td align=\"right\" style=\"background-color: #BFEFFF\">\n";
        } else if ($class->get_extends_class_id() == "158") {
          $markup .= "  <td align=\"right\" style=\"background-color: #AA99FF; color: #EFEFEF;\">\n";
        } else if ($class->get_extends_class_id() == "181") {
          $markup .= "  <td align=\"right\" style=\"background-color: #3399EE; color: #000000;\">\n";
        } else {
          $markup .= "  <td align=\"right\" style=\"background-color: #CCCCCC;\">\n";
        }
        $url = $this->url("classes/" . $class->get_extends_class_id());
        $markup .= "    <a href=\"" . $url . "\">" . $class->get_extends_class_id() . "</a>\n";
        $markup .= "  </td>\n";
      }

      // rank
      $rank_data = $class->get_rank();
      // change color based on value
      $color_blue = "#AADDFA"; # very light blue
      $color = "#CCCCCC";
      if ($rank_data == "999") {
        // skip
      } else if ($rank_data) {
        $color = $color_blue;
      }
      $markup .= "  <td style=\"background: $color; text-align: right; padding: 2px;\">\n";
      $markup .= "    " . $rank_data . "\n";
      $markup .= "  </td>\n";

      // row count (derived)
      //$row_count_data = $class->get_row_count_given_class($class->get_name());
      // todo the above was not working, so there is code below
      $row_count_data = "";
      // change color based on value
      $color_blue = "#AADDFA"; # very light blue
      $color = "#CCCCCC";
      if ($row_count_data) {
        $color = $color_blue;
      }
      $markup .= "  <td style=\"background: $color; text-align: right; padding: 2px;\">\n";
      $markup .= "    " . $row_count_data . "\n";
      $markup .= "  </td>\n";

      // zachman
      $short_table = 0;
      if ($short_table) {
        $markup .= "  <td align=\"middle\">\n";
        // todo build zachman class
        //include_once("zachmans.php");
        //$this->zachman_obj = new Zachmans($this->get_given_config());
        $markup .= "    " . $class->get_zachman_id() . "\n";
        $markup .= "  </td>\n";
      }

      if ($short_table) {
        if ($class->get_relationship() == "container") {
          $markup .= "  <td style=\"background-color: #B5E2AA;\">\n";
        } else if ($class->get_relationship() == "binary&nbsp;reflexive") {
          $markup .= "  <td style=\"background-color: #2299DD;\">\n";
        } else if (strstr($class->get_relationship(), "locations")) {
          $markup .= "  <td style=\"background-color: blueviolet; color: #EFEFEF;\">\n";
        } else if (strstr($class->get_relationship(), "binary&nbsp;nonreflexive")) {
          $markup .= "  <td style=\"background-color: #46A344;\">\n";
        } else if ($class->get_relationship() == "generalization") {
          $markup .= "  <td style=\"background-color: yellow;\">\n";
        } else if ($class->get_relationship() == "/lib class") {
          $markup .= "  <td style=\"background-color: #AAAAFF;\">\n";
        } else if (strstr($class->get_relationship(), "polymorphic")) {
          $markup .= "  <td style=\"background-color: #F3D333;\">\n";

        } else {
          $markup .= "  <td>\n";
        }
        $markup .= "    " . $class->get_relationship() . "\n";
        $markup .= "  </td>\n";
      }

      if ($short_table) {
        // db_postgres
        $postgres = strpos($class->get_db_postgres(), "CREATE TABLE");
        if ($postgres) {
          $markup .= "  <td style=\"background-color: #B5E2F3;\">\n";
        } else {
          $markup .= "  <td>\n";
        }
        $lines_in_postgres = substr_count($class->get_db_postgres(), "\n");
        $markup .= "    " . $lines_in_postgres . "\n";
        $markup .= "  </td>\n";
      }

      // notes
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_notes() . "\n";
      $markup .= "  </td>\n";

      // s.e.
      include_once("scene_elements.php");
      $scene_element_obj = new SceneElements($this->get_given_config());
      $class_id = $class->get_id();
      $user_obj = $this->get_user_obj();
      $scene_element_data = $scene_element_obj->get_scene_element_name_given_class_id($class_id, $user_obj);
      // change color based on value
      $color_blue = "#AADDFA"; # very light blue
      $color = "#CCCCCC";
      if ($scene_element_data) {
        $color = $color_blue;
      }
      $markup .= "  <td style=\"background: $color;\">\n";
      $markup .= $scene_element_data;
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_website_simple_list_display() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $short_table = 0;

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $class) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td style=\"text-align: center;\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td style=\"text-align: right; padding: 2px; text-align: center;\">\n";
      $url = "http://freeradiantbunny.org/main/en/docs/frb/" . $class->get_name() . ".php";
      $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">" . $class->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td style=\"text-align: left; padding: 2px 2px 2px 8px;\">\n";
      $url = "http://freeradiantbunny.org/main/en/docs/frb/" . $class->get_name() . ".php";
      $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">" . $class->get_name() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_db_postgres_display() {

    // guts of the list
    $markup .= "<h1>db_postgres</h1>\n";

    // rows
    $markup .= "<pre>\n";
    foreach ($this->get_list_bliss()->get_list() as $class) {
      $markup .= $class->get_db_postgres();
      $markup .= "\n";
    }
    $markup .= "\n";
    $markup .= "</pre>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // navagate to all classes
    $url = $this->url("classes");
    $parameter = "?view=default";
    $markup .= "<a href=\"" . $url . $parameter . "\">all</a><br /><br />";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $class) {
      $num++;

      $markup .= "<tr>\n";
      $markup .= "  <td style=\"width: 150px;\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"width: 240px;\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_sort() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_name_as_meta_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"width: 500px;\">\n";
      $markup .= "    " . $class->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      if ($class->get_status() == "1.0") {
        $markup .= "  <td style=\"background-color: green;\">\n";
      } else {
        $markup .= "  <td style=\"background-color: #EFEFEF;\">\n";
      }
      $markup .= "    " . $class->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // subsystem
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    subsystem\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_subsystem() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rank
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    rank\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_rank() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // notes
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    notes\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_notes() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    extends_class_id\n";
      $markup .= "    (<em>type</em>, <em>interface</em>)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_extends_class_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    ownership\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_ownership() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    fields\n";
      $markup .= "    (<em>characteristics</em>, <em>attributes</em>, <em>data variables</em>)\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    <pre>" . $class->get_fields() . "</pre>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td style=\"vertical-align: top;\">\n";
      $markup .= "    db_postgres\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "<pre style=\"background-color: #EFEFEF;\">" . $class->get_db_postgres() . "</pre>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    db_mysql\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "<pre>" . $class->get_db_mysql() . "</pre>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    user_name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $class->get_user_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // clean up bin
  // todo keeping this because I might actually learn some SQL while doing this
  // todo keep this as an example of a sql subquery
  //$sql = "select subq.domain_tli, count(subq.subsection) FROM (select domain_tli, subsection FROM webpages GROUP BY domain_tli, subsection) as subq GROUP BY subq.domain_tli ORDER BY subq.domain_tli;";

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "classes";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $classes_obj = new Classes($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $classes_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug processes target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function get_analysis_given_table_name($given_table_name, $given_user_obj) {
    $markup = "";

    $this->set_given_table_name($given_table_name);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    $count_in_classes_table = $this->get_list_bliss()->get_count();
    if (! $count_in_classes_table) {
      $markup .= "<span style=\"background-color: red;\">\n";
    }
    $markup .= "count in <em>classes</em> table = " . $count_in_classes_table . "<br />\n";
    if (! $count_in_classes_table) {
      $markup .= "</span>\n";
    }

    // analyze php file of class
    $filename = $this->get_filename_given_table_name($given_table_name);

    if(! @include_once($filename)) {
      $markup .= "<strong>unable to include filename $filename<br />\n";
      return $markup;
    }

    $class_name = $this->get_class_name_given_table_name($given_table_name);
    $markup .= "class_name = " . $class_name . "<br />\n";
    if (class_exists($class_name)) {

      $dynamically_created_obj = new $class_name($this->get_given_config());

      $row_count = $dynamically_created_obj->get_row_count_from_db();
      $markup .= "row count = " . $row_count . "<br />\n";

      $dynamically_created_obj->get_list_bliss()->empty_list();

      // get oldest sort Z date
      $oldest_sort_obj = $dynamically_created_obj->get_oldest_sort_object_from_db();

      // oldest_sort_date (set default)
      $oldest_sort_date = "";
      if (is_object($oldest_sort_obj)) {
        $oldest_sort_date = $oldest_sort_obj->get_oldest_sort_date();
        // if empty string, then message as such
        $oldest_sort_date = "[empty]";
        // oldest_sort_name
        $oldest_sort_name = $oldest_sort_obj->get_name();
        // output
        $url = $this->url($given_table_name . "/" . $oldest_sort_obj->get_id());
        $markup .= "oldest sort date (class) = " . $oldest_sort_date . " (<a href=\"" . $url . "\">" . $oldest_sort_name . "</a>)<br />\n";
      }   

    } else {
      $markup .= "class_name = " . $class_name . " <strong>does not exist</strong><br />\n";
    }

    return $markup;
  }

  // method
  public function get_class_name_given_table_name($given_table_name) {

    // deal with first character
    $temp = ucfirst($given_table_name);

    // assumes that at most there are 2 underscores in a table_name
    $temp = $this->convert_underscore($temp);
    $table_name = $this->convert_underscore($temp);

    // debug
    //print "debug classes table_name = " . $table_name . "<br />\n";

    return $table_name;
  }

  // method
  private function convert_underscore($given_string) {

    // deal with underscore and character following it
    $position = strpos($given_string, "_");

    if ($position) {

      // debug
      //print "debug classes position = " . $position . "<br />\n";

      // remove the underscore
      $temp = substr_replace($given_string, "", $position, 1);

      // make char uppercase
      $character = $temp[$position];
      $character = strtoupper($character);

      // insert char uppercase
      return substr_replace($temp, $character, $position, 1);
    }

    // no underscore, so return what was input
    return $given_string;
  }

  // method
  public function get_classes_list($given_user_obj) {
    $markup = "";

    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    return $this->get_list_bliss()->get_list();
  }

  // method
  public function get_rank_given_table_name($given_table_name, $given_user_obj) {
    $markup = "";

    $this->set_given_table_name($given_table_name);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // outptut
    foreach ($this->get_list_bliss()->get_list() as $class) {
       $markup .= $class->get_rank();
    }

    return $markup;
  }

  // method
  public function get_ranks_hash($given_user_obj) {
    $markup = "";

    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    if ($markup) {
      print "debug classes prepare_query() has markup<br />\n";
      return $markup;
    }

    // outptut
    $ranks = array();
    foreach ($this->get_list_bliss()->get_list() as $class) {
       $class_name = $class->get_name();
       $ranks[$class_name] =  $class->get_rank();
       // debug
       //print "debug classes class_name = " . $class_name . "<br />\n";
    }

    return $ranks;
  }

  // method
  public function get_filename_given_table_name($given_table_name) {
    $markup = "";

    // make filename
    $filename = $given_table_name . ".php";

    // output
    $markup .= $filename;

    // check on the status of the file that has the filename
    $current_dir = getcwd();
    $fullpath_filename = $current_dir ."/sites/all/modules/freeradiantbunny/php/" . $filename;
    if (file_exists($fullpath_filename)) {
      // ok
      //$markup .= " (exists)";
    } else {
      // error
      $markup .= "<span style=\"background-color: red;\">\n";
      $markup .= " <strong>filename not found</strong>: " . $filename . "<br />\n";
      $markup .= " fullpath_filename = " . $fullpath_filename . "<br />\n";
      $markup .= "</span>\n";
    }
    return $markup;
  }

  // method
  public function get_table_count_given_classes_name($given_classes_name, $given_user_obj) {
    $markup = "";

    // this demands a meta double-think, so look alert!
    // this does not want a row in the classes table
    // rather this function wants the table being referenced

    // note need to fix the string
    $given_classes_name = trim($given_classes_name);
    $given_classes_name = str_replace("\n", "", $given_classes_name);
    
    // does this classes_name exist?
    $filename = "";
    if ($given_classes_name) {
      $filename = $given_classes_name . ".php";
    }

    if ($filename) {
      if ($given_classes_name == "seed_packets") {
        include_once($filename);
        $obj = new SeedPackets($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "domains") {
        include_once($filename);
        $obj = new Domains($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "soil_areas") {
        include_once($filename);
        $obj = new SoilAreas($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "budgets") {
        include_once($filename);
        $obj = new Budgets($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "applications") {
        include_once($filename);
        $obj = new Applications($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "machines") {
        include_once($filename);
        $obj = new Machines($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "songs") {
        include_once($filename);
        $obj = new Songs($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "classes") {
        include_once($filename);
        $obj = new Classes($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "plant_lists") {
        include_once($filename);
        $obj = new PlantLists($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "plants") {
        include_once($filename);
        $obj = new Plants($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "databases") {
        include_once($filename);
        $obj = new Databases($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else if ($given_classes_name == "permaculture_topics") {
        include_once($filename);
        $obj = new PermacultureTopics($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_count($user_obj);
      } else {
        $markup .= "?";
      }
    }

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug classes: ignore, not related<br />\n";
    } else {
      $markup .= "debug classes: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    $markup .= "<strong>views:</strong> ";
    if ($this->get_given_view() == "default") {
      $markup .= "default";
    } else {
      $url = $this->url("classes");
      $parameter = "?view=default";
      $markup .= "<a href=\"" . $url . $parameter . "\">default</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "db_postgres") {
      $markup .= "db_postgres";
    } else {
      $url = $this->url("classes");
      $parameter = "?view=db_postgres";
      $markup .= "<a href=\"" . $url . $parameter . "\">db_postgres</a>";
    }
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  private function output_sort() {
    $markup = "";

    // todo code view and sort so _both_ can be carried to next pageview

    $markup .= "<strong>sort:</strong> ";
    if ($this->get_given_sort() == "name") {
      $markup .= "name";
    } else {
      $url = $this->url("classes");
      $parameter = "?sort=name";
      $markup .= "<a href=\"" . $url . $parameter . "\">name</a>";
    }
    $markup .= " | ";
    if ($this->get_given_sort() == "sort") {
      $markup .= "sort";
    } else {
      $url = $this->url("classes");
      $parameter = "?sort=sort";
      $markup .= "<a href=\"" . $url . $parameter . "\">sort</a>";
    }
    $markup .= " | ";
    if ($this->get_given_sort() == "status") {
      $markup .= "status";
    } else {
      $url = $this->url("classes");
      $parameter = "?sort=status";
      $markup .= "<a href=\"" . $url . $parameter . "\">status</a>";
    }
    $markup .= " | ";
    if ($this->get_given_sort() == "subsystem") {
      $markup .= "subsystem";
    } else {
      $url = $this->url("classes");
      $parameter = "?sort=subsystem";
      $markup .= "<a href=\"" . $url . $parameter . "\">subsystem</a>";
    }
    $markup .= " | ";
    if ($this->get_given_sort() == "extends") {
      $markup .= "extends";
    } else {
      $url = $this->url("classes");
      $parameter = "?sort=extends";
      $markup .= "<a href=\"" . $url . $parameter . "\">extends</a>";
    }
    $markup .= " | ";
    if ($this->get_given_sort() == "notes") {
      $markup .= "notes";
    } else {
      $url = $this->url("classes");
      $parameter = "?sort=notes";
      $markup .= "<a href=\"" . $url . $parameter . "\">notes</a>";
    }
    $markup .= " | ";
    if ($this->get_given_sort() == "rank") {
      $markup .= "rank";
    } else {
      $url = $this->url("classes");
      $parameter = "?sort=rank";
      $markup .= "<a href=\"" . $url . $parameter . "\">rank</a>";
    }
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT classes.id, classes.name FROM classes WHERE classes.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug classes: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "Classes ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug classes: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error classes: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $class) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $class->get_id() . " " . $class->get_name_with_link() . " documentation " . $class->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $class->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error classes: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "classes: no classes found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

  // method
  public function get_row_count_given_class($given_class_name) {
      $row_count_data = "";
      $user_obj = $this->get_user_obj();
      include_once("lib/factory.php");
      $factory_obj  = new Factory($this->get_given_config());
      $obj = $factory_obj->get_object_with_just_given_class_name($given_class_name);
      $row_count_data = $obj->get_row_count_from_db($user_obj);
      return $row_count_data;
  }

}
