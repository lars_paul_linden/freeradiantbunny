<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/units.php

include_once("lib/scrubber.php");

class Units extends Scrubber {

  // given
  private $given_id;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // attribute
  private $id;
  private $name;
  private $description;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  public function get_name_with_link() {
    $url = $this->url("units/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // method
  private function make_unit() {
    $obj = new Units($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT units.id, units.name, units.description FROM units WHERE units.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT units.id, units.name, units.description FROM units ORDER BY units.name, units.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // note there is another table called units in the following schema
    //$database_name = "plantdot_principle";

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $unit_obj = $this->make_unit();
        $unit_obj->set_id(pg_result($results, $lt, 0));
        $unit_obj->set_name(pg_result($results, $lt, 1));
        $unit_obj->set_description(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_name(pg_result($results, $lt, 1));
        $this->set_description(pg_result($results, $lt, 2));
      }
    } else {
      $this->get_db_dash()->print_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // note: all varieties are public

    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    //$markup .= "<div class=\"given-variables\">\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $design) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- id=" . $design->get_id() . " -->\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $design->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $design->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $design->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function print_form_selection() {
    $markup = "";

    // database
    $type = "get_all_units";
    $database_name = "plantdot_principle";
    $this->get_db_dash($database_name)->load($this, $type);

    // output
    $markup .= "<select name=\"units_id\">\n";

    foreach ($this->list as $units_obj) {
      $markup .= "  <option value=\"" . $units_obj->get_id() . "\">" . $units_obj->get_name() . "</option>\n";
    }
    $markup .= "</select>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<p>id = " . $this->get_id() . "</p>\n";
    $markup .= "<p>name = " . $this->get_name() . "</p>\n";
    $markup .= "<p>description = " . $this->get_description() . "</p>\n";

    return $markup;
  }

}
