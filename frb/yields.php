<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/yields.php

include_once("lib/scrubber.php");

class Yields extends Scrubber {

  // given
  private $given_plant_id;
  private $given_unit_id;
  private $given_source_1;
  private $given_source_2;
  private $given_source_3;

  // given_plant_id
  public function set_given_plant_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "plant_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_id = $var;
    }
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // given_unit_id
  public function set_given_unit_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "unit_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_unit_id = $var;
    }
  }
  public function get_given_unit_id() {
    return $this->given_unit_id;
  }

  // given_source_1
  public function set_given_source_1($var) {
    $this->given_source_1 = $var;
  }
  public function get_given_source_1() {
    return $this->given_source_1;
  }

  // given_source_2
  public function set_given_source_2($var) {
    $this->given_source_2 = $var;
  }
  public function get_given_source_2() {
    return $this->given_source_2;
  }

  // given_source_3
  public function set_given_source_3($var) {
    $this->given_source_3 = $var;
  }
  public function get_given_source_3() {
    return $this->given_source_3;
  }

  // attributes
  private $id;
  private $plant_obj;
  private $estimated_yield;
  private $numerator_unit_obj;
  private $denominator_unit_obj;
  private $source;
  private $range;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // estimated_yield
  public function set_estimated_yield($var) {
    $this->estimated_yield = $var;
  }
  public function get_estimated_yield() {
    return $this->estimated_yield;
  }

  // numerator_unit_obj
  public function get_numerator_unit_obj() {
    if (! isset($this->numerator_unit_obj)) {
      include_once("units.php");
      $this->numerator_unit_obj = new Units($this->get_given_config());
    }
    return $this->numerator_unit_obj;
  }

  // denominator_unit_obj
  public function get_denominator_unit_obj() {
    if (! isset($this->denominator_unit_obj)) {
      include_once("units.php");
      $this->denominator_unit_obj = new Units($this->get_given_config());
    }
    return $this->denominator_unit_obj;
  }

  // source
  public function set_source($var) {
    $this->source = $var;
  }
  public function get_source() {
    return $this->source;
  }

  // range
  public function set_range($var) {
    $this->range = $var;
  }
  public function get_range() {
    return $this->range;
  }

  // method
  private function make_yield() {
    $obj = new Yields($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_id()) {
      if ($this->get_given_source_1()) {
        $this->set_type("get_by_plant_id_and_source");
      } else {
        $this->set_type("get_by_plant_id");
      }

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT yields.id, yields.plant_id, yields.estimated_yield, yields.numerator_unit_id, yields.denominator_unit_id, yields.source, plants.common_name, units_n.name, units_d.name FROM yields, plants, units as units_n, units as units_d WHERE yields.denominator_unit_id = units_d.id AND yields.numerator_unit_id = units_n.id AND yields.plant_id = plants.id AND plants.id = " . $this->get_given_plant_id() . " ORDER BY yields.estimated_yield, yields.range asc;";

    } else if ($this->get_type() == "get_by_plant_id_and_source") {
      $sql = "SELECT yields.id, yields.plant_id, yields.estimated_yield, yields.numerator_unit_id, yields.denominator_unit_id, yields.source, plants.common_name, units_n.name, units_d.name FROM yields, plants, units as units_n, units as units_d WHERE yields.denominator_unit_id = units_d.id AND yields.numerator_unit_id = units_n.id AND yields.plant_id = plants.id AND plants.id = " . $this->get_given_plant_id() . " AND yields.source = '" . $this->get_given_source() . "' ORDER BY yields.estimated_yield, yields.range asc;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT yields.id, yields.plant_id, yields.estimated_yield, numerator_unit_id, units_n.name, denominator_unit_id, units_d.name, yields.source, plants.common_name FROM yields, plants, units as units_n, units as units_d WHERE yields.numerator_unit_id = units_n.id AND yields.denominator_unit_id = units_d.id AND yields.plant_id = plants.id ORDER BY plants.common_name, units_n.name, units_d.name, yields.estimated_yield;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));

      }
    } else if ($this->get_type() == "get_by_plant_id" ||
               $this->get_type() == "get_by_plant_id_and_source") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $yield = $this->make_yield();
        $yield->set_id(pg_result($results, $lt, 0));
        $yield->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $yield->set_estimated_yield(pg_result($results, $lt, 2));
        $yield->get_numerator_unit_obj()->set_id(pg_result($results, $lt, 3));
        $yield->get_denominator_unit_obj()->set_id(pg_result($results, $lt, 4));
        $yield->set_source(pg_result($results, $lt, 5)); 
        $yield->get_plant_obj()->set_common_name(pg_result($results, $lt, 6));
        $yield->get_numerator_unit_obj()->set_name(pg_result($results, $lt, 7));
        $yield->get_denominator_unit_obj()->set_name(pg_result($results, $lt, 8));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $yield = $this->make_yield();
        $yield->set_id(pg_result($results, $lt, 0));
        $yield->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $yield->set_estimated_yield(pg_result($results, $lt, 2));
        $yield->get_numerator_unit_obj()->set_id(pg_result($results, $lt, 3));
        $yield->get_numerator_unit_obj()->set_name(pg_result($results, $lt, 4));
        $yield->get_denominator_unit_obj()->set_id(pg_result($results, $lt, 5));
        $yield->get_denominator_unit_obj()->set_name(pg_result($results, $lt, 6));
        $yield->set_source(pg_result($results, $lt, 7)); 
        $yield->get_plant_obj()->set_common_name(pg_result($results, $lt, 8));
     }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    if ($this->get_given_plant_id()) {
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("yields");
      $markup .= "<strong>Widen View</strong>: <a href=\"" . $url . "\">All&nbsp;Yields</a>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    //

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_plant_id() != "") {
      if ($this->get_list_bliss()->get_first_element()) {
        $markup .= "<div class=\"given-variables\">\n";
        $markup .= "    <em>These " . get_class($this) . " are of the plant <strong>" . $this->get_list_bliss()->get_first_element()->get_plant_obj()->get_common_name() . "</strong>.\n";
        $markup .= "</div>\n";
      }
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    estimated yield\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    // numerator
    $markup .= "    unit\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    per\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    // denominator
    $markup .= "    unit\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    source\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $yield) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $yield->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $yield->get_estimated_yield() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $yield->get_numerator_unit_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    per\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $yield->get_denominator_unit_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $yield->get_source() . "\n";
      $markup .= "  </td>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<h2>" . $this->get_name() . "</h2>\n";
    $markup .= "<p>" . $this->get_description() . "</p>\n";
    $markup .= "<p>This is a yield of <strong>" . $this->get_plant_obj()->get_common_name_with_link() . "</strong>.</p>\n";

    return $markup;
  }

  // method
  public function output_sidecar() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<div style=\"margin: 4px 50% 4px 0px; padding: 6px 6px 0px 6px; background-color: #E8C7FF;\">\n";
      $markup .= "<h4 style=\"margin: 4px 0px 0px 4px; padding: 4px 0px 6px 1px;\">Varieties</h4>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_estimated_yield_given_plant_id($given_plant_id, $given_unit_id, $given_source_1 = "", $given_source_2 = "", $given_source_3 = "") {

    // set up variable that is being returned
    // element 0 is estimated yield value
    // element 1 is the source
    // initialize with default values
    $array_to_return = array("", "");

    // set
    $this->set_given_plant_id($given_plant_id);
    $this->set_given_unit_id($given_unit_id);
    $this->set_given_source_1($given_source_1);
    $this->set_given_source_2($given_source_2);
    $this->set_given_source_3($given_source_3);

    if (! $this->get_given_plant_id()) {
      $array_to_return[0] = "<p class=\"error\">Yields: The <strong>given_plant_id</strong> is not known.</p>";
    }
    if (! $this->get_given_unit_id()) {
      $array_to_return[0] = "<p class=\"error\">Yields: The <strong>given_unit_id</strong> is not known.</p>";
    }

    //print "debug yields plant id " . $this->get_given_plant_id() . "<br />";
    //print "debug yields unit " . $this->get_given_unit_id() . "<br />";
    //print "debug yields source_1 " . $this->get_given_source_1() . "<br />";
    //print "debug yields source_2 " . $this->get_given_source_2() . "<br />";

    // assume the database has been used and data is here
    foreach ($this->get_list_bliss()->get_list() as $yield) {

      // todo this is not perfect because there could be more than one yield
      // todo ... matching this criteria in the "if" statement below
      // return data only if same plant and same yield
      // try source_1
      if ($yield->get_plant_obj()->get_id() == $this->get_given_plant_id() && $yield->get_numerator_unit_obj()->get_id() == $this->get_given_unit_id() && $yield->get_source() == $this->get_given_source_1()) {
        $array_to_return[0] = $yield->get_estimated_yield();
        $array_to_return[1] = $yield->get_source();
      }
      // try source_2
      if ($yield->get_plant_obj()->get_id() == $this->get_given_plant_id() && $yield->get_numerator_unit_obj()->get_id() == $this->get_given_unit_id() && $yield->get_source() == $this->get_given_source_2()) {
        $array_to_return[0] = $yield->get_estimated_yield();
        $array_to_return[1] = $yield->get_source();
      }
      if ($yield->get_plant_obj()->get_id() == $this->get_given_plant_id() && $yield->get_numerator_unit_obj()->get_id() == $this->get_given_unit_id() && $this->get_given_source_3() == "Kim") {
        // if string begins with "Kim"
        if (strpos($yield->get_source(), $this->get_given_source_3()) === false) {
          // not found, so skip
        } else {
          // found substring
          $array_to_return[0] = $yield->get_estimated_yield();
          $array_to_return[1] = $yield->get_source();
        }
      }
    }
    return $array_to_return;
  }

  // todo in the Crop Yield Plan table set up the "estimated yield per row foot" so that when the user clicks on the number the link goes to the source (and the formula can be obtained from the second heading

}
