<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-17

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/events.php

include_once("lib/standard.php");

class Events extends Standard {

  // attribute
  private $id;
  private $time_start;
  private $time_finish;
  private $name;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // time_start
  public function set_time_start($var) {
    $this->time_start = $var;
  }
  public function get_time_start() {
    return $this->time_start;
  }

  // time_finish
  public function set_time_finish($var) {
    $this->time_finish = $var;
  }
  public function get_time_finish() {
    return $this->time_finish;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  private function make_event() {
    $obj = new Events($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT * from events where events.id = " . $this->get_given_id() . ";";
      
    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT * from events ORDER BY events.time_start;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_event();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_time_start(pg_result($results, $lt, 1));
        $obj->set_time_finish(pg_result($results, $lt, 2));
        $obj->set_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";
 
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    time_start\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    time_finish\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    interval\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $event) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $event->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $event->get_time_start_trim_seconds() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $event->get_time_finish_trim_seconds() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $event->get_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $event->get_interval() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_interval() {
    $markup = "";

    include_once("lib/time_measurements.php");
    $time_measurements_obj = new TimeMeasurements($this->get_given_config());
    $interval = $time_measurements_obj->get_interval($this->get_time_start(), $this->get_time_finish());
    $markup .= $interval;

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  //
  public function get_time_start_trim_seconds() {
    return substr($this->get_time_start(), 0, 16);
  }

  //
  public function get_time_finish_trim_seconds() {
    return substr($this->time_finish, 0, 16);
  }

}
