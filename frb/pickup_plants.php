<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-18

// about
// http://freeradiantbunny.org/main/en/docs/frb/pickup_plants.php

include_once("lib/scrubber.php");

class PickupPlants extends Scrubber {

  // given
  private $given_id;
  private $given_plant_id;
  private $given_project_id;
  private $given_pickup_id;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_plant_id
  public function set_given_plant_id($var) {
    $this->given_plant_id = $var;
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_pickup_id
  public function set_given_pickup_id($var) {
    $this->given_pickup_id = $var;
  }
  public function get_given_pickup_id() {
    return $this->given_pickup_id;
  }

  // attributes
  private $id;
  private $quantity;
  private $pickup_obj;
  private $plant_obj;
  private $unit_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // quantity
  public function set_quantity($var) {
    $this->quantity = $var;
  }
  public function get_quantity() {
    return $this->quantity;
  }

  // unit_obj
  public function get_unit_obj() {
    if (! isset($this->unit_obj)) {
      include_once("units.php");
      $this->unit_obj = new Units($this->get_given_config());
    }
    return $this->unit_obj;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // pickup_obj
  public function get_pickup_obj() {
    if (! isset($this->pickup_obj)) {
      include_once("pickups.php");
      $this->pickup_obj = new Pickups($this->get_given_config());
    }
    return $this->pickup_obj;
  }

  // method
  private function make_pickup_plant() {
    $obj = new PickupPlants($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_pickup_id()) {
      // subset
      $this->set_type("get_by_pickup_id");

    } else if ($this->get_given_plant_id()) {
      // subset
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_project_id()) {
      // subset
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = ";";

    } else if ($this->get_type() == "get_by_pickup_id") {
      $sql = "SELECT pickup_plants.id, pickup_plants.pickup_id, pickup_plants.plant_id, pickup_plants.quantity, pickup_plants.unit_id, plants.common_name, units.name, pickup_details.date, harvests.id, harvests.name FROM pickup_plants, plants, units, pickups, harvests, pickup_details WHERE pickup_details.pickup_id = pickups.id AND pickup_plants.plant_id = plants.id AND pickup_plants.pickup_id = " . $this->get_given_pickup_id() . " AND pickup_plants.unit_id = units.id AND pickups.id = pickup_plants.pickup_id AND harvests.id = pickups.harvest_id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT pickup_plants.id, pickup_plants.pickup_id, pickup_plants.plant_id, pickup_plants.quantity, pickup_plants.unit_id, plants.common_name, units.name FROM pickup_plants, pickups, plants, units, projects WHERE projects.id = pickups.project_id AND pickups.id = pickup_plants.pickup_id AND pickup_plants.plant_id = plants.id AND projects.id = " . $this->get_given_project_id() . " AND pickup_plants.unit_id = units.id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_pickup_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $pickup_plant = $this->make_pickup_plant();
        $pickup_plant->set_id(pg_result($results, $lt, 0));
        $pickup_plant->get_pickup_obj()->set_id(pg_result($results, $lt, 1));
        $pickup_plant->get_plant_obj()->set_id(pg_result($results, $lt, 2));
        $pickup_plant->set_quantity(pg_result($results, $lt, 3));
        $pickup_plant->get_unit_obj()->set_id(pg_result($results, $lt, 4));
        $pickup_plant->get_plant_obj()->set_common_name(pg_result($results, $lt, 5));
        $pickup_plant->get_unit_obj()->set_name(pg_result($results, $lt, 6));
        // todo fix extra
        //$pickup_plant->get_pickup_obj()->get_pickup_detail_obj()->set_date(pg_result($results, $lt, 7));
        $pickup_plant->get_pickup_obj()->get_harvest_obj()->set_id(pg_result($results, $lt, 8));
        $pickup_plant->get_pickup_obj()->get_harvest_obj()->set_name(pg_result($results, $lt, 9));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

    return $markup;
  }

  // method form
  public function print_form_selection() {
    $markup = "";

    // output
    $markup .= "<select name=\"pickup_plant_id\">\n";

    foreach ($this->list as $pickup_plant_obj) {
      $markup .= "  <option value=\"" . $pickup_plant_obj->get_id() . "\">" . $pickup_plant_obj->get_name() . " at " . $pickup_plant_obj->get_garden_obj()->get_name_with_link() . " based upon " . $pickup_plant_obj->get_design_obj()->get_name_with_link() . "</option>\n";
    }
    $markup .= "</select>\n";
  }

  // method
  public function output_count_given_pickup_id($given_pickup_id) {
    $markup = "";

    // set
    $this->set_given_pickup_id($given_pickup_id);

    // get data
    $this->determine_type();
    $markup .= $this->prepare_query();

    //print "debug pickup_plants type " . $this->get_type() . "<br />";

    $markup .= $this->get_list_bliss()->get_count();

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= "<p>pickup id = " . $this->get_given_pickup_id() . "</p>\n";

    // subheading
    if ($this->get_list_bliss()->get_count() > 0) {
      $list_obj = $this->get_list_bliss()->get_list();
      $obj_0 = $list_obj[0];
      $markup .= "<h3>" . $obj_0->get_pickup_obj()->get_harvest_obj()->get_name_with_link() . " " . $obj_0->get_pickup_obj()->get_pickup_details()->get_date() . "</h3>\n";
   }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup plant id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    common name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    unit\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    quantity\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    estimated market values\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    value\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $tally_estimated_market_values = 0;
    $tally_estimated_market_values_missing_count = 0;
    $tally_estimated_market_values_line = 0;
    foreach ($this->get_list_bliss()->get_list() as $pickup_plant) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- id=" . $pickup_plant->get_id() . " -->\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup_plant->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup_plant->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      //$markup .= "  <td>\n";
      // todo figure out variety in this context
      //$markup .= "    " . $pickup_plant->get_plant_obj()->get_variety_obj()->get_name_with_link() . "\n";
      //$markup .= "  </td>\n";

      // todo figure out below
      //$plant_common_name_and_variety = $pickup_plant->get_plant_obj()->get_common_name() . "-" . $pickup_plant->get_variety_obj()->get_name();

      //if (array_key_exists($plant_common_name_and_variety, $tally_yield_needed_array)) {
        // debug
        //$markup .= "key exists\n";

        // add to current
      //  $current = $tally_yield_needed_array[$plant_common_name_and_variety];
      //  $tally_yield_needed_array[$plant_common_name_and_variety] = $current + $pickup_plant->get_unit_count();
      //} else {

        // debug
        //$markup .= "key does not exists\n";

        // add new entry
        //$tally_yield_needed_array[$plant_common_name_and_variety] = $pickup_plant->get_unit_count();
      //}

      // debug
      //$markup .= "count = " . count($tally_yield_needed_array);

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup_plant->get_unit_obj()->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td style=\"text-align: right;\">\n";
      $markup .= "    " . $pickup_plant->get_quantity() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $estimated_market_values_string = $pickup_plant->get_estimated_market_values();
      $markup .= $estimated_market_values_string  . "\n";
      if (is_numeric($estimated_market_values_string)) {
        $tally_estimated_market_values += floatval($estimated_market_values_string);
      } else {
        // count how many do not have values
        $tally_estimated_market_values_missing_count++;
      }
      $markup .= "  </td>\n";

      // quantity * estimated_market_value
      $markup .= "  <td style=\"text-align: right;\">\n";
      if ($pickup_plant->get_quantity() && is_numeric($estimated_market_values_string)) {
        $value = $pickup_plant->get_quantity() * floatval($estimated_market_values_string);
        $markup .= "    " . number_format($value, 2) . "\n";
        $tally_estimated_market_values_line += $value;
      } else {
        $markup .= $estimated_market_values_string;
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    // tally
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"4\" align=\"right\">\n";
    $markup .= "    total\n";
    $markup .= "  </td>\n";
    $markup .= "  <td align=\"right\">\n";
    $markup .= "    " . number_format($tally_estimated_market_values, 2) . "\n";
    if ($tally_estimated_market_values_missing_count) {
      $markup .= "<p class=\"error\">Error: " . $tally_estimated_market_values_missing_count . " plants do not have estimates.</p>\n";
    }
    $markup .= "  </td>\n";
    $markup .= "  <td align=\"right\">\n";
    $markup .= "    " . number_format($tally_estimated_market_values_line, 2) . "\n";
    if ($tally_estimated_market_values_missing_count) {
      $markup .= "<p class=\"error\">Error: " . $tally_estimated_market_values_missing_count . " plants do not have estimates.</p>\n";
    }
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_sum_of_yields($tally_yield_needed_array) {
    $markup = "";

    // output the data that was collected
    $markup .= "<table>\n";
    foreach ($tally_yield_needed_array as $common_name_and_variety => $unit_count) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $common_name_and_variety . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $unit_count . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function load_matrix($date, $crop_date_matrix_obj) {

    // debug
    //$markup .= "load_matrix: date = " . $date . "<br />\n";

    // database //
    $type = "";  #initialize

    $type = "get_pickup_plants_given_pickup_id";
    $this->get_db_dash()->load($this, $type);

    foreach ($this->list as $pickup_plant) {
      $crop_date_matrix_obj->add_entry($date, $pickup_plant->get_plant_obj(), $pickup_plant->get_variety_obj(), $pickup_plant->get_unit_count());
    }
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<a href=\"project?id=" . $this->get_given_project_id() . "\">Back to project page</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // note: all varieties are public

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_pickup_id()) {    
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "    <em>These pickup_plants are of <strong>pickup_id</strong> = " . $this->get_given_pickup_id() . ".</em>\n";
     $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function output_weekly_share_list_bliss($given_pickup_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_pickup_id($given_pickup_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug pickup plants " . $this->get_given_pickup_id() . "<br />";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();
    
    // todo above is where the error reporting breaks down 
    // todo becaues below the line returns a non-string object

    $list_obj = $this->get_list_bliss()->get_list();
    return $list_obj;
  }

  // method
  public function output_weekly_share_list($given_pickup_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_pickup_id($given_pickup_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug pickup plants " . $this->get_given_pickup_id() . "<br />";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 0) {
      // todo error message (nothing in list)
      return $markup;
    }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    pickup id\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    quantity\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    units\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    certified organic<br />market price<br />derived\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    conventional<br />market price<br />derived\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $sum_market_price_certified_organic = 0;
    $sum_market_price_conventional = 0;
    foreach ($this->get_list_bliss()->get_list() as $pickup_plant) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "<!--  pickup_plant.id = " . $pickup_plant->get_id() . " -->\n";

      //$markup .= "  <td>\n";
      //$markup .= $pickup_plant->get_pickup_obj()->get_id() . "\n";
      //$markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $pickup_plant->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= $pickup_plant->get_quantity() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $pickup_plant->get_unit_obj()->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      //$sum_market_price_certified_organic += $share->get_market_price_certified_organic();
      //$markup .= $pickup_plant->get_market_price_certified_organic() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      //$sum_market_price_conventional += $share->get_market_price_conventional();
      //$markup .= $pickup_plant->get_market_price_conventional() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    // totals row
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"4\" align=\"right\">\n";
    $markup .= "  <strong>market prices derived sum</strong>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= $sum_market_price_certified_organic . "\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= $sum_market_price_conventional . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_estimated_market_values() {
    $markup = "";

    include_once("prices.php");
    $price_obj = new Prices($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $plant_id = $this->get_plant_obj()->get_id();
    $unit_id = $this->get_unit_obj()->get_id();
    $formula_type = "average";
    $markup = $price_obj->get_estimated_market_value($user_obj, $plant_id, $unit_id, $formula_type);
    if (! $markup) {
      $markup .= "<p class=\"error\">No E.M.V.</p>\n";
    }

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup = "debug pickup_plants output_single()<br />\n";

    return $markup;
  }

}
