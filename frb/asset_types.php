<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-21
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/asset_types.php

include_once("lib/scrubber.php");

class AssetTypes extends Scrubber {

  // given
  private $given_id;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // attributes
  private $id;
  private $name;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  private function make_asset_type() {
    $obj = new AssetTypes($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function process_command() {
    /**
     * @todo no commands yet
     */
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id() != "") {
      $this->set_type("get_asset_type_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_all") {
      $sql = "SELECT asset_types.id, asset_types.name FROM asset_types ORDER BY asset_types.name;";

    } else if ($this->get_type() == "get_asset_type_by_id") {
      $sql = "SELECT asset_types.id, asset_types.name FROM asset_types WHERE asset_types.id = " . $this->get_given_id() . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $asset_type = $this->make_asset_type();
        $asset_type->set_id(pg_result($results, $lt, 0));
        $asset_type->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_asset_type_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<a href=\"project?id=" . $this->get_given_project_id() . "\">Back to project page</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // output given
    //$markup .= "<div class=\"given-variables\">\n";
    //$markup .= "    <em>These crop plans are of <strong>project_season_id = </em>" . $this->get_given_project_season_id() . "</strong>\n";
    //$markup .= "    <em<>and project <em>" . $this->get_project_obj()->get_name_with_link()  . "</em></p>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $asset_type) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $asset_type->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $asset_type->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method access by another class
  public function get_name_given_id($given_id) {
    $markup = "";

    // set
    $this->get_given_id($given_id);

    // get data
    $markup .= $this->determine_type();
    $markup .= $this->prepare_query();

    // loop
    foreach ($this->get_list_bliss()->get_list() as $asset_type) {
      if ($given_id == $asset_type->get_id()) {
         $markup .=  $asset_type->get_name();
         return $markup;
      }
    }

    return $markup;
  }

  // method access by another class
  public function get_name_with_link_given_id($given_id) {
    $markup = "";

    // set
    $this->get_given_id($given_id);

    // get data
    $markup .= $this->determine_type();
    $markup .= $this->prepare_query();

    // loop
    foreach ($this->get_list_bliss()->get_list() as $asset_type) {
      if ($given_id == $asset_type->get_id()) {
         $url = $this->url("asset_types/" . $asset_type->get_id());
         $markup .=  "<a href=\"" . $url . "\">" . $asset_type->get_name() . "</a>";
         return $markup;
      }
    }

    return $markup;
  }

  // method form
  public function print_select_list($name, $default_id) {
    $markup = "";

    // parameter "name" is the attribute value for the input element
    $type = "get_asset_type_list";
    $this->get_db_dash()->load($this, $type);
    $markup .= "    <select name=\"" . $name . "\">\n";
    foreach ($this->get_list_bliss()->get_list() as $asset_type) {
      $markup .= "      <option value=\"" . $asset_type->get_id() . "\"";
      if ($default_id) {
        if ($default_id == $asset_type->get_id()) {
          $markup .= " selected=\"selected\"";
        }
      }
      $markup .= ">" . $asset_type->get_name() . "</option>\n";
    }
    $markup .= "    </select>\n";

    return $markup;
  }

}
