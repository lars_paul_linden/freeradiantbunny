<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-11
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/moneymakers.php

include_once("lib/socation.php");

class Moneymakers extends Socation {

  // given
  private $given_webpage_id;
  private $given_domain_tli;
  private $given_view = "online"; // default

  // given_webpage_id;
  public function set_given_webpage_id($given_webpage_id) {
    $this->given_webpage_id = $given_webpage_id;
  }
  public function get_given_webpage_id() {
    return $this->given_webpage_id;
  }

  // given_domain_tli;
  public function set_given_domain_tli($given_domain_tli) {
    $this->given_domain_tli = $given_domain_tli;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attribute
  private $user_name;

  // variable user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // method
  private function make_moneymaker() {
    $obj = new Moneymakers($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT moneymakers.* FROM moneymakers WHERE id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT moneymakers.* FROM moneymakers ORDER BY moneymakers.sort DESC, moneymakers.name;";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT DISTINCT moneymakers.* FROM moneymakers, webpage_moneymakers, webpages, domains WHERE moneymakers.id = webpage_moneymakers.moneymaker_id AND webpage_moneymakers.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND domains.tli = '" . $this->get_given_domain_tli() . "' ORDER BY moneymakers.sort;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    // todo might have to look at tenperday database
    $database_name = "";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_moneymaker();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->set_img_url(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // todo this code is based upon goal_statements.php code
    // build the table using objects
    include_once("lib/html_table.php");
    $table_obj = new HtmlTable();
    $table_type = "";
    $table_attribute_class = "plants";

    // make header cells
    $row_matrix =  array(
      array("header-derived", "#", "", ""),
      array("header", "status", "", ""),
      array("header", "sort", "", ""),
      array("header", "id", "simple", ""),
      array("header-derived", "img_url", "", ""),
      array("header", "name", "simple", ""),
      array("header", "webpages", "", ""),
    );
    $table_obj->make_row($row_matrix);

    // start data
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $num++;

      // set variables
      $padding = "";
      $float = "";
      $width = "65";

      // pre-process for data
      // todo figure out if this is a serious memory issue
      $obj_img_element_data = $obj->get_img_as_img_element_with_link($padding, $float, $width);

      // get list of webpages
      include_once("webpage_moneymakers.php");
      $webpage_moneymaker_obj = new WebpageMoneymakers($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $webpage_list_data = $webpage_moneymaker_obj->get_webpage_list_given_moneymaker_id_and_domain_tli($obj->get_id(), $this->get_given_domain_tli(), $user_obj);

      // todo mess with sort
      // old way
      //$sort_cell_kludge = $obj->get_sort();
      // new way
      $sort_cell_kludge = $obj->get_sort_cell();

      // make header cells
      $row_matrix = array(
        array("", $num, "", ""),
        array("", $obj->get_status(), "", ""),
        array("", $sort_cell_kludge, "", ""),
        array("", $obj->get_id_with_link(), "simple", ""),
        array("", $obj_img_element_data, "", ""),
        array("", $obj->get_name(), "simple", ""),
        array("", $webpage_list_data, "", ""),
      );
      $table_obj->make_row($row_matrix);
    }

    $markup .= $table_obj->craft_table($table_attribute_class, $table_type);


    return $markup;
  }

  // method
  public function get_counter_obj() {
    if (! $this->counter_obj) {
      include_once("lib/counter.php");
      $this->counter_obj = new Counter($this->get_given_config());
    }
    return $this->counter_obj;
  }

  // method
  public function print_domains_table_with_jobs() {

    $markup .= "<table class=\"domains\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"2\" class=\"tiptop\">\n";
    $markup .= "    taskmaster (list dashboards that combine the following: processes, calendar, stakesholders, indiegoals, systems-map)<br /><em>design the information system that will support me doing all that I need to do!</em>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    foreach ($this->domains_array as $domain) {

      $markup .= "<tr>\n";

      $icon_cell = "  <td class=\"mid\">\n" . "    <a href=\"http://" . $domain->get_domain_name() . "\"><img src=\"" . $domain->get_icon_url() . "\" width=\"40\" border=\"0\" alt=\"\" /></a><br />\n" . "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $this->get_jobs_given_domain($domain->get_tli(), $icon_cell) . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "  </tr>\n";

    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function get_data_given_webpage_id($given_webpage_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_webpage_id($given_webpage_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->set_type("get_by_webpage_id");
    $markup = $this->prepare_query();

    // debug
    //print "debug " . $markup;

    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "<strong>" . $obj->get_id() . "</strong>\n";
      $markup .= $obj->get_name();
      $markup .= "<br />\n";
      $markup .= $obj->get_description();
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  protected function output_single_nonstandard($given_parent_view = "") {
    $markup = "";

    return $markup;
  }

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function get_count_moneymakers_given_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();
    // check for errors
    if ($markup) {
      print "debug moneymakers errors = " . $markup . "<br />\n";
      return $markup;
    }

    // only output if there is 1 item
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_moneymakers_given_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // check for errors
    if ($markup) {
      print "debug moneymakers errors = " . $markup . "<br />\n";
      return $markup;
    }

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_aggregate();
    } else {
      $markup .= "<p>[No moneymakers found.]</p>\n";
    }

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "moneymakers";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $moneymaker_obj = new Moneymakers($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $moneymaker_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug processes target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
          // 3
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

}
