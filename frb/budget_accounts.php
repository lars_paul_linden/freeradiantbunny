<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.2 2015-12-01

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/budget_accounts.php

include_once("lib/sproject.php");

class BudgetAccounts extends Sproject {

  // given
  private $given_budge_idt;

  // given_budget_id
  public function set_given_budget_id($var) {
    $this->given_budget_id = $var;
  }
  public function get_given_budget_id() {
    return $this->given_budget_id;
  }

  // attribute
  private $budget_obj;
  private $account_obj;

  // budget_obj
  public function get_budget_obj() {
    if (! isset($this->budget_obj)) {
      include_once("budgets.php");
      $this->budget_obj = new Budgets($this->get_given_config());
    }
    return $this->budget_obj;
  }

  // account_obj
  public function get_account_obj() {
    if (! isset($this->account_obj)) {
      include_once("accounts.php");
      $this->account_obj = new Accounts($this->get_given_config());
    }
    return $this->account_obj;
  }

  // method
  private function make_budget_account() {
    $obj = new BudgetAccounts($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");
      
    } else if ($this->get_given_budget_id()) {
      $this->set_type("get_by_budget_id");
      
    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND goal_statements.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";
      
    } else if ($this->get_type() == "get_all") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else if ($this->get_type() == "get_by_budget_id") {
      $sql = "SELECT * FROM budget_accounts, accounts WHERE budget_accounts.budget_id = " . $this->get_given_budget_id() . " AND budget_accounts.account_id = accounts.id ORDER BY accounts.sort DESC, accounts.state, budget_accounts.account_id;";
      
    } else if ($this->get_type() == "get_by_project_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_budget_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_budget_account();
        $obj->get_budget_obj()->set_id(pg_result($results, $lt, 0));
        $obj->get_account_obj()->set_id(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

  // method
  public function get_sidecar_given_budget_id($given_budget_id, $given_user_obj) {
    $markup = "";

    $this->set_given_budget_id($given_budget_id);
    $this->set_user_obj($given_user_obj);

    // debug
    $markup .= "given budget id = " . $this->get_given_budget_id() . "<br />\n";

    $this->determine_type();

    $markup .= $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $markup .= "<table class=\"scene-elements-list\">\n";
      // heading
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    #<br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    domain_tli<br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    machine_id<br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    budget<br />\n";
      $markup .= "  </td>\n";
      $markup .= "<tr>\n";
      // rows
      foreach ($this->get_list_bliss()->get_list() as $budget_account) {
        $num++;

        $markup .= "<tr>\n";

        // num
        $markup .= "  <td>\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        // domain_tli
        $markup .= "  <td>\n";
        $markup .= "    " . $budget_account->get_domain_obj()->get_tli() . "<br />\n";
        $markup .= "  </td>\n";

        // machine_id
        $markup .= "  <td>\n";
        $markup .= "    " . $budget_account->get_budget_obj()->get_id() . "<br />\n";
        $markup .= "  </td>\n";

        // budget
        $markup .= "  <td>\n";
        $markup .= "    " . $budget_account->get_account_obj()->get_id() . "<br />\n";
        $markup .= "  </td>\n";

      }
      $markup .= "</table>\n";
    } else {
      $markup .= "<p>No budget_accounts found.</p>\n";
    }

    return $markup;
  }

  // method
  public function output_accounts_list_given_budget_id($given_budget_id, $given_user_obj) {
    $markup = "";

    $this->set_given_budget_id($given_budget_id);
    $this->set_user_obj($given_user_obj);
;
    $this->determine_type();

    $markup .= $this->prepare_query();

    // output
    $markup .= "given budget id = " . $this->get_given_budget_id() . "<br />\n";
    if ($this->get_list_bliss()->get_count() > 0) {

      $account_id_list = array();

      foreach ($this->get_list_bliss()->get_list() as $budget_account) {
        array_push($account_id_list, $budget_account->get_account_obj()->get_id());
      }

      $account_id_count = count($account_id_list);
      $markup .= "<p>account count = " . $account_id_count . "</p>\n";

      // note send list to accounts class for formatting and display
      include_once("accounts.php");
      $account_obj = new Accounts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $account_obj->get_table_given_id_list($account_id_list, $user_obj);

    } else {
      $markup .= "<p>No <kbd>budget_accounts.accounts</kbd> found.</p>\n";
    }

    return $markup;
  }

}
