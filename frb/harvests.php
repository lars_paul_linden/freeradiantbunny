<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/harvests.php

include_once("lib/scrubber.php");

class Harvests extends Scrubber {

  // given
  private $given_project_id;
  private $given_plant_list_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // attributes
  private $id;
  private $project_obj;
  private $name;
  private $sort;
  private $shares_estimate;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      include_once("projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  public function get_name_with_link() {
    $url = $this->url("harvests/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // sort
  public function set_sort($var) {
    $this->sort = $var;
  }
  public function get_sort() {
    return $this->sort;
  }

  // shares_estimate
  public function set_shares_estimate($var) {
    $this->shares_estimate = $var;
  }
  public function get_shares_estimate() {
    return $this->shares_estimate;
  }

  // method
  private function make_harvest() {
    $obj = new Harvests($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_subview_class() == "pickups") {
      // subset
      $this->set_type("get_pickups");

    } else if ($this->get_given_subview_class() == "pickup") {
      // subset
      $this->set_type("get_pickup_by_id");

    } else if ($this->get_given_project_id()) {
      // subset
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_plant_list_id()) {
      // subset
      $this->set_type("get_by_plant_list_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT harvests.id, harvests.project_id, harvests.name, harvests.sort, projects.name, harvests.shares_estimate FROM harvests, projects WHERE harvests.project_id = projects.id AND harvests.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY harvests.sort, harvests.id;";

    } else if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT harvests.id, harvests.project_id, harvests.name, harvests.sort, projects.name, harvests.shares_estimate FROM harvests, projects WHERE harvests.project_id = projects.id AND harvests.project_id = " . $this->get_given_project_id() . " ORDER BY harvests.sort, harvests.id;";

    } else if ($this->get_type() == "get_by_plant_list_id") {
      $sql = "SELECT harvests.shares_estimate FROM harvests, projects, plant_list_projects WHERE plant_list_projects.plant_list_id = " . $this->get_given_plant_list_id() . " AND plant_list_projects.project_id = projects.id AND projects.id = harvests.project_id;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT harvests.id, harvests.project_id, harvests.name, harvests.sort, projects.name, harvests.shares_estimate FROM harvests, projects WHERE harvests.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY harvests.sort, harvests.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_harvest();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_name(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->get_project_obj()->set_name(pg_result($results, $lt, 4));
        $obj->set_shares_estimate(pg_result($results, $lt, 5));
      }
    } else if ($this->get_type() == "get_by_plant_list_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_harvest();
        $obj->set_shares_estimate(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_by_id") { 
     $this->get_list_bliss()->add_item($this);
     for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->get_project_obj()->set_id(pg_result($results, $lt, 1));
        $this->set_name(pg_result($results, $lt, 2));
        $this->set_sort(pg_result($results, $lt, 3));
        $this->get_project_obj()->set_name(pg_result($results, $lt, 4));
        $this->set_shares_estimate(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_project_id()) {
      // todo fix the following
      //$markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickups count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    shares estimate\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    shares count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $harvest) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "<!-- harvests.id = " . $harvest->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $harvest->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $harvest->get_project_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $url = $this->url("harvests/" . $harvest->get_id());
      $markup .= "  <a href=\"" . $url . "\">" . $harvest->get_name() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $harvest->get_sort() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $url = $this->url("pickups/harvest/" . $harvest->get_id());
      include_once("lib/counter.php");
      $counter_obj = new Counter($this->get_given_config());
      $counter_obj->set_given_harvest_id($harvest->get_id());
      $markup .= "  <a href=\"" . $url . "\">" . $counter_obj->get_pickups_count() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $markup .= "    " . $harvest->get_shares_estimate() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"right\">\n";
      $url = $this->url("shares/harvest/" . $harvest->get_id());
      $counter_obj->set_given_harvest_id($harvest->get_id());
      $markup .= "  <a href=\"" . $url . "\">" . $counter_obj->get_shares_count() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<h2>";
    $url = $this->url("harvests/" . $this->get_id());
    $markup .= "  <a href=\"" . $url . "\">" . $this->get_name() . "</a>";
    $markup .= "</h2>\n";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $markup .= "<tr>\n";

    $markup .= "  <td>\n";
    $markup .= "    " . $this->get_id() . "\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= $this->get_project_obj()->get_name_with_link();
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= $this->get_sort() . "\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    $markup .= "</table>\n";
    $markup .= "<br />\n";

    include_once("pickup_details.php");
    $pickup_details_obj = new PickupDetails($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $harvest_id = $this->get_id();
    $markup .= $pickup_details_obj->output_table_given_harvest_id($harvest_id, $user_obj);
    $markup .= "<br />\n";

    include_once("shares.php");
    $share_obj = new Shares($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $harvest_id = $this->get_id();
    $markup .= $share_obj->output_table_given_harvest_id($harvest_id, $user_obj);

    return $markup;
  }

  // method
  public function get_shares_estimate_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);

    // get data from database
    $this->determine_type();      
    $this->prepare_query();

    $first_flag = "1";
    foreach ($this->get_list_bliss()->get_list() as $harvest) {
      if ($first_flag) {
        $markup = $harvest->get_shares_estimate();
        $first_flag = "";
      } else {
        // todo do some brain-thinking and consider if harvests of a project have each
        // todo ... have a diffefent number of shares
        // for now, make sure they are all the same
        if ($markup != $harvest->get_shares_estimate()) {
          $markup = "!";
        }
      }
    }
    if (! $markup) {
      // return an error message
      $markup .= "<p class=\"error\">S.E. is not found</p>\n";
    }

    return $markup;
  }

  // method
  public function get_project_id_given_harvest_id($given_harvest_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_harvest_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_project_obj()->get_id();
      //print "debug plant list projects = " . $project_id . "<br />\n"; 
      $markup .= $project_id;
    }

    return $markup;
  }
}
