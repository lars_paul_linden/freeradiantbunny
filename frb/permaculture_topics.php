<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 aka 12-Oct-2013 frb 1.1
// version 1.1 2014-12-29
// version 1.2 2015-01-05
// version 1.6 2016-12-24

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/permaculture_topics.php

include_once("lib/standard.php");

class PermacultureTopics extends Standard {

  // given
  private $given_view = "all"; // default

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // method
  public function get_project_id() {
    return "";
  }

  // method
  private function make_permaculture_topic() {
    $obj = new PermacultureTopics($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // attributes
  private $order_by;

  // order_by
  public function set_order_by($var) {
    $this->order_by = $var;
  }
  public function get_order_by() {
    return $this->order_by;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    //print "debug " . $this->get_type() . "<br />";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT permaculture_topics.* FROM permaculture_topics WHERE permaculture_topics.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") { 
      $sql = "SELECT permaculture_topics.* FROM permaculture_topics ORDER BY permaculture_topics.sort DESC, permaculture_topics.order_by, permaculture_topics.name;";

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM projects WHERE " . $username_sql . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // todo clean up these old variables
    // define database_name
    // ver 1.0
    //$database_name = "mudiacom_psites";
    // ver 1.1
    //$database_name = "mudiacom_frbgar";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  public function set_value_from_result($table, $name, $value) {

     // debug
     //print "debug permaculture_topics set: $name $value<br />\n";

     $setter = "set_" . $name;
     if ($table == "permaculture_topics") {
       $this->$setter($value);
     } else {
       // error
       $error_message = get_class($this)  . " set_value_from_result table is not known: $table<br />\n";
       $this->set_error_message($error_message);
     }
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
// todo store this other way of looping from db
// while($row = $result->fetch_row()) {
//        $num = 0;
//        foreach($row as $value) {
//          $table = $tables[$num];
//          $name = $names[$num];
//          $obj->set_value_from_result($table, $name, $value);
//          $num++;
//        }
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_permaculture_topic();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->set_order_by(pg_result($results, $lt, 5));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_permaculture_topic();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_permaculture_topic();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type: <kbd>" . $type . "</kbd>");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= $this->output_view();

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    order_by\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    num\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    status\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\" style=\"\">\n";
    //$markup .= "    img\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n"; 
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    description\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("tags"); 
    $markup .= "    <a href=\"" . $url . "\">tags</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    total <a href=\"http://permaculturewebsites.org/pws/hyperlinks.php\">hyperlinks</a> ";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    total <a href=\"http://permaculturewebsites.org/pws/webpages.php\">webpages</a> ";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    total <a href=\"http://permaculturewebsites.org/pws/books.php\">books</a> ";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // num
    $num = 0;

    // rows
    foreach ($this->get_list_bliss()->get_list() as $permaculture_topic) {
      $markup .= "<tr>\n";

      //$markup .= "  <td>\n";
      //$markup .= "    " . $permaculture_topic->get_order_by() . "<br >\n";
      //$markup .= "  </td>\n";

      // num
      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "<br >\n";
      $markup .= "  </td>\n";

      // status
      //$markup .= "  <td style=\"width: 120px;\">\n";
      //$markup .= "    " . $permaculture_topic->get_status() . "<br >\n";
      //$markup .= "  </td>\n";

      // sort
      // old way
      //$column_name = "sort";
      //include_once("lib/timekeeper.php");
      //$timekeeper_obj = new Timekeeper();
      //$color = $timekeeper_obj->calculate_cell_color($column_name, $permaculture_topic->get_sort());
      //$markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\">\n";
      //$markup .= "    " . $permaculture_topic->get_sort_as_button() . "\n";
      // centralized way
      $markup .= $permaculture_topic->get_sort_cell();

      // id with link
      $markup .= "  <td>\n";
      $markup .= "    " . $permaculture_topic->get_id_with_link() . "<br >\n";
      $markup .= "  </td>\n";

      // img
      if (0) {
        $markup .= "  <td style=\"background-color: #0099CC;\">\n";
        $padding = " 0px 0px 0px 0px";
        $float = "";
        // todo be able to configure the width of the process img_element
        $width = "65";
        // todo make this a config configurable item
        //$width = "25";
        $markup .= "    " . $permaculture_topic->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
        $markup .= "  </td>\n";
      }

      // name with link
      $markup .= "  <td>\n";
      $markup .= "    <span  style=\"font-size: 110%;\">" . $permaculture_topic->get_name_with_link() . "</span>\n";
      $markup .= "  </td>\n";

      // description
      if (0) {
        $markup .= "  <td>\n";
        if ($permaculture_topic->get_description()) {
          $markup .= "<p>";
        } else {
          $markup .= "<p style=\"background-color: #CCCCFF;\">";
        }
        $markup .= $permaculture_topic->get_description();
        $markup .= "</p>\n";
        $markup .= "  </td>\n";
      }

      // tags
      $markup .= "  <td>\n";
      include_once("tags.php");
      $tag_obj = new Tags($this->get_given_config());
      $markup .= $tag_obj->get_list_given_permaculture_topic_id($permaculture_topic->get_id()) . "\n";
      $markup .= "  </td>\n";

      // status
      //$markup .= "  <td>\n";
      //$markup .= "    " . $permaculture_topic->get_status() . "\n";
      //$markup .= "  </td>\n";

      // hyperlinks
      $markup .= "  <td>\n";
      $markup .= "    " . "\n";
      $markup .= "  </td>\n";

      // webpages
      $markup .= "  <td>\n";
      $markup .= "    " . "\n";
      $markup .= "  </td>\n";

      // books
      $markup .= "  <td>\n";
      $markup .= "    " . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_given_variables() {
    // note override so that it is blank
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $permaculture_topic) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $permaculture_topic->get_id() . "<br >\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $permaculture_topic->get_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // description
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      if ($permaculture_topic->get_description()) {
        $markup .= "  <td>\n";
      } else {
        $markup .= "  <td style=\"background-color: #EEDDDD;\">\n";
      }
      $markup .= "    " . $permaculture_topic->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // sort
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $permaculture_topic->get_sort() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // status
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $permaculture_topic->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // order_by
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    order_by\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $permaculture_topic->get_order_by() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // tags
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    tags\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      include_once("tags.php");
      $tag_obj = new Tags($this->get_given_config());
      $markup .= $tag_obj->get_list_given_permaculture_topic_id($permaculture_topic->get_id()) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT permaculture_topics.id, permaculture_topics.name FROM permaculture_topics WHERE permaculture_topics.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug permaculture_topics: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "PermacultureTopics ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // view
    $parameter_a = new Parameter();
    $parameter_a->set_name("view"); 
    $parameter_a->set_validation_type_as_view();
    array_push($parameters, $parameter_a);

    // sort
    $parameter_b = new Parameter();
    $parameter_b->set_name("sort");
    $parameter_b->set_validation_type_as_sort();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "permaculture_topics";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $permaculture_topic_obj = new PermacultureTopics($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $permaculture_topic_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    $markup .= "<div class=\"subsubmenu\">\n";
    // note: visit a friend
    // permaculture_topics
    if ($this->get_type() == "get_by_id") {
      $url = $this->url("permaculture_topics"); 
      $markup .= "<em>See all</em>: <a href=\"" . $url . "\">Permaculture Topics</a><br />\n";
    }
    // tags
    $url = $this->url("tags"); 
    $markup .= "<em>See also</em>: <a href=\"" . $url . "\">Tags</a><br />\n";
    $markup .= "</div>\n";
    // searches
    $url = $this->url("searches"); 
    $markup .= "<em>See also</em>: <a href=\"" . $url . "\">Searches</a><br />\n";

    return $markup;
  }

  // method
  public function get_name_with_link_given_id($given_user_obj, $given_id) {
    $markup = "";

    $this->set_user_obj($given_user_obj);
    $this->set_given_id($given_id);

    // load data from database
    $this->determine_type();

    // debug
    //print "debug processes given_id " . $given_id . "<br />\n";
    //print "debug processes given_id " . $this->get_given_id() . "<br />\n";
    //print "debug processes type " . $this->get_type() . "<br />\n";

    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $permaculture_topic) {
      $markup .= $permaculture_topic->get_name_with_link();
    }

    return $markup;
  }

}
