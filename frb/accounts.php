<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-21
// version 1.2 2015-01-14
// version 1.2 2016-03-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/accounts.php

include_once("lib/standard.php");

class Accounts extends Standard {

  // given
  private $given_id;
  private $given_class_name_string;
  private $given_class_primary_key_string;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_class_name_string
  public function set_given_class_name_string($var) {
    $this->given_class_name_string = $var;
  }
  public function get_given_class_name_string() {
    return $this->given_class_name_string;
  }

  // given_class_primary_key_string
  public function set_given_class_primary_key_string($var) {
    $this->given_class_primary_key_string = $var;
  }
  public function get_given_class_primary_key_string() {
    return $this->given_class_primary_key_string;
  }

  // attributes
  private $class_name_string;
  private $database_string;
  private $class_primary_key_string;
  private $state;

  // class_name_string
  public function set_class_name_string($var) {
    $this->class_name_string = $var;
  }
  public function get_class_name_string() {
    return $this->class_name_string;
  }

  // database_string
  public function set_database_string($var) {
    $this->database_string = $var;
  }
  public function get_database_string() {
    return $this->database_string;
  }

  // class_primary_key_string
  public function set_class_primary_key_string($var) {
    $this->class_primary_key_string = $var;
  }
  public function get_class_primary_key_string() {
    return $this->class_primary_key_string;
  }

  // state
  public function set_state($var) {
    $this->state = $var;
  }
  public function get_state() {
    return $this->state;
  }

  // method
  /**
   * @todo this might not be needed
   */
  //public function get_name_with_link() {
    /**
     * @todo figure this out in relation to Standard
     */
  //  $url = $this->url("accounts/" . $this->get_id());
  //  return "<a href=\"" . $url . "\">" . $this->get_name() . "</a>";
  //}


  // method
  private function make_account() {
    $obj = new Accounts($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_class_name_string()) {
      if ($this->get_given_class_primary_key_string()) {
        $this->set_type("get_by_class_name_string_and_class_primary_key_string");
      } else {
        $this->set_type("get_by_class_name_string");
      }

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT accounts.* FROM accounts WHERE accounts.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug accounts sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT accounts.* FROM accounts ORDER BY accounts.sort DESC, accounts.status, accounts.state, accounts.name, accounts.id;";

    } else if ($this->get_type() == "get_by_class_name_string") {
      $sql = "SELECT accounts.* FROM accounts WHERE accounts.class_name_string = '" . $this->get_given_class_name_string() . "';";

    } else if ($this->get_type() == "get_by_class_name_string_and_class_primary_key_string") {
      $sql = "SELECT accounts.* FROM accounts WHERE accounts.class_name_string = '" . $this->get_given_class_name_string() . "' AND accounts.class_primary_key_string = '" . $this->get_given_class_primary_key_string() . "';";


    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_class_name_string" ||
        $this->get_type() == "get_by_class_name_string_and_class_primary_key_string") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $account = $this->make_account();
        $account->set_id(pg_result($results, $lt, 0));
        $account->set_name(pg_result($results, $lt, 1));
        $account->set_description(pg_result($results, $lt, 2));
        $account->set_class_name_string(pg_result($results, $lt, 3));
        $account->set_sort(pg_result($results, $lt, 4));
        $account->set_status(pg_result($results, $lt, 5));
        $account->set_img_url(pg_result($results, $lt, 6));
        $account->set_database_string(pg_result($results, $lt, 7));
        $account->set_class_primary_key_string(pg_result($results, $lt, 8));
        $account->set_state(pg_result($results, $lt, 9));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $account = $this->make_account();
        $account->set_id(pg_result($results, $lt, 0));
        $account->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<a href=\"project?id=" . $this->get_given_project_id() . "\">Back to project page</a>\n";
    //$markup .= "</div>\n";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<a href=\"project?id=" . $this->get_given_project_id() . "\">Back to project page</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }
 
  // method
  public function output_user_info() {
    $markup = "";

    return $markup;
  }

 // method
  protected function output_given_variables() {
    $markup = "";

    // output given
    //$markup .= "<div class=\"given-variables\">\n";
    //$markup .= "    <em>These crop plans are of <strong>project_season_id = </em>" . $this->get_given_project_season_id() . "</strong>\n";
    //$markup .= "    <em<>and project <em>" . $this->get_project_obj()->get_name_with_link("")  . "</em></p>\n";
    //$markup .= "</div>\n";

    // output given
    //$markup .= "<div class=\"given-variables\">\n";
    //$markup .= "    <em>These crop plans are of <strong>project_season_id = </em>" . $this->get_given_project_season_id() . "</strong>\n";
    //$markup .= "    <em<>and project <em>" . $this->get_project_obj()->get_name_with_link("")  . "</em></p>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    state\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    /**
     * @todo text has too many characers for column
     */
    //$markup .= "    class_name_string\n";
    /**
     * @todo so have a way to show that it is an abbreviation
     */
    $markup .= "    class\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    /**
     * @todo text has too many characers for column
     */
    //$markup .= "    class_primary_key_string\n";
    /**
     * @todo so have a way to show that it is an abbreviation
     */
    $markup .= "    key\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    animated\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"postings\" style=\"background-color: #EFEFEF;\">\n";
    //$markup .= "    postings<br />count\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    description\n";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $account) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td style=\"width: 10px;\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // status
      include_once("lib/statuskeeper.php");
      $statuskeeper_obj = new StatusKeeper();
      $color = $statuskeeper_obj->calculate_cell_color($account->get_status());
      $markup .= "  <td valign=\"top\" style=\"background: $color; width: 100px;\">\n";
      $markup .= "    " . $account->get_status() . "\n";
      $markup .= "  </td>\n";

      // sort
      //$column_name = "sort";
      //include_once("lib/timekeeper.php");
      //$timekeeper_obj = new Timekeeper();
      //$color = $timekeeper_obj->calculate_cell_color($column_name, $account->get_sort());
      //$markup .= "  <td valign=\"top\" style=\"background: $color; width: 90px;\">\n";
      //$markup .= "    " . $account->get_sort() . "<br />\n";
      //$markup .= "  </td>\n";
      $markup .= $account->get_sort_cell();

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $account->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $account->get_name() . "\n";
      $markup .= "  </td>\n";

      // state
      $markup .= "  <td>\n";
      $markup .= "    " . $account->get_state() . "\n";
      $markup .= "  </td>\n";

      if ($account->get_class_name_string()) {
        $markup .= "  <td style=\"text-align: left;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: left; background-color: #CD5555;\">\n";
      }
      $markup .= "    " . $account->get_class_name_string() . "\n";
      $markup .= "  </td>\n";

      // class_primary_key_string
      if ($account->get_class_primary_key_string()) {
        $markup .= "  <td style=\"text-align: left;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: left; background-color: #CD5555;\">\n";
      }
      $markup .= "    " . $account->get_class_primary_key_string() . "\n";
      $markup .= "  </td>\n";

      // name with link of polymorphic object
      if ($account->get_class_name_string() && $account->get_class_primary_key_string()) {
        $markup .= "  <td style=\"text-align: left;\">\n";
        $markup .= "    " . $account->get_polymorphic_object();
      } else {
        $markup .= "  <td style=\"text-align: left; background-color: #CD5555;\">\n";
        $markup .= "    ";
      }
      $markup .= "  </td>\n";

      /**
       * @todo this is an older earlier idea and may not be valid now
       */
      // posting account
      //include_once("postings.php");
      //$posting_obj = new Postings($this->get_given_config());
      //$user_obj = $account->get_user_obj();
      //$postings_count = 0;
      ////$posting_obj->get_count_given_account_id($account->get_id(), $user_obj);
      //if ($postings_count) {
      //  $markup .= "  <td style=\"text-align: right;\">\n";
      //} else {
      //  $markup .= "  <td style=\"text-align: right; background-color: #CD5555;\">\n";
      //}
      //if ($postings_count) {
      //  $url = $this->url("postings/account/" . $account->get_id());
      //  $markup .= "<a href=\"" . $url . "\">";
      //}
      //$markup .= $postings_count;
      //if ($postings_count) {
      //  $markup .= "</a>\n";
      //}
      //$markup .= "  </td>\n";

      /**
       * @todo too much information on the aggregate page
       */
      // description
      //$markup .= "  <td>\n";
      //$markup .= "    " . $account->get_description() . "\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method form
  public function print_select_list($name, $default_id) {
    $markup = "";

    // parameter "name" is the attribute value for the input element
    $type = "get_account_list";
    $this->get_db_dash()->load($this, $type);
    $markup .= "    <select name=\"" . $name . "\">\n";
    foreach ($this->get_list_bliss()->get_list() as $account) {
      $markup .= "      <option value=\"" . $account->get_id() . "\"";
      if ($default_id) {
        if ($default_id == $account->get_id()) {
          $markup .= " selected=\"selected\"";
        }
      }
      $markup .= ">" . $account->get_name() . "</option>\n";
    }
    $markup .= "    </select>\n";

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    // polymorphic raw data
    $markup .= "<p>(database_string, ";
    $markup .= "class_name_string, ";
    $markup .= "class_primary_key_string) = (";
    // database_string
    // class_name_string
    // class_primary_key_string
    if ($this->get_database_string()) {
      $markup .= $this->get_database_string();
      $markup .= ", ";
    }
    if ($this->get_class_name_string()) {
      $markup .= $this->get_class_name_string();
      $markup .= ", ";
    }
    if ($this->get_class_primary_key_string()) {
      $markup .= $this->get_class_primary_key_string();
    }
    $markup .= ")</p>";

    // polymorphic animated
    $markup .= "<p style=\"background: #999999;\">";
    $markup .= $this->get_polymorphic_object();
    $markup .= "</p>\n";

    // state
    if ($this->get_state()) {
      $markup .= "<p>state = ";
      $markup .= $this->get_state();
      $markup .= "</p>";
    }

    // posting account
    $markup .= "<p>accounts.php: postings not yet coded.</p>\n";
    //if ($this->get_id()) {
    //  include_once("postings.php");
    //  $posting_obj = new Postings($this->get_given_config());
    //  $user_obj = $this->get_user_obj();
    // $postings_count = $posting_obj->get_count_given_account_id($this->get_id(), $user_obj);
    //  $postings_count = "";
    //  if ($postings_count) {
    //    $url = $this->url("postings/account/" . $this->get_id());
    //    $markup .= "<p style=\"font-size: 200%;\">";
    //    $markup .= "postings count = ";
    //    $markup .= "<a href=\"" . $url . "\">";
    //    $markup .= $postings_count;
    //    $markup .= "</a>\n";
    //    $markup .= "</p>\n";
    //  } else {
    //    $markup .= "<p style=\"error\">No count found for postings.</p>\n";
    //  }
    //}

    return $markup;
  }

  // method
  public function get_polymorphic_object() {
    $markup = "";

    // note make sure that the basic variables exist
    if ($this->get_database_string() || $this->get_class_name_string() || $this->get_class_primary_key_string()) {

      /**
       * @todo clean up old way
       */
      //$name_of_polymorphic_object =  
      //$markup .= "<a href=\"" . $this->get_class_name_string() . "/" . $this->get_class_primary_key_string() . "\">" . $name_of_polymorphic_object . " " . $this->get_class_primary_key_string() . "</a>\n";

      // new way uses parent class
      // note test if object is found
      if (is_object($this->get_polymorphic_object_as_obj())) {
        $markup .= $this->get_polymorphic_object_as_obj()->get_name_with_link("") . " ";
      } else {
        // note frb provides code-line-number-to-debug error messages
        $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": accounts could not get_polymorphic_object_as_obj. Non-object fatal error. Unable to load data. accounts.id = " . $this->get_id() . "");
      }
    }

    return $markup;
  }

  // method
  public function get_account_given_class_name_string($given_class_name_string, $given_user_obj) {
    $markup = "";

    $this->set_given_class_name_string($given_class_name_string);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no account.</p>\n";;
      return $markup;
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $markup .= "<p style=\"error\">more than 1 account.</p>\n";;
      return $markup;
    }

    return $this->get_list_bliss()->get_first_element();
  }  

  //  method
  public function get_name_with_link_given_id($given_account_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_account_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no account.</p>\n";;
      return $markup;
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $markup .= "<p style=\"error\">more than 1 account.</p>\n";;
      return $markup;
    }

    return $this->get_list_bliss()->get_first_element()->get_name_with_link("");
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT accounts.id, accounts.name FROM accounts WHERE accounts.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug accounts: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "Accounts ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link("");
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // view
    $parameter_a = new Parameter();
    $parameter_a->set_name("view");
    $parameter_a->set_validation_type_as_view();
    array_push($parameters, $parameter_a);

    // sort
    $parameter_b = new Parameter();
    $parameter_b->set_name("sort");
    $parameter_b->set_validation_type_as_sort();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "accounts";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $account_obj = new Accounts($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $account_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    return $markup;
  }

  //  method
  public function get_account_given_class_name_and_primary_key($given_class_name_string, $given_class_primary_key_string, $given_user_obj) {
    $markup = "";

    $this->set_given_class_name_string($given_class_name_string);
    $this->set_given_class_primary_key_string($given_class_primary_key_string);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug accounts: given_class_primary_key_string = " . $this->get_given_class_primary_key_string() . "<br />\n";

    $this->determine_type();

    // debug
    //print "debug accounts: type = " . $this->get_type() . "<br />\n";

    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $account) {
        $markup .= "<p style=\"\">\n";;
        $markup .= $account->get_name_with_link("");
        $markup .= "</p>\n";;
      }
    }

    return $markup;
  }

  // method
  public function get_table_given_id_list($given_list, $given_user_obj) {
    $markup = "";

    $this->set_user_obj($given_user_obj);

    /**
     * @todo this duplicates the aggregate table, but oh well
     */
    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    class\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    key\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    polymorphic_object\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    state\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    postings\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    /**
     * @todo this is kind of a new situation perhaps
     */
    // given a set of accounts (for the output_table to fun)
    // how to do kind of an aggregate set of searches
    foreach ($given_list as $account_id) {

      // num
      $num++;
      $markup .= "  <td style=\"width: 10px;\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $account = new Accounts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $account->get_row_given_account_id($account_id, $user_obj);

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  private function get_row_given_account_id($given_account_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_account_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $account) {

        // status
        include_once("lib/statuskeeper.php");
        $statuskeeper_obj = new StatusKeeper();
        $color = $statuskeeper_obj->calculate_cell_color($account->get_status());
        $markup .= "  <td valign=\"top\" style=\"background: $color; width: 100px;\">\n";
        $markup .= "    " . $account->get_status() . "\n";
        $markup .= "  </td>\n";

        // sort 
        $markup .= $account->get_sort_cell();

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $account->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // name
        $markup .= "  <td>\n";
        $markup .= "    " . $account->get_name() . "\n";
        $markup .= "  </td>\n";

        if ($account->get_class_name_string()) {
          $markup .= "  <td style=\"text-align: left;\">\n";
        } else {
          $markup .= "  <td style=\"text-align: left; background-color: #CD5555;\">\n";
        }
        $markup .= "    " . $account->get_class_name_string() . "\n";
        $markup .= "  </td>\n";

        // class_primary_key_string
        if ($account->get_class_primary_key_string()) {
          $markup .= "  <td style=\"text-align: left;\">\n";
        } else {
          $markup .= "  <td style=\"text-align: left; background-color: #CD5555;\">\n";
        }
        $markup .= "    " . $account->get_class_primary_key_string() . "\n";
        $markup .= "  </td>\n";

        // name with link of polymorphic object
        if ($account->get_class_name_string() && $account->get_class_primary_key_string()) {
          $markup .= "  <td style=\"text-align: left;\">\n";
         $markup .= "    " . $account->get_polymorphic_object();
        } else {
          $markup .= "  <td style=\"text-align: left; background-color: #CD5555;\">\n";
          $markup .= "    ";
        }
        $markup .= "  </td>\n";

        // state
        // default
        $background_color = "#EFEFEF";
        if ($account->get_state()) {
          $background_color = "#A6D785";
        }
        $markup .= "  <td style=\"background-color: " . $background_color . "; text-align: left;\">\n";
        $markup .= "    " . $account->get_state() . "\n";
        $markup .= "  </td>\n";

        // postings
        $markup .= "  <td>\n";
        $markup .= "    [postings]\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
    }

    return $markup;
  }

}
