<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-14
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/suppliers.php

include_once("lib/socation.php");

class Suppliers extends Socation {

  // given
  private $given_id;
  private $given_view = "online"; // default

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attributes
  private $city;
  private $state;
  private $url;
  private $bioregion;
  private $user_name;
  private $last_password_change;

  // city
  public function set_city($var) {
    $this->city = $var;
  }
  public function get_city() {
    return $this->city;
  }

  // state
  public function set_state($var) {
    $this->state = $var;
  }
  public function get_state() {
    return $this->state;
  }

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // bioregion
  public function set_bioregion($var) {
    $this->bioregion = $var;
  }
  public function get_bioregion() {
    return $this->bioregion;
  }

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // img_url
  public function get_img_url_default() {
    return "http://mudia.com/dash/_images/supplier_stamp.png";
  }

  // last_password_change
  public function set_last_password_change($var) {
    $this->last_password_change = $var;
  }
  public function get_last_password_change() {
    return $this->last_password_change;
  }

  // method
  private function make_supplier() {
    $obj = new Suppliers($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function process_command() {
    // todo no commands yet
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view"); 
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // id
    $parameter_d = new Parameter();
    $parameter_d->set_name("id");
    $parameter_d->set_validation_type_as_id();
    array_push($parameters, $parameter_d);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "suppliers";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $supplier_obj = new Suppliers($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $supplier_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug suppliers target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
          // 3
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
          // 4
          if ($parameter->get_name() == "id") {
            $this->set_given_id($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      // note: distinct
      $sql = "SELECT suppliers.* FROM suppliers WHERE suppliers.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug suppliers sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // todo: security: only get the rows owned by the user

      //if ($this->get_user_obj()) {
      //  $sql = "SELECT suppliers.* FROM suppliers WHERE suppliers.user_name = '" . $this->get_user_obj()->name . "' ORDER BY suppliers.name;";
      //} else { 
      //  $error_message = "Error: " . get_class($this->get_user_obj()) . ": object is null. Unable to load data.";
      //  print $error_message;
      //  $markup .= $this->get_db_dash()->output_error($error_message);
      //}

      $sql = "SELECT suppliers.* FROM suppliers ORDER BY suppliers.sort DESC, suppliers.name;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      //$sql = "SELECT suppliers.* FROM suppliers WHERE suppliers.id = project_suppliers.supplier_id AND project_suppliers.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY suppliers.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_supplier();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_city(pg_result($results, $lt, 2));
        $obj->set_state(pg_result($results, $lt, 3));
        $obj->set_url(pg_result($results, $lt, 4));
        $obj->set_bioregion(pg_result($results, $lt, 5));
        $obj->set_sort(pg_result($results, $lt, 6));
        $obj->set_status(pg_result($results, $lt, 7));
        $obj->set_description(pg_result($results, $lt, 8));
        $obj->set_user_name(pg_result($results, $lt, 9));
        $obj->set_img_url(pg_result($results, $lt, 10));
        $obj->set_last_password_change(pg_result($results, $lt, 11));
      }
    } else if ($this->get_type() == "get_all" ||
               $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_supplier();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_city(pg_result($results, $lt, 2));
        $obj->set_state(pg_result($results, $lt, 3));
        $obj->set_url(pg_result($results, $lt, 4));
        $obj->set_bioregion(pg_result($results, $lt, 5));
        $obj->set_sort(pg_result($results, $lt, 6));
        $obj->set_status(pg_result($results, $lt, 7));
        $obj->set_description(pg_result($results, $lt, 8));
        $obj->set_user_name(pg_result($results, $lt, 9));
        $obj->set_img_url(pg_result($results, $lt, 10));
        $obj->set_last_password_change(pg_result($results, $lt, 11));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    city\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    state\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    bioregion\n";
    //$markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $supplier) {
      $num++;

      $markup .= "<tr>\n";

      // num
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $supplier->get_sort_cell();

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $supplier->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img_url
      $markup .= "  <td>\n";
      $padding = "0px 20px 20px 0px;";
      $float = "float: left;";
      $width = "34";
      $markup .= "    " . $supplier->get_img_as_img_element_with_link($padding, $float, $width) . "</a>" . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      // note that this links the name to the database page
      //$markup .= "    " . $supplier->get_name_with_link() . "\n";
      // what you really want is for the name to go to the entity's homepage
      // and in the previous column, make the id go the the solo read-out
      // where the solo read-out can be explained in REST
      // for example: https://eatlocalfreshfood.org/suppliers/3
      $markup .= "  <a href=\"" . $supplier->get_url() .  "\">";
      $markup .= "    " . $supplier->get_name();
      $markup .= "  </a>\n";
      $markup .= "  </td>\n";

      // city
      $markup .= "  <td>\n";
      $markup .= "    " . $supplier->get_city() . "\n";
      $markup .= "  </td>\n";

      // state
      $markup .= "  <td>\n";
      $markup .= "    " . $supplier->get_state() . "\n";
      $markup .= "  </td>\n";

      // todo this should be a land hierarchy implementation
      // bioregion
      //$markup .= "  <td>\n";
      //$markup .= "    " . $supplier->get_bioregion() . "\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_list_is_empty_message() {
    $markup = "";

    // when error
    $markup .= "<ul>\n";
    $markup .= "  <li>No suppliers were found that match that request.</li>\n";
    $markup .= "</ul>\n";

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    // deal with city and state
    $markup .= "<p>";
    if ($this->get_city() && $this->get_state()) {
        $markup .= $this->get_city();
        $markup .= ", ";
        $markup .= $this->get_state();
    } else {
      $markup .= $this->get_city();
      $markup .= $this->get_state();
    }
    $markup .= "</p>";

    // url
    $markup .= "<p>url = <a href=\"" . $this->get_url() . "\">" . $this->get_url() . "</a></p>";

    // bioregion
    $markup .= "<p>bioregion = " . $this->get_bioregion() . "</p>";

    // last_password_change
    $markup .= "<p>";
    $markup .= "last_password_change = ";
    $markup .= $this->get_last_password_change();
    $markup .= "</p>";

    return $markup;
  }

  // method
  public function get_project_id_given_supplier_id($given_supplier_id, $given_user_obj) {
    $markup = "";

    // todo figure out how to manage discount program data

    // set
    $this->set_given_id($given_supplier_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_project_obj()->get_id();
      // debug
      //print "debug plant list projects = " . $project_id . "<br />\n"; 
      $markup .= $project_id;
    }

    return $markup;
  }

  // method
  public function insert() {
    $sql = "INSERT into suppliers (name, city, state, url) VALUES ('" . $instance->get_name() . "', '" . $instance->get_city() . "', '" . $instance->get_state() . "', '" . $instance->get_url() . "');";
  }

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  //public function get_project_id($given_supplier_id, $given_user_obj) {
  //  $markup = "";

  //  // set
  //  $this->set_given_id($given_supplier_id);
  //  $this->set_user_obj($given_user_obj);

  //  // load data from database
  //  $this->determine_type();
  //  $markup .= $this->prepare_query();

  //  // only output if there are items to output
  //  if ($this->get_list_bliss()->get_count() > 0) {
  //    $list_obj = $this->get_list_bliss()->get_list();
  //    $obj = $list_obj[0];
  //    $project_id = $obj->get_project_obj()->get_id();
  //    // debug
  //    //print "debug plant list projects = " . $project_id . "<br />\n"; 
  //    return $project_id;
  //  }

  //  return $markup;
  //}

  // method
  public function get_img_url_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // return default image
      $markup .= "http://mudia.com/dash/_images/swept_ins_icon_small_blue.png";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) {

      // pre-process data
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= $obj->get_img_url($padding, $float, $width);
    }

    return $markup;
  }

  //  method
  public function get_name_with_link_given_id($given_id = "", $given_user_obj) {
    $markup = "";
 
    // note error check
    if (! $given_id) {
      return "Error suppliers: no name, because no given_id\n";
    }

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() < 1) {
      // todo this error is in markup
      //$markup .= "<p style=\"error\">no supplier.</p>";
      $markup .= "no supplier";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() > 1) {
      // todo this error is in markup
      //$markup .= "<p style=\"error\">more than 1 supplier.</p>\n";;
      $markup .= "more than one supplier";
      return $markup;
    }

    return $this->get_list_bliss()->get_first_element()->get_name_with_link();
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug suppliers: ignore, not related<br />\n";
    } else {
      $markup .= "debug suppliers: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

}
