<?php


// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/designers.php

include_once("lib/standard.php");

class Designers extends Standard {

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug designers type = " . $this->get_type();
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT designers.*, projects.name, projects.img_url FROM designers, projects WHERE designers.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug designers sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT designers.* FROM designers ORDER BY designers.name;";

    } else if ($this->get_type() == "get_by_hyperlink_id") {
      // wow!
      $sql = "select designers.* FROM designers, hyperlink_designers WHERE designers.id = hyperlink_designers.designer_id AND hyperlink_designers.hyperlink_id = " . $this->get_given_hyperlink_id() . " ORDER BY designers.designer;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  private function make_designer() {
    $obj = new Designers($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function transfer($results) {
    $markup = "";

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $designer = $this->make_designer();
        $designer->set_id(pg_result($results, $lt, 0));
        $designer->set_name(pg_result($results, $lt, 1));
        $designer->set_description(pg_result($results, $lt, 2));
        $designer->set_sort(pg_result($results, $lt, 3));
        $designer->set_status(pg_result($results, $lt, 4));
        $designer->set_img_url(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $designer) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_sort() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_status() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $designer) {


      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "  id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_id_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "  img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = "2";
      $float = "";
      $width = "190";
      $markup .= "    " . $designer->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "  name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "  description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "  sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_sort() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "  status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $designer->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "</table>\n";

    }

    return $markup;
  }

  // method menu 4
  public function output_subsubmenu() {

 }

}
