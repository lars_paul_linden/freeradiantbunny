<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_families.php

include_once("lib/scrubber.php");

class PlantFamilies extends Scrubber {

  // given
  private $given_project_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $id;
  private $name;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  public function get_name_with_link() {
    $url = $this->url("plant_families/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // method
  private function make_plant_family() {
    $obj = new PlantFamilies($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");

    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT plant_families.* FROM plant_families WHERE plant_families.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug plant_families sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_families.id, plant_families.name FROM plant_families ORDER BY plant_families.name;";

      // debug
      //print "debug plant_families sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
 
   return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_family_given_id" ||
        $this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant_family = $this->make_plant_family();
        $plant_family->set_id(pg_result($results, $lt, 0));
        $plant_family->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_subsubmenu() {
    // note override
  }

  // method
  public function output_subsubmenu_get_all() {
    $markup = "";

    if ($this->get_type() == "get_all") {
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("plants");
      $markup .= "  See Also: <a href=\"" . $url . "\">Plants</a><br />\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    list plants\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    # output for individual items
    foreach ($this->get_list_bliss()->get_list() as $plant_family) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_family->get_name_with_link() . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("plants/plant_families/" . $plant_family->get_id());
      $markup .= "    <a href=\"" . $url . "\">list plants</a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    foreach ($this->get_list_bliss()->get_list() as $plant_family) {
      // column headings
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_family->get_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    list plants\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("plants/plant_families/" . $plant_family->get_id());
      $markup .= "    <a href=\"" . $url . "\">list plants</a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "  </table>\n";

    return $markup;
  }

  // method
  public function output_list_with_heading($plant_family_id_hilite) {
    $markup = "";

    // runs a showboat in the get_markup() manner

    $this->determine_type();
    $markup .= $this->prepare_query();

    $markup .= "<strong>Plant Families</strong><br />\n";

    if ($this->get_list_bliss()->get_count() > 0) {

      // items exist, so list them
      foreach ($this->get_list_bliss()->get_list() as $plant_family) {
        if ($plant_family_id_hilite == $plant_family->get_id()) {
          $markup .= $plant_family->get_name() . "<br />\n";
        } else {
          $url = $this->url("plants/plant_families/" . $plant_family->get_id());
          $markup .= "<a href=\"" . $url . "\">" . $plant_family->get_name() . "</a><br />\n";
        }
      }

    } else {
      // list is empty
      $markup .= $this->output_list_is_empty_message();
    }

    return $markup;
  }

  // method
  public function output_list() {
    $markup = "";

    # output for individual items
    foreach ($this->get_list_bliss()->get_list() as $plant_family) {
      if ($this->get_given_id() == $plant_family->get_id()) {
        $markup .= $plant_family->get_name() . "<br />\n";
      } else {
        $url = $this->url("plant_families/" . $plant_family->get_id());
        $markup .= "<a href=\"" . $url . "\">" . $plant_family->get_name() . "</a><br />\n";
      }
    }
    return $markup;
  }

  // method
  public function get_plant_family_name_given_id($given_id, $given_user_obj) {
    $markup = "";

    // loop through all the names searching for a match
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $plant_family) {
      if ($given_id == $plant_family->get_id()) {
        return $plant_family->get_name();
      }
    }

    return $markup;
  }

  // method
  protected function output_preface() {
    $markup = "";


  }

}
