<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-20
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/songs.php

include_once("lib/scrubber.php");

class Songs extends Standard {

  // parameters
  private $given_sort;
  private $given_project_id;
  private $given_album_id;

  // given_sort
  public function set_given_sort($var) {
    $this->given_sort = $var;
  }
  public function get_given_sort() {
    return $this->given_sort;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_album_id
  public function set_given_album_id($var) {
    $this->given_album_id = $var;
  }
  public function get_given_album_id() {
    return $this->given_album_id;
  }

  // attributes
  private $id;
  private $name;
  private $album_id;
  private $cover_flag;
  private $sort;
  private $lyrics_url;
  private $mp3_url;
  private $album_sort;
  private $chords;
  private $lyrics;
  private $description;

  // todo this needs attention to be connected to project
  private $project_id;

  // extra
  private $album_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // album_id
  public function set_album_id($var) {
    $this->album_id = $var;
  }
  public function get_album_id() {
    return $this->album_id;
  }

  // cover_flag
  public function set_cover_flag($var) {
    $this->cover_flag = $var;
  }
  public function get_cover_flag() {
    return $this->cover_flag;
  }

  // sort
  public function set_sort($var) {
    $this->sort = $var;
  }
  public function get_sort() {
    return $this->sort;
  }

  // lyrics_url
  public function set_lyrics_url($var) {
    $this->lyrics_url = $var;
  }
  public function get_lyrics_url() {
    return $this->lyrics_url;
  }

  // mp3_url
  public function set_mp3_url($var) {
    $this->mp3_url = $var;
  }
  public function get_mp3_url() {
    return $this->mp3_url;
  }

  // album_sort
  public function set_album_sort($var) {
    $this->album_sort = $var;
  }
  public function get_album_sort() {
    return $this->album_sort;
  }

  // chords
  public function set_chords($var) {
    $this->chords = $var;
  }
  public function get_chords() {
    return $this->chords;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // lyrics
  public function set_lyrics($var) {
    $this->lyrics = $var;
  }
  public function get_lyrics() {
    return $this->lyrics;
  }

  // album_obj
  public function get_album_obj() {
    if (! isset($this->album_obj)) {
      include_once("albums.php");
      $this->album_obj = new Albums($this->get_given_config());
    }
    return $this->album_obj;
  }

  // method
  private function make_song() {
    $obj = new Songs($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_album_id()) {
      $this->set_type("get_by_album_id");

    } else {
      // hierarchy
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {

      $sql = "SELECT songs.*, albums.name FROM songs, albums WHERE songs.album_id = albums.id AND songs.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_by_album_id") {

      $sql = "SELECT songs.*, albums.name FROM songs, albums WHERE songs.album_id = albums.id AND albums.id = " . $this->get_given_album_id() . " ORDER BY songs.album_sort;";

      // debug
      //print "debug songs: sql = $sql\n";

    } else if ($this->get_type() == "get_all") {

      // set default
      $sort_order = " ORDER BY songs.sort DESC, songs.name";
      if ($this->get_given_sort()) {
        if ($this->get_given_sort() == "album") {
          $sort_order = " ORDER BY songs.album_id, songs.album_sort DESC, songs.name";
        } else if ($this->get_given_sort() == "sort") {
          $sort_order = " ORDER BY songs.sort DESC, songs.name";
        } else if ($this->get_given_sort() == "name") {
          $sort_order = " ORDER BY songs.name";
        } else {
          // todo next line is probably redundant but it is also comforting
          $sort_order = " ORDER BY songs.sort DESC, songs.name";
        }
      }

      $sql = "SELECT songs.*, albums.name FROM songs, albums WHERE songs.album_id = albums.id" . $sort_order . ";";

      // todo add option for this sql: AND cover_flag IS NOT TRUE

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM songs WHERE " . $username_sql . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_album_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_song();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_lyrics_url(pg_result($results, $lt, 3));
        $obj->set_mp3_url(pg_result($results, $lt, 4));
        $obj->get_album_obj()->set_id(pg_result($results, $lt, 5));
        $obj->set_cover_flag(pg_result($results, $lt, 6));
        $obj->set_album_sort(pg_result($results, $lt, 7));
        $obj->set_chords(pg_result($results, $lt, 8));
        $obj->set_lyrics(pg_result($results, $lt, 9));
        $obj->set_description(pg_result($results, $lt, 10));
        $obj->get_album_obj()->set_name(pg_result($results, $lt, 11));
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_song();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $view = "";  // default
    //$view = "short";
    // output view
    // todo the column titles have hyperlinks instead
    //$markup .= $this->output_view();

    // css special black-background table design
    // black background color for pft website
    //$markup .= "<table style=\"font-family: Tahoma, sans-serif; border-collapse:collapse; background-color: #000000; color: #FFFFFF; margin: 6px 10px 6px 5px;\" class="plants">\n";
    // white background color for frb interface
    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";

    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";

    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    $url = $this->url("songs/");
    $parameter = "?sort=sort";
    $markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #000000;\">sort</span></a>\n";
    $markup .= "  </td>\n";

    // album_sort (this is the track number)
    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    // todo had to comment out the following; to fix pass along trail song id
    //$url = $this->url("songs/");
    $url = $this->url("");
    $parameter = "?sort=album";
    // colorized
    //$markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #FFFFFF;\">album</span></a>\n";
    // colorized
    $markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #000000;\">album</span></a>\n";
    $markup .= "  </td>\n";

    //if ($this->get_given_sort()) {
    //  if ($this->get_given_sort() == "album") {
    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    $markup .= "    track\n";
    $markup .= "  </td>\n";
    //  }
    //}

    // id
    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";

    // name
    // set up hyperlink so that user can sort rows
    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: left;\">\n";
    $url = $this->url("songs/");
    $parameter = "?sort=name";
    // colorized
    //$markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #FFFFFF;\">name</span></a>\n";
    // colorized
    $markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #000000;\">name</span></a>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    $markup .= "    lyrics_url\n";
    $markup .= "  </td>\n";

    $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    $markup .= "    mp3\n";
    $markup .= "  </td>\n";

    //$markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
    //$markup .= "    chords\n";
    //$markup .= "  </td>\n";

    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $song) {

      $markup .= "<tr>\n";

      // #
      $num++;
      $markup .= "  <td style=\"padding: 3px 10px 3px 3px;\">\n";
      $markup .= "    <span style=\"font-family: monospace; font-size: 90%;\">" . $num . "</span>\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $song->get_sort_cell();
      //$markup .= "  <td style=\"padding: 3px 3px 3px 3px;\" align=\"left\">\n";
      //$markup .= "    " . $song->get_sort() . "<br />\n";
      //$markup .= "  </td>\n";

      // album 
      $markup .= "  <td>\n";
      if ($song->get_album_obj()->get_id()) {
        $url = "albums/" . $song->get_album_obj()->get_id();
        $markup .= "<a href=\"" . $url . "\"><span style=\"color: #000000;\">";
      }
      // todo display album cover as clickable icon
      //$markup .= "<img src=\"" . $song->get_album_obj()->get_cover_front_url() . "\" alt=\"[" . $song->get_album_obj()->get_id() . "]\" width=\"60\" style=\"padding: 0px 8px 0px 0px;\" />\n";
      $markup .= "    " . $song->get_album_obj()->get_name() . "\n";
      if ($song->get_album_obj()->get_id()) {
        $markup .= "</span></a>";
      }
      $markup .= "  </td>\n";

      // album_sort (aka track id)
      $markup .= "  <td style=\"padding: 0px 3px 0px 0px; font-size: 110%;\" align=\"left\">\n";
      $markup .= "    &nbsp;" . $song->get_album_sort() . "<br />\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\" align=\"left\">\n";
      $markup .= "    " . $song->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td style=\"padding: 3px 12px 3px 3px; text-align: left;\">\n";
      if ($song->get_lyrics_url()) {
        $markup .= "<a href=\"" . $song->get_lyrics_url(). "\" style=\"text-decoration: none;\" \">\n";
      }
      // colorized for display on pft website
      //$markup .= "<span style=\"color: #FFFFFF; font-size: 150%; letter-spacing: 2px; font-weight: 900;\">" . strtoupper($song->get_name()) . "</span>\n";
      // colorized for display on frb display
      $markup .= "<span style=\"color: #000000; font-size: 140%; font-weight: 500; font-family: monospace;\">" . strtoupper($song->get_name()) . "</span>\n";
      if ($song->get_lyrics_url()) {
        $markup .= "</a>\n";
      }
      $markup .= "  </td>\n";

      // lyrics_url
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\">\n";
      if ($song->get_lyrics_url()) {
        $markup .= "    <a href=\"" . $song->get_lyrics_url(). "\">lyrics_url</a><br />\n";
      }
      $markup .= "  </td>\n";

      // mp3
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\">\n";
      if ($song->get_mp3_url()) {
        $markup .= "    <a href=\"" . $song->get_mp3_url() . "\">mp3</a><br />\n";
      }
      $markup .= "  </td>\n";

      // chords
      //$markup .= "  <td style=\"padding: 3px 3px 3px 3px;\" align=\"left\">\n";
      //$markup .= "    <pre>" . $song->get_chords() . "</pre><br />\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // view
    $parameter_a = new Parameter();
    $parameter_a->set_name("view");
    $parameter_a->set_validation_type_as_view();
    array_push($parameters, $parameter_a);

    // sort
    $parameter_b = new Parameter();
    $parameter_b->set_name("sort");
    $parameter_b->set_validation_type_as_sort();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "songs";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $song_obj = new Songs($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $song_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }
    return $markup;
  }

  // method
  public function get_song_list_given_album_id($given_album_id, $given_user_obj, $heading = "") {
    $markup = "";

    // set
    $this->set_given_album_id($given_album_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $markup .= $this->determine_type();
    $markup .= $this->prepare_query();

    // list heading
    if ($heading) {
      $markup .= "<h2>Songs</h2>\n";
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= $this->output_aggregate();

    } else {
      $markup .= "<p>No songs found.</p>\n";
    }

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";
    $views = array("sort", "album");
    $view_count = count($views);
    $count = 0;
    foreach ($views as $view) {
      $count++;
      $markup .= "<a href=\"\">" . $view . "</a>";
      if ($view_count != $count) {
        $markup .= " | ";
      }
    }
    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  protected function output_single() {
    $markup = "";

    $view = "";  // default

    $markup .= "<table class=\"plants\">\n";

    foreach ($this->get_list_bliss()->get_list() as $song) {

      $markup .= "<tr>\n";

      // id
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\" align=\"left\">\n";
      $markup .= "    " . $song->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // sort
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $url = $this->url("songs/");
      $parameter = "?sort=sort";
      $markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #000000;\">sort</span></a>\n";
      $markup .= "  </td>\n";
      $markup .= $song->get_sort_cell();

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // album 
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $url = $this->url("songs/");
      $parameter = "?sort=album";
      // colorized
      //$markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #FFFFFF;\">album</span></a>\n";
      // colorized
      $markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #000000;\">album</span></a>\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      if ($song->get_album_obj()->get_id()) {
        $url = "albums/" . $song->get_album_obj()->get_id();
        $markup .= "<a href=\"" . $url . "\"><span style=\"color: #000000;\">";
      }
      // todo display album cover as clickable icon
      //$markup .= "<img src=\"" . $song->get_album_obj()->get_cover_front_url() . "\" alt=\"[" . $song->get_album_obj()->get_id() . "]\" width=\"60\" style=\"padding: 0px 8px 0px 0px;\" />\n";
      $markup .= "    " . $song->get_album_obj()->get_name() . "\n";
      if ($song->get_album_obj()->get_id()) {
        $markup .= "</span></a>";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // album_sort (aka track id)
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $markup .= "    track\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 0px 3px 0px 0px; font-size: 110%;\" align=\"left\">\n";
      $markup .= "    &nbsp;" . $song->get_album_sort() . "<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // name
      // todo set up hyperlink so that user can sort rows
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $url = $this->url("songs/");
      $parameter = "?sort=name";
      // colorized
      //$markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #FFFFFF;\">name</span></a>\n";
      // colorized
      $markup .= "    <a href=\"" . $url . $parameter . "\"><span style=\"color: #000000;\">name</span></a>\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 3px 12px 3px 3px; text-align: left;\">\n";
      if ($song->get_lyrics_url()) {
        $markup .= "<a href=\"" . $song->get_lyrics_url(). "\" style=\"text-decoration: none;\" \">\n";
      }
      // colorized for display on pft website
      //$markup .= "<span style=\"color: #FFFFFF; font-size: 150%; letter-spacing: 2px; font-weight: 900;\">" . strtoupper($song->get_name()) . "</span>\n";
      // colorized for display on frb display
      $markup .= "<span style=\"color: #000000; font-size: 140%; font-weight: 500; font-family: monospace;\">" . strtoupper($song->get_name()) . "</span>\n";
      if ($song->get_lyrics_url()) {
        $markup .= "</a>\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // lyrics_url
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $markup .= "    lyrics_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\">\n";
      if ($song->get_lyrics_url()) {
        $markup .= "    <a href=\"" . $song->get_lyrics_url(). "\">" . $song->get_lyrics_url() . "</a><br />\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // mp3
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $markup .= "    mp3\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\">\n";
      if ($song->get_mp3_url()) {
        $markup .= "    <a href=\"" . $song->get_mp3_url() . "\">mp3</a><br />\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // chords
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $markup .= "    chords\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\" align=\"left\">\n";
      $markup .= "    <pre>" . $song->get_chords() . "</pre><br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // lyrics
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $markup .= "    lyrics\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\" align=\"left\">\n";
      $markup .= "    <pre>" . $song->get_lyrics() . "</pre><br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // description
      $markup .= "  <td style=\"color: #999999; padding: 2px 2px 2px 2px; text-align: center;\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"padding: 3px 3px 3px 3px;\" align=\"left\">\n";
      $markup .= "    <pre>" . $song->get_description() . "</pre><br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_preface() {
    $markup = "";

    // todo should this method be in the parent?

    return $markup;
  }

  // project_id
  public function set_project_id($var) {
    $this->project_id = $var;
  }
  public function get_project_id() {
    return $this->project_id;
  }

}
