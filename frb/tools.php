<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/tools.php

include_once("lib/socation.php");

class Tools extends Socation {

  // given
  private $given_view = "all"; // default

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // method
  private function make_tool() {
    $obj = new Tools($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      // note: distinct below
      $sql = "SELECT tools.* FROM tools WHERE tools.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug tools get_by_id sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT tools.* FROM tools ORDER BY tools.name, tools.id;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT DISTINCT ON (tools.sort, tools.id) tools.* FROM tools, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'tools' AND tools.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY tools.sort DESC, tools.id;";

      // debug
      //print "debug tools get_by_id sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // debug
    //print "debug tools sql = " . $sql . "<br />\n";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_tool();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_img_url(pg_result($results, $lt, 3));
        $obj->set_sort(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        //$obj->get_project_tool_obj()->get_project_obj()->set_id(pg_result($results, $lt, 6));
        //$obj->get_project_tool_obj()->get_project_obj()->set_name(pg_result($results, $lt, 7));
        //$obj->get_project_tool_obj()->get_project_obj()->set_img_url(pg_result($results, $lt, 8));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    project_tools name\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    project_tools name\n";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $tool) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- id = " . $tool->get_id() . " -->\n";

      // num
      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // project
      //$markup .= "  <td>\n";
      //$padding = "";
      //$float = "";
      //$width = "65";
      //$markup .= "    " . $tool->get_project_tool_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      //$markup .= "  </td>\n";

      // sort
      $markup .= $tool->get_sort_cell();

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $tool->get_id() . "\n";
      $markup .= "  </td>\n";

      // img_url
      $markup .= "  <td>\n";
      if ($tool->get_img_url()) {
        $markup .= "    " . $tool->get_img_url_as_img_element() . "\n";
      }
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "    " . $tool->get_name() . "\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $tool->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    // more
    $markup .= "<h3>Equipment</h3>\n";
    $markup .= "<p><a href=\"http://www.orbiscorporation.com/\">Orbis</a></p>\n";
    $markup .= "<p><a href=\"http://www.fergusonmfgco.com/products.htm\">Ferguson Manufacturing Products</a></p>\n";
    $markup .= "<p><a href=\"http://www.rainfloirrigation.com/\">Rain Flo Irrigation</a></p>\n";
    $markup .= "<p><a href=\"http://www.duboisag.com/\">Dubois Agrinovation</a></p>\n";
    $markup .= "<p><a href=\"http://www.sezsdr.com/home.htm\">Seed E-Z Seeder Inc.</a></p>\n";
    $markup .= "<p><a href=\"http://www.buddinghweeder.com/\">BuddinghWeeder</a></p>\n";
    $markup .= "<p><a href=\"http://www.bezzerides.com/\">Bezzerides Brothers</a></p>\n";
    $markup .= "<p><a href=\"http://www.sneeboerusa.com/\">Sneeboer</a></p>\n";
    $markup .= "<br />\n";


    return $markup;
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // only authenticated users
    if ($this->get_user_obj()) {
      //if ($this->get_user_obj()->uid) {
      //  // todo this is on hold because it takes up too much room
      //  //$markup .= "<div class=\"subsubmenu-user\">\n";
      //  //$markup .= "<strong>These crop plans were created by</strong> " . $this->get_user_obj()->name . "<br />\n";
      //  //$markup .= "</div>\n";
      //}
    }
    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $tool) {
      $markup .= "<h2>" . $tool->get_name() . "</h2>\n";
      $markup .= "<p>id = " . $tool->get_id() . "</p>\n";
      $markup .= "<p>" . $tool->get_description() . "</p>\n";
      $width = "65";
      $markup .= "<p>" . $tool->get_img_as_img_element_with_link("", "", $width) . "</p>\n";

      // todo easier to comment it out than to debug it
      //$markup .= "<p>project = " . $tool->get_project_obj()->get_name_with_link()  . "</p>\n";
      $markup .= "<p>sort = " . $tool->get_sort() . "</p>\n";
    }

    $markup .= $tool->get_animated();

    return $markup;
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug tools: ignore, not related<br />\n";
    } else {
      $markup .= "debug tools: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug tools: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error tools: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $tool) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $tool->get_id() . " " . $tool->get_name_with_link() . " maintenance " . $tool->get_energy_flows_dollars("maintenance") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $tool->get_energy_flows_dollars("maintenance");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error tools: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "tools: no tools found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // preprocess
    include_once("scene_elements.php");
    $scene_element_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    // todo note that this is hard-coded and should be a db issue
    $class_name = get_class($this);
    $id = $this->get_id();
    $count_of_matching_scene_elements = $scene_element_obj->get_count_of_matching_scene_elements($class_name, $id, $user_obj);
    
    // debug
    //$markup .= "debug tools: count_of_matching_scene_elements = " . $count_of_matching_scene_elements . "\n";

    if ($given_account_name == "maintenance") {
      // note extract XML from description field
      $field_string = $this->get_description();
      // debug
      //$markup .= $field_string;

      // get first line
      // extract string based upon substring
      $account_rule_string = substr($field_string, 0, strpos($field_string, "account-rule"));
      //$markup .= $account_rule_string;

      // narrow
      // extract string based upon substring
      $amount_string = substr($account_rule_string, 0, strpos($account_rule_string, "-dollars-per-month"));
      $markup .= $this->convert_to_portion_of_scene_elements($amount_string, $count_of_matching_scene_elements);

    } else {
      $markup .= "error tools: given_account_type not known.<br />\n";
    }

    return $markup;
  }

  // method
  public function get_animated() {
    $markup = "";

    include_once("accounts.php");
    $account_obj = new Accounts($this->get_given_config());
    $user_obj = $this->get_user_obj();  
    $class_name_string = "tools";
    $class_primary_key_string = $this->get_id();
    $data = $account_obj->get_account_given_class_name_and_primary_key($class_name_string, $class_primary_key_string, $user_obj);
    
    if ($data) {
      $markup .= "<table style=\"background-color: #999999;\">\n";

      $markup .= "<tr><td style=\"padding: 12px;\">\n";
      $markup .= "<h2 style=\"display: inline;\">accounts</h2>\n";
      $markup .= "</td></tr>\n";

      $markup .= "<tr><td style=\"padding: 12px;\">\n";
      $markup .= $data;
      $markup .= "</td></tr>\n";

      $markup .= "</table>\n";
    }

    return $markup;
  }

}
