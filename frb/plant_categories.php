<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_categories.php

include_once("lib/scrubber.php");

class PlantCategories extends Scrubber {

  // given
  private $given_project_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $id;
  private $name;
  private $description;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  public function get_name_with_link() {
    $url = $this->url("plant_categories/" . $this->get_id());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // method
  private function make_plant_category() {
    $obj = new PlantCategories($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT plant_categories.* FROM plant_categories WHERE plant_categories.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_categories.* FROM plant_categories ORDER BY plant_categories.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant_category = $this->make_plant_category();
        $plant_category->set_id(pg_result($results, $lt, 0));
        $plant_category->set_name(pg_result($results, $lt, 1));
        $plant_category->set_description(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  protected function output_subsubmenu() {
    // note this is where to override function in parent class
  }

  // method
  protected function output_given_variables() {
    // note this is where to override function in parent class
  }

  // method
  public function output_subsubmenu_get_all() {
    $markup = "";

    if ($this->get_type() == "get_all") {
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("plants");
      $markup .= "  See Also: <a href=\"" . $url . "\">Plants</a><br />\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<p><em>This is a look-up table.</em></p>\n";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    list plants\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // output for individual items
    foreach ($this->get_list_bliss()->get_list() as $plant_category) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_category->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_category->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $plant_category->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $url = $this->url("plants/plant_categories/" . $plant_category->get_id());
      $markup .= "    <a href=\"" . $url . "\">list plants</a><br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    list plants\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $plant_category) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_category->get_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_category->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("plants/plant_categories/" . $plant_category->get_id());
      $markup .= "    <a href=\"" . $url . "\">list plants</a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "  </tr>\n";
    }

    $markup .= "  </table>\n";

    return $markup;
  }

  // method
  public function output_list_with_heading($plant_category_id_hilite) {
    $markup = "";

    $this->determine_type();
    $markup .= $this->prepare_query();

    $markup .= "<strong>Plant PlantCategories</strong><br />\n";

    if ($this->get_list_bliss()->get_count() > 0) {

      // items exist, so list them
      foreach ($this->get_list_bliss()->get_list() as $plant_category) {
        if ($plant_category_id_hilite == $plant_category->get_id()) {
          $markup .= $plant_category->get_name() . "<br />\n";
        } else {
          $url = $this->url("plants/plant_categories/" . $plant_category->get_id());
          $markup .= "<a href=\"" . $url . "\">" . $plant_category->get_name() . "</a><br />\n";
        }
      }

    } else {
      // list is empty
      $markup .= $this->output_list_is_empty_message();
    }

    return $markup;
  }

  // method
  public function output_list() {
    $markup = "";

    // output for individual items
    foreach ($this->get_list_bliss()->get_list() as $plant_category) {
      if ($this->get_given_id() == $plant_category->get_id()) {
        $markup .= $plant_category->get_name() . "<br />\n";
      } else {
        $url = $this->url("plants/plant_categories/" . $plant_category->get_id());
        $markup .= "<a href=\"" . $url . "\">" . $plant_category->get_name() . "</a><br />\n";
      }
    }
    return $markup;
  }

  // method
  public function get_plant_category_name_given_id($given_id, $given_user_obj) {
    $markup = "";

    // loop through all the names searching for a match
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $plant_category) {
      if ($given_id == $plant_category->get_id()) {
        $markup .= $plant_category->get_name();
      }
    }

    return $markup;
  }

}
