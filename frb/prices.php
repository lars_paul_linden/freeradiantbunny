<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/prices.php

include_once("lib/scrubber.php");

class Prices extends Scrubber {

  // given
  private $given_plant_id;
  private $given_unit_id;

  // given_plant_id
  public function set_given_plant_id($plant_id) {
    $this->given_plant_id = $plant_id;
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // given_unit_id
  public function set_given_unit_id($unit_id) {
    $this->given_unit_id = $unit_id;
  }
  public function get_given_unit_id() {
    return $this->given_unit_id;
  }

  // attribute
  private $id;
  private $plant_obj;
  private $supplier_obj;
  private $date;
  private $dollars;
  private $unit_count;
  private $unit_obj;
  private $organic_flag;
  private $quality;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // supplier_obj
  public function get_supplier_obj() {
    if (! isset($this->supplier_obj)) {
      include_once("suppliers.php");
      $this->supplier_obj = new Suppliers($this->get_given_config());
    }
    return $this->supplier_obj;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // dollars
  public function set_dollars($var) {
    $this->dollars = $var;
  }
  public function get_dollars() {
    return $this->dollars;
  }

  // unit_count
  public function set_unit_count($var) {
    $this->unit_count = $var;
  }
  public function get_unit_count() {
    return $this->unit_count;
  }

  // unit_obj
  public function get_unit_obj() {
    if (! isset($this->unit_obj)) {
      include_once("units.php");
      $this->unit_obj = new Units($this->get_given_config());
    }
    return $this->unit_obj;
  }

  // organic_flag
  public function set_organic_flag($var) {
    $this->organic_flag = $var;
  }
  public function get_organic_flag() {
    return $this->organic_flag;
  }

  // quality
  public function set_quality($var) {
    $this->quality = $var;
  }
  public function get_quality() {
    return $this->quality;
  }

  // method
  private function make_price() {
    $obj = new Prices($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_id() && $this->get_given_unit_id()) {
      // subset
      $this->set_type("get_values_given_plant_id_and_unit_id");

    } else if ($this->get_given_plant_id()) {
      // subset
      $this->set_type("get_costs_given_plant_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
        $this->sql_statement = "SELECT costs.id, costs.plant_id, costs.supplier_id, costs.date, costs.dollars, costs.unit_count, costs.unit, costs.quality, plants.common_name, suppliers_a.name, suppliers_b.name, costs.organic_flag FROM costs, plants, suppliers as suppliers_a, suppliers as suppliers_b WHERE costs.id = " . $instance->get_id() . " and costs.plant_id = plants.id and costs.supplier_id = suppliers_a.id ORDER BY costs.date, suppliers_a.name;";

    } else if ($this->get_type() == "get_values_given_plant_id_and_unit_id") {
      $sql = "SELECT values.id , values.plant_id, values.supplier_id, values.date, values.dollars, values.unit_count, values.unit_id, values.quality, values.organic_flag, plants.common_name, suppliers.name, units.name FROM values, plants, suppliers, units WHERE plants.id = values.plant_id AND suppliers.id = values.supplier_id AND values.unit_id = units.id AND values.plant_id = " . $this->get_given_plant_id() . " AND values.unit_id = " . $this->get_given_unit_id() . " ORDER BY plants.common_name, units.name, values.dollars;";

    } else if ($this->get_type() == "get_costs_given_plant_id") {
      $this->sql_statement = "SELECT costs.id, costs.plant_id, costs.supplier_id, costs.date, costs.dollars, costs.unit_count, costs.unit, costs.quality, plants.common_name, suppliers.name, costs.organic_flag FROM costs, plants, suppliers WHERE costs.plant_id = " . $this->get_given_plant_id() . " and costs.plant_id = plants.id and costs.supplier_id = suppliers.id ORDER BY costs.date, suppliers.name;";


    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT values.id , values.plant_id, values.supplier_id, values.date, values.dollars, values.unit_count, values.unit_id, values.quality, values.organic_flag, plants.common_name, suppliers.name, units.name FROM values, plants, suppliers, units WHERE plants.id = values.plant_id AND suppliers.id = values.supplier_id AND values.unit_id = units.id ORDER BY plants.common_name, units.name, values.dollars;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // todo deal with update
  //} else if ($type == "update_cost") {
  //  $this->sql_statement = "UPDATE costs SET plant_id = " . $this->get_given_plant_id() . ", supplier_id = " . $this->get_supplier_id() . ", date = '" . $this->get_date() . "', dollars = '" . $this->get_dollars() . "', unit_count = '" . $this->get_unit_count() . "', unit = '" . $this->get_unit_id() . "', quality = '" . $this->get_quality() . "' WHERE id = " . $this->get_id() . ";";
  //}
  // todo deal with insert
  //} else if ($type == "insert_cost") {
  //  $this->sql_statement = "INSERT INTO costs (plant_id, supplier_id, date, dollars, unit_count, unit_id, quality, organic_flag) VALUES (" . $this->get_given_plant_id() . ", " . $this->get_supplier_id() . ", '" . $this->get_date() . "', '" . $this->get_dollars() . "', '" . $this->get_unit_count() . "', '" . $this->get_unit_id() . "', '" . $this->get_quality() . "', '" . $this->get_organic_flag() . "');";
  // }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_costs_given_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_price();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_plant_id(pg_result($results, $lt, 1));
        $obj->set_supplier_id(pg_result($results, $lt, 2));
        $obj->set_date(pg_result($results, $lt, 3));
        $obj->set_dollars(pg_result($results, $lt, 4));
        $obj->set_unit_count(pg_result($results, $lt, 5));
        $obj->get_unit_obj()->set_id(pg_result($results, $lt, 6));
        $obj->set_quality(pg_result($results, $lt, 7));
        $obj->set_plant_name(pg_result($results, $lt, 8));
        $obj->set_supplier_name(pg_result($results, $lt, 9));
      }
    } else if ($this->get_type() == "get_values_given_plant_id_and_unit_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_price();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_date(pg_result($results, $lt, 3));
        $obj->set_dollars(pg_result($results, $lt, 4));
        $obj->set_unit_count(pg_result($results, $lt, 5));
        $obj->get_unit_obj()->set_id(pg_result($results, $lt, 6));
        $obj->set_quality(pg_result($results, $lt, 7));
        $obj->set_organic_flag(pg_result($results, $lt, 8));
        $obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 9));
        $obj->get_supplier_obj()->set_name(pg_result($results, $lt, 10));
        $obj->get_unit_obj()->set_name(pg_result($results, $lt, 11));
      }
    } else if ($this->get_type() == "get_cost_by_given_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_plant_id(pg_result($results, $lt, 1));
        $this->set_supplier_id(pg_result($results, $lt, 2));
        $this->set_date(pg_result($results, $lt, 3));
        $this->set_dollars(pg_result($results, $lt, 4));
        $this->set_unit_count(pg_result($results, $lt, 5));
        $this->get_unit_obj()->set_id(pg_result($results, $lt, 6));
        $this->set_quality(pg_result($results, $lt, 7));
        $this->set_plant_name(pg_result($results, $lt, 8));
        $this->set_supplier_name(pg_result($results, $lt, 9));
        $this->set_organic_flag(pg_result($results, $lt, 10));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_price();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_date(pg_result($results, $lt, 3));
        $obj->set_dollars(pg_result($results, $lt, 4));
        $obj->set_unit_count(pg_result($results, $lt, 5));
        $obj->get_unit_obj()->set_id(pg_result($results, $lt, 6));
        $obj->set_quality(pg_result($results, $lt, 7));
        $obj->set_organic_flag(pg_result($results, $lt, 8));
        $obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 9));
        $obj->get_supplier_obj()->set_name(pg_result($results, $lt, 10));
        $obj->get_unit_obj()->set_name(pg_result($results, $lt, 11));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    // other options
    $markup .= "<div class=\"subsubmenu\">\n";

    $markup .= "Price Options:<br />\n";
    $markup .= "<a href=\"cost.php?command=record\">record a price</a><br />\n";
    $markup .= "<a href=\"http://gnucash.org/\">GnuCash</a><br />\n";
    $markup .= "<a href=\"http://wiki.gnucash.org/wiki/GnuCash\">GnuCash Wiki</a><br />\n";

    $markup .= "</div>\n";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" width=\"100\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    dollars\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    unit count\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    units\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    $ per unit\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    units\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    organic flag\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    quality\n";
    $markup .= "  </td>\n";

    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    &nbsp;\n";
    //$markup .= "  </td>\n";

    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $value) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- value id=" . $value->get_id() . " -->\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $value->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $value->get_plant_obj()->get_common_name_with_link() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $value->get_supplier_obj()->get_name_with_link() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $value->get_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\" align=\"right\">\n";
      $markup .= "    " . $value->get_dollars() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\" align=\"right\">\n";
      $markup .= "    " . $value->get_unit_count() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $value->get_unit_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\" align=\"right\">\n";
      $markup .= "    " . $value->get_dollars_per_unit() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    $/" . $value->get_unit_obj()->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width-center\">\n";
      $markup .= "    " . $value->get_organic_flag() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $value->get_quality() . "\n";
      $markup .= "  </td>\n";

      //$markup .= "  <td class=\"specify-width\">\n";
      //$markup .= "    <a href=\"cost.php?command=record&amp;id=" . $value->get_id() . "\">edit</a>\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_form() {
    $markup = "";

    // todo fix command
    // debug
    //print "<p>" . $this->get_command() . "</p>\n";
    //if ($this->get_command() == "record") {
    //  $this->output_form();
    //} else if ($this->get_id()) {
    //  $this->output_row();
    //} else {
    //  $this->get_db_dash()->print_error("Cost id is not known.\n");
    //}

  }

  // method
  protected function output_single() { 
    $markup = "";

    // get data from database
    //$type = "get_cost_by_given_id";
    //$this->get_db_dash()->load($this, $type);

    $markup .= "<h2>Cost</h2>\n";

    $markup .= "<p>plant = <a href=\"plant.php?id=" . $this->get_plant_obj()->get_id() . "\">" . $this->get_plant_obj()->get_name() . "</a></p>";

    $markup .= "<p>supplier = <a href=\"supplier.php?id=" . $this->get_supplier_id() . "\">" . $this->get_supplier_name() . "</a></p>";

    $markup .= "<p>date = " . $this->get_date() . "</p>";

    $markup .= "<p>dollars = ";
    $markup .= $this->get_dollars();
    $markup .= "</p>";

    $markup .= "<p>unit count = ";
    $markup .= $this->get_unit_count();
    $markup .= "</p>";

    $markup .= "<p>unit_id = ";
    $markup .= $this->get_unit_id();
    $markup .= "</p>";

    $markup .= "<p>quality = ";
    $markup .= $this->get_organic_flag();
    $markup .= "</p>";

    $markup .= "<p>quality = ";
    $markup .= $this->get_quality();
    $markup .= "</p>";

    return $markup;
  }

  // method
  public function get_dollars_per_unit() {
    $markup = "<p class=\"error\">unkonwn</p>]n";

    // derived
    // protect divide by zero
    if (is_numeric($this->unit_count) && $this->unit_count != 0) {
      $dollars_per_unit = $this->dollars / $this->unit_count;
      $markup = round($dollars_per_unit,2);
    }

    return $markup;
  }

  // method
  public function get_estimated_market_value($given_user_obj, $given_plant_id, $given_unit_id, $given_formula_type) {
    $markup = "<p class=\"error\">Error Prices: <em>formula_type</em> is not known.</p>\n";

    // set
    $this->set_user_obj($given_user_obj);
    $this->set_given_plant_id($given_plant_id);
    $this->set_given_unit_id($given_unit_id);

    // get data
    $this->determine_type();
    $this->prepare_query();

    if ($given_formula_type == "average") {
      // get average
      $count = 0;
      $sum = 0;
      foreach ($this->get_list_bliss()->get_list() as $value) {
        $count++;
        $sum += floatval($value->get_dollars());
        // debug
        //$markup .= $value->get_dollars() . "<br />\n";
      }
      if ($count != 0) {
        $average = $sum / $count;
        $markup = number_format($average, 2);
      }
    }

    return $markup;
  }
}
