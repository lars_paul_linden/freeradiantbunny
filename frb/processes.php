<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-17
// version 1.2 2015-01-04
// version 1.4 2015-06-19
// version 1.5 2015-10-16
// version 1.5 2016-03-12
// version 1.6 2016-03-25
// version 1.7 2017-04-22
// version 1.8 2017-04-28
// version 1.8 2017-04-28

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/processes.php

include_once("lib/standard.php");

class Processes extends Standard {

  // given
  private $given_business_plan_text_id;
  private $given_project_id;
  private $given_process_id;
  private $given_display_stats_toggle;  // if 0, display stats
                                        // if 1, do not display stats

  // given_business_plan_text_id
  public function set_given_business_plan_text_id($var) {
    $this->given_business_plan_text_id = $var;
  }
  public function get_given_business_plan_text_id() {
    return $this->given_business_plan_text_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_display_stats_toggle
  public function set_given_display_stats_toggle($var) {
    $this->given_display_stats_toggle = $var;
  }
  public function get_given_display_stats_toggle() {
    return $this->given_display_stats_toggle;
  }

  // attributes
  private $business_plan_text_obj;
  private $responsibility;
  private $priority;
  private $time_rules;
  private $yield;
  //private $process_obj;

  // todo document that the r.d.c. is the number of days that can elapse after the last timestamp during which the responsibility is considered fulfilled and all-good.

  // business_plan_text_obj
  public function get_business_plan_text_obj() {
    if (! isset($this->business_plan_text_obj)) {
      include_once("business_plan_texts.php");
      $this->business_plan_text_obj = new BusinessPlanTexts($this->get_given_config());
    }
    return $this->business_plan_text_obj;
  }

  // responsibility
  public function set_responsibility($var) {
    $this->responsibility = $var;
  }
  public function get_responsibility() {
    return $this->responsibility;
  }

  // priority
  public function set_priority($var) {
    $this->priority = $var;
  }
  public function get_priority() {
    return $this->priority;
  }

  // time_rules
  public function set_time_rules($var) {
    $this->time_rules = $var;
  }
  public function get_time_rules() {
    return $this->time_rules;
  }

  // yield
  public function set_yield($var) {
    $this->yield = $var;
  }
  public function get_yield() {
    return $this->yield;
  }

  // process_obj
  public function get_process_obj() {
    if (! isset($this->process_obj)) {
      include_once("processes.php");
      $this->process_obj = new Processes($this->get_given_config());
    }
    return $this->process_obj;
  }

  // img_url default
  public function get_img_url_default() {
    // todo move the hard-coded URL below to config file
    return "http://mudia.com/dash/_images/snd_broom.png";
  }

  // method
  private function make_process() {
    $obj = new Processes($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_business_plan_text_id()) {
      $this->set_type("get_by_bpt_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // debug
    //print "debug processes: given_id = " . $this->get_given_id() . "<br />\n";
    //print "debug processes: type = " . $this->get_type() . "<br />\n";
    //print "debug processes: given_bpt_id = " . $this->get_given_business_plan_text_id() . "<br />\n";
    //print "debug processes: name = " . $this->get_user_obj()->name . "<br />\n";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT processes.*, business_plan_texts.name, business_plan_texts.img_url, goal_statements.id, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND processes.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug processes sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      // todo move this to the view code object
      // todo for now however just allow for subset of all
      //if ($this->is_online_only()) {

      // pre-process order_by
      // todo select the default order_by using comments
      // todo organize this sort by priority
      //$order_by = "processes.priority, processes.sort DESC, processes.responsibility::integer";
      // todo old sort order_by with time_rules
      $order_by = "processes.sort DESC, processes.time_rules, processes.priority, processes.name";
      if ($this->get_given_sort()) {
        if ($this->get_given_sort() == "priority") {
          $order_by = "processes.priority";
        }
      }
      // based on view
      if ($this->get_given_view() == "all") {
        // all
        // todo fix this so that the user may select this sort order_by

        $sql = "SELECT processes.*, business_plan_texts.name, business_plan_texts.img_url, goal_statements.id, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND processes.status LIKE 'zoneline' ORDER BY processes.priority, " . $order_by . ";";

        // debug
        //print "debug processes sql = " . $sql . "<br />\n";

      } else if ($this->get_given_view() == "offline") {
        // all
        $sql = "SELECT processes.*, business_plan_texts.name, business_plan_texts.img_url, projects.id, projects.name, projects.img_url FROM processes, business_plan_texts, goal_statements, projects WHERE  projects.status = 'offline' AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id ORDER BY " . $order_by . ";";

      } else {
        if ($this->get_given_view() == "by-project") {
          $order_by = "projects.sort DESC, projects.name, processes.sort DESC, processes.priority, processes.name";       }
        // default
        // quick, by-project
        // note that the default may be set above with the data statements
        // removed: "processes.status = 'online' AND "
        $sql = "SELECT processes.*, business_plan_texts.name, business_plan_texts.img_url, goal_statements.id, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND processes.status NOT LIKE 'offline%' ORDER BY " . $order_by . ";";
      }

    } else if ($this->get_type() == "get_by_process_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT processes.*, business_plan_texts.name, business_plan_texts.img_url, goal_statements.id, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND processes.id = " . $this->get_given_process_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY processes.sort DESC, projects.id, business_plan_texts.name, processes.name;";

      // debug
      //print "debug sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT processes.*, business_plan_texts.name, business_plan_texts.img_url, goal_statements.id, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY processes.sort DESC, processes.name;";

      // debug
      //print "debug sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_bpt_id") {
      $sql = "SELECT processes.*, business_plan_texts.name, business_plan_texts.img_url, goal_statements.id, goal_statements.name, goal_statements.img_url, projects.id, projects.name, projects.img_url FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id ANd goal_statements.project_id = projects.id AND processes.business_plan_text_id = " . $this->get_given_business_plan_text_id() . " ORDER BY processes.sort DESC, processes.priority, processes.name;";

      // debug
      //print "debug sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_process();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->get_business_plan_text_obj()->set_id(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        $obj->set_priority(pg_result($results, $lt, 6));
        $obj->set_img_url(pg_result($results, $lt, 7));
        $obj->set_time_rules(pg_result($results, $lt, 8));
        $obj->set_responsibility(pg_result($results, $lt, 9));
        $obj->set_yield(pg_result($results, $lt, 10));
        $obj->get_business_plan_text_obj()->set_name(pg_result($results, $lt, 11));
        $obj->get_business_plan_text_obj()->set_img_url(pg_result($results, $lt, 12));
        $obj->get_business_plan_text_obj()->get_goal_statement_obj()->set_id(pg_result($results, $lt, 13));
        $obj->get_business_plan_text_obj()->get_goal_statement_obj()->set_name(pg_result($results, $lt, 14));
        $obj->get_business_plan_text_obj()->get_goal_statement_obj()->set_img_url(pg_result($results, $lt, 15));
        $obj->get_business_plan_text_obj()->get_project_obj()->set_id(pg_result($results, $lt, 16));
        $obj->get_business_plan_text_obj()->get_project_obj()->set_name(pg_result($results, $lt, 17));
        $obj->get_business_plan_text_obj()->get_project_obj()->set_img_url(pg_result($results, $lt, 18));
      }
    } else if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_bpt_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_process();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->get_business_plan_text_obj()->set_id(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        $obj->set_priority(pg_result($results, $lt, 6));
        $obj->set_img_url(pg_result($results, $lt, 7));
        $obj->set_time_rules(pg_result($results, $lt, 8)); 
        $obj->set_responsibility(pg_result($results, $lt, 9));
        $obj->set_yield(pg_result($results, $lt, 10));
        $obj->get_business_plan_text_obj()->set_name(pg_result($results, $lt, 11));
        $obj->get_business_plan_text_obj()->set_img_url(pg_result($results, $lt, 12));
        $obj->get_business_plan_text_obj()->get_goal_statement_obj()->set_id(pg_result($results, $lt, 13));
        $obj->get_business_plan_text_obj()->get_goal_statement_obj()->set_name(pg_result($results, $lt, 14));
        $obj->get_business_plan_text_obj()->get_goal_statement_obj()->set_img_url(pg_result($results, $lt, 15));
        $obj->get_business_plan_text_obj()->get_project_obj()->set_id(pg_result($results, $lt, 16));
        $obj->get_business_plan_text_obj()->get_project_obj()->set_name(pg_result($results, $lt, 17));
        $obj->get_business_plan_text_obj()->get_project_obj()->set_img_url(pg_result($results, $lt, 18));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_process();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type: <kbd>" . $type . "</kbd>");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_view();

    $markup .= $this->output_sort();

    $display_stats_toggle = "1";

    if ($this->get_given_view() == "quick") {
      $markup .= $this->output_table_quick($display_stats_toggle);
    } else {
       $markup .= $this->output_table($display_stats_toggle);
    }

    return $markup;
  }

  // method
  public function output_table($given_display_stats_toggle =  "") {
    $markup = "";

    // todo based upon CISA Review Manual 2005 "Business Process Reengineering and Process Change Projects" (page 429)
    print "<p>owner_responsible, staff_roles, customers, estimated_duration, trigger, frequency, inputs, yields, risks_and_control, performance_measurement, steps</p>\n";

    // guts of the list
    $markup .= "<table class=\"plants\" style=\"font-size: 120%;\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";

    // process_flows
    $process_flows_display_flag = 0;
    if ($process_flows_display_flag) {
      $markup .= "  <td class=\"header\" style=\"width: 30px;\">\n";
      $url = $this->url("process_flows");
      $markup .= "    <a href=\"" . $url . "\">process_flows</a>\n";
      $markup .= "  </td>\n";
    }

    $markup .= "  <td class=\"header\" style=\"width: 120px;\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 24px;\">\n";
    $markup .= "    proj.\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 24px;\">\n";
    $markup .= "    g.s.\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 24px;\">\n";
    $markup .= "    b.p.t.\n";
    $markup .= "  </td>\n"; 
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    resp. day\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 140px; margin: 10px;\" cellpadding=\"6\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    // todo recognize the next three as an important series
    $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    img\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 380px;\">\n";
    $markup .= "    process name\n";
    $markup .= "  </td>\n";

    if (1) {
      $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
      // todo make this link sort the list by this field
      $url = $this->url("processes?view=priority");
      $markup .= "    <a href=\"" . $url . "\">priority</a>\n";
      $markup .= "  </td>\n";
    }
    if (1) {
      $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
      $url = $this->url("processes?view=yield");
      $markup .= "    <a href=\"" . $url . "\">yield</a>\n";
      $markup .= "  </td>\n";
    }
    if (0) {
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $url = $this->url("tickets");
      $markup .= "    <a href=\"" . $url . "\">tickets</a><br />count\n";
      $markup .= "  </td>\n";
    }

    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    domains\n";
    $markup .= "  </td>\n";

    $flag_se = 0;
    if ($flag_se) {
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      //$markup .= "    polymorphic count\n";
      $url = $this->url("scene_elements") . "?view=by-parent-class-name";
      $markup .= "    <a href=\"" . $url . "\">s.e. count</a>\n";
      $markup .= "  </td>\n";
    }

    // s.e. class
    $markup .= "  <td class=\"header\" style=\"width: 280px;\">\n";
    // todo this url could be more specific, need to create a parameter
    $url = $this->url("scene_elements") . "?view=by-parent-class-name";
    $markup .= "    <a href=\"" . $url . "\">s.e. class</a>\n";
    $markup .= "  </td>\n";

    // process_flows_display_flag
    if ($process_flows_display_flag) {
      $markup .= "  <td class=\"header\" style=\"width: 30px;\">\n";
      $url = $this->url("process_flows");
      $markup .= "    <a href=\"" . $url . "\">process_flows</a>\n";
      $markup .= "  </td>\n";
    }
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $sort_green_count = 0;
    $sort_red_count = 0;
    $responsibility_days_count_count = 0;
    $priority_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $process) {
      $markup .= "<tr>\n";

      // todo potentially need to change all standard classes ...
      // todo so that found instances know same databasedashboard
      // provide the instance the databasedashboard
      //$process->set_db_dash($this->get_db_dash());

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // process_flows (in)
      if ($process_flows_display_flag) {
        $markup .= "  <td style=\"text-align: left; font-size: 80%;\">\n";
        if ($process->get_id()) {
          include_once("process_flows.php");
          $process_flow_obj = new ProcessFlows($this->get_given_config());
          $user_obj = $this->get_user_obj();
          $process_flows_data = $process_flow_obj->get_process_flows_in_given_process_id($process->get_id(), $user_obj);
          $markup .= $process_flows_data . "\n";
        }
        $markup .= "  </td>\n";
      }

      // status
      // todo problem is that the font-size is a poor fix for big string issue
      $markup .= "  <td style=\"text-align: center; background-color: " . $process->get_status_background_color() . "; font-size: 50%;\">\n";
      $markup .= "    " . $process->get_status() . "\n";
      $markup .= "  </td>\n";

      // projects
      $markup .= "  <td style=\"text-align: center;\">\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      // todo be able to configure the width of the process img_element
      $width = "20";
      $height = "20";
      $markup .= "    " . $process->get_business_plan_text_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      $markup .= "  </td>\n";

      // goal_statements
      $markup .= "  <td style=\"text-align: center;\">\n";
      if ($process->get_business_plan_text_obj()->get_goal_statement_obj()->get_id()) {
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "20";
      $height = "20";
      $markup .= "    " . $process->get_business_plan_text_obj()->get_goal_statement_obj()->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      }
      $markup .= "  </td>\n";

      // business_plan_texts
      $markup .= "  <td style=\"text-align: center;\">\n";
      if ($process->get_business_plan_text_obj()->get_id()) {
        $padding = " 0px 0px 0px 0px";
        $float = "";
        $width = "20";
        $height = "20";
        $markup .= "    " . $process->get_business_plan_text_obj()->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      } else {
        $markup .= "error: business_plan_texts.id is not known<br />\n";
      }
      $markup .= "  </td>\n";

      // resp. day
      $responisibility_count = $process->get_responsibility();
      $sort = $process->get_sort_no_z();
      // todo clean up because it may not be needed
      include_once("lib/responsibilities.php");
      $obj = new Responsibilities($this->get_given_config());
      $background_color = $obj->get_report_background_color($responisibility_count, $sort);
      if ($background_color == "#A6D785") {
        $sort_green_count++;
      } else {
        $sort_red_count++;
      }
      $markup .= "  <td style=\"background-color: " . $background_color . "; text-align: center;\">\n";
      if ($process->get_responsibility()) {
        $markup .= "    <small>" . $process->get_responsibility() . "</small>\n";
        $responsibility_days_count_count += $process->get_responsibility();
      }
      $markup .= "  </td>\n";

      // sort
      $markup .= $process->get_sort_cell();

      // id
      $markup .= "  <td style=\"text-align: right;\">\n";
      $markup .= "    " . $process->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img
      $markup .= "  <td style=\"text-align: center; background-color: #0099CC;\">\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      // todo be able to configure the width of the process img_element
      $width = "20";
      $height = "20";
      $markup .= "    " . $process->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      $markup .= "  </td>\n";

      // processes.name
      $name = $process->get_name();
      if (strpos($name,'develop') !== false) {
        $markup .= "  <td style=\"background-color: #CCFF88;\">\n";
      } else if (strpos($name,'chore') !== false) {
        $markup .= "  <td style=\"background-color: #A4D3EE;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $process->set_given_process_id($this->get_given_process_id());
      $markup .= "    <strong>" . $process->get_name_with_link() . "</strong>\n";
      $markup .= "  </td>\n";

      // priority
      if (1) {
        // note heads-up colorize by string length
        if ($process->get_priority() < 100) {
          $background_color = "#A6D785";
        } else {
          $background_color = "#CD5555";
        }
        $markup .= "  <td style=\"background-color: " . $background_color . "; text-align: center;\">\n";
        $markup .= "    " . $process->get_priority() . "\n";
        //if ($process->get_priority() == "1") {
        //  $priority_count += $process->get_priority();
        //}
        $markup .= "  </td>\n";
      }

      // yield
      if (1) {
        if ($process->get_yield()) {
          $background_color = "#A6D785";
        } else {
          $background_color = "#CD5555";
        }
        $markup .= "  <td style=\"background-color: " . $background_color . ";\">\n";
        $markup .= "    " . $process->get_yield() . "\n";
        $markup .= "  </td>\n";
      }

      // tickets count
      if (0) {
        include_once("lib/counter.php");
        $counter_obj = new Counter($this->get_given_config());
        $user_obj = $process->get_user_obj();
        $counter_obj->set_user_obj($user_obj);
        $output_name_flag = 0;
        $markup .= $counter_obj->output_count_cell_given_process_id("tickets", $process->get_id(), $output_name_flag);
      }

      // domains (derived subset)
      // todo this code is duplicated from elsewhere in this file
      include_once("domains.php");
      $domain_obj = new Domains($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $domain_info = $domain_obj->get_domains_list_given_process_id($process->get_id(), $user_obj);
      if ($domain_info) {
        $markup .= "  <td style=\"text-align: center; background-color: #EFEF33;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $markup .= $domain_info . "\n";
      $markup .= "  </td>\n";

      if ($flag_se) {
        // scene_element count (s.e. count)
        if (strpos($process->get_scene_element_count(), ">0<") === FALSE) {
          $markup .= "  <td style=\"text-align: center;\">\n";
        } else {
          $markup .= "  <td style=\"text-align: center; background-color: #CD5555;\">\n";
        }
        $markup .= $process->get_scene_element_count() . "\n";
        $markup .= "  </td>\n";
      }

      // scene_element class name (s.e. class)
      $se_classes_name_with_link = $process->get_scene_element_classes_name("with-link");
      if ($se_classes_name_with_link) {
        $background_color = "#A6D785";
        $markup .= "  <td style=\"text-align: center; background-color: " . $background_color . ";\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center; background-color: #CCCCCC;\">\n";

      }
      $markup .= "<span style=\"font-size: 90%;\">";
      $markup .= $se_classes_name_with_link;
      $markup .= "</span>\n";
      $markup .= "  </td>\n";

      // flows
      // process_flows (out)
      if ($process_flows_display_flag) {
        $markup .= "  <td style=\"text-align: left; font-size: 80%;\">\n";
        include_once("process_flows.php");
        $process_flow_obj = new ProcessFlows($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $process_flows_data = $process_flow_obj->get_process_flows_out_given_process_id($process->get_id(), $user_obj);
        $markup .= $process_flows_data . "\n";
        $markup .= "  </td>\n";
      }

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

    // display_stats
    if ($given_display_stats_toggle) {
      $markup .= $this->get_display_stats($responsibility_days_count_count, $num, $sort_green_count, $sort_red_count);
    }

    // more stats
    $markup .= "<p>priority count = " . $priority_count . "</p>\n";

    return $markup;
  }

  // method
  public function output_table_quick($given_display_stats_toggle =  "") {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    proj.\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    img\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 380px;\">\n";
    $markup .= "    process name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 100px; align: center;\">\n";
    $markup .= "    sort<br />\n";
    $markup .= "    timecards\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    resp. day\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 100px;\">\n";
    $markup .= "    time rules\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
    // todo make this link sort the list by this field
    $url = $this->url("processes?view=quick&amp;sort=priority");
    $markup .= "    <a href=\"" . $url . "\">priority</a> (roles)\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 260px;\">\n";
    $url = $this->url("tickets");
    $markup .= "    &nbsp;<a href=\"" . $url . "\">tickets</a>&nbsp;(role&nbsp;goals)&nbsp;\n";
    $markup .= "  </td>\n";

    //$markup .= "  <td class=\"header\" style=\"width: 100px;\">\n";
    //$markup .= "    shifts\n";
    //$markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("scene_elements") . "?view=by-parent-class-name";
    $markup .= "    <a href=\"" . $url . "\">s.e. count</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("domains");
    $markup .= "    <a href=\"" . $url . "\">domains</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center; width: 240px;\">\n";
    // todo this url could be more specific, need to create a parameter
    $url = $this->url("scene_elements") . "?view=by-parent-class-name";
    $markup .= "    <a href=\"" . $url . "\">s.e. class</a><br />\n";
    $markup .= "    stocks\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 440px;\">\n";
    //$markup .= "    budgets flows<br />\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center; width: 240px;\">\n";
    $url = $this->url("plant_lists");
    $markup .= "    <a href=\"" . $url . "\">plant_lists</a><br />\n";
    $markup .= "  </td>\n";
    if (0) {
      $markup .= "  <td class=\"header\" style=\"width: 30px;\">\n";
      $url = $this->url("process_flows");
      $markup .= "    <a href=\"" . $url . "\">process_flows</a>\n";
      $markup .= "  </td>\n";
    }
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $sort_green_count = 0;
    $sort_red_count = 0;
    $responsibility_days_count_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $process) {
      $markup .= "<tr>\n";

      // todo potentially need to change all standard classes ...
      // todo so that found instances know same databasedashboard
      // provide the instance the databasedashboard
      //$process->set_db_dash($this->get_db_dash());

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // projects
      $markup .= "  <td>\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "66";
      $height = "99";
      $markup .= "    " . $process->get_business_plan_text_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      $markup .= "  </td>\n";

      // status
      $markup .= "  <td style=\"text-align: center; background-color: " . $process->get_status_background_color() . ";\">\n";
      $markup .= "    " . $process->get_status() . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $process->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img
      //$markup .= "  <td style=\"background-color: #0099CC;\">\n";
      $markup .= "  <td>\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "66";
      $height = "99";
      $markup .= "    " . $process->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      $markup .= "  </td>\n";

      // processes.name
      $name = $process->get_name();
      //if (strpos($name,'develop') !== false || strpos($name,'webmaster') !== false) {
      // green
      //  $markup .= "  <td style=\"background-color: #CCFF88;\">\n";
      //} else if (strpos($name,'chore') !== false) {
      //  $markup .= "  <td style=\"background-color: #A4D3EE;\">\n";
      //} else {
        $markup .= "  <td>\n";
      //}
      $process->set_given_process_id($this->get_given_process_id());
      $markup .= "    <strong>" . $process->get_name_with_link() . "</strong>\n";
      $markup .= "  </td>\n";

      // sort with timecards
      include_once("timecards.php");
      $timecards_obj = new Timecards($this->get_given_config());
      $process_id = $process->get_id();
      // debug
      //print "debug processes process_id = " . $process_id . "<br />\n";
      $timecards_data = "";
      if ($process_id) {
        $user_obj = $this->get_user_obj();
        $url = $this->url("timecards/process/" . $process->get_id());
        $timecards_data = "[<a href=\"" . $url . "\">" . $timecards_obj->get_count_given_process_id($process_id, $user_obj) . "&nbsp;timecards</a>]";
      }
      $markup .= $process->get_sort_cell($timecards_data);

      // resp. day
      $responisibility_count = $process->get_responsibility();
      $sort = $process->get_sort_no_z();
      // todo clean up because it may not be needed
      include_once("lib/responsibilities.php");
      $obj = new Responsibilities($this->get_given_config());
      $background_color = $obj->get_report_background_color($responisibility_count, $sort);
      if ($background_color == "#A6D785") {
        $sort_green_count++;
      } else {
        $sort_red_count++;
      }
      $markup .= "  <td style=\"background-color: " . $background_color . "; text-align: center;\">\n";
      if ($process->get_responsibility()) {
        $markup .= "    " . $process->get_responsibility() . "\n";
        $responsibility_days_count_count += $process->get_responsibility();
      }
      $markup .= "  </td>\n";

      // time_rules
      $markup .= $process->get_time_cell();

      // priority
      $priority = $process->get_priority();
      if (strstr($priority, "documentation")) {
        $background_color = "#0099CC";
      } else if ($priority) {
        //$background_color = "#A6D785";
        $background_color = "";
      } else {
        // red
        // $background_color = "#CD5555";
        // background
        $background_color = "#CCC";
      }
      $markup .= "  <td style=\"background-color: " . $background_color . ";\">\n";
      $markup .= "    " . $process->get_priority() . "\n";
      $markup .= "  </td>\n";

      // tickets
      include_once("tickets.php");
      $ticket_obj = new Tickets($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $tickets_data = $ticket_obj->get_tickets_given_process_id($process->get_id(), $user_obj);
      $markup .= "  <td>\n";
      $markup .= "    <span style=\"font-size: 80%;\">" . $tickets_data . "</span>\n";
      $markup .= "  </td>\n";

      // shifts
      //$markup .= $process->get_shift_cell();

      // scene_element count (s.e. count)
      if (strpos($process->get_scene_element_count(), ">0<") === FALSE) {
        $markup .= "  <td style=\"text-align: center;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center; background-color: #CD5555;\">\n";
      }
      $markup .= $process->get_scene_element_count() . "\n";
      $markup .= "  </td>\n";

      // domains
      include_once("domains.php");
      $domain_obj = new Domains($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $domain_info = $domain_obj->get_domains_list_given_process_id($process->get_id(), $user_obj);
      if ($domain_info) {
        $markup .= "  <td style=\"text-align: center; background-color: #EFEF33;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $markup .= $domain_info . "\n";
      $markup .= "  </td>\n";

      // scene_element classes (s.e. class)
      $se_classes_id = $process->get_scene_element_classes_id("with-link");
      if ($se_classes_id) {
        $markup .= "  <td style=\"text-align: center; background-color: #EFEFEF;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center; background-color: #CCCCCC;\">\n";
      }
      // se_class_id
      $markup .= "  " . $se_classes_id . "<br />\n";
      // todo refresh my memory as to what stocks are
      //$markup .= "    <span style=\"font-size: 50%;\">" . $process->get_stocks() . "</span>\n";
      // complete row markup
      $markup .= "  </td>\n";

      // budgets flows
      if (0) {
        include_once("budgets.php");
        $budget_obj = new Budgets($this->get_given_config());
        $user_obj = $this->get_user_obj();
        // debug using test array
        //$scene_element_id_list = array(274, 302, 298);
        // get ids as array
        $scene_element_id_list = $process->get_scene_element_id_as_array();
        $budget_info = $budget_obj->get_derived_budget_net_given_scene_element_id_list($scene_element_id_list, $user_obj);
        // set color
        if ($budget_info) {
          //$color = "#FEE8D6";
          $color = "#FEE5A0";
          $markup .= "  <td style=\"text-align: center; background-color: " . $color . ";\">\n";
        } else {
          $markup .= "  <td style=\"text-align: center; background-color: #CCCCCC;\">\n";
        }
        // todo this is too big and need to get net amount not big table
        $markup .= $budget_info . "\n";
        $markup .= "  </td>\n";
      }

      // plant_lists flows
      include_once("plant_lists.php");
      $plant_list_obj = new PlantLists($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_lists_flows = $plant_list_obj->get_count_given_process_id($process->get_id(), $user_obj);
      // set color
      if ($plant_lists_flows) {
        //$color = "#FEE8D6";
        $color = "#FEE5A0";
        $markup .= "  <td style=\"text-align: center; background-color: " . $color . ";\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center; background-color: #CCCCCC;\">\n";
      }
      if ($plant_lists_flows) {
        $url = $this->url("plant_lists/processes/" . $process->get_id());
        $markup .= "[<a href=\"" . $url . "\">" . $plant_lists_flows . "&nbsp;plant_lists</a>]\n";
      }
      $markup .= "  </td>\n";

      // process_flows
      if (0) {
        $markup .= "  <td style=\"text-align: left; font-size: 80%;\">\n";
        include_once("process_flows.php");
        $process_flow_obj = new ProcessFlows($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $process_flows_data = $process_flow_obj->get_process_flows_in_given_process_id($process->get_id(), $user_obj);
        $process_flows_data .= $process_flow_obj->get_process_flows_out_given_process_id($process->get_id(), $user_obj);
        $markup .= $process_flows_data . "\n";
        $markup .= "  </td>\n";
      }

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

    // display_stats
    if ($given_display_stats_toggle) {
      $markup .= $this->get_display_stats($responsibility_days_count_count, $num, $sort_green_count, $sort_red_count);
    }

    return $markup;
  }

  // method
  private function get_scene_element_id_as_array() {
    $scene_element_id_list = array();
 
    include_once("scene_elements.php");
    $scene_element_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    return $scene_element_obj->get_scene_element_id_list_given_processes_id($this->get_id(), $user_obj);
  }

  // method
  private function get_display_stats($responsibility_days_count_count, $num, $sort_green_count, $sort_red_count) {
    $markup = "";

    // totals
    $markup .= "<p>total responsibility days = " . $responsibility_days_count_count . "</p>\n";
    $markup .= "<p>total processes count = " . $num . "</p>\n";

    $average_rdc_per_process = "";
    if ($num) {
      $average_rdc_per_process = round($responsibility_days_count_count / $num);
      $markup .= "<p>average responsibility days per process = " . $average_rdc_per_process . "</p>\n";
    } else {
      // skip (do not divide by zero)
    }

    if ($average_rdc_per_process) {
      $average_processes_per_day = round($num / $average_rdc_per_process);
      $markup .= "<p>average processes per responsbility day = " . $average_processes_per_day . "</p>\n";
    } else {
      // skip (do not divide by zero)
    }
    //$markup .= "<br />\n";

    $markup .= "<p>sort green count = " . $sort_green_count . "</p>\n";
    $markup .= "<p>sort red count = " . $sort_red_count . "</p>\n";
    if ($sort_green_count + $sort_red_count) {
      $percent_green = round(($sort_green_count / ($sort_green_count + $sort_red_count)) * 100);
      $markup .= "<p>percent green = " . $percent_green . "%</p>\n";
    } else {
      // skip (do not divide by zero)
    }

    return $markup;
  }

  // method
  public function output_aggregate_simple() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 30px;\">\n";
    $url = $this->url("process_flows");
    $markup .= "    <a href=\"" . $url . "\">process_flows</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    img\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 380px;\">\n";
    $markup .= "    process name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    resp. day\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
    // todo make this link sort the list by this field
    $url = $this->url("");
    $markup .= "    <a href=\"" . $url . "\">priority</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    //$markup .= "    polymorphic count\n";
    $url = $this->url("scene_elements") . "?view=by-parent-class-name";
    $markup .= "    <a href=\"" . $url . "\">s.e. count</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    // todo this url could be more specific, need to create a parameter
    $url = $this->url("scene_elements") . "?view=by-parent-class-name";
    $markup .= "    <a href=\"" . $url . "\">s.e. class</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 30px;\">\n";
    $url = $this->url("process_flows");
    $markup .= "    <a href=\"" . $url . "\">process_flows</a>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $responsibility_days_count_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $process) {
      $markup .= "<tr>\n";

      // todo potentially need to change all standard classes ...
      // todo so that found instances know same databasedashboard
      // provide the instance the databasedashboard
      //$process->set_db_dash($this->get_db_dash());

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // process_flows (in)
      $markup .= "  <td style=\"text-align: left; font-size: 80%;\">\n";
      if ($process->get_id()) {
        include_once("process_flows.php");
        $process_flow_obj = new ProcessFlows($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $process_flows_data = $process_flow_obj->get_process_flows_in_given_process_id($process->get_id(), $user_obj);
        $markup .= $process_flows_data . "\n";
      }
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $process->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img
      $markup .= "  <td style=\"background-color: #0099CC;\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $process->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // processes.name
      $name = $process->get_name();
      if (strpos($name,'develop') !== false) {
        $markup .= "  <td style=\"background-color: #CCFF88;\">\n";
      } else if (strpos($name,'chore') !== false) {
        $markup .= "  <td style=\"background-color: #A4D3EE;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $process->set_given_process_id($this->get_given_process_id());
      $markup .= "    <strong>" . $process->get_name_with_link() . "</strong>\n";
      $markup .= "  </td>\n";

      // resp. day
      $markup .= "  <td style=\"text-align: center;\">\n";
      if ($process->get_responsibility()) {
        $markup .= $process->get_responsibility() . "\n";
        $responsibility_days_count_count += $process->get_responsibility();

      }
      $markup .= "  </td>\n";

      // priority
      $priority = $process->get_priority();
      if (strstr($priority, "documentation")) {
        $background_color = "#0099CC";
      } else if ($priority < 100) {
        $background_color = "#A6D785";
      } else {
        $background_color = "#CD5555";
      }
      $markup .= "  <td style=\"background-color: " . $background_color . ";\">\n";
      $markup .= "    " . $process->get_priority() . "\n";
      $markup .= "  </td>\n";

      // scene_element count (s.e. count)
      if (strpos($process->get_scene_element_count(), ">0<") === FALSE) {
        $markup .= "  <td style=\"text-align: center;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center; background-color: #CD5555;\">\n";
      }
      $markup .= $process->get_scene_element_count() . "\n";
      $markup .= "  </td>\n";

      // scene_element class (s.e. class)
      // todo copy from above func
      //$se_classes_id = $process->get_scene_element_classes_id("with-link");
      if ($se_classes_id) {
        $markup .= "  <td style=\"text-align: center; background-color: #EFEFEF;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center; background-color: #CCCCCC;\">\n";
      }
      //$markup .= $se_classes_id . "\n";
      $markup .= "  </td>\n";

      // process_flows (out)
      $markup .= "  <td style=\"text-align: left; font-size: 80%;\">\n";
      if ($process->get_id()) {
        include_once("process_flows.php");
        $process_flow_obj = new ProcessFlows($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $proces_flows_data = $process_flow_obj->get_process_flows_out_given_process_id($process->get_id(), $user_obj);
        $markup .= $proces_flows_data . "\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_aggregate_list() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    img\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 380px;\">\n";
    $markup .= "    process name\n";
    $markup .= "  </td>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    //$markup .= "    polymorphic count\n";
    $url = $this->url("scene_elements") . "?view=by-parent-class-name";
    $markup .= "    <a href=\"" . $url . "\">scene elements count</a>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $responsibility_days_count_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $process) {
      $markup .= "<tr>\n";

      // todo potentially need to change all standard classes ...
      // todo so that found instances know same databasedashboard
      // provide the instance the databasedashboard
      //$process->set_db_dash($this->get_db_dash());

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $process->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img
      $markup .= "  <td style=\"background-color: #0099CC;\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $process->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // processes.name
      $name = $process->get_name();
      if (strpos($name,'develop') !== false) {
        $markup .= "  <td style=\"background-color: #CCFF88;\">\n";
      } else if (strpos($name,'chore') !== false) {
        $markup .= "  <td style=\"background-color: #A4D3EE;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $process->set_given_process_id($this->get_given_process_id());
      $markup .= "    <strong>" . $process->get_name_with_link() . "</strong>\n";
      $markup .= "  </td>\n";

      // scene_element count (s.e. count)
      if (strpos($process->get_scene_element_count(), ">0<") === FALSE) {
        $markup .= "  <td style=\"text-align: center;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center; background-color: #CD5555;\">\n";
      }
      $markup .= $process->get_scene_element_count() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    $markup .= "<h3>Special Process Fields</h3>\n";

    // nonstandard
    $markup .= "<table class=\"plants\">\n";

    // priority
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header-vertical\">\n";
    $markup .= "    <em>priority</em>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $this->get_priority() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // r d c
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header-vertical\">\n";
    $markup .= "    <em>responsibility</em>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"padding: 10px 10px 10px 10px;\">\n";
    $markup .= "    " . $this->get_responsibility() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // time_rules
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header-vertical\">\n";
    $markup .= "    <em>time_rules</em>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $this->get_time_rules() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</table>\n";
    $markup .= "<br />\n";

    $style_flag = "standard_flag";
    if ($this->get_given_view() == "printable") {
      // sidecar (hint reference style)
      // note intended to be a quick "hint reference" as to what is next
      $style_flag = "hint_flag";
    }

    include_once("scene_elements.php");
    $scene_element_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $heading = "true"; 
    $column_design = "in-process";
    $markup .= $scene_element_obj->output_sidecar($this->get_id(), $user_obj, $style_flag, $heading, $column_design);

    // before leaving, reset
    // todo ok so this means it has to get db data again, eventually fix that
    $markup .= $scene_element_obj->get_list_bliss()->empty_list();

    if ($this->get_given_view() == "printable") {
      //skip
    } else {

      // output count
      $markup .= "<br />\n";
      $markup .= "scene element count = " . $this->get_scene_element_count() . "\n";

      // output display
      include_once("scene_elements.php");
      $scene_element_obj = new SceneElements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      // todo set up the variable below in the user's config file
      $heading = "true";
      $debug_flag = "true";
      $markup .= $scene_element_obj->display($this->get_id(), $user_obj, $heading, $debug_flag);

      // before leaving, reset
      // todo ok so this means it has to get db data again, eventually fix that
      $markup .= $scene_element_obj->get_list_bliss()->empty_list();

      // list tickets
      include_once("tickets.php");
      $tickets_obj = new Tickets($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $heading = "true";
      $markup .= $tickets_obj->output_sidecar($this->get_id(), $user_obj, $heading);

      // list process_flows
      include_once("process_flows.php");
      $process_flow_obj = new ProcessFlows($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $heading = "true";
      $markup .= $process_flow_obj->output_sidecar($this->get_id(), $user_obj, $heading);
    }

    // todo clean up the following
    //$markup .= "<h2>More Fields</h2>\n";
    //$markup .= "<table class=\"plants\">\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>owner responsible</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_owner_responsible() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>staff roles</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_staff_roles() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>customers</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_customers() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>estimated duration</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_estimated_duration() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>trigger</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_trigger() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>frequency</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_frequency() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>inputs</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_inputs() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>yields</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_yields() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>risks and control</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_risks_and_control() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>performance measurement</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_performance_measurement() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "<tr>\n";
    //$markup .= "  <td class=\"header-vertical\">\n";
    //$markup .= "    <em>steps</em>\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    // debug following is on hold
    //$markup .= "    " . $this->get_steps() . "\n";
    //$markup .= "    (on hold)";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";
    //$markup .= "</table>\n";

    // todo display history of this process
    //$markup .= "<h3>History</h3>\n";
    //$markup .= "<p>Eventually, the system will accept log reports, so that a history of the process being performed can be recorded.</p>\n";

    return $markup;
  }

  // method
  protected function output_preface_single() {
    $markup = "";

    // associated
    // name with link
    $padding = "";
    $float = "";
    $width = "32";
    $markup .= $this->get_business_plan_text_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width);
    $markup .= " ";
    $markup .= $this->get_business_plan_text_obj()->get_project_obj()->get_id();
    $markup .= " ";
    $markup .= $this->get_business_plan_text_obj()->get_project_obj()->get_name() . "<br />\n";

    // associated
    // list related goal_statemensts
    // todo figure out where this next line goes, it is lost
    //$heading = "true";
    $padding = "";
    $float = "";
    $width = "32";
    $markup .= $this->get_business_plan_text_obj()->get_goal_statement_obj()->get_img_as_img_element_with_link($padding, $float, $width);
    $markup .= " ";
    $markup .= $this->get_business_plan_text_obj()->get_goal_statement_obj()->get_id();
    $markup .= " ";
    $markup .= $this->get_business_plan_text_obj()->get_goal_statement_obj()->get_name();
    include_once("goal_statements.php");
    $goal_statement_obj = new GoalStatements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $bpt_id = $this->get_id();
    //$markup .= $goal_statement_obj->get_goal_statement_given_bpt_id($bpt_id, $user_obj);
    $markup .= "<br />\n";

    // associated
    // name with link
    include_once("business_plan_texts.php");
    $business_plan_text_obj = new BusinessPlanTexts($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $business_plan_text_id = $this->get_business_plan_text_obj()->get_id();
    $markup .= $business_plan_text_obj->get_three_part_given_id($business_plan_text_id, $user_obj);
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  protected function output_frontrunner() {
    $markup = "";

    // todo the following was replaced by preface
    if (0) {
      
      $markup .= "<br />\n";

      // nonstandard
      $markup .= "<table class=\"plants\">\n";

      // project
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header-vertical\">\n";
      $markup .= "project";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "32";
      $markup .= $this->get_business_plan_text_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width);
      $markup .= $this->get_business_plan_text_obj()->get_project_obj()->get_name_with_link();
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // goal_statements
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header-vertical\">\n";
       $markup .= "goal_statements";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "32";
      $markup .= $this->get_business_plan_text_obj()->get_goal_statement_obj()->get_img_as_img_element_with_link($padding, $float, $width);
      $markup .= $this->get_business_plan_text_obj()->get_goal_statement_obj()->get_name_with_link();
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // business_plan_text
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header-vertical\">\n";
      $markup .= "    business_plan_text\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "32";
      $markup .= $this->get_business_plan_text_obj()->get_img_as_img_element_with_link($padding, $float, $width);
      $markup .= "    " . $this->get_business_plan_text_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "</table>\n";
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  public function get_process_id_given_process_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 1) {
      // 1 row exists, so output
      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_business_plan_text_obj()->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function get_processes_given_business_plan_text_id($given_business_plan_text_id, $given_user_obj, $print_flag = "") {
    $markup = "";

    // set
    $this->set_given_business_plan_text_id($given_business_plan_text_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug given_business_plan_text_id = " . $this->get_given_business_plan_text_id() . "<br />\n";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $padding = " padding: 1px 0px 1px 14px;";
      $first_flag = 1;
      foreach ($this->get_list_bliss()->get_list() as $process) {
        if ($first_flag) {
          $markup .= "<ul style=\"margin: 6px 1px 2px 1px;" . $padding ."\">\n";;
          $first_flag = 0;
        }
        if ($print_flag == "no-links") {
          $markup .= "<li style=\"margin: 0px;" . $padding . "\">" . $process->get_name() . "</li>\n";
        } else if ($print_flag == "hidden_underlink_link") {
          $markup .= "<li style=\"margin: 0px;" . $padding . "\">" . $process->get_name_with_hidden_link() . "</li>\n";
        } else {
          $markup .= "<li style=\"margin: 0px; padding: 2px;\"><span style=\"color: #996699;\">" . $process->get_id() . "</span> " . $process->get_name_with_link() . "</li>\n";
        }
      }
      $markup .= "</ul>\n";;
    } else {
      $markup .= "No processes.\n";;
    }

    return $markup;
  }

  // method
  function get_processes_table_given_bpt_id($given_user_obj, $given_bpt_id, $style_flag = "", $given_display_stats_toggle = "") {
    $markup = "";

    // load data from database
    $this->set_given_business_plan_text_id($given_bpt_id);
    $this->set_user_obj($given_user_obj);
    $this->set_given_display_stats_toggle($given_display_stats_toggle);

    // get data
    $this->determine_type();

    // debug
    //print "debug Processes given_business_plan_text_id = " . $this->get_given_business_plan_text_id() . "<br />\n";
    //print "debug Processes type = " . $this->get_type() . "<br />\n";
    //print "debug Processes style_flag = " . $style_flag . "<br />\n";

    $markup = $this->prepare_query();
    // check for error message
    if ($markup) {
      print "error process: " . $markup;
    }

    $plural_string = "";
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">No processes were found.</p>\n";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() == 1) {
      $plural_string = "";
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $plural_string = "es";
    }

    if ($style_flag == "" || $style_flag == "standard_flag") {
      $markup .= "<h4>Processes</h4>\n";

      // simple list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\" style=\"width: 80px;\">\n";
      $markup .= "    projects\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 120px;\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\" style=\"width: 300px;\">\n";
      //$markup .= "    business plan text\n";
      //$markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"\">\n";
      $markup .= "    img\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 380px;\">\n";
      $markup .= "    process name\n";
      $markup .= "  </td>\n";
      // todo have the heading follow the behavior of the cell
      if (0) {
        $markup .= "  <td class=\"header\" style=\"width: 200px;\">\n";
        // todo make this link sort the list by this field
        $url = $this->url("");
        $markup .= "    <a href=\"" . $url . "\">priority</a>\n";
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      $sort_green_count = 0;
      $sort_red_count = 0;
      $responsibility_days_count_count = 0;
      foreach ($this->get_list_bliss()->get_list() as $process) {
        $markup .= "<tr>\n";

        // todo potentially need to change all standard classes ...
        // todo so that found instances know same databasedashboard
        // provide the instance the databasedashboard
        //$process->set_db_dash($this->get_db_dash());

        // projects
        $markup .= "  <td>\n";
        $padding = " 0px 0px 0px 0px";
        $float = "";
        $width = "65";
        $markup .= "    " . $process->get_business_plan_text_obj()->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
        $markup .= "  </td>\n";

        // num
        $num++;
        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // status
        $markup .= "  <td style=\"text-align: center; background-color: " . $process->get_status_background_color() . "\">\n";
        $markup .= "    " . $process->get_status() . "\n";
        $markup .= "  </td>\n";

        // sort
        $markup .= $process->get_sort_cell();

        // business_plan_texts
        if (0) {
          $markup .= "  <td>\n";
          if ($process->get_business_plan_text_obj()->get_id()) {
            $markup .= "    " . $process->get_business_plan_text_obj()->get_name_with_link() . "\n";
          } else {
            $markup .= "error: business_plan_texts.id is not known<br />\n";
          }
          $markup .= "  </td>\n";
        }

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $process->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // img
        $markup .= "  <td style=\"background-color: #0099CC;\">\n";
        $padding = " 0px 0px 0px 0px";
        $float = "";
        $width = "65";
        $markup .= "    " . $process->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
        $markup .= "  </td>\n";

        // processes.name
        $name = $process->get_name();
        if (strpos($name,'develop') !== false) {
          $markup .= "  <td style=\"background-color: #CCFF88;\">\n";
        } else if (strpos($name,'chore') !== false) {
          $markup .= "  <td style=\"background-color: #A4D3EE;\">\n";
        } else {
          $markup .= "  <td>\n";
        }
        $process->set_given_process_id($this->get_given_process_id());
        $markup .= "    <strong>" . $process->get_name_with_link() . "</strong>\n";
        $markup .= "  </td>\n";

        // priority
        // todo make this a configuration setting and add flag variable
        if (0) {
          $priority = $process->get_priority();
          if (strstr($priority, "documentation")) {
            $background_color = "#0099CC";
          } else if ($priority) {
            $background_color = "#A6D785";
          } else {
            $background_color = "#CD5555";
          }
          $markup .= "  <td style=\"background-color: " . $background_color . ";\">\n";
          $markup .= "    " . $process->get_priority() . "\n";
          $markup .= "  </td>\n";
        }

        $markup .= "</tr>\n";

       }

      $markup .= "</table>\n";

      // display_stats
      if ($given_display_stats_toggle) {
        $markup .= $this->get_display_stats($responsibility_days_count_count, $num, $sort_green_count, $sort_red_count);
      }

    } else if ($style_flag == "hint_flag") {
      $markup .= "<p>This business_plan_text has the following proceses" . $plural_string . ":</p>\n";

      // simple list
      $markup .= "<table class=\"plants\">\n";

      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"width: 380px;\">\n";
      $markup .= "    process name\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // rows
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $process) {
        $markup .= "<tr>\n";

        // num
        $num++;
        $markup .= "  <td>\n";
        $markup .= "    " . $num . "\n";
        $markup .= "  </td>\n";

        // id
        $markup .= "  <td>\n";
        $markup .= "    " . $process->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        // processes.name
        $name = $process->get_name();
        if (strpos($name,'develop') !== false) {
          $markup .= "  <td style=\"background-color: #CCFF88;\">\n";
        } else if (strpos($name,'chore') !== false) {
          $markup .= "  <td style=\"background-color: #A4D3EE;\">\n";
        } else {
          $markup .= "  <td>\n";
        }
        $process->set_given_process_id($this->get_given_process_id());
        $markup .= "    <strong>" . $process->get_name_with_link() . "</strong>\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";

       }

      $markup .= "</table>\n";

    } else {
      $markup .= "<p style=\"error\">Error processes: style_flag is not known..</p>\n";
      return $markup;
    }

    return $markup;
  }

  // method
  function get_processes_table_list_given_bpt_id($given_user_obj, $given_bpt_id, $given_display_stats_toggle = "") {
      $markup = "";

      // load data from database
      $this->set_given_business_plan_text_id($given_bpt_id);
      $this->set_user_obj($given_user_obj);
      $this->set_given_display_stats_toggle($given_display_stats_toggle);

      $this->determine_type();

      $markup = $this->prepare_query();
      // check for error message
      if ($markup) {
        print "error process: " . $markup;
      }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    image\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 380px;\">\n";
    $markup .= "    process name\n";
    $markup .= "  </td>\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    polymorphic count\n";
    //$url = $this->url("scene_elements") . "?view=by-parent-class-name";
    //$markup .= "    <a href=\"" . $url . "\">scene elements count</a>\n";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $responsibility_days_count_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $process) {
      $markup .= "<tr>\n";

      // todo potentially need to change all standard classes ...
      // todo so that found instances know same databasedashboard
      // provide the instance the databasedashboard
      //$process->set_db_dash($this->get_db_dash());

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $process->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img
      $markup .= "  <td style=\"background-color: #0099CC;\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $process->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // processes.name
      $name = $process->get_name();
      if (strpos($name,'develop') !== false) {
        $markup .= "  <td style=\"background-color: #CCFF88;\">\n";
      } else if (strpos($name,'chore') !== false) {
        $markup .= "  <td style=\"background-color: #A4D3EE;\">\n";
      } else {
        $markup .= "  <td>\n";
      }
      $process->set_given_process_id($this->get_given_process_id());
      $markup .= "    <strong>" . $process->get_name_with_link() . "</strong>\n";
      $markup .= "  </td>\n";

      // scene_element count (s.e. count)
      //if (strpos($process->get_scene_element_count(), ">0<") === FALSE) {
      //  $markup .= "  <td style=\"text-align: center;\">\n";
      //} else {
      //  $markup .= "  <td style=\"text-align: center; background-color: #CD5555;\">\n";
      //}
      //$markup .= $process->get_scene_element_count() . "\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

      return $markup;
  }

  // method
  public function get_responsibility_test_results($given_scene_element_obj, $given_keyword) {
    $markup = "";

    // this field contains a keyword that specifies time period
    // does a string contain a string?

    if ($given_keyword) {
      // (1) check for month assuming last_timecard_date
      $pos = strpos($this->get_responsibility(), $given_keyword);
      if ($pos === false) {
        $markup .= "    <p class=\"error\">processes: <em>FAIL (given keyword is not found in day count string)</em> given_keyword = " . $given_keyword . "</p>\n";
      } else {

        // debug
        if (! $given_scene_element_obj) {
          // debug
          print "debug scene_elements given_scene_element_obj is not known<br />\n";
        } else {
          // debug
          //print "debug processes scene_element class_name_string = " . $given_scene_element_obj->get_class_name_string();;
        }

        if ($given_scene_element_obj->get_last_timecard_date($given_scene_element_obj, $given_keyword)) {
          $markup .= $given_scene_element_obj->get_last_timecard_date($given_scene_element_obj, $given_keyword);
        } else {
          $markup .= "    <p class=\"error\">processes: <em>FAIL (last_timecard_date is false)</em> given_keyword = " . $given_keyword . "</p>\n";
        }
      }
    } else {
      // debug
      //print "debug processes given_keyword is not known<br />\n";
      $markup .= "    <p class=\"error\">processes: <em>FAIL (given_keyword parameter is not known)</p>\n";
    }

    return $markup;
  }

  // method
  public function get_name_with_link_given_id($given_id = "", $given_user_obj) {
    $markup = "";

    if (! $given_id) {
      // do not know what to get, so return a null string
      return "";
    }

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug shifts given_id = " . $this->get_given_id() . "<br />\n";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_name_with_link();
    }

    return $markup;
  }

  // method
  public function get_scene_element_count() {
    $markup = "";

    include_once("scene_elements.php");
    $scene_elements_obj = new SceneElements($this->get_given_config());
    $process_id = $this->get_id();
    $user_obj = $this->get_user_obj();
    $url = $this->url("scene_elements/processes/" . $process_id);
    $markup .= "<a href=\"" . $url . "\">" . $scene_elements_obj->get_count_given_process_id($process_id, $user_obj) . "</a>";

    return $markup;
  }

  // method
  public function get_scene_element_classes_id($link_flag = "") {
    $markup = "";

    include_once("scene_elements.php");
    $scene_elements_obj = new SceneElements($this->get_given_config());
    $process_id = $this->get_id();
    $user_obj = $this->get_user_obj();
    $markup .= $scene_elements_obj->get_classes_id_given_process_id($process_id, $user_obj, $link_flag);

    return $markup;
  }

  // method
  public function get_scene_element_classes_name($link_flag = "") {
    $markup = "";

    include_once("scene_elements.php");
    $scene_elements_obj = new SceneElements($this->get_given_config());
    $process_id = $this->get_id();
    $user_obj = $this->get_user_obj();

    // debug
    //print "debug processes scene_elements_obj class = " . get_class($scene_elements_obj) . "<br />\n";
    //print "debug processes process id = " . $process_id . "<br />\n";
    //print "debug processes user_obj class = " . get_class($user_obj) . "<br />\n";
    //print "debug processes link_flag = " . $link_flag . "<br />\n";
    
    $markup = $scene_elements_obj->get_classes_name_given_process_id($process_id, $user_obj, $link_flag);

    return $markup;
  }

  // special
  public function get_project_id($given_user_obj) {
    $markup = "";

    $this->set_given_id($this->get_id());
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_business_plan_text_obj()->get_project_obj()->get_id();

    }

    return $markup;
  }

  // special
  public function get_project_id_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_business_plan_text_obj()->get_project_obj()->get_id();

    }

    return $markup;
  }

  // method
  public function get_build_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    // debug
    //print "debug processes type " . $this->get_type() . "\n";

    $markup .= $this->prepare_query();

    // todo remove the following kludge and learn how to write SQL
    //$this->remove_duplicates();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no process were found.</p>\n";;
      return $markup;
    }

    $markup .= "<h3 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px;\">All processes of this project</h3>\n";

    // rows
    $num = 0;
    $total_count = $this->get_list_bliss()->get_count();
    foreach ($this->get_list_bliss()->get_list() as $process) {
      $num++;

      // add space for breathing room
      $markup .= "<br />\n";

      // num
      $markup .= "  <p>\n";
      $markup .= "    #" . $num . " of " . $total_count . " processes<br />\n";
      $markup .= "  </p>\n";

      // img
      $padding = "";
      $float = "left";
      $width = "65";
      $markup .= "    " . $process->get_img_as_img_element_with_link($padding, $float, $width);

      // id
      $markup .= "  <p><em>This is process id " . $process->get_id() . ".</em></p>\n";

      // name
      $process->set_given_project_id($this->get_given_project_id());
      $markup .= "    <h1>" . $process->get_name() . "</h1>\n";

      // description
      $markup .= "  <p>\n";
      $markup .= "    " . $process->get_description() . "\n";
      //$markup .= "    <div style=\"float: right;\"><em>sort = " . $process->get_sort() . "</em></div>\n";
      $markup .= "  </p>\n";

      // scene_elements
      $markup .= "<br />\n";
      $markup .= "  <h3>scene_elements of this process";
      $markup .= " (<em>id " . $process->get_id() . "</em>)";
      $markup .= "</h3>\n";
      $markup .= "  <p>\n";
      include_once("scene_elements.php");
      $scene_elements_obj = new SceneElements($this->get_given_config());
      $process_id = $process->get_id();
      $markup .= "    " . $scene_elements_obj->get_table_given_process_id($process_id, $given_user_obj) . "\n";
      $markup .= "  </p>\n";
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  private function is_online_only() {
    $online_only = 0;
    // get parameter
    if (isset($_GET["status"])) {
      if ($_GET["status"] == "online") {
        $online_only = 1; 
      }
    }
    return $online_only;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "processes";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $processes_obj = new Processes($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $processes_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug processes target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
          // 3
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // method
  public function get_name_with_link_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $obj) {
        $markup .= $obj->get_name_with_link() . "\n";
      }
    } else {
      $markup .= "No processes found.<br />\n";
    }

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    $markup .= "<p>view: ";
    $views = array("quick", "by-project", "online", "all", "offline");
    $last_pos = count($views) - 1;
    foreach ($views as $view) {
      $markup .= $this->get_menu_item($view, "view");
      if ($view != $views[$last_pos]) {
        $markup .= " | ";
      }
    }
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  private function output_sort() {
    $markup = "";

    if ($this->get_given_sort() == "priority") {
      $markup .= "<p>sort: ";
      $sorts = array("default", "priority");
      $last_pos = count($sorts) - 1;
      foreach ($sorts as $sort) {
        $markup .= $this->get_menu_item($sort, "sort");
        if ($sort != $sorts[$last_pos]) {
          $markup .= " | ";
        }
      }
      $markup .= "</p>\n";
    }

    return $markup;
  }

  // method
  private function get_domain_tli_list($given_process_id) {
    $markup = "";

    // search scene_elements and find all class domains with this process_id

    include_once("scene_elements.php");
    $scene_elements_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup = $scene_elements_obj->get_domain_tli_list_given_process_id($given_process_id, $user_obj);

    return $markup;
  }

  // method
  private function get_stocks() {
    $markup = "";

    // todo this whole function needs to be checked
    // todo because the expectation is that they row counts
    // todo are accurate while the select may be taking subsets
    // todo examine this (make different "get_all" levels) uniform
    // todo across all classes

    // todo build the following so that it reports on the stocks
    // todo the stocks is the inventory, what is on balance sheet
    // todo in this set is fixed and temporary

    // try this
    // if this process has a scene_element that is a "classes"
    //$se_classes_id = $this->get_scene_element_classes_id();
    $se_classes_name = $this->get_scene_element_classes_name("with-link");

    // count how many rows are in this table
    include_once("classes.php");
    $classes_obj = new Classes($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $row_count_string = $classes_obj->get_table_count_given_classes_name($se_classes_name, $user_obj);

    // add label the above number
    $row_count_string .= $se_classes_name;
    $url = $this->url(trim($se_classes_name));
    $markup .= "<a href=\"" . $url . "\">" . $row_count_string . "</a>";

    return $markup;
  }

  // method
  private function get_time_cell() {
    $markup = "";

    $background_color = "";
    if ($this->get_time_rules()) {
      $background_color == "#A6D785";
    } else {
      $background_color = "#CCCCCC";
    }
    $markup .= "  <td style=\"background-color: " . $background_color . ";\">\n";
    $delimeter = " ";
    // not that explode reminds me of split function
    $time_rules_word_array = explode($delimeter, $this->get_time_rules());
    $count = count($time_rules_word_array);
    // this maintains some consistency to start things out
    if ($count == 1 || $count == 3) {

      // get ready
      include_once("lib/dates.php");
      $date_obj = new Dates();

      // set up variables
      $unknown_words_array = array();
      $num = 0;

      // loop
      foreach ($time_rules_word_array as $word) {
        $num++;
        $markup .= $num . "&nbsp;";
        if ($word == "daily") {
          $sort = $this->get_sort_no_z();
          // debug
          //print "debug processes sort = " . $sort . "<br />\n";
          if ($date_obj->is_today($sort)) {
           $markup .= "<span style=\"font-family: monospace; background-color: green; color: #FCD115; padding: 2px 4px 1px 4px;\">" . $word . "</span><br />\n";
          } else {
           $markup .= "<span style=\"font-family: monospace; background-color: #CCCCCC; color: #000000; padding: 2px 4px 1px 4px;\">" . $word . "</span><br />\n";
          }
        } else if ($word == "weekly") {
          $sort = $this->get_sort_no_z();
          if ($date_obj->get_days_elapsed($sort) <= 7) {
           $markup .= "<span style=\"font-family: monospace; background-color: green; color: #FCD115; padding: 2px 4px 1px 4px;\">" . $word . "</span><br />\n";
          } else {
           $markup .= "<span style=\"font-family: monospace; background-color: #CCCCCC; color: #000000; padding: 2px 4px 1px 4px;\">" . $word . "</span><br />\n";
          }
        } else if ($word == "xmonthly") {
          $sort = $this->get_sort_no_z();
          if ($date_obj->get_days_elapsed($sort) <= 28) {
           $markup .= "<span style=\"font-family: monospace; background-color: green; color: #FCD115; padding: 2px 4px 1px 4px;\">" . $word . "</span><br />\n";
          } else {
           $markup .= "<span style=\"font-family: monospace; background-color: #CCCCCC; color: #000000; padding: 2px 4px 1px 4px;\">" . $word . "</span><br />\n";
          }
        } else {
          array_push($unknown_words_array, $word);
          $markup .= "<br />\n";
          //$markup .= "  " . $word . "<br />\n";
        }
      }
      // output unknown
      // debug below is minimal data output
      //$markup .= "  <strong>unknown:</strong> \n";
      //foreach ($unknown_words_array as $word) {
        // more information
        //$markup .= $word . " \n";
        // minimal informatino
        //$markup .= "*";
      //}
    } else {
      $markup .= "  " . $this->get_time_rules() . "\n";
      //$markup .= "  <strong>count = " . $count . "</strong>\n";
    }
    $markup .= "  </td>\n";

    return $markup;
  }

  // method
  private function get_shift_cell() {
    $markup = "";

    include_once("shifts.php");
    $shift_obj = new Shifts($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $shift_data = $shift_obj->get_shift_given_process_id($this->get_id(), $user_obj);
    $background_color = "";
    if ($shift_data) {
      $background_color == "#A6D785";
    } else {
      $background_color = "#CCCCCC";
    }
    $markup .= "  <td style=\"background-color: " . $background_color . ";\">\n";
    $markup .= "  " . $shift_data;
    $markup .= "  </td>\n";

    return $markup;
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT processes.id, processes.name FROM processes WHERE processes.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug processes: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "Processes ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug processes: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error processes: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $process) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $process->get_id() . " " . $process->get_name_with_link() . " documentation " . $process->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $process->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error processes: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "processes: no processes found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

// todo consider bug below
// blaireric@basecamp:/media/toshiba/sifted/larsland/mudia/t/repository/freeradiantbunny/frb-clone3/freeradiantbunny/frb$ diff processes.php processes_bug.php// 19c19
// < //inclde_once("lib/standard.php");
// ---
// > inclde_once("lib/standard.php");
// 21c21
// < class Processes extends standard {
// ---
// > class Processes extends Standard {
// 185c185
// <       $order_by = "processes.sort DESC, processes.responsibility, processes.priority, processes.name";
// ---
// >       $order_by = "processes.sort DESC, processes.name";

}
