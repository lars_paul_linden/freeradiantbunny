<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/land_beds.php

include_once("lib/scrubber.php");

class LandBeds extends Scrubber {

  // attribute
  private $id;
  private $soil_area_obj;
  private $bed_num;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // soil_area_obj
  public function get_soil_area_obj() {
    if (! isset($this->soil_area_obj)) {
      include_once("soil_areas.php");
      $this->soil_area_obj = new SoilAreas($this->get_given_config());
    }
    return $this->soil_area_obj;
  }

  // bed_num
  public function set_bed_num($var) {
    $this->bed_num = $var;
  }
  public function get_bed_num() {
    return $this->bed_num;
  }

<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/visits.php

include_once("lib/scrubber.php");

class Visits extends Scrubber {

  // given 
  private $given_project_id;
  private $given_plant_list_id;
  private $given_plant_history_event_id;
  private $given_soil_area_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_plant_history_event_id;
  public function set_given_plant_history_event_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "plant_history_event_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_history_event_id = $var;
    }
  }
  public function get_given_plant_history_event_id() {
    return $this->given_plant_history_event_id;
  }

  // given_soil_area_id
  public function set_given_soil_area_id($var) {
    $this->given_soil_area_id = $var;
  }
  public function get_given_soil_area_id() {
    return $this->given_soil_area_id;
  }

  // attributes
  private $id;
  private $rough_date;
  private $fact;
  private $land_obj;
  private $name;
  private $plant_history_event_obj;
  private $order;
  private $count;
  private $layout_obj;
 
  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // rough_date
  public function set_rough_date($var) {
    $this->rough_date = $var;
  }
  public function get_rough_date() {
    return $this->rough_date;
  }

  // fact
  public function set_fact($var) {
    $this->fact = $var;
  }
  public function get_fact() {
    return $this->fact;
  }

  public function get_fact_user_facing() {
    if ($this->fact == "t") {
      return "fact";
    }
    return "plan";
  }

  // todo land_obj was soil_area_obj

  // land_obj
  public function get_land_obj() {
    if (! isset($this->land_obj)) {
      include_once("lands.php");
      $this->land_obj = new Lands($this->get_given_config());
    }
    return $this->land_obj;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // order
  public function set_order($var) {
    $this->order = $var;
  }
  public function get_order() {
    return $this->order;
  }

  // count
  public function set_count($var) {
    $this->count = $var;
  }
  public function get_count() {
    return $this->count;
  }

  // plant_history_event_obj
  public function get_plant_history_event_obj() {
    if (! isset($this->plant_history_event_obj)) {
      include_once("plant_history_events.php");
      $this->plant_history_event_obj = new PlantHistoryEvents($this->get_given_config());
    }
    return $this->plant_history_event_obj;
  }

 
  // layout_obj
  public function get_layout_obj() {
    if (! isset($this->layout_obj)) {
      include_once("layouts.php");
      $this->layout_obj = new Layouts($this->get_given_config());
    }
    return $this->layout_obj;
  }

  // method
  private function make_visit() {
    $obj = new Visits($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // plant_history_id
    $parameter_a = new Parameter();
    $parameter_a->set_name("plant_history_id");
    $parameter_a->set_validator_function_string("id");
    array_push($parameters, $parameter_a);

    // get parameters (if any) and valirough_date
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "plant_history_id") {
            $this->set_given_plant_history_event_id($parameter->get_value());
          }
        }
      }
    }

    return $markup;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT land_beds.* FROM land_beds ORDER BY land_beds.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_land_bed();
        $obj->set_id(pg_result($results, $lt, 0));

      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  private function make_land_bed() {
    $obj = new LandBeds($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // view
    //$markup .= $this->output_view();
    //$markup .= $this->output_table();
    $markup .= "debug land_beds output_aggregate()<br />\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

}
