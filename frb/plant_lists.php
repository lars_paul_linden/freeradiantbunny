<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.5 2015-10-16
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_lists.php

include_once("lib/socation.php");

class PlantLists extends Socation {

  // given
  private $given_process_id;
  private $given_plant_id;
  private $given_plant_history_id;

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_plant_id
  public function set_given_plant_id($var) {
    $this->given_plant_id = $var;
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // given_plant_history_id
  public function set_given_plant_history_id($var) {
    $this->given_plant_history_id = $var;
  }
  public function get_given_plant_history_id() {
    return $this->given_plant_history_id;
  }

  // todo validate given id
  // $error_message = $this->get_validation_obj()->validate_id($var, "plant_id");

  // derived
  private $derived_count_of_plants_on_list;

  // derived_count_of_plants_on_list
  public function set_derived_count_of_plants_on_list($var) {
    $this->derived_count_of_plants_on_list = $var;
  }
  public function get_derived_count_of_plants_on_list() {
    return $this->derived_count_of_plants_on_list;
  }

  // method
  private function make_plant_list() {
    $obj = new PlantLists($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function process_command() {
    $markup = "";

    // todo add REST POST here
    // this allows the user to change (edit) the data into the database
    if ($this->get_user_obj() && $this->get_user_obj()->uid) {
      if (isset($_GET['command'])) {
        if ($_GET['command'] == "create_a_new_list") {
          // private
          include_once("plant_lists.php");
          $plant_lists_obj = new PlantLists($this->get_given_config());
          $plant_lists_obj->set_user_obj($this->get_user_obj());
          $markup .= $this->process_create_a_new_list();
        }
      }
    }

    // check for command
    // this is a user edit system
    // this allows the user to input data into the database
    // this allows the user to change (edit) the data into the database
    if ($this->get_user_obj()) {
      if (isset($_GET['command'])) {
        if ($_GET['command'] == "add_a_plant_to_this_list") {
          $this->set_command($_GET['command']);
          // private
          $markup .= $this->output_given_variables();
          $markup .= $this->process_add_a_plant_to_this_list();
        }
      }
    }

    return $markup;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_plant_id()) {
      $this->set_type("get_by_plant_id");

    } else if ($this->get_given_plant_history_id()) {
      $this->set_type("get_by_plant_history_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT plant_lists.* FROM plant_lists WHERE plant_lists.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT plant_lists.* FROM plant_lists ORDER BY plant_lists.status, plant_lists.sort DESC, plant_lists.name;";

    } else if ($this->get_type() == "get_by_project_id") {
      // todo careful because this is a useful sqsl statemetn; this gets the plant_lists that are scene_elements;
      $sql = "SELECT DISTINCT ON (plant_lists.id) plant_lists.*, projects.id, projects.name, projects.img_url FROM plant_lists, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'plant_lists' AND plant_lists.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY plant_lists.id;";

    } else if ($this->get_type() == "get_by_process_id") {
      $sql = "SELECT DISTINCT ON (plant_lists.id) plant_lists.* FROM plant_lists, scene_elements WHERE scene_elements.class_name_string = 'plant_lists' AND plant_lists.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = " . $this->get_given_process_id() . " ORDER BY plant_lists.id;";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT plant_lists.* FROM plant_lists, plant_list_plants WHERE plant_lists.id = plant_list_plants.plant_list_id AND plant_list_plants.plant_id = " . $this->get_given_plant_id() . " ORDER BY plant_lists.id;";

      // debug
      //$markup .= "debug plant_lists sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_plant_history_id") {
      $sql = "SELECT plant_lists.* FROM plant_lists, plant_list_plants, plant_histories WHERE plant_lists.id = plant_list_plants.plant_list_id AND plant_histories.id = " . $this->get_given_plant_history_id() . " AND plant_histories.plant_list_plant_id = plant_list_plants.id ORDER BY plant_lists.id;";

      // debug
      //$markup .= "debug plant_lists sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_by_plant_id" ||
        $this->get_type() == "get_by_plant_history_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_list();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_description(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        if ($this->get_type() == "get_by_project_id") {
          $obj->get_project_obj()->set_id(pg_result($results, $lt, 5));
          $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 7));
        } else {
          $obj->set_img_url(pg_result($results, $lt, 5));
        }
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // make-sort-today
    $parameter_a = new Parameter();
    $parameter_a->set_name("make-sort-today");
    $parameter_a->set_validation_type_as_id();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "plant_lists";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $plant_list_obj = new PlantLists($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $plant_list_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    // todo deal with abstract nature of this subsubmenu
    if ($this->get_given_id() ||
        $this->get_given_project_id()) {

      $flag = 0;

      if ($flag) {
        $markup .= "<div class=\"subsubmenu\">\n";

        // widen view

        $markup .= "<strong>See Also</strong>: \n";

        // all plant lists
        $url = $this->url("plant_lists");
        $markup .= "<a href=\"" . $url . "\">All&nbsp;Plant&nbsp;Lists</a> \n";
        $markup .= " | ";

        // plant_histories
        $url = $this->url("plant_histories/plant_list/" . $this->get_given_id());
        $markup .= "<a href=\"" . $url . "\">Plant Histories of this Plant List</a> \n";
        $markup .= " | ";

        // todo ask if there is a standard place for "add" buttons
        // todo make this in RESTful form
        // todo make this a button image
        $url = $this->url("plants");
        $markup .= "<a href=\"" . $url . "?command=create_a_new_list\">Create a new list</a> \n";

        // add a plant_list
        //$url = $this->url("plant_lists/" . $this->get_given_id());
        //$markup .= "<a href=\"" . $url . "?command=add_a_plant_to_this_list\
">Add a plant to this list</a>.\n";
        $markup .= " | ";

        // about
        $url = $this->url("plant_lists/" . $this->get_given_id() . "?view=about");
        $markup .= "<a href=\"" . $url . "\">About details</a> \n";

        //if ($this->get_given_id() || 
        //    $this->get_given_project_id()) {
        //  $url = $this->url("crop_plans/plant_list/" . $this->get_given_id());
        //  $markup .= "<p>See also: <a href=\"" . $url . "\">Crop Plans</a> (given plant_list)</p>\n";
        //  $url = $this->url("crop_plans/project/" . $this->get_given_project_id());
        //  $markup .= "<p>See also: <a href=\"" . $url . "\">Crop Plans</a> (given project)</p>\n";
        //}
        //if ($this->get_given_id()) {
        //  $url = $this->url("layouts/plant_list/" . $this->get_given_id());
        //  $markup .= "<p>See also: <a href=\"" . $url . "\">Layouts</a></p>\n";
        //} else if ($this->get_given_project_id()) {
        //  $url = $this->url("layouts/project/" . $this->get_given_project_id());
        //  $markup .= "<p>See also: <a href=\"" . $url . "\">Layouts</a></p>\n";
        //}

        $markup .= "</div>\n";
      }
    }

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // todo deal with abstract nature of this user_info

    // output if showing 1
    if ($this->get_given_id()) {
      include_once("plant_lists.php");
      $plant_lists_obj = new PlantLists($this->get_given_config());
      $markup .= $plant_lists_obj->output_submenu_user_plant_lists($this->get_given_id(), $this->get_user_obj());
    }

    return $markup;
  }

  // method
  public function output_given_variables() {
    $markup = "";

    if ($this->get_given_project_id()) {
      // todo figure out this function below
      //$markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  public function output_submenu_user_plant_lists($id_of_current = "", $given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // only authenticated users
    // todo wow check out the following and see if this should be standardized
    // todo fix security
    //if (user_is_logged_in()) {

      $url = $this->url("user");
      if (0) {
        $markup .= "<strong>Plant Lists by <a href=\"\" class=\"noshow\">" . $this->get_user_obj()->name . "</a></strong><br />\n";
        $markup .= "<br />\n";
      }

      // instead of whole get_markup()

      $this->determine_type();
      $markup .= $this->prepare_query();

      if ($this->get_list_bliss()->get_count() > 0) {
        if (0) {
          $markup .= "<ul style=\"margin: 0px 0px 0px 12px; padding: 0px 4px 0px 0px;\">\n";
          foreach ($this->get_list_bliss()->get_list() as $plant_list) {
            $markup .= "<li style=\"margin: 0px 0px 0px 4px; padding: 0px 4px 2px 0px;\">\n";
            if ($id_of_current == $plant_list->get_id()) {
              // no hyperlink
            } else {
              $url = $this->url("plant_lists/" . $plant_list->get_id());
              $markup .= "  <a href=\"" . $url . "\">";
            }
            $markup .= $plant_list->get_name();
            if ($id_of_current == $plant_list->get_id()) {
              // no hyperlink
            } else {
              $markup .= "</a>";
            }
            $markup .= "</li>\n";
          }
          $markup .= "<ul>\n";
          $markup .= "<br />\n";
        }
      }
    //}

    return $markup;
  }

  // method
  public function output_plant_list_name_given_id($given_id) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_type("get_plant_list_given_id");

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are 1 item to output
    if ($this->get_list_bliss()->get_count() == 1) {
      $plant_lists_list = $this->get_list_bliss()->get_list();
      $plant_list_obj = $plant_lists_list[0];
      $plant_list_name = $plant_list_obj->get_name();
      if ($plant_list_name) {
        $markup .= "The plant list name is <strong>" . $plant_list_name . "</strong>\n";
      }
    }
    // check
    if (! $markup) {
      // return error message
      $markup .= "The plant list id is <strong>" . $this->get_given_plant_list_id() . "</strong>\n";
    }

    return $markup;
  }

  // method
  public function output_plant_lists_given_plant_id($given_plant_id) {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);
    $this->set_type("get_by_plant_id");

    // debug
    //$markup .= "debug plant_lists given_plant_id = " . $this->get_given_plant_id() . "<br />\n";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are 1 item to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= "<h3>Plant Lists</h3>\n";
      $markup .= "<ol>\n";
      foreach ($this->get_list_bliss()->get_list() as $plant_list) {
          $markup .= "<li>" . $plant_list->get_name_with_link() . "</li>\n";
      }
      $markup .= "</ol>\n";
    }
    // todo perhaps build a debug flag system here
    if (0) {
      // check
      if (! $markup) {
        // return error message
        $markup .= "The plant list id is <strong>" . $this->get_given_plant_list_id() . "</strong>\n";
      }
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    projects<br />\n";
    $markup .= "    se found\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    count of plants\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    count of plant_histories\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    count of seed_packets\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    count of plant_history_events\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    count of visits\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // aggregate
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_list) {
      $num++;
      $markup .= "<tr>\n";

      // num
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // se found
      $markup .= "  <td>\n";
      include_once("scene_elements.php");
      $se_obj = new SceneElements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $se_obj->get_se_list_given_plant_list_id($plant_list->get_id(), $user_obj);
      $markup .= "  </td>\n";

      // status
      if ($plant_list->get_status() == "in progress") {
        $markup .= "  <td style=\"background-color: lightgreen\">\n"; 
      } else {
        $markup .= "  <td style=\"background-color: red\">\n"; 
      }
      $markup .= $plant_list->get_status();
      $markup .= "  </td>\n";

      // sort
      $markup .= $plant_list->get_sort_cell();

      // id
      $markup .= "  <td>\n"; 
      $markup .= $plant_list->get_id();
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "<h3 style=\"padding: 4px 20px 0px 10px; margin: 0px 0px 0px 0px;\">";
      $url = $this->url("plant_lists/" . $plant_list->get_id());
      $markup .= "<a href=\"" . $url . "\" style=\"text-decoration: none;\">";
      $markup .= $plant_list->get_name();
      $markup .= "</a>";
      $markup .= "</h3>";
      $markup .= "  </td>\n";

      // count of plants via plant_list_plants
      $markup .= "  <td align=\"center\" style=\"padding: 4px 2px 2px 2px;\">\n";
      $url = $this->url("plant_list_plants/plant_lists/" . $plant_list->get_id());
      $markup .= "  <a href=\"" . $url . "\">";
      include_once("plant_list_plants.php");
      $plant_list_plants_obj = new PlantListPlants($this->get_given_config());
      $plant_list_plants_obj->set_user_obj($this->get_user_obj());
      $user_obj = $this->get_user_obj();
      $markup .= $plant_list_plants_obj->output_plant_count_given_plant_list_id($plant_list->get_id(), $user_obj);
      $markup .= "</a>";
      $markup .= "  </td>\n";

      // plant_histories
      $markup .= "  <td align=\"center\" style=\"padding: 4px 2px 2px 2px;\">\n";
      $url = $this->url("plant_histories/plant_lists/" . $plant_list->get_id());
      $markup .= "  <a href=\"" . $url . "\">";
      include_once("plant_histories.php");
      $plant_history_obj = new PlantHistories($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $plant_history_obj->output_plant_history_count_given_plant_list_id($plant_list->get_id(), $user_obj);
      $markup .= "</a>";
      $markup .= "  </td>\n";

      // seed_packets
      $markup .= "  <td align=\"center\" style=\"padding: 4px 2px 2px 2px;\">\n";
      $url = $this->url("seed_packets/plant_lists/" . $plant_list->get_id());
      $markup .= "  <a href=\"" . $url . "\">";
      include_once("seed_packets.php");
      $seed_packet_obj = new SeedPackets($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $seed_packet_obj->output_seed_packet_count_given_plant_list_id($plant_list->get_id(), $user_obj);
      $markup .= "</a>";
      $markup .= "  </td>\n";

      // plant_history_events
      $markup .= "  <td align=\"center\" style=\"padding: 4px 2px 2px 2px;\">\n";
      $url = $this->url("plant_history_events/plant_lists/" . $plant_list->get_id());
      $markup .= "  <a href=\"" . $url . "\">";
      include_once("plant_history_events.php");
      $plant_history_event_obj = new PlantHistoryEvents($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $plant_history_event_obj->output_plant_history_event_count_given_plant_list_id($plant_list->get_id(), $user_obj);
      $markup .= "</a>";
      $markup .= "  </td>\n";

      // visits
      $markup .= "  <td align=\"center\" style=\"padding: 4px 2px 2px 2px;\">\n";
      $url = $this->url("visits/plant_lists/" . $plant_list->get_id());
      $markup .= "  <a href=\"" . $url . "\">";
      include_once("visits.php");
      $visit_obj = new Visits($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $visit_obj->output_visit_count_given_plant_list_id($plant_list->get_id(), $user_obj);
      $markup .= "</a>";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    // about
    if ($this->get_given_view() == "about") {
      // debug
      $markup .= "<p>view = " . $this->get_given_view() . "</p>\n";
      // output the about information of the plant_lists
      return $this->output_about();
    }

    $markup .= "<p>This <em>plant_list</em> has the following plants:</p>\n";

    // output the plant_list_plants for this plant list
    include_once("plant_list_plants.php");
    $plant_list_plants_obj = new PlantListPlants($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $plant_list_plants_obj->output_table_given_plant_list_id($this->get_id(), $user_obj);
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  private function offline_output_list() {
    $markup = "";

    // todo not sure if this is used
    // todo so, might be able to delete this function
    // aggregate
    foreach ($this->get_list_bliss()->get_list() as $plant_list) {
      $url = $this->url("plant_lists/" . $plant_list->get_id());
      $markup .= "  <a href=\"" . $url . "\">";
      $markup .= $plant_list->get_name();
      $markup .= "</a>";
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  public function output_plant_lists_table($given_type_url, $given_type_string, $given_list_obj) {
    $markup = "";

    $markup .= "<h3>Plant Lists of this Project</h3>\n";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($given_list_obj as $plant_list) {
      $num++;
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_list->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url($given_type_url . "/plant_list/" . $plant_list->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $given_type_string . " of " . $plant_list->get_name() . "</a>\n";
      $markup .= "</a>";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_count_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug plant_lists: ignore, not related<br />\n";
    } else if (get_class($given_pm_obj) == "Machines") {
      $markup .= "debug plant_lists: ignore, not related<br />\n";
    } else if (get_class($given_pm_obj) == "Webpages") {
      $markup .= "debug plant_lists: ignore, not related<br />\n";
    } else {
      $markup .= "debug plant_lists: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function play($given_process_id, $given_user_obj, $given_heading) {
    $markup = "";

    $markup .= "<div style=\"padding: 2px 2px 1px 2px; background-color: #EFEFEF; text-align: center; border-style: dashed; border-width: 1px; border-color: blue; display: inline-block;\">\n";
    $padding = " 0px 0px 0px 0px";
    $float = "";

    // todo move this width and heigth to a better place
    $width = "65";
    $height = "80";
    $markup .= $this->get_img_as_img_element_with_link($padding, $float, $width, $height) . "<br/>\n";
    $markup .= "<span style=\"font-size: 80%; color: blue;\">" . $this->get_name() . "</span><br />\n";
    $markup .= "<span style=\"font-size: 80%; padding: 2px 2px 1px 2px;background-color: #CCCCCC; color: blue;\"><em>" . get_class($this) . "</em></span><br />\n";
    $markup .= "(a) show where the plant_lists is.<br />\n";
    $markup .= "(b) show where the plants on the plant_lists are.<br />\n";
    $markup .= "</div>\n";

    return $markup;
  }

  // method
  public function get_plant_list_id_given_plant_history_id($given_plant_history_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_history_id($given_plant_history_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $plant_list) {
      $markup .= $plant_list->get_id_with_link() . "<br />\n";
    }

    return $markup;;
  }
  // method
  public function get_plant_list_name_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // refresh
    $this->get_list_bliss()->empty_list();

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $plant_list) {
      $markup .= $plant_list->get_name_with_link() . "<br />\n";
    }

    return $markup;
  }

  // method
  private function output_about() {
    $markup = "";

    $markup .= "<p>About each plant on the <em>plant_list</em>.</p>\n";

    // output the plant_list_plants for this plant list
    include_once("plant_list_plants.php");
    $plant_list_plants_obj = new PlantListPlants($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $plant_list_plants_obj->output_table_about_given_plant_list_id($this->get_id(), $user_obj);
    $markup .= "<br />\n";

    return $markup;
  }

}
