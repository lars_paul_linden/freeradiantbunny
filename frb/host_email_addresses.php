<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.1 2014-08-20
// version 1.2 2015-01-04
// version 1.8 2017-07-10

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/host_email_addresses.php

include_once("lib/sproject.php");

class HostEmailAddresses extends Sproject {

  // attributes
  private $given_domain_tli;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // attribute
  private $domain_obj;
  private $machine_obj;
  private $email_address_obj;

  // domain_obj
  public function get_domain_obj() {
    if (! isset($this->domain_obj)) {
      include_once("domains.php");
      $this->domain_obj = new Domains($this->get_given_config());
    }
    return $this->domain_obj;
  }

  // machine_obj
  public function get_machine_obj() {
    if (! isset($this->machine_obj)) {
      include_once("machines.php");
      $this->machine_obj = new Machines($this->get_given_config());
    }
    return $this->machine_obj;
  }

  // email_address_obj
  public function get_email_address_obj() {
    if (! isset($this->email_address_obj)) {
      include_once("email_addresses.php");
      $this->email_address_obj = new EmailAddresses($this->get_given_config());
    }
    return $this->email_address_obj;
  }

  // method
  private function make_host_email_address() {
    $obj = new HostEmailAddresses($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");
      
    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");
      
    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND goal_statements.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";
      
    } else if ($this->get_type() == "get_all") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else if ($this->get_type() == "get_by_project_id") {
      //$sql = "SELECT goal_statements.*, projects.name, projects.img_url FROM goal_statements, projects WHERE goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "'" . $order_by . ";";

    } else if ($this->get_type() == "get_by_domain_tli") {
      $sql = "SELECT domain_tli, machine_id, email_address_id FROM host_email_addresses WHERE domain_tli = '" . $this->get_given_domain_tli() . "' ORDER BY machine_id, email_address_id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_host_email_address();
        $obj->get_domain_obj()->set_tli(pg_result($results, $lt, 0));
        $obj->get_machine_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_email_address_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

  // method
  public function get_email_addresses_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // todo try something
    // debug technique
    //return $given_domain_tli;

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup = $this->prepare_query();

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $obj) {
        $num++;
        include_once("email_addresses.php");
        $email_address_obj = new EmailAddresses($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $num . " " . $email_address_obj->get_email_address_given_id($obj->get_email_address_obj()->get_id(), $user_obj) . "<br />\n";
    }

    return $markup;
  }

  // method
  public function get_email_addresses_count_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // todo try something
    // debug technique
    //return $given_domain_tli;

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup = $this->prepare_query();

    return $this->get_list_bliss()->get_count();
  }

}
