<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.6 2016-12-03

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/linkmakers.php

include_once("lib/socation.php");

class Linkmakers extends Socation {

  // attribute
  private $url;

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // method
  private function make_linkmaker() {
    $obj = new Linkmakers($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT linkmakers.* FROM linkmakers WHERE linkmakers.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT linkmakers.* FROM linkmakers;";

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM linkmakers;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_linkmaker();
        $obj->set_id(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_linkmaker();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    url\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $linkmaker) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // linkmakers
      $markup .= "  <td>\n";
      $markup .= "    " . $linkmaker->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $linkmaker->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      if ($linkmaker->get_url()) {
        $markup .= "    <a href=\"" . $linkmaker->get_url() . "\">" . $linkmaker->get_url() . "</a>\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $linkmaker) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $linkmaker->get_id_with_link() . "\n";
      $markup .= "</tr>\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $linkmaker->get_name_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // url
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      if ($linkmaker->get_url()) {
        $markup .= "    <a href=\"" . $linkmaker->get_url() . "\">" . $linkmaker->get_url() . "</a>\n";
      }
      $markup .= "</tr>\n";

    }

    $markup .= "</table>\n";

    return $markup;
  }

}
