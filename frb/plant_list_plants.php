<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_list_plants.php

include_once("lib/scrubber.php");

class PlantListPlants extends Scrubber {

  // todo clean up below
  public function get_given_project_id() {
    return;
  }

  // given
  private $given_plant_list_id;
  private $given_plant_id;

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_plant_id
  public function set_given_plant_id($var) {
    $this->given_plant_id = $var;
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // attributes
  private $id;
  private $plant_obj;
  private $plant_list_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // plant_list_obj
  public function get_plant_list_obj() {
    if (! isset($this->plant_list_obj)) {
      include_once("plant_lists.php");
      $this->plant_list_obj = new PlantLists($this->get_given_config());
    }
    return $this->plant_list_obj;
  }

  // method
  private function make_plant_list_plant() {
    $obj = new PlantListPlants($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_list_id()) {
      // subset
      $this->set_type("get_given_plant_list_id");

    } else if ($this->get_given_plant_id()) {
      // subset
      $this->set_type("get_by_plant_id");

    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug plant_list_plants type = " . $this->get_type() . "<br />\n";

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // authenticated users only
    // security allows only authenticated users to load project data
    if (! $this->get_user_obj()) {
      return $markup;
    }
    // todo figure out the security and the following lines
    // now really make sure
    //if (! $this->get_user_obj()->uid) {
    //  return $markup;
    //}

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT plants.id, plants.common_name, plant_list_plants.id, plant_list_plants.plant_list_id FROM plant_lists, plant_list_plants, plants WHERE plant_lists.id = plant_list_plants.plant_list_id AND plant_list_plants.plant_id = plants.id AND plant_list_plants.id = " . $this->get_given_id() . " ORDER BY plant_list_plants.plant_list_id, plants.common_name;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_list_plants.* FROM plant_list_plants ORDER BY plant_list_plants.plant_list_id, plant_list_plants.plant_id;";

    } else if ($this->get_type() == "get_given_plant_list_id") {
      $sql = "SELECT plant_list_plants.id, plants.* FROM plants, plant_list_plants WHERE plant_list_plants.plant_id = plants.id AND plant_list_plants.plant_list_id = " . $this->get_given_plant_list_id() . " ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_plant_id") {
      $sql = "SELECT plant_list_plants.* FROM plant_list_plants WHERE plant_list_plants.plant_id = " . $this->get_given_plant_id() . " ORDER BY plant_list_plants.plant_list_id, plant_list_plants.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // debug
    //print "debug plant_list_plants sql = " . $sql . "<br />\n";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_given_plant_list_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_list_plant();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 2));
        $obj->get_plant_obj()->set_scientific_name(pg_result($results, $lt, 3));
        $obj->get_plant_obj()->get_plant_category_obj()->set_id(pg_result($results, $lt, 4));
        $obj->get_plant_obj()->get_plant_family_obj()->set_id(pg_result($results, $lt, 5));
      }
    } else if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant_list_plants_obj = $this->make_plant_list_plant();
        $plant_list_plants_obj->get_plant_obj()->set_id(pg_result($results, $lt, 0));
        $plant_list_plants_obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 1));
        $plant_list_plants_obj->set_id(pg_result($results, $lt, 2));
        $plant_list_plants_obj->get_plant_list_obj()->set_id(pg_result($results, $lt, 3));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_list_plant();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_list_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 2));
        // debug
        //print "debug plant_list_plants id = " . $obj->get_id() . "<br />\n";
      }
    } else if ($this->get_type() == "get_by_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant_list_plants_obj = $this->make_plant_list_plant();
        $plant_list_plants_obj->set_id(pg_result($results, $lt, 0));
        $plant_list_plants_obj->get_plant_list_obj()->set_id(pg_result($results, $lt, 1));
        $plant_list_plants_obj->get_plant_obj()->set_common_name(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  public function output_user_info() {
    $markup = "";

    // todo the following reveals a uid undefined proprty bug
    //$markup .= $this->output_user_info_message();

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    //$markup .= "    plant list plant id\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    common name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant histories count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    # output for individual items
    $num = 0;
    $plant_histories_tally = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_list_plant) {
      $num++;
      $markup .= "<tr>\n";

      // num
      // plant list plant id
      $markup .= "  <td>\n";
      $url = $this->url("plant_histories/plant_list_plant/" . $plant_list_plant->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $num . "</a>\n";
      $markup .= "  </td>\n";

      // plant id
      $markup .= "  <td align=\"center\">\n";
      $markup .= "    " . $plant_list_plant->get_plant_obj()->get_id() . "\n";
      $markup .= "  </td>\n";

      // plant common_name
      $markup .= "  <td>\n";
      $url = $this->url("plants/" . $plant_list_plant->get_plant_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $plant_list_plant->get_plant_obj()->get_common_name() . "</a><br />\n";
      $markup .= "  </td>\n";

      // histories
      $markup .= "  <td>\n";
      $markup .= "  </td>\n";
      // counts_plant_histories
      //include_once("lib/counter.php");
      //$counter_obj = new Counter();
      //$counter_obj->set_user_obj($this->get_user_obj());
      // todo not sure why there are to different functions here so justify
      //if ($this->get_given_plant_list_id()) {
      //  $counts_plant_histories = $counter_obj->get_plant_histories_count_for_given_plant_list($this->get_given_plant_list_id(), $plant_list_plant->get_plant_obj()->get_id());
      //} else {
      //  $counts_plant_histories = $counter_obj->get_plant_histories_count($plant_list_plant->get_plant_obj()->get_id());
      //}
      //if ($counts_plant_histories == 0) {
      //  // colorize the projects that have not yet been started
      //  $markup .= "  <td style=\"text-align: center; background: #333399; color: #EFEFEF;\">\n";
      //} else {
      //  $markup .= "  <td style=\"text-align: center;\">\n";
      //}
      //if ($counts_plant_histories) {
      //  $url = $this->url("plant_histories/plant_list_plant/" . $plant_list_plant->get_id());
      //  $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">";
      //}
      // todo need to change the name of inlist counting to "tally"
      //$plant_histories_tally += $counts_plant_histories;
      //$markup .= $counts_plant_histories;
      //if ($counts_plant_histories) {
      //  $markup .= "</a>\n";
      //}
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    // scoreboard
    //$markup .= "<tr>\n";
    //$markup .= "  <td colspan=\"2\" class=\"header\">\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "  total\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td align=\"center\" class=\"header\">\n";
    //$markup .= "  " . $plant_histories_tally . "\n";
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // output for individual items
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_list_plant) {
      $num++;

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_list_plant->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    plant list\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_list_plant->get_plant_list_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    plant&nbsp;common&nbsp;name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $url = $this->url("plants/" . $plant_list_plant->get_plant_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $plant_list_plant->get_plant_obj()->get_common_name() . "</a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_table_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    $flag = 0;

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    //$markup .= "    plant_list _plant_id\n";
    $markup .= "  </td>\n";

    if (1) {
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    plant_id\n";
      $markup .= "  </td>\n";
    }

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    common&nbsp;name\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    scientific name\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant family\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\">\n";
    // todo do not know why, just had to rename it, want to be neutral
    //$markup .= "    category\n";
    $markup .= "    general description\n";
    $markup .= "  </td>\n";

    if (1) {
      $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";

      $url = $this->url("plant_lists/" . $this->given_plant_list_id . "?view=about");
      $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">about</a>\n";
      $markup .= "  </td>\n";
    }

    $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";
    $markup .= "    B\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";
    $markup .= "    DP\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";
    $markup .= "    NF\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";
    $markup .= "    DA\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";
    $markup .= "    F\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";
    $markup .= "    NS\n";
    $markup .= "  </td>\n";

    $markup .= "  <td class=\"header\" style=\"width: 30px; text-align: center;\">\n";
    $markup .= "    DT\n";
    $markup .= "  </td>\n";

    // plant_histories
    if (0) {
      $markup .= "  <td class=\"header\" style=\"background-color: #B4EEB4;\">\n";
      // note here is a link to all plant histories of this plant list
      $url = $this->url("plant_histories/plant_list/" . $this->get_given_plant_list_id());
      $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">Plant Histories<br />count</a>\n";
      $markup .= "    [seed_packets]\n";
      $markup .= "  </td>\n";
    }

    // plant_history_events
    if (0) {
      $markup .= "  <td class=\"header\" style=\"background-color: #B4EEB4;\">\n";
      $url = $this->url("plant_history_events/plant_list/" . $this->get_given_plant_list_id());
      $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">Plant History Events<br />count</a>\n";
      $markup .= "  </td>\n";
    }

    $markup .= "</tr>\n";

    # output for individual items
    $num = 0;
    $plant_histories_tally = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_list_plant) {
      $num++;
      $markup .= "<tr>\n";

      // num
      // plant list plant id

      $markup .= "  <td>\n";
      $url = $this->url("plant_list_plants/" . $plant_list_plant->get_id());
      $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">" . $num . "</a>\n";
      $markup .= "  </td>\n";

      if (1) {
        // plant_id
        $markup .= "  <td align=\"center\">\n";
        $markup .= "    " . $plant_list_plant->get_plant_obj()->get_id() . "\n";
        $markup .= "  </td>\n";
      }

      // common_name
      $markup .= "  <td>\n";
      $url = $this->url("plants/" . $plant_list_plant->get_plant_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\" style=\"text-decoration: none;\">" . $plant_list_plant->get_plant_obj()->get_common_name() . "</a>\n";
      $markup .= "  </td>\n";

      // scientific_name
      $markup .= "  <td>\n";
      $url = $this->url("plants/" . $plant_list_plant->get_plant_obj()->get_id());
      $markup .= "    <em><a href=\"" . $url . "\" style=\"text-decoration: none;\">" . $plant_list_plant->get_plant_obj()->get_scientific_name() . "</a></em>\n";
      $markup .= "  </td>\n";

      // plant_family
      $markup .= "  <td>\n";
      $family_id = $plant_list_plant->get_plant_obj()->get_plant_family_obj()->get_id();
      include_once("plant_families.php");
      $plant_family_obj = new PlantFamilies($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $plant_family_obj->get_plant_family_name_given_id($family_id, $user_obj);

      $markup .= "  </td>\n";

      // plant_categories (architecture, from)
      $markup .= "  <td>\n";
      $plant_category_id = $plant_list_plant->get_plant_obj()->get_plant_category_obj()->get_id();
      include_once("plant_categories.php");
      $plant_category_obj = new PlantCategories($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $plant_category_obj->get_plant_category_name_given_id($plant_category_id, $user_obj);
      $markup .= "  </td>\n";

      if (1) {
        // about
        // plant_attributes count
        $markup .= "  <td style=\"text-align: center;\">\n";
        // todo add php file
        include_once("plant_attributes.php");
        $plant_attributes_obj = new PlantAttributes($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $url = $this->url("plant_attributes/plants/" . $plant_list_plant->get_plant_obj()->get_id());
        $family_attribute_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id($plant_list_plant->get_plant_obj()->get_id(), $user_obj);
        if ($family_attribute_count) {
          $markup .= "<a href=\"" . $url . "\">" . $family_attribute_count . "</a>\n";
        } else {
          $markup .= $family_attribute_count . "\n";
        }
        $markup .= "  </td>\n";
      }

      // matrix
      $markup .= "  <td style=\"text-align: center;\">\n";
      include_once("plant_attributes.php");
      $plant_attributes_obj = new PlantAttributes($this->get_given_config());
      $user_obj = $this->get_user_obj();
      // note search for a set (i.e. could be zero to many)
      $attribute_set = "B";
      $family_attribute_set_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id_and_attribute_set($plant_list_plant->get_plant_obj()->get_id(), $attribute_set, $user_obj);
      if ($family_attribute_set_count) {
        $markup .= "&bull;\n";
      } else {
        $markup .= "\n";
      }
      $markup .= "  </td>\n";

      // matrix
      $markup .= "  <td style=\"text-align: center;\">\n";
      $attribute_set = "DP";
      $family_attribute_set_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id_and_attribute_set($plant_list_plant->get_plant_obj()->get_id(), $attribute_set, $user_obj);
      if ($family_attribute_set_count) {
        $markup .= "&bull;\n";
      } else {
        $markup .= "\n";
      }
      $markup .= "  </td>\n";

      // matrix
      $markup .= "  <td style=\"text-align: center;\">\n";
      $attribute_set = "NF";
      $family_attribute_set_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id_and_attribute_set($plant_list_plant->get_plant_obj()->get_id(), $attribute_set, $user_obj);
      if ($family_attribute_set_count) {
        $markup .= "&bull;\n";
      } else {
        $markup .= "\n";
      }
      $markup .= "  </td>\n";

      // matrix
      $markup .= "  <td style=\"text-align: center;\">\n";
      $attribute_set = "DA";
      $family_attribute_set_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id_and_attribute_set($plant_list_plant->get_plant_obj()->get_id(), $attribute_set, $user_obj);
      if ($family_attribute_set_count) {
        $markup .= "&bull;\n";
      } else {
        $markup .= "\n";
      }
      $markup .= "  </td>\n";

      // matrix
      $markup .= "  <td style=\"text-align: center;\">\n";
      $attribute_set = "F";
      $family_attribute_set_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id_and_attribute_set($plant_list_plant->get_plant_obj()->get_id(), $attribute_set, $user_obj);
      if ($family_attribute_set_count) {
        $markup .= "&bull;\n";
      } else {
        $markup .= "\n";
      }
      $markup .= "  </td>\n";

      // matrix
      $markup .= "  <td style=\"text-align: center;\">\n";
      $attribute_set = "NS";
      $family_attribute_set_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id_and_attribute_set($plant_list_plant->get_plant_obj()->get_id(), $attribute_set, $user_obj);
      if ($family_attribute_set_count) {
        $markup .= "&bull;\n";
      } else {
        $markup .= "\n";
      }
      $markup .= "  </td>\n";

      // matrix
      $markup .= "  <td style=\"text-align: center;\">\n";
      $attribute_set = "DT";
      $family_attribute_set_count = $plant_attributes_obj->get_plant_attribute_count_given_plant_id_and_attribute_set($plant_list_plant->get_plant_obj()->get_id(), $attribute_set, $user_obj);
      if ($family_attribute_set_count) {
        $markup .= "&bull;\n";
      } else {
        $markup .= "\n";
      }
      $markup .= "  </td>\n";

      if (0) {
        // plant_histories count (seed_packets)
        $markup .= "  <td style=\"background-color: #B4EEB4; text-align: center;\">\n";
        include_once("plant_histories.php");
        $plant_histories_obj = new PlantHistories($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $plant_histories_count = $plant_histories_obj->get_count_given_plant_list_plant_id($plant_list_plant->get_id(), $user_obj);
        $url = $this->url("plant_histories/plant_list_plant/" . $plant_list_plant->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $plant_histories_count . "</a>\n";
        $markup .= "    " . $plant_histories_obj->output_seed_packet_info_given_plant_list_plant_id($plant_list_plant->get_id(), $user_obj) . "\n";
        $markup .= "  </td>\n";
      }

      // plant_history_events count
      if (0) {
        include_once("plant_history_events.php");
        $plant_history_event_obj = new PlantHistoryEvents($this->get_given_config());
        $user_obj = $this->get_user_obj();
        // todo develop this function
        $plant_history_events_count = $plant_history_event_obj->output_plant_history_event_count_given_plant_list_plant_id($plant_list_plant->get_id(), $user_obj);
        if ($plant_history_events_count == 0) {
          $markup .= "  <td style=\"background-color: #FFFFFFF; text-align: center;\">\n";
          // no hyperlink
          $markup .= $plant_history_events_count;
        } else {
          $markup .= "  <td style=\"background-color: #B4EEB4; text-align: center;\">\n";
          // with hyperlink
          $url = $this->url("plant_history_events/plant_list_plants/" . $plant_list_plant->get_id());
          $markup .= "    <a href=\"" . $url . "\">" . $plant_history_events_count . "</a>\n";
        }
        $markup .= "  </td>\n";
      }

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // todo formalize this database heading code key
    $markup .= "<br />\n";
    $markup .= "<h3>Goal Articulation Summary</h3>\n";
    $markup .= "<p>\n";
    $markup .= "<div style=\"display: block;\"><ol><li>Used by <strong>B</strong>eneficial insects and wildlife.<li><strong>D</strong>eters <strong>P</strong>ests.<li>Has <strong>N</strong>itrogen-<strong>F</strong>ixing plants.<li>Has <strong>D</strong>ynamic <strong>A</strong>ccumulator plants.<li><strong>F</strong>ood is harvested.<li>Is filled with <strong>N</strong>ative <strong>S</strong>pecies.<li><strong>T</strong>olarates <strong>D</strong>rought.</ol> <small><em>August 4, 2017</em></small></div>\n";
    $markup .= "</p>\n";
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function output_table_about_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    # output for individual items
    $num = 0;
    $plant_histories_tally = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_list_plant) {
      $url = $this->url("plants/" . $plant_list_plant->get_plant_obj()->get_id());
      $markup .= "<h2><a href=\"" . $url . "\" style=\"text-decoration: none;\">" . $plant_list_plant->get_plant_obj()->get_common_name();
      $url = $this->url("plants/" . $plant_list_plant->get_plant_obj()->get_id()) . "</a> ";
      $markup .= " (<em><a href=\"" . $url . "\" style=\"text-decoration: none;\">" . $plant_list_plant->get_plant_obj()->get_scientific_name() . "</a></em>)</h2>\n";

      // plant_categories (architecture, from)
      $plant_category_id = $plant_list_plant->get_plant_obj()->get_plant_category_obj()->get_id();
      include_once("plant_categories.php");
      $plant_category_obj = new PlantCategories($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $plant_category_obj->get_plant_category_name_given_id($plant_category_id, $user_obj) . "<br />\n";
      $markup .= "<br />\n";

      // plant_family
      $family_id = $plant_list_plant->get_plant_obj()->get_plant_family_obj()->get_id();
      include_once("plant_families.php");
      $plant_family_obj = new PlantFamilies($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "Family: " . $plant_family_obj->get_plant_family_name_given_id($family_id, $user_obj) . "<br /><br />\n";

      // about
      include_once("plant_attributes.php");
      $plant_attributes_obj = new PlantAttributes($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $type = "simple";
      $markup .= $plant_attributes_obj->get_plant_attribute_list_given_plant_id($plant_list_plant->get_plant_obj()->get_id(), $user_obj, $type) . "<br />\n";

    }

    return $markup;
  }

  // method
  public function output_plant_count_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_pure_list($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_given_plant_list_id");

    // debug
    //print "debug user_obj " . $given_user_obj->name . "<br />";

    // load data from database
    //$this->determine_type();

    $markup .= $this->prepare_query();

    // todo this is bad becaues the markup from the prepare_query() function is lost
    // todo find a way to return this to the user (perhaps a whole error object)

    // return the array
    $list_obj = $this->get_list_bliss()->get_list();
    return $list_obj;
  }

  // method
  public function get_plant_id_given_id($given_plant_list_plant_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_plant_list_plant_id);
    $this->set_user_obj($given_user_obj);

    //print "debug plant list plant id = " . $this->get_given_id() . "<br />";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }   

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      // todo change the next two lines of code to a bliss_list function
      $list_obj = $this->get_list_bliss()->get_list();
      $plant_list_plant_obj = $list_obj[0];
      $markup .= $plant_list_plant_obj->get_plant_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function get_plant_list_id_given_id($given_plant_list_plant_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_plant_list_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }   

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      // todo change the next two lines of code to a bliss_list function
      $markup .= $this->get_list_bliss()->get_first_element()->get_plant_list_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function get_count_in_given_plant_id($given_plant_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_id($given_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }   

    return $this->get_list_bliss()->get_count();
  }

  // method
  protected function output_preface() {
    $markup = "";
    return $markup;
  }

}
