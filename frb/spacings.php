<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/spacings.php

include_once("lib/scrubber.php");

class Spacings extends Scrubber {

  // given
  private $given_plant_id;
  private $given_plant_list_id;

  // given_plant_id
  public function set_given_plant_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "plant_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_id = $var;
    }
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "plant_list_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_list_id = $var;
    }
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // attributes
  private $id;
  private $plant_obj;
  private $rows_per_bed;
  private $inches_between_plants;
  private $source;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // rows_per_bed
  public function set_rows_per_bed($var) {
    $this->rows_per_bed = $var;
  }
  public function get_rows_per_bed() {
    return $this->rows_per_bed;
  }

  // inches_between_plants
  public function set_inches_between_plants($var) {
    $this->inches_between_plants = $var;
  }
  public function get_inches_between_plants() {
    return $this->inches_between_plants;
  }

  // source
  public function set_source($var) {
    $this->source = $var;
  }
  public function get_source() {
    return $this->source;
  }

  // method
  private function make_spacing() {
    $obj = new Spacings($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_list_id()) {
      // subset
      $this->set_type("get_by_plant_list_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "";

    } else if ($this->get_type() == "get_all") {
      // todo this is set to private, so need to connect to project_id
      $sql = "SELECT spacings.id, spacings.plant_id, spacings.rows_per_bed, spacings.inches_between_plants, spacings.source FROM spacings ORDER BY spacings.rows_per_bed, spacings.inches_between_plants;";

    } else if ($this->get_type() == "get_by_plant_list_id") {
      $sql = "SELECT spacings.id, spacings.plant_id, spacings.inches_between_plants FROM plant_list_plants, spacings WHERE  plant_list_plants.plant_list_id = " . $this->get_given_plant_list_id() . " AND plant_list_plants.plant_id = spacings.plant_id AND spacings.inches_between_plants IS NOT NULL ORDER BY plant_list_plants.plant_id;";

      //print "debug sql = " . $sql . "<br />";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));

      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_spacing();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_rows_per_bed(pg_result($results, $lt, 2));
        $obj->set_inches_between_plants(pg_result($results, $lt, 3));
        $obj->set_source(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "get_by_plant_list_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_spacing();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_plant_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_inches_between_plants(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    if ($this->get_given_id()) {
      $markup .= "<div class=\"subsubmenu\">\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // todo code

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    rows per bed\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    inches between plants\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    source\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // todo clean up the following (not sure what it is)
    //foreach ($sorted_list_obj as $plant_id => $yield_array) {
    foreach ($this->get_list_bliss()->get_list() as $spacing) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $spacing->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<h2>" . $this->get_name() . "</h2>\n";

    $markup .= "<p>This is a yield of <strong>" . $this->get_plant_obj()->get_common_name_with_link() . "</strong>.</p>\n";

    return $markup;
  }

  // method
  public function get_inches_between_plants_given_plant_id($given_plant_id) {

    // set
    $this->set_given_plant_id($given_plant_id);

    // initialize
    $get_rows_per_plant = "";

    //print "debug plant id " . $this->get_given_plant_id() . "<br />";

    // assume the database has been used and data is here
    foreach ($this->get_list_bliss()->get_list() as $spacing) {

      // return data only if same plant
      if ($spacing->get_plant_obj()->get_id() == $this->get_given_plant_id()) {
        return $spacing->get_inches_between_plants();
      }
    }
    return "<p class=\"error\">I.B.P. not found.</p>";
  }

  // method
  public function get_rows_per_bed_given_plant_id($given_plant_id) {

    // set
    $this->set_given_plant_id($given_plant_id);

    // initialize
    $get_rows_per_plant = "";

    //print "debug plant id " . $this->get_given_plant_id() . "<br />";

    // assume the database has been used and data is here
    foreach ($this->get_list_bliss()->get_list() as $spacing) {

      // return data only if same plant
      if ($spacing->get_plant_obj()->get_id() == $this->get_given_plant_id()) {
        return $spacing->get_rows_per_bed();
      }
    }
    return "<p class=\"error\">R.P.B. not found.</p>";
  }

  // method
  public function get_plants_per_foot($given_plant_id) {
    $markup = "<p class=\"error\">P.P.F. is not defined.</p>\n";

    // initialize
    $inches_per_foot = 12;

    //print " **** debug given plant id " . $given_plant_id . "<br />\n";

    // loop to find the correct object instance
    foreach ($this->get_list_bliss()->get_list() as $spacing) {


      if ($spacing->get_plant_obj()->get_id() == $given_plant_id) {
        //print "debug found plant id " . $spacing->get_plant_obj()->get_id();
        //print " " . $spacing->get_inches_between_plants() . "<br /><br />";

        if ($spacing->get_inches_between_plants()) {
          //print "debug spacing ------- inches between plants --> " . $spacing->get_inches_between_plants() . "<br /><br />";
          $markup = $inches_per_foot / $spacing->get_inches_between_plants();
          break;
        }
      }
    }

    return $markup;
  }
}
