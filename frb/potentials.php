<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/potentials.php

include_once("lib/scrubber.php");

class Potentials extends Scrubber {

  // given
  private $given_hyperlink_id;
  private $given_href = "";

  // given_hyperlink_id
  public function set_given_hyperlink_id($id) {
    $this->given_hyperlink_id = $id;
  }
  public function get_given_hyperlink_id() {
   return $this->given_hyperlink_id;
  }

  public function set_given_href($var) {
    $this->given_href = $var;
  }
  public function get_given_href() {
    return $this->given_href;
  }

  // attributes
  //private $hyperlinks_obj;
  // for relative links
  //private $base_href;
  // from a element
  //private $href;
  //private $potential_flag_obj;
  // chardata of a element
  //private $content;
  //private $pagination_obj;

  private $id;
  private $name;
  private $address;
  private $homepage;
  private $notes;
  private $sort;
  private $status;
  private $webmaster_info;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // address
  public function set_address($var) {
    $this->address = $var;
  }
  public function get_address() {
    return $this->address;
  }

  // homepage
  public function set_homepage($var) {
    $this->homepage = $var;
  }
  public function get_homepage() {
    return $this->homepage;
  }

  // notes
  public function set_notes($var) {
    $this->notes = $var;
  }
  public function get_notes() {
    return $this->notes;
  }

  // sort
  public function set_sort($var) {
    $this->sort = $var;
  }
  public function get_sort() {
    return $this->sort;
  }

  // status
  public function set_status($var) {
    $this->status = $var;
  }
  public function get_status() {
    return $this->status;
  }

  // webmaster_info
  public function set_webmaster_info($var) {
    $this->webmaster_info = $var;
  }
  public function get_webmaster_info() {
    return $this->webmaster_info;
  }

  // hyperlinks_obj
  //public function get_hyperlinks_obj() {
  //  if (! isset($this->hyperlinks_obj)) {
  //    include_once("hyperlinks.php");
  //    $this->hyperlinks_obj = new Hyperlinks($this->get_given_config());
  //  }
  //  return $this->hyperlinks_obj;
  //}

  // base_href
  //public function set_base_href($var) {
  //  $this->base_href = $var;
  //}
  //public function get_base_href() {
  //  return $this->base_href;
  //}

  // href
  //public function set_href($var) {
  //  $this->href = $var;
  //}
  //public function get_href() {
  //  return $this->href;
  //}

  // potential_flag_obj
  //public function get_potential_flag_obj() {
  //  if (! isset($this->potential_flag_obj)) {
  //    include_once("potential_flags.php");
  //    $this->potential_flag_obj = new PotentialFlags($this->get_given_config());
  //  }
  //  return $this->potential_flag_obj;
  //}

  // content
  //public function set_content($var) {
  //  $this->content = $var;
  //}
  //public function get_content() {
  //  return $this->content;
  //}

  // method
  public function get_content_sifted() {
    // todo figure this function out
    $sifted_content = trim($this->content);
    // the content may have HTML markup
    // so if img, return value of alt attribute
    if (preg_match("/^&lt;img(.*)&gt;$/", $sifted_content, $matches)) {
      // is img tag only, so get alt value
      $sifted_content = $matches[1];
      if (preg_match("/alt=\"(.*?)\"/", $sifted_content, $matches)) {
        $sifted_content = $matches[1];
      }
    }
    if (preg_match("/^&lt;em\&gt;(.*)\&lt;\/em\&gt;$/", $sifted_content, $matches)) {
      // is em tag only, so get chardata
      $sifted_content = $matches[1];
    } else {
      // debug
      // print $sifted_content;
    }
    return $sifted_content;
  }

  // pagination_obj
  public function get_pagination_obj() {
    if (! isset($this->pagination_obj)) {
      include_once("lib/pagination.php");
      $this->pagination_obj = new Pagination($this->get_given_config());
    }
    return $this->pagination_obj;
  }

  // method
  private function make_potential() {
    $obj = new Potentials($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_hyperlink_id()) {
      $this->set_type("get_given_hyperlink_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
        $sql = "SELECT potentials.base_href, potentials.href, potentials.potential_flag_id, potential_flags.name, potentials.content FROM potentials, potential_flags WHERE potentials.potential_flag_id = potential_flags.id AND potentials.hyperlink_id = " . $this->get_given_id() . "ORDER BY potential_flags.name, potentials.href;";

    } else if ($this->get_type() == "get_hyperlinks_given_plant_id") {
      // special for lff
      $sql = ";";

    } else if ($this->get_type() == "not_yet_analyzed") {
      $sql = "SELECT potentials.potential_flag_id FROM potentials WHERE potentials.hyperlink_id = " . $this->get_given_hyperlink_id() . ";";
      //print "debug sql " . $sql . "<br />";

    } else if ($this->get_type() == "get_count") {
      $sql ="SELECT count(*) FROM potentials;";

    } else if ($this->get_type() == "get_all") {
      // start by getting a list of all the hyperlink_ids and href counts
      $sql = "SELECT potentials.hyperlink_id, hyperlinks.url, count(potentials.href) FROM potentials, hyperlinks WHERE potentials.hyperlink_id = hyperlinks.id  GROUP BY potentials.hyperlink_id, hyperlinks.url ORDER BY potentials.hyperlink_id LIMIT " . $this->get_pagination_obj()->get_count_per_page() . " offset " . $this->get_pagination_obj()->get_last_rank() . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
      //print "debug type = " . $this->get_type() . "<br />\n";
    }

    // define database based upon context (domain name, aka host)
    //print "debug host: " . $_SERVER['HTTP_HOST'] . "\n";
    if (strstr($_SERVER['HTTP_HOST'], "permaculturewebsites.org")) {
      // pws
      $database_name = "mudiacom_psites";
    } else {
      // lff
      $database_name = "plantdot_soiltoil";
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

//     "update_potential"
//        $this->sql_statement = "UPDATE potentials SET content = '" . $instance->get_content() . "', potential_flag_id = " . $instance->get_potential_flag_id() . " WHERE hyperlink_id = " . $instance->get_hyperlink_id() . " AND href = '" . $instance->get_href() . "';";

//   "list_potentials_of_linktype"
//        $this->sql_statement = "SELECT potentials.base_href, potentials.href, potentials.potential_flag_id, potential_flags.name, potentials.content FROM potentials, potential_flags WHERE potentials.potential_flag_id = potential_flags.id AND potentials.href like 'http://%' ORDER BY potentials.hyperlink_id, potentials.href;";

//    "list_potentials_counts_of_linktype"
//        // get potential_flad.id = 4 (aka "not yet analyzed")
//        $this->sql_statement = "SELECT potentials.hyperlink_id, count(potentials.href) FROM potentials WHERE potentials.potential_flag_id = 4 AND potentials.href like 'http://%' GROUP BY potentials.hyperlink_id ORDER BY count(potentials.href);";
//      } else if ($this->get_type() == "list_websites_and_potentials_of_linktype") {

//        // get potential_flad.id = 4 (aka not yet analyzed)
//        $this->sql_statement = "SELECT hyperlinks.id, hyperlinks.url, hyperlinks.name, potentials.href FROM hyperlinks, potentials WHERE hyperlinks.id = potentials.hyperlink_id AND potentials.potential_flag_id = 4 AND potentials.href like 'http://%' ORDER BY hyperlinks.name;";

//  "insert_potential"
//        $this->sql_statement = $instance->get_sql_statement();
//      } else if ($this->get_type() == "get_potential_given_hyperlink_id_and_href") {
//        $this->sql_statement = "SELECT potentials.base_href, potentials.potential_flag_id, potentials.content FROM potentials WHERE potentials.hyperlink_id = " . $instance->get_given_id() . " AND potentials.href = '" . $instance->get_given_href() . "';";

// "load_all_potentials"
//        $this->sql_statement = "SELECT potentials.hyperlink_id, potentials.base_href, potentials.href, potentials.potential_flag_id, potential_flags.name, potentials.content FROM potentials, potential_flags WHERE potentials.potential_flag_id = potential_flags.id AND potentials.hyperlink_id = " . $instance->get_hyperlink_id() . ";";
//      }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "not_yet_analyzed") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_potential();
        $obj->get_potential_flag_obj()->set_id(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "load_all_potentials") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $potential_obj = $this->make_potential();
        $potential_obj->set_hyperlink_id(pg_result($results, $lt, 0));
        $potential_obj->set_base_href(pg_result($results, $lt, 1));
        $potential_obj->set_href(pg_result($results, $lt, 2));
        $potential_obj->set_potential_flag_id(pg_result($results, $lt, 3));
        $potential_obj->set_potential_flag_name(pg_result($results, $lt, 4));
        $potential_obj->set_content(pg_result($results, $lt, 5));
        // debug
        //print "potentials: " . $potential_obj->get_content() . "<br />\n";
      }
    } else if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_potential();
        $obj->set_base_href(pg_result($results, $lt, 0));
        $obj->set_href(pg_result($results, $lt, 1));
        $obj->get_potential_flag_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_potential_flag_obj()->set_name(pg_result($results, $lt, 3));
        $obj->set_content(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "list_potentials_of_linktype") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $potential_obj = $this->make_potential();
        $potential_obj->set_base_href(pg_result($results, $lt, 0));
        $potential_obj->set_href(pg_result($results, $lt, 1));
        $potential_obj->set_potential_flag_id(pg_result($results, $lt, 2));
        $potential_obj->set_potential_flag_name(pg_result($results, $lt, 3));
        $potential_obj->set_content(pg_result($results, $lt, 4));
      }
    } else if ($this->get_type() == "list_potentials_counts_of_linktype") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $website_obj = $this->add_website();
        $website_obj->set_id(pg_result($results, $lt, 0));
        $website_obj->get_potentials_obj()->set_count(pg_result($results, $lt, 1));
        //$website_obj->set_url(pg_result($results, $lt, 1));
        //$website_obj->set_name(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "list_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $website_obj = $this->add_website();
        $website_obj->set_id(pg_result($results, $lt, 0));
        $website_obj->set_url(pg_result($results, $lt, 1));
        $website_obj->set_name(pg_result($results, $lt, 2));
        $website_obj->get_potentials_obj()->set_count(pg_result($results, $lt, 3));
        //$website_obj->set_url(pg_result($results, $lt, 1));
        //$website_obj->set_name(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_potential_given_hyperlink_id_and_href") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $potential_obj = $this->make_potential();
        $potential_obj->set_base_href(pg_result($results, $lt, 0));
        $potential_obj->set_potential_flag_id(pg_result($results, $lt, 1));
        $potential_obj->set_content(pg_result($results, $lt, 2));
        // debug
        //print "potentials: " . $potential_obj->get_content() . "<br />\n";
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $potential_obj = $this->make_potential();
        $potential_obj->get_hyperlinks_obj()->set_id(pg_result($results, $lt, 0));
        $potential_obj->get_hyperlinks_obj()->set_url(pg_result($results, $lt, 1));
        $potential_obj->get_hyperlinks_obj()->set_href_count(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_all_on_hold") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $potential_obj = $this->make_potential();
        $potential_obj->set_hyperlink_id(pg_result($results, $lt, 0));
        $potential_obj->set_base_href(pg_result($results, $lt, 1));
        $potential_obj->set_href(pg_result($results, $lt, 2));
        $potential_obj->set_potential_flag_id(pg_result($results, $lt, 3));
        //$potential_obj->set_content(pg_result($results, $lt, 4));
      }
    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . " does not know the type.");
    }

    return $markup;
  }

   // ok
  public function output_list_all() {
 
    // database //
    //$this->set_type("list_all");
    //$this->get_db_dash()->load($this);

    // output //

    // heading
    print "<h3>";
    print "<a href=\"linkmaster.php?command=list_links\" class=\"noshow\">Potentials Counts</a>";
    print "</h3>\n";

    $websites_array = $this->get_websites_array();
    if (count($websites_array)) {
      $this->output_table($websites_array);
    } else {
      $this->get_db_dash()->print_error("No potentials found.");
    }
  }

  // method
  public function output_list_all_absolute() {
 
    // get all so that the list can be post-processed
    include_once("websites.php");
    $websitelist = new WebSites($this->get_given_config());
    $websites_array = $websitelist->get_array_of_all_rows_simple();

    // post select processing
    // remove sites that are not potentials_flag_id == 4 ("not yet anaylzed")
    // remove all sites where url matches hyperlink.url
    $sifted_websites_array = array();
    foreach ($websites_array as $website) { 
      $url = str_replace('/', '\/', $website->get_url());
      $pattern = '/^' . $url . '/';
      $website->get_potentials_obj()->do_potentials_sifted_array($pattern);
      if ($website->get_potentials_obj()->get_sifted_count()) {
        array_push($sifted_websites_array, $website);
      }
    }

    // heading
    print "<h3>";
    print "<a href=\"linkmaster.php?command=list_links\" class=\"noshow\">Potentials Counts</a>";
    print " (<a href=\"linkmaster.php?command=list_links&amp;linktype=absolute\" class=\"noshow\">absolute</a>)";
    print "</h3>\n";

    if (count($sifted_websites_array)) {
      $this->output_table($sifted_websites_array);
    } else {
      $this->get_db_dash()->print_error("No potentials found.");
    }
  }

  // method
  private function output_table($websites_array) {
    
    print "  <table class=\"linkmaster\">\n";
    print "  <tr>\n";
    print "    <td class=\"header\">id</td>\n";
    print "    <td class=\"header\">permaculture website</td>\n";
    if ($this->get_linktype() == "absolute") {
      print "    <td class=\"header\">\"not yet analyzed\" and absolute count</td>\n";
    } else {
      print "    <td class=\"header\">\"not yet analyzed\" count</td>\n";
    }
    print "  </tr>\n";
    $tally = 0;
    foreach ($websites_array as $website) { 
      //$website->load_data_from_database();
      print "  <tr>\n";
      print "    <td>" . $website->get_id() . "</td>\n";
      print "    <td>" . $website->get_name_with_link() . "</td>\n";
      $link = "linkmaster.php?command=list_links&amp;id=" . $website->get_id();
      if ($this->get_linktype() == "absolute") {
        // append parameter
        $link .= "&amp;linktype=absolute";
      }
      if ($this->get_linktype() == "absolute") {
        $count = $website->get_potentials_obj()->get_sifted_count();
      } else {
        $count = $website->get_potentials_obj()->get_nonsifted_count();
      }
      print "    <td><a href=\"" . $link . "\" class=\"show\">" . $count . "</a></td>\n";
      $tally += $count;
      print "  </tr>\n";
    }
    // totals
    print "  <tr>\n";
    print "    <td class=\"total\" colspan=\"2\">total</td>\n";
    print "    <td class=\"total\">$tally</td>\n";
    print "  </tr>\n";
    print "  </table>\n";
  }

  // method
  public function output_list_given_id_absolute($id) {

    // store input
    $this->set_given_id($id);

    include_once("websites.php");
    $website = new WebSites($this->get_given_config());
    $website->set_id($id);
    $website->load_data_from_database();

    // post select processing
    // remove sites that are not potentials_flag_id == 4 ("not yet anaylzed")
    // remove all sites where url matches hyperlink.url
    $sifted_websites_array = array();
    $url = str_replace('/', '\/', $website->get_url());
    $pattern = '/^' . $url . '/';
    $potentials_array = $website->get_potentials_obj()->do_potentials_sifted_array($pattern);

    // output //
    // heading
    print "<h3>\n";
    print "<a href=\"linkmaster.php?command=list_links\" class=\"noshow\">Potentials</a>";
    print " (<a href=\"linkmaster.php?command=list_links&amp;linktype=absolute\" class=\"noshow\">absolute</a>)";
    print "</h3>\n";

    // list name of this website
    print "<p>" . $website->get_name_with_link_noshow() . "</p>\n";

    // get
    if (count($potentials_array)) {
      $this->output_given_id_table($potentials_array, $id);
    } else {
      $this->get_db_dash()->print_error("No potentials found.");
    }
  }

  // method
  public function edit_form() {
    // parameters
    $id = '';
    if (isset($_GET['id'])) {
      $id = $this->sanitize_id($_GET['id']);
      if ($id) {
        $this->set_given_id($id);
      } else {
        $this->get_db_dash()->print_error("Not an id.");
      }
    }
    $href_passthru = '';
    $href = '';
    if (isset($_GET['href'])) {
      $href_passthru = $this->sanitize_linkmaster_input($_GET['href']);
      // undo what previous function did
      $href = urldecode($href_passthru);
      //$href = preg_replace('/%23/', "!", $href);
      $href = preg_replace('/\s/', "%20", $href);
      // may have a url in a url
      $href = $this->encode_second_url_in_url($href);
      $href = preg_replace("/exclamationpoint/" , "%21", $href); 
      // debug
      print "href = " . $href . "<br />\n";
      $this->set_given_href($href);
    }

    // database
    //$this->set_type("get_potential_given_hyperlink_id_and_href");
    //$this->get_db_dash()->load($this, $type);

    // output
    print "<h3>Edit Potential</h3>\n";
    $potentials_array = $this->get_potentials_array();
    if (count($potentials_array)) {
      foreach ($potentials_array as $potential) { 
        print "<form method=\"post\" action=\"linkmaster.php\">\n";
        print "  <input type=\"hidden\" name=\"command\" value=\"update_potential\" />\n";
        print "  <input type=\"hidden\" name=\"hyperlink_id\" value=\"" . $id . "\" />\n";
        if ($this->get_linktype() == "absolute") {
          print "  <input type=\"hidden\" name=\"linktype\" value=\"absolute\" />\n";
        }
        print "<table border=\"0\">\n";
        print "<tr>\n";
        print "  <td>\n";
        print "    base_href\n";
        print "  </td>\n";
        print "  <td>\n";
        print "    " . $potential->get_base_href() . "\n";
        print "  </td>\n";
        print "</tr>\n";
        print "<tr>\n";
        print "  <td>\n";
        print "    href\n";
        print "  </td>\n";
        print "  <td>\n";
        print "    <input type=\"hidden\" name=\"href\" value=\"" . $href_passthru . "\" />\n";
        print "    " . $href . "\n";
        print "  </td>\n";
        print "</tr>\n";
        print "<tr>\n";
        print "  <td>\n";
        print "    content\n";
        print "  </td>\n";
        print "  <td>\n";
        print "    <input type=\"text\" name=\"content\" size=\"90\" value=\"" . $potential->get_content_sifted() . "\" />\n";
        print "  </td>\n";
        print "</tr>\n";
        print "<tr>\n";
        print "  <td>\n";
        print "    potential flag (status)\n";
        print "  </td>\n";
        print "  <td>\n";
        include_once("potential_flags.php");
        $potential_flags_obj = new PotentialFlags($this->get_given_config());
        $potential_flags_obj->output_form_select_potential_flags($potential->get_potential_flag_id());
        print "  </td>\n";
        print "</tr>\n";
        print "<tr>\n";
        print "  <td>\n";
        print "   &nbsp;\n";
        print "  </td>\n";
        print "  <td align=\"right\">\n";
        print "    <input type=\"submit\" name=\"submit\" value=\"Add\" />\n";
        print "  </td>\n";
        print "</tr>\n";
        print "</table>\n";
        print "</form>\n";
      }
    } else {
      // todo make error
      print "<p>Not found.</p>\n";
      print "<p>id = " . $this->get_given_id() . "</p>\n";
      print "<p>href = " . $this->get_given_href() . "</p>\n";
    }
  }

  // method
  public function update() {
    // database
    $type = "update_potential";
    $message = $this->get_db_dash()->update($this, $type);
    if ($message) {
      $this->get_db_dash()->print_error($message);
    } else {

      // old screen
      // todo output feedback
      //print "<p>Update had no error message; so, looks good.</p>\n";
      //print "<p><a href=\"linkmaster.php?command=list_links&amp;id=" . $this->get_hyperlink_id();
      //if ($linktype == "absolute") {
      //  print "&amp;linktype=absolute";
      //}
      //print "\">Potentials</a></p>\n";

      // redirect
      return "command=list_links&amp;id=" . $this->get_hyperlink_id();
    }

    // from the other file
    print "<h3>Update <a href=\"linkmaster.php?command=list_links\" class=\"noshow\">Potential</a></h3>\n";

    // parameters
    $hyperlink_id = '';
    if (isset($_POST['hyperlink_id'])) {
      $hyperlink_id = $this->sanitize_id($_POST['hyperlink_id']);
    }
    if (! $hyperlink_id) {
      $this->get_db_dash()->print_error("Error: hyperlink_id is not defined.");
      return;
    }

    if (isset($_POST['href'])) {
      $href = $this->sanitize_linkmaster_input($_POST['href']);
      // debug
      //print "href = " . $href . "<br />";
      $href = preg_replace("/&/", "&amp;", $href);
      $href = preg_replace('/\s/', "%20", $href);
      $href = $this->encode_second_url_in_url($href);
      $href = preg_replace("/exclamationpoint/" , "%21", $href); 
      // debug
      //print "href = " . $href . "<br />";
    }
    if (! $href) {
      $this->get_db_dash()->print_error("Error: href is not defined.");
      return;
    }

    if (isset($_POST['content'])) {
      $content = $this->sanitize_user_input($_POST['content']);
    }
    if (! $content) {
      $this->get_db_dash()->print_error("Error: content is not defined.");
      return;
    }

    if (isset($_POST['potential_flag_id'])) {
      $potential_flag_id = $this->sanitize_id($_POST['potential_flag_id']);
    }
    if (! $potential_flag_id) {
      $this->get_db_dash()->print_error("Error: potential_flag_id is not defined.");
      return;
    }

    include_once("potential.php");
    $potential_obj = new Potential($this->get_given_config());
    $potential_obj->set_hyperlink_id($hyperlink_id);
    $potential_obj->set_href($href);
    $potential_obj->set_content($content);
    $potential_obj->set_potential_flag_id($potential_flag_id);
    $redirect = $potential_obj->update($this->get_linktype());
    if (isset($_POST['linktype'])) {
      $linktype = $this->get_linktype();
      if ($linktype) {
        $redirect .= "&amp;linktype=" . $linktype;
      }
    }
    return $redirect;
  }

  // ok
  public function output_add_raw_form() {

    // for adding sql
    print "<h3>Add Potentials</h3>\n";
    print "<form method=\"post\" action=\"linkmaster.php\">\n";
    print "  <input type=\"hidden\" name=\"command\" value=\"insert_raw_potentials\" />\n";
    print "<table border=\"0\">\n";
    print "<tr>\n";
    print "  <td>\n";
    print "    code\n";
    print "  </td>\n";
    print "  <td>\n";
    print "    <input type=\"text\" name=\"code\" size=\"90\" />\n";
    print "  </td>\n";
    print "</tr>\n";
    print "<tr>\n";
    print "  <td>\n";
    print "    lines\n";
    print "  </td>\n";
    print "  <td>\n";
    print "    <textarea name=\"sql\" cols=\"90\"rows=\"90\"></textarea>\n";
    print "  </td>\n";
    print "</tr>\n";
    print "<tr>\n";
    print "  <td>\n";
    print "   &nbsp;\n";
    print "  </td>\n";
    print "  <td align=\"right\">\n";
    print "    <input type=\"submit\" name=\"submit\" value=\"Add\" />\n";
    print "  </td>\n";
    print "</tr>\n";
    print "</table>\n";
    print "</form>\n";
  }

  // method
  public function insert_raw() {

    // for adding sql
    print "<h3>Insert Potentials</h3>\n";

    // parameters //
    if (isset($_POST['code'])) {
      if ($_POST['code'] == "pws") {
        $sql = $this->sanitize_insert_sql($_POST['sql']);
      } else {
        $this->get_db_dash("Sorry, <em>code</em> is not correct.", undef);
        return;
      }
    } else {
      // todo make print_error
      $this->get_db_dash("Sorry, <em>code</em> is not correct.", undef);
      return;
    }

    print "<h4>sql</h4>\n";
    if (! $sql) {
      $this->get_db_dash("Sorry, sql is not defined.", undef);
      exit();
    }
    $lines = preg_split("/\n/", $sql);
    $num = 0;

    // type
    $this->set_type("insert_potential");

    print "  <table border=\"1\">\n";

    foreach ($lines as $line) {
      // deal with blank line
      if (! trim($line)) {
        // null string, go skip
        continue;
      }
      $num++;
      $line = str_replace("\'", "'", $line);
      $this->set_sql_statement($line);
      // database //
      $error_message = $this->get_db_dash()->insert($this, $type);
      print "<tr>\n";
      print "  <td>" . $num . "</td>\n";
      print "  <td>" . $line . "</td>\n";
      print "  <td>" . $error_message . "</td>\n";
      print "</tr>\n";
    }
    print "  </table>\n";
    print "  <br />\n";
    print "  <p><a href=\"linkmaster.php?command=add_potentials\">Add Potentials</a></p>\n";
    print "  <br />\n";
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    <em>webpage</em>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    href count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    not-yet-analyzed count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // loop
    $num = $this->get_pagination_obj()->get_last_rank();
    foreach ($this->get_list_bliss()->get_list() as $potential) {

      $markup .= "  <tr>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $url = $this->url("hyperlinks/" . $potential->get_hyperlinks_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $potential->get_hyperlinks_obj()->get_url() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\" style=\"text-align: left;\">\n";
      $url = $this->url("potentials/" . $potential->get_hyperlinks_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $potential->get_hyperlinks_obj()->get_href_count() . "</a>\n";
      $markup .= "  </td>\n";

      // not yet analyzed count
      $special_potentials_obj = new Potentials($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $count = $special_potentials_obj->get_not_yet_analyzed($user_obj, $potential->get_hyperlinks_obj()->get_id());
      if ($count == 0) {
        // green
        $markup .= "  <td class=\"specify-width\" style=\"text-align: left;background-color: #8CDD81;\">\n";
      } else {
        // white
        $markup .= "  <td class=\"specify-width\" style=\"text-align: left;background-color: #FFFFFF;\">\n";
      }
      $markup .= "    " . $count . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // pagination menu
    $markup .= $this->get_pagination_obj()->output_pagination_alphabetical("potentials");

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // show base_href of this potential
    $markup .= "<p><em>base href: </em>";
    $list = $this->get_list_bliss()->get_list();
    $element = $list[0];
    $markup .= "<a href=\"" . $element->get_base_href() . "\">" . $element->get_base_href() . "</a>";
    $markup .= "  </p>\n";

    // output table
    $markup .= "  <table class=\"plants\">\n";

    // headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    href\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    potential flag\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    content\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    <em>flag hyperlink</em>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // loop
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $potential) {

      $markup .= "  <tr>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\" style=\"text-align: left;\">\n";
      $link = "";
      if ($potential->is_absolute_link()) {
        $link = $potential->get_href();
      } else {
        $link = $potential->get_base_href() . $potential->get_href();
      }
      $markup .= "    <a href=\"" . $link . "\">" . $potential->get_href() . "</a>\n";
      $markup .= "  </td>\n";

      if ($potential->get_potential_flag_obj()->get_id() == 1) {
        // "already in database"
        // green
        $markup .= "    <td style=\"background-color:#8CDD81;\">";
      } else if ($potential->get_potential_flag_obj()->get_id() == 4) {
        // "not yet analyzed"
        // red
        $markup .= "    <td style=\"background-color:#EEB4B4;\">";
      } else {
        // purple
        $markup .= "    <td style=\"background-color:#AB82FF;\">";
      }
      $markup .= "    " . $potential->get_potential_flag_obj()->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $potential->get_content() . "\n";
      $markup .= "  </td>\n";

      // flag hyperlink
      if ($potential->get_potential_flag_obj()->get_id() == 1) {
        // if already in db, do not display add-hyperlink button
        // not-yet-analyzed, so nothing to do
        $markup .= "<td style=\"background-color:#8CDD81;\">";
        $markup .= "  already in database\n";
      } else if ($potential->get_potential_flag_obj()->get_id() != 4) {
        // if already rejected for db, do not display add-hyperlink button
        // already flagged, so nothing to do
        $markup .= "  <td style=\"background-color:#AB82FF;\">";
        $markup .= "  <em>already flagged</em>\n";
      } else {
        // check if this URL is in the database
        include_once("hyperlinks.php");
        $hyperlinks_obj = new Hyperlinks($this->get_given_config());
        if ($hyperlinks_obj->is_url_in_db($link)) {
          $markup .= "    <td style=\"background-color:#8CDD81;\">\n";
          $markup .= "      0\n";
        } else {
          // not already in db, so  display add-hyperlink button
          $markup .= "    <td style=\"background-color:#EFEFEF;\">\n";
          //$add_hyperlink_url = "linkmaster.php?command=add_hyperlink&amp;url=" . $link . "&amp;name=" . urlencode($potential->get_content());
          //$markup .= "  <a href=\"" . $add_hyperlink_url . "\">Add&nbsp;Hyperlink</a></td>\n";
        }
      }
      $markup .= "  </td>\n";
 
      // link for form
      //deal with exclamation point
      //$special = preg_replace("/%21/", "exclamationpoint" , $potential->get_href());
      //$status_link = $this->encode_url_in_url("linkmaster.php?command=edit_potential&amp;id=" . $potential->get_hyperlink_id() . "&amp;href=" . $special);

      //  todo place this were it needs to change a from-the-wild URL
      //  $markup .= "<a href=\"";
      //  // check for double slash
      //  if (preg_match("/\/$/", $potential->get_base_href()) && preg_match("/^\//", $potential->get_href())) {
      //    // remove a slash 
      //    $markup .= $potential->get_base_href() . preg_replace("/^\//", "", $potential->get_href());
      //  } else {
      //    $markup .= $potential->get_base_href() . $potential->get_href();
      //  }
      //  $markup .=  "\">" . $potential->get_href() . "</a>";


      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function is_absolute_link() {
    $pattern = "/^http:\/\//";
    if (preg_match($pattern, $this->get_href())) {
      return "true";
    }
    return "";
  }

  // method
  public function get_not_yet_analyzed($given_user_obj, $given_hyperlink_id) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);
    $this->set_given_hyperlink_id($given_hyperlink_id);

    // database
    $this->set_type("not_yet_analyzed");
    $this->prepare_query();

    //$total_count = 0;
    $count = 0;
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $potential) {
        //$total_count++;
        if ($potential->get_potential_flag_obj()->get_id() == 4) {
          $count++;
        }
      }
    }

    //return $count . " of " . $total_count;
    return $count;
  }

  // derived
  private $potentials_sifted_array = array(); // subset

  // method
  public function do_potentials_sifted_array($pattern) {
    if (! count($this->potentials_sifted_array)) {
      // save only if
      // (a) absolute,
      // (b) if potential_flag == "not yet analyzed", and
      // (c) not a relative mascarading as an absolute
      $this->load_all_potentials();

      // debug 
      //$count = count($this->get_potentials_array());
      //print "count before = " . $count . "<br />\n";

      foreach($this->get_potentials_array() as $potential_obj) {
        if ($potential_obj->is_absolute_link()) { 
          if ($potential_obj->is_potential_flag_not_yet_analyzed()) { 
            if (! preg_match($pattern, $potential_obj->get_href())) {
              array_push($this->potentials_sifted_array, $potential_obj);
            }
          }
        }
      }

      // debug 
      //$count = count($this->potentials_sifted_array);
      //print "count after = " . $count . "<br />\n";

    }

    return $this->potentials_sifted_array;
  }

  // method
  public function get_sifted_count() {
    return count($this->potentials_sifted_array);
  }
  // todo figure out
  private $linktype; // cull list if value is "absolute"

  // method
  public function get_linktype() {
    // from parameter
    // linktype is absolute or null string
    if (! isset($this->linktype)) {
      // parameter
      if (isset($_GET['linktype'])) {
        if ($_GET['linktype'] == "absolute") {
          $this->linktype = "absolute";
        } else {
          $this->get_db_dash()->print_error("Not a linktype.");
          $this->linktype = "";
        }
      } else if (isset($_POST['linktype'])) {
        if ($_POST['linktype'] == "absolute") {
          $this->linktype = "absolute";
        } else {
          $this->get_db_dash()->print_error("Not a linktype.");
          $this->linktype = "";
        }
      } else {
        $this->linktype = "";
      }
    }
    return $this->linktype;
  }
}
