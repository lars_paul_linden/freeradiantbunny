<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19
// version 1.6 2016-05-06

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/webpage_tags.php

include_once("lib/sproject.php");

class WebpageTags extends Sproject {

  // given
  private $given_tli;
  private $given_webpage_id;
  private $given_tag_id;

  // given_tli
  public function set_given_tli($var) {
    $this->given_tli = $var;
  }
  public function get_given_tli() {
    return $this->given_tli;
  }

  // given_webpage_id
  public function set_given_webpage_id($var) {
    $this->given_webpage_id = $var;
  }
  public function get_given_webpage_id() {
    return $this->given_webpage_id;
  }

  // given_tag_id
  public function set_given_tag_id($var) {
    $this->given_tag_id = $var;
  }
  public function get_given_tag_id() {
    return $this->given_tag_id;
  }

  // attributes
  private $id;
  private $webpage_obj;
  private $tag_obj;

  // id
  private function set_id($var) {
    $this->id = $var;
  }
  private function get_id() {
    return $this->id;
  }

  // webpage_obj
  private function get_webpage_obj() {
    if (! isset($this->webpage_obj)) {
      include_once("webpages.php");
      $this->webpage_obj = new Webpages($this->get_given_config());
    }
    return $this->webpage_obj;
  }

  // tag_obj
  public function get_tag_obj() {
    if (! isset($this->tag_obj)) {
      include_once("tags.php");
      $this->tag_obj = new Tags($this->get_given_config());
    }
    return $this->tag_obj;
  }

  // method
  private function make_webpage_tag() {
    $obj = new WebpageTags($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_webpage_id()) {
      $this->set_type("get_by_webpage_id");

    } else if ($this->get_given_tli()) {
      $this->set_type("get_by_tli");

    } else if ($this->get_given_tag_id()) {
      $this->set_type("get_by_tag_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_tags.* FROM webpage_tags, tags, webpages WHERE webpage_tags.tag_id = tags.id AND webpage_tags.webpage_id = webpages.id AND webpage_tags.webapage_id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_tags.* FROM webpage_tags, tags, webpages WHERE webpage_tags.tag_id = tags.id AND webpage_tags.webpage_id = webpages.id ORDER BY tags.name;";

    } else if ($this->get_type() == "get_by_webpage_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_tags.* FROM webpage_tags, tags, webpages WHERE webpage_tags.tag_id = tags.id AND webpage_tags.webpage_id = webpages.id AND webpages.id = " . $this->get_given_webpage_id() . " ORDER BY tags.name;";

      // debug
      //print "debug webpage_tags sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_tags.* FROM webpage_tags, tags, webpages WHERE webpage_tags.tag_id = tags.id AND webpage_tags.webpage_id = webpages.id AND webpages.domain_tli = '" . $this->get_given_tli() . "' ORDER BY tags.name;";

    } else if ($this->get_type() == "get_by_tag_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_tags.* FROM webpage_tags WHERE webpage_tags.tag_id = " . $this->get_given_tag_id() . " ORDER BY webpage_tags.webpage_id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    // debug
    //print "debug webpage_tags " . $markup . "<br />\n";

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_webpage_id" ||
        $this->get_type() == "get_by_tag_id" ||
        $this->get_type() == "get_by_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage_tag();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_webpage_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_tag_obj()->set_id(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_project_id()) {
      $markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    domains\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    webpage_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    webpages\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    tag_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    tags\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage_tag) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $webpage_tag->get_webpage_obj()->get_domain_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // webpage_id
      $markup .= "  <td>\n";
      $markup .= $webpage_tag->get_webpage_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // webpage
      $markup .= "  <td>\n";
      include_once("webpages.php");
      $webpage_obj = new Webpages($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $webpage_data = $webpage_obj->get_webpage_sym_given_id($webpage_tag->get_webpage_obj()->get_id(), $user_obj);
      $markup .= $webpage_data . "<br >\n";
      $markup .= "  </td>\n";

      // tag_id
      $markup .= "  <td>\n";
      $markup .= $webpage_tag->get_tag_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // tag
      $markup .= "  <td>\n";
      include_once("tags.php");
      $tag_obj = new Tags($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $tag_data = $tag_obj->get_tag_given_id($webpage_tag->get_tag_obj()->get_id(), $user_obj);
      $markup .= $tag_data . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup = "not yet built in webpage_tags";

    return $markup;
  }

  // method
  public function get_tag_list_given_webpage_id($given_webpage_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_webpage_id($given_webpage_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug webpage_tags: given_webpage_id = " . $this->get_given_webpage_id() . "<br />\n";

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    foreach ($this->get_list_bliss()->get_list() as $webpage_tag) { 
      include_once("tags.php"); 
      $tag_obj = new Tags($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $tag_data = $tag_obj->get_tag_sym_given_id($webpage_tag->get_tag_obj()->get_id(), $user_obj);
      $markup .= $tag_data . "<br />\n";
    }

    return $markup;
  }

  // method
  public function get_webpages_given_tag_id($given_tag_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_tag_id($given_tag_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    foreach ($this->get_list_bliss()->get_list() as $webpage_tag) { 
      $webpage_id = $webpage_tag->get_webpage_obj()->get_id();
      // debug
      //$markup .= "webpage_tags: debug " . $given_tag_id . "<br />\n";

      include_once("webpages.php"); 
      $webpage_obj = new Webpages($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $webpage_obj->get_webpage_sym_given_id($webpage_id, $user_obj);
    }

    return $markup;
  }

  // method
  protected function output_preface() {
    $markup = "";

    $markup .= "<br />\n";

    return $markup;
  }


  // method
  public function get_count_given_tli($given_domain_tli, $given_user_obj) {

    // set
    $this->set_given_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    return $this->get_list_bliss()->get_count();
  }

}
