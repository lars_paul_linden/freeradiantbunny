<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-17
// version 1.2 2015-01-04
// version 1.3 2015-02-15
// version 1.4 2015-06-19
// version 1.5 2015-10-16
// version 1.6 2016-03-25
// version 1.6 2016-12-25
// version 1.7 2017-04-22

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/projects.php

include_once("lib/standard.php");

class Projects extends Standard {

  // given
  private $given_domain_tli;
  private $given_process_id;
  private $given_scene_element_id;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_scene_element_id
  public function set_given_scene_element_id($var) {
    $this->given_scene_element_id = $var;
  }
  public function get_given_scene_element_id() {
    return $this->given_scene_element_id;
  }

  // attributes
  private $user_name;
  private $database_id;

  // todo the variable below should probably be spelled username
  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // database_id
  public function set_database_id($var) {
    $this->database_id = $var;
  }
  public function get_database_id() {
    return $this->database_id;
  }

  // method
  public function get_project_id() {
    // todo probably something to refactor here
    // todo does this mean that a project, in a way, contains itself
    // todo check this and see Scrubber to determine if it is logical
    // todo this should probably return a null or some sign of terminal
    // todo need to change the name of something to prevent confusion
    return $this->get_id();
  }

  // todo try to remove because /lib inherted class has function
  // todo need to delete this function but compare out inherted first
  // method
  public function get_img_url_as_img_element($padding = "padding: 0px 0px 0px 0px;", $float = "", $width = "66", $height = "66") {

    if ($this->get_img_url()) {
      // note returns HTML
      $string_to_return = "<img src=\"" . $this->get_img_url() . "\" style=\"" . $float . " padding: " . $padding . ";\" alt=\"img_url\"";
      if ($width) {
        $string_to_return .= " width=\"" . $width . "\"";
      }
      $string_to_return .= " />";

      return $string_to_return;
    }

    // todo should probably return html-image object (just one saying no img)
    return "no img_url\n";
  }

  // todo try to move this into a /lib inherted class
  // method
  public function get_icon_as_img_element($padding = "0px 20px 20px 0px;", $float = "float: left;", $width = "") {
    return $this->get_img_url_as_img_element($padding, $float, $width);
  }

  // method
  private function make_project() {
    $obj = new Projects($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    // discover type
    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_scene_element_id()) {
      $this->set_type("get_by_scene_element_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // note pre-process: figure out order_by for sql
    if ($this->get_given_sort() == "name") {
      $order_by = " ORDER BY projects.name";

    } else if ($this->get_given_sort() == "status") {
      $order_by = " ORDER BY projects.status, projects.name";

    } else {
      # note default sort is sort
      // todo testing different sorts to see what works
      //$order_by = " ORDER BY projects.sort DESC, projects.name, projects.id";
      $order_by = " ORDER BY projects.sort DESC, projects.status DESC, projects.name, projects.id";
    }

    // username_sql
    // this insures that a user may only get their own rows
    $username_sql = " projects.user_name = '" . $this->get_user_obj()->name . "' ";

    // note figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT * FROM projects WHERE projects.id = " . $this->get_given_id() . " AND " . $username_sql . ";";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      // note special only display projects with no parents (subquery below)

      if ($this->get_given_view() == "all_traditional_list" ||
          $this->get_given_view() == "simple") {

        // all_traditional, simple 
        // todo the following is on hold for debug purposes
        //$sql = "SELECT * FROM projects WHERE " . $username_sql . $order_by . ";";
        $sql = "SELECT * FROM projects;";

      } else if ($this->get_given_view() == "offline") {
        // offline
        $sql = "SELECT * FROM projects WHERE projects.status = 'offline' " . " AND " . $username_sql . " ORDER BY projects.sort DESC, projects.name;";

      } else if ($this->get_given_view() == "condensed" ||
                 $this->get_given_view() == "all") {
        // all, condensed
        $sql = "SELECT * FROM projects WHERE " . $username_sql . " " . $order_by . ";";
      } else {
        // default
        // default view is online
        // default subset is zoneline (or in progress)
        // todo here zoneline means status = 'in progress'
        $sql = "SELECT * FROM projects WHERE ( projects.status = 'in progress' OR projects.status = 'zoneline' ) AND " . $username_sql . " AND projects.status NOT LIKE 'offline%' " . $order_by . ";";
      }

    } else if ($this->get_type() == "get_by_project_id") {

      $sql = "SELECT * FROM projects WHERE projects.id = '" . $this->get_given_project_id() . "';";

    } else if ($this->get_type() == "get_by_process_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT * FROM projects, goal_statements, business_plan_texts, processes WHERE processes.id = '" . $this->get_given_process_id() . "' AND " . $username_sql . " AND projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id;";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT projects.* FROM domains, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'domains' AND domains.tli = scene_elements.class_primary_key_string AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND domains.tli = '" . $this->get_given_domain_tli() . "' AND " . $username_sql . ";";

    } else if ($this->get_type() == "get_all_special") {
      // security: only get the rows owned by the user
      $sql = "SELECT projects.* FROM projects WHERE " . $username_sql . " ORDER BY projects.sort DESC, projects.name;";

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM projects WHERE " . $username_sql . ";";

    } else if ($this->get_type() == "get_oldest_sort_obj") {
      $sql = "SELECT id, name, sort FROM projects WHERE " . $username_sql . " ORDER BY sort, name LIMIT 1;";

    } else if ($this->get_type() == "get_by_scene_element_id") {
      $sql = "SELECT * FROM projects, goal_statements, business_plan_texts, processes, scene_elements WHERE scene_elements.id = '" . $this->get_given_scene_element_id() . "' AND scene_elements.process_id = processes.id AND projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND " . $username_sql . " ORDER BY projects.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // debug
    //print "debug projects sql = " . $sql . "<br />\n";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_all_special" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_by_scene_element_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_project();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_user_name(pg_result($results, $lt, 4));
        $obj->set_status(pg_result($results, $lt, 5));
        $obj->set_img_url(pg_result($results, $lt, 6));
        $obj->set_database_id(pg_result($results, $lt, 7));
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_project();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_project();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_oldest_sort_obj") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_project();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_oldest_sort_date(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": does not know the type.");
    }
  }

  // method
  public function output_user_info() {
    $markup = "";

    // todo figure out public and private
    //$markup .= $this->output_user_info_message();

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // debug showing database_name of the connection
    $markup .= "<p style=\"background-color: #EFEFEF; padding: 10px 10px 10px 12px;\"><em>database_name</em>: " . $this->get_given_config()->get_database_name_connected() . "</p>\n";
    
    // todo ok all of this needs to be debugged and reworked
    // todo but at least the design is becoming more apparent
    // todo take each of these concepts and enforce them via MVC

    // todo move the following to the design dictionary
    // todo or all of the documentation is with the code (a la literate code)

    // todo view is VIEW
    // todo sort is SQL statement ORDER BY 
    // todo mode is 
    // todo subset is SQL statement WHERE that may reduce the number of rows

    $markup .= $this->output_view();

    $markup .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    $markup .= $this->output_sort();

    $markup .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    $markup .= $this->output_mode();

    $markup .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    $markup .= $this->output_subset();

    $markup .= "<br />\n";
    $markup .= "<br />\n";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";

    // todo clean up the comment below
    //$markup .= "<p>The table is a stack. Each part is an instance that will get its day in the sun of the cpu. The table is also a part of the whole, so it will have to share information back and forth with the other parts of the table (and outside the table), be patient while the other parts are being calculated, and have a way of saying I am reaady to be output.";

    // note that all below is the default view
    if ($this->get_given_view() == "hierarchy" || $this->get_given_view() == "all") {
      // todo this might be better performed by the sql query
      // remove rows that are not parent
      // todo ok this was turned off because it seemed like a poor design
      if (0) {
        $remove_list = array();
        foreach ($this->get_list_bliss()->get_list() as $project) {
          // find projects that are scene_elements
          include_once("scene_elements.php");
          $scene_element_obj = new SceneElements($this->get_given_config());
          $user_obj = $project->get_user_obj();
          if ($scene_element_obj->is_project_id_exists_as_projects_class($project->get_id(), $user_obj)) {
            // remove from list
            array_push($remove_list, $project);
          }
        }
        // take care of business
        foreach ($remove_list as $project_obj) {
          // remove from list
          // debug
          //$markup .= "debug projects.php to remove project.id = " . $project_obj->get_id() . "<br />\n";
          $markup .= $this->get_list_bliss()->remove_from_list_by_project_obj($project_obj);
        }
      }
    }

    if ($this->get_given_view() == "simple") {
      $markup .= $this->output_simple_table();
      return $markup;
    }

    $markup .= "<table class=\"plants\">\n";

    // header assigning subsystems to columns
    if (1) {
      $markup .= "<tr>\n";
      // column headings
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    IDENTITY\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    row\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    MM\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    field\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    field\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    field\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    field\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    field\n";
      $markup .= "  </td>\n";
      # todo figure out this is a refactoring kind of way
      #if (! $this->get_given_view() == "all_traditional_list") {
        $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
        $markup .= "    M.M.\n";
        $markup .= "  </td>\n";
      #}
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    atoms\n";
      $markup .= "  </td>\n"; 
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    files\n";
      $markup .= "  </td>\n"; 
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    virtu\n";
      $markup .= "  </td>\n"; 
      if (0) {
        $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px; background-color: #6aFFac;\">\n";
        $markup .= "    P.L.\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    \n";
        $markup .= "  </td>\n"; 
        $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px; background-color: #6aFFac;\" colspan=\"4\";>\n";
        $markup .= "    Locations\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px; background-color: #6aFFac;\">\n";
        $markup .= "    P.\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px; background-color: #6aFFac;\">\n";
        $markup .= "    [doc]\n";
        $markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\">\n";
        //$markup .= "    [doc]\n";
        //$markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\">\n";
        //$markup .= "    T.\n";
        //$markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
        //$markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
        //$markup .= "    s.e.\n";
        //$markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
        //$markup .= "  </td>\n";
      }

      // shifts
      // todo note for refactoring that this is the atomic pattern
      //$markup .= "  <td class=\"header\">\n";
      //$url = $this->url("shifts") . "?status=zoneline";
      //$markup .= "<a href=\"" . $url . "\">";
      //$markup .= "shift cnt";
      //$markup .= "</a>\n";
      //$markup .= "    \n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    respo count\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    pd count\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    bud cnt\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    M\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    T\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    W\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    T\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    F\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\" width=\"250\">\n";
      //$markup .= "    dashboard\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    sft count\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    counts\n";
      //$markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    // main header
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    user_names\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    pp\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"100\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n"; 
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    if (! $this->get_given_view() == "all_traditional_list") {
      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    ch\n";
      $markup .= "  </td>\n";
    }
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 40px; text-align: center;\">\n"; 
    $url = $this->url("goal_statements?view=" . $this->get_given_view());
    $markup .= "   <a href=\"" . $url . "\">gs</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 40px; text-align: center;\">\n"; 
    $url = $this->url("business_plan_texts?view=" . $this->get_given_view());
    $markup .= "   <a href=\"" . $url . "\">bpt</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 40px; text-align: center;\">\n";
    # $url = $this->url("processes?view=" . $this->get_given_view());
    $url = $this->url("processes");
    $markup .= "    <a href=\"" . $url . "\">proc</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 40px; text-align: center;\">\n";
    #$url = $this->url("scene_elements?view=" . $this->get_given_view()); 
    $url = $this->url("scene_elements"); 
    $markup .= "    <a href=\"" . $url . "\">se</a>\n";
    $markup .= "  </td>\n";

    if (0) {
      $markup .= "  <td class=\"header\" width=\"240\">\n";
      $url = $this->url("process_flows"); 
      $markup .= "    <a href=\"" . $url . "\">pf</a>\n";
      $markup .= "  </td>\n"; 

      $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "    pc\n";
      $markup .= "  </td>\n";

      // account
      //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      //$url = $this->url("accounts");
      //$markup .= "    <a href=\"" . $url . "\">accounts</a><br />\n";
      //$markup .= "  </td>\n";

      // lands
      $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      $url = $this->url("lands");
      $markup .= "    <a href=\"" . $url . "\">lands</a><br />\n";
      $markup .= "  </td>\n";

      // machines
      $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      $url = $this->url("machines");
      $markup .= "    <a href=\"" . $url . "\">machines</a><br />\n";
      $markup .= "  </td>\n";

      // domains
      $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      $url = $this->url("domains");
      $markup .= "    <a href=\"" . $url . "\">domains</a><br />\n";
      $markup .= "  </td>\n";

      // tickets
      //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      //$url = $this->url("tickets.php");
      //$markup .= "    <a href=\"" . $url . "\">tickets</a><br />\n";
      //$markup .= "  </td>\n";

      // plant_lists
      $markup .= "  <td class=\"header\" style=\"text-align: center; width: 80px;\">\n";
      $url = $this->url("plant_lists");
      $markup .= "    <a href=\"" . $url . "\">plant lists</a><br />\n";
      $markup .= "  </td>\n";

      // applications
      $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      $url = $this->url("applications");
      $markup .= "    <a href=\"" . $url . "\">applications</a><br />\n";
      $markup .= "  </td>\n";

      // databases
      $markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      $url = $this->url("databases");
      $markup .= "    <a href=\"" . $url . "\">databases</a><br />\n";
      $markup .= "  </td>\n";

      // email_addresses
      //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      //$url = $this->url("email_addressess");
      //$markup .= "    <a href=\"" . $url . "\">email_addressess</a><br />\n";
      //$markup .= "  </td>\n";

      // suppliers
      //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      //$url = $this->url("suppliers");
      //$markup .= "    <a href=\"" . $url . "\">suppliers</a><br />\n";
      //$markup .= "  </td>\n";

      // designs
      //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      //$url = $this->url("designs");
      //$markup .= "    <a href=\"" . $url . "\">designs</a><br />\n";
      //$markup .= "  </td>\n";

      // builds
      //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 80px;\">\n";
      //$url = $this->url("builds");
      //$markup .= "    <a href=\"" . $url . "\">builds</a><br />\n";
      //$markup .= "  </td>\n";

      // todo clean up because timecards are now associated with processes
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    timecards<br />count\n";
      //$markup .= "  </td>\n";

      // budgets
      //$markup .= "  <td class=\"header\" style=\"text-align: center; width: 60px;\">\n";
      //$url = $this->url("budgets");
      //$markup .= "    <a href=\"" . $url . "\">budgets</a>\n";
      //$markup .= "  </td>\n";
    }

    // shifts
    // todo note for refactoring that this is the atomic pattern
    //$markup .= "  <td class=\"header\">\n";
    //$url = $this->url("shifts") . "?status=zoneline";
    //$markup .= "<a href=\"" . $url . "\">";
    //$markup .= "shift cnt";
    //$markup .= "</a>\n";
    //$markup .= "    \n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    respo count\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    pd count\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    bud cnt\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    M\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    T\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    W\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    T\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    F\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\" width=\"250\">\n";
    //$markup .= "    dashboard\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    sft count\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    counts\n";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $total_scene_elements = 0;
    $total_goal_statements = 0;
    $total_business_plan_texts = 0;
    $total_processes = 0;
    $total_scene_elements = 0;
    $total_process_flows = 0;
    $total_shifts = 0;
    $total_domains = 0;
    $total_tickets = 0;
    $total_lands = 0;
    $total_machines = 0;
    $total_designs = 0;

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $project) {
      $num++;
      $markup .= "<tr>\n";

      // user_names
      $column_name = "user_names";
      $markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
      $markup .= "    " . $project->get_user_name() . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <!-- id=" . $project->get_id() . " -->\n";

      // set-up
      include_once("lib/counter.php");
      $counter_obj = new Counter($this->get_given_config());
      $user_obj = $project->get_user_obj();
      $counter_obj->set_user_obj($user_obj);
      $counter_obj->set_given_project_id($project->get_id());
      $output_name_flag = 0;

      // num
      $column_name = "num";
      $markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      // project parent (pp)
      $column_name = "pp";
      $markup .= "  <td style=\"padding: 4px 8px 4px 8px; background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
      include_once("scene_elements.php");
      $scene_element_obj = new SceneElements($this->get_given_config());
      $user_obj = $project->get_user_obj();
      $count = $scene_element_obj->get_parent_count_given_child_project_id($user_obj, $project->get_id());
      if ($count) {
        $markup .= $count;
      }
      $markup .= "  </td>\n";

      // status
      $column_name = "status";
      $markup .= "  <td style=\"background-color: " . $project->get_status_background_color() . "\">\n";
      if ($this->get_user_obj()) {
        $user_obj = $this->get_user_obj();
        $project->set_user_obj($user_obj);
        $markup .= "    " . $project->get_status() . "\n";
      } else {
        $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": user is not known in <em>projects</em> context.");
      }
      $markup .= "  </td>\n";

      // sort
      // old non-centralized way
      //$sort = $project->get_sort();
      //$column_name = "sort";
      //$class_name_for_url = "projects";
      //$id = $project->get_id();
      //$given_view = $this->get_given_view();
      //$markup .= $this->get_timekeeper_obj()->get_cell_colorized_given_sort_date($sort, $column_name, $class_name_for_url, $id, $given_view, "");
      // centralized way
      $markup .= $project->get_sort_cell();

      // id
      $column_name = "id";
      $markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
      $markup .= "    " . $project->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      if (! $this->get_given_view() == "all_traditional_list") {
        // children count
        $column_name = "children count";
        $markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
        //include_once("project_projects.php");
        //$project_projects_obj = new ProjectProjects($this->get_given_config());
        //$user_obj = $project->get_user_obj();
        //$url = $this->url("projects.php/project/" . $project->get_id());
        //$markup .= "<a href=\"" . $url . "\">";
        //$markup .= $project_projects_obj->get_project_children_counts_given_project_id($project->get_id(), $user_obj);
        //$markup .= "  </a>\n";
        $markup .= "  </td>\n";
      }

      // img_url
      $column_name = "img_url";
      $markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
      $padding = "0px 0px 0px 0px;";
      $float = "";
      $width = "50"; 
      $url = $this->url("projects/" . $project->get_id());
      $markup .= "    <a href=\"" . $url . "\">";
      $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
      $markup .= "</a>";
      $markup .= "\n";
      $markup .= "  </td>\n";

      // name
      $column_name = "name";
      $markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
      $markup .= "    " . $project->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // gs count
      $cell = $counter_obj->output_count_cell("goal_statements", $project->get_id(), $output_name_flag);
      $total_goal_statements += $this->extract_number($cell);
      $markup .= $cell;

      // bpt count
      $cell = $counter_obj->output_count_cell("business_plan_texts", $project->get_id(), $output_name_flag);
      $total_business_plan_texts += $this->extract_number($cell);
      $markup .= $cell;

      // proc count
      $cell = $counter_obj->output_count_cell("processes", $project->get_id(), $output_name_flag);
      $total_processes += $this->extract_number($cell);
      $markup .= $cell;
      
      // se count
      $cell = $counter_obj->output_count_cell("scene_elements", $project->get_id(), $output_name_flag);
      $total_scene_elements += $this->extract_number($cell);
      $markup .= $cell;

      if (0) {
        // process_flows
        $column_name = "process_flows";
        $markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
        include_once("process_flows.php");
        $process_flow_obj = new ProcessFlows($this->get_given_config());
        $user_obj = $project->get_user_obj();
        $url = $this->url("process_flows/projects/" . $project->get_id());
        $markup .= "<a href=\"" . $url . "\">";
        $cell= $process_flow_obj->get_count_given_project_id($project->get_id(), $user_obj);
        $total_process_flows += $this->extract_number($cell);
        $markup .= $cell;
        $markup .= "  </a>\n";
        $markup .= "  </td>\n";

        // project child count (pc) [scene_elements of project class]
        $column_name = "pc";
        $markup .= "  <td style=\"padding: 4px 8px 4px 8px; background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; text-align: center;\">\n";
        include_once("scene_elements.php");
        $cene_element_obj = new SceneElements($this->get_given_config());
         $user_obj = $project->get_user_obj();
        $project_class_count = $scene_element_obj->get_project_class_count_given_project_id($user_obj, $project->get_id());
        if ($project_class_count) {
          $markup .= $project_class_count;
        }
        $markup .= "  </td>\n";

        // accounts count
        //$cell = $counter_obj->output_count_cell("accounts", $project->get_id(), $output_name_flag);
        //$markup .= $cell;

        // lands count
        $cell = $counter_obj->output_count_cell("lands", $project->get_id(), $output_name_flag);
        //$total_lands += $this->extract_number($cell);
        $markup .= $cell;

        // machines count
        // todo note that this counts the scene_elements instances of class_name
        // todo note that this count is different than the by_project_id count
        $cell = $counter_obj->output_count_cell("machines", $project->get_id(), $output_name_flag);
        //$total_machines += $this->extract_number($cell);
        $markup .= $cell;

        // domains count
        $cell = $counter_obj->output_count_cell("domains", $project->get_id(), $output_name_flag);
        //$total_domains += $this->extract_number($cell);
        $markup .= $cell;

        // tickets
        //$cell = $counter_obj->output_count_cell("tickets", $project->get_id(), $output_name_flag);
        //$total_tickets += $this->extract_number($cell);
        //$markup .= $cell;

        // plant_lists count
        $markup .= $counter_obj->output_count_cell("plant_lists", $project->get_id(), $output_name_flag);

        // applications count
        $cell = $counter_obj->output_count_cell("applications", $project->get_id(), $output_name_flag);
        //$total_domains += $this->extract_number($cell);
        $markup .= $cell;

        // databases count
        $cell = $counter_obj->output_count_cell("databases", $project->get_id(), $output_name_flag);
        //$total_lands += $this->extract_number($cell);
        $markup .= $cell;

        // email_addresses count
        //$cell = $counter_obj->output_count_cell("email_addresses", $project->get_id(), $output_name_flag);
        //$total_lands += $this->extract_number($cell);
        //$markup .= $cell;

        // suppliers count
        //$cell = $counter_obj->output_count_cell("suppliers", $project->get_id(), $output_name_flag);
        //$total_lands += $this->extract_number($cell);
        //$markup .= $cell;

        // designs count
        //$cell = $counter_obj->output_count_cell("designs", $project->get_id(), $output_name_flag);
        //$total_lands += $this->extract_number($cell);
        //$markup .= $cell;

        // builds
        //$markup .= "  <td colspan=\"1\">\n";
        //$markup .= "    " . "\n";
        //$markup .= "  </td>\n";

        // timecards count
        $markup .= $counter_obj->output_count_cell("timecards", $project->get_id(), $output_name_flag);
        // todo all of this is the old way of displaying timecards
        //$column_name = "timecards";
        //$markup .= "  <td style=\"text-align: center; background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "\">\n";
        // 1 of 2: tc hours (total hours of timecards)
        //include_once("timecards.php");
        //$timecards_obj = new Timecards($this->get_given_config());
        //$user_obj = $this->get_user_obj();
        //$url = $this->url("timecards/project/" . $project->get_id());
        // first way was to display all hours
        //if (1) {
        //  $markup .= "    <a href=\"" . $url . "\">" . $timecards_obj->get_count_given_project_id($project->get_id(), $user_obj) . "</a>\n";
        //} else {
        //  // second way only shows hours from shifts
        //  $timecards_with_shifts_hours = $timecards_obj->get_total_hours_from_timecards_with_shifts_given_project_id($project->get_id(), $user_obj);
        //  $tc_with_shifts_count = "    <a href=\"" . $url . "\">" . $timecards_with_shifts_hours . "</a>\n";
        //  $total_timecards_with_shifts_hours += $timecards_with_shifts_hours;
        //  $needle = "/0/";
        //  if (preg_match($needle, $tc_with_shifts_count)) {
        //    // nothing there, so ok
        //    $markup .= "  <td style=\"text-align: center; background-color: " . $this>get_timekeeper_obj()->calculate_cell_color($column_name) . "\">\n";
        //  } else {
        //    // red alert
        //    $markup .= "  <td style=\"font-size: 110%; color: yellow; text-align: center; background-color: #CFCCFC;\">\n";
        //  }
        //  $markup .= $tc_with_shifts_count;
        //}
        //$markup .= "  </td>\n";

        // budgets count
        //$cell = $counter_obj->output_count_cell("budgets", $project->get_id(), $output_name_flag);
        //$markup .= $cell;
      }

      // shift count
      //$cell = $counter_obj->output_count_cell("shifts", $project->get_id(), $output_name_flag);
      //$total_shifts += $this->extract_number($cell);
      //$markup .= $cell;
      //$column_name = "shifts";
      // 2 of 2: shifts
      //$markup .= "  <td style=\"text-align: center; background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "\">\n";
      ////$markup .= "  <td style=\"background-color: " . $this->get_timekeeper_obj()->calculate_cell_color($column_name) . "; vertical-align: top; text-align: center;\">\n";
      //include_once("shifts.php");
      //$shifts_obj = new Shifts($this->get_given_config());
      //$user_obj = $this->get_user_obj();
      //$url = $this->url("shifts/project/" . $project->get_id());
      //$markup .= "    <a href=\"" . $url . "\">" . $shifts_obj->get_shifts_given_project_id($project->get_id(), $user_obj, "count-only") . "</a>\n";
      //$markup .= "  </td>\n";

      // budgets count
      //$markup .= $counter_obj->output_count_cell("budgets", $project->get_id(), $output_name_flag);

      // counts_sum
      //$counts_sum = $counter_obj->get_counts_sum();
      //if ($counts_sum == 0) {
      //  // colorize the projects that have not yet been started
      //  $markup .= "  <td style=\"text-align: center; background: #AAAAFF; color: #EFEFEF;\">\n";
      //} else {
      //  $markup .= "  <td style=\"text-align: center;\">\n";
      //}
      //$markup .= "    " . $counts_sum . "\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    $markup .= "<tr>\n";

    $markup .= "  <td colspan=\"8\">\n";
    $markup .= "    totals\n";
    $markup .= "  </td>\n";

    $markup .= "  <td style=\"text-align: center;\">\n";
    $url = $this->url("goal_statements?view=" . $this->get_given_view());
    $markup .= "    <a href=\"" . $url . "\">" . $total_goal_statements . "</a>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td style=\"text-align: center;\">\n";
    $url = $this->url("business_plan_texts?view=" . $this->get_given_view());
    $markup .= "    <a href=\"" . $url . "\">" . $total_business_plan_texts . "</a>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td style=\"text-align: center;\">\n";
    $url = $this->url("processes?view=" . $this->get_given_view());
    $markup .= "    <a href=\"" . $url . "\">" . $total_processes . "</a>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td style=\"text-align: center;\">\n";
    $url = $this->url("scene_elements?view=" . $this->get_given_view());
    $markup .= "    <a href=\"" . $url . "\">" . $total_scene_elements . "</a>\n";
    $markup .= "  </td>\n";

    //$markup .= "  <td style=\"text-align: center;\">\n";
    //$url = $this->url("process_flows?view=" . $this->get_given_view());
    //$markup .= "    <a href=\"" . $url . "\">" . $total_process_flows . "</a>\n";

    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  private function output_simple_table() {
    $markup = "";

    $markup .= "<table class=\"plants\" cellpadding=\"6\">\n";

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    user_names\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    img_url\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    notes\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $project) {
      $num++;
      $markup .= "<tr>\n";

      // user_names
      $column_name = "user_names";
      $markup .= "  <td style=\"text-align: left;\">\n";
      $markup .= "    " . $project->get_user_name() . "\n";
      $markup .= "  </td>\n";

      // num
      $column_name = "num";
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $column_name = "id";
      $markup .= "  <td style=\"text-align: center;\">\n";
      $markup .= "    <span style=\"font-size: 140%;\">" . $project->get_id() . "</span>\n";
      $markup .= "  </td>\n";

      // name
      $column_name = "name";
      $markup .= "  <td style=\"text-align: left;\">\n";
      $markup .= "    <span style=\"font-size: 140%;\">" . $project->get_name() . "</span>\n";
      $markup .= "  </td>\n";

      // notes (empty cell)
      $markup .= "  <td>\n";
      $markup .= "  &nbsp;\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // note check for errors
    // note make sure that there is a row for the given_id
    foreach ($this->get_list_bliss()->get_list() as $project) {
       if (! $project->get_name()) {
         return "<p style=\"error\">Error: no project given id = " . $this->get_given_id() . ".</p>\n";
       }
    }

    // output_view
    if ($this->get_given_view() == "printable") {
      // skip
    } else {
      $markup .= $this->output_view();
      $markup .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    }

    // get object
    $project = $this->get_list_bliss()->get_first_element();

    // create chapbook margins when printing
    $markup .= "<div class=\"chapbook\">\n";

    // img_url
    // note output img_url only if img_url url exists
    if ($project->get_img_url()) {
      $padding = "20px 20px 20px 0px";
      $float = "float: left;";
      $width = "100";
      $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
    } else {
      $markup .= "<p style=\"error\">Error: no img_url</p>\n";
    }

    // name
    $markup .= "<h2>";
    $markup .= $project->get_name();
    $markup .= "</h2>\n";

    // description
    // todo move flag for field labels to global config so user can toggle
    //$markup .= "<h3>description</h3>";
    if ($project->get_description()) {
      // todo could not get the css class to work
      $markup .= "<p>" . $project->get_description() . "</p>\n";
    } else {
      $markup .= "<p style=\"error\">Error: description not defined.</p>\n";
    }

    // end chapbook margins
    $markup .= "</div>\n";

    // note output these minor fields in smaller font
    $markup .= "<table class=\"plants\" style=\"font-size: 65%; margin-left: 10px;\">\n";

    // sort
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\" style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <h3>sort</h3>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <p>" . $project->get_sort() . "</p>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // status
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\" style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <h3>status</h3>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <p>" . $project->get_status() . "</p>\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // user_name
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\" style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <h3>user_name</h3>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <strong><em>projects instance has field:</em></strong><br />user_name: " . $project->get_user_name() . "<br />\n";
    $markup .= "        <strong><em>usernames instance has fields:</em></strong><br />" . "\n";
    include_once("usernames.php");
    $username_obj = new Usernames($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $username_obj->get_sidecar_given_user_name($project->get_user_name(), $user_obj) . "<br />\n";
    $markup .= "    <strong><em>... related to other objects ...</em></strong><br />" . "<br />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // database_id
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\" style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <h3>database_id</h3>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td style=\"padding: 0px 5px 0px 5px;\">\n";
    $markup .= "    <strong><em>databasess instance has field:</em></strong><br />database_id: " . $project->get_database_id() . "<br />\n";
    if ($project->get_database_id()) {
      include_once("databases.php");
      $database_obj = new Databases($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $url = $this->url("databases/" . $project->get_database_id());
      $markup .= "name: <a href=\"" . $url . "\">";
      $markup .= $database_obj->get_name_given_id($project->get_database_id(), $user_obj);
      $markup .= "</a><br />\n";
      $markup .= "    <strong><em>host_databases instance has fields:</em></strong><br />\n";
      include_once("host_databases.php");
      $host_database_obj = new HostDatabases($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $host_database_obj->get_sidecar_given_database_id($project->get_database_id(), $user_obj);
      $markup .= "\n";
      $markup .= "    <strong><em>... related to other objects ...</em></strong><br />" . "\n";
    } else {
      $markup .= "Error projects: database_id not found.\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    // sidecar
    // default
    $style_flag = "standard_flag";
    if ($this->get_given_view() == "printable") {
      // sidecar (hint reference style)
      // note intended to be a quick "hint reference" as to what is next
      $style_flag = "hint_flag";
    }
    // start sidecar output
    include_once("goal_statements.php");
    $goal_statement_obj = new GoalStatements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $goal_statement_obj->get_sidecar_given_project_id($project->get_id(), $user_obj, $style_flag);

    // more
    if ($this->get_given_view() == "printable") {
      // skip
    } else {
      // note here is a mix of interface styles
      $markup .= "<br />\n";
      $markup .= $project->get_subsystem_stack_view();
      $markup .= $project->get_parent_structure_view();
      $markup .= $project->get_child_structure_view();
      $markup .= $project->get_motivational_model_view();
      $markup .= $project->get_energy_flows_view();
    }

    $markup .= "<br />\n";

    return $markup;
  }

  // method
  private function get_motivational_model_view() {
    $markup = "";

    // load counter
    include_once("lib/counter.php");
    $counter_obj = new Counter($this->get_given_config());
    $counter_obj->set_given_project_id($this->get_id());

    // project_projects
    // note: this has 2 parts
    ///$markup .= "<div style=\"background-color: #EFEFEF; padding: 10px 0px 10px 0px;\">\n";
    // is this project a child of another project?
    //include_once("project_projects.php");
    //$project_project_obj = new ProjectProjects($this->get_given_config());
    //$heading = "yes";
    //$user_obj = $this->get_user_obj();
    //$markup .= $project_project_obj->get_parent_given_child_project_id($user_obj, $this->get_id(), $heading);
    //$markup .= "</div>\n";

    // table
    $markup .= "<table class=\"plants\">\n";

    // row of column headers

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"\" colspan=\"6\" align=\"center\" style=\"padding: 10px;\">\n";
    $markup .= "  Zachman Framework\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\" width=\"14%\">\n";
    $markup .= "    motivations&nbsp;(why)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"14%\">\n";
    $markup .= "    processes (how)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"14%\">\n";
    $markup .= "    things (what)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"14%\">\n";
    $markup .= "   locations (where)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"14%\">\n";
    $markup .= "    people (who)\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"14%\">\n";
    $markup .= "    timing (when)\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // about row
    $markup .= "<tr>\n";

    $markup .= "  <td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $markup .= "      <li>business vision and mission</li>\n";
    $markup .= "      <li>strategy</li>\n";
    $markup .= "      <li>business rule model</li>\n";
    $markup .= "    </ul>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $markup .= "      <li>class library</li>\n";
    $markup .= "      <li>semanitic model<br />\n";
    $markup .= "      <li>database schema<br />\n";
    $markup .= "    </ul>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $markup .= "      <li>state diagrams<br />\n";
    $markup .= "      <li>activity diagrams<br />\n";
    $markup .= "    </ul>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $markup .= "      <li>application architecture<br />\n";
    $markup .= "      <li>appmap<br />\n";
    $markup .= "      <li>network architecture<br />\n";
    $markup .= "    </ul>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $markup .= "      <li>use cases</li>\n";
    $markup .= "      <li>human-computer interfaces</li>\n";
    $markup .= "      <li>workflows</li>\n";
    $markup .= "      <li>security process</li>\n";
    $markup .= "    <ul>\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $markup .= "      <li>schedule model</li>\n";
    $markup .= "    <ul>\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // set up mini-globals
    //$output_name_flag = 0;
    $output_name_flag = 1;

    // regular rows
    // this is modelled after zachman's framework architecture
    $markup .= "<tr>\n";

    // <!-- 1 motivation -->
    $markup .= "  <td valign=\"top\">\n";
    $markup .= "    <table>\n";
    $markup .= "    <tr>\n";

    // projects (this instance itself)
    $markup .= "[this project]<br />\n";
    // goal_statements
    $markup .= $counter_obj->output_count_cell("goal_statements", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";
    // business_plan_texts
    $markup .= $counter_obj->output_count_cell("business_plan_texts", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    $markup .= "    </table>\n";
    $markup .= "  </td>\n";

    // <!-- 2 processes -->
    $markup .= "  <td valign=\"top\">\n";
    $markup .= "    <table>\n";
    $markup .= "    <tr>\n";

    // processes
    $markup .= $counter_obj->output_count_cell("processes", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // scene_elements
    $markup .= $counter_obj->output_count_cell("scene_elements", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // process_flows
    $markup .= $counter_obj->output_count_cell("process_flows", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // applications
    $markup .= $counter_obj->output_count_cell("applications", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // projects (in the context of being scene_elements)
    $markup .= $counter_obj->output_count_cell("projects", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // databases count
    $markup .= $counter_obj->output_count_cell("databases", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // machines count
    $markup .= $counter_obj->output_count_cell("machines", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // moneymakers count
    $markup .= $counter_obj->output_count_cell("moneymakers", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // design_instances count
    $markup .= $counter_obj->output_count_cell("design_instances", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // webpages count
    $markup .= $counter_obj->output_count_cell("webpages", $this->get_id(), $output_name_flag);

    // todo clean up the following kruft
    //$markup .= "      <td style=\"text-align: center;\">\n";
    //$url = $this->url("webpages/project/" . $this->get_given_id());
    //$markup .= "      <a href=\"" . $url . "\">webpages for project id " . $this->get_given_id() . "</a>\n";
    //$markup .= "      </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // classes count
    $markup .= $counter_obj->output_count_cell("classes", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // plant_lists count
    $markup .= $counter_obj->output_count_cell("plant_lists", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";

    //$markup .= "    <tr>\n";
    // budgets count
    //$markup .= $counter_obj->output_count_cell("budgets", $this->get_id(), $output_name_flag);
    //$markup .= "    </tr>\n";

    $markup .= "    <tr>\n";

    // designs count
    $markup .= $counter_obj->output_count_cell("designs", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // tools count
    $markup .= $counter_obj->output_count_cell("tools", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // books count
    $markup .= $counter_obj->output_count_cell("books", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // two columns
    $markup .= "      <td valign=\"top\">\n";
    $markup .= "        <p>this project is related to database_id = " . $this->get_database_id() . "</p>\n";
    $markup .= "      </td>\n";

    $markup .= "    </tr>\n";

    $markup .= "    </table>\n";
    $markup .= "  </td>\n";

    // <!-- 3 things (what) -->
    $markup .= "  <td valign=\"top\">\n";

    $markup .= "    <table>\n";
    $markup .= "    <tr>\n";

    // hyperlinks count
    $markup .= $counter_obj->output_count_cell("hyperlinks", $this->get_id(), $output_name_flag);

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // todo fix tags because it no longer has a project_id field
    // tags
    //$markup .= $counter_obj->output_count_cell("tags", $this->get_id(), $output_name_flag);
    $markup .= "    <td>tags</td>\n";
    $markup .= "    <td>offline</td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    $markup .= "      <td style=\"text-align: center;\">\n";
    $url = $this->url("builds/projects/" . $this->get_id());
    $markup .= "        <a href=\"" . $url . "\">builds</a>\n";
    $markup .= "      </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    $markup .= "      <td>\n";
    // related to plant_lists
    $markup .= "        <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $url = $this->url("plant_list_plants");
    $markup .= "          <li><a href=\"" . $url . "\">Plant List Plants</a></li>\n";
    $url = $this->url("plant_list_projects");
    $markup .= "          <li><a href=\"" . $url . "\">Plant List Projects</a></li>\n";
    $markup .= "        </ul>\n";
    $markup .= "      </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    $markup .= "      <td>\n";
    // accounts
    $markup .= "        <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $url = $this->url("postings");
    $markup .= "          <li><a href=\"" . $url . "\">Postings</a></li>\n"; 
    $url = $this->url("accounts");
    $markup .= "          <li><a href=\"" . $url . "\">Accounts</a></li>\n"; 
    $url = $this->url("journals");
    $markup .= "          <li><a href=\"" . $url . "\">Journals</a></li>\n"; 
    $url = $this->url("asset_types");
    $markup .= "          <li><a href=\"" . $url . "\">Asset Types</a></li>\n"; 
    $markup .= "        </ul>\n";
    $markup .= "      </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    $markup .= "    <td>\n";
    $markup .= "      <p>Polymorphic Animated 3D Model of Permaculture Project as Project Documentation</p>\n";
    $markup .= "    </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    $markup .= "    <td>\n";
    // old code needs to be re-written
    //$markup .= $project_project_obj->get_children_given_parent_project_id($user_obj, $this->get_id(), "");

    $markup .= "    </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    $markup .= "    <td>";
    $markup .= "      <p>Maxonomy Rallytally</p>\n";
    $markup .= "      <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";

    // todo the line below is weird... why child_obj?
    //$id = $project_project_obj->get_child_obj()->get_id();

    // todo this replaced the above
    $id = $this->get_id();
    if (! $id) {
       $id = "?";
    }

    $markup .= "      <li><a href=\"https://permaculturewebsites.org/rallytallies\">RallyTallies</a></li>";
    $markup .= "      <li><a href=\"https://permaculturewebsites.org/maxonomies\">Maxonomies</a> (lookup table)</li>";
    $markup .= "    </ul>\n";

    $markup .= "    </td>";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    $markup .= "      <td>\n";
    $markup .= "        <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $url = $this->url("observations/project/" . $this->get_id());
    $markup .= "          <li><a href=\"" . $url . "\">Observations</a></li>\n"; 
    $url = $this->url("guest_passes/" . $this->get_given_id());
    $markup .= "          <li><a href=\"" . $url . "\">Guest Passes</a></li>\n"; 
    $url = $this->url("design_orders");
    $markup .= "          <li><a href=\"" . $url . "\" class=\"show\">Design Orders</a></li>";
    $url = $this->url("design_order_items");
    $markup .= "          <li><a href=\"" . $url . "\" class=\"show\">Design Order Items</a></li>";
    $markup .= "        </ul>\n";
    $markup .= "      </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";
    // plants
    $markup .= "      <td>\n";
    $url = $this->url("plants");
    $markup .= "        <h3><a href=\"" . $url . "\">Plants</a></h3>\n";
    $markup .= "        <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $url = $this->url("plant_aliases");
    $markup .= "          <li><a href=\"" . $url . "\">Plant Aliases</a></li>\n"; 
    $url = $this->url("plant_categories");
    $markup .= "          <li><a href=\"" . $url . "\">Plant Categories</a></li>\n"; 
    $url = $this->url("families");
    $markup .= "          <li><a href=\"" . $url . "\">Families</a></li>\n";
    $url = $this->url("varieties");
    $markup .= "          <li><a href=\"" . $url . "\">Varieties</a></li>\n";
    $markup .= "        </ul>\n";
    $markup .= "      </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // under plant_lists
    $markup .= "<td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $url = $this->url("seed_packets");
    $markup .= "      <li><a href=\"" . $url . "\">Seed Packets</a></li>\n";
    $url = $this->url("values");
    $markup .= "      <li><a href=\"" . $url . "\">Values</a></li>\n"; 
    $markup .= "      <li>The <em>weekly pickup lists</em> have an estimated market value tally.</li>\n";
    $markup .= "      <li>Barter</li>\n";
    $markup .= "    </ul>\n";
    $markup .= "</td>\n";;

    $markup .= "    </tr>\n";
    $markup .= "    <tr>\n";

    // plants
    $markup .= "  <td>\n";
    $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
    $url = $this->url("units");
    $markup .= "      <li><a href=\"" . $url . "\">Units</a></li>\n";
    $url = $this->url("plant_units");
    $markup .= "      <li><a href=\"" . $url . "\">Plant Units</a></li>\n";
    $url = $this->url("yields");
    $markup .= "      <li><a href=\"" . $url . "\">Yields</a></li>\n";
    $markup .= "    </ul>\n";
    $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // special objects
      $markup .= "      <td>\n";
      $markup .= "        <h3>Special Categorizable</h3>\n";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $url = $this->url("images");
      $markup .= "      <li style=\"background-color: orange;\"><a href=\"" . $url . "\">Images</a></li>\n";
      $url = $this->url("plant_images");
      $markup .= "      <li style=\"background-color: orange;\"><a href=\"" . $url . "\">Plant Images</a></li>\n";
      $url = $this->url("book_clips");
      $markup .= "      <li style=\"background-color: orange;\"><a href=\"" . $url . "\">Book Clips</a></li>\n";
      $url = $this->url("storages");
      $markup .= "      <li style=\"background-color: orange;\"><a href=\"" . $url . "\">Storages</a></li>\n";
      $markup .= "  </ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // indiegoals
      $markup .= "<td>\n";
      $markup .= "    <p style=\"color: #000000;\">Focus on building the <em>blob</em>, a database schema with associaed PHP and Perl objects, because this is my 10,000 hours. I am a database designer.</p>";
      $markup .= "    <p style=\"color: #000000;\">Ig Agents can be perl objects.</p>";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $markup .= "      <li><a href=\"https://permaculturewebsites.org/indiegoals\">IndieGoals</a></li>";
      $markup .= "    </ul>\n";
      $markup .= "    <p>External Database</p>\n";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $url = $this->url("indiegoals");
      $markup .= "      <li><a href=\"" . $url . "\">Indiegoals</a> - whereas the project.tli is the inbound indiegoal space, this indiegoal table declares the outbout indiegoal space (the indiegoals implied by this project)</li>\n";
      $url = $this->url("styles");
      $markup .= "      <li><a href=\"" . $url . "\">Styles</a></li>\n";
      $markup .= "    </ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";

      $markup .= "    </table>\n";
      $markup .= "  </td>\n";

      // <!-- 4 locations -->
      $markup .= "  <td valign=\"top\">\n";
      $markup .= "    <table>\n";
      $markup .= "    <tr>\n";

      // lands
      $markup .= $counter_obj->output_count_cell("lands", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // domains count
      $markup .= $counter_obj->output_count_cell("domains", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // applications count
      $markup .= $counter_obj->output_count_cell("applications", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // under lands
      $markup .= "<td>\n";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $url = $this->url("project_lands");
      $markup .= "      <li><a href=\"" . $url . "\">Project Lands</a></li>\n";
      $url = $this->url("agricultural_types");
      $markup .= "      <li><a href=\"" . $url . "\">Agricultural Types</a></li>\n";
      $url = $this->url("soil_areas");
      $markup .= "      <li><a href=\"" . $url . "\">Soil Areas</a></li>\n";
      $url = $this->url("spacings");
      $markup .= "      <li><a href=\"" . $url . "\">Spacings</a></li>\n";
      $url = $this->url("soil_tests");
      $markup .= "      <li><a href=\"" . $url . "\">Soil Tests</a></li>\n";
      $markup .= "    </ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      $markup .= "<td>\n";
      $markup .= "    <h3>Virtual Tables</h3>\n";
      $markup .= "    <ul style=\"padding: 4px 4px 4px 4px;margin:9px 4px 9px 2px;background-color: lightblue; list-style-position: inside;\">\n";

      $url = $this->url("crop_yield_plans/project/" . $this->get_id());
      $markup .= "      <li style=\"background-color: lightblue; margin: 0px 0px 4px 0px;\"><a href=\"" . $url . "\">Crop Yield Plans (Row Feet Row &amp; Beds Needed)</a> [row_feet_needed, beds_needed]</li>\n";

      $url = $this->url("crop_maps/project/" . $this->get_id());
      $markup .= "      <li style=\"background-color: lightblue; margin: 0px 0px 4px 0px;\"><a href=\"" . $url . "\">Crop Maps</a></li>\n";

      $url = $this->url("greenhouse_plans/project/" . $this->get_id());
      $markup .= "      <li style=\"background-color: lightblue; margin: 0px 0px 4px 0px;\"><a href=\"" . $url . "\">Greenhouse Plans (Trays Needed)</a> [plants_needed, trays_needed]</li>\n";

      $url = $this->url("greenhouse_details/project/" . $this->get_id());
      $markup .= "      <li style=\"background-color: lightblue; margin: 0px 0px 4px 0px;\"><a href=\"" . $url . "\">Greenhouse Details</a></li>\n";

      $url = $this->url("crop_plan_milestones/project/" . $this->get_id());
      $markup .= "      <li style=\"background-color: lightblue; margin: 0px 0px 4px 0px;\"><a href=\"" . $url . "\">Crop Plan Milestones</a></li>\n";
      $markup .= "    </ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // mapserver
      $markup .= "<td>\n";
      $markup .= "    <h3 style=\"margin: 6px 0px 0px 0px;padding: 0px 0px 0px 0px;\">Mapserver</h3>";
      $markup .= "    <p>Maya AutoDesk 2013</p>\n";
      $markup .= "    <p>what data format? X,Y,Z plane</p>\n";
      $markup .= "    <p>print maps</p>\n";
      $markup .= "    <p>GIS geographic information system</p>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";

      $markup .= "    </table>\n";
      $markup .= "  </td>\n";

      // <!-- 5 people  -->
      $markup .= "  <td valign=\"top\">\n";
      $markup .= "    <table>\n";
      $markup .= "    <tr>\n";

      // usernames
      $markup .= $counter_obj->output_count_cell("usernames", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // suppliers
      $markup .= $counter_obj->output_count_cell("suppliers", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // designers
      //$markup .= $counter_obj->output_count_cell("designers", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // email_addresses
      $markup .= $counter_obj->output_count_cell("email_addresses", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // harvests
      $markup .= $counter_obj->output_count_cell("harvests", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // shares
      $markup .= $counter_obj->output_count_cell("shares", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // roles and personnae
      $markup .= "<td>\n";
      $markup .= "    <h3>Roles &amp; Personnae</h3>";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $markup .= "      <li>webmasters</li>";
      $markup .= "      <li>Members</li>";
      $markup .= "      <li>Sharers</li>";
      $markup .= "      <li>Volunteers</li>";
      $markup .= "      <li>eaters<br />\n";
      $markup .= "      <li>farmstand_workhands<br />\n";
      $markup .= "      <li>cooks<br />\n";
      $markup .= "      <li>professional cooks<br />\n";
      $markup .= "      <li>project owner<br />\n";
      $markup .= "      <li>angel investors<br />\n";
      $markup .= "    </ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      $markup .= "      <td>\n";

      $url = $this->url("tickets/projects/" . $this->get_id());
      $markup .= "    <p><a href=\"" . $url . "\">tickets</a></p>\n";
      $markup .= "    <br />\n";
      $markup .= "    <h3>Server</h3>\n";
      $markup .= "    <p>Send messages back and forth.</p>\n";
      $markup .= "    <p>Automatic messages for condistions (e.g., if observation page reaches a measurement threshold), implies rules bank.</p>\n";
      $markup .= "    <p>Send out e-mail or SMS or letter.</p>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";

      $markup .= "    </table>\n";
      $markup .= "  </td>\n";

      // <!-- 6 timing -->
      $markup .= "  <td valign=\"top\">\n";

      $markup .= "    <table>\n";

      //$markup .= "    <tr>\n";
      // shifts
      //$markup .= $counter_obj->output_count_cell("shifts", $this->get_id(), $output_name_flag);
      //$markup .= "    </tr>\n";

      $markup .= "    <tr>\n";

      // tickets count
      //$markup .= $counter_obj->output_count_cell("tickets", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // timecards count
      $output_name_flag = 1;
      $markup .= $counter_obj->output_count_cell("timecards", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // pickups
      //$markup .= $counter_obj->output_count_cell("pickups", $this->get_id(), $output_name_flag);

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      $markup .= "<td>\n";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $url = $this->url("pickup_plants/project/" . $this->get_id());
      $markup .= "  <li><a href=\"" . $url . "\">Pickup Plants</a></li>\n";
      $url = $this->url("pickup_details/project/" . $this->get_given_id());
      $markup .= "    <li><a href=\"" . $url . "\">Pickup Details</a></li>\n";
      $markup .= "</ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // plant_histories
      $markup .= "<td>\n";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $url = $this->url("plant_histories/project/" . $this->get_id());
      $markup .= "    <li><a href=\"" . $url . "\">Plant Histories</a></li>\n";
      $url = $this->url("plant_history_events/project/" . $this->get_id());
      $markup .= "    <li><a href=\"" . $url . "\">Plant History Events</a></li>\n";
      $url = $this->url("sowings/project/" . $this->get_id());
      $markup .= "    <li><a href=\"" . $url . "\">Sowings</a></li>\n";
      $markup .= "    </ul>";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      $markup .= "<td>\n";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $url = $this->url("crop_plans/project/" . $this->get_id());
      $markup .= "      <li><a href=\"" . $url . "\">Crop Plans (Simulation)</a></li>\n";
      $markup .= "    </ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // shifts
      $markup .= "<td>\n";
      $markup .= "  <h3>Schedules</h3>\n";
      $markup .= "    <ul style=\"margin: 0px; padding: 0px; list-style-position: inside;\">\n";
      $markup .= "    <li><a href=\"https://eatlocalfreshfood.org/lff/_images/timeline_2012_whole.jpg\">Timeline 2012</a></li>";
      $markup .= "  </ul>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      //  farmtasks
      $markup .= "<td>\n";
      $markup .= "    <h3>Farmtasks</h3>\n";
      $markup .= "    <p><small>1. This should be related to sowings table and converting \"plan\" to \"fact\".</small></p>\n";
      $markup .= "    <p><small>2. Record all work quickly at the end of the task. Test this Using droid. Make a webpage form that helps a farmhand record events of work quickly in the field (with a fphotograph and using timestamp and ownership data). Map out a system accept these and input theme into the database.</small></p>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      // events
      $markup .= "<td>\n";
      $markup .= "    <h3>List of Events</h3>";
      $markup .= "    <p>weekly, quarterly, and annual income statements</p>\n";
      $markup .= "    <p>moon, sun, planets, and constellation events</p>\n";
      $markup .= "    <p>solstice and equinox events</p>\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    <tr>\n";

      $markup .= "<td>\n";
      //$url = $this->url("calendars/project/" . $project->get_id());
      $markup .= "    Here are the various calendars associated with a project.<br />\n";
      $markup .= "</td>\n";

      $markup .= "    </tr>\n";
      $markup .= "    </table>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $markup .= "</table>\n";
      $markup .= "<br />\n";

      return $markup;
  }

  // method
  private function get_subsystem_stack_view() {
    $markup = "";

    // load counter
    include_once("lib/counter.php");
    $counter_obj = new Counter($this->get_given_config());
    $counter_obj->set_given_project_id($this->get_id());
    $output_name_flag = 1;

    $markup .= "    <table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"\" colspan=\"3\" align=\"center\" style=\"padding: 10px;\">\n";
    $markup .= "    Motivational Model Subsystem\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "    <tr>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 6px;\">\n";
    $markup .= "        subsystems\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 6px;\">\n";
    $markup .= "        classes<br />\n";
    $markup .= "(database&nbsp;table_names)\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 6px;\">\n";
    $markup .= "        about (e.g. count)\n";
    $markup .= "      </td>\n";
    $markup .= "    </tr>\n";

    // projects
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Motivational Model Subsystem\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #CFCCFC;padding: 6px;\">\n";
    $markup .= "        <strong>projects</strong>\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #CFCCFC;padding: 6px;\">\n";
    $markup .= "        (this project; id = " . $this->get_id() . ")\n";
    $markup .= "      </td>\n";
    $markup .= "    </tr>\n";

    // goal_statements
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Motivational Model Subsystem\n";
    $markup .= "      </td>\n";
    // output cells
    $markup .= $counter_obj->output_count_cell("goal_statements", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // business_plan_texts
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Motivational Model Subsystem\n";
    $markup .= "      </td>\n";
    // output cells
    $markup .= $counter_obj->output_count_cell("business_plan_texts", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // processes
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Motivational Model Subsystem\n";
    $markup .= "      </td>\n";
    // output cells
    $markup .= $counter_obj->output_count_cell("processes", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // scene_elements
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Motivational Model Subsystem\n";
    $markup .= "      </td>\n";
    // output cells
    $markup .= $counter_obj->output_count_cell("scene_elements", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // process_flows
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Motivational Model Subsystem\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #CFCCFC; text-align: left; padding: 6px;\">\n";
    $markup .= "    <strong>process_flows</strong>\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #CFCCFC; text-align: left;\">\n";
    include_once("process_flows.php");
    $process_flow_obj = new ProcessFlows($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $url = $this->url("process_flows/project/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\">";
    $markup .= $process_flow_obj->get_count_given_project_id($this->get_id(), $user_obj);
    $markup .= "  </a>\n";
    $markup .= "      </td>\n";

    $markup .= "    </tr>\n";
    $markup .= "    </table>\n";
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  private function get_child_structure_view() {
    $markup = "";

    // preprocess
    include_once("scene_elements.php");
    $scene_element_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $list = $scene_element_obj->list_child_given_project_id($this->get_id(), $user_obj);

    if ($list) {
      $markup .= "    <table class=\"plants\">\n";

      // heading
      $markup .= "    <tr>\n";
      $markup .= "      <td style=\"background-color: #EFEFEF;padding: 6px;\">\n";
      $markup .= "        child projects of this project\n";
      $markup .= "      </td>\n";
      $markup .= "      <td style=\"padding: 4px;\">\n";
      $markup .= $list;
      $markup .= "      </td>\n";
      $markup .= "    </tr>\n";
      $markup .= "    </table>\n";
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  private function get_parent_structure_view() {
    $markup = "";

    // todo enforce that a project can only be one scene_elements
    // todo enforce that a child has only one parent

    // preprocess
    include_once("scene_elements.php");
    $scene_element_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $list = $scene_element_obj->list_parent_given_project_id($this->get_id(), $user_obj);

    if ($list) {
      $markup .= "    <table class=\"plants\">\n";

      // heading
      $markup .= "    <tr>\n";
      $markup .= "      <td style=\"background-color: #EFEFEF;padding: 6px;\">\n";
      $markup .= "        parent project of this project\n";
      $markup .= "      </td>\n";
      $markup .= "      <td style=\"padding: 4px;\">\n";
      $markup .= $list;
      $markup .= "      </td>\n";
      $markup .= "    </tr>\n";
      $markup .= "    </table>\n";
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  public function print_scene_elements_elements($counter_obj, $output_name_flag) {
    $markup = "";

    // scene_elements elements
    $markup .= "    <p>SceneElements elements</p>";
    $markup .= "    <table class=\"plants\">\n";

    // plant_lists
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Plant List Subsystem\n";
    $markup .= "      </td>\n"; 
    $markup .= $counter_obj->output_count_cell("plant_lists_subsystem", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // locations
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Locations Subsystem\n";
    $markup .= "      </td>\n"; 
    $markup .= $counter_obj->output_count_cell("locations_subsystem", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // budgets
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Budgets Subsystem\n";
    $markup .= "      </td>\n"; 
    $markup .= $counter_obj->output_count_cell("budgets_subsystem", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // times subsystem
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Times Subsystem\n";
    $markup .= "      </td>\n"; 
    $markup .= $counter_obj->output_count_cell("times_subsystem", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // people subsystem
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        People Subsystem\n";
    $markup .= "      </td>\n"; 
    $markup .= $counter_obj->output_count_cell("people_subsystem", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    // domains subsystem
    $markup .= "    <tr>\n";
    $markup .= "      <td>\n";
    $markup .= "        Domains Subsystem\n";
    $markup .= "      </td>\n"; 
    $markup .= $counter_obj->output_count_cell("domains_subsystem", $this->get_id(), $output_name_flag);
    $markup .= "    </tr>\n";

    $markup .= "    </table>\n";

    return $markup;
  }

  // method
  public function print_radio_button_list($name) {
    $markup = "";

    // parameter "name" is the attribute value for the input element
    $type = "get_project_list";
    $sql = "SELECT projects.id, projects.name, projects.description, projects.user_name FROM projects WHERE projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY projects.sort, projects.name;";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $project) {
      $markup .= "<input type=\"radio\" name=\"" . $name . "\" value=\"" . $project->get_id() . "\" />" . $project->get_name() . "<br />\n";
    }

    return $markup;
  }

  // method
  public function load_data_given_id($given_id) {
    $markup = "";

    // set
    $this->set_given_id($given_id);

    // load data from database
    $this->determine_type("get_by_id");
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
 
      $markup .= "<div style=\"margin: 4px 50% 4px 0px; padding: 6px 6px 0px 6px; background-color: #E8C7FF;\">\n";
      $markup .= "<h4 style=\"margin: 4px 0px 0px 4px; padding: 4px 0px 6px 1px;\">Varieties</h4>\n";
      $markup .= $this->get_name_with_link();
      $markup .= "  </div>\n";
    }

    return $markup;
  }

  // method
  public function get_array_of_project_id($given_user_obj) {

    // special new project obj
    $project_obj = new Projects($this->get_given_config());

    // set
    $project_obj->set_user_obj($given_user_obj);

    // get data
    // hierarchy type will be get_all
    $project_obj->set_type("get_all_special");
    $project_obj->prepare_query();

    // initialize
    $project_id_array = array();

    if ($project_obj->get_list_bliss()->get_count() > 0) {
      foreach ($project_obj->get_list_bliss()->get_list() as $project) {
        array_push($project_id_array, $project->get_id());
      }
    }

    return $project_id_array;
  }

  // method
  public function get_name_with_link_given_project_id($given_id, $given_user_obj) {

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug projects given_id = " . $this->get_given_id() . "<br />\n";

    // get data
    $this->determine_type();
    $this->prepare_query();

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $project) {
        return $project->get_name_with_link();
      }
    } else {
      // debug
      //print "debug projects project row not found for given_project_id = " . $this->get_given_project_id() . "<br />\n";
    }

    // not found
    return "";
  }

  // method
  public function get_img_element_with_link_given_project_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // get data
    $this->determine_type();
    $this->prepare_query();

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $project) {
        $padding = "0px 0px 0px 0px;";
        $float = "";
        $width = "50"; 
        $url = $this->url("projects/" . $project->get_id());
        $markup .= "    <a href=\"" . $url . "\">";
        $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
        $markup .= "</a>";
        $markup .= "\n";
      }
    }

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    $markup .= "<strong>view</strong>: \n";
    $views = array("all", "hierarchy", "simple", "condensed", "default", "all_traditional_list", "offline", "printable");
    $last_pos = count($views) - 1;
    foreach ($views as $view) {
      $markup .= $this->get_menu_item($view, "view");
      if ($view != $views[$last_pos]) {
        $markup .= " | ";
      }
      $markup .= "\n";
    }

    return $markup;
  }

  // method
  private function output_subset() {
    $markup = "";

    $markup .= "<strong>subset</strong>: \n";
    $subsets = array("all", "zoneline", "offline");
    $last_pos = count($subsets) - 1;
    foreach ($subsets as $subset) {
      $markup .= $this->get_menu_item($subset, "subset");
      if ($subset != $subsets[$last_pos]) {
        $markup .= " | ";
      }
      $markup .= "\n";
    }

    return $markup;
  }

  // method
  public function output_sort() {
    $markup = "";

    $markup .= "<strong>sort</strong>: \n";
    $sorts = array("sort", "name", "status");
    $last_pos = count($sorts) - 1;
    foreach ($sorts as $sort) {
      // debug
      //print "debug projects sort = $sort<br />\n";
      $markup .= $this->get_menu_item($sort, "sort");
      if ($sort != $sorts[$last_pos]) {
        $markup .= " | ";
      }
      $markup .= "\n";
    }

    return $markup;
  }

  // method
  public function output_mode() {
    $markup = "";

    $markup .= "<strong>mode</strong>: \n";
    $modes = array("html5", "xml", "animated");
    $last_pos = count($modes) - 1;
    foreach ($modes as $mode) {
      // debug
      //print "debug projects mode = $mode<br />\n";
      $markup .= $this->get_menu_item($mode, "mode");
      if ($mode != $modes[$last_pos]) {
        $markup .= " | ";
      }
      $markup .= "\n";
    }

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // set defaults
    $this->set_given_view("default");
    $this->set_given_subset("zoneline");

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");

    // todo the repitition of lines below look ripe for refactoring
    // todo and why do it, because it is a place for variable name errors

    // view
    $parameter_a = new Parameter();
    $parameter_a->set_name("view");
    $parameter_a->set_validation_type_as_view();
    array_push($parameters, $parameter_a);

    // sort
    $parameter_b = new Parameter();
    $parameter_b->set_name("sort");
    $parameter_b->set_validation_type_as_sort();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // subset
    $parameter_d = new Parameter();
    $parameter_d->set_name("subset");
    $parameter_d->set_validation_type_as_subset();
    array_push($parameters, $parameter_d);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "projects";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $project_obj = new Projects($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $project_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function get_projects_given_tli($given_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_tli);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_table();
    }

    return $markup;
  }

  // method
  public function get_build_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no projects was found.</p>\n";;
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() > 1) {
      $markup .= "<p style=\"error\">too many projects were found: " . $this->get_list_bliss()->get_count() . "</p>\n";;
      return $markup;
    }

    // loop
    foreach ($this->get_list_bliss()->get_list() as $project) {

      // move user_obj along to other objects
      $user_obj = $this->get_user_obj();
      $project->set_user_obj($user_obj);

      if ($project->get_img_url()) {
        $padding = "0px 20px 20px 0px";
        $float = "float: left;";
        $width = "65";
        $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
      }

      // project name
      $markup .= "<h2 style=\"background-color: #FFFFFFF;padding: 2px 2px 2px 2px;\">" . $project->get_name() . "</h2>\n";

      $markup .= "<div class=\"paragraphs\">\n";

      // project description
      $markup .= "<div style=\"margin: 0px 18% 0px 0px;\">\n";
      $markup .= "<p><strong><em>description</em></strong></p>\n";
      $markup .= "<p>" . $project->get_description() . "</p>\n";
      $markup .= "</div><!-- end class paragraphs -->\n";

      // goal_statements
      include_once("goal_statements.php");
      $obj = new GoalStatements($this->get_given_config());
      $markup .= $obj->get_build_given_id($project->get_id(), $user_obj);

      // business_plan_texts
      include_once("business_plan_texts.php");
      $obj = new BusinessPlanTexts($this->get_given_config());
      $markup .= $obj->get_build_given_id($project->get_id(), $user_obj);

      // processes
      include_once("processes.php");
      $obj = new Processes($this->get_given_config());
      $markup .= $obj->get_build_given_id($project->get_id(), $user_obj);

      // child projects
      // 2 of 2: does this project have children projects
      //include_once("project_projects.php");
      //$project_project_obj1 = new ProjectProjects($this->get_given_config());
      //$project_projects1 = $project_project_obj1->get_dashboard_component($user_obj, $project->get_id());
      // output
      //if ($project_projects1) {
      //  $markup .= "<p><strong><em>child projects</em></strong></p>\n"; 
      //  $markup .= $project_projects1;
      //} else {
      //  $markup .= "<p><em>This project has no children.</em></p>\n";
      //}

      // metadata
      //$markup .= "<p><strong><em>metadata</em></strong></p>\n";
      // project id
      //$markup .= "<p><em>This is projects id " . $project->get_id() . "</em>.</p>\n";

      // project_projects
      //include_once("project_projects.php");
      // note: this has 2 parts
      // is this project a child of another project?
      //$project_project_obj2 = new ProjectProjects($this->get_given_config());
      //$parent_projects2 = $project_project_obj2->get_parent_if_any($user_obj, $project->get_id());
      //if ($parent_projects2) {
        //$markup .= "<p><strong><em>parent of this project</em></strong></p>\n"; 
        //$markup .= $parent_projects2;
      //} else {
        //$markup .= "<p><em>This project has no parent.</em></p>\n";
      //}

      // status
      //$markup .= "<p><strong><em>status</em></strong></p>\n";
      //$markup .= "<p>" . $project->get_status() . "</p>\n";

      // scene_elements
      //include_once("scene_elements.php");
      //$obj = new SceneElements($this->get_given_config());
      //$markup .= $obj->get_build_given_id($project->get_id(), $user_obj);

      // shifts
      // todo on hold because it outputting double rows
      // include_once("shifts.php");
      // $obj = new Shifts($this->get_given_config());
      // $user_obj = $project->get_user_obj();
      // $markup .= $obj->get_build_given_id($project->get_id(), $user_obj);

      // budgets
      // todo this is on hold because the postings need to be sorted out
      //include_once("budgets.php");
      //$obj = new Budgets($this->get_given_config());
      //$user_obj = $project->get_user_obj();
      //$markup .= $obj->get_build_given_id($project->get_id(), $user_obj);

      // lands
      // todo this is on hold because the list does not look correct
      //include_once("lands.php");
      //$obj = new Lands($this->get_given_config());
      //$user_obj = $project->get_user_obj();
      //$markup .= $obj->get_build_given_id($project->get_id(), $user_obj);

      // process_flows
      $markup .= "<br />\n";
      $markup .= "  <h3>process_flows of this project";
      $markup .= " (<em>id " . $project->get_id() . "</em>)";
      $markup .= "</h3>\n";
      $markup .= "  <p>\n";
      include_once("process_flows.php");
      $process_flows_obj = new ProcessFlows($this->get_given_config());
      $project_id = $project->get_id();
      $markup .= "    " . $process_flows_obj->get_table_given_project_id($project_id, $given_user_obj) . "\n";
      $markup .= "  </p>\n";
      $markup .= "<br />\n";
    }

    return $markup;
  }

  // method
  private function extract_number($given_cell) {
    // note if number already, returns it
    if (is_numeric($given_cell)) {
      return $given_cell;
    }
    // note: takes cell HTML and extracts chardata number
    if (preg_match("/>([0-9]*)</", $given_cell, $matches)) {
      // debug
      //print "debug projects given_cell " . $given_cell . "<br />";
      //print "debug projects Match was found <br />";
      //print "debug projects found " . $matches[0] . "<br />\n";
      //print "debug projects found " . $matches[1] . "<br />\n";
      return $matches[1];
    } else {
      // debug
      print "debug projects number not found in given_cell = " . $given_cell . "<br />\n";
    }
    return "";
  }

  // method
  public function get_img_url_given_project_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug projects given_id = " . $given_id . "<br />\n";

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // note: too few
      $markup .= "<p style=\"error\">no projects was found.</p>\n";;
      return $markup;
    } else if ($this->get_list_bliss()->get_count() > 1) {
      // note: too many
      $markup .= "<p style=\"error\">error: too many projects found.</p>\n";;
      return $markup;
    }

    // get object
    $project_obj = $this->get_list_bliss()->get_first_element();

    // return img_url
    return $project_obj->get_img_url();
  }

  // method
  public function get_name_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no projects was found.</p>\n";;
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $project) {
      $padding = "4px 4px 4px 4px;";
      $float = "";
      $width = "50"; 
      $url = $this->url("projects/" . $project->get_id());
      $markup .= "    <a href=\"" . $url . "\">";
      $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
    }

    return $markup;
  }

  // method
  public function get_project_img_url_as_element_with_link_given_tli($given_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_tli);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $project) {
        $padding = "0px 0px 0px 0px;";
        $float = "";
        $width = "50"; 
        $url = $this->url("projects/" . $project->get_id());
        $markup .= "    <a href=\"" . $url . "\">";
        $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
        $markup .= "</a>";
        $markup .= "\n";
      }
    }

    return $markup;
  }

  // method
  public function get_img_with_link_given_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $project) {
        $padding = "0px 0px 0px 0px;";
        $float = "";
        $width = "50"; 
        $url = $this->url("projects/" . $project->get_id());
        $markup .= "    <a href=\"" . $url . "\">";
        $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
        $markup .= "</a>";
        $markup .= "\n";
      }
    }

    return $markup;
  }

  // method
  public function get_img_with_link_given_given_scene_element_id($given_scene_element_id, $given_user_obj) {
    $markup = "";

    $this->set_given_scene_element_id($given_scene_element_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $project) {
        $padding = "0px 0px 0px 0px;";
        $float = "";
        $width = "50"; 
        $url = $this->url("projects/" . $project->get_id());
        $markup .= "    <a href=\"" . $url . "\">";
        $markup .= $project->get_img_url_as_img_element($padding, $float, $width);
        $markup .= "</a>";
        $markup .= "\n";
      }
    }

    return $markup;
  }

  // method
  public function get_img_url_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // return default image
      $markup .= "http://mudia.com/dash/_images/swept_ins_icon_small_blue.png";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) {

      // pre-process data
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= $obj->get_img_url($padding, $float, $width);
    }

    return $markup;
  }

  // method
  private function get_dynamic_status() {
    $markup = "";

    // todo this function is not yet implemented
    // todo this is a complex attribute (probably too complex)

    // output 2 of 2
    // status is derived

    $markup .= "<small>";

    // set up what to search
    // get a list of all the classes related to a project
    // $class_name => $value
    $names_of_classes_related_to_a_project = array(
      "projects" => "?",
      "business_plan_texts" => "?",
      "goal_statements" => "?",
    );

    // get derived value
    // now change status if there are business plans

    // display result
    $num = 0;
    foreach ($names_of_classes_related_to_a_project as $class_name => $value) {
      $num++;

      $markup .= "#" . $num . " ";
      $markup .= $class_name;
      $markup .= " => ";
      if ($class_name == "project") {
        $derived_string = "<em>\"";
        // status stored in database
        // allows the user to have some development control
        // this string is associated with the project
        // because the project is the topmost in the hierarchy
        $derived_string .= $this->status;
        $derived_string .= "\"</em>";
        // ready
        $names_of_classes_related_to_a_project[$class_name] = $derived_string;
        $markup .= $derived_string;
      } else if ($class_name == "business_plan") {
        // todo clean this up because it looks incorrect with that name
        $derived_string = "<em>";
        $user_obj = $this->get_user_obj();
        if ($user_obj) {
          include_once("business_plan_texts.php");
          $business_plan_texts_obj = new BusinessPlanTexts($this->get_given_config());
          $business_plan_texts_obj->set_user_obj($user_obj);
          $business_plan_texts_obj->set_given_project_id($this->get_id());
          $message = $business_plan_texts_obj->get_business_plan_texts_count();
          if ($message[0]) {
            // error
            $derived_string .= $message[1];
          } else {
            // if count is greather than zero, override status in database
            if ($message[1] > 0) {
              $derived_string .= "<em>";
              $derived_string .= "developing business plan";
              $derived_string .= "</em>";
            }
          }
          // ready
          $names_of_classes_related_to_a_project[$class_name] = $derived_string;
          $markup .= $derived_string;
        } else {
          $error_message = "Error " . get_class($this) . ": user is not known in <em>project</em> context.";
          print "Error: " . $error_message . "<br />\n";
          $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": user is not known in <em>project</em> context.");
        }
      } else if ($class_name == "goal_statement") {
          $derived_string = "NOT YET DEVELOPED";
          // ready
          $names_of_classes_related_to_a_project[$class_name] = $derived_string;
          $markup .= $derived_string;
      }
      $markup .= "<br />\n";
      // todo fix this so that all of the information is in a compact space
      if ($num == 1) {
        break;
      }
    }
    $markup .= "</small>\n";

    return $markup;
  }

  // method
  public function get_oldest_sort_object_from_db() {
    $markup = "";

    $this->set_type("get_oldest_sort_obj");

    $markup .= $this->prepare_query();

    if ($markup) {
      $markup .= "<p>error get_oldest_sort_object_from_db()</p>\n";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $project) {
      // debug
      //print "debug projects oldest obj name = " . $project->get_name() . "<br />\n";
      return $project;
    }
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT projects.id, projects.name FROM projects WHERE projects.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug projects: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "Projects ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function get_name_with_link_given_id($given_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    //if ($this->get_list_bliss()->get_count() < 1) {
    //  // return default image
    //  $markup .= "no projects found";
    //  return $markup;
    //}

    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= $obj->get_name_with_link() . "<br />\n";
    }

    return $markup;
  }

  // todo deal with a project having a scene_element...
  // todo is a project that has a scene_element that is a project.

  // method
  private function get_energy_flows_view() {
    $markup = "";

    $markup .= "    <h3>energy flows</h3>\n";
    $markup .= "    <table class=\"plants\">\n";

    // heading
    $markup .= "    <tr>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        #\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        class\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        energy_flows details\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        energy_flows dollars\n";
    $markup .= "      </td>\n";
    $markup .= "    </tr>\n";

    // define
    // todo move these to the usr configuration file
    $class_names = array("projects", "goal_statements", "business_plan_texts", "processes", "scene_elements", "process_flows", "usernames", "projects", "databases", "machines", "classes", "domains", "tools", "plant_lists", "webpages", "email_addresses", "suppliers", "applications", "moneymakers", "lands", "designs", "books");

    $num = 0;
    $total_dollars = 0;
    foreach ($class_names as $class_name) {
      $num++;

      $markup .= "    <tr>\n";
      $markup .= "      <td style=\"padding: 2px;\">\n";
      $markup .= "        " . $num . "\n";
      $markup .= "      </td>\n";
      $markup .= "      <td style=\"padding: 2px;\">\n";
      $markup .= "        " . $class_name . "\n";
      $markup .= "      </td>\n";
      $markup .= "      <td style=\"padding: 2px;\">\n";
      $user_obj = $this->get_user_obj();
      $markup .= "        " . $this->get_energy_flows_given_class("details", $class_name, $user_obj) . "\n";
      $markup .= "      </td>\n";
      $markup .= "      <td style=\"padding: 2px;\">\n";
      $user_obj = $this->get_user_obj();
      $dollars = $this->get_energy_flows_given_class("dollars", $class_name, $user_obj) . "\n";
      $total_dollars += $dollars;;
      $markup .= "        " . $dollars . "\n";
      $markup .= "      </td>\n";
      $markup .= "    </tr>\n";
    }

    // footer
    $markup .= "    <tr>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        \n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        \n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        total dollars (monthly)\n";
    $markup .= "      </td>\n";
    $markup .= "      <td style=\"background-color: #EFEFEF;padding: 2px;\">\n";
    $markup .= "        " . $total_dollars . "\n";
    $markup .= "      </td>\n";
    $markup .= "    </tr>\n";

    $markup .= "    </table>\n";

    return $markup;
  }

  // method
  private function get_energy_flows_given_class($given_type, $given_class_name, $given_user_obj) {
    $markup = "";

    // debug
    //$markup .= "debug project " . $this->get_id() . " energy_flow $given_class_name<br />\n";

    // make object
    include_once("lib/factory.php");
    $factory_obj = new Factory($this->get_given_config());
    $request_uri = $this->remove_parameter($_SERVER['REQUEST_URI']);
    $obj = $factory_obj->get_object_given_class_name($given_class_name, $request_uri);

    // output
    if ($obj) {
      $markup .= $obj->get_energy_flows_given_project_id($given_type, $this->get_id(), $given_user_obj);
    } else {
      $markup .= "Error projects: obj not found<br />\n";
    }
  
    return $markup;
  }

  // method
  public function get_database_string_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $project) {
      include_once("databases.php");
      $database_obj = new Databases($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $url = $this->url("databases/" . $project->get_database_id());
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= $database_obj->get_name_given_id($project->get_database_id(), $user_obj);
      $markup .= "</a>\n";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug processes: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error projects: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $project) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $project->get_id() . " " . $project->get_name_with_link() . " documentation " . $project->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $project->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error project: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "project: no processes found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

}
