<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.8 2017-06-17
// version 1.8 2017-07-10
// version 1.8 2017-11-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/maxonomies.php

include_once("lib/standard.php");

class Maxonomies extends Standard {

  // given
  private $given_webpage_id;
  private $given_group_name;
  private $given_domain_tli;

  // given_webpage_id
  public function set_given_webpage_id($var) {
    $this->given_webpage_id = $var;
  }
  public function get_given_webpage_id() {
    return $this->given_webpage_id;
  }

  // given_group_name
  public function set_given_group_name($var) {
    $this->given_group_name = $var;
  }
  public function get_given_group_name() {
    return $this->given_group_name;
  }

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // attributes
  private $user_name;
  private $categorization;
  private $group_name;
  private $how_to_measure;
  private $ocm;
  private $order_by;
  private $group_name_count;

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // categorization
  public function set_categorization($var) {
    $this->categorization = $var;
  }
  public function get_categorization() {
    return $this->categorization;
  }

  // group_name
  public function set_group_name($var) {
    $this->group_name = $var;
  }
  public function get_group_name() {
    return $this->group_name;
  }

  // how_to_measure
  public function set_how_to_measure($var) {
    $this->how_to_measure = $var;
  }
  public function get_how_to_measure() {
    return $this->how_to_measure;
  }

  // ocm
  public function set_ocm($var) {
    $this->ocm = $var;
  }
  public function get_ocm() {
    return $this->ocm;
  }

  // order_by
  public function set_order_by($var) {
    $this->order_by = $var;
  }
  public function get_order_by() {
    return $this->order_by;
  }

  // group_name_count
  public function set_group_name_count($var) {
    $this->group_name_count = $var;
  }
  public function get_group_name_count() {
    return $this->group_name_count;
  }

  // method
  public function get_img_url_as_img_element($padding = "", $float = "", $width = "", $height = "") {
    $markup = "";
    if (! $this->get_img_url()) {
      return "[img]";
    }
    $alt = "icon";
    // todo this was removed because the strings have HTML
    //if ($this->get_id()) {
    //  // todo add search-replace that removes any HTML elements from string
    //  $alt = "icon for " . $this->get_name();
    //}
    $markup .= "<img src=\"" . $this->get_img_url() . "\" alt=\"" . $alt . "\" width=\"" . $width . "\"";
    // add a border so that it stands out
    if ($this->get_order_by() && $this->get_categorization() == "problemography") {
      $markup .= " style=\"border: 1px solid #021a48; padding: 1px 1px 1px 1px;\"";
    }
    $markup .= " />";
    return $markup;
  }

  // method
  private function make_maxonomy() {
    $obj = new Maxonomies($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_webpage_id()) {
      $this->set_type("get_by_webpage_id");

    } else if ($this->get_given_group_name()) {
      $this->set_type("get_by_group_name");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT maxonomies.* FROM maxonomies WHERE maxonomies.id = " . $this->get_given_id() . " AND maxonomies.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug maxonomies: sql = $sql<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT maxonomies.* FROM maxonomies ORDER BY maxonomies.sort DESC, maxonomies.group_name, maxonomies.order_by, maxonomies.status, maxonomies.sort DESC;";

    } else if ($this->get_type() == "get_by_webpage_id") {
      // security: only get the rows owned by the user
      //$sql = "SELECT maxonomies.* FROM maxonomies, webpage_maxonomies WHERE maxonomies.id = webpage_maxonomies.maxonomy_id AND webpages.id = webpage_maxonomies.webpage_id AND webpages.id = " . $this->get_given_webpage_id() . " AND maxonomies.user_name = '" . $this->get_user_obj()->name . "' ORDER BY maxonomies.name;";
      $sql = "SELECT maxonomies.* FROM maxonomies, webpage_maxonomies WHERE maxonomies.id = webpage_maxonomies.maxonomy_id AND webpage_maxonomies.webpage_id = " . $this->get_given_webpage_id() . " ORDER BY maxonomies.sort DESC;";

    } else if ($this->get_type() == "get_by_group_name") {
      // security: only get the rows owned by the user
      // note filter gets only rows of the given group_name field
      $sql = "SELECT maxonomies.* FROM maxonomies WHERE maxonomies.group_name = '" . $this->get_given_group_name() . "' ORDER BY maxonomies.sort DESC, maxonomies.group_name, maxonomies.order_by, maxonomies.status, maxonomies.sort DESC;";

      // debug
      //print "debug maxonomies: sql = $sql<br />\n";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      // note filter gets only rows of the given group_name field
      $sql = "SELECT distinct(maxonomies.*) FROM maxonomies, webpage_maxonomies, webpages WHERE webpages.id = webpage_maxonomies.webpage_id AND webpage_maxonomies.maxonomy_id = maxonomies.id AND webpages.domain_tli = '" . $this->get_given_domain_tli() . "' ORDER BY maxonomies.sort DESC, maxonomies.order_by, maxonomies.id;";

      // debug
      //print "debug maxonomies: sql = $sql<br />\n";

    } else if ($this->get_type() == "get_group_names") {
      // security: only get the rows owned by the user
      // for group_name menu
      $sql = "SELECT maxonomies.group_name, count(maxonomies.id) FROM maxonomies GROUP BY maxonomies.group_name ORDER BY maxonomies.group_name;";

      // debug
      //print "debug maxonomies: sql = $sql<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_by_webpage_id" ||
        $this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_group_name" ||
        $this->get_type() == "get_by_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_maxonomy();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_img_url(pg_result($results, $lt, 2));
        $obj->set_status(pg_result($results, $lt, 3));
        $obj->set_user_name(pg_result($results, $lt, 4));
        $obj->set_sort(pg_result($results, $lt, 5));
        $obj->set_description(pg_result($results, $lt, 6));
        $obj->set_categorization(pg_result($results, $lt, 7));
        $obj->set_group_name(pg_result($results, $lt, 8));
        $obj->set_how_to_measure(pg_result($results, $lt, 9));
        $obj->set_ocm(pg_result($results, $lt, 10));
        $obj->set_order_by(pg_result($results, $lt, 11));
      }
    } else if ($this->get_type() == "get_group_names") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_maxonomy();
        $obj->set_group_name(pg_result($results, $lt, 0));
        $obj->set_group_name_count(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  public function output_view() {
    $markup = "";

    if ($this->get_given_group_name()) {
      $markup .= "group: " . $this->get_given_group_name() . "<br />\n";

      $markup .= "other groups: ";

      // todo this is a menu of the virtual group_name look_up table
      $maxonomy_obj = new Maxonomies($this->get_given_config());
      $maxonomy_obj->set_user_obj($this->get_user_obj());

      // load data from database
      $maxonomy_obj->set_type("get_group_names");
      $markup .= $maxonomy_obj->prepare_query();

      // only output if there are items to output
      foreach ($maxonomy_obj->get_list_bliss()->get_list() as $obj) {
        if ($obj->get_group_name()) {
          if ($obj->get_group_name() == $this->get_given_group_name()) {
            // skip
          } else {
            $url = $this->url("maxonomies?group-name=" . $obj->get_group_name());
            $markup .= "<a href=\"" . $url . "\">" . $obj->get_group_name() . "</a>&nbsp;(" . $obj->get_group_name_count() . ") ";
          }
        }
      }
      $markup .= "<br />\n";
      $markup .= "<br />\n";
    }

    // if given_domain_tli, display given_domain_tli
    if ($this->get_given_domain_tli()) {
      $markup .= "domain: ";
      $url = $this->url("domains/" . $this->get_given_domain_tli());
      $markup .= "<a href=\"" . $url . "\">" . $this->get_given_domain_tli() . "</a>";
      $markup .= "<br />\n";
    }
    
    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // consider what is missing
    $three_digits['000'] = "";
    $three_digits['001'] = "";
    $three_digits['002'] = "";
    $three_digits['003'] = "";
    $three_digits['004'] = "";
    $three_digits['005'] = "";
    $three_digits['006'] = "";
    $three_digits['007'] = "";
    $three_digits['008'] = "";
    $three_digits['009'] = "";
    $three_digits['010'] = "";
    $three_digits['011'] = "";
    $three_digits['012'] = "";
    $three_digits['013'] = "";
    $three_digits['014'] = "";

    // navigate to all
    // this might be in the wrong place in this file
    $url = $this->url("maxonomies/");
    $markup .= "<p>Widen View: <a href=\"" . $url . "\">All Maxonomies</a></p>\n";

    if ($this->get_type() == "get_by_webpage_id") {
       $markup .= "<p>only maxonomies associated with webpage id = " . $this->get_given_webpage_id() . "</p>\n";
    }

    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    order_by\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"colhead\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"colhead\">\n";
    //$markup .= "    group_name\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"colhead\">\n";
    //$markup .= "    categorization\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"colhead\">\n";
    //$markup .= "    kernel_theory; bibliography;\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"colhead\">\n";
    //$markup .= "    user_name\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"\">\n";
    $markup .= "    webpages\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $maxonomy) {

      $markup .= "<tr>\n";

      $num++;
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      // status
      // todo problem is that the font-size is a poor fix for big string issue
      $markup .= "  <td style=\"text-align: center; background-color: " . $maxonomy->get_status_background_color() . "; font-size: 50%;\">\n";
      $markup .= "    " . $maxonomy->get_status() . "\n";
      $markup .= "  </td>\n";

      // sort
      // todo clean up old sort
      //$markup .= "  <td class=\"mid\" align=\"left\">\n";
      //$markup .= "    " . $maxonomy->get_sort() . "<br />\n";
      //$markup .= "  </td>\n";
      // sort
      $markup .= $maxonomy->get_sort_cell();

      // order_by
      $markup .= "  <td class=\"mid\" align=\"center\">\n"; 
      if (array_key_exists($maxonomy->get_order_by(), $three_digits)) {
        $three_digits[$maxonomy->get_order_by()] = "found";
      }
      $markup .= "    " . $maxonomy->get_order_by() . "<br />\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td class=\"mid\">\n";
      $url = $this->url("maxonomies/" . $maxonomy->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $maxonomy->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      // img
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $padding = "";
      $float = "";
      $width = "36";
      $markup .= "    " . $maxonomy->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "    " . $maxonomy->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // group_name
      //$markup .= "  <td class=\"mid\" align=\"center\">\n";
      //$url = $this->url("maxonomies?group-name=" . $maxonomy->get_group_name());
      //$markup .= "    <a href=\"" . $url . "\">" . $maxonomy->get_group_name() . "</a>\n";
      //$markup .= "  </td>\n";

      // categorization
      //$markup .= "  <td class=\"mid\" align=\"center\">\n";
      //$markup .= "    " . $maxonomy->get_categorization() . "<br />\n";
      //$markup .= "  </td>\n";

      // user_name
      //$markup .= "  <td class=\"mid\" align=\"left\">\n";
      //$markup .= "    " . $maxonomy->get_user_name() . "\n";
      //$markup .= "  </td>\n";

      // webpages associated via webpage_maxonomies
      // count of maxonomy_manifests
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      include_once("webpages.php");
      $webpage_obj = new Webpages($this->get_given_config());
      $user_obj = $this->get_user_obj();
      if ($this->get_given_domain_tli()) {
        $markup .= $webpage_obj->get_list_given_maxonomy_id($maxonomy->get_id(), $user_obj, "zoneline") . "\n";
      } else {
        //$url = $this->url("webpage_maxonomies/maxonomy/" . $maxonomy->get_id());
        //$markup .= "  <a href=\"" . $url . "\">";
        $markup .= $webpage_obj->get_list_given_maxonomy_id($maxonomy->get_id(), $user_obj) . "\n";
        $markup .= "</a>\n";
      }
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    $markup .= "<p>Consider: ";
    foreach ($three_digits as $order_by => $status) {
      // only what was not found
      if ($status == "") {
        $markup .= $order_by . "\n";
      }
    }
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

      $markup .= "<table class=\"plants\">\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $maxonomy) {
        $num++;

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    #\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    id\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $maxonomy->get_id() . "<br />\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    status\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_status() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    sort\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_sort() . "<br />\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    img_url\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_img_url_as_img_element() . "<br />\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    name\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_name() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    description\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_description() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    group_name\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_group_name() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    categorization\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_categorization() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    how_to_measure\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_how_to_measure() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    ocm\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_ocm() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    order_by\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_order_by() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\">\n";
        $markup .= "    user_name\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $maxonomy->get_user_name() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\" style=\"background-color: green;\">\n";
        $markup .= "    <strong>webpages</strong>\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        include_once("webpages.php");
        $webpage_obj = new Webpages($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= "    " . $webpage_obj->get_list_with_name_given_maxonomy_id($maxonomy->get_id(), $user_obj) . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"colhead\" style=\"background-color: green;\">\n";
        $markup .= "    <strong>domains</strong>\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        include_once("domains.php");
        $domain_obj = new Domains($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= "    " . $domain_obj->get_webpages_given_maxonomy_id($maxonomy->get_id(), $user_obj) . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";
      $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function get_maxonomy_name_given_maxonomy_id($maxonomies_array, $maxonomy_id) {

    $maxonomy_name = "";

    // match
    foreach ($maxonomies_array as $maxonomy) {
      // debug
      //print "debug maxonomy: " . $maxonomy->get_id() . " " . $maxonomy->get_name() . " searching for = \"" . $maxonomy_id . "\"<br />\n";
      if ($maxonomy_id == $maxonomy->get_id()) {
        // debug
        //print "found<br />\n";
        $maxonomy_name = $maxonomy->get_name();
        break;
      }
    }
    return $maxonomy_name;
  }

  // method
  public function get_maxonomy_id_given_maxonomy_name($maxonomies_array, $maxonomy_name) {

    $maxonomy_id = "";

    // match
    foreach ($maxonomies_array as $maxonomy) {
      if ($maxonomy_name == $maxonomy->get_name()) {
        // debug
        //print "found<br />\n";
        $maxonomy_id = $maxonomy->get_id();
        break;
      }
    }
    return $maxonomy_id;
  }

  // special
  public function get_project_id() {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // group_name
    $parameter_d = new Parameter();
    $parameter_d->set_name("group-name");
    // todo not sure if this is the correct validator_function_string
    $parameter_d->set_validation_type_as_id();
    //$parameter_d->set_validator_function_string("filter");
    array_push($parameters, $parameter_d);

    // domain_tli
    $parameter_e = new Parameter();
    $parameter_e->set_name("domain_tli");
    $parameter_e->set_validation_type_as_domain_tli();
    array_push($parameters, $parameter_e);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        // debug
        //print "debug maxonomies parameter value = " . $parameter->get_name() . " " . $parameter->get_value() . "<br />\n";
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "view") {
            //$this->set_given_view($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $group_name = "maxonomies";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $maxonomies_obj = new Maxonomies($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $maxonomies_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($group_name, $db_table_field, $sort_letter, $database_name);

            // redirect_simple
            // todo make this a user option, this old way goes to single
            //$target_page = $parameter->get_value();
            // refresh
            $target_page = "";
            // debug
            //print "debug maxonomies target_page = " . $target_page . "<br />\n";
            $this->redirect_helper($target_page);
          }
          // 3
          if ($parameter->get_name() == "sort") {
            //$this->set_given_sort($parameter->get_value());
          }
          // 4
          if ($parameter->get_name() == "group-name") {
            $this->set_given_group_name($parameter->get_value());
            // debug
            //print "debug maxonomies given_group_name = " . $this->get_given_group_name() . "<br />\n";
          }
          // 5
          if ($parameter->get_name() == "domain_tli") {
            $this->set_given_domain_tli($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // method
  public function get_maxonomy_icon_with_link_given_maxonomy_id($given_maxonomy_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_maxonomy_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $maxonomy) {
        $padding = " 0px 0px 0px 0px";
        $float = "";
        $width = "20";
        $height = "20";
        $markup .= "    " . $maxonomy->get_img_as_img_element_with_link($padding, $float, $width, $height) . "\n";
      }
    }

    return $markup;
  }

}
