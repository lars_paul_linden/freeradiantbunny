<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/images.php

include_once("lib/scrubber.php");

class Images extends Scrubber {

  // attributes
  private $id;
  private $caption;
  private $description;
  private $photographer;
  private $url;
  private $license;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // caption
  public function set_caption($var) {
    $this->caption = $var;
  }
  public function get_caption() {
    return $this->caption;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // photographer
  public function set_photographer($var) {
    $this->photographer = $var;
  }
  public function get_photographer() {
    return $this->photographer;
  }

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // license
  public function set_license($var) {
    $this->license = $var;
  }
  public function get_license() {
    return $this->license;
  }

  // method
  private function make_image() {
    $obj = new Images($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    // add random functionality
    //} else if ($this->get_given_view()) {
    //  // todo set it up so that more than one image can be shown
    //  $this->set_type("get_random_images");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT images.id, images.caption, images.description, images.photographer, images.url, images.license FROM images WHERE images.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_random_images") {
      $sql = "SELECT images.id, images.caption, images.description, images.photographer, images.url, images.license FROM images ORDER BY random() LIMIT 2;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT images.id, images.caption, images.description, images.photographer, images.url, images.license FROM images ORDER by images.url;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->set_caption(pg_result($results, $lt, 1));
        $this->set_description(pg_result($results, $lt, 2));
        $this->set_photographer(pg_result($results, $lt, 3));
        $this->set_url(pg_result($results, $lt, 4));
        $this->set_license(pg_result($results, $lt, 5));
      }
    } else if ($this->get_type() == "get_random_images" ||
               $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $image = $this->make_image();
        $image->set_id(pg_result($results, $lt, 0));
        $image->set_caption(pg_result($results, $lt, 1));
        $image->set_description(pg_result($results, $lt, 2));
        $image->set_photographer(pg_result($results, $lt, 3));
        $image->set_url(pg_result($results, $lt, 4));
        $image->set_license(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu_get_all() {
    $markup = "";

    if ($this->get_type() == "get_all") {
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("plants");
      $markup .= "  See Also: <a href=\"" . $url . "\">Plants</a><br />\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    caption\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    # output for individual items

    // todo add paginator and other interface mechanisms
    foreach ($this->get_list_bliss()->get_list() as $image) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $url = $this->url("images/" . $image->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $image->get_id() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    <a href=\"" . $image->get_url() . "\">" . $image->get_url() . "</a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $image->get_caption() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $image) {
      $markup .= $image->output_image_html("show-caption");
    }

    return $markup;
  }

  // method
  public function output_image_html($caption_flag = "") {
    $markup = "";

    $given_id = $this->get_id();
    $this->set_given_id($given_id);

    $this->determine_type();
    $markup .= $this->prepare_query();

    // output html img element
    $markup .= "<img src=\"" . $this->get_url() . "\" alt=\"" . $this->get_description() . "\" />\n";

    // caption is optional
    if ($caption_flag == "show-caption") {
      $markup .= "<p class=\"caption\">";
      $markup .= $this->get_caption();
      $markup .= " ";
      $markup .= "<span style=\"font-size: 90%; color: #CCCCCC;\">";
      $markup .= "(photo by " . $this->get_photographer() . ")\n";
      $markup .= "</span>\n";
      $markup .= "</p>\n";
    }

    return $markup;
  }

  // method
  public function output_given_id($given_id) {
    $markup = "";

    $this->set_given_id($given_id);

    // based upon the parameter values, get data from database
    // and even if there were not any parameters at all
    // check to see if there is any data...
    // that needs to be loaded from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // main content
    $markup .= $this->output_data_set_table();

    return $markup;
  }

  // method
  public function output_random_images() {
    $markup = "";

    // todo consider this as a "sidecar"

    $this->set_type("get_random_images");

    // based upon the parameter values, get data from database
    // and even if there were not any parameters at all
    // check to see if there is any data...
    // that needs to be loaded from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    $markup .= $this->get_data_set_table();

    return $markup;
  }
}
