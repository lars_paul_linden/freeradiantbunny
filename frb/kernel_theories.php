<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.2 2015-01-05

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/kernel_theories.php

include_once("lib/standard.php");

class KernelTheories extends Standard {

  // method
  private function make_kernel_theory() {
    $obj = new KernelTheories($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // set order_by
    $order_by = " ORDER BY kernel_theories.sort DESC, kernel_theories.name";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT kernel_theories.* FROM kernel_theories;";
      
    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
        $sql = "SELECT kernel_theories.* FROM kernel_theories " . $order_by . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    // debug
    //print "debug goaL_statements type = " . $this->get_type() . "<br />\n";
    //print "debug goal_statements sql = " . $sql . "<br />\n";

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_goal_statement();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_status(pg_result($results, $lt, 3));
        $obj->set_img_url(pg_result($results, $lt, 4));
        $obj->set_description(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table($sidecar = "") {
    $markup = "";

    $markup .= "test";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "test";

    return $markup;
  }

}
