<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-11
// version 1.6 2017-02-02

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/blogposts.php

include_once("lib/scrubber.php");

class Blogposts extends Scrubber {

  // given
  private $given_domain_tli;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // attribute
  private $id;
  private $name;
  private $body;
  private $img_url;
  private $tags;
  private $url_alias;
  private $author;
  private $pubdate;
  private $webpage_obj;
  private $database_string;
  private $class_name_string;
  private $class_primary_key_string;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // body
  public function set_body($var) {
    $this->body = $var;
  }
  public function get_body() {
    return $this->body;
  }

  // img_url
  public function set_img_url($var) {
    $this->img_url = $var;
  }
  public function get_img_url() {
    return $this->img_url;
  }

  // tags
  public function set_tags($var) {
    $this->tags = $var;
  }
  public function get_tags() {
    return $this->tags;
  }

  // url_alias
  public function set_url_alias($var) {
    $this->url_alias = $var;
  }
  public function get_url_alias() {
    return $this->url_alias;
  }

  // author
  public function set_author($var) {
    $this->author = $var;
  }
  public function get_author() {
    return $this->author;
  }

  // pubdate
  public function set_pubdate($var) {
    $this->pubdate = $var;
  }
  public function get_pubdate() {
    return $this->pubdate;
  }

  // webpage_obj
  public function get_webpage_obj() {
    if (! isset($this->webpage_obj)) {
      include_once("webpages.php");
      $this->webpage_obj = new Webpages($this->get_given_config());
    }
    return $this->webpage_obj;
  }

  // database_string
  public function set_database_string($var) {
    $this->database_string = $var;
  }
  public function get_database_string() {
    return $this->database_string;
  }

  // class_name_string
  public function set_class_name_string($var) {
    $this->class_name_string = $var;
  }
  public function get_class_name_string() {
    return $this->class_name_string;
  }

  // class_primary_key_string
  public function set_class_primary_key_string($var) {
    $this->class_primary_key_string = $var;
  }
  public function get_class_primary_key_string() {
    return $this->class_primary_key_string;
  }

  // method
  private function make_blogpost() {
    $obj = new Blogposts($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_count_all_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT blogposts.* FROM blogposts WHERE blogposts.id = " . $this->get_given_id() . ";";
      
    } else if ($this->get_type() == "get_count_all_domain_tli") {
      $sql = "SELECT blogposts.* FROM blogposts, webpages, domains WHERE blogposts.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND domains.tli = '" . $this->get_given_domain_tli() . "' ORDER BY blogposts.sort DESC, blogposts.name, blogposts,id;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT blogposts.* from blogposts ORDER BY id DESC;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $database_name = "eatloca1_soiltoil";
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_count_all_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_blogpost();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_body(pg_result($results, $lt, 2));
        $obj->set_img_url(pg_result($results, $lt, 3));
        $obj->set_tags(pg_result($results, $lt, 4));
        $obj->set_url_alias(pg_result($results, $lt, 5));
        $obj->set_author(pg_result($results, $lt, 6));
        $obj->set_pubdate(pg_result($results, $lt, 7));
        $obj->get_webpage_obj()->set_id(pg_result($results, $lt, 8));
        $obj->set_database_string(pg_result($results, $lt, 9));
        $obj->set_class_name_string(pg_result($results, $lt, 10));
        $obj->set_class_primary_key_string(pg_result($results, $lt, 11));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 20px;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    img\n";
    $markup .= "    name\n";
    $markup .= "    blurb\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $blogpost) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $blogpost->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // img
      $background_color = "#0099CC";
      $markup .= "  <td style=\"background-color: " . $background_color . ";\">\n";

      $padding = " 0px 4px 4px 0px";
      $float = "left";
      $width = "";
      $link = $blogpost->get_url_alias();
      $markup .= $blogpost->get_img_as_img_element_with_given_link($link, $padding, $float, $width) . "\n";
      $name = $blogpost->get_name();
      $markup .= "<strong><a href=\"" . $link . "\">" . $blogpost->get_name() . "</a></strong>\n";
      $markup .= "<br />\n";
      $markup .= $blogpost->get_body() . "\n";

      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

    return $markup;
  }
  
  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  protected function output_subsubmenu() {
    // todo this function is only here to overcome error
  }

  // method
  protected function output_preface() {
    // todo this function is only here to overcome error
  }

  // method
  protected function get_id_with_link() {
    $markup = "";

    // todo this function is duplicated in lib/standard (refactor?)

    include_once("lib/factory.php");
    $factory = new Factory($this->get_given_config());
    $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_id()) {
      $markup .= $this->get_id();
    } else {
      // note: is this too dangerous or just a good fallback plan
      if ($this->get_given_id()) {
        $markup .= $this->get_given_id();
      } else {
        // no data was found, so return null string
        print "error blogposts: no id or given_id<br />\n";
        print "error blogposts: subclass = " . get_class($this) . "<br />\n";
        return "";
      }
    }
    $markup .= "</a>";

    return $markup;
  }

  // method
  public function get_img_as_img_element_with_given_link($given_link, $given_padding = "0px 0px 0px 0px", $float, $width, $height = "") {
    $markup = "";

    // todo this function is a adaptation of function in lib/standard

    include_once("lib/factory.php");
    $factory = new Factory($this->get_given_config());
    $markup .= "<a href=\"" . $given_link . "\">" . $this->get_img_url_as_img_element($given_padding, $float, $width, $height) . "</a>";

    return $markup;
  }

  // method
  public function get_img_url_as_img_element($given_padding = "0px 0px 0px 0px", $float = "", $width = "66", $height = "99") {
    $markup = "";

    // todo this function is duplicated in lib/standard (refactor?)

    // treat parameters
    if ($given_padding == "" ||
        $given_padding == "0") {
      $given_padding = "0px 0px 0px 0px";
    }

    if (! $this->get_img_url()) {
      return "[img]";
    }

    $alt = "icon";

    // output
    $markup .= "<img src=\"" . $this->get_img_url() . "\" alt=\"" . $alt . "\" width=\"" . $width . "\" height=\"" . $height . "\" style=\"padding: " . $given_padding . ";";
    if ($float) {
      $markup .= " float: " . $float . ";";
    }
    $markup .= "\"/>";

    return $markup;
  }

  // method
  public function get_given_project_id() {
    $markup = "";
    return $markup;
  }

  // method
  function get_count_blogposts($given_tli, $given_user_obj) {
      $count_blogposts = "unknown";

      // get total number of rows for this tli

      // load data from database
      $this->set_given_domain_tli($given_tli);
      $this->set_user_obj($given_user_obj);

      // load data from database
      $this->set_type("get_count_all_domain_tli");
      $markup = $this->prepare_query();

      // check for error message
      if ($markup) {
        print $markup;
      }

      // return row count
      return $this->get_list_bliss()->get_count();
  }

}
