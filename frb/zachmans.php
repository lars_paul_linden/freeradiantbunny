<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/zachmans.php

include_once("lib/scrubber.php");

class Zachmans extends Scrubber {

  // attributes
  private $id;
  private $name;
  private $description;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }

  // method
  private function make_zachman() {
    $obj = new Zachmans($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT zachmans.* FROM zachmans WHERE zachmans.id = " . $this->get_given_id() . ";";
      
    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT zachmans.* FROM zachmans ORDER BY zachmans.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    // debug
    //print "debug goaL_statements type = " . $this->get_type() . "<br />\n";
    //print "debug goal_statements sql = " . $sql . "<br />\n";

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_zachman();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_subsubmenu() {
    // note this is where to override function in parent class
  }

  // method
  protected function output_given_variables() {
    // note this is where to override function in parent class
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<p><em>This is a look-up table.</em></p>\n";
    //$markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "debug zachamans output_single()<br />\n";

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\" style=\"width: 10px;\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $sort_green_count = 0;
    $sort_red_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $zachman) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $zachman->get_id() . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= "    " . $zachman->get_name() . "\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $zachman->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";

     }

    $markup .= "</table>\n";

    return $markup;
  }

}
