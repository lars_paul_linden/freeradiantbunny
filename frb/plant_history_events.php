<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/plant_history_events.php

include_once("lib/scrubber.php");

class PlantHistoryEvents extends Scrubber {

  // given 
  private $given_project_id;
  private $given_plant_history_id;
  private $given_plant_list_id;
  private $given_plant_list_plant_id;
  
  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_plant_history_id;
  public function set_given_plant_history_id($var) {
    // todo figure out this validation
    //$error_message = $this->get_validation_obj()->validate_id($var, "plant_history_id");
    //if ($error_message) {
    //  $this->set_error_message($error_message);
    //} else {
      $this->given_plant_history_id = $var;
    //}
  }
  public function get_given_plant_history_id() {
    return $this->given_plant_history_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_plant_list_plant_id
  public function set_given_plant_list_plant_id($var) {
    $this->given_plant_list_plant_id = $var;
  }
  public function get_given_plant_list_plant_id() {
    return $this->given_plant_list_plant_id;
  }

  // attributes
  private $id;
  private $plant_history_obj;
  private $plant_count;
  private $direct_seed;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // plant_history_obj
  public function get_plant_history_obj() {
    if (! isset($this->plant_history_obj)) {
      include_once("plant_histories.php");
      $this->plant_history_obj = new PlantHistories($this->get_given_config());
    }
    return $this->plant_history_obj;
  }

  // plant_count
  public function set_plant_count($var) {
    $this->plant_count = $var;
  }
  public function get_plant_count() {
    return $this->plant_count;
  }

  // direct_seed
  public function set_direct_seed($var) {
    $this->direct_seed = $var;
  }
  public function get_direct_seed() {
    return $this->direct_seed;
  }

  public function get_direct_seed_user_facing() {
    if ($this->direct_seed == 't') {
      return "direct seed";
    }
    return "greenhouse/transplant";
  }

  // derived
  private $count;
 
  // count
  public function set_count($var) {
    $this->count = $var;
  }
  public function get_count() {
    return $this->count;
  }

  // method
  private function make_plant_history_event() {
    $obj = new PlantHistoryEvents($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  public function make_plant_list() {
    include_once("plant_lists.php");
    $obj = new PlantLists($this->get_given_config());
    $this->get_list_bliss()->add_item($obj);
    return $obj ;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_history_id()) {
      $this->set_type("get_by_plant_history_id");

    } else if ($this->get_given_plant_list_id()) {
      $this->set_type("get_by_plant_list_id");

    } else if ($this->get_given_plant_list_plant_id()) {
      $this->set_type("get_by_plant_list_plant_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT plant_history_events.id, plant_history_events.plant_history_id, plant_history_events.plant_count, plant_history_events.direct_seed FROM plant_history_events WHERE plant_history_events.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_by_plant_history_id") {
      $sql = "SELECT plant_history_events.id, plant_history_events.plant_history_id, plant_history_events.plant_count, plant_history_events.direct_seed, plants.id, plants.common_name, varieties.id, varieties.name, suppliers.id, suppliers.name, plant_histories.plant_list_plant_id FROM plant_history_events, plant_histories, plants, varieties, plant_list_plants, seed_packets, suppliers WHERE plant_histories.plant_list_plant_id = plant_list_plants.id AND plants.id = plant_list_plants.plant_id AND varieties.id = seed_packets.variety_id AND seed_packets.id = plant_histories.seed_packet_id AND plant_histories.id = plant_history_events.plant_history_id AND plant_history_events.plant_history_id = " . $this->get_given_plant_history_id() . " AND suppliers.id = seed_packets.supplier_id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_plant_list_id") {
      $sql = "SELECT plant_history_events.id, plant_history_events.plant_history_id, plant_history_events.plant_count, plant_history_events.direct_seed, plants.id, plants.common_name, varieties.id, varieties.name, suppliers.id, suppliers.name FROM plants, plant_history_events, plant_histories, plant_list_plants, plant_lists, varieties, seed_packets, suppliers WHERE plant_lists.id = " . $this->get_given_plant_list_id() . " AND plant_lists.id = plant_list_plants.plant_list_id AND plant_list_plants.id = plant_histories.plant_list_plant_id ANd plant_histories.id = plant_history_events.plant_history_id AND plants.id = plant_list_plants.plant_id AND varieties.id = seed_packets.variety_id AND seed_packets.id = plant_histories.seed_packet_id AND suppliers.id = seed_packets.supplier_id ORDER BY plant_history_events.plant_count;";

    } else if ($this->get_type() == "get_by_plant_list_plant_id") {
      $sql = "SELECT plant_history_events.* FROM plant_history_events, plant_histories WHERE plant_histories.plant_list_plant_id = " . $this->get_given_plant_list_plant_id() . " AND plant_histories.id = plant_history_events.plant_history_id ORDER BY plant_history_events.id;";

      // debug
      //print "debug plant_history_events sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_project_id") {
      // a project has plant_lists, so this is a precursor because a user must select a plant list
      $sql = "SELECT plant_lists.id, plant_lists.name FROM plant_lists, plant_list_projects WHERE plant_lists.id = plant_list_projects.plant_list_id AND plant_list_projects.project_id = " . $this->get_given_project_id() . " ORDER BY plant_lists.sort;";

    } else if ($this->get_type() == "get_count_given_plant_history_id") {
      $sql = "SELECT count(plant_history_events.id) FROM plant_history_events WHERE plant_history_events.plant_history_id = " . $this->get_given_plant_history_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT plant_history_events.* FROM plant_history_events ORDER BY plant_history_events.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_plant_history_id" ||
        $this->get_type() == "get_by_plant_list_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant_history_event = $this->make_plant_history_event();
        $plant_history_event->set_id(pg_result($results, $lt, 0));
        $plant_history_event->get_plant_history_obj()->set_id(pg_result($results, $lt, 1));
        $plant_history_event->set_plant_count(pg_result($results, $lt, 2));  
        $plant_history_event->set_direct_seed(pg_result($results, $lt, 3));
        $plant_history_event->get_plant_history_obj()->get_plant_list_plant_obj()->get_plant_obj()->set_id(pg_result($results, $lt, 4));
        $plant_history_event->get_plant_history_obj()->get_plant_list_plant_obj()->get_plant_obj()->set_common_name(pg_result($results, $lt, 5));
        $plant_history_event->get_plant_history_obj()->get_seed_packet_obj()->get_variety_obj()->set_id(pg_result($results, $lt, 6));
        $plant_history_event->get_plant_history_obj()->get_seed_packet_obj()->get_variety_obj()->set_name(pg_result($results, $lt, 7));
        $plant_history_event->get_plant_history_obj()->get_seed_packet_obj()->get_supplier_obj()->set_id(pg_result($results, $lt, 8));
        $plant_history_event->get_plant_history_obj()->get_seed_packet_obj()->get_supplier_obj()->set_name(pg_result($results, $lt, 9));
        if ($this->get_type() == "get_by_plant_history_id") {
          $plant_history_event->get_plant_history_obj()->get_plant_list_plant_obj()->set_id(pg_result($results, $lt, 10));
        }
      }
    } else if ($this->get_type() == "get_count_given_plant_history_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->get_list_bliss()->add_item($this);
        $this->set_count(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_by_plant_list_plant_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->get_list_bliss()->add_item($this);
        $this->set_id(pg_result($results, $lt, 0));
        $this->get_plant_history_obj()->set_id(pg_result($results, $lt, 1));
        $this->set_plant_count(pg_result($results, $lt, 2));
        $this->set_direct_seed(pg_result($results, $lt, 3));
      }
    } else if ($this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_plant_list();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->get_list_bliss()->add_item($this);
        $this->set_id(pg_result($results, $lt, 0));
        $this->get_plant_history_obj()->set_id(pg_result($results, $lt, 1));
        $this->set_plant_count(pg_result($results, $lt, 2));  
        $this->set_direct_seed(pg_result($results, $lt, 3));  
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $plant_history_event = $this->make_plant_history_event();
        $plant_history_event->set_id(pg_result($results, $lt, 0));
        $plant_history_event->get_plant_history_obj()->set_id(pg_result($results, $lt, 1));
        $plant_history_event->set_plant_count(pg_result($results, $lt, 2));  
        $plant_history_event->set_direct_seed(pg_result($results, $lt, 3));  
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    // todo figure out this subsubmenu
    //$markup .= "<strong>See Also</strong>: \n";

    // plant_list
    if ($this->get_given_plant_list_id() ||
      $this->get_given_plant_history_id()) {

      // get plant_list_id for the links
      $plant_list_id = $this->get_given_plant_list_id();
      if (! $plant_list_id) {
        include_once("plant_histories.php");
        $plant_history_obj = new PlantHistories($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $plant_history_id = $this->get_given_plant_history_id();
        $plant_list_id = $plant_history_obj->get_plant_list_id_given_id($plant_history_id, $user_obj);
      }

      // plant_lists
      $url = $this->url("plant_lists/" . $plant_list_id);
      $markup .= "<a href=\"" . $url . "\">Plant List " . $plant_list_id . "</a>\n";
      $markup .= " | ";
      // plant_histories
      $url = $this->url("plant_histories/plant_list/" . $plant_list_id);
      $markup .= "<a href=\"" . $url . "\">Plant Histories of this Plant List </a>\n";
      $markup .= " | ";
      // visits
      $url = $this->url("visits/plant_list/" . $plant_list_id);
      $markup .= "<a href=\"" . $url . "\">Visits for this Plant List</a><br />\n";
    }
    if ($this->get_type() == "get_all") {
      $url = $this->url("plant_histories");
      $markup .= "  See Also: <a href=\"" . $url . "\">PlantHistories</a><br />\n";
    }
    $markup .= "<br />";

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // todo address this
    //$markup .= $this->output_user_info_message();

    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_type() == "get_by_project_id") {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "To list a set of <strong>Plant History Events</strong>, click on a <strong>plant_list</strong> name.";
      $markup .= "</div>\n";
    }
    if ($this->get_type() == "get_by_plant_list_id") {
      $markup .= "<div class=\"given-variables\">\n";
      include_once("plant_lists.php");
      $plant_lists_obj = new PlantLists($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_lists_obj->set_user_obj($user_obj);
      $given_plant_list_id = $this->get_given_plant_list_id();
      $markup .= $plant_lists_obj->output_plant_list_name_given_id($given_plant_list_id);
      $markup .= "</div>\n";
    }
    if ($this->get_type() == "get_by_plant_history_id") {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "These are the plant_history events of plant_history_id = " . $this->get_given_plant_history_id() . ".\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    if ($this->get_type() == "get_by_project_id") {
      // this lists helps the user select a particular list
      include_once("plant_lists.php");
      $plant_list_obj = new PlantLists($this->get_given_config());
      $list_obj = $this->get_list_bliss()->get_list();
      $type_url = "plant_history_events";
      $type_string = "Plant History Events";
      $markup .= $plant_list_obj->output_plant_lists_table($type_url, $type_string, $list_obj);
      return $markup;
    } else {
      $markup .= $this->output_table();
    }

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // preprocess 1 of 2
    // initialize this array for an error report
    // the report shows plant_histories with no plant_history_events
    $error_report_obj_list = new ListBliss();

    // preprocess 2 of 2
    // helps to remove plant_history_id when the same as above row
    $flag_plant_history_id = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";

    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_history_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_list_plant_id\n"; 
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n"; 
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    variety\n"; 
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n"; 
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    $num_inside = 0;
    foreach ($this->get_list_bliss()->get_list() as $plant_history_event) {
      $num++;

      // save id for error report
      $plant_history_id = $plant_history_event->get_plant_history_obj()->get_id();
      $error_report_obj_list->add_item($plant_history_id);

      $markup .= "<tr>\n";

      // num
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // plant_history_events id
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_history_event->get_id() . "\n";
      $markup .= "  </td>\n";

      // plant_histories.id
      $markup .= "  <td align=\"center\">\n";
      //if ($flag_plant_history_id == $plant_history_event->get_plant_history_obj()->get_id()) {
      //  // duplicate, so skip
      //} else {
      //
      //}
      $markup .= "    " . $plant_history_event->get_plant_history_obj()->get_id() . "\n";
      // todo figure out if the following can be removed
      // set for the next iteration
      //$flag_plant_history_id = $plant_history_event->get_plant_history_obj()->get_id();
      $markup .= "  </td>\n";

      // plant_count
      $markup .= "  <td>\n";
      $markup .= "    " . $plant_history_event->get_plant_count() . "\n";
      $markup .= "  </td>\n";

      // plant_list_plants.id
      $markup .= "  <td align=\"center\">\n";
      //$markup .= "    " . $plant_history_event->get_plant_history_obj()->get_plant_list_plant_obj()->get_id() . "\n";
      $markup .= "  </td>\n";

      // plant
      $markup .= "  <td>\n";
      //$markup .= "    " . $plant_history_event->get_plant_history_obj()->get_plant_list_plant_obj()->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // variety
      $markup .= "  <td>\n";
      //$markup .= "    " . $plant_history_event->get_plant_history_obj()->get_seed_packet_obj()->get_variety_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // supplier
      $markup .= "  <td>\n";
      //$markup .= "    " . $plant_history_event->get_plant_history_obj()->get_seed_packet_obj()->get_supplier_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // show error report
    $plant_list_id = $this->get_given_plant_list_id();
    if ($plant_list_id) {
      include_once("lib/errors.php");
      $errors_obj = new Errors();
      $class = get_class($this);
      $user_obj = $this->get_user_obj();
      $markup .= $errors_obj->output_missing_error_report($error_report_obj_list, $num_inside, $class, $plant_list_id, $user_obj);
    }

    return $markup;
  }

  // method
  public function output_list() {
    $markup = "";

    // todo fix this function
    // set
    //$this->set_given_plant_id($given_plant_id);
    //$this->set_type("get_variety_given_plant_id");

    // load data from database
    //$this->determine_type();
    //$markup .= $this->prepare_query();

    // only output if there are items to output 
    //if ($this->get_list_bliss()->get_count() > 0) {
    //  $markup .= $this->output_set();
    //}

    return $markup;
  }

  // method
  public function output_set() {
    $markup = "";

    $markup .= "not yet implemented\n";

    return $markup;
  }

  // method
  public function output_table_given_plant_history_id($given_plant_history_id) {
    $markup = "";

    // set
    $this->set_given_plant_history_id($given_plant_history_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output 
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_list_table();
    }

    return $markup;
  }

  // method
  public function get_count_given_plant_history_id($given_user_obj, $given_plant_history_id) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);
    $this->set_given_plant_history_id($given_plant_history_id);

    // load data from database
    $this->set_type("get_count_given_plant_history_id");
    $markup .= $this->prepare_query();

    // only output if there are items to output 
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $plant_history_event) {
        $markup .= $plant_history_event->get_count();
       }
    }

    return $markup;
  }

  // method
  public function output_list_bliss_given_plant_history_id($given_plant_history_id) {
    $markup = "";

    // set
    $this->set_given_plant_history_id($given_plant_history_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output 
    if ($this->get_list_bliss()->get_count() > 0) {
      return $this->get_list_bliss();
    }

    return $markup;
  }

  // method
  public function get_pure_list($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // kludge so that the markup from the prepare_query() function is not lost
    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    // return the array
    $list_obj = $this->get_list_bliss()->get_list();
    return $list_obj;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function output_plant_history_event_count_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    $count = $this->get_list_bliss()->get_count();

    $markup .= $count;

    return $markup;
  }

  // method
  public function output_plant_history_event_count_given_plant_list_plant_id($given_plant_list_plant_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_plant_id($given_plant_list_plant_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    $markup .= $this->get_list_bliss()->get_count();

    return $markup;
  }

}
