<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/factory.php

class Factory {

  private $given_config;

  // given_config
  public function set_given_config($var) {
    $this->given_config = $var;
  }
  public function get_given_config() {
    return $this->given_config;
  }

  // method
  function __construct($given_config) {
    $this->set_given_config($given_config);
    if ($this->get_given_config()->get_debug()) {
      // debug
      //print "debug " . get_class($this) . " constructing " . get_class($this) . "<br />\n";
    }
  }

  // method
  public function get_object_with_just_given_class_name($given_class_name) {
    include_once("freeradiantbunny/frb/" . $given_class_name . ".php");
    $object_name = $this->get_object_name_given_class_name($given_class_name);
    $obj = new $object_name($this->get_given_config());
    return $obj;
  }

  // method
  public function get_object_given_class_name($given_class_name, $given_request_uri) {
    // todo move this to the config file
    include_once("freeradiantbunny/frb/" . $given_class_name . ".php");
    $object_name = $this->get_object_name_given_class_name($given_class_name);
    // debug
    //print "debug factory: object_name = " . $object_name . "<br />\n";
    $obj = new $object_name($this->get_given_config());
    // pass along parameters query string
    $query_string = "";
    if (strpos($given_request_uri, "?")) {
      $query_string = substr($given_request_uri, strpos($given_request_uri, "?") + 1);
      $obj->set_given_query_string($query_string);
    }
    return $obj;
  }

  // method
  public function get_class_name_given_object($given_obj) {

    // todo make more efficient by converting once and storing the value

    // about this function 
    // changes camalcase object_name to a class_name ready for filename or url
    // for example, given "GoalStatements" return "goal_statements"

    // get name of the class
    $object_name = get_class($given_obj);

    // debug
    //print "debug factory: object_name = " . $object_name . "<br />\n";

    // note some class names need underscores between the words
    // these underscores go before all uppercase letters, except first letter
    $class_name = lcfirst($object_name);

    // note add underscores
    $pattern = '/[A-Z]/';
    $match_result = preg_match_all($pattern, $class_name, $matches, PREG_OFFSET_CAPTURE );

    // matches variable holds an array of arrays
    if ($match_result) {

      // note need a counter for the position
      $count = -1;
      foreach ($matches as $match_sub_array) {
        foreach ($match_sub_array as $match) {

          // note as underscores are added, pos needs to be adjusted too
          $count++;
          $pos = $match[1] + $count;

          // debug
          //print "debug factory class_standard [letter] = " . $match[0] . "<br />\n";
          //print "debug factory class_standard [pos] = " . $match[1] . "<br />\n";

          // note add underscore
          $class_name = substr_replace($class_name, "_", $pos, 0);
        }
      }
    }

    // note finally, make all lower
    $class_name = strtolower($class_name);

    // debug
    //print "debug factory: class_name = " . $class_name . "<br />\n";

    return $class_name;
  }

  // method
  public function get_object_name_given_class_name($given_class_name) {
    $given_class_name[0] = strtoupper($given_class_name[0]);
    // is there an underscore?
    $func = create_function('$c', 'return strtoupper($c[1]);');
    return preg_replace_callback('/_([a-z])/', $func, $given_class_name);
  }

}
