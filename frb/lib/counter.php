<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this file
// http://freeradiantbunny.org/main/en/docs/frb/lib/counter.php

include_once("scrubber.php");

class Counter extends Scrubber {

  // given
  private $given_project_id;
  private $given_plant_list_id;
  private $given_plant_id;
  private $given_pickup_id;
  private $given_harvest_id;
  private $given_field_name;
  private $given_process_id;

  // variable
  private $counts = array();  // class_name+field_name => count given project_id

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_plant_id
  public function set_given_plant_id($var) {
    $this->given_plant_id = $var;
  }
  public function get_given_plant_id() {
    return $this->given_plant_id;
  }

  // given_pickup_id
  public function set_given_pickup_id($var) {
    $this->given_pickup_id = $var;
  }
  public function get_given_pickup_id() {
    return $this->given_pickup_id;
  }

  // given_harvest_id
  public function set_given_harvest_id($var) {
    $this->given_harvest_id = $var;
  }
  public function get_given_harvest_id() {
    return $this->given_harvest_id;
  }

  // given_field_name
  public function set_given_field_name($var) {
    $this->given_field_name = $var;
  }
  public function get_given_field_name() {
    return $this->given_field_name;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // count
  public function add_count($class_name, $counts, $field_name = "") {
    // add key value pair to array
    $key = $class_name . $field_name;
    $this->counts[$key] = $counts;
  }
  public function get_counts() {
    return $this->counts;
  }

  // method
  protected function process_command() {
    // todo no commands yet
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // no parameters

    return $markup;
  }

  // method
  protected function determine_type() {
  }

  // method
  public function prepare_query() {
    $markup = "";

    $sql = "";  // initialize

    // figure out what to load
    if ($this->get_type() == "goal_statements") {
      $sql = "SELECT count(*) FROM goal_statements, projects WHERE goal_statements.project_id = " . $this->get_given_project_id() . " AND goal_statements.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "usernames") {
      $sql = "SELECT count(*) FROM usernames, projects WHERE projects.id= " . $this->get_given_project_id() . " AND usernames.username = projects.user_name AND projects.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "process_flows") {
      $sql = "SELECT SUM(totalcount) AS totalcount FROM (SELECT count(*) AS totalcount FROM process_flows, processes, business_plan_texts, goal_statements, projects WHERE process_flows.parent_process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' UNION ALL SELECT count(*) AS totalcount FROM process_flows, processes, business_plan_texts, goal_statements, projects WHERE process_flows.child_process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "') AS totalcount;";

    } else if ($this->get_type() == "business_plan_texts") {
      $sql = "SELECT count(*) FROM business_plan_texts, goal_statements, projects WHERE business_plan_texts.goal_statement_id  = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "shifts") {
      $sql = "SELECT count(DISTINCT shifts.id) FROM shifts, processes, business_plan_texts, goal_statements, projects, process_profiles WHERE shifts.process_profile_id = process_profiles.id AND process_profiles.id = processes.profile_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "process_profiles") {
      $sql = "SELECT count(DISTINCT process_profiles.id) FROM processes, business_plan_texts, goal_statements, projects, process_profiles WHERE process_profiles.id = processes.profile_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "domains") {
      // todo debug the following sql
      $sql = "SELECT count(*) FROM domains, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'domains' AND domains.tli = scene_elements.class_primary_key_string AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";
      // todo remove debug sql statement below
      //$sql = "SELECT count(*) FROM domains;";

    } else if ($this->get_type() == "designers") {
      $sql = "SELECT count(*) FROM designers, projects WHERE designers.project_id = " . $this->get_given_project_id() . " AND designers.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "webpages") {
      // does not work because it crosses 2 databases
      $sql = "SELECT count(*) FROM webpages, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'webpages' AND webpages.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "scene_elements") {
      $sql = "SELECT count(*) FROM scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "tickets") {

      if ($this->get_given_process_id()) {
        // todo this looks too complicated
        //$sql = "SELECT count(*) FROM tickets, processes, business_plan_texts, goal_statements, projects  WHERE tickets.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND processes.id = " . $this->get_given_process_id() . ";";
        $sql = "SELECT count(*) FROM tickets, processes WHERE tickets.process_id = processes.id AND processes.id = " . $this->get_given_process_id() . ";";
      } else {
        // todo something is not correct
        $markup .= "tickets but no process_id\n";
      }

    } else if ($this->get_type() == "tools") {
      $sql = "SELECT count(*) FROM tools s WHERE s.id IN (SELECT DISTINCT ON (tools.id) tools.id FROM tools, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'tools' AND tools.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ");";

    } else if ($this->get_type() == "projects") {
      $sql = "SELECT count(*) FROM projects s WHERE s.id IN (SELECT DISTINCT ON (pse.id) pse.id FROM projects pse, scene_elements, processes, business_plan_texts, goal_statements, projects pp WHERE scene_elements.class_name_string = 'projects' AND pse.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = pp.id AND pp.id = " . $this->get_given_project_id() . ");";

    } else if ($this->get_type() == "timecards") {
      $sql = "SELECT count(*) FROM timecards WHERE timecards.project_id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "suppliers") {
      $sql = "SELECT count(*) FROM suppliers s WHERE s.id IN (SELECT DISTINCT ON (suppliers.id) suppliers.id FROM suppliers, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'suppliers' AND suppliers.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ");";

    } else if ($this->get_type() == "documentations") {
      $sql = "SELECT count(*) FROM project_documentations, processes, business_plan_texts, goal_statements, projects WHERE project_documentations.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "books") {
      $sql = "SELECT count(*) FROM books l WHERE l.id IN (SELECT DISTINCT ON (books.id) books.id FROM books, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'books' AND books.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ");";

    } else if ($this->get_type() == "tags") {
      $sql = "SELECT count(*) FROM tags, projects WHERE tags.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "pickups") {
      if ($this->get_given_harvest_id()) {
        $sql = "SELECT count(*) FROM pickups WHERE pickups.harvest_id = " . $this->get_given_harvest_id() . ";";
      } 
      // todo was producing errors so commented out
      //if ($this->get_given_project_id()) {
      //  $sql = "SELECT count(*) FROM pickups, harvests, projects WHERE pickups.harvest_id = harvests.id AND harvests.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";
      //}

    } else if ($this->get_type() == "accounts") {
      $sql = "SELECT count(*) FROM accounts l WHERE l.id IN (SELECT DISTINCT ON (accounts.id) accounts.id FROM accounts, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'accounts' AND accounts.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ");";

    } else if ($this->get_type() == "lands") {
      $sql = "SELECT count(*) FROM lands l WHERE l.id IN (SELECT DISTINCT ON (lands.id) lands.id FROM lands, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'lands' AND lands.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ");";

    } else if ($this->get_type() == "budgets") {
      $sql = "SELECT count(*) FROM budgets, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.id = budgets.scene_element_id AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "pickup_plants") {
      $sql = "SELECT count(*) FROM pickup_plants WHERE pickup_plants.pickup_id = " . $this->get_given_pickup_id() . ";";

    } else if ($this->get_type() == "shares") {
      if ($this->get_given_harvest_id()) {
        $sql = "SELECT count(*) FROM shares WHERE shares.harvest_id = " . $this->get_given_harvest_id() . ";";
      } 
      if ($this->get_given_project_id()) {
        $sql = "SELECT count(*) FROM shares, harvests, projects WHERE projects.id = " . $this->get_given_project_id() . " AND projects.id = harvests.project_id AND harvests.id = shares.harvest_id;";
      }

    } else if ($this->get_type() == "harvests") {
      $sql = "SELECT count(*) FROM harvests WHERE harvests.project_id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "plant_histories") {
      if ($this->get_given_plant_list_id()) {
        // todo this is commented out until I get time to fix it properly
        //$sql = "SELECT count(*) FROM plant_list_plants, plant_histories WHERE plant_list_plants.plant_list_id = " . $this->get_given_plant_list_id(). " AND plant_list_plants.plant_id = " . $this->get_given_plant_id() . " AND plant_histories.plant_list_plant_id = plant_list_plants.id;";
        // todo this is temporary SQL (incorrect, too)
        $sql = "SELECT count(*) FROM plant_histories;";
      } else {
        $sql = "SELECT count(*) FROM plant_list_plants, plant_histories WHERE plant_list_plants.plant_id = " . $this->get_given_plant_id() . " AND plant_histories.plant_list_plant_id = plant_list_plants.id;";
      }

    } else if ($this->get_type() == "plant_history_events") {
      $sql = "";

    } else if ($this->get_type() == "plant_lists") {
      $sql = "SELECT count(*) FROM plant_lists l WHERE l.id IN (SELECT DISTINCT ON (plant_lists.id) plant_lists.id FROM plant_lists, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'plant_lists' AND plant_lists.id = cast(scene_elements.class_primary_key_string as integer) AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ");";

    } else if ($this->get_type() == "designs") {
      $sql = "SELECT count(*) FROM designs;";

    } else if ($this->get_type() == "design_instances") {
      // the following is a backup
      //$sql = "SELECT count(*) FROM design_instances, designs, processes WHERE designs.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND design_instances.design_id = designs.id;";
      $sql = "SELECT count(*) FROM design_instances;";

    } else if ($this->get_type() == "processesresponsibility") {
        $sql = "SELECT count(responsibility) FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id ANd goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND responsibility IS NOT NULL;";
    //    // debug
    //    //print "debug counter processesresponsibility sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "processes") {
      $sql = "SELECT count(*) FROM processes, business_plan_texts, goal_statements, projects WHERE processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "lands") {
      $sql = "SELECT count(*) FROM lands, project_lands WHERE lands.id = project_lands.land_id AND project_lands.project_id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "email_addresses") {
      $sql = $this->get_scene_element_sql($this->get_type());

    } else if ($this->get_type() == "hyperlinks") {
      $sql = $this->get_scene_element_sql($this->get_type());

    } else if ($this->get_type() == "classes") {
      $sql = $this->get_scene_element_sql($this->get_type());

    } else if ($this->get_type() == "applications") {
      $sql = $this->get_scene_element_sql($this->get_type());

    } else if ($this->get_type() == "moneymakers") {
      $sql = $this->get_scene_element_sql($this->get_type());

    } else if ($this->get_type() == "databases") {
      $sql = $this->get_scene_element_sql($this->get_type());

    } else if ($this->get_type() == "machines") {
      $sql = $this->get_scene_element_sql($this->get_type());

    } else if ($this->get_type() == "budgets") {
      $sql = "SELECT count(budgets.id) FROM budgets WHERE budgets.project_id = " . $this->get_given_project_id() . " AND budgets.user_name = '" . $this->get_user_obj()->name . "';";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type \"" . $this->get_type() . "\"is not known. Unable to load data.");
    }

    // define database
    // defined above in the blocks of the "if" statement

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  private function get_scene_element_sql($given_class_name_string) {
    return "SELECT count(*) FROM scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string='" . $given_class_name_string . "' AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";
  }

  // method
  public function transfer($results) {
    $markup = "";

    $key = $this->get_type();
    if ($key == "business_plan_texts" ||
        $key == "shifts" ||
        $key == "process_profiles" ||
        $key == "goal_statements" ||
        $key == "domains" ||
        $key == "designers" ||
        $key == "process_flows" ||
        $key == "webpages" ||
        $key == "usernames" ||
        $key == "scene_elements" ||
        $key == "tickets" ||
        $key == "tools" ||
        $key == "timecards" ||
        $key == "projects" ||
        $key == "suppliers" ||
        $key == "documentations" ||
        $key == "books" ||
        $key == "tags" ||
        $key == "budgets" ||
        $key == "pickups" ||
        $key == "pickup_plants" ||
        $key == "shares" ||
        $key == "harvests" ||
        $key == "designs" ||
        $key == "design_instances" ||
        $key == "processes" ||
        $key == "processesresponsibility" ||
        $key == "accounts" ||
        $key == "lands" ||
        $key == "hyperlinks" ||
        $key == "email_addresses" ||
        $key == "classes" ||
        $key == "applications" ||
        $key == "moneymakers" ||
        $key == "machines" ||
        $key == "databases" ||
        $key == "plant_lists" ||
        $key == "plant_histories" ||
        $key == "plant_history_events" ||
        $key == "budgets") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->add_count($key, pg_result($results, $lt, 0));
      }
    } else {
      $markup .= $this->get_db_dash()->output_error("Error in <em>transfer</em>, <em>counter</em> type is not known. <strong>key</strong> = \"" . $key . "\"");
    }

    return $markup;
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo figure out the subsubmenu
    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<strong>Widen View</strong>: <a href=\"projects\">All&nbsp;Projects</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo add user info code here

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // todo nothing here yet

    return $markup;
  }

  // method
  public function output_data_set_table() {
    $markup = "";

    $markup .= "todo";

    return $markup;
  }

  // method
  public function get_count_with_link($given_class_name, $given_project_id) {
    $markup = "";

    $url = $this->url($given_class_name . "/projects/" . $given_project_id);
    $markup .= "<a href=\"" . $url . "\" style=\"text-decoration: none;\">";
    $markup .= $this->get_count_given_class_name($given_class_name);
    $markup .= "</a>";
    //$markup .= "\n";

    // note output percent y only for certain classes
    if ($given_class_name == "goal_statements" ||
        $given_class_name == "business_plan_texts" ||
        $given_class_name == "processes" ||
        $given_class_name == "scene_elements") {
      $markup .= " " . $this->output_percent_y($given_class_name);
    }

    return $markup;
  }

  // method
  public function get_count_given_class_name($given_class_name, $given_field_name = "") {
    $markup = "";

    // make key
    $key = $given_class_name . $given_field_name;

    // debug
    //print "debug counter key = <strong>" . $key . "</strong><br />\n";

    // does it already exist?
    if (! array_key_exists($key, $this->get_counts())) {
      // count does not exist, so query database
      // to make this class handle counts of many different tables...
      // make the type variable even more dynamic than before
      $this->set_type($key);
      $markup .= $this->prepare_query();
    }

    // if key exists
    if (array_key_exists($key, $this->get_counts())) {
      $markup .= $this->counts[$key];

    } else {
      print "<p class=\"error\">Error in Class Counter: no key in counts array found where key = " . $key . "</p>\n";
    }

    return $markup;
  }

  // method
  public function get_counts_sum() {
    $counts_sum = 0;

    // note: when you add a function to this class...
    // you much manuall add that function here...
    // so that it can be added to the counts_sum
    $counts_sum += $this->get_count_with_link("goal_statements", $given_project_id);
    $counts_sum += $this->get_count_with_link("business_plan_texts", $given_project_id);
    $counts_sum += $this->get_count_with_link("processes", $given_project_id);
    $counts_sum += $this->get_count_with_link("scene_elements", $given_project_id);
    $counts_sum += $this->get_count_with_link("shifts", $given_project_id);
    $counts_sum += $this->get_count_with_link("process_profiles", $given_project_id);
    $counts_sum += $this->get_count_with_link("domains", $given_project_id);
    $counts_sum += $this->get_count_with_link("webpages", $given_project_id);
    $counts_sum += $this->get_count_with_link("designers", $given_project_id);
    $counts_sum += $this->get_count_with_link("tickets", $given_project_id);
    $counts_sum += $this->get_count_with_link("tools", $given_project_id);
    $counts_sum += $this->get_count_with_link("timecards", $given_project_id);
    $counts_sum += $this->get_count_with_link("suppliers", $given_project_id);
    $counts_sum += $this->get_count_with_link("documentations", $given_project_id);
    $counts_sum += $this->get_count_with_link("books", $given_project_id);
    $counts_sum += $this->get_count_with_link("tags", $given_project_id);
    $counts_sum += $this->get_count_with_link("plant_lists", $given_project_id);
    $counts_sum += $this->get_count_with_link("designs", $given_project_id);
    $counts_sum += $this->get_count_with_link("design_instances", $given_project_id);
    $counts_sum += $this->get_count_with_link("lands", $given_project_id);
    $counts_sum += $this->get_count_with_link("hyperlinks", $given_project_id);
    $counts_sum += $this->get_count_with_link("email_addresses", $given_project_id);
    $counts_sum += $this->get_count_with_link("applications", $given_project_id);
    $counts_sum += $this->get_count_with_link("moneymakers", $given_project_id);
    $counts_sum += $this->get_count_with_link("machines", $given_project_id);
    $counts_sum += $this->get_count_with_link("databases", $given_project_id);
    $counts_sum += $this->get_count_with_link("pickups", $given_project_id);
    $counts_sum += $this->get_count_with_link("budgets", $given_project_id);

    return $counts_sum;
  }

  // method
  public function output_count_cell($regular_class_name, $given_project_id, $output_name_flag) {
    $markup = "";

    // todo the parameter is "regular_class_name"
    // todo however the projects class is sending subsystem names   
    // todo need to actually have this could all classes in subsystem
    // todo so refactor this part to another function that then calls this function
    if ($regular_class_name == "plant_lists_subsystem") {
      $regular_class_name = "plant_lists";
    } else if ($regular_class_name == "locations_subsystem") {
      $regular_class_name = "lands";
    } else if ($regular_class_name == "budgets_subsystem") {
      $regular_class_name = "budgets";
    } else if ($regular_class_name == "times_subsystem") {
      $regular_class_name = "tickets";
    } else if ($regular_class_name == "people_subsystem") {
      $regular_class_name = "suppliers";
    } else if ($regular_class_name == "domains_subsystem") {
      $regular_class_name = "domains";
    }
    // check for needed variables
    if (! $regular_class_name) {
      // required input is not known
      return "Error counter: regular_class_name is not know known " . $regular_class_name . "<br />\n";
    }

    // get counts
    $counts_string = $this->get_count_with_link($regular_class_name, $given_project_id);

    // regular_class_name
    if ($output_name_flag) {
      // output cell
      $counts = "";
      $markup .= "  <td style=\"" . $this->get_style($counts, $output_name_flag) . " padding: 6px;\" valign=\"top\">\n";
      if ($output_name_flag) {
        $markup .= "    ". $regular_class_name;
        $markup .= "  \n";
      }
      $markup .= "  </td>\n";
    }

    // count
    // output cell
    if ($regular_class_name && array_key_exists($regular_class_name, $this->counts )) {
      $counts = $this->counts[$regular_class_name];
      $markup .= "  <td style=\"" . $this->get_style($counts, $output_name_flag) . "; padding: 6px;\" valign=\"top\">\n";
    } else {
      $counts = "?";
      $markup .= "  <td style=\"" . $this->get_style($counts, $output_name_flag) . "; padding: 6px;\" valign=\"top\">\n";
    }

    $markup .= $counts_string;
    $markup .= "  \n";
    $markup .= "  </td>\n";

    return $markup;
  }

  // method
  public function output_count_cell_given_process_id($regular_class_name, $given_process_id, $output_name_flag) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);

    // counts
    $counts = 0;

    // todo validate regular_class_name

    // see which regular class_name the user needs a count of
    if ($regular_class_name == "tickets") {
      $counts = $this->get_count_given_class_name($regular_class_name);
    } else {
      // required input is not known; so, reset variable to null string
      print "Error lib/counter: regular_class_name is not now known " . $regular_class_name . "<br />\n";
      //$aregular_class_name = "";
    }

    $markup .= "  <td style=\"" . $this->get_style($counts, $output_name_flag) . "\" valign=\"top\">\n";

    // output tab
    $markup .= "  ";

    // output counts
    
    $url = $this->url($regular_class_name . "/processes/" . $given_process_id);
    $markup .= "<a href=\"" . $url . "\" style=\"text-decoration: none;\">";
    $markup .= $counts;
    $markup .= "</a>\n";

    if ($output_name_flag) {
      $markup .= "&nbsp;";
      $url = $this->url($regular_class_name . "/projects/" . $given_project_id);
      $markup .= "<a href=\"" . $url . "\" style=\"text-decoration: none;\">";
      $markup .= $regular_class_name;
      $markup .= "</a>\n";
    }
    $markup .= "  </td>\n";

    return $markup;
  }

  // method
  private function get_style($counts, $output_name_flag) {
    $markup = "";

    $style_all = "text-align: center; vertical-align: middle;font-weight: bold; font-size: 105%;";
    if ($counts > 0) {
      // green
      $markup .= "background-color: #CCFFCC;" . $style_all;
    } else {
      // medium purple
      $markup .= "background-color: #AAAAFF;" . $style_all;
      // off-white
      //$markup .= "background-color: #EFEFEF;" . $style_all;
    }

    return $markup;
  }

  // method
  private function output_percent_y($given_class_name) {
    $markup = "";

    // get variable that is needed
    $project_id = $this->get_given_project_id();

    // calculate percent_y
    $percent_y = "";
    if ($given_class_name == "goal_statements") {
      // note the code below gets a file in another directory
      include_once(dirname(__DIR__) . "/goal_statements.php");
      $obj = new GoalStatements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $percent_y = $obj->get_y_percent_given_project_id($project_id, $user_obj);
    } else if ($given_class_name == "business_plan_texts") {
      // note the code below gets a file in another directory
      include_once(dirname(__DIR__) . "/business_plan_texts.php");
      $obj = new BusinessPlanTexts($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $percent_y = $obj->get_y_percent_given_project_id($project_id, $user_obj);
    } else if ($given_class_name == "processes") {
      // note the code below gets a file in another directory
      include_once(dirname(__DIR__) . "/processes.php");
      $obj = new Processes($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $percent_y = $obj->get_y_percent_given_project_id($project_id, $user_obj);
    } else if ($given_class_name == "scene_elements") {
      // note the code below gets a file in another directory
      include_once(dirname(__DIR__) . "/scene_elements.php");
      $obj = new SceneElements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $percent_y = $obj->get_y_percent_given_project_id($project_id, $user_obj);
    } else {
      // todo figure this out for now it is not needed
      //$markup .= "error in lib/counter.php, percent_y is not defined";
    }

    // output
    if (isset($percent_y)) {
      if ($percent_y < 100) {
        $markup .= "<br />\n";
        $color = "#000000";
        if (round($percent_y) == 0) {
          $color = "#EE3B3b";
        }
        $markup .= "<span style=\"font-size: 60%; color: " . $color . ";\">";
        $markup .= round($percent_y) . "%Y";
        $markup .= "</span>";
        $markup .= "<br />\n";
        $markup .= "<!-- " . $percent_y . " -->\n";
      }
    }

    return $markup;
  }
}
