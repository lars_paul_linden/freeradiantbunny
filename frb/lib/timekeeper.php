<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/timekeeper.php

include_once("dates.php");

class TimeKeeper extends Dates {

  // method
  public function get_oldest_date_in_db() {
    // todo fix the hard-coded date below
    return "2010-10-09";
  }

  // method
  public function get_now_timestamp() {
    return time();
  }

  // method
  public function this_date_plus_one_day($date) { 
    $timestamp = strtotime($date . " + 1 day");
    $next_date = date('Y-m-d', $timestamp);
    return $next_date;
  }

  // method
  public function convert_from_timestamp_to_date_as_first_year_style($timestamp) {
    // debug
    //print "debug Timekeeper: converting timestamp = " . $timestamp . "<br />\n";
    $date = strftime('%Y-%m-%d', $timestamp);
    return $date;
  }

  // method
  public function get_today_date_as_first_year_style() {
    // get year
    $today_year = date("Y");
    // returns year (e.g. 2010)
    // get month
    $today_month = date("m");
    // returns numerical month (e.g. 11)
    // get day
    $today_day = date("d");
    // returns numerical day (e.g. 21)
    return $today_year . "-" . $today_month . "-" . $today_day;
  }

  // method
  public function convert_year_first_style_date_to_timestamp($year_first_style_date_start) {

    // debug
    //print "debug timekeeper.php year_first_style_date_start = " . $year_first_style_date_start . "<br />\n";

    $hyphen_count = substr_count($year_first_style_date_start, '-');

    // debug
    //print "debug timekeeper.php hyphen_count = " . $hyphen_count . "<br />\n";

    // note: year_first_style_date is in the "2010-11-21" style
    // todo test the string first to make sure that 2 dashes exist
    // only split if there exists the correct form
    $year = "";
    $month = "";
    $day = "";
    if ($hyphen_count == 2) {
      list($year, $month, $day) = preg_split('/-/', $year_first_style_date_start);
    } else {
      return "timekeeper.php error: bad sort form";
    }     
    // debug
    //print "input year = " . $year . "<br />\n";
    //print "input month = " . $month . "<br />\n";
    //print "input day = " . $day . "<br />\n";
    // convert date input into timestamp
    // note that this function appears to assume local zone (+4 from GMT)
    return mktime(0,0,0,$month,$day,$year);
  }

  // method
  public function get_production_day_count($year_first_style_date_start = "") {
    // count the first day
    // and count today (even if partial)
    // and all days in between the two above
    $year_first_style_date_start = $this->get_oldest_date_in_db();
    // declare variable that is being solved for
    $days_elapsed = 0;
    // deal with date input
    // the funcion below was a date but for 4 o'clock
    // debug
    //print "<p>timekeeper: year first style date to timestamp = $year_first_style_date_start</p>\n";
    // this day is EST
    // to get GMT add 4 hours
    $timestamp_start = $this->convert_year_first_style_date_to_timestamp($year_first_style_date_start);
    // debug
    //print "<p>timekeeper: timestamp start = " . $timestamp_start . "</p>\n";
    $year_first_style_date_today = $this->get_today_date_as_first_year_style();
    // debug
    //print "<p>timekeep: year first style data today = $year_first_style_date_today</p>\n";
    // this day is rounded to the beginning of the day
    // this day is EST
    // to get GMT add 4 hours
    $timestamp_today = $this->convert_year_first_style_date_to_timestamp($year_first_style_date_today);
    // debug
    //print "<p>timekeeper: timestamp today = " . $timestamp_today . "</p>\n";
    $diff = $timestamp_today - $timestamp_start;
    // debug
    //print "<p>timekeeper: diff = " . $diff . "</p>\n";
    // divide by seconds minutes hours
    // in order to make days
    $days_elapsed = round(($diff / (60 * 60 * 24)), 0);
    // now add the fraction so that the hours of today are included
    $hours_elapsed_today = ($this->get_now_timestamp() - $timestamp_today) / (60 * 60);
    // convert to fraction of a day
    $hours_elapsed_today_in_days = $hours_elapsed_today / 24;
    // debug
    //print "<p>timekeeper: hours elapsed today in days = " . $hours_elapsed_today_in_days . "</p>\n";
    // add the hours that have elapsed today
    $days_elapsed += $hours_elapsed_today_in_days;
    // round
    $days_elapsed = round($days_elapsed, 2);
    // debug
    //print "timekeeper: days elapsed = " . $days_elapsed . "<br />\n";
    // all done
    return $days_elapsed;
  }

  // method
  public function unknown($date_to_test_as_string) {
    $boolean = "";

    if ($date_to_test_as_string) {
      $boolean = "true";

      // use this class as a helper class
      include_once("time_measurements.php");
      $time_measurements_obj = new TimeMeasurements();
      $time_measurements_obj->set_foremost_date($date_to_test_as_string);

    }
    return $boolean;
  }

  // method
  public function get_cell_colorized_given_sort_date($given_sort_date, $given_column_name, $given_class_name_for_url = "", $given_id = "", $given_view = "", $cell_element_flag = "") {
    $markup = "";

    // create an HTML table cell

    // create a flag that tells whether there is a link or not
    // assumes a letter and a space at the beginning of string
    // for example "Z " or "Y "
    $link_flag = 0;
    if ($given_class_name_for_url && $given_id && ! $this->is_today(substr($given_sort_date, 2))) {
      // is today, so up goes the flag to add a link
      $link_flag = 1;
    }
    if ($cell_element_flag != "off") {
      $markup .= "  <td style=\"background-color: " . $this->calculate_cell_color($given_column_name, $given_sort_date) . "; text-align: center;\">\n";
    }
    $markup .= "      <span style=\"font-size: 80%;\">";
    if ($link_flag) {
      $url = $given_class_name_for_url . "?";
      // todo move this to a be passed via parameter: below is for processes
      //if ($this->get_status() == "online") {
      //  $url .= "status=online";
      //  $url .= "&amp;";
      //}
      $url .= "make-sort-today=" . $given_id;
      $markup .= "<a href=\"" . $url . "\" style=\"text-decoration: none;\">";
    }
    $markup .= $given_sort_date;
    if ($link_flag) {
      $markup  .= "</a>";
    }
    $markup .= "</span>\n";
    if ($cell_element_flag != "off") {
      $markup .= "  </td>\n";
    }

    return $markup;
  }

  // method
  public function calculate_cell_color($given_column_name = "", $given_sort_date = "") {

    // todo save old colors
    //$color = "#0099CC";

    // define colors
    $color_z = "#CD0000"; # firebrick red;
 
    // set default color rainbow
    $color_1 = "#A6D785";
    $color_1b = "#84BE6A"; # green
    $color_2 = "#33811D"; # dark green
    $color_3 = "#3D77E4"; # blue
    $color_4 = "#7F49D0"; # purple
    $color_5 = "#E28D31"; # orange
    $color_5b = "#928e88"; # light deep purple
    $color_6 = "#CD5555"; # red
    $color_7 = "#3BF965"; # yellow-green

    // debug
    //print "debug timekeeper.php given_column_name " . $given_column_name . "<br />\n";
    //print "debug timekeeper.php given_sort_date " . $given_sort_date . "<br />\n";

    if ($given_column_name == "sort") {
      // check date prefix to see if this is an old "Z" date
      if (substr($given_sort_date, 0, 1) == "Y") {
        // debug
        //print "debug timekeeper.php prefix is Y<br />\n";
        // color according to the timespan
        $date = substr($given_sort_date, 2);
        $timespans = array(10, 90, 180, 200, 365);
        // debug
        //print "debug timekeeper.php date = " . $date . "<br />\n";
        //print "debug timekeeper.php days_elapsed = " . $this->get_days_elapsed($date) . "<br />\n";
        if ($this->is_today($date)) {
          $color = $color_7;
        } else if ($this->get_days_elapsed($date) <= $timespans[0]) {
          $color = $color_1;
        } else if ($this->get_days_elapsed($date) <= $timespans[1]) {
          $color = $color_1b;
        } else if ($this->get_days_elapsed($date) <= $timespans[2]) {
          $color = $color_3; 
        } else if ($this->get_days_elapsed($date) <= $timespans[3]) {
          $color = $color_5;
        } else if ($this->get_days_elapsed($date) <= $timespans[4]) {
          $color = $color_4;
        } else {
          $color = $color_5b;
        }
      } else {
        // this is an old "Z" date
        $color = $color_z;
      }
    } else {
      // all other columns
      $color = "#CCFFCC";
    }

    return $color;
  }

}
