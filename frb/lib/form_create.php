<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/form_create.php

class FormCreate {

  private $given_config;

  // given_config
  public function set_given_config($var) {
    $this->given_config = $var;
  }
  public function get_given_config() {
    return $this->given_config;
  }

  // method
  function __construct($given_config) {
    $this->set_given_config($given_config);
    if ($this->get_given_config()->get_debug()) {
      // debug
      //print "debug " . get_class($this) . " constructing " . get_class($this) . "<br />\n";
    }
  }
 
  // attribute
  private $command;

  // command
  public function set_command($var) {
    $this->command = $var;
  }
  public function get_command() {
    return $this->command;
  }

  // method
  public function output_add_link($given_obj) {
    $markup = "";

    // output a hyperlink that is a form
    // a form is needed so that the method is POST

    include_once("factory.php");
    $factory_obj = new Factory($this->get_given_config());
    $class_name_as_filename_style = $factory_obj->get_class_name_given_object($given_obj);
    $action = $this->url($class_name_as_filename_style . "/create");

    $value_string = "Create a " . $class_name_as_filename_style;

    $markup .= "<form action=\"" . $action . "\" method=\"post\">\n";
    $markup .= "  <input type=\"submit\" value=\"" . $value_string . "\" />\n";
    $markup .= "</form>\n";

    return $markup;
  }

  // method
  public function output_edit_link($given_obj) {
    $markup = "";

    // note: output a hyperlink that is a form
    // note: form is needed so that the method is POST

    include_once("factory.php");
    $factory_obj = new Factory($this->get_given_config());
    $action = $this->url($factory_obj->get_class_name_as_filename_style($given_obj). "/" . $given_obj->get_id() . "/edit");
    $value_string = "Edit";

    $markup .= "<form action=\"" . $action . "\" method=\"post\">\n";
    $markup .= "  <input type=\"submit\" value=\"" . $value_string . "\" />\n";
    $markup .= "</form>\n";

    return $markup;
  }

  // method
  public function output_form($given_this, $given_user_obj) { 
    $markup = "";

    // todo test that this is good security
    if (! isset($given_user_obj->name)) {
      // no an authenticated user, so return blank
      return "";
    }

    // heading
    if ($this->get_command() == "create") {
      $markup .= "<p><strong>Create a new " . get_class($given_this) . ":</strong></p>\n";

    } else if ($this->get_command() == "edit") {
      $markup .= "<h2>Edit " . get_class($given_this) . "</h2>\n";

    } else {
      // output error
      if ($this->get_command()) {
        $markup .= "<p class=\"error\">Error: ";
        $markup .= "<em>command</em> is not set.";
        $markup .= "</p>\n";
     } else {
       $markup .= "<p class=\"error\">Error: ";
       $markup .= "<em>command</em> (" . $this->get_command() . ") is not known.";
       $markup .= "</p>\n";
      }
 
      return $markup;
    }

    $markup .= "<form action=\"\" method=\"post\">\n";
    $markup .= "<table class=\"plants\">\n";

    $form_field_objs = array();

    if (get_class($given_this) == "Timecards") {

      // preprocess (set-up for defaults)
      include_once("dates.php");
      $dates_obj = new Dates();

      $field = "doer_user_name";
      $value_system_generated = $given_user_obj->name;
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "date";
      $value_system_generated = "";
      $value_default = $dates_obj->get_now_date();
      $input_type = "";
      $validation_type = "date";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "time_in";
      $value_system_generated = "";
      $value_default = $dates_obj->get_time_now_as_am_pm_style();
      $input_type = "";
      $validation_type = "time_am_pm";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "time_out";
      $value_system_generated = "";
      $value_default = $dates_obj->get_time_now_as_am_pm_style();;
      $input_type = "";
      $validation_type = "time_am_pm";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "description";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "textarea";
      $validation_type = "text_not_null";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "project_id";
      $value_system_generated = $given_this->get_given_project_id();
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "process_id";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

    } else if (get_class($given_this) == "Tags") {

      $field = "name";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "url";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "reference";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "project_id";
      $value_system_generated = $given_this->get_given_project_id();
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

    } else if (get_class($given_this) == "Searches") {

      $field = "name";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "";
      $validation_type = "";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

    } else if (get_class($given_this) == "Values") {

      // preprocess (set-up for defaults)
      include_once("dates.php");
      $dates_obj = new Dates();

      $field = "plant_id";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "select";
      $validation_type = "id";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "supplier_id";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "select";
      $validation_type = "id";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "price";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "";
      $validation_type = "currency";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "amount";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "";
      $validation_type = "number";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "unit_id";
      $value_system_generated = "";
      $value_default = "1";
      $input_type = "select";
      $validation_type = "id";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "date";
      $value_system_generated = "";
      $value_default = $dates_obj->get_now_date();
      $input_type = "";
      $validation_type = "date";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      $field = "project_id";
      $value_system_generated = "";
      $value_default = "";
      $input_type = "select";
      $validation_type = "id";

      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

    } else if (get_class($given_this) == "GoalStatements") {

      $populated_obj = $given_this;
      if ($this->get_command() == "edit") {

        // field id
        $field = "id";
        $value_system_generated = $given_this->get_given_id();
        $value_default = "";
        $input_type = "";
        $validation_type = "id";
        $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
        array_push($form_field_objs, $form_field_obj);

        // populate the other fields from the database
        $error_message = $given_this->populate();
        $populated_obj = $given_this->get_list_bliss()->get_first_element();

        // check for errors
        if ($error_message) {
          print "debug create error_message: " . $error_message . "<br />\n";
          return $error_message;
        }    
      }

      // field name
      $field = "name";
      $value_system_generated = "";
      $value_default = $populated_obj->get_name();
      $input_type = "";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field description
      $field = "description";
      $value_system_generated = "";
      $value_default = $populated_obj->get_description_html_encoded();
      $input_type = "";
      $validation_type = "html";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field project_id
      $field = "project_id";
      $value_system_generated = $populated_obj->get_project_obj()->get_id();
      $value_default = "";
      $input_type = "";
      $validation_type = "id";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field sort
      $field = "sort";
      $value_system_generated = "";
      $value_default = $populated_obj->get_sort();
      $input_type = "";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field status
      $field = "status";
      $value_system_generated = "";
      $value_default = $populated_obj->get_status();
      $input_type = "";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

    } else if (get_class($given_this) == "Products") {

      $populated_obj = $given_this;
      if ($this->get_command() == "edit") {

        // field id
        $field = "id";
        $value_system_generated = $given_this->get_given_id();
        $value_default = "";
        $input_type = "";
        $validation_type = "id";
        $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
        array_push($form_field_objs, $form_field_obj);

        // populate the other fields from the database
        $error_message = $given_this->populate();
        $populated_obj = $given_this->get_list_bliss()->get_first_element();

        // check for errors
        if ($error_message) {
          print "debug create error_message: " . $error_message . "<br />\n";
          return $error_message;
        }    
      }

      // field year
      $field = "year";
      $value_system_generated = "";
      $value_default = $populated_obj->get_year();
      $input_type = "";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field name
      $field = "name";
      $value_system_generated = "";
      $value_default = $populated_obj->get_name();
      $input_type = "";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field side
      $field = "side";
      $value_system_generated = "";
      $value_default = $populated_obj->get_side();
      $input_type = "select";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field box
      $field = "box";
      $value_system_generated = $populated_obj->get_box();
      $value_default = "TL-";
      $input_type = "";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field condition
      $field = "condition";
      if ($this->get_command() == "edit") {
        $value_system_generated = $populated_obj->get_condition();
      } else {
        $value_system_generated = "used";
      }
      $value_default = "";
      $input_type = "";
      $validation_type = "text_not_null";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // field sort
      $field = "sort";
      include_once("dates.php");
      $dates_obj = new Dates();
      $value_system_generated = "Z " . $dates_obj->get_now_date();
      $value_default = $populated_obj->get_sort();
      $input_type = "";
      $validation_type = "text";
      $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
      array_push($form_field_objs, $form_field_obj);

      // note: if creating a product, the status is set to a null string
      if ($this->get_command() == "edit") {
        // field status
        $field = "status";
        $value_system_generated = "";
        $value_default = $populated_obj->get_status();
        $input_type = "";
        $validation_type = "text";
        $form_field_obj = $this->make_form_field($field, $value_system_generated, $value_default, $input_type, $validation_type);
        array_push($form_field_objs, $form_field_obj);
      }

    } else {
      $markup .= "<p class=\"error\">Error, create class does not know this class.</p>\n";
      return $markup;
    }

    // now, get the rows
    foreach ($form_field_objs as $form_field_obj) {
      $markup .= $form_field_obj->get_input_row($given_user_obj);
    }

    // submit button
    $markup .= "<tr>\n";
    $markup .= "  <td colspan=\"2\" align=\"right\">\n";
    $markup .= "    <input type=\"hidden\" name=\"iteration-count\" value=\"1\" />\n";

    // debug
    //print "debug create command = " . $this->get_command() . "<br />\n";

    if ($this->get_command() == "create") {
      $markup .= "    <input type=\"submit\" value=\"Create\" />\n";
    } else if ($this->get_command("edit")) {
      $markup .= "    <input type=\"submit\" value=\"Save edits\" />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</table>\n";
    $markup .= "</form>\n";

    // check if this is ready to insert
    $ready_to_insert = 1;
    foreach ($form_field_objs as $form_field_obj) {
      if (! $form_field_obj->get_ready_to_insert()) {
        $ready_to_insert = 0;
      }
    }
    if ($ready_to_insert) {
      if ($this->get_command() == "create") {
        $markup .= "<p>Adding...</p>\n";
        if (get_class($given_this) == "Searches") {
          // skip (referential)
        } else if (get_class($given_this) == "Timecards") {
          // transfer values to class instance
          foreach ($form_field_objs as $form_field_obj) {
            $value = $form_field_obj->get_value_to_insert();
            if ($form_field_obj->get_name() == "doer_user_name") {
              $given_this->set_doer_user_name($value);
            }
            if ($form_field_obj->get_name() == "date") {
              $given_this->set_date($value);
            }
            if ($form_field_obj->get_name() == "time_in") {
              $given_this->set_time_in($value);
            }
            if ($form_field_obj->get_name() == "time_out") {
              $given_this->set_time_out($value);
            }
            if ($form_field_obj->get_name() == "description") {
              $given_this->set_description($value);
            }
            if ($form_field_obj->get_name() == "project_id") {
              $given_this->get_project_obj()->set_id($value);
            }
            if ($form_field_obj->get_name() == "process_id") {
              $given_this->get_process_obj()->set_id($value);
            }
          }
        } else if (get_class($given_this) == "Tags") {
          // transfer values to class instance
          foreach ($form_field_objs as $form_field_obj) {
            $value = $form_field_obj->get_value_to_insert();
            if ($form_field_obj->get_name() == "name") {
              $given_this->set_name($value);
            }
            if ($form_field_obj->get_name() == "url") {
              $given_this->set_url($value);
            }
            if ($form_field_obj->get_name() == "reference") {
              $given_this->set_reference($value);
            }
            if ($form_field_obj->get_name() == "project_id") {
              $given_this->get_extra_project_obj()->set_id($value);
            }
          }
        } else if (get_class($given_this) == "Products") {
          // transfer values to class instance
          foreach ($form_field_objs as $form_field_obj) {
            $value = $form_field_obj->get_value_to_insert();
            if ($form_field_obj->get_name() == "year") {
              $given_this->set_year($value);
            }
            if ($form_field_obj->get_name() == "name") {
              $given_this->set_name($value);
            }
            if ($form_field_obj->get_name() == "side") {
              $given_this->set_side($value);
            }
            if ($form_field_obj->get_name() == "box") {
              $given_this->set_box($value);
            }
            if ($form_field_obj->get_name() == "condition") {
              $given_this->set_condition($value);
            }
            if ($form_field_obj->get_name() == "sort") {
              $given_this->set_sort($value);
            }
            if ($form_field_obj->get_name() == "status") {
              $given_this->set_status($value);
            }
          }
        } else if (get_class($given_this) == "Hyperlinks") {

            // transfer values to class instance
            foreach ($form_field_objs as $form_field_obj) {
              $value = $form_field_obj->get_value_to_insert();
              if ($form_field_obj->get_name() == "date") {
                $given_this->set_date($value);
              }
              if ($form_field_obj->get_name() == "url") {
                $given_this->set_url($value);
              }
              if ($form_field_obj->get_name() == "name") {
                $given_this->set_name($value);
              }
              if ($form_field_obj->get_name() == "addon") {
                $given_this->set_addon($value);
              }
              if ($form_field_obj->get_name() == "alphabetical") {
                $given_this->set_alphabetical($value);
              }
            }

        } else if (get_class($given_this) == "Values") {
          // transfer values to class instance
          foreach ($form_field_objs as $form_field_obj) {
            $value = $form_field_obj->get_value_to_insert();
            if ($form_field_obj->get_name() == "plant_id") {
              $given_this->get_plant_obj()->set_id($value);
            }
            if ($form_field_obj->get_name() == "supplier_id") {
              //print "debug create supplier_id " . $value . "<br />";
              $given_this->get_supplier_obj()->set_id($value);
            }
            if ($form_field_obj->get_name() == "price") {
              $given_this->set_price($value);
            }
            if ($form_field_obj->get_name() == "amount") {
              $given_this->set_amount($value);
            }
            if ($form_field_obj->get_name() == "unit_id") {
              $given_this->get_unit_obj()->set_id($value);
            }
            if ($form_field_obj->get_name() == "date") {
              $given_this->set_date($value);
            }
            if ($form_field_obj->get_name() == "project_id") {
              $given_this->get_project_obj()->set_id($value);
            }
          }
        } else {
          $markup .= "<p class=\"error\">Error: class (" . get_class($given_this) . ") is not known.</p>\n";
        }
        // insert
        $error_message = $given_this->insert();

      } else if ($this->get_command() == "edit") {

        $markup .= "<p>Saving edits...</p>\n";
        if (get_class($given_this) == "GoalStatements") {
          // transfer values to class instance
          // todo should the validation of the user strings happen here
          foreach ($form_field_objs as $form_field_obj) {
            $value = $form_field_obj->get_value_to_insert();
            // id already set
            if ($form_field_obj->get_name() == "name") {
              $given_this->set_name($value);
            }
            if ($form_field_obj->get_name() == "description") {
              $given_this->set_description_html_encoded($value);
            }
            if ($form_field_obj->get_name() == "project_id") {
              $given_this->get_project_obj()->set_id($value);
            }
            if ($form_field_obj->get_name() == "sort") {
              $given_this->set_sort($value);
            }
            if ($form_field_obj->get_name() == "status") {
              $given_this->set_status($value);
            }
          }
        }
        // update
        $error_message = $given_this->update();
      }

      // deal error (if any)
      $pos = strpos($error_message, "Error");
      if ($pos === true) {
        $markup .= "<p class=\"error\">There was an error.</p>\n";
        $markup .= "<p class=\"error\">Error character count: \"" . strlen($error_message) . "\"</p>\n";
        $markup .= "<p class=\"error\">Error message: \"" . htmlentities($error_message) . "\"</p>\n";
        return $markup;
      }

      // odd behavior in that the error_message contains "Saved" message
      $markup .= $error_message;
      if ($this->get_command() == "edit") {
        $markup .= "<p style=\"background-color: green; color: yellow;\">Saved.</p>\n";
        if (get_class($given_this) == "GoalStatements") {
          $url = $this->url("goal_statements/" . $given_this->get_given_id());
          $markup .= "To <a href=\"" . $url . "\">goal statement id " . $given_this->get_given_id() . "</a><br />\n";
        }
      }
    }

    return $markup;
  }

  // method
  private function make_form_field($given_field_name, $given_value_system_generated, $given_default_value, $given_input_type, $given_validation_type) {
    include_once("form_field.php");
    $form_field_obj = new FormField();
    $form_field_obj->set_name($given_field_name);
    $form_field_obj->set_value_system_generated($given_value_system_generated);
    $form_field_obj->set_value_default($given_default_value);
    $form_field_obj->set_input_type($given_input_type);
    $form_field_obj->set_validation_type($given_validation_type);
    return $form_field_obj;
  }

  // todo old method to clean up
  // from plants.php
  // method
  public function print_plants_radio_button_list($parameter_name) {
    $markup = "";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<table border=\"1\" cellpadding=\"6\">\n";
      $markup .= "<tr>\n";
      $markup .= "  <td valign=\"top\">\n";
      $count = -1;
      foreach ($this->get_list_bliss()->get_list() as $plant) {
        $count++;
        if ($count == 16) {
          $count = 0;
          $markup .= "  </td>\n";
          $markup .= "  <td valign=\"top\">\n";
        }
        $markup .= "    <input type=\"radio\" name=\"" . $parameter_name . "\" value=\"" . $plant->get_id() . "\" />" . $plant->get_common_name() . "<br />\n";
      }
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "</table>\n";
    }

    return $markup;
  }

  // todo old method to clean up
  // from plants.php
  // method
  public function print_select_list($name, $value_default_plant_id) {
    $markup = "";
    // parameter "name" is the attribute value for the input element

    $type = "get_plant_list_given_user_name";
    $this->get_db_dash()->load($this, $type);

    $markup .= "    <select name=\"" . $name . "\">\n";
    foreach ($this->get_list_bliss()->get_list() as $plant) {
      $markup .= "      <option value=\"" . $plant->get_id() . "\"";
      if ($value_default_plant_id) {
        if ($value_default_plant_id == $plant->get_id()) {
          $markup .= " selected=\"selected\"";
        }
      }
      $markup .= ">" . $plant->get_common_name() . "</option>\n";
    }
    $markup .= "    </select>\n";
    return $markup;
  }

  // from crop_plans
  // method form
  private function process_add_an_event_to_this_plant_list() {
    $markup = "";

    // get data from user
    $request = "";
    if (isset($_GET['request'])) {
      if ($_GET['request'] == "process_submitted_form_data") {
        $request = "process_submitted_form_data";
      }
    }
    $plant_history_id = "";
    if (isset($_GET['plant_history_id'])) {
      if ($this->validate_user_input_alphanumeric($_GET['plant_history_id'])) {
        $plant_history_id = $_GET['plant_history_id'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>plant_history_id</strong> is not valid. Please, try again.");
      }
    }
    $variety_id = "";
    if (isset($_GET['variety_id'])) {
      if ($this->validate_user_input_alphanumeric($_GET['variety_id'])) {
        $variety_id = $_GET['variety_id'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>variety_id</strong> is not valid. Please, try again.");
      }
    }
    // needed for varieties radio list in form
    $plant_id = "";
    if (isset($_GET['plant_id'])) {
      if ($this->validate_user_input_alphanumeric($_GET['plant_id'])) {
        $plant_id = $_GET['plant_id'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>plant_id</strong> is not valid. Please, try again.");
      }
    }
    $name = "";
    if (isset($_GET['name'])) {
      if ($this->validate_user_input_alphanumeric($_GET['name'])) {
        $name = $_GET['name'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>name</strong> is not valid. Please, try again.");
      }
    }
    $date = "";
    if (isset($_GET['date'])) {
      if ($this->validate_user_input_alphanumeric($_GET['date'])) {
        $date = $_GET['date'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>date</strong> is not valid. Please, try again.");
      }
    }
    $plant_count = "";
    if (isset($_GET['plant_count'])) {
      if ($this->validate_user_input_alphanumeric($_GET['plant_count'])) {
        $date = $_GET['plant_count'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>plant_count</strong> is not valid. Please, try again.");
      }
    }

    // given data from user, figure out what to do
    if ($request && $plant_history_id && $name && $date && $plant_count) {
      $markup .= "<p>Saving data...</p>\n";
      $markup .= "<p><strong>plant_list_plant_id</strong> = $plant_list_plant_id</p>";
      $error_message = "";
      // todo might be a good policy to always set the user_name in the query
      $type = "insert";
      $sql = "INSERT INTO plant_history_events (plant_history_id, name, multifunction_date_id, plant_count) VALUES ('" . $plant_history_id . "', '" . $name . "', '" . $date . "', '" . $plant_count . "');";
      // todo save this because it is needed soon
      //$sql = "INSERT INTO plant_histories (plant_list_plant_id, variety_id) VALUES ('" . $plant_list_plant_id . "', '" . $variety_id . "');";
      $database_name = "plantdot_soiltoil";
      $error_message = $this->get_db_dash()->insert($this, $type, $sql, $database_name);
      if ($error_message) {
        $markup .= "<p class=\"error\">Not saved.</p>\n";
        $markup .= $this->get_db_dash()->output_error($error_message);
      } else {
        $markup .= "<p style=\"background-color: green; color: yellow;\">Saved.</p>\n";
       $url = $this->url("plant_histories/plant_list/" . $this->get_given_plant_list_id());
        $markup .= "<p>Back to <a href=\"" . $url . "\">plant list</a>.</p>\n";
      }
      $markup .= "<br />";
    } else {

      // default_value: form
      $markup .= "<p>Please, specify the details of the event, and click the \"Submit\" button.</p>\n";
      $markup .= "<form method=\"get\">\n";
      $markup .= "  <input type=\"hidden\" name=\"command\" value=\"add_an_event_to_this_plant_list\" />\n";
      $markup .= "  <input type=\"hidden\" name=\"request\" value=\"process_submitted_form_data\" />\n";
      $markup .= "  <input type=\"hidden\" name=\"plant_list_id\" value=\"" . $this->get_given_plant_list_id() . "\" />\n";
      $markup .= "  <input type=\"hidden\" name=\"plant_history_id\" value=\"" . $plant_history_id . "\" />\n";
 
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name what was done during the event\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "  <input type=\"text\" name=\"name\" value=\"\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    <input type=\"text\" name=\"date\" value=\"\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    <input type=\"text\" name=\"plant_count\" value=\"\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    // get radio buttons
    //$parameter_name = "variety_id";
    //include_once("varieties.php");
    //$varieties_obj = new Varieties;
    //$varieties_obj->set_user_obj($this->get_user_obj());
    //$markup .= $varieties_obj->print_radio_button_list($parameter_name, $plant_id);

      $markup .= "<br />\n";
      $markup .= "  <div style=\"margin-left: 40px\"><input type=\"submit\" value=\"Submit\"></div>\n";
      $markup .= "</form>\n";
      $markup .= "<br />";
    }

    return $markup;
  }

  // from suppliers
  // method
  public function output_supplier_form() { 
    $markup = "";

    $ready_to_insert = 1;

    $markup .= "<h2>Supplier</h2>\n";
    $markup .= "<form action=\"suppliers.php\" method=\"post\">\n";
    $markup .= "<input type=\"hidden\" name=\"command\" value=\"add\" />\n";
    $markup .= "<table class=\"plant\">\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $name = "";
    if (isset($_POST['name'])) {

      include_once("factory.php");
      $validator = new Validator();
      $name = $validator->sanitize_user_input($_POST['name']);
      $this->set_name($name);
      $markup .= "    " . $name . "\n";
      $markup .= "    <input type=\"hidden\" name=\"name\" value=\"" . $name . "\" />\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"name\" />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    city\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $name = "";
    if (isset($_POST['city'])) {
      $city = $this->sanitize_user_input($_POST['city']);
      $this->set_city($city);
      $markup .= "    " . $city . "\n";
      $markup .= "    <input type=\"hidden\" name=\"city\" value=\"" . $city . "\" />\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"city\" />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    state\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['state'])) {
      $state = $this->sanitize_user_input($_POST['state']);
      $this->set_state($state);
      $markup .= "    " . $state . "\n";
      $markup .= "    <input type=\"hidden\" name=\"state\" value=\"" . $state . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"state\" />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['url'])) {
      $url = $this->sanitize_user_input($_POST['url']);
      $this->set_url($url);
      $markup .= "    " . $url . "\n";
      $markup .= "    <input type=\"hidden\" name=\"url\" value=\"" . $url . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"url\" />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    &nbsp;\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    <input type=\"submit\" value=\"Add\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</table>\n";
    $markup .= "</form>\n";

    if ($ready_to_insert) {
      $markup .= "<p>Adding...</p>\n";
      $type = "insert_supplier";
      $error_message = $this->get_db_dash()->insert($this, $type);
      if ($error_message) {
        $markup .= "<p>There was an error.</p>\n";
        $markup .= "<p>" . $error_message . "</p>\n";
      } else {
         $markup .= "<p>Added successfully.</p>\n";
         $url = $this->url("suppliers");
         $markup .= "<p><a href=\"" . $url . "\">suppliers</a>\n";
      }
    } else {
      $markup .= "<p>Please, complete the form.</p>\n";
    }

    // todo integrate the following code
    //if ($type == "insert_supplier") {
    //   $sql = "INSERT into suppliers (name, city, state, url) VALUES ('" . $instance->get_name() . "', '" . $instance->get_city() . "', '" . $instance->get_state() . "', '" . $instance->get_url() . "');";
    //}

    //if ($this->get_command() == "add") {
    //  $markup .= $this->output_form();
    //} else if ($this->get_id()) {
    //  // get data from database

    // $markup .= $this->output_given_id();

    return $markup;
  }

  // from suppliers
  // method
  public function print_suppliers_radio_button_list($name) {
    $markup = "";

    // parameter "name" is the attribute value for the input element
    $type = "get_supplier_list";
    $this->get_db_dash()->load($this, $type);
    foreach ($this->get_list_bliss()->get_list() as $supplier) {
      $markup .= "<input type=\"radio\" name=\"" . $name . "\" value=\"" . $supplier->get_id() . "\" />" . $supplier->get_name() . "<br />\n";
    }

    return $markup;
  }

  // from suppliers
  // method
  public function print_suppliers_select_list($name, $value_default_id) {
    $markup = "";

    // parameter "name" is the attribute value for the input element
    $type = "get_supplier_list";
    $this->get_db_dash()->load($this, $type);
    $markup .= "    <select name=\"" . $name . "\">\n";
    foreach ($this->get_list_bliss()->get_list() as $supplier) {
      $markup .= "      <option value=\"" . $supplier->get_id() . "\"";
      if ($value_default_id) {
        if ($value_default_id == $supplier->get_id()) {
          $markup .= " selected=\"selected\"";
        }
      }
      $markup .= ">" . $supplier->get_name() . "</option>\n";
    }
    $markup .= "    </select>\n";

    return $markup;
  }

  // from plant_list
  // method form
  private function process_create_a_new_list() {
    $markup = "";

    // get data from user
    $request = "";
    if (isset($_GET['request'])) {
      if ($_GET['request'] == "process_submitted_form_data") {
        $request = "process_submitted_form_data";
      }
    }
    $name = "";
    if (isset($_GET['name'])) {
      if ($this->validate_user_input_alphanumeric($_GET['name'])) {
        $name = $_GET['name'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>name</strong> is not valid. Please, try again.");
      }
    }

    // given data from user, figure out what to do
    if ($request && $name) {
      $markup .= "<p>Saving data...</p>\n";
      $markup .= "<p><strong>name</strong> = $name</p>";
      $error_message = "";
      // todo should code check for duplicate names? (add this in next release)
      $type = "insert_plant_list";
      $sql = "INSERT INTO plant_lists (name, project_id) VALUES ('" . $name . "', '" . $this->get_project_obj()->get_id() . "');";
      $database_name = "plantdot_soiltoil";
      $error_message = $this->get_db_dash()->insert($this, $type, $sql, $database_name);
      if ($error_message) {
        $markup .= "<p class=\"error\">Not saved.</p>\n";
        $markup .= $this->get_db_dash()->output_error($error_message);
      } else {
        $markup .= "<p style=\"background-color: green; color: yellow;\">Saved.</p>\n";
        $url = $this->url("plants");
        $markup .= "<p>Go to <a href=\"" . $url . "\">plants</a>";
        $url = $this->url("plant_lists");
        $markup .= " or <a href=\"" . $url . "\">plant lists</a>.</p>\n";
      }
      $markup .= "<br />";
    } else {
      // default_value: form
      $markup .= "<p>To create a new list of plants, complete this very short form and click the \"Submit\" button.</p>\n";
      $markup .= "<p>Use only alphabet letters, numbers, and spaces when entering a name. Names should not have more than 40 characters.</p>\n";
      $markup .= "<form method=\"get\">\n";
      $markup .= "  <input type=\"hidden\" name=\"command\" value=\"create_a_new_list\" />\n";
      $markup .= "  <input type=\"hidden\" name=\"request\" value=\"process_submitted_form_data\" />\n";
      $markup .= "  list <strong>name</strong>: <input type=\"text\" name=\"name\" value=\"" . $name . "\" /><br />\n";
      $markup .= "<br />\n";
      $markup .= "  <div style=\"margin-left: 140px\"><input type=\"submit\" value=\"Submit\"></div>\n";
      $markup .= "</form>\n";
      $markup .= "<br />";
    }

    return $markup;
  }

  // from plant_lists
  // method form
  private function process_add_a_plant_to_this_list() {
    $markup = "";

    // get data from user
    $request = "";
    if (isset($_GET['request'])) {
      if ($_GET['request'] == "process_submitted_form_data") {
        $request = "process_submitted_form_data";
      }
    }
    $plant_id = "";
    if (isset($_GET['plant_id'])) {
      if ($this->validate_user_input_alphanumeric($_GET['plant_id'])) {
        $plant_id = $_GET['plant_id'];
      } else {
        $markup .= $this->get_db_dash()->output_error("Error: That <strong>plant_id</strong> is not valid. Please, try again.");
      }
    }

    // given data from user, figure out what to do
    if ($request && $plant_id && $this->get_given_id()) {
      $markup .= "<p>Saving data...</p>\n";
      $markup .= "<p><strong>plant_list_id</strong> = " . $this->get_given_id() . "</p>\n";
      $markup .= "<p><strong>plant_id</strong> = $plant_id</p>";
      $error_message = "";
      // todo might be a good policy to always set the user_name in the query
      $sql = "INSERT INTO plant_list_plants (plant_list_id, plant_id) VALUES ('" . $this->get_given_id() . "', '" . $plant_id . "');";
      $database_name = "plantdot_soiltoil";
      $error_message = $this->get_db_dash()->insert($this, $type, $sql, $database_name);
      if ($error_message) {
        $markup .= "<p class=\"error\">Not saved.</p>\n";
        $markup .= $this->get_db_dash()->output_error($error_message);
      } else {
        $markup .= "<p style=\"background-color: green; color: yellow;\">Saved.</p>\n";
        $url = $this->url("plant_lists/" . $this->get_given_id());
        $markup .= "<p>Back to <a href=\"" . $url . "\">plant list</a>.</p>\n";
      }
      $markup .= "<br />";
    } else {

      // form

      $markup .= "<p>To add a plant to the given plant list, mark to radio button next to the plant's common name, and click the \"Submit\" button.</p>\n";
      $markup .= "<form method=\"get\">\n";
      $markup .= "  <input type=\"hidden\" name=\"command\" value=\"add_a_plant_to_this_list\" />\n";
      $markup .= "  <input type=\"hidden\" name=\"request\" value=\"process_submitted_form_data\" />\n";
      $markup .= "  <input type=\"hidden\" name=\"id\" value=\"" . $this->get_given_id() . "\" />\n";

      // get radio buttons
      $parameter_name = "plant_id";
      include_once("plants.php");
      $plants_obj = new Plants;
      $plants_obj->set_user_obj($this->get_user_obj());
      $markup .= $plants_obj->print_radio_button_list($parameter_name);

      $markup .= "<br />\n";
      $markup .= "  <div style=\"margin-left: 40px\"><input type=\"submit\" value=\"Submit\"></div>\n";
      $markup .= "</form>\n";
      $markup .= "<br />";
    }

    return $markup;
  }

  // from prices
  // method
  public function output_form_decision() { 

    // this form works for inserting new row and for editing existing row

    if ($this->get_id()) { 
      // build form
      $markup .= "<p><em>edit existing</em></p>\n";
      // get data from database
      $type = "get_cost_by_given_id";
      $this->get_db_dash()->load($this, $type);
    }

    // for "add" (helps with multi request filling out of form
    $ready_to_insert = 1;

    $markup .= "<h2>Cost</h2>\n";
    $markup .= "<form action=\"cost.php\" method=\"post\">\n";
    $markup .= "<input type=\"hidden\" name=\"command\" value=\"record\" />\n";
    if ($this->get_id()) {
      $markup .= "<input type=\"hidden\" name=\"id\" value=\"" . $this->get_id() . "\" />\n";
    }
    $markup .= "<table class=\"plant\">\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $plant_id = "";
    if (isset($_POST['plant_id'])) {
      $plant_id = $this->sanitize_user_input($_POST['plant_id']);
      $this->set_plant_id($plant_id);
       // get plant common name
      include_once("plant.php");
      $plant_obj = new Plant();
      $plant_obj->set_id($plant_id);
      $plant_obj->load_data();
      $markup .= "    " . $plant_obj->get_common_name_with_link() . "\n";
      $markup .= "    <input type=\"hidden\" name=\"plant_id\" value=\"" . $plant_id . "\" />\n";
    } else {
      $ready_to_insert = 0;
      // select
      include_once("plantlist.php");
      $plantlist_obj = new PlantList();
      // old
      //$plantlist_obj->print_radio_button_list("plant_id");
      // new
      $plantlist_obj->print_select_list("plant_id", $this->get_plant_obj->get_id());
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['supplier_id'])) {
      $supplier_id = $this->sanitize_user_input($_POST['supplier_id']);
      $this->set_supplier_id($supplier_id);
      // get supplier name
      include_once("supplier.php");
      $supplier_obj = new Supplier();
      $supplier_obj->set_id($supplier_id);
      $supplier_obj->load_data();
      $markup .= "    " . $supplier_obj->get_name_with_link() . "\n";
      $markup .= "    <input type=\"hidden\" name=\"supplier_id\" value=\"" . $supplier_id . "\" />\n";
    } else {
      $ready_to_insert = 0;
      // select
      include_once("supplierlist.php");
      $supplierlist_obj = new SupplierList();
      // old
      //$supplierlist_obj->print_radio_button_list("supplier_id");
      // new
      $supplierlist_obj->print_select_list("supplier_id", $this->get_supplier_id());
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['date'])) {
      $markup .= "    date\n";
    } else {
      // show format
      $markup .= "    date\n";
    }
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['date'])) {
      $date = $this->sanitize_user_input($_POST['date']);
      $this->set_date($date);
      $markup .= "    " . $date . "\n";
      $markup .= "    <input type=\"hidden\" name=\"date\" value=\"" . $date . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"date\" value=\"";
      if ($this->get_date()) {
        $markup .= $this->get_date();
      } else {
        $prime_date = date("Y-m-d"); // format 2001-04-01;
        $markup .= $prime_date;
      }
      $markup .= "\" />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    dollars\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['dollars'])) {
      $dollars = $this->sanitize_user_input($_POST['dollars']);
      $this->set_dollars($dollars);
      $markup .= "    " . $dollars . "\n";
      $markup .= "    <input type=\"hidden\" name=\"dollars\" value=\"" . $dollars . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"dollars\"";
      if ($this->get_dollars()) {
        $markup .= " value=\"" . $this->get_dollars() . "\"";
      }
      $markup .= " />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    unit count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['unit_count'])) {
      $unit_count = $this->sanitize_user_input($_POST['unit_count']);
      $this->set_unit_count($unit_count);
      $markup .= "    " . $unit_count . "\n";
      $markup .= "    <input type=\"hidden\" name=\"unit_count\" value=\"" . $unit_count . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"unit_count\"";
      if ($this->get_unit_count()) {
        $markup .= " value=\"" . $this->get_unit_count() . "\"";
      }
      $markup .= "  />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    unit\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['unit'])) {
      $unit = $this->sanitize_user_input($_POST['unit']);
      $this->set_unit($unit);
      $markup .= "    " . $unit . "\n";
      $markup .= "    <input type=\"hidden\" name=\"unit\" value=\"" . $unit . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"unit\"";
      if ($this->get_unit()) {
        $markup .= " value=\"" . $this->get_unit() . "\"";
      }
      $markup .= " />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    quality\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['quality'])) {
      $quality = $this->sanitize_user_input($_POST['quality']);
      $this->set_quality($quality);
      $markup .= "    " . $quality . "\n";
      $markup .= "    <input type=\"hidden\" name=\"quality\" value=\"" . $quality . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      $markup .= "    <input type=\"text\" name=\"quality\"";
      if ($this->get_quality()) {
        $markup .= " value=\"" . $this->get_quality() . "\"";
      }
      $markup .= " />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    organic flag\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['organic_flag'])) {
      $organic_flag = $this->sanitize_user_input($_POST['quality_flag']);
      $this->set_organic_flag($organic_flag);
      $markup .= "    " . $organic_flag . "\n";
      $markup .= "    <input type=\"hidden\" name=\"organic_flag\" value=\"" . $organic_flag . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      // <!-- simple combobox for organic_flag -->
      $markup .= "    <select name=\"organic_flag\">\n";
      $markup .= "      <option value=\"t\">true</option>\n";
      $markup .= "      <option value=\"f\">false</option>\n";
      $markup .= "    </select>\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    buyer\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    if (isset($_POST['buyer_id'])) {
      $buyer_id = $this->sanitize_user_input($_POST['buyer_id']);
      $this->set_buyer_id($buyer_id);
      // get buyer name
      include_once("supplier.php");
      $buyer_obj = new Supplier();
      $buyer_obj->set_id($buyer_id);
      $buyer_obj->load_data();
      $markup .= "    " . $buyer_obj->get_name_with_link() . "\n";
      $markup .= "    <input type=\"hidden\" name=\"buyer_id\" value=\"" . $buyer_id . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      // select
      include_once("supplierlist.php");
      $buyerlist_obj = new SupplierList();
      // old
      //$buyerlist_obj->print_radio_button_list("buyer_id");
      // new
      $buyerlist_obj->print_select_list("buyer_id", $this->get_buyer_id());
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    if (isset($_POST['id'])) {
      // skip
    } else {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    &nbsp;\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      if ($this->get_id()) {
        $markup .= "    <input type=\"submit\" value=\"Edit\" />\n";
      } else {
        $markup .= "    <input type=\"submit\" value=\"Add\" />\n";
      }
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";
    $markup .= "</form>\n";

    if ($ready_to_insert) {
      if (isset($_POST['id'])) {
        // update
        $markup .= "<p>Updating database...</p>\n";
        $type = "update_cost";
        $error_message = $this->get_db_dash()->update($this, $type);
        if ($error_message) {
          $markup .= "<p>There was an error.</p>\n";
          $markup .= "<p>" . $error_message . "</p>\n";
        } else {
          $markup .= "<p>Edit was successful!</p>\n";
          $markup .= "<p><a href=\"transactions.php\">transactions</a>\n";
        }
      } else {
        $markup .= "<p>Adding to database...</p>\n";
        $type = "insert_cost";
        $error_message = $this->get_db_dash()->insert($this, $type);
        if ($error_message) {
          $markup .= "<p>There was an error.</p>\n";
          $markup .= "<p>" . $error_message . "</p>\n";
        } else {
          $markup .= "<p>Add was successful!</p>\n";
          $markup .= "<p><a href=\"transactions.php\">transactions</a>\n";
        }
      }
    } else {
      $markup .= "<p>Please, complete the form.</p>\n";
    }

    return $markup;
  }

  // from postings
  // method form
  private function output_postings_form() {

    // this form works for inserting new row and for editing existing row

    if ($this->get_id()) { 
      // build form
      print "<p><em>edit existing</em></p>\n";
      // get data from database
      $type = "get_posting_by_given_id";
      $this->get_db_dash()->load($this, $type);
    }

    // for "add" (helps with multi request filling out of form
    $ready_to_insert = 1;

    print "<h2>Posting</h2>\n";
    print "<form action=\"posting.php\" method=\"post\">\n";
    print "<input type=\"hidden\" name=\"command\" value=\"record\" />\n";
    print "<input type=\"hidden\" name=\"budget_id\" value=\"" . $this->get_budget_id() . "\"/>\n";
    if ($this->get_id()) {
      print "<input type=\"hidden\" name=\"id\" value=\"" . $this->get_id() . "\" />\n";
    }
    print "<table class=\"plant\">\n";

    print "<tr>\n";
    print "  <td>\n";
    print "    account\n";
    print "  </td>\n";
    print "  <td>\n";
    $plant_id = "";
    if (isset($_POST['account_id'])) {
      $account_id = $this->sanitize_user_input($_POST['account_id']);
      $this->set_account_id($account_id);
       // get account name
      include_once("accounts.php");
      $account_obj = new Accounts();
      // todo moves this lines into a sidecar function in accounts class
      $account_obj->set_id($account_id);
      $account_obj->determine_type();
      $account_obj->prepare_query();
      print "    " . $account_obj->get_name() . "\n";
      print "    <input type=\"hidden\" name=\"plant_id\" value=\"" . $plant_id . "\" />\n";
    } else {
      $ready_to_insert = 0;
      // select
      include_once("accounts.php");
      $accounts_obj = new Accounts();
      // new
      $accounts_obj->print_select_list("account_id", $this->get_account_id());
    }
    print "  </td>\n";
    print "</tr>\n";

    print "<tr>\n";
    print "  <td>\n";
    print "    amount\n";
    print "  </td>\n";
    print "  <td>\n";
    if (isset($_POST['amount'])) {
      $amount = $this->sanitize_user_input($_POST['amount']);
      $this->set_amount($amount);
      print "    " . $amount . "\n";
      print "    <input type=\"hidden\" name=\"amount\" value=\"" . $amount . "\"/>\n";
    } else {
      $ready_to_insert = 0;
      print "    <input type=\"text\" name=\"amount\"";
      if ($this->get_amount()) {
        print " value=\"" . $this->get_amount() . "\"";
      }
      print " />\n";
    }
    print "  </td>\n";
    print "</tr>\n";

    print "<tr>\n";
    print "  <td>\n";
    print "    asset type\n";
    print "  </td>\n";
    print "  <td>\n";
    $plant_id = "";
    if (isset($_POST['asset_type_id'])) {
      $asset_type_id = $this->sanitize_user_input($_POST['asset_type_id']);
      $this->set_asset_type_id($asset_type_id);
       // get asset type
      include_once("asset_types.php");
      $asset_type_obj = new AssetTypes();
      // todo move these lines into a new sidecar function in asset_type class
      $asset_type_obj->set_id($asset_type_id);
      $asset_type_obj->determine_type();
      $asset_type_obj->prepare_query();
      print "    " . $asset_type_obj->get_name() . "\n";
      print "    <input type=\"hidden\" name=\"asset_type_id\" value=\"" . $asset_type_id . "\" />\n";
    } else {
      $ready_to_insert = 0;
      // select
      include_once("asset_types.php");
      $asset_types_obj = new AssetTypes();
      // new
      $asset_types_obj->print_select_list("asset_type_id", $this->get_asset_type_id());
    }
    print "  </td>\n";
    print "</tr>\n";

    print "<tr>\n";
    print "  <td>\n";
    print "    journal\n";
    print "  </td>\n";
    print "  <td>\n";
    if (isset($_POST['journal_id'])) {
      $journal_id = $this->sanitize_user_input($_POST['journal_id']);
      $this->set_journal_id($journal_id);
      // get journal
      include_once("journals.php");
      $journal_obj = new Journals;
      $journal_name = $journal_obj->get_name_given_id($journal_id);
      print "    " . $journal_name . "\n";
      print "    <input type=\"hidden\" name=\"journal_id\" value=\"" . $journal_id . "\" />\n";
    } else {
      $ready_to_insert = 0;
      // select
      print "    <select name=\"journal_id\">\n";
      print "      <option value=\"2\">withdrawal</option>\n";
      print "      <option value=\"1\">deposit</option>\n";
      print "    </select>\n";
    }
    print "  </td>\n";
    print "</tr>\n";

    if (isset($_POST['id'])) {
      // skip
    } else {
      if (! $ready_to_insert) {
        print "<tr>\n";
        print "  <td>\n";
        print "    &nbsp;\n";
        print "  </td>\n";
        print "  <td>\n";
        if ($this->get_id()) {
          print "    <input type=\"submit\" value=\"Edit\" />\n";
        } else {
          print "    <input type=\"submit\" value=\"Add\" />\n";
        }
        print "  </td>\n";
        print "</tr>\n";
      }
    }

    print "</table>\n";
    print "</form>\n";

    if ($ready_to_insert) {
      if (isset($_POST['id'])) {
        // update
        print "<p>Updating database...</p>\n";
        $type = "update_posting";
        $error_message = $this->get_db_dash()->update($this, $type);
        if ($error_message) {
          print "<p>There was an error.</p>\n";
          print "<p>" . $error_message . "</p>\n";
        } else {
          print "<p>Edit was successful!</p>\n";
          $url = $this->url("budgets/" . $this->get_budget_id());
          print "<p><a href=\"" . $url . "\">budget</a>\n";
        }
      } else {
        print "<p>Adding to database...</p>\n";
        $type = "insert_posting";
        $error_message = $this->get_db_dash()->insert($this, $type);
        if ($error_message) {
          print "<p>There was an error.</p>\n";
          print "<p>" . $error_message . "</p>\n";
        } else {
          print "<p>Add was successful!</p>\n";
          $url = $this->url("budgets/" . $this->get_budget_id());
          print "<p><a href=\"" . $url . "\">budget</a>\n";
          print "<p><a href=\"posting.php?command=record&amp;budget_id=" . $this->get_budget_id(). "\">add posting</a></p>\n";
        }
      }
    } else {
      print "<p>Please, complete the form.</p>\n";
    }
    //      if ($type == "insert_posting") {
    //        $this->sql_statement = "INSERT INTO postings (account_id, journal_id, asset_type_id, amount, budget_id) VALUES (" . $instance->get_account_id() . ", " . $instance->get_journal_id() . ", " . $instance->get_asset_type_id() . ", '" . $instance->get_amount() . "', " . $instance->get_budget_id() . ");";
    //
    //      }
  }

  // from postings
  // method form
  public function print_add_posting() {
    $markup = "";

    $markup .= "<a href=\"posting?command=record&amp;budget_id=" . $this->get_given_budget_id(). "\">add posting</a>\n";

    return $markup;
  }

  // method
  public function url($given_string) {
    // todo move base_url to a config file
    $base_url = $this->get_given_config()->get_base_url();
    return $base_url . $given_string;
  }

}
