<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-22

// about this class
// This class helps to draw shapes

class Point3D  {

  // attributes
  private $x;
  private $y;
  private $z;

  // method
  function __construct($given_x, $given_y, $given_z) {
    $this->x = $given_x;
    $this->y = $given_y;
    $this->z = $given_z;
  }

  // method
  public function get_data() {
    return array($this->x, $this->y, $this->z);
  }

  // method
  public function print_data() {
    $markup = "";

    $markup .= "(" . $this->x . ", " . $this->y . ", " . $this->z . ")";

    return $markup;
  }
}

class Cube3D {

  // attributes
  private $vertices;

  // method
  function __construct($point_obj_1, $point_obj_2, $point_obj_3, $point_obj_4, $point_obj_5, $point_obj_6, $point_obj_7, $point_obj_8) {

    $this->vertices = array();
    array_push($this->vertices, $point_obj_1);
    array_push($this->vertices, $point_obj_2);
    array_push($this->vertices, $point_obj_3);
    array_push($this->vertices, $point_obj_4);
    array_push($this->vertices, $point_obj_5);
    array_push($this->vertices, $point_obj_6);
    array_push($this->vertices, $point_obj_7);
    array_push($this->vertices, $point_obj_8);
    // debug
    //print "debug model vertices count = " . count($this->vertices) . "<br />\n";

  }

  // method
  public function get_array() {
    // return an array
    return $this->vertices;
  }

  // method
  public function print_data() {
    $markup = "";

    $markup .= "Cube3D:<br />\n";

    // debug
    //print "debug model array count = " . count($this->get_array()) . "<br />\n";

    // loop
    foreach ($this->get_array() as $point_obj) {
      $markup .= "&nbsp;&nbsp;point = " . $point_obj->print_data() . "<br />\n";
    }

    return $markup;
  }

}
