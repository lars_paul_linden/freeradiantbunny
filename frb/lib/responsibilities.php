<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/responsibilities.php

include_once("scrubber.php");

class Responsibilities extends Scrubber {

  // given
  private $given_sort;
  private $given_responsibility;

  // given_sort
  public function set_given_sort($var) {
    $this->given_sort = $var;
  }
  public function get_given_sort() {
    return $this->given_sort;
  }

  // given_responsibility
  public function set_given_responsibility($var) {
    $this->given_responsibility = $var;
  }
  public function get_given_responsibility() {
    return $this->given_responsibility;
  }

  // derived state
  private $derived_state_colors = array(
    'RESPONSIBLE' => '#A6D785',
    'NOT_RESPONSIBLE' => '#CD5555',
    'PUSH' => '#EFEFEF',
    'DEFAULT' => '#000000'
   );

  // attributes
  private $id;
  private $performance_measurement;
  private $estimated_duration;
  private $triggering_agent;
  private $how_often;

  // derived state
  private $derived_state = "DEFAULT";

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // performance_measurement
  public function set_performance_measurement($var) {
    $this->performance_measurement = $var;
  }
  public function get_performance_measurement() {
    return $this->performance_measurement;
  }

  // estimated_duration
  public function set_estimated_duration($var) {
    $this->estimated_duration = $var;
  }
  public function get_estimated_duration() {
    return $this->estimated_duration;
  }

  // triggering_agent
  public function set_triggering_agent($var) {
    $this->triggering_agent = $var;
  }
  public function get_triggering_agent() {
    return $this->triggering_agent;
  }

  // how_often
  public function set_how_often($var) {
    $this->how_often = $var;
  }
  public function get_how_often() {
    return $this->how_often;
  }

  // derived_state
  public function set_derived_state_as_responsible() {
    $this->derived_state = "RESPONSIBLE";
  }
  public function set_derived_state_as_not_responsible() {
    $this->derived_state = "NOT_RESPONSIBLE";
  }
  public function set_derived_state_as_push() {
    $this->derived_state = "PUSH";
  }
  public function get_derived_state() {
    if ($this->derived_state == "DEFAULT") {
      // calculate it
      $this->calculate_derived_state();
    }
    return $this->derived_state;
  }
  public function get_derived_state_color() {
    $derived_state = $this->get_derived_state();
    $derived_state_colors_array = $this->derived_state_colors;
    return $derived_state_colors_array[$derived_state];
  }

  // method
  public function make_responsibility() {
    $obj = new Responsibilities($this->get_given_config());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {
    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug responsibilities type = " . $this->get_type() . "<br />\n";;
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT responsibilities.* FROM responsibilities WHERE responsibilities.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT responsibilities.* FROM responsibilities ORDER BY responsibilities.performance_measurement;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    // todo might have to define database_name below
    $database_name = "";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  public function transfer($results) {
    $markup = "";

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_responsibility();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_performance_measurement(pg_result($results, $lt, 1));
        $obj->set_estimated_duration(pg_result($results, $lt, 2));
        $obj->set_triggering_agent(pg_result($results, $lt, 3));
        $obj->set_how_often(pg_result($results, $lt, 4));
      }
    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }

    return $markup;
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    if ($this->get_given_id()) {
      // single instance
      $markup .= "<div class=\"subsubmenu\">\n";
      $url = $this->url("responsibilities");
      $markup .= "<p><a href=\"" . $url . "\">All responsibilities</a></p>\n";
      $markup .= "<br />\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function output_aggregate() {
    $markup = "";

        $markup .= "<table class=\"plants\">\n";

        // heading
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    #\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    id\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    performance_measurement\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    estimated_duration<br />\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    triggering_agent<br />\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    how_often<br />\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $num = 0;
        foreach ($this->get_list_bliss()->get_list() as $responsibility) {

          // process
          $markup .= "<tr>\n";

          $num++;
          $markup .= "  <td class=\"mid\">\n";
          $markup .= "    " . $num . "<br />\n";
          $markup .= "  </td>\n";

          $markup .= "  <td class=\"mid\" align=\"left\">\n";
  	      $url = $this->url("responsibilities/" . $responsibility->get_id());
          $markup .= "<a href=\"" . $url . "\">";
          $markup .= $responsibility->get_id();
          $markup .= "</a>\n";
          $markup .= "  </td>\n";

          $markup .= "  <td class=\"mid\" align=\"left\">\n";
          $markup .= "    " . $responsibility->get_performance_measurement() . "<br />\n";
          $markup .= "  </td>\n";

          $markup .= "  <td class=\"mid\" align=\"left\">\n";
          $markup .= "    " . $responsibility->get_estimated_duration() . "<br />\n";
          $markup .= "  </td>\n";

          $markup .= "  <td class=\"mid\" align=\"left\">\n";
          $markup .= "    " . $responsibility->get_triggering_agent() . "<br />\n";
          $markup .= "  </td>\n";

          $markup .= "  <td class=\"mid\" align=\"left\">\n";
          $markup .= "    " . $responsibility->get_how_often() . "<br />\n";
          $markup .= "  </td>\n";

          $markup .= "</tr>\n";
        }
        $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function output_given_variables() {
  }

  // method
  private function calculate_derived_state() {

    if (! $this->get_given_responsibility()) {
      // not ok
      return $this->set_derived_state_as_not_responsible();
    }

    // the user uses this to know if the task has been accomplished

    // get today's date
    include_once("dates.php");
    $date_obj = new Dates();
    $now_date = $date_obj->get_now_date();

    // calculate days_diff
    $days_diff = $date_obj->get_days_diff_fine($this->get_given_sort(), $now_date);

    // debug
    //print "debug responsibilities given_sort = " . $this->get_given_sort() . "<br />\n";
    //print "debug responsibilities now_date = " . $now_date . "<br />\n";
    //print "debug responsibilities days_diff = " . $days_diff . "<br />\n";
    //print "debug responsibilities given_responsibility = " . $this->get_given_responsibility() . "<br />\n";
    //print "debug responsibilities<br />\n";

    if ($days_diff <= $this->get_given_responsibility()) {
      // ok
      $this->set_derived_state_as_responsible();
      // all done, so exit function
      return;
    }

    // not ok
    return $this->set_derived_state_as_not_responsible();
  }

  // method
  public function get_report_with_link() {
    $markup = "";

    $markup .= "<span style=\"padding: 2px; background-color: " . $this->get_derived_state_color() . "\">";
    $url = $this->url("responsibilities/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_performance_measurement()) {
      $markup .= $this->get_performance_measurement();
      $markup .= " ";
      $markup .= "<em>";
      $markup .= $this->get_how_often();
      $markup .= "</em>";
    } else {
      // note: is this too dangerous or just a good fallback plan
      if ($this->get_given_id()) {
        $markup .= $this->get_given_id();
      } else {
        // no data was found, so return null string
        print "error responsibilites: no id or given_id<br />\n";
        return "";
      }
    }
    $markup .= "</a>";
    $markup .= "</span>\n";

    return $markup;
  }

  // method
  public function get_report_background_color($given_responisibility_count, $given_sort) {

    // debug
    //print "debug responsibilities given_responsibility = " . $given_responisibility_count . "<br />\n";

    // store parameter
    $this->set_given_sort($given_sort);
    $this->set_given_responsibility($given_responisibility_count);

    return $this->get_derived_state_color();
  }

}
