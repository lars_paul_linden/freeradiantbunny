<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/sproject.php

include_once("scrubber.php");

abstract class Sproject extends Scrubber {

  // given
  private $given_project_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attribute
  private $project_obj;

  // project_obj
  public function get_project_obj() {
    if (! isset($this->project_obj)) {
      // note the code below gets a file in another directory
      include_once(dirname(__DIR__) . "/projects.php");
      $this->project_obj = new Projects($this->get_given_config());
    }
    return $this->project_obj;
  }

}
