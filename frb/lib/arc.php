<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-06
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/arc.php

class Arc {

  private $scenes_actor_1;
  private $scenes_actor_2;
  private $display_flag = 1;

  // scenes_actor_1
  public function set_scenes_actor_1($var) {
    $this->scenes_actor_1 = $var;
  }
  public function get_scenes_actor_1() {
    return $this->scenes_actor_1;
  }

  // scenes_actor_2
  public function set_scenes_actor_2($var) {
    $this->scenes_actor_2 = $var;
  }
  public function get_scenes_actor_2() {
    return $this->scenes_actor_2;
  }

  // display_flag
  public function get_display_flag() {
    return $this->display_flag;
  }
  // method
  public function evaluate() {
    $markup = "";

    // same object at both ends
    // this is the diagnal of the matrix
    if ($this->get_scenes_actor_1() === $this->get_scenes_actor_2()) {

      // debug
      $markup .= "debug arc <span style=\"color: #99DD00; font-weight: 600;\">diagnal</span><br />\n";

      // turn off
      $this->display_flag = 0;

    } else {

      // debug
      $markup .= "debug arc setting valid arc<br />\n";

    }
  }
}
