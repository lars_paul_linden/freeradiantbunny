<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-22

// about this class
// This class holds data in the icalendar format. Theses data hold shifts.
// From wikipedia: "For example, the calendar component can specify an event, a to-do list, a journal entry, time zone information, or free/busy time information, or an alarm."

class Icalendars {

  // attributes
  private $dtstart;
  private $dtend;
  private $summary;
  private $attach;

  // dtstart
  public function set_dtstart($var) {
    $this->dtstart = $var;
  }
  public function get_dtstart() {
    return $this->dtstart;
  }
  public function get_dtstart_simple() {
    $dtstart = $this->dtstart;
    // date
    $start = 4;
    $length = 4;
    $simple = substr($dtstart, $start, $length);
    // start time
    $start = 9;
    $length = 4;
    $simple .= "-" . substr($dtstart, $start, $length);
    return $simple;
  }

  // dtend
  public function set_dtend($var) {
    $this->dtend = $var;
  }
  public function get_dtend() {
    return $this->dtend;
  }
  public function get_dtend_simple() {
    $dtend = $this->dtend;
    // end time
    $start = 9;
    $length = 4;
    $simple = substr($dtend, $start, $length);
    return $simple;
  }

  // summary
  public function set_summary($var) {
    $this->summary = $var;
  }
  public function get_summary() {
    return $this->summary;
  }

  // attach
  // example: ATTACH:http://eatlocalfreshfood.org/index.html
  public function set_attach($var) {
    $this->attach = $var;
  }
  public function get_attach() {
    return $this->attach;
  }
  public function get_attach_as_link() {
    return "<a href=\"" . $this->attach . "\">" . $this->attach . "</a>\n";
  }

  // method
  public function get_whole($given_icalendar_data) {
    $markup = "";

    $lines = explode("\n", $given_icalendar_data);

    if ($lines) {
      foreach ($lines as $line) {
        $markup .= $line . "\n";
      }
    }

    return $markup;
  }

  // method
  public function extract_variables($given_icalendar_data) {

    $lines = explode("\n", $given_icalendar_data);

    if ($lines) {
      foreach ($lines as $line) {

        if ($line == "BEGIN:VCALENDAR") {
          //print "exact begin calendar<br />\n";
        } else if ($line == "BEGIN:VEVENT") {
          //print "exact begin event<br />\n";
        } else if ($line == "END:VEVENT") {
          //print "exact end event<br />\n";
        } else if ($line == "END:VCALENDAR") {
          //print "exact end calendar<br />\n";
        } else {
          // process all the strings that are not exact
          // dtstart
          $field = "DTSTART:";
          $found = $this->search_for($line, $field);
          if ($found) {
            $this->set_dtstart($found);
            //print $field . " " . $this->get_dtstart() . "<br />\n";
          }
          // dtend
          $field = "DTEND:";
          $found = $this->search_for($line, $field);
          if ($found) {
            $this->set_dtend($found);
            //print $field . " " . $this->get_dtend() . "<br />\n";
          }
          // dtend
          $field = "SUMMARY:";
          $found = $this->search_for($line, $field);
          if ($found) {
            $this->set_summary($found);
            //print $field . " " . $this->get_summary() . "<br />\n";
          }
          // attach
          $field = "ATTACH:";
          $found = $this->search_for($line, $field);
          if ($found) {
            $this->set_attach($found);
            //print $field . " " . $this->get_attach() . "<br />\n";
          }
        }
      }
    }
  }

  // method
  private function search_for($given_line, $given_field) {

    $pattern = "/^$given_field(.*)/";

    preg_match($pattern, $given_line, $matches);

    if (count($matches) > 0) {
      $found = $matches[1];
      return $found;
    }

    // no match, so return null string
    return "";
  }

}
