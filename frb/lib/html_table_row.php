<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/html_table_row.php

class HtmlTableRow {

  // attributes
  // a row is an array of cells
  private $row_of_cells = array();

  // method
  public function makeHmtlTableCell($given_cell_array) {
    include_once("html_table_cell.php");
    $cell = new HtmlTableCell();
    array_push($this->row_of_cells, $cell);
    $cell->set_element_class($given_cell_array[0]);
    $cell->set_data($given_cell_array[1]);
    $cell->set_visibility($given_cell_array[2]);
    $cell->set_styles($given_cell_array[3]);
  }

  // method
  public function makeHmtlTableCellFromGivenMarkup($given_markup) {
    include_once("html_table_cell.php");
    $cell = new HtmlTableCell();
    array_push($this->row_of_cells, $cell);
    $cell->set_given_markup($given_markup);
  }

  // method 
  public function craft_row($given_requested_visibility) {
    $markup = "";

    // craft data cells
    $markup .= "<tr>\n";
    foreach ($this->row_of_cells as $cell) {
      if ($cell->get_given_markup()) {
        $markup .= $cell->get_given_markup();
      } else {
        $markup .= $cell->craft_cell($given_requested_visibility);
      }
    }
    $markup .= "</tr>\n";

    return $markup;
  }

}
