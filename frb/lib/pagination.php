<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-22

// about this class
// This class allows a big long list to be served up in smaller lists.

include_once("scrubber.php");

class Pagination extends Scrubber {

  // attributes
  private $count_per_page = 12;     // number of websites in a list
  private $last_rank;               // rank of last website on previous page
  private $rows_on_this_page = 12;  // total number of rows on this page
  private $total_row_count;         // total number of rows possible in search
  private $reason_name = "";        // for a specific list (e.g., qualifier)

  // count_per_page
  public function get_count_per_page() {
    return $this->count_per_page;
  }

  // last_rank
  public function set_last_rank($last_rank) {
    $this->last_rank = $this->get_validation_obj()->sanitize_input_as_integer($last_rank);
  }
  public function get_last_rank() {
    if (! isset($this->last_rank)) {
      // default
      $this->last_rank = 0;
    }
    return $this->last_rank;
  }

  // rows_on_this_page
  public function get_rows_on_this_page() {
    return $this->rows_on_this_page;
  }

  // method
  protected function determine_type() {
    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {

    } else if ($this->get_type() == "get_all") {

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  public function transfer($results) {
    // database work
    if ($this->get_type() == "get_hyperlinks_row_count" ||
        $this->get_type() == "get_given_reason_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->total_row_count = pg_result($results, $lt, 0);
      }
    } else {
      $this->get_db_dash()->print_error("Sorry, " . get_class($this) . " does not know the type.");
    }
  }

  // method
  public function get_total_rows() {
    $markup = "";

    if (! isset($total_row_count)) {
      $sql = "";
      if ($this->get_reason_name()) {
        $this->set_type("get_given_reason_row_count");
        $sql = "SELECT count(hyperlinks.id) FROM hyperlinks, reasons, hyperlink_reasons WHERE hyperlinks.id = hyperlink_reasons.hyperlink_id and reasons.id = hyperlink_reasons.reason_id and reasons.name = '" . $instance->get_reason_name() . "';";
      } else {
        $this->set_type("get_hyperlinks_row_count");
        $sql = "SELECT count(*) FROM hyperlinks";
      }
      $database_name = "mudiacom_psites";
      $markup .= parent::load_data($this, $sql, $database_name);
      // todo deal with markup above
    }

    return $this->total_row_count;
  }

  // reason_name
  public function set_reason_name($var) {
    $this->reason_name = $var;
  }
  public function get_reason_name() {
    return $this->reason_name;
  }

  // extra_parameters
  public function get_extra_parameters() {
    if ($this->get_reason_name() == "") {
      // no extra parameter
      return "";
    }
    return "&amp;qualifier=" . $this->get_reason_name();
  }

  // this_last_rank
  private function get_this_last_rank() {
    // derived
    if (($this->get_last_rank() + $this->get_rows_on_this_page()) > $this->get_total_rows()) {
      // not a full page, so print total row count
      return $this->get_total_rows();;
    }
    return $this->get_last_rank() + $this->get_rows_on_this_page();
  }

  // count_on_next_page
  private function get_count_on_next_page() {
    // returns a derived value of how many links are on the next page

    // next page is empty (i.e., there is no next page)
    if ($this->get_total_rows() <= $this->get_this_last_rank()) {
      return 0;
    }

    // next page is partially full
    $next_page_last_rank = $this->get_total_rows() - $this->get_this_last_rank();
    if ($next_page_last_rank < $this->get_count_per_page()) {
      return $next_page_last_rank;
    }

    // next page is full
    return $this->get_count_per_page();
  }

  // method
  public function output_pagination_random($pagename = "index.php") {
    $markup = "";

    // pagination if data_sort == random
    $markup .= "<div class=\"pagination\">\n";
    $markup .= "<a href=\"" . $pagename . "\">more</a>\n";
    //$markup .= "<br />\n";
    $markup .= "<br />\n";
    // todo wire up
    $data_sort = "";
    $markup .= "<ul>\n";
    $markup .= "  <li>The list above is in " . $data_sort . " order. View the list in ";
    // toggle data-sort
    if ($data_sort != "alphabetical") {
      $markup .= "<a href=\"" . $pagename . "?data-sort=alphabetical\">alphabetical order</a>";
    } else {
      $markup .= "<a href=\"" . $pagename . "\">random order</a>";
    }
    $markup .= ".</li>\n";
    $markup .= "</ul>\n";
    $markup .= "</div><!-- pagination end -->\n";

    return $markup;
  }

  // method
  public function output_pagination_alphabetical($pagename = "hyperlinks") {
    $markup = "";

    // this function does too much
    // because it is RESTful for pws potentials and parameters for pws home

    // pagination when data_sort == alphabetical
    $markup .= "<div class=\"pagination\">\n";

    // debug
    //$markup .= "<p>debug pagination: total rows on this page = " . $this->get_rows_on_this_page() . "</p>\n";
    //$markup .= "<p>debug pagination: total rows = " . $this->get_total_rows() . "</p>\n";
    //$markup .= "<p>debug pagination: this last rank = " . $this->get_this_last_rank() . "</p>\n";

    // todo problem that potentials uses RESTful and pws home uses parameters
    if ($pagename == "home" || $pagename == "hyperlinks") {
      // get parameters
      if (isset($_GET['last_rank'])) {
        // filter
        include_once("validation.php");
        $validation_obj = new Validation();
        $this->last_rank = $validation_obj->sanitize_input_as_integer($_GET['last_rank']);
      }
    }

    // debug
    //$markup .= "<p>debug pagination: this last rank = " . $this->get_this_last_rank() . "</p>\n";

    // output navigation
    // start by setting up a variable
    $previous_last_rank =  $this->get_last_rank() - $this->get_count_per_page();
    // check floor
    // prevent previous last rank from being a negative number
    if ($previous_last_rank < 0) {
      $previous_last_rank =  0;
    }

    if ($this->get_this_last_rank() > $this->get_count_per_page() && $previous_last_rank > 0) {
      $url = url($pagename);
      if ($pagename == "home" || $pagename == "hyperlinks") {
        $url = $pagename . "?data-sort=alphabetical";
      }
      $markup .= "<a href=\"" . $url . "\">beginning</a>";
      $markup .= "&nbsp;&nbsp;&nbsp;";
    }
    if ($this->get_this_last_rank() <= $this->get_count_per_page()) {
      // first page, so skip this link
    } else {
      // last page
      $url = url($pagename . "/alphabetical/" . $previous_last_rank);
      if ($pagename == "home" || $pagename == "hyperlinks") {
        $url = $pagename . "?data-sort=alphabetical&last_rank=" . $previous_last_rank;
      }
      $markup .= "<a href=\"" . $url . "\">previous</a>";
    }
    $markup .= "&nbsp;&nbsp;&nbsp;";
    if ($pagename == "home") {
      // skip
    } else {
      // display the rank range for the hyperlinks being displayed
      $markup .= $this->get_last_rank() + 1;
      $markup .= " - ";
      $markup .= $this->get_this_last_rank();
      $markup .= "&nbsp;&nbsp;&nbsp;";
    }
    if ($this->get_total_rows() > $this->get_this_last_rank()) {
      // not last page
      $url = url($pagename . "/alphabetical/" . $previous_last_rank);
      if ($pagename == "home" || $pagename == "hyperlinks") {
        $next_page_rank = $this->get_last_rank() + $this->get_count_per_page();
        if ($next_page_rank > $this->total_row_count) {
          $next_page_rank = $this->get_last_rank() + $this->get_count_on_next_page();
        }
        $url = $pagename . "?data-sort=alphabetical&last_rank=" . $next_page_rank;
      }
      $markup .= "<a href=\"" . $url. "\">next</a>";
    }
    $markup .= "\n";
    $markup .= "</div><!-- pagination end -->\n";
    $markup .= "<br />\n";

    return $markup;
  }

}
