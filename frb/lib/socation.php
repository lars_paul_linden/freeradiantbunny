<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2015-01-03
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/socation.php

include_once("standard.php");

abstract class Socation extends Standard {

  // attributes
  private $width = "40";  #default
  private $height = "80"; #default

  // width
  public function set_width($var) {
    $this->width = $var;
  }
  public function get_width() {
    return $this->width;
  }
  // height
  public function set_height($var) {
    $this->height = $var;
  }
  public function get_height() {
    return $this->height;
  }

  // method
  public function output_sprite($given_process_id, $given_user_obj, $given_heading) {
    $markup = "";

    $markup .= "<div style=\"padding: 2px 2px 1px 2px; background-color: #EFEFEF; text-align: center; border-style: dashed; border-width: 1px; border-color: blue; display: inline-block;\">\n";
    $padding = " 0px 0px 0px 0px";
    $float = "";
    $markup .= $this->get_img_as_img_element_with_link($padding, $float, $this->get_width(), $this->get_height()) . "<br/>\n";
    $markup .= "<span style=\"font-size: 80%; color: blue;\">" . $this->get_name() . "</span><br />\n";
    $markup .= "<span style=\"font-size: 80%; padding: 2px 2px 1px 2px;background-color: #CCCCCC; color: blue;\"><em>" . get_class($this) . "</em></span><br />\n";
    $markup .= "</div>\n";

    //$markup .= "<em>put_on_play()</em><br />";
    //$markup .= "<em>given_process_id = " . $given_process_id . "</em><br />";
    //$markup .= "<em>class = " . get_class($this) . "</em><br />";
    //$markup .= "<em>primary_key = " . $this->get_id() . "</em><br />";
    //$markup .= "<em>sort = " . $this->get_sort() . "</em><br />";
    //$markup .= "<br />\n";

    //$markup .= "list the processes instance that this is for<br />";
    //$markup .= "list sprites (locations instances)<br />";
    //$markup .= "instantiate a stage<br />";
    //$markup .= "get instructions from the director<br />";
    //$markup .= "* who gets instructions on design from the stage designer (g)<br />";
    //$markup .= "* who gets instructions on sound from the technician (s)<br />";
    //$markup .= "log these details and ask if there enough memory and time?<br />";
    //$markup .= "* scale?<br />";
    //$markup .= "play<br />";

    return $markup;
  }

  // method
  private function key_compare_func($a, $b) {
    // use something simple for now
    //if (! is_object($a) || ! is_object($b)) {
    //  // debug
    //  print "debug scene_elements cannot compare<br />\n";
    //  return 0;
    //}
    // alphabetical
    $a_up = $a->get_name();
    $b_up = $b->get_name();
    // by id
    //$a_up = $a->get_id();
    //$b_up = $b->get_id();
    if ($a_up == $b_up){
      return 0;
    }
    return ($a_up < $b_up) ? -1 : 1;
  }

  // method
  public function get_location_sprite() {
    $markup = "";

    // this function can be over-ridden by the child of Locations class

    // debug
    //print "debug socaationget_location_sprite() width = " . $this->get_width() . "<br />\n";

    // default values
    $sprite = array("width" => "65", "height" => "80");

    // change at location child level
    $this->set_width($sprite["width"]);
    $this->set_height($sprite["height"]);

    // debug
    $markup .= "debug socation sprite initial width = " . $this->get_width() . "<br />\n";
    $markup .= "debug socation sprite initial height = " . $this->get_height() . "<br />\n";

    return $markup;
  }

  // method
  public function relate($given_socation_obj) {
    // debug
    return "debug socation: relate() function is not defined in class<br />\n";
  }

  // method
  public function get_sprite() {

    // debug
    //print "debug lands width = " . $width . "<br />\n";

    $sprite = array("width" => "800", "height" => "400");

    return $sprite;
  }

}
