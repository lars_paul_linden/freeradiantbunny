<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/standard.php

include_once("sproject.php");

abstract class Standard extends Sproject {

  // note that this class has no setters

  // given
  private $given_make_sort_today_id;

  // given_make_sort_today_id
  public function set_given_make_sort_today_id($var) {
    $this->given_make_sort_today_id = $var;
  }
  public function get_given_make_sort_today_id() {
    return $this->given_make_sort_today_id;
  }

  // attributes
  private $id;           // primary key
  private $name;         // short text
  private $description;  // longer text
  private $img_url;      // url of image of icon
  private $sort;         // holds "Z" date last updated
  private $status;       // holds freeform text
  private $timekeeper_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }
  public function get_primary_key() {
    return $this->get_id();
  }
  public function get_class_primary_key_string() {
    return $this->get_id();
  }
  public function get_id_with_link() {
    $markup = "";

    include_once("factory.php");
    $factory = new Factory($this->get_given_config());
    $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_id()) {
      $markup .= $this->get_id();
    } else {
      // note: is this too dangerous or just a good fallback plan
      if ($this->get_given_id()) {
        $markup .= $this->get_given_id();
      } else {
        // no data was found, so return null string
        print "error standard: no id or given_id<br />\n";
        print "error standard: subclass = " . get_class($this) . "<br />\n";
        return "";
      }
    }
    $markup .= "</a>";

    return $markup;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }
  public function get_name_with_link($given_target_directory = "") {
    $markup = "";

    include_once("factory.php");
    $factory = new Factory($this->get_given_config());
    $url = "";
    if ($given_target_directory) {
      $url = $given_target_directory . "/" . $factory->get_class_name_given_object($this) . "/" . $this->get_id();
    // debug
    //print "debug lib/standard given_target_directory = " . $given_target_directory . "<br />\n";
    //print "debug url = " . $url . "<br />\n";

    } else {
      $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
    }
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_name()) {
      $markup .= $this->get_name();
    } else {
      if ($this->get_id()) {
        $markup .= $this->get_id();
      } else {
        // note: is this too dangerous or just a good fallback plan
        if ($this->get_given_id()) {
          $markup .= $this->get_given_id();
        } else {
          // no data was found, so return null string
          print "error standard: no id or given_id<br />\n";
          print "error standard: subclass = " . get_class($this) . "<br />\n";
          return "";
        }
      }
    }
    $markup .= "</a>";

    return $markup;
  }
  public function get_name_with_hidden_link() {
    $markup = "";

    include_once("factory.php");
    $factory = new Factory();
    $url = $this->url($factory->get_class_name_as_filename_style($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"noshow\">";
    if ($this->get_name()) {
      $markup .= $this->get_name();
    } else {
      if ($this->get_id()) {
        $markup .= $this->get_id();
      } else {
        // note: is this too dangerous or just a good fallback plan
        if ($this->get_given_id()) {
          $markup .= $this->get_given_id();
        } else {
          // no data was found, so return null string
          print "error standard: no id or given_id<br />\n";
          print "error standard: subclass = " . get_class($this) . "<br />\n";
          return "";
        }
      }
    }
    $markup .= "</a>";

    return $markup;
  }
  public function get_name_with_link_natural() {
    $markup = "";

    if ($this->get_name()) {
      $name = $this->get_name();
    } else {
      // no name, so try id
      if ($this->get_id()) {
        $name = "id=" . $this->get_id();
      } else {
        // no id, so try question mark
        $name = "?";
      }      
    }
    include_once("factory.php");
    $factory = new Factory($this->get_given_config());
    $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    $markup .= $name;
    $markup .= "</a>";
    return $markup;
  }
  public function get_name_or_img_if_img_exists() {
    $markup = "";

    // display based upon context
    $width = "65";
    $img = $this->get_img_as_img_element_with_link("", "", $width);
    if ($img) {
      // display image
      $markup .= $img;
    } else {
     // display name with link
      // may need to find the data, so first check if it is there
      if (! $process->get_business_plan_text_obj()->get_project_obj()->get_id()) {
        // there is not projects.id, so get it
        // get the projects.name and projects.id
        $business_plan_text_id = $process->get_business_plan_text_obj()->get_id();
        // debug
        //print "debug processes business_plan_text_id = " . $business_plan_text_id . "<br  />\n";

        $user_obj = $this->get_user_obj();
        $project_id = $process->get_business_plan_text_obj()->get_project_id_given_business_plan_text_id($business_plan_text_id, $user_obj);

        // debug
        //print "debug processes project_id = " . $project_id . "<br  />\n";

        // now store
        $process->get_business_plan_text_obj()->get_project_obj()->set_id($project_id);
      }
      $markup .= "    " . $process->get_business_plan_text_obj()->get_project_obj()->get_name_with_link() . "\n";
    }

    return $markup;
  }

  // description
  public function set_description($var) {
    $this->description = $var;
  }
  public function get_description() {
    return $this->description;
  }
  public function set_description_html_encoded($var) {
    $this->description = html_entity_decode($var);
  }
  public function get_description_html_encoded() {
    return htmlentities($this->description);
  }

  // sort
  public function set_sort($var) {
    $this->sort = $var;
  }
  public function get_sort() {
    return $this->sort;
  }
  public function get_sort_no_z() {
    // assumes "Z " is at the beginning of string
    return substr($this->sort, 2);
  }

  // sort data as hyperlink
  public function get_sort_as_button() {
    $markup = "";

    // note set url
    $url = "";

    include_once("dates.php");
    $date_obj = new Dates();
    $sort_no_z = $this->get_sort_no_z();
    if ($date_obj->is_today($sort_no_z)) {
      // today, so no link
      $markup .= $this->get_sort();
      // todo this if else clause should not be here so un-hardcode this
    } else {
      // not today, so link button
      // figure out the class of this instance
      $this_class = get_class($this);
      // todo this if else clause should not be here so un-hardcode this
      if ($this_class == "Projects") {
        // make url
        $url .= $this->url("projects");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "GoalStatements") {
        // make url
        $url .= $this->url("goal_statements");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Builds") {
        // make url
        $url .= $this->url("builds");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "BusinessPlanTexts") {
        // make url
        $url .= $this->url("business_plan_texts");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Classes") {
        // make url
        $url .= $this->url("classes");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Accounts") {
        // make url
        $url .= $this->url("accounts");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Suppliers") {
        // make url
        $url .= $this->url("suppliers");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Machines") {
        // make url
        $url .= $this->url("machines");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "PermacultureTopics") {
        // make url
        $url .= $this->url("permaculture_topics");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Songs") {
        // make url
        $url .= $this->url("songs");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Processes") {
        // make url
        $url .= $this->url("processes");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        if ($this->get_status() == "online") {
          $url .= "status=online";
          $url .= "&amp;";
        }
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "SceneElements") {
        // make url
        $url .= $this->url("scene_elements");
        $url .= "/?";
        // note deal with parameters of this class
        // add other parameters here
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Applications") {
        // make url
        $url .= $this->url("applications");
        $url .= "/?";
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Maxonomies") {
        // make url
        $url .= $this->url("maxonomies");
        $url .= "/?";
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "PlantLists") {
        // make url
        $url .= $this->url("plant_lists");
        $url .= "/" . $this->get_id();
        // note deal with parameters of this class
        // add other parameters here
        $url .= "?";
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Webpages") {
        // make url
        $url .= $this->url("webpages");
        $url .= "/" . $this->get_id();
        // note deal with parameters of this class
        // add other parameters here
        $url .= "?";
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Moneymakers") {
        // make url
        $url .= $this->url("moneymakers");
        $url .= "/" . $this->get_id();
        // note deal with parameters of this class
        // add other parameters here
        $url .= "?";
        $url .= "make-sort-today=" . $this->get_id();
      } else if ($this_class == "Tickets") {
        // make url
        $url .= $this->url("tickets");
        $url .= "/" . $this->get_id();
        // note deal with parameters of this class
        // add other parameters here
        $url .= "?";
        $url .= "make-sort-today=" . $this->get_id();
      }
      if ($url) {
        $markup .= "<a href=\"" . $url . "\" style=\"text-decoration: none;\">";
      }
      $markup .= $this->get_sort();
      if ($url) {
        $markup .= "</a>";
      }
    }

    return $markup;
  }

  // status
  public function set_status($var) {
    $this->status = $var;
  }
  public function get_status() {
    // dynamic get
    // returns a hyperlink if string begins with https://
    if (substr($this->status, 0, 8) === "https://") {
      $url = $this->status;
      return "<a href=\"" . $url . "\">" . $this->status . "</a>";
    }

    return $this->status;
  }

  // get_status_color
  public function get_status_background_color() {
    $markup = "#CD5555"; // default

    if ($this->get_status() == "zoneline") {
      # purple
      $markup = "#C0509F";
    } else if ($this->get_status() == "offline") {
      // alert           
      $markup = "#F08080";
    } else if ($this->get_status() == "in progress") {
      $markup = "#84BE6A";
    } else if (strpos($this->get_status(), "rowster") !== false ||                            strpos($this->get_status(), "rowster-instance") !== false) {
      $markup = "#A6D7FF";
    } else if ($this->get_status() == "dev") {
      // alert           
      $markup = "#A6D7FF";
    } else if (strpos($this->get_status(), "http:") !== false) {
      $markup = "#A4D3EE";
    }

    return $markup;
  }

  // img_url
  public function set_img_url($var) {
    $this->img_url = $var;
  }
  public function get_img_url() {
    // todo study this function
    // todo but for now it will try to by dynamic
    // todo the user can ask for offline state
    // todo and the app will replace part of the URL
    // todo with an address of the intranet webserver address
    // todo move this to the config file
    // todo so notice the wrapper that makes the localhost string
 
    // todo clean this up because this was a bad idea
    //if (get_class($this) == "ProcessProfiles") {
    //  $this->img_url = "http://mudia.com/_images/not_defined.png";
    //}

    if ($this->img_url) {
      // when img_url is in database
      return $this->make_localhost($this->img_url);

    } else if (method_exists($this, "get_img_url_default")) {
      // the option exists to run this method, so see if value exists
      if ($this->get_img_url_default()) {
        // when default img_url is needed and exists
        return $this->make_localhost($this->get_img_url_default());
      }
    }

    // note: if all else fails
    // soft else
    // when all else fails use the system version
    // move this to user config code
    // todo had to change the one below
    //$img_url_when_not_defined = "http://mudia.com/_images/not_defined.png";
    //return $this->make_localhost($img_url_when_not_defined);
    // todo need to find this image
    //return "_images/not_defined.png";
    // todo here is the replacement in the meantime
    return "http://basecamp/~blaireric/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html/dash/_images/phd.gif";
  }

  public function make_localhost($given_url) {
    // todo need to keep the variables and move these values to the config
    // 1
    $search = "http://mudia.com";
    $replace = "http://basecamp/~blaireric/dev/domains/mud/mud_up/mud_v15/mud_base/mud_html/public_html";
    $given_url = str_replace($search, $replace, $given_url);
    // 2
    $search = "http://permaculturewebsites.org";
    $replace = "http://basecamp/~blaireric/dev/domains/pws/pws_mist/pws_up/pws_v02/pws_base/pws_html/public_html";
    $localhost_url = str_replace($search, $replace, $given_url);
    return $localhost_url;
  }

  // method
  public function get_img_url_as_img_element($given_padding = "0px 0px 0px 0px", $given_float = "", $given_width = "", $given_height = "") {
    $markup = "";

    // treat parameters
    if ($given_padding == "" ||
        $given_padding == "0") {
      $given_padding = "0px 0px 0px 0px";
    }

    if (! $this->get_img_url()) {
      return "[img]";
    }

    $alt = "icon";

    // debug
    //print "debug standard: 2 given_height = " . $given_height . "<br />\n";

    // output
    $markup .= "<img src=\"" . $this->get_img_url() . "\" alt=\"" . $alt . "\" width=\"" . $given_width . "\" height=\"" . $given_height . "\" style=\"padding: " . $given_padding . ";";
    if ($given_float) {
      $markup .= " float: " . $given_float . ";";
    }
    $markup .= "\"/>";

    return $markup;
  }

  // method
  public function get_img_as_img_element_with_link($given_padding = "0px 0px 0px 0px", $given_float, $given_width = "", $given_height = "") {
    $markup = "";

    // debug
    //print "debug standard: 1 given_height = " . $given_height . "<br />\n";

    include_once("factory.php");
    $factory = new Factory($this->get_given_config());
    $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\">" . $this->get_img_url_as_img_element($given_padding, $given_float, $given_width, $given_height) . "</a>";

    return $markup;
  }

  // method
  // only for pages that are accessing the database
  protected function determine_type() {

  }

  // method
  // force the developer to consider the database
  protected function prepare_query() {

  }

  // method
  protected function output_preface() {
    $markup = "";

    if ($this->get_given_id()) {
      foreach ($this->get_list_bliss()->get_list() as $obj) {
        // note pass along view from this to obj
        $obj->set_given_view($this->get_given_view());
        // output preface
        // pass on user_obj
        $user_obj = $this->get_user_obj();
        $obj->set_user_obj($user_obj);
        if (method_exists($obj, "output_preface_single")) {
          $markup .= $obj->output_preface_single();
        }
      }
    }

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // make sure that there is only 1 element in the array
    if (count($this->get_list_bliss()->get_list()) != 1) {
      return "Error standard: has count should be just 1 but count is " . count($this->get_list_bliss()->get_list()) . "<br />\n";
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) {

      // output single nonstandard frontrunner
      // pass on user_obj
      $user_obj = $this->get_user_obj();
      $obj->set_user_obj($user_obj);
      if (method_exists($obj, "output_frontrunner")) {
        $markup .= $obj->output_frontrunner();
      }

      // create chapbook margins when printing
      $markup .= "<div class=\"chapbook\">\n";

      // img
      // todo move these variables into the config of the usr
      //$markup .= "<br /><br />\n";  // kludge!
      $padding = "10px 10px 10px 10px";
      $float = "left";
      $width = "65";
      $markup .= "    " . $obj->get_img_as_img_element_with_link($padding, $float, $width) . "\n";

      // name
      if ($obj->get_name()) {
        $markup .= "<h2 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px; width: 100%;\">" . $obj->get_name() . "</h2>\n";
      }

      // description
      //$markup .= "<p><em>description:</em></p>\n";
      if ($obj->get_description()) {
        $markup .= "<div class=\"paragraphs\">\n";
        if ($obj->get_description() == "pushed") {
          $markup .= "  <p style=\"background-color: #A474CC;\">";
        } else {
          $markup .= "  <p>";
        }
        $markup .= $obj->get_description() . "</p>\n";
        $markup .= "</div>\n";
      }

      // end chapbook margins
      $markup .= "</div>\n";

      $markup .= "<table class=\"plants\">\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header-vertical\">\n";
      $markup .= "    <em>sort</em>\n";
      $markup .= "  </td>\n";

      // note use sort cell factory
      $markup .= $obj->get_sort_cell();

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header-vertical\">\n";
      $markup .= "    <em>status</em>\n";
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $obj->get_status_background_color() . "\">\n";
      $markup .= "    " . $obj->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "</table>\n";
 
      // output nonstandard
      // pass on user_obj
      $user_obj = $this->get_user_obj();
      $obj->set_user_obj($user_obj);
      if (method_exists($obj, "output_single_nonstandard")) {
        // note pass along view from this to obj
        $obj->set_given_view($this->get_given_view());
        // debug
        //print "lib/standard this view = " . $this->get_given_view() . "<br />\n";
        //print "lib/standard obj view = " . $obj->get_given_view() . "<br />\n";
        $markup .= $obj->output_single_nonstandard();
      }
    }

    return $markup;
  }

  // method
  public function update_sort_to_today($given_table_name, $given_db_table_field, $given_sort_letter, $given_database_name = "") {
    $markup = "";

    // debug
    //print "debug standard: make_sort_today: " . $this->get_given_make_sort_today_id() . "<br />\n";;

    if ($given_table_name && $given_db_table_field) {
      // update database
      // todo move to config
      include_once("timekeeper.php");
      $timekeeper_obj = new Timekeeper();
      if ($given_db_table_field == "tli") {
        // place quotes around variable
        $sql = "UPDATE " . $given_table_name . " set sort= '" . $given_sort_letter . " " . $timekeeper_obj->get_now_date() . "' WHERE " . $given_db_table_field . " = '" . $this->get_given_make_sort_today_id() . "';";
      } else {
        // no quotes around variable
        $sql = "UPDATE " . $given_table_name . " set sort= '" . $given_sort_letter . " " . $timekeeper_obj->get_now_date() . "' WHERE " . $given_db_table_field . " = " . $this->get_given_make_sort_today_id() . ";";
        // debug
        // print "lib/standard.php debug sql = $sql\n";
      }
      $markup .= $this->get_db_dash()->new_update($this, $sql);
    }

    // debug
    //print "deug standard: error: " . $markup . "<br />\n";;

    return $markup;
  }

  // method
  public function get_sort_letter_given_id($given_id, $given_user_obj) {
    $markup = "";

    if (get_class($this) == "Domains") {
      $this->set_given_domain_tli($given_id);
    } else {
      $this->set_given_id($given_id);
    }
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $first_element = $this->get_list_bliss()->get_first_element();
      $sort = $first_element->get_sort();
      // debug
      //print "debug projects given_id = " . $this->get_given_id() . "<br />\n";
      //print "debug projects sort = " . $sort . "<br />\n";
      return substr($sort, 0, 1);
    }

    return "";

  }

  // method
  public function get_y_percent_given_project_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // no count, so count is zero
      $markup = "0";
      return $markup;
    }

    $y_count = 0;
    $total_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $total_count++;
      // see if there is a sort
      if ($obj->get_sort()) {
        // see if this sort has a first character that is a "Y"
        if (substr($obj->get_sort(), 0, 1) == "Y") {
          // found it, so increment the counter
          $y_count++;
        }
      }
    }
    // output percent
    $y_percent = ($y_count / $total_count) * 100;
    $markup = $y_percent;

    // debug
    //print "debug standards y_count = " . $y_count . "<br />\n";
    //print "debug standards total_count = " . $total_count . "<br />\n";
    //print "debug standards y_percent = " . $y_percent . "<br />\n";

    return $markup;
  }

  // method
  public function get_tenperday($given_obj){
    $markup = "";

    // load data from database
    $markup .= $this->set_type("get_all");
    $markup .= $this->prepare_query();

    if ($markup) {
      $markup .= "<p>error " . get_class($given_obj) . ": prepare_query() had an error</p>\n";
      return $markup;
    }

    // set some variables
    $padding = "10px 10px 10px 10px";
    $float = "";
    $width = "66";
    include_once("dates.php");
    $date_obj = new Dates();

    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $sort_y_date = $obj->get_sort();
      $date = $date_obj->remove_y($sort_y_date);
      // filter for today
      if ($date_obj->is_today($date)) {
        $class_name = get_class($this);
        $class_name_string = strtolower($class_name);
        $url = $this->url($class_name_string . "/" . $obj->get_id());
        $width = "20";
        $height = "25";
        $markup .= "<table border=\"0\" style=\"display: inline; vertical-align: top;\">\n";
        $markup .= "<tr>\n";
        $markup .= "<td style=\"width: 30px; text-align: center; font-size: 80%;\">\n";
        $markup .= "  <a href=\"" . $url . "\">" . $obj->get_img_url_as_img_element($padding, $float, $width, $height)  . "</a><br />\n";
        $markup .= "  <span style=\"font-size: 80%;\"><a href=\"" . $url . "\">" . $obj->get_name() . "</a></span>\n";
        $markup .= "</td>\n";
        $markup .= "</tr>\n";
        $markup .= "</table>\n";
      }
    }

    return $markup;
  }

  // method
  public function get_sort_cell($given_timecards_data = "") {
    $markup = "";

    $column_name = "sort";
    include_once("timekeeper.php");
    $timekeeper_obj = new Timekeeper();
    $color = $timekeeper_obj->calculate_cell_color($column_name, $this->get_sort());
    $markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle; width: 140px;\">\n";
    $markup .= "    " . $this->get_sort_as_button() . "\n";
    if ($given_timecards_data) {
      $markup .= "    " . $given_timecards_data . "\n";
    }
    $markup .= "  </td>\n";

    return $markup;
  }

  // method
  protected function get_timekeeper_obj() {

    // note singularity pattern
    if (! isset($this->timekeeper_obj)) {
      // instantiate class
      include_once("timekeeper.php");
      $this->timekeeper_obj = new Timekeeper();
    }
    return $this->timekeeper_obj;
  }

  // method
  public function remove_parameter($given_uri) {
    return strstr($given_uri, '?', true);
  }

  // method
  public function get_polymorphic_object_as_obj($given_field_to_return = "", $count_num = "", $count_last = "") {
    $markup = "";

    // is this a known table name for this database?
    if (
      // todo somehow these hard-coled values should be refactored
      // todo this is a factory pattern, so read up on that ideas
      // todo instead check if the object is a socation class
      $this->get_class_name_string() == "applications" ||
      $this->get_class_name_string() == "lands" ||
      $this->get_class_name_string() == "projects" ||
      $this->get_class_name_string() == "land_traits" ||
      $this->get_class_name_string() == "books" ||
      $this->get_class_name_string() == "domains" ||
      $this->get_class_name_string() == "processes" ||
      $this->get_class_name_string() == "webpages" ||
      $this->get_class_name_string() == "classes" ||
      $this->get_class_name_string() == "designers" ||
      $this->get_class_name_string() == "moneymakers" ||
      $this->get_class_name_string() == "projects" ||
      $this->get_class_name_string() == "design_instances" ||
      $this->get_class_name_string() == "email_addresses" ||
      $this->get_class_name_string() == "machines" ||
      $this->get_class_name_string() == "soil_areas" ||
      $this->get_class_name_string() == "hyperlinks" ||
      $this->get_class_name_string() == "webpage_maxonomies" ||
      $this->get_class_name_string() == "designs" ||
      $this->get_class_name_string() == "suppliers" ||
      $this->get_class_name_string() == "databases" ||
      $this->get_class_name_string() == "plant_lists" ||
      $this->get_class_name_string() == "budgets" ||
      $this->get_class_name_string() == "shifts" ||
      $this->get_class_name_string() == "postings" ||
      $this->get_class_name_string() == "tools"
    ) {

      // note: not calling a method but working on object from this function
      $obj = $this;
      $given_class_name = $this->get_class_name_string();
      include_once("freeradiantbunny/frb/" . $given_class_name . ".php");
      if ($this->get_class_name_string() == "applications") {
        $obj = new Applications($this->get_given_config());
      } else if ($this->get_class_name_string() == "lands") {
        $obj = new Lands($this->get_given_config());
      } else if ($this->get_class_name_string() == "projects") {
        $obj = new Projects($this->get_given_config());
      } else if ($this->get_class_name_string() == "land_traits") {
        $obj = new LandTraits($this->get_given_config());
      } else if ($this->get_class_name_string() == "books") {
        $obj = new Books($this->get_given_config());
      } else if ($this->get_class_name_string() == "designers") {
        $obj = new Designers($this->get_given_config());
      } else if ($this->get_class_name_string() == "domains") {
        $obj = new Domains($this->get_given_config());
      } else if ($this->get_class_name_string() == "processes") {
        $obj = new Processes($this->get_given_config());
      } else if ($this->get_class_name_string() == "webpages") {
        $obj = new Webpages($this->get_given_config());
      } else if ($this->get_class_name_string() == "classes") {
        $obj = new Classes($this->get_given_config());
      } else if ($this->get_class_name_string() == "moneymakers") {
        $obj = new Moneymakers($this->get_given_config());
      } else if ($this->get_class_name_string() == "soil_areas") {
        $obj = new SoilAreas($this->get_given_config());
      } else if ($this->get_class_name_string() == "projects") {
        $obj = new Projects($this->get_given_config());
      } else if ($this->get_class_name_string() == "hyperlinks") {
        $obj = new Hyperlinks($this->get_given_config());
      } else if ($this->get_class_name_string() == "design_instances") {
        $obj = new DesignInstances($this->get_given_config());
      } else if ($this->get_class_name_string() == "email_addresses") {
        $obj = new EmailAddresses($this->get_given_config());
      } else if ($this->get_class_name_string() == "machines") {
        $obj = new Machines($this->get_given_config());
      } else if ($this->get_class_name_string() == "designs") {
        $obj = new Designs($this->get_given_config());
      } else if ($this->get_class_name_string() == "suppliers") {
        $obj = new Suppliers($this->get_given_config());
      } else if ($this->get_class_name_string() == "plant_lists") {
        $obj = new PlantLists($this->get_given_config());
      } else if ($this->get_class_name_string() == "databases") {
        $obj = new Databases($this->get_given_config());
      } else if ($this->get_class_name_string() == "shifts") {
        $obj = new Shifts($this->get_given_config());
      } else if ($this->get_class_name_string() == "budgets") {
        $obj = new Budgets($this->get_given_config());
      } else if ($this->get_class_name_string() == "postings") {
        $obj = new Postings($this->get_given_config());
      } else if ($this->get_class_name_string() == "tools") {
        $obj = new Tools($this->get_given_config());
      } else {
        return "Error: lib/standard.php class_name_string is not known: " . $this->get_class_name_string() . "<br />\n";
      }

      // set
      $obj_given_id = $this->get_class_primary_key_string();

      // make sure that id exists
      if (! $obj_given_id) {
        print "Error lib/standard.php no id<br />\n";
        return;
      }

      // debug
      // todo look at these results and note that it might be running twice

      // debug
      //print "debug lib/standard/php class_name_string class_primary_key_strong = " . $this->get_class_name_string() . " " . $this->get_class_primary_key_string() . "<br />\n";

      if ($this->get_class_name_string() == "domains") {
        // no id, so set to tli
        $obj->set_given_domain_tli($obj_given_id);
        $obj->set_type("get_by_tli");
      } else {
        // default
        $obj->set_given_id($obj_given_id);
        $obj->set_type("get_by_id");
      }

      // set
      $user_obj = $this->get_user_obj();
      $obj->set_user_obj($user_obj);

      // load data from database
      $markup .= $obj->prepare_query();

      if ($markup) {
        print "Error: lib/standard.php " . $markup . "<br />\n";
      }

      // only output if there is one item to output
      if ($obj->get_list_bliss()->get_count() == 0) {
        $markup .= "<span style=\"error\">Error lib/standard.php polymorphic row not found. (only output if there is one item to output)</span><br />\n";
      
        $markup .= "<span style=\"error\">Error lib/standard.php class_name_string = " . $this->get_class_name_string() . "<br />\n";
        $markup .= "<span>Error lib/standard.php given_id = " . $obj_given_id . "</span><br />\n";
        //$markup .= "<span>count = " . $obj->get_list_bliss()->get_count() . "</span><br />\n";
        $markup .= "<span>Error lib/standard.php class = " . get_class($obj) . "</span><br />\n";
        return $markup;
      } else if ($obj->get_list_bliss()->get_count() == 1) {
        // ok
        return $obj->get_list_bliss()->get_first_element();
      } else {
        // error
        $markup .= "<span style=\"error\">Error lib/standard.php class_name_string = " . $this->get_class_name_string() . "<br />\n";
        $markup .= "<span>Error lib/standard.php given_id = " . $obj_given_id . "</span><br />\n";
        //$markup .= "<span>count = " . $obj->get_list_bliss()->get_count() . "</span><br />\n";
        $markup .= "<span>Error lib/standard.php class = " . get_class($obj) . "</span><br />\n";
        $markup .= "<span style=\"error\">Error lib/standard.php polymorphic more than one row was found. (only output if there is one item to output)</span><br />\n";
        // show what was found
        $num = 0;
        foreach ($obj->get_list_bliss()->get_list() as $obj) {
          $num++;
          $markup .= $num . " <span style=\"background-color: green;\">Error found id = " . $obj->get_id() . "</span><br />\n";
          $markup .= $num . " <span style=\"background-color: green;\">Error found name = " . $obj->get_name() . "</span><br />\n";
        }      

        return $markup;
      }

    } else {

     $markup .= "<p class=\"error\">Error lib/standard.php: table name is not known for this database_name = " . $this->get_database_string() . "</p>\n";

    }
  }

  // method
  public function get_polymorphic_object_as_details($given_field_to_return = "", $count_num = "", $count_last = "") {
    $markup = "";

    // validate parameter
    if ($given_field_to_return == "" || $given_field_to_return == "simple" || $given_field_to_return == "class-name") {
      // ok
      // skip
    } else {
      // debug
      print "debug message lib/standard.php: field_to_return is not known: \"" . $given_field_to_return . "\"<br />\n";
    }

    $obj = $this->get_polymorphic_object_as_obj($given_field_to_return, $count_num, $count_last);

    if (is_string($obj)) {
      $message = "Error: polymorphic_object is string and should be an object. See lib/standard.php id " . $this->get_id() . ". The string is:<br /><span style=\"font-size: 114%; background-color: #AA9999;\"><strong> " . $obj . "</strong></span>";
      $markup .= $this->get_db_dash()->output_error($message);
    } else {
      // output
      if ($given_field_to_return == "class-name") {
        // output class_name
        $markup .= strtolower(get_class($obj));
      } else if ($given_field_to_return == "simple") {
        $markup .= $this->output_simple($obj, $count_num, $count_last);
      } else {
        $markup .= $this->output_complex($obj);
      }
    }

    return $markup;
  }

}
