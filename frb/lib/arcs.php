<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-06
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/arcs.php

//include_once("standard.php");

class Arcs {

  private $arcs_array = array();
  private $display_flag = 1;

  // method
  public function get_arcs_array() {
    return $this->arcs_array;
  }

  // method
  public function add_arc($given_scenes_actor_1, $given_scenes_actor_2) {
    $markup = "";

    // arc factory code
    include_once("arc.php");
    $arc_obj = new Arc();
    array_push($this->arcs_array, $arc_obj);

    // transfer values
    $arc_obj->set_scenes_actor_1($given_scenes_actor_1);
    $arc_obj->set_scenes_actor_2($given_scenes_actor_2);

    // evaluate    
    $markup .= $arc_obj->evaluate();

    return $markup;
  }

  // method
  private function get_total_lands_class_count() {
    $total_lands_class_count = 0;

    foreach ($this->list_of_arcs as $arc) {
      if ($arc->display_flag) {
        $location_1 = $arc->get_actor_1();
        if (get_class($location_1) == "Lands") {
          $total_lands_class_count++;
        }
      }
    }

    // debug
    //print "debug arcs total_lands_class_count = " .  $total_lands_class_count . "<br />\n";

    return $total_lands_class_count;
  }

}
