<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/html_table.php

class HtmlTable {

  // attributes
  private $rows = array();

  // method
  public function make_row($given_row_matrix) {

    // debug
    //print "debug html_table given_row_matrix count = " . count($given_row_matrix) . "<br />\n";

    include_once("html_table_row.php");
    $row = new HtmlTableRow();
    array_push($this->rows, $row);
    // now load the row with cell objects
    foreach ($given_row_matrix as $col_num => $cell_array) {
      $needle = "<td";
      $haystack = $cell_array[1];
      if (strpos($haystack, $needle) !== false) {
        // this comes as a cell, so do  not make one
        $row->makeHmtlTableCellFromGivenMarkup($cell_array[1]);
        // debug
        //print "debug html_table cell_data" . $cell_array[1] . "<br/ >\n";
      } else {
        $row->makeHmtlTableCell($cell_array);
      }
    }
  }

  // method
  public function craft_table($given_table_attribute_class, $given_requested_visibility = "") {
    $markup = "";

    // debug
    //print "debug html_table row count = " . count($this->rows) . "<br />\n";

    $markup .= "<table class=\"" . $given_table_attribute_class . "\">\n";
    foreach ($this->rows as $row) {
      $markup .= $row->craft_row($given_requested_visibility);
    }
    $markup .= "</table>\n";

    return $markup;
  }

}
