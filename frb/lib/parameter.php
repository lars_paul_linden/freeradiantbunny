<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this file
// http://freeradiantbunny.org/main/en/docs/frb/lib/parameter.php

class Parameter {

  // variables
  private $name;
  private $validation_type;
  private $error_message;
  private $value;

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // validation_type
  public function set_validation_type($var) {
    $this->validation_type = $var;
  }
  public function get_validation_type() {
    return $this->validation_type;
  }

  // validation_type that has a specific id
  public function set_validation_type_as_id() {
    $this->set_validation_type("id");
  }
  public function is_validation_type_id() {
    if ($this->get_validation_type() == "id") {
      return 1;
    }
    return 0;
  }

  // validation_type that has a specific domain_tli
  public function set_validation_type_as_domain_tli() {
    $this->set_validation_type("domain_tli");
  }
  public function is_validation_type_domain_tli() {
    if ($this->get_validation_type() == "domain_tli") {
      return 1;
    }
    return 0;
  }

  // validation_type that has a specific filter
  public function set_validation_type_as_filter() {
    $this->set_validation_type("filter");
  }
  public function is_validation_type_filter() {
    if ($this->get_validation_type() == "filter") {
      return 1;
    }
    return 0;
  }

  // validation_type that has a specific command
  public function set_validation_type_as_command() {
    $this->set_validation_type("command");
  }
  public function is_validation_type_command() {
    if ($this->get_validation_type() == "command") {
      return 1;
    }
    return 0;
  }

  // validation_type that has a specific tli
  public function set_validation_type_as_tli() {
    $this->set_validation_type("tli");
  }
  public function is_validation_type_tli() {
    if ($this->get_validation_type() == "tli") {
      return 1;
    }
    return 0;
  }

  // validation_type has a specific sort
  public function set_validation_type_as_sort() {
    $this->set_validation_type("sort");
  }
  public function is_validation_type_sort() {
    if ($this->get_validation_type() == "sort") {
      return 1;
    }
    return 0;
  }

  // validation_type has a specific subset
  public function set_validation_type_as_subset() {
    $this->set_validation_type("subset");
  }
  public function is_validation_type_subset() {
    if ($this->get_validation_type() == "subset") {
      return 1;
    }
    return 0;
  }

  // validation_type that has a specific view
  public function set_validation_type_as_view() {
    $this->set_validation_type("view");
  }
  public function is_validation_type_view() {
    if ($this->get_validation_type() == "view") {
      return 1;
    }
    return 0;
  }

  // error_message
  public function set_error_message($var) {
    $this->error_message = $var;
  }
  public function get_error_message() {
    return $this->error_message;
  }

  // value
  public function set_value($var) {
    $this->value = $var;
  }
  public function get_value() {
    return $this->value;
  }

}
