<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04
// version 1.6 2016-03-25

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/scrubber.php

include_once("satabase.php");

class User {
  public $name = "webmaster";
}

abstract class Scrubber extends Satabase {

  // given
  private $given_id;
  private $given_subview_class;
  private $given_query_string;
  private $given_config;
  // todo move the default setting to a user config file
  private $given_view = "all"; // default
  private $given_sort = "sort"; // default
  private $given_subset = "zoneline"; // default

  // given_id
  public function set_given_id($var) {
    $error_message = $this->get_validator_obj()->validate_id($var, "id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_id = $var;
    }
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_subview_class
  public function set_given_subview_class($var) {
    // validate
    if ($var == "pickups" || $var == "pickup" || $var == "date") {
      $this->given_subview_class = $var;
    }
  }
  public function get_given_subview_class() {
    return $this->given_subview_class;
  }

  // given_query_string
  public function set_given_query_string($var) {
    $this->given_query_string = $var;
  }
  public function get_given_query_string() {
    return $this->given_query_string;
  }

  // given_config
  public function set_given_config($var) {
    $this->given_config = $var;
  }
  public function get_given_config() {
    return $this->given_config;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // given_sort
  public function set_given_sort($var) {
    $this->given_sort = $var;
  }
  public function get_given_sort() {
    return $this->given_sort;
  }

  // given_subset
  public function set_given_subset($var) {
    $this->given_subset = $var;
  }
  public function get_given_subset() {
    return $this->given_subset;
  }

  // attributes
  private $user_obj;       // holds drupal's user object
  private $list_bliss;     // holds aggregation data and functions
  private $validator_obj;  // holds validation functions
  private $type;           // holds the type (which database query)
  private $error_message = "";
  private $form_create_obj;
  private $factory;     // holds methods used by several classes
  private $row_count;
  private $oldest_sort_date;

  // user_obj
  public function set_user_obj($var) {
    $this->user_obj = $var;
  }
  protected function get_user_obj() {
    // todo fix the following which is a temp fix
    if (! $this->user_obj) {
      $this->user_obj = new User();
    }
    return $this->user_obj;
  }

  // list_bliss
  public function get_list_bliss() {
    if (! isset($this->list_bliss)) {
      include_once("list_bliss.php");
      $this->list_bliss = new ListBliss();
    }
    return $this->list_bliss;
  }

  // validator_obj
  protected function get_validator_obj() {
    if (! isset($this->validator_obj)) {
      include_once("validator.php");
      $this->validator_obj = new Validator();
    }
    return $this->validator_obj;
  }

  // type
  protected function set_type($var) {
    $this->type = $var;
  }
  public function get_type() {
    return $this->type;
  }

  // error_message
  public function set_error_message($var) {
    $this->error_message = $var;
  }
  protected function get_error_message() {
    return $this->error_message;
  }

  // form_create_obj
  public function get_form_create_obj() {
    if (! $this->form_create_obj) {
      include_once("form_create.php");
      $this->form_create_obj = new FormCreate($this->get_given_config());
    }
    return $this->form_create_obj;
  }

  // factory
  protected function get_factory() {
    if (! isset($this->factory)) {
      include_once("factory.php");
      $this->factory = new Factory();
    }
    return $this->factory;
  }

  // row_count
  public function set_row_count($var) {
    $this->row_count = $var;
  }
  public function get_row_count() {
    return $this->row_count;
  }

  // oldest_sort_date
  public function set_oldest_sort_date($var) {
    $this->oldest_sort_date = $var;
  }
  protected function get_oldest_sort_date() {
    return $this->oldest_sort_date;
  }

  // todo it might be useful to move all of the sanitize functions...
  // todo ... into a class of their own

  // method
  protected function encode_url_in_url($link) {
    $link  = preg_replace('/#/', "%23", $link);
    $link  = preg_replace('/@/', "%40", $link);
    //print $link;
    $link  = preg_replace('/%20/', "+", $link);
    $chars = str_split($link);
    // skip the first match of question mark, and replace all others
    $flag_first_qm = 1;
    // skip the first and second matches of ampersand, and replace all others
    $flag_first_amp = 2;
    $link = "";
    foreach ($chars as $char) {
      if ($char == "?") {
        if ($flag_first_qm) { 
          $flag_first_qm = 0;
          $link .= $char;
        } else {
          $link .= "%3F";
        }
      } else if ($char == "&") {
        if ($flag_first_amp) { 
          $flag_first_amp--;
          $link .= $char;
        } else {
          $link .= "%26";
        }
      } else {
        $link .= $char;
      }
    }
    return $link;
  }

  // method
  protected function encode_second_url_in_url($link) {
    $chars = str_split($link);
    // skip the first match of colon, and replace all others
    $flag_first_c = 1;
    // skip until after ?, and replace all others
    $flag_first_fs = 1;
    $link = "";
    foreach ($chars as $char) {
      if ($char == ":") {
        if ($flag_first_c) { 
          $flag_first_c = 0;
          $link .= $char;
        } else {
          $link .= "%3A";
        }
      } else if ($char == "/") {
        if ($flag_first_fs) { 
          $link .= $char;
        } else {
          $link .= "%2F";
        }
      } else if ($char == "?") {
        $flag_first_fs = 0;
        $link .= $char;
      } else {
        $link .= $char;
      }
    }
    return $link;
  }

  // method
  protected function sanitize_insert_sql($sql_string_from_user) {
    // todo make this even more stringent
    // allow only insert sql
    if (preg_match('/grant/', $sql_string_from_user)) {
      // no grants
      return '';
    } else if (preg_match('/update/', $sql_string_from_user)) {
      // no updates
      return '';
    } else if (preg_match('/select/', $sql_string_from_user)) {
      // no select
      return '';
    } else {
      return $sql_string_from_user;
    }
  }

  // method
  // a child class does not need to implement this class
  // this is the entrance for caller from outside of module
  // so, in a humble way, this is the eat local fresh food API
  public function get_markup() {
    $markup = "";

    if ($this->get_given_config()->get_debug()) {
      // debug
      //print "debug scrubber get_markup()<br />\n";
    }

    // check if user is attempting to form_create an element
    if ($this->get_form_create_obj()->get_command()  == "create" ||
        $this->get_form_create_obj()->get_command()  == "edit") {
      // debug
      //print "debug scrubber command = " . $this->get_form_create_obj()->get_command() . "<br />\n";
      $user_obj = $this->get_user_obj();
      $markup .= $this->get_form_create_obj()->output_form($this, $user_obj);
      return $markup;
    }

    // parameters
    $error_message = $this->deal_with_parameters();
    if ($error_message) {
      return $error_message;
    }

    // post
    $error_message = $this->deal_with_posts();
    if ($error_message) {
      return $error_message;
    }

    // debug
    //print "class scrubber determine_type()...<br />\n";

    // based upon the parameter values, get data from database
    // and even if there were not any parameters at all
    // check to see if there is any data from database
    $this->determine_type();

    // debug
    //print "debug scrubber type = " . $this->get_type() . "<br />\n";

    // only authenticated users only may load data of this class
    if (! $this->is_authenticated_for_database_access()) {

      // debug
      print "debug scrubber: not authenticated.<br />\n";

      // one more chance
      // if home
      $class_name = get_class($this);
      if ($class_name == "Hyperlinks") {
        if ($this->get_type() == "get_by_id" ||
            $this->get_type() == "get_all" ||
            $this->get_type() == "get_all_random") {
          // ok, let pass
          // debug
          //print "class scrubber let pass type = " . $this->get_type() . "<br />\n";
        } else {
          // not authenticated or home, so fail
          return;
        }
      } else {
        // not authenticated or home, so fail
        return;
      }
    }

    // ok
    $markup .= $this->prepare_query();

    // menus
    $markup .= $this->output_user_info();
    $markup .= $this->output_subsubmenu();

    // main content

    $markup .= $this->output_preface();
    $markup .= $this->output_title();
    $markup .= $this->output_given_variables();
    $markup .= $this->output_search();
    $markup .= $this->output_data_set();
    $markup .= $this->output_notes();

    // close database
    $this->get_db_dash()->close_connection();

    return $markup;
  }

  // method
  // for the developer to deal with incoming URI parameters
  protected function deal_with_parameters() {
    // do nothing
    // but this allows the child class to override
  }

  // method
  // for the developer to deal with incoming URI parameters
  protected function deal_with_posts() {
    // do nothing
    // but this allows the child class to override
  }

  // method
  // the child class uses this to process each parameter in a uniform way
  protected function process_parameters($given_parameters) {

    // loop through given_queries
    if (! $this->get_given_query_string()) {
      // no query_strings to process
      return;
    }

    $given_queries = explode("&", $this->get_given_query_string());
    $given_queries_count = count($given_queries);

    // debug
    //print "debug scrubber given_queries_count = " . $given_queries_count . "<br />\n";

    foreach ($given_queries as $given_query) {

      // debug
      //print "debug scrubber given_query = " . $given_query . "<br />\n";

      $given_query_name = substr($given_query, 0, strpos($given_query, "="));

      // debug
      //print "debug scrubber given_query_name = " . $given_query_name . "<br />\n";

      $given_query_value = substr($given_query, strpos($given_query, "=") + 1);

      // debug
      //print "debug scrubber given_query_value = " . $given_query_value . "<br />\n";

      // loop through given_parameters
      foreach ($given_parameters as $given_parameter) {
        if ($given_query_name == $given_parameter->get_name()) {

          // debug
          //print "debug scrubber: found parameter_name = " . $given_parameter->get_name() . "<br />\n";

          // users_value is set, so validate
          $error_message = ""; // initialize
          //$parameter_a->set_validation_type_as_sort();

          if ($this->get_validator_obj()->validate_id($given_query_name, $given_query_name)) {

            // users_value is valid, so store
            $given_parameter->set_value($given_query_value);

            // debug
            //print "debug scrubber: found valid parameter_value = " . $given_query_value . "<br />\n";

          } else {
            // debug
            print "debug scrubber: not valid parameter_value = " . $given_query_value . "<br />\n";
          }
        } else {
          // debug
          //print "debug scrubber: given_parameter not found parameter_name = " . $given_parameter->get_name() . "<br />\n";

        }
      }
    }
  }

  // method
  protected function load_data($instance, $sql, $database_name = "") {
    $markup = "";

    $markup .= $this->get_db_dash()->load($instance, $sql, $database_name);
    if ($this->get_db_dash()->get_results()) {
      $markup .= $instance->transfer($this->get_db_dash()->get_results());
    }
    $this->get_db_dash()->free_memory();

    return $markup;
  }

  // method
  // only for pages that are accessing the database
  abstract protected function determine_type();

  // method
  // force the developer to consider the database
  abstract protected function prepare_query();

  // method
  // only for pages that are accessing the database
  // this may not be absolutely necessary
  // however it goes with the above
  // force the developer to consider the database
  abstract protected function transfer($results);

  // method
  protected function output_subsubmenu() {
    $markup = "";

    if ($this->get_type() == "get_by_id") {
      include_once("factory.php");
      $factory = new Factory($this->get_given_config());
      // first link
      $url = $this->url($factory->get_class_name_given_object($this));
      $markup .= "<div class=\"no-print\">\n";
      $markup .= "  Widen View: <a href=\"" . $url . "\">All&nbsp;" . get_class($this) . "</a><br />\n";
      $markup .= "</div>\n";
      // another link
      $user_obj = $this->get_user_obj();
      $class_name = get_class($this);
      $temp_obj = new $class_name($this->get_given_config());
      $temp_obj->set_given_id($this->get_given_id());
      $user_obj = $this->get_user_obj();
      $temp_obj->set_user_obj($user_obj);
      $project_id = $temp_obj->get_project_id($user_obj);
      $url = $this->url($factory->get_class_name_given_object($this) . "/project/" . $project_id);
      // todo the following line was printing out for projects page
      //$markup .= "  Widen View: <a href=\"" . $url . "\">All&nbsp;" . get_class($this) . " with the same project as this one</a><br />\n";
    } else if ($this->get_given_project_id()) {
      $url = $this->url("projects/" . $this->get_given_project_id());
      $markup .= "<a href=\"" . $url . "\">Back to project page</a><br />\n";
      $url = $this->url("projects");
      $markup .= "  Widen View: <a href=\"" . $url . "\">All&nbsp;projects</a><br />\n";
      include_once("factory.php");
      $factory = new Factory($this->get_given_config());
      $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
      $markup .= "  Widen View: <a href=\"" . $url . "\">All&nbsp;" . get_class($this) . "</a><br />\n";

      // todo need to move these to the classes
      if (get_class($this) == "GoalStatements") {
        $url = $this->url("business_plan_texts/project/" . $this->get_given_project_id());
        $markup .= "  Also View: <a href=\"" . $url . "\">All BusinessProecessTexts of this project</a><br />\n";
      } else if (get_class($this) == "BusinessPlanTexts") {
        $url = $this->url("goal_statements/project/" . $this->get_given_project_id());
        $markup .= "  Also View: <a href=\"" . $url . "\">All GoalStatements of this project</a><br />\n";
        $url = $this->url("processes/project/" . $this->get_given_project_id());
        $markup .= "  Also View: <a href=\"" . $url . "\">All Processes of this project</a><br />\n";
      } else if (get_class($this) == "Timecards") {
        // display add button
        if (isset($this->get_user_obj()->name)) {
          $markup .= $this->get_form_create_obj()->output_add_link($this);
        }
      } else if (get_class($this) == "Processes") {
        $url = $this->url("business_plan_texts/project/" . $this->get_given_project_id());
        $markup .= "  Also View: <a href=\"" . $url . "\">All BusinessProecessTexts of this project</a><br />\n";
        $url = $this->url("scene_elements/project/" . $this->get_given_project_id());
        $markup .= "  Also View: <a href=\"" . $url . "\">All SceneElements of this project</a<<br />\n";
      }
    } else {
      if (get_class($this) == "Webpages") {
        // todo display if subsection but should be moved to given
        if ($this->get_type() == "get_by_subsection_id") {
          $url = $this->url("webpages/domain/" . $this->get_given_domain_tli());
          $markup .= "<p><em>Widen View: <a href=\"" . $url . "\">all webpages of domain " . $this->get_given_domain_tli() . "</a></em></p>\n";
        }
      }
    }

    // enclose in markup
    if ($markup) {
      $markup = "<div class=\"subsubmenu\">\n" . $markup . "</div>\n";
    }

    return $markup;
  }

  // method
  // developer can override this
  protected function output_user_info() {
    return "";
  }

  // method
  protected function output_user_info_message() {
    $markup = "";

    // only authenticated users
    if ($this->get_user_obj()) {
      if ($this->get_user_obj()->uid) {
          $markup .= "<div class=\"subsubmenu-user\">\n";
          $markup .= "<strong>These " . get_class($this) . " were form_created by</strong> " . $this->get_user_obj()->name . "<br />\n";
          $markup .= "</div>\n";
      }
    }

    return $markup;
  }

  // method
  // developer can override this
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_id()) {
      // todo this was replaced by the title having the id in it
      //$class_type = "This is id " ;
      //$given_variable = $this->get_given_id();
      //$name_with_link = "";
      //$markup_being_sent = "";
      //$markup .= $this->output_given_variables_boilerplate($class_type, $given_variable, $name_with_link, $markup_being_sent);

    } else if ($this->get_given_project_id()) {
      // deal with plural
      if ($this->get_list_bliss()->get_count() == 1) {
        // todo remove the "s" at the end of the class name
        $class_type = "This is a " . get_class($this) . " of ";
      } else {
        $class_type = "These are " . get_class($this) . " of ";
      }
      $given_variable = "project";
      // note the code below gets a file in another directory
      include_once(dirname(__DIR__) . "/projects.php");
      $project_obj = new Projects($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $project_id = $this->get_given_project_id();
      $name_with_link = $project_obj->get_name_with_link_given_project_id($project_id, $user_obj);
      $markup_being_sent = "";
      $markup .= $this->output_given_variables_boilerplate($class_type, $given_variable, $name_with_link, $markup_being_sent);

    } else {
      // note: check subclass to see if there are more variables
      if (method_exists($this, "output_given_variables_extra")) {
        // ok
        $markup .= $this->output_given_variables_extra();
      } else {
        //$markup .= "debug scrubber output_given_variables() [get other givens]<br />\n";
      }
    }

    // enclose in markup
    if ($markup) {
      $markup = "<div class=\"given-variables\">\n" . $markup . "</div>\n";
    }

    return $markup;
  }

  // method
  // developer can override this
  protected function output_given_variables_given_id() {
    $markup = "";

    if ($this->get_given_id()) {
      $markup .= "<div class=\"given-variables\">\n";
      // make sure that the name exists
      if (function_exists("get_name") && $this->get_name() && $this->get_id()) {
        $markup .= "  <em>This is <strong>" . $this->get_name_with_link() . "</strong></em>\n";
      } else {
        $markup .= "  <em>This is <strong>" . get_class($this) . "</strong> id = " . $this->get_given_id() . "</em>\n";
      }
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  // todo this might now be kruft
  protected function output_given_variables_boilerplate($class_type, $given_variable, $name_with_link, $markup_being_sent = "") {
    $markup = "";

    $markup .= "  " . $class_type . $given_variable;
    if ($name_with_link) {
      $markup .= " <strong>" . $name_with_link . "</strong>.";
    }
    $markup .= $markup_being_sent;
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  protected function output_data_set() {
    $markup = "";

    if ($this->get_list_bliss()->get_count() > 0) {
      if ($this->get_error_message()) {
        $markup .= $this->get_error_message();
      } else {

        // debug
        //print "debug scrubber either single or aggregate<br />\n";

        if ($this->get_given_id()) {
          $markup .= $this->output_single();
        } else {
          $markup .= $this->output_aggregate();
        }
      }
    } else {
      $markup .= $this->output_list_is_empty_message();
    }

    return $markup;
  }

  // method
  // this is a uniform error message
  protected function output_list_is_empty_message() {
    $markup = "";

    // get class name
    $implementing_class = get_class($this);

    // return message
    $markup .= "<p class=\"error\">No <strong>" . $implementing_class . "</strong> were found.</p>\n";
    $url = $this->url("home");
    $markup .= "<p class=\"subsub\">Directions: <a href=\"" . $url . "\">Go to Home</a>.</p>\n";

    // todo write code that -- if the situation is right -- the system offers...
    // todo ...to create an instance of the class they were interested in listing
    return $markup;
  }

  // method
  // this is an optional function
  // for placing notes at the bottom of the page
  protected function output_notes() {
    // skip
  }

  // method
  // this is an optional function
  // for placing search form and search results
  protected function output_search() {
    // skip
  }

  // method
  public function is_owner($given_user_obj, $given_project_id) {

    // debug
    //print "debug scrubber given_project_id = " . $given_project_id . "<br />\n";

    // initialize what is to be returned
    $access_flag = 0;

    // check
    // note the code below gets a file in another directory
    include_once(dirname(__DIR__) . "/projects.php");
    $project_obj = new Projects($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $project_id_array = $project_obj->get_array_of_project_id($user_obj);
    foreach ($project_id_array as $project_id) {
      // debug
      //print "debug scrubber project_id = " . $project_id . "<br />\n";
      if ($project_id == $given_project_id) {
        // grant access
        $access_flag = 1;
        // debug
        //print "debug project_id = " . $project_id . " MATCHED<br />\n";
        break;
      }
    }

    return $access_flag;
  }

  // method
  public function has_guest_pass($given_user_obj, $given_project_id, $given_class_name) {

    include_once("guest_passes.php");
    $guest_pass_obj = new GuestPasses();

    return $guest_pass_obj->get_access_flag($given_user_obj, $given_project_id, $given_class_name);
  }

  // method
  public function is_authenticated_for_database_access() {

    // todo need to trace the user_obj through the system (draw a diagram)

    // some classes are public classes
    // todo document these public classes with public data inside
    $public_classes = array("Plants",
    		            "PlantCategories",
    		            "PlantAttributes",
			            "Families",
			            "Varieties");

    // all users have access to these classes
    $class_name = get_class($this);

    // debug
    //print "debug scrubber class_name = " . $class_name . "<br />\n";

    // check if this class is a public class
    if (in_array($class_name, $public_classes)) {
        return "true";
    }

    // debug
    //print "debug scrubber class_name = " . $class_name . " (not a public class)<br />\n";

    // the remaining classes are private classes
    // users must qualify to gain acess to the database
    if ($this->get_user_obj()) {

      // get variables needed for the assessment
      $user_obj = $this->get_user_obj();

      // debug
      //print "debug scrubber class_name = " . $class_name . "<br />\n";
      //print "debug scrubber type = " . $this->get_type() . "<br />\n";

      // todo security todo reject if not https://
      if ($class_name == "Applications" ||
          $class_name == "AgriculturalTypes" ||
          $class_name == "Accounts" ||
          $class_name == "Builds" ||
          $class_name == "BlogPosts" ||
          $class_name == "Books" ||
          $class_name == "Budgets" ||
          $class_name == "BusinessPlanTexts" ||
          $class_name == "Categories" ||
          $class_name == "Checklists" ||
          $class_name == "Classes" ||
          $class_name == "CropPlanMilestones" ||
          $class_name == "Databases" ||
          $class_name == "Designers" ||
          $class_name == "Designs" ||
          $class_name == "DesignInstances" ||
          $class_name == "Documentations" ||
          $class_name == "Domains" ||
          $class_name == "Dashboards" ||
          $class_name == "DomainMeasurements" ||
          $class_name == "PermacultureTopics" ||
          $class_name == "EmailAddresses" ||
          $class_name == "Hosts" ||
          $class_name == "HostApplications" ||
          $class_name == "HostDatabases" ||
          $class_name == "HostEmailAddresses" ||
          $class_name == "Hyperlinks" ||
          $class_name == "Keywords" ||
          $class_name == "GoalStatements" ||
          $class_name == "Searches" ||
          $class_name == "Indiegoals" ||
          $class_name == "Payments" ||
          $class_name == "PickupDetails" ||
          $class_name == "Invoices" ||
          $class_name == "Customers" ||
          $class_name == "Albums" ||
          $class_name == "Songs" ||
          $class_name == "Events" ||
          $class_name == "Lands" ||
          $class_name == "LandTraits" ||
          $class_name == "Machines" ||
          $class_name == "ManifestMaxonomies" ||
          $class_name == "Maxonomies" ||
          $class_name == "Moneymakers" ||
          $class_name == "PlantLists" ||
          $class_name == "PlantHistories" ||
          $class_name == "PlantFamilies" ||
          $class_name == "PlantAttributes" ||
          $class_name == "PlantHistoryEvents" ||
          $class_name == "PlantListPlants" ||
          $class_name == "Postings" ||
          $class_name == "Processes" ||
          $class_name == "ProcessFlows" ||
          $class_name == "Products" ||
          $class_name == "Blogposts" ||
          $class_name == "Projects" ||
          $class_name == "ProjectProjects" ||
          $class_name == "Questions" ||
          $class_name == "Reasons" ||
          $class_name == "Responsibilities" ||
          $class_name == "SeedPackets" ||
          $class_name == "SceneElements" ||
          $class_name == "Shifts" ||
          $class_name == "SoilAreas" ||
          $class_name == "SiteDesigns" ||
          $class_name == "Suggestions" ||
          $class_name == "Suppliers" ||
          $class_name == "Tags" ||
          $class_name == "Tenperdays" ||
          $class_name == "Timecards" ||
          $class_name == "Tickets" ||
          $class_name == "Tools" ||
          $class_name == "Usernames" ||
          $class_name == "Units" ||
          $class_name == "Visits" ||
          $class_name == "Webmasters" ||
          $class_name == "Webpages" ||
          $class_name == "WebpageMaxonomies" ||
          $class_name == "WebpageMoneymakers" ||
          $class_name == "WebpageTags" ||
          $class_name == "Zachmans"
        ) {
        if ($this->get_type() == "get_by_id" ||
            $this->get_type() == "get_all" ||
            $this->get_type() == "get_by_project_id") {
          // user_name is built within select statement
          return "false";
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Hyperlinks") {
          if ($this->get_type() == "get_mudiabot_data") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Usernames") {
          if ($this->get_type() == "get_by_username") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Visits") {
          // todo hey, this appears to be twice in this file: what is that?
          if ($this->get_type() == "get_by_plant_history_event_id" ||
              $this->get_type() == "get_by_plant_list_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Searches") {
          // todo hey, this appears to be twice in this file: what is that?
          if ($this->get_type() == "get_by_name") {
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Maxonomies") {
          if ($this->get_type() == "get_by_webpage_id") {
            // user_name is built within select statement
            return "false";
          }
          if ($this->get_type() == "get_by_group_name") {
            // user_name is built within select statement
            return "false";
          }
          if ($this->get_type() == "get_by_domain_tli") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "PlantLists") {
          // todo hey, this appears to be twice in this file: what is that?
          if ($this->get_type() == "get_by_process_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "PlantListPlants") {
          if ($this->get_type() == "get_by_plant_list_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Shifts") {
          if ($this->get_type() == "get_by_process_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "PlantHistories") {
          if ($this->get_type() == "get_by_seed_packet_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "GoalStatements") {
          if ($this->get_type() == "get_by_business_plan_text_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // note: security pinhole special
        if ($class_name == "PlantHistoryEvents") {
          if ($this->get_type() == "get_by_plant_list_plant_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Postings") {
          if ($this->get_type() == "get_by_account_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "ProjectProjects") {
          if ($this->get_type() == "get_by_parent_project_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Tickets") {
          if ($this->get_type() == "get_by_process_id") {
            // user_name is built within select statement
            return "false";
          } else if ($this->get_type() == "get_by_project_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Timecards") {
          if ($this->get_type() == "get_by_responsibility_id" ||
              $this->get_type() == "get_by_process_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Moneymakers") {
          if ($this->get_type() == "get_by_domain_tli") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Webpages") {
          if ($this->get_type() == "get_by_domain_tli" ||
              $this->get_type() == "get_by_subsection_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Designs") {
          if ($this->get_type() == "get_by_domain_tli") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "DesignInstances") {
          if ($this->get_type() == "get_by_design_id") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Applications") {
          if ($this->get_type() == "get_by_manifest_tli" ||
              $this->get_type() == "get_by_domain_tli") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "EmailAddresses") {
          if ($this->get_type() == "get_by_domain_tli") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "Databases") {
          if ($this->get_type() == "get_by_domain_tli") {
            // user_name is built within select statement
            return "false";
          }
        }
        // special case
        // note: security pinhole special
        if ($class_name == "SceneElements") {
          if ($this->get_type() == "get_by_process_id") {
            // user_name is built within select statement
            return "false";
          }
        }
      } else if ($class_name == "PlantHistories") {
        // debug
        //print "debug scrubber class_name = " . $class_name . "<br />\n";
        if ($this->get_type() == "get_by_id" || $this->get_type() == "get_by_plant_list_id" || $this->get_type() == "get_by_plant_list_plant_id" || $this->get_type() == "get_all") {

          // debug
          //print "debug scrubber type = " . $this->get_type() . "<br />\n";
          // this assumes that the class will get rows owned by user
          return "false";
        }

      } else if ($class_name == "SeedPackets" ||
                 $class_name == "Tickets" ||
                 $class_name == "Accounts" ||
                 $class_name == "Songs" ||
                 $class_name == "Styles" ||
                 $class_name == "RallyTallies" ||
                 $class_name == "IndieGoals" ||
                 $class_name == "SoilTests" ||
                 $class_name == "Shares" ||
                 $class_name == "DesignOrders" ||
                 $class_name == "CropYieldPlans" ||
                 $class_name == "GreenhousePlans" ||
                 $class_name == "GreenhouseDetails" ||
                 $class_name == "CropPlans" ||
                 $class_name == "Spacings" ||
                 $class_name == "PlantHistoryEvents" ||
                 $class_name == "PlantListPlants" ||
                 $class_name == "ProcessProfiles" ||
                 $class_name == "Visits" ||
                 $class_name == "Harvests" ||
                 $class_name == "Values" ||
                 $class_name == "Pickups" ||
                 $class_name == "Linkmaster" ||
                 $class_name == "Observations") {

        // debug
        //print "debug scrubber is_auth. class_name = " . $class_name . "<br />\n";
        //print "debug scrubber type = " . $this->get_type() . "<br />\n";

        if ($this->get_type() == "get_by_id") {
          return "false";
        } else if ($this->get_type() == "get_by_project_id") {
          $project_id = $this->get_given_project_id($user_obj);
          if ($this->is_owner($user_obj, $project_id)) {
            return "true";
          }
          // not yet done, check for guest pass
          if ($this->has_guest_pass($user_obj, $project_id, $class_name)) {
            // pinhole
            // ok (may access with guest_pass from data's owner)
            return "true";
          }
        } else if ($this->get_type() == "get_by_plant_list_id") {
          if ($class_name == "GreenhousePlans" ||
              $class_name == "CropYieldPlans" ||
              $class_name == "PlantHistoryEvents" ||
              $class_name == "CropPlans") {
            // this assumes that the class will get rows owned by user
            return "true";
          }
        } else if ($this->get_type() == "get_by_plant_history_event_id" ||
                  $this->get_type() == "get_by_plant_list_id") {
          if ($class_name == "Visits") {
            // this assumes that the class will get rows owned by user
            return "true";
          }
        } else if ($this->get_type() == "get_by_plant_history_id") {
          if ($class_name == "PlantHistoryEvents") {
            // this assumes that the class will get rows owned by user
            return "true";
          }
        } else if ($this->get_type() == "get_by_seed_packet_id") {
          if ($class_name == "PlantHistories") {
            // this assumes that the class will get rows owned by user
            return "true";
          }
        } else if ($this->get_type() == "get_all") {
          // this assumes that the class will get rows owned by user
          return "true";
        }
      }
    } else {
      //print "debug scrubber no user_obj or user_obj but without uid<br />\n";
    }

    return ""; // false
  }

  // method
  private function figure_out_project_id($given_user_obj) {
    $project_id = "";

    // older
    //if (get_class($this) == "Tickets") {
    //  if ($this->get_given_id()) {
    //    $database_id = $this->get_given_id();
    //    include_once("tickets.php");
    //    $ticket_obj = new Tickets();
    //    $project_id = $ticket_obj->get_project_id_given_documentation_id($databases_id, $given_user_obj);
    //  }
    if (get_class($this) == "Songs") {
      // skip
    } else if (get_class($this) == "Styles") {
      if ($this->get_given_id()) {
        $database_id = $this->get_given_id();
        include_once("styles.php");
        $styles_obj = new Styles();
        $project_id = $styles_obj->get_project_id_given_documentation_id($databases_id, $given_user_obj);
      }
    } else if (get_class($this) == "RallyTallies") {
      if ($this->get_given_id()) {
        $database_id = $this->get_given_id();
        include_once("rallytallies.php");
        $obj = new RallyTallies();
        $project_id = $obj->get_project_id_given_documentation_id($databases_id, $given_user_obj);
      }
    } else if (get_class($this) == "IndieGoals") {
      if ($this->get_given_id()) {
        $database_id = $this->get_given_id();
        include_once("indiegoals.php");
        $obj = new IndieGoals();
        $project_id = $obj->get_project_id_given_documentation_id($databases_id, $given_user_obj);
      }
    } else if (get_class($this) == "TableDefinitions") {
      if ($this->get_given_id()) {
        $database_id = $this->get_given_id();
        include_once("table_definitions.php");
        $obj = new TableDefinitions();
        $project_id = $obj->get_project_id_given_documentation_id($databases_id, $given_user_obj);
      }
    } else if (get_class($this) == "DesignInstancess") {
      if ($this->get_given_id()) {
        $design_instance_id = $this->get_given_id();
        include_once("design_instances.php");
        $design_instance_obj = new DesignInstances();
        $project_id = $design_instance_obj->get_project_id_given_design_instance_id($design_instance_id, $given_user_obj);
      }
    } else if (get_class($this) == "Harvests") {
      if ($this->get_given_id()) {
        $harvest_id = $this->get_given_id();
        include_once("harvests.php");
        $harvest_obj = new Harvests();
        $project_id = $harvest_obj->get_project_id_given_harvest_id($harvest_id, $given_user_obj);
      }
    } else if (get_class($this) == "Pickups") {
      if ($this->get_given_id()) {
        $pickup_id = $this->get_given_id();
        include_once("pickups.php");
        $pickup_obj = new Pickups();
        $project_id = $pickup_obj->get_project_id_given_pickup_id($pickup_id, $given_user_obj);
      }
    } else if (get_class($this) == "Materials") {
      if ($this->get_given_id()) {
        $material_id = $this->get_given_id();
        include_once("materials.php");
        $material_obj = new Materials();
        $project_id = $material_obj->get_project_id_given_material_id($material_id, $given_user_obj);
      }
    } else if (get_class($this) == "SeedPackets") {
      if ($this->get_given_id()) {
        $seed_packet_id = $this->get_given_id();
        include_once("seed_packets.php");
        $seed_packet_obj = new SeedPackets();
        $project_id = $seed_packet_obj->get_project_id_given_seed_packet_id($seed_packet_id, $given_user_obj);
      }
    } else if (get_class($this) == "Visits") {
      if ($this->get_given_id()) {
        $visit_id = $this->get_given_id();
        include_once("visits.php");
        $visit_obj = new Visits();
        $project_id = $visit_obj->get_project_id_given_visit_id($visit_id, $given_user_obj);
      }
    } else if (get_class($this) == "SoilTests") {
      if ($this->get_given_id()) {
        $soil_test_id = $this->get_given_id();
        include_once("soil_tests.php");
        $soil_test_obj = new SoilTests();
        $project_id = $soil_test_obj->get_project_id_given_soil_test_id($soil_test_id, $given_user_obj);
      }
    } else if (get_class($this) == "CropPlans") {
      if ($this->get_given_id()) {
        $crop_plan_id = $this->get_given_id();
        include_once("crop_plans.php");
        $crop_plan_obj = new SoilTests();
        $project_id = $crop_plan_obj->get_project_id_given_crop_plan_id($crop_plan_id, $given_user_obj);
      }
    }
    return $project_id;
  }

  // method form
  public function output_select_list($parameter_name, $default_id = "") {
    $markup = "";

    $this->set_type("get_all");
    $this->prepare_query();

    $markup .= "    <select name=\"" . $parameter_name . "\">\n";
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "      <option value=\"" . $obj->get_id() . "\"";
      if ($default_id) {
        if ($default_id == $obj->get_id()) {
          $markup .= " selected=\"selected\"";
        }
      }
      $markup .= ">" . $obj->get_name() . "</option>\n";
    }
    $markup .= "    </select>\n";

    return $markup;
  }

    // method
    public function is_sort_not_expired($given_time_period) {
      // declare variable to be returned with a default value
      $boolean = "";

      // start by getting the string
      $sort = $this->get_sort();

      // debug
      //print "debug scrubber: running is_sort_not_expired() given_time_period = " . $given_time_period . "<br />\n";

      // validate the time periods allowed for this test
      if ($given_time_period) {
        $special_sort_becomes_date_field = "Z ";
        $pos = strpos($sort, $special_sort_becomes_date_field);

        // debug
        //print "debug scrubber: pos = " . $pos . "<br />\n";
        //print "debug scrubber: sort = " . $sort . "<br />\n";
        //print "debug scrubber: special_sort_becomes_date_field = " . $special_sort_becomes_date_field . "<br />\n";

        if ($pos === false) {
          // skip
        } else {
          //$boolean .= $pos . " ";
          // assumes the string begins with special_sort_becomes_date_field
          $last_timecard_date = substr($sort, strlen($special_sort_becomes_date_field));

          include_once("dates.php");
          $date_obj = new Dates;
          if ($given_time_period == "month") {
            if ($date_obj->is_less_than_month_apart($last_timecard_date)) {
              // true, so return the date string
              $boolean .= $last_timecard_date;
            }
          } else if ($given_time_period == "week") {
            if ($date_obj->is_less_than_week_apart($last_timecard_date)) {
              // true, so return the date string
              $boolean .= $last_timecard_date;
            }
          } else if ($given_time_period == "scheduled") {
            if ($date_obj->is_less_than_day_apart($last_timecard_date)) {
              // true, so return the date string
              $boolean .= $last_timecard_date;
            }
          } else {
            // error
            print "error message scrubber: given_time_period is not valid: " . $given_time_peroid . "<br />\n";
          }
        }
      } else {
        // error
        print "error message scrubber: given_time_period is not known<br />\n";
      }

      return $boolean;
    }

  // method
  public function populate() {
    $markup = "";

    $this->determine_type();
    $markup .= $this->prepare_query();
    // check for errors
    if ($markup) {
      print "Error: scrubber populate() prepare_query() message = " . $markup . ",br />\n";
    }

    return $markup;
  }

  // method
  public function url($given_string) {
    return $this->get_given_config()->get_base_url() . $given_string;
  }

  // method
  protected function output_title() {
    $markup = "";

    $markup .= "<h1 class=\"title\">";
    $markup .= get_class($this);

    // note if single, output id
    if ($this->get_list_bliss()->get_count() == 1 &&
        $this->get_given_id()) {
      $markup .= " id " . $this->get_given_id();
    }

    $markup .= "</h1>\n";

    return $markup;
  }

  // method
  public function get_menu_item($given_menu_item, $menu_type = "") {
    $markup = "";

    // note menu_item is an alias for url_parameter

    if ($menu_type == "") {
      # default menu_type is "view"
      $menu_type = "view";
    }

    $given_menu_item_user_clicked;
    if ($menu_type == "sort") {
      # sort
      $given_menu_item_user_clicked = $this->get_given_sort();

    } else if ($menu_type == "view") {
      $given_menu_item_user_clicked = $this->get_given_view();

    } else if ($menu_type == "mode") {
      // todo should this mode be matched with given_view question mark
      $given_menu_item_user_clicked = $this->get_given_view();

    } else if ($menu_type == "subset") {
      $given_menu_item_user_clicked = $this->get_given_subset();

    } else if ($menu_type == "filter") {
      // todo see if this can be nixed and subset be the dominate roleplayer
      $given_menu_item_user_clicked = $this->get_given_filter();

    } else {
      // todo fix error message
      print "error scrubber: menu_type is not known.<br />\n";
    }

    if ($given_menu_item_user_clicked == $given_menu_item) {
      $markup .= $given_menu_item;
    } else {
      // output with link
      $parameter = "?" . $menu_type . "=" . $given_menu_item;
      $request_uri = $this->remove_parameter($_SERVER['REQUEST_URI']);
      $url = $request_uri . $parameter;
      $markup .= "<a href=\"" . $url . "\">" . $given_menu_item . "</a>";
    }

    return $markup;
  }

  // method
  private function remove_parameter($given_uri) {
    return strstr($given_uri, '?', true);
  }

  // method
  protected function redirect_helper($given_target_page = "") {
    if ($given_target_page) {

      // debug
      //print "debug scrubber " . $given_target_page . "<br />\n";

      // jump to another webpage
      include_once("url_helper.php");
      $url_helper = new UrlHelper();
      $url_helper->redirect_simple($given_target_page);
    }
  }

  // method
  public function set_given_variables($class_name, $specifier, $third_seat, $validator_obj) {
    if ($specifier) {
      if ($third_seat) {
        // both specifier and third_seat

        if ($specifier == "account") {
          $this->set_given_account_id($third_seat);

        } else if ($specifier == "domain") {
          $this->set_given_domain_tli($third_seat);

        } else if ($specifier == "plant_categories") {
          $this->set_given_plant_category_id($third_seat);

        } else if ($specifier == "plants") {
          $this->set_given_plant_id($third_seat);

        } else if ($specifier == "plant_id") {
          $this->set_given_plant_id($third_seat);

        } else if ($specifier == "plant_families") {
          $this->set_given_plant_family_id($third_seat);

        } else if ($specifier == "plant_histories") {
          $this->set_given_plant_history_id($third_seat);

        } else if ($specifier == "plant_list_plants") {
          $this->set_given_plant_list_plant_id($third_seat);

        } else if ($specifier == "plant_list_plant") {
          $this->set_given_plant_list_plant_id($third_seat);

        } else if ($specifier == "plant_lists") {
          $this->set_given_plant_list_id($third_seat);

        } else if ($specifier == "processes") {
          $this->set_given_process_id($third_seat);

        } else if ($specifier == "process") {
          $this->set_given_process_id($third_seat);

        } else if ($specifier == "projects") {
          $this->set_given_project_id($third_seat);

        } else if ($specifier == "seed_packets") {
          $this->set_given_seed_packet_id($third_seat);

        } else if ($specifier == "webpages") {
          $this->set_given_webpage_id($third_seat);

        } else {
          print "FRB error: scrubber specifier not known and third_seat not used.<br />\n";
        }

      } else {
        // just specifier (no third_seat)
        if (is_numeric($specifier)) {
          $this->set_given_id($specifier);
        } else if ($validator_obj->is_tli($specifier)) {
          $this->set_given_domain_tli($specifier);
        } else if ($class_name == "usernames") {
          $this->set_given_username($specifier);
        } else {
          //print "FRB error: scrubber specifier not known<br />\n";
        }
      }
    }
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    $markup .= "debug scrubber: given_type = " . $given_type . "; class = " . get_class($this) . "; given_project_id = " . $given_project_id . "<br />\n";
    $markup .= "debug scrubber: override class not yet coded";

    return $markup;
  }

  // method
  public function convert_to_monthly($given_amount_string) {
      if (! $given_amount_string) {
        return $given_amount_string;
      }
      $monthly_amount_string = $given_amount_string / 12;
      return round($monthly_amount_string, 2);
  }

  // method
  public function convert_to_portion_of_scene_elements($given_amount_string, $count_of_matching_scene_elements) {

      // check
      if (! $given_amount_string) {
        // todo perhaps a warning
        return $given_amount_string;
      }

      // check
      if (! $count_of_matching_scene_elements) {
        // todo perhaps a warning
        return $given_amount_string;
      }

      // process
      $amount_string = $given_amount_string / $count_of_matching_scene_elements;

      return round($amount_string, 2);
  }

}
