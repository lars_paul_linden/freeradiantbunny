<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-07
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/url_helper.php

class UrlHelper {

  // method
  public function redirect_simple($given_target_page) {
    // note do not execute if headers already sent, so test for headers sent
    if (! headers_sent()) {
      header('Location: ' . $given_target_page);
    }
  }

  // method
  public function redirect($given_base_url, $given_target_page) {
    $url = $given_base_url . $given_target_page;
    header('Location: ' . $url);
  }

}
