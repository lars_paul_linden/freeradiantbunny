<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/html_table_cell.php

class HtmlTableCell {

  // given
  private $given_markup;

  // given_markup
  public function set_given_markup($var) {
    $this->given_markup = $var;
  }
  public function get_given_markup() {
    return $this->given_markup;
  }

  // attributes
  private $element_class;
  private $data;
  private $visibility;
  private $styles;      // an array

  // element_class
  public function set_element_class($var) {
    $this->element_class = $var;
  }
  public function get_element_class() {
    return $this->element_class;
  }

  // data
  public function set_data($var) {
    $this->data = $var;
  }
  public function get_data() {
    return $this->data;
  }

  // visibility
  public function set_visibility($var) {
    $this->visibility = $var;
  }
  public function get_visibility() {
    return $this->visibility;
  }

  // styles
  public function set_styles($var) {
    $this->styles = $var;
  }
  public function get_styles() {
    if ($this->styles) {
      return $this->styles;
    } else {
      return array();
    }
  }

  // method  
  public function craft_cell($given_requested_visibility) {
    $markup = "";

    // show if either of two criteria are met
    if ($given_requested_visibility == "" ||
        $given_requested_visibility == $this->get_visibility()) {
        $markup .= "  <td";
      if ($this->get_element_class()) {
        $markup .= " class=\"" . $this->get_element_class() . "\"";
      } else if (count($this->get_styles())) {
        $markup .= " style=\"";
        foreach ($this->get_styles() as $style) {
          $markup .= $style;
        }
        $markup .= "\"";
      }
      $markup .= ">\n";
      $markup .= "    ";
      $markup .= $this->get_data();
      $markup .= "\n";
      $markup .= "  </td>\n";
    }

    return $markup;
  }

}
