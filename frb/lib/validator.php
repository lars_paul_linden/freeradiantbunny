<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19
// version 1.4 2015-03-04

// about this file
// http://freeradiantbunny.org/main/en/docs/frb/lib/validator.php

class Validator {

  // method
  public function sanitize_user_input_as_keyword($user_input) {
    $var = "";
    $user_input = strip_tags($user_input);
    $user_input = stripcslashes($user_input);
    $length = strlen($user_input);
    if ($length < 50) {
      if ($user_input == "alphabetical" ||
          $user_input == "random") {
        $var = $user_input;
      }
    }
    return $var;
  }

  // method
  public function is_username_type($user_input) {
    $var = "";
    if (ctype_alpha($user_input)) {
      $length = strlen($user_input);
      if ($length < 20) {
        $var = $user_input;
      }
    }
    return $var;
  }

  // method
  public function sanitize_user_input($user_input) {
    $var = "";
    //  todo read security books on sql injection
    $user_input = strip_tags($user_input);
    $user_input = stripcslashes($user_input);
    $user_input = preg_replace("/[^a-zA-Z0-9\.\/:\;_\- \?]/", "", $user_input);
    $length = strlen($user_input);
    if ($length < 400) {
      $var = $user_input;
    }
    return $var;
  }

  // method
  public function OFFLINE_sanitize_user_input_as_html($user_input) {
    // todo fix this function so that it is not offline
    $var = "";
    // todo read security books on sql injection
    // todo fix the regex so that it matches html
    //$user_input = strip_tags($user_input);
    //$user_input = stripcslashes($user_input);
    //$user_input = preg_replace("/[^a-zA-Z0-9\.\/:_\- ]/", "", $user_input);
    //$length = strlen($user_input);
    //if ($length < 400) {
      $var = $user_input;
    //}
    return $var;
  }

  // method
  public function is_short_alphanumeric($user_input) {
    $var = "";
    // note: the following is like a double-negative
    if (! preg_match("/[^a-zA-Z0-9_]/", $user_input)) {
      $length = strlen($user_input);
      if ($length <= 50) {
        $var = $user_input;
      }
    }
    return $var;
  }

  // method
  public function sanitize_input_as_integer($user_input) {
    $var = ''; // default
    if (is_numeric($user_input)) {
      $var = $user_input;
    } else {
      $this->get_db_dash()->print_error("FRB error: not an integer.");
    }
    return $var;
  }

  // method
  public function is_tli($user_input) {

    // note variable name is set
    $variable_name = "tli";

    // check if an tli (three letter identifier)
    // todo check from database

    if (strlen($user_input) == 3) {
      // debug
      //print "debug validator output is 3 char in length<br />\n";
      if (preg_match("/^[a-z]+$/", $user_input)) {
        // debug
        //print "debug validator output only alpha chars<br />\n";
        // ok
        return $user_input;
      }
    }
    //$markup .= $this->output_error("FRB error: parameter <strong>" . $variable_name . "</strong> is not a valid three letter identifier.");
    return 0;
  }

  // method
  public function validate_id($user_input, $variable_name) {
    $markup = "";

    // if valid, returns null string
    // if not valid, returns error message

    if (! is_numeric($user_input)) {
      $markup .= $this->output_error("FRB error: parameter <strong>" . $variable_name . "</strong> is not a number.");
    } else if ($user_input <= 0) {
      $markup .= $this->output_error("FRB error: parameter <strong>" . $variable_name . "</strong> is not a positive number.");
    } else if (strlen($user_input) > 6) {
      $markup .=  $this->output_error("FRB error: parameter <strong>" . $variable_name . "</strong> is too large of a number.");
    } else {
      // ok, do nothing
    }

    return $markup;
  }

  // method
  public function validate_sort($user_input, $variable_name) {
    $markup = "";

    // if valid, returns null string
    // if not valid, returns error message
    // set the valid values for sort
    if ($user_input == "date" ||
        $user_input == "sort" ||
        $user_input == "domain_name" ||
        $user_input == "hosting" ||
        $user_input == "drupal" ||
        $user_input == "domain" ||
        $user_input == "tli" ||
        $user_input == "album" ||
        $user_input == "name" ||
        $user_input == "plant") {
      // valid
    } else {
      $markup .= $this->output_error("FRB error: parameter <strong>" . $variable_name . "</strong> is not valid.");
    }

    return $markup;
  }

  // method
  public function validate_view($user_input, $variable_name) {
    $markup = "";

    // if valid, returns null string
    // if not valid, returns error message
    // set the valid values for sort
    if ($user_input == "all" ||
        $user_input == "online" ||
        $user_input == "about") {
      // valid
    } else {
      $markup .= $this->output_error("FRB error: parameter <strong>" . $variable_name . "</strong> is not valid.");
    }

    return $markup;
  }

  // method
  public function validate_as_domain_tli($user_input, $variable_name) {
    $markup = "";

    $length = strlen($user_input);
    if ($length == 3) {
      // valid
    } else {
      $markup .= $this->output_error("FRB error: domain_tli <strong>" . $variable_name . "</strong> is not valid.");
    }

    return $markup;
  }

  // method
  public function sanitize_linkmaster_input($user_input) {
    $var = "";
    $user_input = strip_tags($user_input);
    $user_input = stripcslashes($user_input);
    $user_input = preg_replace("/[^a-zA-Z0-9s\.\/:_ \-\(\)\'\=\?&@%;#,~]/", "", $user_input);
    // todo fix single quotes for sql statement
    $user_input = preg_replace("/\'/", "\\\'", $user_input);
    $user_input = preg_replace("/\%/", "\\\%", $user_input);
    $length = strlen($user_input);
    if ($length < 200) {
      $var = $user_input;
    }
    return $var;
  }

  // method
  public function output_error($message, $debug = "") {
   $markup = "";

   // todo note there is a duplicate of this function in databasedashboard
   // todo this function should probably be moved to /lib/errors.php

   if (isset($debug) && $debug == "off") {
      return $markup;
    }
    $markup .= "<p class=\"error\">" . $message . "</p>\n";

    return $markup;
  }

  // method
  public function validate_form_field($given_parameter, $given_validation_type = "") {
    $error_message = "";

    //print "debug validator validating " . $given_validation_type . " " . $_POST[$given_parameter] . "<br />";

    // todo add validation code here
    if ($given_validation_type == "") {
      // default is text
    } else if ($given_validation_type == "date") {
      // string should be like this YYYY-MM-DD
      $pattern = '/^[0-9][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$/';
      $match_count = preg_match($pattern, $_POST[$given_parameter], $matches, PREG_OFFSET_CAPTURE);
      if ($match_count == 0) {
        $error_message = "Not a valid date. Please, use this format: YYYY-MM-DD.";
      }
    } else if ($given_validation_type == "time_am_pm") {
      // string should be like this HH:MM mm
      $pattern = '/^[0-1][0-9]:[0-5][0-9]\s[ap][m]$/';
      $match_count = preg_match($pattern, $_POST[$given_parameter], $matches, PREG_OFFSET_CAPTURE);
      if ($match_count == 0) {
        $error_message = "Not a valid time. Here is an example of a valid date: 06:00 pm.";
      }
    } else if ($given_validation_type == "text_not_null") {
      if (! $_POST[$given_parameter]) {
        $error_message = "Not valid because the field has no text. Please, add text.";
      }
      $sanitized = $this->sanitize_user_input($_POST[$given_parameter]);
      // see if they are different
      if ($_POST[$given_parameter] != $sanitized) {
        $error_message = "Not valid because the text is not valid. Please, add alphanumeric text.";
      }
    } else if ($given_validation_type == "text") {
      $sanitized = $this->sanitize_user_input($_POST[$given_parameter]);
      // see if they are different
      if ($_POST[$given_parameter] != $sanitized) {
        $error_message = "Not valid because the text is not valid. Please, add alphanumeric text.";
      }
    } else if ($given_validation_type == "html") {
      $sanitized = $this->sanitize_user_input_as_html($_POST[$given_parameter]);
      // see if they are different
      if ($_POST[$given_parameter] != $sanitized) {
        $error_message = "Not valid because the text is not valid. Please, add alphanumeric text.";
      }
    } else if ($given_validation_type == "id") {
      if (! is_numeric($_POST[$given_parameter])) {
        $error_message = "Not valid because the field has no text. Please, add text.";
      }
      $sanitized = $this->validate_user_input_alphanumeric($_POST[$given_parameter]);
      // see if they are different
      if ($_POST[$given_parameter] != $sanitized) {
        $error_message = "Not valid because it is not an integer. Please, add an integer.";
      }
    } else if ($given_validation_type == "currency") {
      if (! is_numeric($_POST[$given_parameter])) {
        $error_message = "Not valid because the field is not a number. Please, input a number to represent the price.";
      }
    } else if ($given_validation_type == "number") {
      if (! is_numeric($_POST[$given_parameter])) {
        $error_message = "Not valid because it is not a number. Please, input a number.";
      }
    } else if ($given_validation_type == "url") {
      // string should be like this HH:MM mm
      $pattern = '/^http/';
      $match_count = preg_match($pattern, $_POST[$given_parameter], $matches, PREG_OFFSET_CAPTURE);
      if ($match_count == 0) {
        $error_message = "Not valid because this is not a URL. Please, add an URL.";
      }
      $sanitized = $this->sanitize_user_input($_POST[$given_parameter]);
      // see if they are different
      if ($_POST[$given_parameter] != $sanitized) {
        $error_message = "Not valid because the text is not a URL. Please, input a URL. Starts with http://.";
      }
    } else {
      $error_message = "FRB error: validating-type is not known. $given_validation_type";
    }

    return $error_message;
  }

  // method
  public function extract_class_name($config) {

    // bail
    // if the user inputs something that is not valid, the system bails
    // when the system bails, it skips over processes

    // determine user's intensions
    // start by figuring out which class_name the user is using
    // so, determine if there is a class defined
    // this can be done by checking the parameters
    // note that the .htaccess file changes REST urls to url parameters
    // class_name
    $class_name = "";

    // did the user supply a class_name?
    if (isset($_GET['class_name'])) {
      // filter 1 
      if ($config->get_debug()) {
        // debug
        print "debug validator _GET class_name is set<br />\n";
      }
      if ($this->is_short_alphanumeric($_GET['class_name'])) {
        // filter 2
        if ($config->get_debug()) {
          // debug
          print "debug validator _GET class_name is_short_alphanumerica<br />\n";
        }
        if ($this->is_known_class($_GET['class_name'], $config)) {
          // ok
          $class_name = $this->is_known_class($_GET['class_name'], $config);
          if ($config->get_debug()) {
            // debug
            print "debug validator class_name = " . $class_name . "<br />\n";
          }
          return $class_name;
        } else {
          // error
          print "FRB validator error: not a known class_name: " . $_GET['class_name'] . "<br />\n";
          // because the user's input is not valid the system should bail
          return 0;
        }
      } else {
        // error
        print "FRB error: not a valid class_name.<br />\n";
        // because the user's input is not valid the system should bail
        return 0;
      }
    }

  }

  // method
  public function output_freeradiantbunny($config, $class_name) {

    // debug
    if ($config->get_debug()) {
      print "debug validator output_freeradiantbunny()<br />\n";
    }

    if (! $class_name) {
      // todo should the message be sent?
      // frb message
      print "<p>FRB message: class_name is unknown.</p>";
      // todo is this the correct place to output metadata
      if ($config) {
        // todo add the url function to the href below
        // frb message
        print "<p>FRB message: config says that default_database_name is <a href=\"\">" . $config->get_default_database_name() . "</a>.</p>";
      }
      return "";
    }

    // request_url
    $request_uri = $_SERVER['REQUEST_URI'];

    // factory object
    include_once("factory.php");
    $factory = new Factory($config);
    $obj = $factory->get_object_given_class_name($class_name, $request_uri);

    if (! $obj) {
      print "FRB error: no obj based on class_name.";
      return "";
    }

    $bail = 0;

    // specifier
    $specifier = "";

    // determine user's intensions
    // figure out if the user has specifier and which one it is
    // what a specifier does is to create a subclass of the class_name
    // so, determine if there is a specifier defined
    // this can be done by checking the parameters
    // note that the .htaccess file changes REST urls to url parameters

    // is the specifier set
    if (isset($_GET['specifier'])) {
      // filter 1
      if ($this->is_short_alphanumeric($_GET['specifier'])){
        // filter 2
        if ($this->is_known_specifier($_GET['specifier'], $config) || ($class_name == "usernames" && $this->is_username_type($_GET['specifier'], $config))) {
          // ok
          $specifier = $_GET['specifier'];

          if ($config->get_debug()) {
            // debug
            print "debug validator specifier = " . $specifier . "<br />\n";
          } 
        } else {
          // debug
          print "debug validator: specifier = " . $_GET['specifier'] . "<br />\n";
          // debug
          print "debug validator: user input a specifier that was not known<br />\n";
          // because the user's input is not valid the system should bail
          $bail = true;
        }
      } else {
        // debug
        print "debug validator user input a specifier that was not valid<br />\n";
        // because the user's input is not valid the system should bail
        $bail = true;
      }
    }

    // third_seat
    $third_seat = "";

    if (! $bail && $specifier) {
      // only check for third_seat if there is a specifier (aka second_seat)
      // about the third_seat
      // the third_seat is the specifier of the specifier
      // for example ...
      // ... the class_name goal_statements has a subset of project
      // ... the third_seat specifies which projcet id of the subset is
      // is the third_seat set?
      if (isset($_GET['third_seat'])) {
        // filter 1
        if ($this->is_short_alphanumeric($_GET['third_seat'])) {
          // filter 2
          if ($this->is_known_third_seat($_GET['third_seat'], $config)) {
            // so, assign the third_seat
            $third_seat = $this->is_known_third_seat($_GET['third_seat'], $config);
            if ($config->get_debug()) {
             // debug
             print "debug validator third_seat = " . $third_seat . "<br />\n";
            }
          } else {
            // debug
            print "debug validator user input a third_seat that was not known<br />\n";
            // because the user's input is not valid the system should bail
            $bail = true;
          }
        } else {
          // debug
           print "debug validator user input a third_seat that was not valid<br />\n";
          // because the user's input is not valid the system should bail
          $bail = true;
        }
      }
    }

    // output markup
    if (! $bail) {

      // deal with specifier
      $obj->set_given_variables($class_name, $specifier, $third_seat, $this);

      // markup
      print $obj->get_markup();

    } else {
      if ($config->get_debug()) {
        // debug
        print "debug validator bailing before output<br />\n";
      }
    }
  }

  // method
  private function is_known_class($untrusted_string, $config) {

    // loop through the known_class_names and try to match
    foreach ($config->get_known_class_names() as $known_class_name) {
      // does the user's input class_name match a known_class_name
      if ($known_class_name == $untrusted_string) {
        // match, so assign value
        return $known_class_name;
      }
    }
    return 0;
  }

  // method
  private function is_known_specifier($untrusted_string, $config) {
    $specifier = "";

    // check if specifier is an interger
    if (is_numeric($untrusted_string)) {
      // ok
      $specifier = $untrusted_string;
      if ($config->get_debug()) {
        // debug
        print "debug validator specifier type is numeric = " . $specifier . "<br />\n";
      }
    } else if ($this->is_known_class($untrusted_string, $config)) {
      // ok
      $specifier = $this->is_known_class($untrusted_string, $config);
      if ($config->get_debug()) {
        // debug
        print "debug validator specifier resolved as scoping_class_name = " . $specifier . "<br />\n";
      }
    } else if ($this->is_tli($untrusted_string, $config)) {
      // ok
      $specifier = $this->is_tli($untrusted_string, $config);
      // specifier have 3 letters
      if ($config->get_debug()) {
        // debug
        print "debug validator specifier resolved as tli = " . $specifier . "<br />\n";
      }
    }

    return $specifier;
  }

  // method
  private function is_known_third_seat($untrusted_string, $config) {

    if (is_numeric($untrusted_string)) {
      $third_seat = $untrusted_string;
      // debug
      if ($config->get_debug()) {
        print "debug validator third_seat is numeric.<br />\n";
      }
      return $third_seat;
    } else {
      // third_seat still not resolved, so keep trying
      // does the third_seat have 3 letters
      $variable_name = "tli";
      if (! $this->validate_id($untrusted_string, $variable_name)) {
        $obj->set_given_domain_tli($third_seat);
        $debug = 0;
        if ($debug) {
          // debug
          print "debug validator third_seat resolved as given_domain_tli = " . $obj->get_given_domain_tli() . "<br />\n";
        }
      } else {
        $debug = 0;
        if ($debug) {
          // debug
          print "debug validator third_seat not valiated tli = " . $third_seat. "<br />\n";
        }
      }
    }
  }

  // method
  public function output_id_and_name($config, $class_name) {
    $markup = "";

    // get speicifier
    // note that this does not figure out all of the URL
    $specifier = "";
    // is the specifier set
    if (isset($_GET['specifier'])) {
      // filter 1
      if ($this->is_short_alphanumeric($_GET['specifier'])){
        // filter 2
        if ($this->is_known_specifier($_GET['specifier'], $config) || ($class_name == "usernames" && $this->is_username_type($_GET['specifier'], $config))) {
          // ok
          $specifier = $_GET['specifier'];

          if ($config->get_debug()) {
            // debug
            print "debug validator specifier = " . $specifier . "<br />\n";
          } 
        }
      }
    }

    // output
    if ($specifier) {
      $markup .= $specifier;
    } else {
      // debug
      //print "debug validator: missing specifier = " . $specifier . "<br />\n";
    }

    // todo code this so that it gets the name of the obj

    return $markup;
  }

}
