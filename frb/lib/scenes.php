<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.1 2015-01-03
// version 1.2 2015-01-19

// about this file
// http://freeradiantbunny.org/main/en/docs/frb/lib/scenes.php

class Scenes {

  // attributes
  private $arcs_obj;
  private $scenes_actors_array = array();
  private $offscene_report = "";

  // arcs_obj
  private function get_arcs_obj() {
    if (! isset($this->arcs_obj)) {
      include_once("arcs.php");
      $this->arcs_obj = new Arcs($this->get_given_config());
    }
    return $this->arcs_obj;
  }

  // method
  public function is_socation_class_child($given_obj) {
    if (is_subclass_of($given_obj, "Socation")) {
      return 1;
    }
    return 0;
  }

  // method
  public function add_actor($given_scene_element_obj, $given_polymorphic_object_obj, $given_heading, $debug_flag = 0) {
    $markup = "";

    // heading
    if ($given_heading) {
      //$markup .= "<h3>actor</h3>\n";
    }

    // show the parameters
    if ($debug_flag) {
      // debug
      //$markup .= "debug scenes adding scene_element id " . $given_scene_element_obj->get_id() . " (polymorphic object class = "  . get_class($given_polymorphic_object_obj) . ")<br /><br />\n";
    }

    // only Socation classes can be placed in the scenes
    if ($this->is_socation_class_child($given_polymorphic_object_obj)) {
      // ok, store it on the stage
      include_once("scenes_actors.php");
      $scenes_actor_obj = new ScenesActors($this->get_given_config());
      $scenes_actor_obj->set_scene_element_obj($given_scene_element_obj);
      $scenes_actor_obj->set_polymorphic_object_obj($given_polymorphic_object_obj);

    } else {
      // debug
      //print "debug scenes: not a socation class<br />\n";
      $this->offscene_report .= "Off-scene is non-location scene_elements: "  . get_class($given_polymorphic_object_obj) . "<br />\n";
      // note harsh exit
      // note reject with no message;
      return $markup;
    }

    // add to array
    array_push($this->scenes_actors_array, $scenes_actor_obj);

    // debug
    //$markup .= "debug scenes scenes_actors_array count = " . count($this->scenes_actors_array) . "<br />\n";

    return $markup;
  }

  // method
  public function relate_arcs($given_heading, $debug_flag = 0) {
    $markup = "";

    if ($given_heading) {
      $markup .= "<h3>relate arcs</h3>\n";
    }

    // check that there are enough elements to make an arc
    if (count($this->scenes_actors_array) < 2) {
      $markup .= "no arcs to be made\n";
      return $markup;
    }

    // figure out the edges of the graph
    // provide the actors (nodes) to the arc (edges) class
    // copy the array where a x b = matrix
    $scenes_actors_array_a = $this->scenes_actors_array;
    $scenes_actors_array_b = $this->scenes_actors_array;

    // outside loop
    $num_outside = 0;
    $num_inside = 0;
    //$markup .= "<table class=\"plants\">\n";
    foreach ($scenes_actors_array_a as $scenes_actor_a) {
      $num_outside++;

      //$markup .= "<tr>\n";

      // debug
      //$markup .= "debug scenes <strong>num_outside = " . $num_outside . "</strong><br />\n";

      // inside loop
      foreach ($scenes_actors_array_b as $scenes_actor_b) {
        $num_inside++;

        // debug
        //$markup .= "debug scenes <strong>num_inside = " . $num_inside . "</strong><br />\n";

        // debug
        //$markup .= "<td>\n";
        //$markup .= "  " . $num_outside . ", " . $num_inside . "\n";
        // validate
        $markup .= $this->get_arcs_obj()->add_arc($scenes_actor_a, $scenes_actor_b);

        //$markup .= "</td>\n";

      }

      //$markup .= "</tr>\n";

      // reset
      $num_inside = 0;
    }
    //$markup .= "</table>\n";
    //$markup .= "<br />\n";

    // note arcs are loaded, have each arc relate itself

    // debug
    if ($debug_flag) {
      $markup .= "debug scenes arcs count = " . count($this->get_arcs_obj()->get_arcs_array()) . "<br />\n";
      $markup .= "<br />\n";
    }    

    // todo make a flag that the user can raise and lower
    $markup .= "<p>[relate arcs display is off]</p>\n";
    if (0) {
        $num = 0;
        $markup .= "<table class=\"plants\">\n";
        $last_scenes_actor = "";
        $opposite_direction_arc = array();
        foreach ($this->get_arcs_obj()->get_arcs_array() as $arc_obj) {
          $num++;
          // start with lands (because it is the main "site")
          $scenes_actor_1 = $arc_obj->get_scenes_actor_1();
          $scenes_actor_2 = $arc_obj->get_scenes_actor_2();

          if ($scenes_actor_1 == $last_scenes_actor) {
            // skip
          } else {
            // complete previous row (before starting a new row)
            if (! $last_scenes_actor) {
              $markup .= "</tr>\n";
            }
            // start new row
            $markup .= "<tr>\n";
          }
          $cell_contents = "";

          // debug
          $cell_contents .= "  arc num " . $num . " of " . count($this->get_arcs_obj()->get_arcs_array()) . "<br />\n";

          // output arc details
          // debug
          $cell_contents .= "  " . get_class($scenes_actor_1->get_polymorphic_object_obj()) . ":" . $scenes_actor_1->get_polymorphic_object_obj()->get_id() . ", " . get_class($scenes_actor_2->get_polymorphic_object_obj()) . ":" . $scenes_actor_2->get_polymorphic_object_obj()->get_id() . "<br />\n";

          if ($arc_obj->get_display_flag()) {

            // figure out how classes relate
            // test them here
            // for example, ask: does a machine know a domain?
            // give each location a chance for its own perspective
            // solve until everything is naturally positioned with each other
            // each object has an implied set of relationships with other objects
            // find the current relations of the object (creating a graph)
            // if more than one scene, match these graph and overlap where same
            // all domains are grouped into a "cyberspace" trace
            // if measurement type class then have a "scoreboard" trace
            // the "cyberspace" trace is side-by-side the "lands" trace
            // the "scoreboard" trace is side-by-side the "lands" trace
            // once the overall size is known, all land objects are solved for
            // all of the lands instances must share the "lands" trace
            // "lands" trace shows reflexive data due to relative sizes & locations
            $pm_1_obj = $scenes_actor_1->get_polymorphic_object_obj();
            $pm_2_obj = $scenes_actor_2->get_polymorphic_object_obj();
            if (get_class($scenes_actor_1->get_polymorphic_object_obj()) == get_class($scenes_actor_2->get_polymorphic_object_obj()) && $this->has_arc_to_suppress($pm_1_obj, $pm_2_obj, $opposite_direction_arc)) { 
              // polymorphic objects have same class, so use only the first arc
              $cell_contents .= "opposite arc; display is off";
            } else {
              // note that 2 is before 1 in the following array
              $arc_to_suppress = array($pm_2_obj, $pm_1_obj);
              array_push($opposite_direction_arc, $arc_to_suppress);
              $cell_contents .= $pm_1_obj->relate($pm_2_obj);
            }
          } else {
            // display is off
            // debug
            $cell_contents .= "arc display is off<br />\n";
          }

          if (! $arc_obj->get_display_flag()) {
            $markup .= "<td style=\"vertical-align: top; background-color: #CCCCCC;\">\n";
          } else if (strpos($cell_contents, "opposite arc")) {
            $markup .= "<td style=\"vertical-align: top; background-color: #999999;\">\n";
          } else {
            $markup .= "<td style=\"vertical-align: top;\">\n";
          }
          $markup .= $cell_contents;
          $markup .= "</td>\n";
          // helps to control the table markup
          $last_scenes_actor = $scenes_actor_1;
    
        }
        $markup .= "<table>\n";
    }
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  private function has_arc_to_suppress($pm_1_obj, $pm_2_obj, $opposite_direction_arc) {
    foreach ($opposite_direction_arc as $arc) {
      if ($arc[0] = $pm_1_obj && $arc[1] == $pm_2_obj) {
        return 1;
      }
    }
    return 0;
  }

  // method
  public function process($given_process_id, $given_user_obj, $given_heading) {
    $markup = "";

    // loop and play each
    foreach ($this->scenes_actors_array as $scenes_actor) {
      $markup .= $scenes_actor->get_polymorphic_object_obj()->output_sprite($given_process_id, $given_user_obj, $given_heading);
    }
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function output_offscene_report($given_heading) {
    $markup = "";

    if ($this->offscene_report) {
      if ($given_heading) {
        $markup .= "<h3>offscene report</h3>\n";
      }
      $markup .= "<p>" . $this->offscene_report . "</p>\n";
    }

    return $markup;
  }

  // method
  private function scale() {
    // todo this function has kruft code
          if ($scenes_actor_1->get_width() == "800") {
            // too big, so shrink
            //$markup .= " <em>shrink</em>\n";
            $total_count = $this->get_total_lands_class_count();
            $new_width = round($scenes_actor_1->get_width() / $total_count);
            $new_height = round($scenes_actor_1->get_height() / $total_count);

            // a
            $scenes_actor_1->set_width($new_width);
            $scenes_actor_1->set_height($new_height);
          }
  }

  // method
  public function display($given_process_id, $given_user_obj, $given_heading) {
    $markup = "";

    $markup .= $this->relate_arcs($given_heading);
    $markup .= $this->process($given_process_id, $given_user_obj, $given_heading);
    $markup .= $this->output_offscene_report($given_heading);

    return $markup;
  }

    // sprite
    // solve for initial width and height (size sprites)
    //foreach ($this->scenes_actors_array as $scenes_actor) {
    //  $sprite = $scenes_actor->get_polymorphic_object_obj()->get_sprite();
    //  ob_start();
    //  var_dump($sprite);
    //  $markup .= ob_get_clean() . "<br />\n";
    //}

}
