<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-22

// about this file
// This is for controlling errors from a central place.

class Errors {

  // method
  public function output_missing_error_report($error_report_obj_list, $given_num, $given_class, $given_plant_list_id, $given_user_obj) {
    $markup = "";

     // heading
     $markup .= "<h3>Error Report</h3>";

    // todo this function is old and appears to be cruft
    $missing_obj_list = array();

    if ($given_class == "PlantHistories") {
      include_once(dirname(__DIR__) . "/plant_list_plants.php");
      $plant_list_plants_obj = new PlantListPlants;
      $needed_list = $plant_list_plants_obj->get_pure_list($given_plant_list_id, $given_user_obj);

    } else if ($given_class == "PlantHistoryEvents") {
      // note the code below gets a file in another directory
      include_once(dirname(__DIR__) . "/plant_histories.php");
      $plant_histories_obj = new PlantHistories;
      $needed_list = $plant_histories_obj->get_pure_list($given_plant_list_id, $given_user_obj);

    } else if ($given_class == "Sowings") {
      include_once(dirname(__DIR__) . "/plant_history_events.php");
      $plant_history_events_obj = new PlantHistoryEvents;
      $needed_list = $plant_history_events_obj->get_pure_list($given_plant_list_id, $given_user_obj);

    } else {
      return "<p class=\"error\">Error: no error report, class is not known.</p>\n";
    }

    if (count($needed_list) == 0) {
      return "<p class=\"error\">Error: no error report, the needed_list is empty.</p>\n";
    }

    foreach ($needed_list as $needed_obj) {
      $flag = ""; // reset
      foreach ($error_report_obj_list->get_list() as $id_found) {
        if ($needed_obj->get_id() == $id_found) {
          $flag = "found";
        }
      }
      // now see what the result was... and if not found... record
      if ($flag != "found") {
        array_push($missing_obj_list, $needed_obj);
      }
    }

    // output newly-created list
    if (count($missing_obj_list) > 0) {
      sort($missing_obj_list);
      $markup .= "<p class=\"error\">\n";
      foreach ($missing_obj_list as $missing_obj) {
        $given_num++;
        $markup .= $given_num;
        $markup .= " No " . $given_class . " yet for ";
        if ($given_class == "PlantHistories") {
          $url = url("plant_list_plants/" . $missing_obj->get_id());
          $markup .= "<a href=\"" . $url . "\">plant_list_plants_id = " . $missing_obj->get_id() . "</a>";
        } else if ($given_class == "PlantHistoryEvents") {
          $url = url("plant_histories/" . $missing_obj->get_id());
          $markup .= "<a href=\"" . $url . "\">plant_history_id = " . $missing_obj->get_id() . "</a>";
        } else if ($given_class == "Sowings") {
          $url = url("plant_history_events/" . $missing_obj->get_id());
          $markup .= "<a href=\"" . $url . "\">plant_history_event_id = " . $missing_obj->get_id() . "</a>";
        }
        $markup .= ".<br />\n";
      }
      $markup .= "</p>\n";

    } else {
      // all accounted for
      if ($given_class == "PlantHistories") {
        $markup .= "<em>All <em>PlantListPlants</em> are represented on the " . $given_class . " list.</em>";
      } else if ($given_class == "PlantHistoryEvents") {
        $markup .= "<em>All <em>PlantHistories</em> are represented on the " . $given_class . " list.</em>";
      } else if ($given_class == "Sowings") {
        $markup .= "<em>All <em>PlantHistoryEvents</em> are represented on the " . $given_class . " list.</em>";
      }     
    }

    return $markup;
  }
}
