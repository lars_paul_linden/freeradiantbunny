<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.1 2015-01-03
// version 1.2 2015-01-19

// about this file
// http://freeradiantbunny.org/main/en/docs/frb/lib/scenes_actors.php

class ScenesActors {

   // given
   private $given_scene_element_obj;
   private $given_polymorphic_object_obj;

   // method
   public function set_scene_element_obj($given_scene_element_obj) {
     $this->given_scene_element_obj = $given_scene_element_obj;
   }
   public function get_scene_element_obj() {
     return $this->given_scene_element_obj;
   }

   // method
   public function set_polymorphic_object_obj($given_polymorphic_object_obj) {
     $this->given_polymorphic_object_obj = $given_polymorphic_object_obj;
   }
   public function get_polymorphic_object_obj() {
     return $this->given_polymorphic_object_obj;
   }

}
