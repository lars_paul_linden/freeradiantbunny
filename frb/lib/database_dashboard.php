<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this file
// http://freeradiantbunny.org/main/en/docs/frb/lib/database_dashboard.php

class DatabaseDashboard {

  private $connection;
  private $results; // results after executing something with the database

  // no setter or getter
  private $debug = "off"; // set to "off" to suppress debug
  private $linkmaster_sql_statement = "";

  // connection
  public function get_connection($instance, $database_name = "") {

    // debug
    //print "debug database_dashboard instance " . get_class($instance) . "<br />\n";

    if (! $database_name) {
      $database_name = $instance->get_given_config()->get_default_database_name();
    }

    if (! $this->connection) {
      // instantiate array
      $this->connection = array();
    }

    if (! array_key_exists($database_name, $this->connection)) {

      // the connection is an array
      // maps database_name => connection_obj

      // note the "at" sign in front of function name suppresses warnings
      // note the @ symbol suppresses warnings

      // get connection string with database credentials
      // user info is accessed below
      $database_connection_string = $instance->get_given_config()->get_connection_string($database_name);
      if (! $database_connection_string) {
        $message = "The database_connection_string is not known for database_name = " . $database_name . "<br />";
        $this->output_error($message);
        exit;
      } else {
        // debug
        //print "debug database_dashboard connection_string = " . $database_connection_string . "<br />\n";
      }

      $this->connection[$database_name] = pg_connect($database_connection_string);
    }

    return $this->connection[$database_name];
  }

  // results
  public function get_results() {
    // depricated
    if (! $this->results) {
       //$this->output_error("Error: the database has no results.");
       // todo clean up because this used to be fatal
       // now if no results, then the object will just have to deal with it
       //exit;
    }
    return $this->results;
  }

  // method
  public function get_date() {
    // todo remove this and replace with call to timekeeper class
    // format example: 2010-11-21
    return date('Y-m-d');
  }

  // method
  public function close_connection() {
    if ($this->connection) {
      foreach ($this->connection as $database_name => $resouce) {
        // close it
        if ($this->connection[$database_name]) {
          // debug
          //print "debug database_dashboard: closing database connection where database_name " . $database_name . "<br />";
          pg_close($this->connection[$database_name]);
        }
      }
      unset($this->connection);
    }
  }

  // method
  public function load($instance, $sql, $database_name = "") {
    $error_message = "";

    // debug
    //print "debug database_dashboard load()<br />\n";

    // todo look at all agents and have them use this functionality
    // get databasename based upon class_name
    if (
        get_class($instance) == "Domains" ||
        get_class($instance) == "Webpages" ||
        get_class($instance) == "Tenperdays" ||
        get_class($instance) == "Blogposts" ||
        get_class($instance) == "Applications" ||
        get_class($instance) == "Databases" ||
        get_class($instance) == "EmailAddresses" ||
        get_class($instance) == "HostApplications" ||
        get_class($instance) == "HostDatabases" ||
        get_class($instance) == "HostEmailAddresses" ||
        get_class($instance) == "Maxonomies" ||
        get_class($instance) == "Moneymakers" ||
        get_class($instance) == "Tags" ||
        get_class($instance) == "WebpageMaxonomies" ||
        get_class($instance) == "WebpageMoneymakers" ||
        get_class($instance) == "WebpageTags" ||
        get_class($instance) == "DomainMeasurements"
    ) {
      $database_name = "mudiaco1_mudfrb";
    } else if (get_class($instance) == "Hyperlinks") {
      // use parameter
      // todo hey this is probably not possible, sorry about that 
    }

    if ($database_name == "") {
      // make, print, and return error_message
      $type = $instance->get_type();
      // old message
      $error_message = "database_dashboard.php: load() database_name <strong>" . get_class($instance) . "</strong> is not known; [hint: add to database_dashboard.php file... the type = $type]<br />\n"; 
      //$this->output_error($error_message);
      return $error_message;
    }

    // debug
    //print "debug database_dashboard about to get connection<br />\n";

    if (! $this->get_connection($instance, $database_name)) {
      // todo create an obtion for the verbose message for superuser
      $error_message = "Error: no connection in database_dashboard with database_name = " . $database_name; 
      $this->output_error($error_message);
      return $error_message;
    } else {
      // debug
      //print "debug database_dashboard found connection<br />\n";
    }

    if (! $sql) {
      $error_message = "Error database_dashboard load() <strong>sql</strong> is not known; $type = $type<br />\n"; 
      $this->output_error($error_message);
      return $error_message;
    }

    // debug
    //print "debug database_dashboard instance = " . get_class($instance) . "<br />\n";
    //print "debug database_dashboard database_name = " . $database_name . "<br />\n";
    //print "debug database_dashboard sql = " . $sql . "<br />\n";

    $connection = $this->get_connection($instance, $database_name);
    $this->results = pg_exec($connection, $sql);
 
   if ($this->results) {
      // ok
      // debug
      //print "debug results found.<br />\n";
      //print "debug database_dashboard row count = " . pg_num_rows($this->results) . "<br />\n";    
    } else {
      // output error meesage
      print "database_dashboard.php: pg_last_error = " . pg_last_error() . "<br />\n";
      print "database_dashboard.php: sql = " . $sql . "<br />\n";
      $error_message .= $this->output_error(pg_last_error());
      $error_message .= $this->output_error("database_dashboard.php: database_name = $database_name\n");
      $error_message .= $this->output_error("database_dashboard.php: no data found.\n");
    }

    return $error_message;
  }

  // method
  public function insert($instance, $type, $sql, $database_name) {
    $error_message = "";

    // todo kludge to update the database_name 
    //if ($database_name == "mudiacom_soiltoil" || $database_name == "plantdot_soiltoil") {
    //    $database_name = "eatloca1_soiltoil";
    //}

    // the @ symbol suppresses warning messages
    //@pg_exec($this->get_connection($instance, $database_name), $sql);
    pg_exec($this->get_connection($instance, $database_name), $sql);
    if (pg_last_error($this->get_connection($instance, $database_name))) {
      $error_message = pg_last_error($this->get_connection($instance, $database_name));
    }

    return $error_message;
  }

  // method
  public function update($instance, $type) {
    return $this->insert($instance, $type);
  }

  // method
  public function output_error($message, $debug = "") {
    // todo remove this and replace with function from errors class
    if (isset($debug) && $debug == "off") {
      return;
    }
    print "<span class=\"error\">" . $message . "</span><br />\n";
  }

  // method
  public function new_update($instance, $sql) {
    $markup = "";

    // update the database
    $result = pg_exec($this->get_connection($instance), $sql);
    // test if results is defined
    if (isset($results)) {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        print "debug database_dashboard: row element: " . pg_result($results, $lt, 0) . "<br />\n";
      }
    } else {
      //$markup .= $this->output_error(pg_last_error());
      //$markup .= $this->output_error("Error: database_name = $database_name\n");
      //$markup .= $this->output_error("Error: no data found.\n");
    }
    // check for errrors
    if (pg_last_error($this->get_connection($instance))) {
      $markup .= pg_last_error($this->get_connection($instance));
    }

    return $markup;
  }

  // method
  public function new_insert($instance, $database_name, $sql) {
    $markup = "";

    // set default
    $error_message = "";

    // update the dataase
    //@pg_exec($this->get_connection($instance), $sql);
    pg_exec($this->get_connection($instance), $sql);
    if (pg_last_error($this->get_connection($instance))) {
      $error_message = pg_last_error($this->get_connection($instance));
    }

    // check if there were any errors
    if ($error_message) {
      $markup .= $this->output_error("Not saved.");
      $markup .= $this->output_error($error_message);
    } else {
      $markup .= "<p style=\"background-color: green; color: yellow;\">Saved.</p>\n";
    }

    return $markup;
  }

  // method
  public function free_memory() {
    if (isset($this->results)) {
      pg_free_result($this->results);
      unset($this->results);
    }
  }

}
