<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-22

// about this class
// This class is used to model the output of perl crunch.pl XML data.

class Xml {

  // attributes
  private $linkmaster_crunch; // raw data from the database field
  private $linkmaster_crunch_exploded; // array of lines
 
  // linkmaster_crunch
  public function set_linkmaster_crunch($var) {
    $this->linkmaster_crunch = $var;
  }
  public function get_linkmaster_crunch() {
    return $this->linkmaster_crunch;
  }

  // method
  public function convert_brackets($string) {
    $lc = preg_replace("/</", "&lt;", $string);
    $lc = preg_replace("/>/", "&gt;", $lc);
    return $lc;
  }

  // method
  public function get_linkmaster_crunch_xml() {
    return $this->convert_brackets($this->linkmaster_crunch);
  }

  // method
  public function get_linkmaster_crunch_exploded() {
    if (! $this->linkmaster_crunch_exploded) {
      $this->linkmaster_crunch_exploded = explode("\n", $this->linkmaster_crunch);
    }
    return $this->linkmaster_crunch_exploded;
  }

  // method
  public function get_doctype() {
    $pattern = '/<doctype>(.*)<\/doctype>/';
    preg_match($pattern, $this->linkmaster_crunch, $matches);
    return $this->convert_brackets($matches[1]);
  }

  // method
  public function get_title() {
    $pattern = '/<title>(.*)<\/title>/';
    preg_match($pattern, $this->linkmaster_crunch, $matches);
    return $this->convert_brackets($matches[1]);
  }

  // author
  public function get_author() {
    // start with meta
    $pattern = '/<meta>(.*)<\/meta>/';

    // loop
    foreach($this->get_linkmaster_crunch_exploded() as $line) {
      if (preg_match($pattern, $line, $matches)) {
        // found the pattern
        // now, test if this has an author
        if (strstr($matches[1], "author")) {
          $pattern2 = '/content="(.*)"/';
          if (preg_match($pattern2, $matches[1], $matches2)) {
            // found the pattern, so return
            return $matches2[1];
          }
        }
      }
    }

    // not found
    return "";
  }

  // location
  public function get_location() {
    $pattern = '/<location>(.*)<\/location>/';
    preg_match($pattern, $this->linkmaster_crunch, $matches);
    return $matches[1];
  }

  // quotes
  public function get_quotes() {
    $pattern = '/<quote>(.*)<\/quote>/';
    preg_match($pattern, $this->linkmaster_crunch, $matches);
    return $matches[1];
  }

}
