<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/satabase.php

abstract class Satabase {

  // method
  function __construct($given_config) {
    $this->set_given_config($given_config);
    if ($given_config->get_debug()) {
      // debug
      //print "debug " . get_class($this) . " constructing " . get_class($this) . "<br />\n";
    }
  }
 
  private $db_dash;        // holds database instance
  // db_dash
  protected function set_db_dash($var) {
    $this->db_dash = $var;
  }

  protected function get_db_dash() {
    if (! isset($this->db_dash)) {
      include_once("database_dashboard.php");
      $this->db_dash = new DatabaseDashboard();
    }
    return $this->db_dash;
  }

  // method
  public function get_row_count_from_db($given_user_obj) {
    $markup = "";

    $this->set_user_obj($given_user_obj);

    $this->set_type("get_row_count");

    $markup .= $this->prepare_query();

    if (! defined($this->get_list_bliss()->get_first_element())) {
      // note skip
      // note return a zero value rather than an error 
      // todo should probably build error message
      return 0;
    }
    return $this->get_list_bliss()->get_first_element()->get_row_count();
  }

}
