<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2016-08-26

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/statuskeeper.php

class StatusKeeper {


  // method
  public function calculate_cell_color($given_status = "") {

    // define colors
    $color_z = "#CD0000"; # firebrick red;
    $color_1 = "#84BE6A"; # green
    $color_2 = "#33811D"; # dark green
    $color_3 = "#3D77E4"; # blue
    $color_4 = "#7F49D0"; # purple
    $color_5 = "#E28D31"; # orange
    $color_6 = "#CD5555"; # red
    $color_7 = "#3BF965"; # yellow-green

    if ($given_status == "offline") {
      // note offline is red
      $color = $color_z;
    } else {
      $color = $color_1;
    }

    return $color;
  }

}
