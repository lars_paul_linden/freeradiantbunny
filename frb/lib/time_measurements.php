<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-19

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/time_measurements.php

class TimeMeasurements {

  // given
  private $given_timecard_id;
  private $given_subtotal_string;
  private $grand_subtotal_string_hours = 0;

  // given_timecard_id
  public function get_given_timecard_id() {
    return $this->given_timecard_id;
  }

  // given_subtotal_string
  public function get_given_subtotal_string() {
    return $this->given_subtotal_string;
  }

  // attributes
  public $firstmost_date;
  public $lastmost_date;

  // firstmost_date
  public function set_firstmost_date($date_as_string) {
    $this->given_timecard_id = $date_as_string;
  }

  // lastmost_date
  public function set_lastmost_date_as_now() {
    include_once("dates.php");
    $time_in_class_dates_obj = new Dates;
    $this->given_timecard_id = $time_in_class_dates_obj->get_now_date(); 
  }

  // derived
  private $total_hours;

  // method
  public function get_total_hours() {
    // use measure_time for setter
    return $this->total_hours;
  }

  // method
  public function measure_time($given_timecard_id, $given_date, $given_time_in, $given_time_out, $given_subtotal_string, $given_description) {

    // todo assumes that both readings are the same date (I think)...
    // todo so test that the dates are indeed the same

    // assumes sorted by date
    if (! isset($this->firstmost_date)) {
      $this->firstmost_date = $given_date;
    }
    $this->lastmost_date = $given_date;

    // debug flag
    //$debug_flag = "measure_time()\n";
    $debug_flag = "";

    // set id
    $this->given_timecard_id = $given_timecard_id;
    $this->given_subtotal_string = $given_subtotal_string;

    // make time instances
    if ($debug_flag) {
      $debug .= "given_date = " . $given_date . "\n";
      $debug .= "given_time_in = " . $given_time_in . "\n";
      $debug .= "given_time_out = " . $given_time_out . "\n";
    }

    // create a date obj based upon time_in
    include_once("dates.php");
    $time_in_class_dates_obj = new Dates;
    $time_in_datetime_obj = $time_in_class_dates_obj->get_date_obj($given_date); 

    // create a date obj based upon time_out
    include_once("dates.php");
    $time_out_class_dates_obj = new Dates;
    $time_out_datetime_obj = $time_out_class_dates_obj->get_date_obj($given_date); 

    if ($debug_flag) {
      $debug .= "given_subtotal-string = " . $this->get_given_subtotal_string() . "\n";
    }

    // calculate decimal_time
    $hours = $this->calculate_decimal_time($time_in_datetime_obj, $time_out_datetime_obj, $given_time_in, $given_time_out);

    // trim the leading zeroes (for example 02 --> 2)
    $hours = ltrim($hours, "0");
    if ($hours == "") {
      $hours = 0;
    }

    if ($debug_flag) {
      $debug .= "hours = " . $hours . "\n";
    }

    // log stuff
    // add to total hours

    if (! isset($this->total_hours)) {
      // initialize
      $this->total_hours = $hours;
    } else {
      // add to exising
      $this->total_hours += $hours;
    }

    if ($debug_flag) {
      $debug .= "total_hours = " . $this->get_total_hours() . "\n";
    }

    if ($given_description && $this->get_given_subtotal_string()) {
      if (strstr(strtolower($given_description), strtolower($this->get_given_subtotal_string()))) {
        $this->grand_subtotal_string_hours = $this->grand_subtotal_string_hours + $hours;

        // debug
        //print "<p>subtotal_string found, so hours = " . $hours . "</p>\n";
        //print "<p>subtotal_string found, now grand_subtotal_string_hours = " . $this->grand_subtotal_string_hours . "</p>\n";

      }
    }

    // derive stuff
    // discover weekday (based upon given_time_in)
    $weekday = "unknown";
    $weekday_code = $time_in_datetime_obj->format("l");

    if ($debug_flag) {
      $debug .= "weekday_code = " . $weekday_code . "\n";
    }
    
    if ($weekday_code == "Monday") {
      $weekday = "monday";
    } else if ($weekday_code == "Tuesday") {
      $weekday = "tuesday";
    } else if ($weekday_code == "Wednesday") {
      $weekday = "wednesday";
    } else if ($weekday_code == "Thursday") {
      $weekday = "thursday";
    } else if ($weekday_code == "Friday") {
      $weekday = "friday";
    } else if ($weekday_code == "Saturday") {
      $weekday = "saturday";
    } else if ($weekday_code == "Sunday") {
      $weekday = "sunday";
    }    

    if ($debug_flag) {
      $debug .= "weekday = " . $weekday . "\n";
    }

    if ($debug_flag) {
      print "<pre>debug time_measurement debug = " . $debug . "</pre><br />\n";
    }

    return $hours;
  }

  // weekday hours
  private $monday_hours;
  private $tuesday_hours;
  private $wednesday_hours;
  private $thursday_hours;
  private $friday_hours;
  private $saturday_hours;
  private $sunday_hours;

  // weekday hours
  private function set_week_day_hours($var, $weekday) {
    if ($weekday == "monday") {
      $this->monday_hours = $var;
    } else if ($weekday == "tuesday") {
      $this->tuesday_hours = $var;
    } else if ($weekday == "wednesday") {
      $this->wednesday_hours = $var;
    } else if ($weekday == "thursday") {
      $this->thursday_hours = $var;
    } else if ($weekday == "friday") {
      $this->friday_hours = $var;
    } else if ($weekday == "saturday") {
      $this->saturday_hours = $var;
    } else if ($weekday == "sunday") {
      $this->sunday_hours = $var;
    } else {
      print "time_measurement error";
    }
  }

  public function get_week_day_hours() {
    if ($weekday == "monday") {
      return $this->monday_hours;
    } else if ($weekday == "tuesday") {
      return $this->tuesday_hours;
    } else if ($weekday == "wednesday") {
      return $this->wednesday_hours;
    } else if ($weekday == "thursday") {
      return $this->thursday_hours;
    } else if ($weekday == "friday") {
      return $this->friday_hours;
    } else if ($weekday == "saturday") {
      return $this->saturday_hours;
    } else if ($weekday == "sunday") {
      return $this->sunday_hours;
    } else {
      print "time_measurement error";
    }
  }

  // todo can this be removed?
  //private $total_weekly_hours_given_week; // monday date of week => total hours

  // method
  public function get_weekly_average_of_total_hours() {
  }

  // method
  private function calculate_decimal_time($time_in_datetime_obj, $time_out_datetime_obj, $given_time_in, $given_time_out) {
    $hours = 0;

    include_once("dates.php");
    $class_dates_obj = new Dates;

    // parse given_time_in
    $pattern = '/(.*):(.*) (.*)/';
    if (preg_match($pattern, $given_time_in, $matches)) {
      $hours_string = $matches[1];
      //print "time_measurement hours_string: " . $hours_string . "<br />\n";
      $minutes_string = $matches[2];
      $am_pm_string = trim($matches[3]);
      // duplicated below
      if ($am_pm_string == "am") {
        if ($hours_string == "12") {
          // reset to 0
          $hours_string = 0;
        } else {
          // skip
        }
        //print "am<br />\n";
      } else if ($am_pm_string == "pm") {
        if ($hours_string == "12") {
          // skip
        } else {
          // advance to afternoon by adding 12 hours
          $hours_string += 12;
        }
        //print "pm<br />\n";
      } else {
        print "error time_measurement: am_pm_string not known<br />\n";
      }

      // debug
      //print "debug time_measurement set time: " . $hours_string . "<br />\n";
      //print "debug time_measurement set time: " . $minutes_string . "<br />\n";

      // set time
      $time_in_datetime_obj->setTime($hours_string, $minutes_string);
    }

    // parse given_time_out
    if (preg_match($pattern, $given_time_out, $matches)) {
      $hours_string = $matches[1];
      //print "debug time_measurement hours_string: " . $hours_string . "<br />\n";
      $minutes_string = $matches[2];
      $am_pm_string = $matches[3];
      // from above
      if ($am_pm_string == "am") {
        if ($hours_string == "12") {
          // reset to 0
          $hours_string = 0;
        } else {
          // skip
        }
        //print "am<br />\n";
      } else if ($am_pm_string == "pm") {
        if ($hours_string == "12") {
          // skip
        } else {
          // advance to afternoon by adding 12 hours
          $hours_string += 12;
        }
        //print "pm<br />\n";
      } else {
        print "error time_measurement: am_pm_string not known<br />\n";
      }
      // set time
      //print "debug time_measurement set time: " . $hours_string . "<br />\n";
      //print "debug time_measurement set time: " . $minutes_string . "<br />\n";
      $time_out_datetime_obj->setTime($hours_string, $minutes_string);
    }
    
    // debug
    //print "<p>time_in  = " . date_format($time_in_datetime_obj, 'Y-m-d H:i:s')  . "</p>\n";
    //print "<p>time_out = " . date_format($time_out_datetime_obj, 'Y-m-d H:i:s')  . "</p>\n";

    // compare
    $interval_obj = date_diff($time_in_datetime_obj, $time_out_datetime_obj);

    // from the manual, here is an example
    //$out = $interval_obj->format("Years:%Y,Months:%M,Days:%d,Hours:%H,Minutes:%i,Seconds:%s");

    // for now I just want to sum these up by their given duration in hours

    // hours
    $hours = $interval_obj->format("%H");

    // minutes
    $minutes = $interval_obj->format("%i");
    $decimal_minutes = $minutes / 60;

    // debug
    //print "<p>debug raw hours = " . $hours . "</p>\n";
    //print "<p>debug raw minutes = " . $minutes . "</p>\n";
    //print "<p>debug raw decimal_minutes = " . $decimal_minutes . "</p>\n";

    $decimal_time = $hours + $decimal_minutes;
    $decimal_time = round($decimal_time, 2);
    $decimal_time = sprintf("%01.2f", $decimal_time);

    return $decimal_time;
  }

  // method
  public function get_grand_subtotal_string_hours() {
   return $this->grand_subtotal_string_hours;
  }

  // method 
  public function get_week_count() {
    $week_count = "uknown";
    $week_count = $this->firstmost_date . " " . $this->lastmost_date . "<br />\n";

    include_once("dates.php");
    $firstmost_class_dates_obj = new Dates;
    $firstmost_datetime_obj = $firstmost_class_dates_obj->get_date_obj($this->firstmost_date); 
    $lastmost_class_dates_obj = new Dates;
    $lastmost_datetime_obj = $lastmost_class_dates_obj->get_date_obj($this->lastmost_date); 

    $interval_obj = date_diff($firstmost_datetime_obj, $lastmost_datetime_obj);
    // from the manual, here is an example
    //$out = $interval_obj->format("Years:%Y,Months:%M,Days:%d,Hours:%H,Minutes:%i,Seconds:%s");
    // for now I just want to sum these up by their given duration in hours
    // weeks

    $days = $interval_obj->days;
    $week_count = round($days / 7);

    return $week_count;
  }

  // method
  public function get_interval($given_time_start, $given_time_finish) {

    // create a date obj based upon time_in
    include_once("dates.php");
    $date_obj = new Dates;
    $start_timestamps_obj = $date_obj->get_timestamps_obj($given_time_start); 
    $finish_timestamps_obj = $date_obj->get_timestamps_obj($given_time_finish); 

    // calculate decimal_time
    $interval = $this->calculate_interval($start_timestamps_obj, $finish_timestamps_obj);

    return $interval;
  }

  //method
  public function calculate_interval($given_start_timestamps_obj, $given_finish_timestampts) {
    $markup = "";

    $interval = $given_start_timestamps_obj->diff($given_finish_timestampts);
    $markup .= $interval->format('%a days %h hours');

    return $markup;
  }

}
