<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-22

// about this class
// This class helps with the colors on the webpages.

class Colors {

  // method
  public function get_color($number_of_days) {
    $color = "#FFFFFF";     
    if ($number_of_days < 0) {
      // red
      $color = "#FF6666";
    } else if ($number_of_days <= 2) {
      // green
      // fresh, ready to harvest
      $color = "#33FF66";
    } else if ($number_of_days <= 4) {
      // ice blue
      // storage
      $color = "#8DEEEE";
    } else {
      // old
      // same color as simulated data cells
      $color = "#EEC591";
    }
    return $color;
  }

  // method
  public function get_color_simple($number_of_days) {
    $color = "#FFFFFF";     
    if ($number_of_days < 0) {
      // red
      $color = "#FF6666";
    }
    return $color;
  }

}
