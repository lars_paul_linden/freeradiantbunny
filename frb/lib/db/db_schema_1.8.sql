-- FreeRadiantBunny
-- Copyright (C) 2015 Lars Paul Linden
-- see README.txt

-- FreeRadiantBunny database schema
-- for use with postgres
-- version 1.4 2015-03-15
-- version 1.8 2017-04-29

-- about this file
-- use psql to install the following SQL statements
-- for more about this file see the following webpage
-- http://freeradiantbunny.org/main/en/docs/frb/lib/db_schemas.php

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
--- SET search_path = public, pg_catalog;

--
-- Name: account_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE account_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.account_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE accounts (
    id integer DEFAULT nextval('account_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    class_name_string text,
    sort text,
    status text,
    img_url text,
    database_string text,
    class_primary_key_string text,
    state text
);


ALTER TABLE public.accounts OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: agricultural_type_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE agricultural_type_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.agricultural_type_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: agricultural_types; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE agricultural_types (
    id integer DEFAULT nextval('agricultural_type_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    img_url text
);


ALTER TABLE public.agricultural_types OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: album_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE album_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.album_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: albums; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE albums (
    id integer DEFAULT nextval('album_id_seq'::regclass) NOT NULL,
    name text,
    year character varying(4),
    cover_front_url text,
    cover_back_url text,
    notes text,
    album_url text
);


ALTER TABLE public.albums OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: application_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE application_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.application_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: applications; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE applications (
    id integer DEFAULT nextval('application_id_seq'::regclass) NOT NULL,
    name text,
    url text,
    source_code_url text,
    development text,
    sort text DEFAULT '?'::text,
    img_url text,
    description text,
    status text
);


ALTER TABLE public.applications OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: asset_type_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE asset_type_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.asset_type_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: asset_types; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE asset_types (
    id integer DEFAULT nextval('asset_type_id_seq'::regclass) NOT NULL,
    name text
);


ALTER TABLE public.asset_types OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: blog_post_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE blog_post_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.blog_post_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: blogposts; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE blogposts (
    id integer DEFAULT nextval('blog_post_id_seq'::regclass) NOT NULL,
    name text,
    body text,
    img_url text,
    tags text,
    url_alias text,
    author text,
    pubdate text,
    webpage_id integer,
    database_string text,
    class_name_string text,
    class_primary_key_string text,
    sort text
);


ALTER TABLE public.blogposts OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: book_clip_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE book_clip_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.book_clip_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: book_clips; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE book_clips (
    id integer DEFAULT nextval('book_clip_id_seq'::regclass) NOT NULL,
    image_id integer,
    plant_id integer,
    excerpt text,
    see_also text,
    book_id integer
);


ALTER TABLE public.book_clips OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: book_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE book_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.book_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: books; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE books (
    id integer DEFAULT nextval('book_id_seq'::regclass) NOT NULL,
    name text,
    url text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.books OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: budget_accounts; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE budget_accounts (
    budget_id integer NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE public.budget_accounts OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: budget_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE budget_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.budget_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: budgets; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE budgets (
    id integer DEFAULT nextval('budget_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    scene_element_id integer NOT NULL,
    img_url text
);


ALTER TABLE public.budgets OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: build_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE build_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.build_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: builds; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE builds (
    id integer DEFAULT nextval('build_id_seq'::regclass) NOT NULL,
    project_id integer,
    status text,
    description text,
    sort text,
    name text,
    img_url text,
    client_supplier_id integer,
    versions text,
    ranking text
);


ALTER TABLE public.builds OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: business_plan_text_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE business_plan_text_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.business_plan_text_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: business_plan_texts; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE business_plan_texts (
    id integer DEFAULT nextval('business_plan_text_id_seq'::regclass) NOT NULL,
    description text,
    name text,
    sort text,
    status text,
    img_url text,
    goal_statement_id integer NOT NULL,
    "order" integer
);


ALTER TABLE public.business_plan_texts OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: calendar_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.calendar_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: calendars; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE calendars (
    id integer DEFAULT nextval('calendar_id_seq'::regclass) NOT NULL,
    name text
);


ALTER TABLE public.calendars OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: class_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE class_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.class_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: classes; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE classes (
    id integer DEFAULT nextval('class_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    ownership text,
    notes text,
    db_mysql text,
    user_name text,
    zachman_id integer,
    fields text,
    sort text,
    status text,
    img_url text,
    relationship text,
    db_postgres text,
    extends_class_id integer,
    spare_code text,
    rank text,
    subsystem text
);


ALTER TABLE public.classes OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: color_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE color_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.color_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: colors; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE colors (
    id integer DEFAULT nextval('color_id_seq'::regclass) NOT NULL,
    tla character varying(3),
    name text,
    url text,
    hex_code text,
    full_name text
);


ALTER TABLE public.colors OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE customer_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.customer_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: customers; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE customers (
    id integer DEFAULT nextval('customer_id_seq'::regclass) NOT NULL,
    name text,
    project_id integer
);


ALTER TABLE public.customers OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: database_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE database_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.database_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: databases; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE databases (
    id integer DEFAULT nextval('database_id_seq'::regclass) NOT NULL,
    management_system character varying(20),
    name text,
    date_last_backup text,
    schema_version text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.databases OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: design_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE design_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.design_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: design_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE design_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.design_instance_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: design_instances; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE design_instances (
    id integer DEFAULT nextval('design_instance_id_seq'::regclass) NOT NULL,
    design_id integer,
    name text,
    status text,
    unit_id integer,
    description text,
    sort text,
    img_url text
);


ALTER TABLE public.design_instances OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: design_order_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE design_order_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.design_order_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: design_order_item_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE design_order_item_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.design_order_item_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: design_order_items; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE design_order_items (
    id integer DEFAULT nextval('design_order_item_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    design_order_id integer,
    cost text,
    shipping_cost text,
    design_instance_id integer
);


ALTER TABLE public.design_order_items OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: design_orders; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE design_orders (
    id integer DEFAULT nextval('design_order_id_seq'::regclass) NOT NULL,
    date text,
    note text,
    supplier_id integer
);


ALTER TABLE public.design_orders OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: designer_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE designer_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.designer_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: designers; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE designers (
    id integer DEFAULT nextval('designer_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.designers OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: designs; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE designs (
    id integer DEFAULT nextval('design_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    project_id integer,
    domain_tli character varying(3),
    img_url text
);


ALTER TABLE public.designs OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: documentation_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE documentation_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.documentation_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: documentations; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE documentations (
    id integer DEFAULT nextval('documentation_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    user_name text,
    categorization text,
    table_name text,
    img_url text,
    how_to_measure text
);


ALTER TABLE public.documentations OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: domain_measurement_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE domain_measurement_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.domain_measurement_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: domain_measurements; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE domain_measurements (
    id integer DEFAULT nextval('domain_measurement_id_seq'::regclass) NOT NULL,
    domain_tli character varying(3),
    domain_field_string text,
    webmaster_responsibility text,
    sort text
);


ALTER TABLE public.domain_measurements OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: domains; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE domains (
    tli character varying(3) NOT NULL,
    domain_name character varying(100),
    tagline text,
    img_url text,
    sort character varying(12),
    registrar text,
    hosting text,
    status text,
    crm text,
    name text,
    user_name text,
    spotlight text,
    backups text,
    log text,
    design_id integer,
    description text
);


ALTER TABLE public.domains OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: email_address_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE email_address_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.email_address_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: email_addresses; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE email_addresses (
    id integer DEFAULT nextval('email_address_id_seq'::regclass) NOT NULL,
    address text,
    name text,
    sort text,
    user_name text,
    domain_tli text,
    status text,
    img_url text,
    description text
);


ALTER TABLE public.email_addresses OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: event_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE event_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.event_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: events; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE events (
    id integer DEFAULT nextval('event_id_seq'::regclass) NOT NULL,
    time_start timestamp with time zone DEFAULT '2014-04-01 04:00:00-04'::timestamp with time zone,
    time_finish timestamp with time zone DEFAULT '2014-04-01 03:00:00-04'::timestamp with time zone,
    name text
);


ALTER TABLE public.events OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: field_test_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE field_test_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.field_test_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: field_tests; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE field_tests (
    id integer DEFAULT nextval('field_test_id_seq'::regclass) NOT NULL,
    date text,
    farmer text,
    garment text,
    farm_task text,
    field_report text
);


ALTER TABLE public.field_tests OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: goal_statement_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE goal_statement_id_seq
    START WITH 7
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.goal_statement_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: goal_statements; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE goal_statements (
    id integer DEFAULT nextval('goal_statement_id_seq'::regclass) NOT NULL,
    description text,
    project_id integer NOT NULL,
    sort text,
    status text,
    name text,
    img_url text
);


ALTER TABLE public.goal_statements OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: guest_pass_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE guest_pass_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.guest_pass_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: guest_passes; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE guest_passes (
    id integer DEFAULT nextval('guest_pass_id_seq'::regclass) NOT NULL,
    owner_name text,
    url text,
    guest_name text
);


ALTER TABLE public.guest_passes OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: harvest_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE harvest_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.harvest_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: harvests; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE harvests (
    id integer DEFAULT nextval('harvest_id_seq'::regclass) NOT NULL,
    project_id integer,
    name text,
    sort integer,
    shares_estimate integer
);


ALTER TABLE public.harvests OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: host_applications; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE host_applications (
    domain_tli character varying(3) NOT NULL,
    application_id integer NOT NULL,
    machine_id integer NOT NULL
);


ALTER TABLE public.host_applications OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: host_databases; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE host_databases (
    domain_tli character varying(3) NOT NULL,
    machine_id integer NOT NULL,
    database_id integer NOT NULL
);


ALTER TABLE public.host_databases OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: host_email_addresses; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE host_email_addresses (
    domain_tli character varying(3) NOT NULL,
    machine_id integer NOT NULL,
    email_address_id integer NOT NULL
);


ALTER TABLE public.host_email_addresses OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: hosts; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE hosts (
    parent_class_name_string text NOT NULL,
    parent_class_primary_key_string text NOT NULL,
    child_class_name_string text NOT NULL,
    child_class_primary_key_string text NOT NULL
);


ALTER TABLE public.hosts OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: hyperlink_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE hyperlink_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hyperlink_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: hyperlink_reasons; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE hyperlink_reasons (
    id integer NOT NULL,
    hyperlink_id integer,
    reason_id integer
);


ALTER TABLE public.hyperlink_reasons OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: hyperlink_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE hyperlink_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hyperlink_tag_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: hyperlinks; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE hyperlinks (
    id integer DEFAULT nextval('hyperlink_id_seq'::regclass) NOT NULL,
    date character varying(10),
    url text,
    name text,
    addon text,
    alphabetical text,
    user_name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.hyperlinks OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: image_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE image_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.image_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: images; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE images (
    id integer DEFAULT nextval('image_id_seq'::regclass) NOT NULL,
    caption text,
    description text,
    photographer text,
    url text,
    license text
);


ALTER TABLE public.images OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: indiegoals; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE indiegoals (
    id integer NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    date text NOT NULL,
    reading text,
    yawp_agent_type text
);


ALTER TABLE public.indiegoals OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: indiegoal_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE indiegoal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.indiegoal_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: indiegoal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER SEQUENCE indiegoal_id_seq OWNED BY indiegoals.id;


--
-- Name: invoice_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE invoice_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.invoice_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: invoice_line_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE invoice_line_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.invoice_line_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: invoice_lines; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE invoice_lines (
    id integer DEFAULT nextval('invoice_line_id_seq'::regclass) NOT NULL,
    invoice_id integer,
    product_id integer,
    quantity text,
    price text,
    note text
);


ALTER TABLE public.invoice_lines OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: invoices; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE invoices (
    id integer DEFAULT nextval('invoice_id_seq'::regclass) NOT NULL,
    customer_id integer,
    date text
);


ALTER TABLE public.invoices OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: journal_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE journal_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.journal_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: journals; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE journals (
    id integer DEFAULT nextval('journal_id_seq'::regclass) NOT NULL,
    name text
);


ALTER TABLE public.journals OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: kernel_theory_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE kernel_theory_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.kernel_theory_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: kernel_theories; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE kernel_theories (
    id integer DEFAULT nextval('kernel_theory_id_seq'::regclass) NOT NULL,
    name text,
    sort text,
    status text,
    img_url text,
    description text
);


ALTER TABLE public.kernel_theories OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: kernel_theory_set_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE kernel_theory_set_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.kernel_theory_set_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: kernel_theory_sets; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE kernel_theory_sets (
    id integer DEFAULT nextval('kernel_theory_set_id_seq'::regclass) NOT NULL,
    name text
);


ALTER TABLE public.kernel_theory_sets OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: keyword_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE keyword_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.keyword_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_beds; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE land_beds (
    id integer NOT NULL,
    soil_area_id integer,
    bed_num integer NOT NULL
);


ALTER TABLE public.land_beds OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_bed_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE land_bed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.land_bed_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_bed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER SEQUENCE land_bed_id_seq OWNED BY land_beds.bed_num;


--
-- Name: land_city_state_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE land_city_state_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.land_city_state_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_city_states; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE land_city_states (
    id integer DEFAULT nextval('land_city_state_id_seq'::regclass) NOT NULL,
    land_id integer,
    city text,
    state text
);


ALTER TABLE public.land_city_states OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_farmer_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE land_farmer_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.land_farmer_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_farmers; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE land_farmers (
    id integer DEFAULT nextval('land_farmer_id_seq'::regclass) NOT NULL,
    soil_area_id integer,
    farmer text
);


ALTER TABLE public.land_farmers OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE land_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.land_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_trait_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE land_trait_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.land_trait_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_traits; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE land_traits (
    id integer DEFAULT nextval('land_trait_id_seq'::regclass) NOT NULL,
    name text,
    value text,
    sort text,
    status text,
    land_id integer,
    reference text,
    img_url text,
    description text
);


ALTER TABLE public.land_traits OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_width_length_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE land_width_length_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.land_width_length_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: land_width_lengths; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE land_width_lengths (
    id integer DEFAULT nextval('land_width_length_id_seq'::regclass) NOT NULL,
    land_id integer,
    width text,
    length text,
    soil_area_id integer
);


ALTER TABLE public.land_width_lengths OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: lands; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE lands (
    id integer DEFAULT nextval('land_id_seq'::regclass) NOT NULL,
    name text,
    agricultural_type_id integer,
    sort text,
    description text,
    status text,
    img_url text
);


ALTER TABLE public.lands OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: linkmakers; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE linkmakers (
    id integer NOT NULL,
    name text,
    sort text,
    status text
);


ALTER TABLE public.linkmakers OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: location_workdates; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE location_workdates (
    class_primary_key_string integer NOT NULL,
    date text NOT NULL,
    class_name_string text NOT NULL
);


ALTER TABLE public.location_workdates OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: machine_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE machine_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 29;


ALTER TABLE public.machine_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: machines; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE machines (
    id integer DEFAULT nextval('machine_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    cpu text,
    filesystems text,
    sort text,
    status text,
    user_name text,
    ip_address text,
    img_url text
);


ALTER TABLE public.machines OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: maxonomy_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE maxonomy_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 18;


ALTER TABLE public.maxonomy_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: maxonomies; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE maxonomies (
    id integer DEFAULT nextval('maxonomy_id_seq'::regclass) NOT NULL,
    name text,
    img_url text,
    status text,
    user_name text,
    sort text,
    description text,
    categorization text,
    table_name text,
    how_to_measure text,
    ocm integer,
    order_by text
);


ALTER TABLE public.maxonomies OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: moneymaker_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE moneymaker_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.moneymaker_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: moneymakers; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE moneymakers (
    id integer DEFAULT nextval('moneymaker_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.moneymakers OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: object_image_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE object_image_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.object_image_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: object_images; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE object_images (
    id integer DEFAULT nextval('object_image_id_seq'::regclass) NOT NULL,
    image_id integer,
    class_primary_key_string text,
    class_name_string text
);


ALTER TABLE public.object_images OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: observation_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE observation_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.observation_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: observations; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE observations (
    id integer DEFAULT nextval('observation_id_seq'::regclass) NOT NULL,
    land_id integer,
    unit_id integer DEFAULT 21,
    ts text,
    notes text,
    design_instance_id integer,
    raw text,
    raw_read boolean DEFAULT false,
    measurement text
);


ALTER TABLE public.observations OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: payment_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE payment_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.payment_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: payments; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE payments (
    id integer DEFAULT nextval('payment_id_seq'::regclass) NOT NULL,
    date text,
    invoice_id integer,
    description text,
    amount text
);


ALTER TABLE public.payments OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: permaculture_topic_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE permaculture_topic_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.permaculture_topic_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: permaculture_topics; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE permaculture_topics (
    id integer DEFAULT nextval('permaculture_topic_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    order_by text,
    img_url text
);


ALTER TABLE public.permaculture_topics OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: pickup_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE pickup_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.pickup_detail_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: pickup_details; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE pickup_details (
    id integer DEFAULT nextval('pickup_detail_id_seq'::regclass) NOT NULL,
    date text,
    land_id integer
);


ALTER TABLE public.pickup_details OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: pickup_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE pickup_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.pickup_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: pickup_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE pickup_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.pickup_plant_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: pickup_plants; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE pickup_plants (
    id integer DEFAULT nextval('pickup_plant_id_seq'::regclass) NOT NULL,
    pickup_id integer,
    plant_id integer,
    unit_id integer,
    quantity real
);


ALTER TABLE public.pickup_plants OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: pickups; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE pickups (
    id integer DEFAULT nextval('pickup_id_seq'::regclass) NOT NULL,
    pickup_detail_id integer,
    invoice_line_id integer,
    delivery_stop_flag text
);


ALTER TABLE public.pickups OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_alias_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_alias_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_alias_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_aliases; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_aliases (
    id integer DEFAULT nextval('plant_alias_id_seq'::regclass) NOT NULL,
    plant_id integer,
    name text
);


ALTER TABLE public.plant_aliases OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_attribute_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_attributes; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_attributes (
    id integer DEFAULT nextval('plant_attribute_id_seq'::regclass) NOT NULL,
    plant_id integer,
    attribute_name text,
    attribute_value text
);


ALTER TABLE public.plant_attributes OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_category_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_category_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_category_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_categories; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_categories (
    id integer DEFAULT nextval('plant_category_id_seq'::regclass) NOT NULL,
    name text,
    description text
);


ALTER TABLE public.plant_categories OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_family_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_family_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_family_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_families; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_families (
    id integer DEFAULT nextval('plant_family_id_seq'::regclass) NOT NULL,
    name text
);


ALTER TABLE public.plant_families OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_history_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_history_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_history_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_histories; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_histories (
    id integer DEFAULT nextval('plant_history_id_seq'::regclass) NOT NULL,
    plant_list_plant_id integer,
    seed_packet_id integer
);


ALTER TABLE public.plant_histories OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_history_event_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_history_event_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_history_event_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_history_events; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_history_events (
    id integer DEFAULT nextval('plant_history_event_id_seq'::regclass) NOT NULL,
    plant_history_id integer,
    plant_count text,
    direct_seed boolean DEFAULT false
);


ALTER TABLE public.plant_history_events OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_list_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_list_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_list_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_list_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_list_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_list_plant_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_list_plants; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_list_plants (
    id integer DEFAULT nextval('plant_list_plant_id_seq'::regclass) NOT NULL,
    plant_list_id integer,
    plant_id integer
);


ALTER TABLE public.plant_list_plants OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_lists; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_lists (
    id integer DEFAULT nextval('plant_list_id_seq'::regclass) NOT NULL,
    name text,
    sort text,
    description text,
    status text,
    img_url text
);


ALTER TABLE public.plant_lists OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE plant_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.plant_unit_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plant_units; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plant_units (
    id integer DEFAULT nextval('plant_unit_id_seq'::regclass) NOT NULL,
    plant_id integer,
    unit_id integer
);


ALTER TABLE public.plant_units OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: plants; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE plants (
    id integer DEFAULT nextval('plant_id_seq'::regclass) NOT NULL,
    common_name text,
    scientific_name text,
    plant_category_id integer,
    plant_family_id integer
);


ALTER TABLE public.plants OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: posting_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE posting_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.posting_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: postings; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE postings (
    id integer DEFAULT nextval('posting_id_seq'::regclass) NOT NULL,
    account_id integer,
    journal_id integer,
    asset_type_id integer,
    amount text,
    budget_id integer,
    sort text,
    description text,
    status text,
    transaction_date text,
    transfer_account_id integer,
    supplier_id integer,
    due_date text,
    name text,
    img_url text
);


ALTER TABLE public.postings OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: potential_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE potential_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.potential_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: potentials; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE potentials (
    id integer DEFAULT nextval('potential_id_seq'::regclass) NOT NULL,
    name text,
    address text,
    homepage text,
    notes text,
    sort text,
    status text,
    webmaster_info text
);


ALTER TABLE public.potentials OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: price_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE price_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.price_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: prices; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE prices (
    id integer DEFAULT nextval('price_id_seq'::regclass) NOT NULL,
    plant_id integer,
    supplier_id integer,
    date character varying(10),
    dollars character varying(10),
    unit_count text,
    quality text,
    organic_flag character varying(10),
    unit_id integer
);


ALTER TABLE public.prices OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: process_flows; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE process_flows (
    parent_process_id integer NOT NULL,
    child_process_id integer NOT NULL,
    description text
);


ALTER TABLE public.process_flows OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: process_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE process_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.process_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: processes; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE processes (
    id integer DEFAULT nextval('process_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    business_plan_text_id integer NOT NULL,
    status text,
    priority text,
    img_url text,
    time_rules text,
    responsibility text,
    yield text
);


ALTER TABLE public.processes OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: products; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE products (
    id integer DEFAULT nextval('product_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.products OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: project_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE project_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.project_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: projects; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE projects (
    id integer DEFAULT nextval('project_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    user_name text,
    status text,
    img_url text,
    database_id integer
);


ALTER TABLE public.projects OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: reasons; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE reasons (
    id integer NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.reasons OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: scene_element_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE scene_element_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.scene_element_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: scene_elements; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE scene_elements (
    id integer DEFAULT nextval('scene_element_id_seq'::regclass) NOT NULL,
    process_id integer,
    sort text,
    database_string text,
    class_name_string text,
    class_primary_key_string text,
    description text,
    yield text,
    status text,
    img_url text,
    name text
);


ALTER TABLE public.scene_elements OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: search_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE search_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.search_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: searches; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE searches (
    id integer DEFAULT nextval('search_id_seq'::regclass) NOT NULL,
    name text,
    count integer
);


ALTER TABLE public.searches OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: seed_packet_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE seed_packet_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.seed_packet_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: seed_packets; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE seed_packets (
    id integer DEFAULT nextval('seed_packet_id_seq'::regclass) NOT NULL,
    variety_id integer,
    supplier_id integer DEFAULT 67,
    packed_for_year text DEFAULT '2015'::text,
    contents text DEFAULT 'in inventory'::text,
    sow_instructions text,
    days_to_maturity integer,
    days_to_germination text,
    notes text,
    product_details text,
    sort text DEFAULT 'Y 2015-03-23'::text,
    status text DEFAULT 'in progress'::text,
    name text,
    img_url text,
    description text
);


ALTER TABLE public.seed_packets OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: share_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE share_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.share_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: shares; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE shares (
    id integer DEFAULT nextval('share_id_seq'::regclass) NOT NULL,
    harvest_id integer,
    owner text,
    price text
);


ALTER TABLE public.shares OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: shift_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE shift_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.shift_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: shifts; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE shifts (
    id integer DEFAULT nextval('shift_id_seq'::regclass) NOT NULL,
    sort text,
    start text,
    timeout text,
    day_of_week integer
);


ALTER TABLE public.shifts OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: soil_area_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE soil_area_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.soil_area_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: soil_areas; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE soil_areas (
    land_id integer,
    name text,
    sort text,
    status text,
    description text,
    id integer DEFAULT nextval('soil_area_id_seq'::regclass) NOT NULL,
    img_url text
);


ALTER TABLE public.soil_areas OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: soil_test_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE soil_test_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.soil_test_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: soil_tests; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE soil_tests (
    id integer DEFAULT nextval('soil_test_id_seq'::regclass) NOT NULL,
    name text
);


ALTER TABLE public.soil_tests OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: song_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE song_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.song_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: songs; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE songs (
    id integer DEFAULT nextval('song_id_seq'::regclass) NOT NULL,
    name text,
    sort text,
    lyrics_url text,
    mp3_url text,
    album_id integer,
    cover_flag boolean DEFAULT false,
    album_sort text,
    chords text,
    lyrics text,
    description text
);


ALTER TABLE public.songs OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: spacing_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE spacing_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.spacing_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: spacings; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE spacings (
    id integer DEFAULT nextval('spacing_id_seq'::regclass) NOT NULL,
    plant_id integer,
    rows_per_bed text,
    inches_between_plants text,
    source text
);


ALTER TABLE public.spacings OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: stakeholder_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE stakeholder_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.stakeholder_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: stakeholders; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE stakeholders (
    id integer DEFAULT nextval('stakeholder_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.stakeholders OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: storage_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE storage_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.storage_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: storages; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE storages (
    id integer DEFAULT nextval('storage_id_seq'::regclass) NOT NULL,
    plant_id integer,
    instructions text,
    source text
);


ALTER TABLE public.storages OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: style_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE style_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.style_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: styles; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE styles (
    id integer DEFAULT nextval('style_id_seq'::regclass) NOT NULL,
    number character varying(5),
    description text,
    url text,
    season character varying(4),
    color_id integer,
    puppy boolean,
    owner text
);


ALTER TABLE public.styles OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.supplier_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: suppliers; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE suppliers (
    id integer DEFAULT nextval('supplier_id_seq'::regclass) NOT NULL,
    name text,
    city text,
    state text,
    url text,
    bioregion text,
    sort text,
    status text,
    description text,
    user_name text,
    img_url text,
    last_password_change text
);


ALTER TABLE public.suppliers OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tag_elements; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE tag_elements (
    id integer NOT NULL,
    tag_id integer,
    database_string text,
    class_name_string text,
    class_primary_key_string text
);


ALTER TABLE public.tag_elements OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tags; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE tags (
    id integer DEFAULT nextval('tag_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text,
    permaculture_topic_id integer
);


ALTER TABLE public.tags OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tenperday_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE tenperday_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tenperday_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tenperdays; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE tenperdays (
    date text NOT NULL,
    webpages_blogmastered text,
    id integer DEFAULT nextval('tenperday_id_seq'::regclass) NOT NULL,
    view text
);


ALTER TABLE public.tenperdays OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.ticket_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tickets; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE tickets (
    id integer DEFAULT nextval('ticket_id_seq'::regclass) NOT NULL,
    name text,
    sort text,
    status text DEFAULT 'open'::text,
    process_id integer NOT NULL,
    description text,
    img_url text,
    action_to_take text
);


ALTER TABLE public.tickets OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: timecard_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE timecard_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.timecard_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: timecards; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE timecards (
    id integer DEFAULT nextval('timecard_id_seq'::regclass) NOT NULL,
    doer_user_name text,
    date text,
    time_in text,
    time_out text,
    description text,
    project_id integer,
    process_id integer,
    shift_id integer,
    scene_element_id integer
);


ALTER TABLE public.timecards OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tool_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE tool_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.tool_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: tools; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE tools (
    id integer DEFAULT nextval('tool_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    img_url text,
    sort text,
    status text
);


ALTER TABLE public.tools OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: unit_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE unit_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.unit_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: units; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE units (
    id integer DEFAULT nextval('unit_id_seq'::regclass) NOT NULL,
    name text,
    description text
);


ALTER TABLE public.units OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: usernames; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE usernames (
    username character varying(12) NOT NULL,
    email_address_id integer,
    coded_cookie_word text,
    password character(100) NOT NULL
);


ALTER TABLE public.usernames OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: variety_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE variety_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.variety_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: varieties; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE varieties (
    id integer DEFAULT nextval('variety_id_seq'::regclass) NOT NULL,
    plant_id integer,
    name text,
    description text
);


ALTER TABLE public.varieties OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: visit_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE visit_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.visit_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: visits; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE visits (
    id integer DEFAULT nextval('visit_id_seq'::regclass) NOT NULL,
    rough_date text,
    fact boolean,
    land_id integer,
    name text,
    plant_history_event_id integer,
    "order" text,
    count integer,
    layout integer
);


ALTER TABLE public.visits OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webmaster_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE webmaster_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.webmaster_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webmasters; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE webmasters (
    id integer DEFAULT nextval('webmaster_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text,
    user_name text
);


ALTER TABLE public.webmasters OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webpage_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE webpage_id_seq
    START WITH 4510
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.webpage_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webpage_maxonomies; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE webpage_maxonomies (
    webpage_id integer,
    maxonomy_id integer
);


ALTER TABLE public.webpage_maxonomies OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webpage_moneymaker_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE webpage_moneymaker_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.webpage_moneymaker_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webpage_moneymakers; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE webpage_moneymakers (
    id integer DEFAULT nextval('webpage_moneymaker_id_seq'::regclass) NOT NULL,
    webpage_id integer,
    moneymaker_id integer
);


ALTER TABLE public.webpage_moneymakers OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webpage_tags; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE webpage_tags (
    webpage_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.webpage_tags OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: webpages; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE webpages (
    id integer DEFAULT nextval('webpage_id_seq'::regclass) NOT NULL,
    domain_tli character varying(3),
    description text,
    name text,
    sort text,
    img_url text,
    status text,
    path text,
    parameters text
);


ALTER TABLE public.webpages OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: yield_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE yield_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 9
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.yield_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: yields; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE yields (
    id integer DEFAULT nextval('yield_id_seq'::regclass) NOT NULL,
    plant_id integer,
    estimated_yield text,
    numerator_unit_id integer,
    source text,
    denominator_unit_id integer,
    range text
);


ALTER TABLE public.yields OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: zachman_id_seq; Type: SEQUENCE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

CREATE SEQUENCE zachman_id_seq
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 99999999
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.zachman_id_seq OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: zachmans; Type: TABLE; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

CREATE TABLE zachmans (
    id integer DEFAULT nextval('zachman_id_seq'::regclass) NOT NULL,
    name text,
    description text
);


ALTER TABLE public.zachmans OWNER TO [YOUR_TABLE_OWNER_HERE];

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY indiegoals ALTER COLUMN id SET DEFAULT nextval('indiegoal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY land_beds ALTER COLUMN id SET DEFAULT nextval('land_bed_id_seq'::regclass);


--
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: agricultural_types_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY agricultural_types
    ADD CONSTRAINT agricultural_types_pkey PRIMARY KEY (id);


--
-- Name: albums_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY albums
    ADD CONSTRAINT albums_pkey PRIMARY KEY (id);


--
-- Name: applications_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY applications
    ADD CONSTRAINT applications_pkey PRIMARY KEY (id);


--
-- Name: asset_types_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY asset_types
    ADD CONSTRAINT asset_types_pkey PRIMARY KEY (id);


--
-- Name: blog_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY blogposts
    ADD CONSTRAINT blog_posts_pkey PRIMARY KEY (id);


--
-- Name: book_clips_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT book_clips_pkey PRIMARY KEY (id);


--
-- Name: books_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: budgets_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY budgets
    ADD CONSTRAINT budgets_pkey PRIMARY KEY (id);


--
-- Name: builds_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_pkey PRIMARY KEY (id);


--
-- Name: business_plan_texts_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY business_plan_texts
    ADD CONSTRAINT business_plan_texts_pkey PRIMARY KEY (id);


--
-- Name: calendars_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY calendars
    ADD CONSTRAINT calendars_pkey PRIMARY KEY (id);


--
-- Name: classes_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY classes
    ADD CONSTRAINT classes_pkey PRIMARY KEY (id);


--
-- Name: classes_unique_name; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY classes
    ADD CONSTRAINT classes_unique_name UNIQUE (name);


--
-- Name: colors_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY colors
    ADD CONSTRAINT colors_pkey PRIMARY KEY (id);


--
-- Name: customers_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: databases_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY databases
    ADD CONSTRAINT databases_pkey PRIMARY KEY (id);


--
-- Name: design_instances_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY design_instances
    ADD CONSTRAINT design_instances_pkey PRIMARY KEY (id);


--
-- Name: design_order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY design_order_items
    ADD CONSTRAINT design_order_items_pkey PRIMARY KEY (id);


--
-- Name: design_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY design_orders
    ADD CONSTRAINT design_orders_pkey PRIMARY KEY (id);


--
-- Name: designers_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY designers
    ADD CONSTRAINT designers_pkey PRIMARY KEY (id);


--
-- Name: designs_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY designs
    ADD CONSTRAINT designs_pkey PRIMARY KEY (id);


--
-- Name: documentations_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY documentations
    ADD CONSTRAINT documentations_pkey PRIMARY KEY (id);


--
-- Name: domain_machine_databases_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT domain_machine_databases_pkey PRIMARY KEY (domain_tli, machine_id, database_id);


--
-- Name: domain_measurements_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY domain_measurements
    ADD CONSTRAINT domain_measurements_pkey PRIMARY KEY (id);


--
-- Name: domains_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY domains
    ADD CONSTRAINT domains_pkey PRIMARY KEY (tli);


--
-- Name: email_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY email_addresses
    ADD CONSTRAINT email_addresses_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: field_tests_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY field_tests
    ADD CONSTRAINT field_tests_pkey PRIMARY KEY (id);


--
-- Name: goal_statements_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY goal_statements
    ADD CONSTRAINT goal_statements_pkey PRIMARY KEY (id);


--
-- Name: guest_passes_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY guest_passes
    ADD CONSTRAINT guest_passes_pkey PRIMARY KEY (id);


--
-- Name: harvests_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY harvests
    ADD CONSTRAINT harvests_pkey PRIMARY KEY (id);


--
-- Name: host_applications_compound_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_compound_pkey PRIMARY KEY (domain_tli, application_id, machine_id);


--
-- Name: host_email_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_email_addresses_pkey PRIMARY KEY (domain_tli, machine_id, email_address_id);


--
-- Name: hosts_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY hosts
    ADD CONSTRAINT hosts_pkey PRIMARY KEY (parent_class_name_string, parent_class_primary_key_string, child_class_name_string, child_class_primary_key_string);


--
-- Name: hyperlink_reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY hyperlink_reasons
    ADD CONSTRAINT hyperlink_reasons_pkey PRIMARY KEY (id);


--
-- Name: hyperlinks_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY hyperlinks
    ADD CONSTRAINT hyperlinks_pkey PRIMARY KEY (id);


--
-- Name: images_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: indiegoals_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY indiegoals
    ADD CONSTRAINT indiegoals_pkey PRIMARY KEY (id);


--
-- Name: invoice_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY invoice_lines
    ADD CONSTRAINT invoice_lines_pkey PRIMARY KEY (id);


--
-- Name: invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY invoices
    ADD CONSTRAINT invoices_pkey PRIMARY KEY (id);


--
-- Name: journals_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY journals
    ADD CONSTRAINT journals_pkey PRIMARY KEY (id);


--
-- Name: kernel_theories_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY kernel_theories
    ADD CONSTRAINT kernel_theories_pkey PRIMARY KEY (id);


--
-- Name: kernel_theory_sets_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY kernel_theory_sets
    ADD CONSTRAINT kernel_theory_sets_pkey PRIMARY KEY (id);


--
-- Name: land_beds_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY land_beds
    ADD CONSTRAINT land_beds_pkey PRIMARY KEY (id);


--
-- Name: land_city_states_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY land_city_states
    ADD CONSTRAINT land_city_states_pkey PRIMARY KEY (id);


--
-- Name: land_farmers_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY land_farmers
    ADD CONSTRAINT land_farmers_pkey PRIMARY KEY (id);


--
-- Name: land_traits_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY land_traits
    ADD CONSTRAINT land_traits_pkey PRIMARY KEY (id);


--
-- Name: land_width_lengths_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY land_width_lengths
    ADD CONSTRAINT land_width_lengths_pkey PRIMARY KEY (id);


--
-- Name: lands_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY lands
    ADD CONSTRAINT lands_pkey PRIMARY KEY (id);


--
-- Name: linkmasters_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY linkmakers
    ADD CONSTRAINT linkmasters_pkey PRIMARY KEY (id);


--
-- Name: location_workdates_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY location_workdates
    ADD CONSTRAINT location_workdates_pkey PRIMARY KEY (class_name_string, class_primary_key_string, date);


--
-- Name: machines_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY machines
    ADD CONSTRAINT machines_pkey PRIMARY KEY (id);


--
-- Name: maxonomies_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY maxonomies
    ADD CONSTRAINT maxonomies_pkey PRIMARY KEY (id);


--
-- Name: moneymakers_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY moneymakers
    ADD CONSTRAINT moneymakers_pkey PRIMARY KEY (id);


--
-- Name: object_images_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY object_images
    ADD CONSTRAINT object_images_pkey PRIMARY KEY (id);


--
-- Name: observations_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY observations
    ADD CONSTRAINT observations_pkey PRIMARY KEY (id);


--
-- Name: payments_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: permaculture_topics_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY permaculture_topics
    ADD CONSTRAINT permaculture_topics_pkey PRIMARY KEY (id);


--
-- Name: pickup_details_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY pickup_details
    ADD CONSTRAINT pickup_details_pkey PRIMARY KEY (id);


--
-- Name: pickup_plants_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY pickup_plants
    ADD CONSTRAINT pickup_plants_pkey PRIMARY KEY (id);


--
-- Name: pickups_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY pickups
    ADD CONSTRAINT pickups_pkey PRIMARY KEY (id);


--
-- Name: plant_aliases_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_aliases
    ADD CONSTRAINT plant_aliases_pkey PRIMARY KEY (id);


--
-- Name: plant_attributes_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_attributes
    ADD CONSTRAINT plant_attributes_pkey PRIMARY KEY (id);


--
-- Name: plant_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_categories
    ADD CONSTRAINT plant_categories_pkey PRIMARY KEY (id);


--
-- Name: plant_families_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_families
    ADD CONSTRAINT plant_families_pkey PRIMARY KEY (id);


--
-- Name: plant_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_histories
    ADD CONSTRAINT plant_histories_pkey PRIMARY KEY (id);


--
-- Name: plant_history_events_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_history_events
    ADD CONSTRAINT plant_history_events_pkey PRIMARY KEY (id);


--
-- Name: plant_list_plants_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_list_plants
    ADD CONSTRAINT plant_list_plants_pkey PRIMARY KEY (id);


--
-- Name: plant_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_lists
    ADD CONSTRAINT plant_lists_pkey PRIMARY KEY (id);


--
-- Name: plant_units_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plant_units
    ADD CONSTRAINT plant_units_pkey PRIMARY KEY (id);


--
-- Name: plants_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY plants
    ADD CONSTRAINT plants_pkey PRIMARY KEY (id);


--
-- Name: postings_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_pkey PRIMARY KEY (id);


--
-- Name: potentials_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY potentials
    ADD CONSTRAINT potentials_pkey PRIMARY KEY (id);


--
-- Name: prices_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_pkey PRIMARY KEY (id);


--
-- Name: process_flows_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY process_flows
    ADD CONSTRAINT process_flows_pkey PRIMARY KEY (parent_process_id, child_process_id);


--
-- Name: processes_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY processes
    ADD CONSTRAINT processes_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: reasons_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY reasons
    ADD CONSTRAINT reasons_pkey PRIMARY KEY (id);


--
-- Name: scene_elements_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY scene_elements
    ADD CONSTRAINT scene_elements_pkey PRIMARY KEY (id);


--
-- Name: seaches_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY searches
    ADD CONSTRAINT seaches_pkey PRIMARY KEY (id);


--
-- Name: seed_packets_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY seed_packets
    ADD CONSTRAINT seed_packets_pkey PRIMARY KEY (id);


--
-- Name: shares_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY shares
    ADD CONSTRAINT shares_pkey PRIMARY KEY (id);


--
-- Name: shifts_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY shifts
    ADD CONSTRAINT shifts_pkey PRIMARY KEY (id);


--
-- Name: soil_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY soil_areas
    ADD CONSTRAINT soil_areas_pkey PRIMARY KEY (id);


--
-- Name: soil_tests_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY soil_tests
    ADD CONSTRAINT soil_tests_pkey PRIMARY KEY (id);


--
-- Name: songs_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY songs
    ADD CONSTRAINT songs_pkey PRIMARY KEY (id);


--
-- Name: spacings_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY spacings
    ADD CONSTRAINT spacings_pkey PRIMARY KEY (id);


--
-- Name: stakeholders_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY stakeholders
    ADD CONSTRAINT stakeholders_pkey PRIMARY KEY (id);


--
-- Name: storages_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY storages
    ADD CONSTRAINT storages_pkey PRIMARY KEY (id);


--
-- Name: styles_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY styles
    ADD CONSTRAINT styles_pkey PRIMARY KEY (id);


--
-- Name: suppliers_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY suppliers
    ADD CONSTRAINT suppliers_pkey PRIMARY KEY (id);


--
-- Name: tag_elements_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY tag_elements
    ADD CONSTRAINT tag_elements_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: tenperdays_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY tenperdays
    ADD CONSTRAINT tenperdays_pkey PRIMARY KEY (id);


--
-- Name: tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: timecards_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY timecards
    ADD CONSTRAINT timecards_pkey PRIMARY KEY (id);


--
-- Name: tools_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY tools
    ADD CONSTRAINT tools_pkey PRIMARY KEY (id);


--
-- Name: units_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: usernames_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY usernames
    ADD CONSTRAINT usernames_pkey PRIMARY KEY (username);


--
-- Name: varieties_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY varieties
    ADD CONSTRAINT varieties_pkey PRIMARY KEY (id);


--
-- Name: visits_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT visits_pkey PRIMARY KEY (id);


--
-- Name: webmasters_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY webmasters
    ADD CONSTRAINT webmasters_pkey PRIMARY KEY (id);


--
-- Name: webpage_moneymakers_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY webpage_moneymakers
    ADD CONSTRAINT webpage_moneymakers_pkey PRIMARY KEY (id);


--
-- Name: webpage_tags_key; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY webpage_tags
    ADD CONSTRAINT webpage_tags_key PRIMARY KEY (webpage_id, tag_id);


--
-- Name: webpages_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY webpages
    ADD CONSTRAINT webpages_pkey PRIMARY KEY (id);


--
-- Name: yields_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT yields_pkey PRIMARY KEY (id);


--
-- Name: zachmans_pkey; Type: CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]; Tablespace: 
--

ALTER TABLE ONLY zachmans
    ADD CONSTRAINT zachmans_pkey PRIMARY KEY (id);


--
-- Name: blog_posts_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY blogposts
    ADD CONSTRAINT blog_posts_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id);


--
-- Name: book_clips_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT book_clips_book_id_fkey FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: book_clips_image_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT book_clips_image_id_fkey FOREIGN KEY (image_id) REFERENCES images(id);


--
-- Name: book_clips_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT book_clips_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: budget_accounts_accounts; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY budget_accounts
    ADD CONSTRAINT budget_accounts_accounts FOREIGN KEY (account_id) REFERENCES accounts(id);


--
-- Name: budget_accounts_budgets; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY budget_accounts
    ADD CONSTRAINT budget_accounts_budgets FOREIGN KEY (budget_id) REFERENCES budgets(id);


--
-- Name: budgets_scene_element_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY budgets
    ADD CONSTRAINT budgets_scene_element_id_fkey FOREIGN KEY (scene_element_id) REFERENCES scene_elements(id);


--
-- Name: builds_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: business_plan_texts_goal_statement_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY business_plan_texts
    ADD CONSTRAINT business_plan_texts_goal_statement_id_fkey FOREIGN KEY (goal_statement_id) REFERENCES goal_statements(id);


--
-- Name: classes_extends_class_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY classes
    ADD CONSTRAINT classes_extends_class_id_fkey FOREIGN KEY (extends_class_id) REFERENCES classes(id);


--
-- Name: classes_zachman_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY classes
    ADD CONSTRAINT classes_zachman_id_fkey FOREIGN KEY (zachman_id) REFERENCES zachmans(id);


--
-- Name: color_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY styles
    ADD CONSTRAINT color_id_fkey FOREIGN KEY (color_id) REFERENCES colors(id);


--
-- Name: customers_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: design_instances_design_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY design_instances
    ADD CONSTRAINT design_instances_design_id_fkey FOREIGN KEY (design_id) REFERENCES designs(id);


--
-- Name: design_instances_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY design_instances
    ADD CONSTRAINT design_instances_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: design_order_items_design_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY design_order_items
    ADD CONSTRAINT design_order_items_design_instance_id_fkey FOREIGN KEY (design_instance_id) REFERENCES design_instances(id);


--
-- Name: design_order_items_design_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY design_order_items
    ADD CONSTRAINT design_order_items_design_order_id_fkey FOREIGN KEY (design_order_id) REFERENCES design_orders(id);


--
-- Name: design_orders_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY design_orders
    ADD CONSTRAINT design_orders_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: designs_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY designs
    ADD CONSTRAINT designs_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: domains_design_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY domains
    ADD CONSTRAINT domains_design_id_fkey FOREIGN KEY (design_id) REFERENCES designs(id);


--
-- Name: email_addresses_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY email_addresses
    ADD CONSTRAINT email_addresses_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: goal_statements_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY goal_statements
    ADD CONSTRAINT goal_statements_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: harvests_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY harvests
    ADD CONSTRAINT harvests_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: host_applications_application_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_application_id_fkey FOREIGN KEY (application_id) REFERENCES applications(id);


--
-- Name: host_applications_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: host_applications_machine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_machine_id_fkey FOREIGN KEY (machine_id) REFERENCES machines(id);


--
-- Name: host_databases_database_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT host_databases_database_id_fkey FOREIGN KEY (database_id) REFERENCES databases(id);


--
-- Name: host_databases_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT host_databases_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: host_databases_machine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT host_databases_machine_id_fkey FOREIGN KEY (machine_id) REFERENCES machines(id);


--
-- Name: host_email_addresses_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_email_addresses_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: host_email_addresses_email_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_email_addresses_email_address_id_fkey FOREIGN KEY (email_address_id) REFERENCES email_addresses(id);


--
-- Name: host_email_addresses_machine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_email_addresses_machine_id_fkey FOREIGN KEY (machine_id) REFERENCES machines(id);


--
-- Name: hyperlink_reasons_hyperlink_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY hyperlink_reasons
    ADD CONSTRAINT hyperlink_reasons_hyperlink_fkey FOREIGN KEY (hyperlink_id) REFERENCES hyperlinks(id);


--
-- Name: hyperlink_reasons_reason_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY hyperlink_reasons
    ADD CONSTRAINT hyperlink_reasons_reason_fkey FOREIGN KEY (reason_id) REFERENCES reasons(id);


--
-- Name: invoice_lines_invoice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY invoice_lines
    ADD CONSTRAINT invoice_lines_invoice_id_fkey FOREIGN KEY (invoice_id) REFERENCES invoices(id);


--
-- Name: invoice_lines_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY invoice_lines
    ADD CONSTRAINT invoice_lines_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: invoices_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY invoices
    ADD CONSTRAINT invoices_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customers(id);


--
-- Name: land_beds_soil_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY land_beds
    ADD CONSTRAINT land_beds_soil_area_id_fkey FOREIGN KEY (soil_area_id) REFERENCES soil_areas(id);


--
-- Name: land_city_states_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY land_city_states
    ADD CONSTRAINT land_city_states_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: land_farmers_soil_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY land_farmers
    ADD CONSTRAINT land_farmers_soil_area_id_fkey FOREIGN KEY (soil_area_id) REFERENCES soil_areas(id);


--
-- Name: land_traits_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY land_traits
    ADD CONSTRAINT land_traits_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: land_width_lengths_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY land_width_lengths
    ADD CONSTRAINT land_width_lengths_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: land_width_lengths_soil_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY land_width_lengths
    ADD CONSTRAINT land_width_lengths_soil_area_id_fkey FOREIGN KEY (soil_area_id) REFERENCES soil_areas(id);


--
-- Name: lands_agricultural_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY lands
    ADD CONSTRAINT lands_agricultural_type_id_fkey FOREIGN KEY (agricultural_type_id) REFERENCES agricultural_types(id);


--
-- Name: object_images_image_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY object_images
    ADD CONSTRAINT object_images_image_id_fkey FOREIGN KEY (image_id) REFERENCES images(id);


--
-- Name: observations_design_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY observations
    ADD CONSTRAINT observations_design_instance_id_fkey FOREIGN KEY (design_instance_id) REFERENCES design_instances(id);


--
-- Name: observations_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY observations
    ADD CONSTRAINT observations_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: observations_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY observations
    ADD CONSTRAINT observations_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: payments_invoice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_invoice_id_fkey FOREIGN KEY (invoice_id) REFERENCES invoices(id);


--
-- Name: pickup_details_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY pickup_details
    ADD CONSTRAINT pickup_details_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: pickup_plants_pickup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY pickup_plants
    ADD CONSTRAINT pickup_plants_pickup_id_fkey FOREIGN KEY (pickup_id) REFERENCES pickups(id);


--
-- Name: pickup_plants_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY pickup_plants
    ADD CONSTRAINT pickup_plants_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: pickup_plants_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY pickup_plants
    ADD CONSTRAINT pickup_plants_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: pickups_invoice_line_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY pickups
    ADD CONSTRAINT pickups_invoice_line_id_fkey FOREIGN KEY (invoice_line_id) REFERENCES invoice_lines(id);


--
-- Name: pickups_pickup_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY pickups
    ADD CONSTRAINT pickups_pickup_detail_id_fkey FOREIGN KEY (pickup_detail_id) REFERENCES pickup_details(id);


--
-- Name: plant_aliases_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_aliases
    ADD CONSTRAINT plant_aliases_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: plant_histories_plant_list_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_histories
    ADD CONSTRAINT plant_histories_plant_list_plant_id_fkey FOREIGN KEY (plant_list_plant_id) REFERENCES plant_list_plants(id);


--
-- Name: plant_histories_seed_packet_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_histories
    ADD CONSTRAINT plant_histories_seed_packet_id_fkey FOREIGN KEY (seed_packet_id) REFERENCES seed_packets(id);


--
-- Name: plant_history_events_plant_history_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_history_events
    ADD CONSTRAINT plant_history_events_plant_history_id_fkey FOREIGN KEY (plant_history_id) REFERENCES plant_histories(id);


--
-- Name: plant_list_plants_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_list_plants
    ADD CONSTRAINT plant_list_plants_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: plant_list_plants_plant_list_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_list_plants
    ADD CONSTRAINT plant_list_plants_plant_list_id_fkey FOREIGN KEY (plant_list_id) REFERENCES plant_lists(id);


--
-- Name: plant_units_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_units
    ADD CONSTRAINT plant_units_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: plant_units_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plant_units
    ADD CONSTRAINT plant_units_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: plants_plant_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plants
    ADD CONSTRAINT plants_plant_category_id_fkey FOREIGN KEY (plant_category_id) REFERENCES plant_categories(id);


--
-- Name: plants_plant_family_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY plants
    ADD CONSTRAINT plants_plant_family_id_fkey FOREIGN KEY (plant_family_id) REFERENCES plant_families(id);


--
-- Name: postings_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_account_id_fkey FOREIGN KEY (account_id) REFERENCES accounts(id);


--
-- Name: postings_asset_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_asset_type_id_fkey FOREIGN KEY (asset_type_id) REFERENCES asset_types(id);


--
-- Name: postings_budget_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_budget_id_fkey FOREIGN KEY (budget_id) REFERENCES budgets(id);


--
-- Name: postings_journal_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_journal_id_fkey FOREIGN KEY (journal_id) REFERENCES journals(id);


--
-- Name: postings_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: postings_transfer_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_transfer_account_id_fkey FOREIGN KEY (transfer_account_id) REFERENCES accounts(id);


--
-- Name: prices_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: prices_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: prices_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: process_flows_child_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY process_flows
    ADD CONSTRAINT process_flows_child_process_id_fkey FOREIGN KEY (child_process_id) REFERENCES processes(id);


--
-- Name: process_flows_parent_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY process_flows
    ADD CONSTRAINT process_flows_parent_process_id_fkey FOREIGN KEY (parent_process_id) REFERENCES processes(id);


--
-- Name: processes_business_plan_text_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY processes
    ADD CONSTRAINT processes_business_plan_text_id_fkey FOREIGN KEY (business_plan_text_id) REFERENCES business_plan_texts(id);


--
-- Name: scene_elements_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY scene_elements
    ADD CONSTRAINT scene_elements_process_id_fkey FOREIGN KEY (process_id) REFERENCES processes(id);


--
-- Name: seed_packets_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY seed_packets
    ADD CONSTRAINT seed_packets_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: seed_packets_variety_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY seed_packets
    ADD CONSTRAINT seed_packets_variety_id_fkey FOREIGN KEY (variety_id) REFERENCES varieties(id);


--
-- Name: shares_harvest_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY shares
    ADD CONSTRAINT shares_harvest_id_fkey FOREIGN KEY (harvest_id) REFERENCES harvests(id);


--
-- Name: soil_areas_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY soil_areas
    ADD CONSTRAINT soil_areas_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: songs_album_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY songs
    ADD CONSTRAINT songs_album_id_fkey FOREIGN KEY (album_id) REFERENCES albums(id);


--
-- Name: spacings_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY spacings
    ADD CONSTRAINT spacings_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: storages_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY storages
    ADD CONSTRAINT storages_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: tickets_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_process_id_fkey FOREIGN KEY (process_id) REFERENCES processes(id);


--
-- Name: timecards_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY timecards
    ADD CONSTRAINT timecards_process_id_fkey FOREIGN KEY (process_id) REFERENCES processes(id);


--
-- Name: timecards_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY timecards
    ADD CONSTRAINT timecards_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: timecards_scene_element_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY timecards
    ADD CONSTRAINT timecards_scene_element_id_fkey FOREIGN KEY (scene_element_id) REFERENCES scene_elements(id);


--
-- Name: timecards_shift_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY timecards
    ADD CONSTRAINT timecards_shift_id_fkey FOREIGN KEY (shift_id) REFERENCES shifts(id);


--
-- Name: usernames_email_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY usernames
    ADD CONSTRAINT usernames_email_address_id_fkey FOREIGN KEY (email_address_id) REFERENCES email_addresses(id);


--
-- Name: varieties_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY varieties
    ADD CONSTRAINT varieties_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: visits_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT visits_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: visits_plant_history_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT visits_plant_history_event_id_fkey FOREIGN KEY (plant_history_event_id) REFERENCES plant_history_events(id);


--
-- Name: webpage_maxonomies_maxonomy_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY webpage_maxonomies
    ADD CONSTRAINT webpage_maxonomies_maxonomy_id_fkey FOREIGN KEY (maxonomy_id) REFERENCES maxonomies(id);


--
-- Name: webpage_maxonomies_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY webpage_maxonomies
    ADD CONSTRAINT webpage_maxonomies_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id) ON DELETE CASCADE;


--
-- Name: webpage_moneymakers_moneymaker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY webpage_moneymakers
    ADD CONSTRAINT webpage_moneymakers_moneymaker_id_fkey FOREIGN KEY (moneymaker_id) REFERENCES moneymakers(id);


--
-- Name: webpage_moneymakers_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY webpage_moneymakers
    ADD CONSTRAINT webpage_moneymakers_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id);


--
-- Name: webpage_tags_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY webpage_tags
    ADD CONSTRAINT webpage_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: webpage_tags_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY webpage_tags
    ADD CONSTRAINT webpage_tags_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id);


--
-- Name: webpages_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY webpages
    ADD CONSTRAINT webpages_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: yields_denominator_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT yields_denominator_unit_id_fkey FOREIGN KEY (denominator_unit_id) REFERENCES units(id);


--
-- Name: yields_numerator_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT yields_numerator_unit_id_fkey FOREIGN KEY (numerator_unit_id) REFERENCES units(id);


--
-- Name: yields_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: [YOUR_TABLE_OWNER_HERE]
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT yields_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


