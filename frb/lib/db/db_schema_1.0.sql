
--
-- FreeRadiantBunny
-- http://freeradiantbunny.org
--
-- Copyright 2014 Lars Paul Linden
--
--
-- PostgreSQL database dump version 1.0
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: histogram; Type: TYPE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE TYPE histogram AS (
	min double precision,
	max double precision,
	count bigint,
	percent double precision
);


ALTER TYPE public.histogram OWNER TO YOUR_USERNAME_HERE;

--
-- Name: quantile; Type: TYPE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE TYPE quantile AS (
	quantile double precision,
	value double precision
);


ALTER TYPE public.quantile OWNER TO YOUR_USERNAME_HERE;

--
-- Name: reclassarg; Type: TYPE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE TYPE reclassarg AS (
	nband integer,
	reclassexpr text,
	pixeltype text,
	nodataval double precision
);


ALTER TYPE public.reclassarg OWNER TO YOUR_USERNAME_HERE;

--
-- Name: valuecount; Type: TYPE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE TYPE valuecount AS (
	value double precision,
	count integer,
	percent double precision
);


ALTER TYPE public.valuecount OWNER TO YOUR_USERNAME_HERE;

--
-- Name: accounts_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE accounts_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.accounts_id_sequence OWNER TO YOUR_USERNAME_HERE;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE accounts (
    id integer DEFAULT nextval('accounts_id_sequence'::regclass) NOT NULL,
    name text,
    description text,
    class_name_string text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.accounts OWNER TO YOUR_USERNAME_HERE;

--
-- Name: agricultural_types; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE agricultural_types (
    id integer NOT NULL,
    name text,
    description text,
    img_url text
);


ALTER TABLE public.agricultural_types OWNER TO YOUR_USERNAME_HERE;

--
-- Name: album_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE album_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.album_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: albums; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE albums (
    id integer DEFAULT nextval('album_id_seq'::regclass) NOT NULL,
    name text,
    year character varying(4),
    cover_front_url text,
    cover_back_url text,
    notes text,
    album_url text
);


ALTER TABLE public.albums OWNER TO YOUR_USERNAME_HERE;

--
-- Name: application_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE application_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.application_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: application_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE application_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.application_instance_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: applications; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE applications (
    id integer DEFAULT nextval('application_id_sequence'::regclass) NOT NULL,
    name text,
    url text,
    source_code_url text,
    development_url text,
    sort text DEFAULT '?'::text,
    img_url text,
    description text,
    status text
);


ALTER TABLE public.applications OWNER TO YOUR_USERNAME_HERE;

--
-- Name: asset_types; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE asset_types (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.asset_types OWNER TO YOUR_USERNAME_HERE;

--
-- Name: beds_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE beds_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.beds_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: blogpost_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE blogpost_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.blogpost_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: blog_posts; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE blog_posts (
    id integer DEFAULT nextval('blogpost_id_seq'::regclass) NOT NULL,
    name text,
    body text,
    images text,
    tags text,
    url_alias text,
    author text,
    pubdate text,
    webpage_id integer
);


ALTER TABLE public.blog_posts OWNER TO YOUR_USERNAME_HERE;

--
-- Name: book_clips; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE book_clips (
    id integer NOT NULL,
    image_id integer,
    plant_id integer,
    excerpt text,
    see_also text,
    book_id integer
);


ALTER TABLE public.book_clips OWNER TO YOUR_USERNAME_HERE;

--
-- Name: book_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.book_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: bookclips_excerpt_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE bookclips_excerpt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bookclips_excerpt_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: bookclips_excerpt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE bookclips_excerpt_seq OWNED BY book_clips.excerpt;


--
-- Name: bookclips_image_url_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE bookclips_image_url_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bookclips_image_url_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: bookclips_image_url_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE bookclips_image_url_seq OWNED BY book_clips.image_id;


--
-- Name: bookclips_plant_common_name_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE bookclips_plant_common_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bookclips_plant_common_name_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: bookclips_plant_common_name_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE bookclips_plant_common_name_seq OWNED BY book_clips.plant_id;


--
-- Name: books; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE books (
    id integer DEFAULT nextval('book_id_seq'::regclass) NOT NULL,
    name text,
    url text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.books OWNER TO YOUR_USERNAME_HERE;

--
-- Name: budget_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE budget_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.budget_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: budgets; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE budgets (
    id integer DEFAULT nextval('budget_id_sequence'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    scene_element_id integer NOT NULL,
    img_url text
);


ALTER TABLE public.budgets OWNER TO YOUR_USERNAME_HERE;

--
-- Name: build_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE build_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.build_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: builds; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE builds (
    id integer DEFAULT nextval('build_id_seq'::regclass) NOT NULL,
    project_id integer,
    status text,
    description text,
    sort text,
    name text,
    img_url text
);


ALTER TABLE public.builds OWNER TO YOUR_USERNAME_HERE;

--
-- Name: business_plan_texts_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE business_plan_texts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.business_plan_texts_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: business_plan_texts; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE business_plan_texts (
    id integer DEFAULT nextval('business_plan_texts_id_seq'::regclass) NOT NULL,
    description text,
    name text,
    sort text,
    status text,
    img_url text,
    goal_statement_id integer NOT NULL
);


ALTER TABLE public.business_plan_texts OWNER TO YOUR_USERNAME_HERE;

--
-- Name: business_plans_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE business_plans_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.business_plans_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: calendar_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1;


ALTER TABLE public.calendar_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE categories (
    id integer DEFAULT nextval('categories_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    parent_category_id integer,
    sort text,
    status text,
    img_url text,
    project_id integer,
    permaculture_topic_id integer
);


ALTER TABLE public.categories OWNER TO YOUR_USERNAME_HERE;

--
-- Name: class_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.class_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: classes; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE classes (
    id integer DEFAULT nextval('class_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    ownership text,
    notes text,
    db_mysql text,
    user_name text,
    zachman_id integer,
    fields text,
    sort text,
    status text,
    img_url text,
    relationship text,
    db_postgres text,
    extends_class_id integer,
    spare_code text,
    rank text,
    subsystem text
);


ALTER TABLE public.classes OWNER TO YOUR_USERNAME_HERE;

--
-- Name: costs_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE costs_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.costs_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: databases; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE databases (
    id integer NOT NULL,
    management_system character varying(20),
    name text,
    date_last_backup text,
    schema_version text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.databases OWNER TO YOUR_USERNAME_HERE;

--
-- Name: design_docs_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE design_docs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.design_docs_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: design_instances; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE design_instances (
    id integer NOT NULL,
    design_id integer,
    name text,
    status text,
    layout_id integer,
    units_id integer,
    description text,
    sort text,
    img_url text
);


ALTER TABLE public.design_instances OWNER TO YOUR_USERNAME_HERE;

--
-- Name: design_order_items; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE design_order_items (
    id integer NOT NULL,
    name text,
    description text,
    design_order_id integer,
    cost text,
    shipping_cost text,
    design_instance_id integer
);


ALTER TABLE public.design_order_items OWNER TO YOUR_USERNAME_HERE;

--
-- Name: design_orders; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE design_orders (
    id integer NOT NULL,
    date text,
    note text,
    supplier_id integer
);


ALTER TABLE public.design_orders OWNER TO YOUR_USERNAME_HERE;

--
-- Name: designer_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE designer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.designer_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: designers; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE designers (
    id integer DEFAULT nextval('designer_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text,
    project_id integer
);


ALTER TABLE public.designers OWNER TO YOUR_USERNAME_HERE;

--
-- Name: designs_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE designs_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 56;


ALTER TABLE public.designs_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: designs; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE designs (
    id integer DEFAULT nextval('designs_id_sequence'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    project_id integer,
    domain_tli character varying(3),
    img_url text
);


ALTER TABLE public.designs OWNER TO YOUR_USERNAME_HERE;

--
-- Name: documentations; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE documentations (
    id integer NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    user_name text,
    categorization text,
    table_name text,
    img_url text,
    how_to_measure text
);


ALTER TABLE public.documentations OWNER TO YOUR_USERNAME_HERE;

--
-- Name: domain_application_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE domain_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.domain_application_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: domain_measurement_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE domain_measurement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.domain_measurement_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: domain_measurements; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE domain_measurements (
    id integer DEFAULT nextval('domain_measurement_id_seq'::regclass) NOT NULL,
    domain_tli character varying(3),
    domain_field_string text,
    webmaster_responsibility text,
    sort text
);


ALTER TABLE public.domain_measurements OWNER TO YOUR_USERNAME_HERE;

--
-- Name: domain_online_community_metrics_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE domain_online_community_metrics_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.domain_online_community_metrics_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: domains; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE domains (
    tli character varying(3) NOT NULL,
    domain_name character varying(100),
    tagline text,
    img_url text,
    sort character varying(12),
    registrar text,
    hosting text,
    status text,
    crm text,
    name text,
    user_name text,
    spotlight text,
    backups text,
    log text,
    design_id integer
);


ALTER TABLE public.domains OWNER TO YOUR_USERNAME_HERE;

--
-- Name: email_address_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE email_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.email_address_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: email_addresses; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE email_addresses (
    id integer DEFAULT nextval('email_address_id_seq'::regclass) NOT NULL,
    address text,
    name text,
    sort text,
    user_name text,
    domain_tli text,
    status text,
    img_url text,
    description text
);


ALTER TABLE public.email_addresses OWNER TO YOUR_USERNAME_HERE;

--
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.events_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: events; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE events (
    id integer DEFAULT nextval('events_id_seq'::regclass) NOT NULL,
    time_start timestamp with time zone DEFAULT '2014-04-01 04:00:00-04'::timestamp with time zone,
    time_finish timestamp with time zone DEFAULT '2014-04-01 03:00:00-04'::timestamp with time zone,
    name text
);


ALTER TABLE public.events OWNER TO YOUR_USERNAME_HERE;

--
-- Name: farmwork_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE farmwork_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.farmwork_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: goal_statements_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE goal_statements_id_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.goal_statements_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: goal_statements; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE goal_statements (
    id integer DEFAULT nextval('goal_statements_id_seq'::regclass) NOT NULL,
    description text,
    project_id integer NOT NULL,
    sort text,
    status text,
    name text,
    img_url text
);


ALTER TABLE public.goal_statements OWNER TO YOUR_USERNAME_HERE;

--
-- Name: guest_pass_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE guest_pass_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.guest_pass_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: guest_passes; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE guest_passes (
    id integer DEFAULT nextval('guest_pass_id_seq'::regclass) NOT NULL,
    owner_name text,
    url text,
    guest_name text
);


ALTER TABLE public.guest_passes OWNER TO YOUR_USERNAME_HERE;

--
-- Name: harvests; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE harvests (
    id integer NOT NULL,
    project_id integer,
    name text,
    sort integer,
    shares_estimate integer
);


ALTER TABLE public.harvests OWNER TO YOUR_USERNAME_HERE;

--
-- Name: harvests_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE harvests_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.harvests_plant_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: host_applications; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE host_applications (
    domain_tli character varying(3) NOT NULL,
    application_id integer NOT NULL,
    machine_id integer NOT NULL
);


ALTER TABLE public.host_applications OWNER TO YOUR_USERNAME_HERE;

--
-- Name: host_databases; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE host_databases (
    domain_tli character varying(3) NOT NULL,
    machine_id integer NOT NULL,
    database_id integer NOT NULL
);


ALTER TABLE public.host_databases OWNER TO YOUR_USERNAME_HERE;

--
-- Name: host_email_addresses; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE host_email_addresses (
    domain_tli character varying(3) NOT NULL,
    machine_id integer NOT NULL,
    email_address_id integer NOT NULL
);


ALTER TABLE public.host_email_addresses OWNER TO YOUR_USERNAME_HERE;

--
-- Name: hosts; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE hosts (
    parent_class_name_string text NOT NULL,
    parent_class_primary_key_string text NOT NULL,
    child_class_name_string text NOT NULL,
    child_class_primary_key_string text NOT NULL
);


ALTER TABLE public.hosts OWNER TO YOUR_USERNAME_HERE;

--
-- Name: hyperlinks_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE hyperlinks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.hyperlinks_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: hyperlinks; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE hyperlinks (
    id integer DEFAULT nextval('hyperlinks_id_seq'::regclass) NOT NULL,
    date character varying(10),
    url text,
    name text,
    addon text,
    alphabetical text,
    category text,
    category_id integer,
    user_name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.hyperlinks OWNER TO YOUR_USERNAME_HERE;

--
-- Name: hyperlinks_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE hyperlinks_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.hyperlinks_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: images; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE images (
    id integer NOT NULL,
    caption text,
    description text,
    photographer text,
    url text,
    license text
);


ALTER TABLE public.images OWNER TO YOUR_USERNAME_HERE;

--
-- Name: indiegoals; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE indiegoals (
    id integer NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    date text NOT NULL,
    reading text,
    yawp_agent_type text
);


ALTER TABLE public.indiegoals OWNER TO YOUR_USERNAME_HERE;

--
-- Name: indiegoals_date_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE indiegoals_date_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indiegoals_date_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: indiegoals_date_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE indiegoals_date_seq OWNED BY indiegoals.date;


--
-- Name: indiegoals_description_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE indiegoals_description_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indiegoals_description_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: indiegoals_description_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE indiegoals_description_seq OWNED BY indiegoals.description;


--
-- Name: indiegoals_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE indiegoals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indiegoals_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: indiegoals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE indiegoals_id_seq OWNED BY indiegoals.id;


--
-- Name: indiegoals_name_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE indiegoals_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.indiegoals_name_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: indiegoals_name_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE indiegoals_name_seq OWNED BY indiegoals.name;


--
-- Name: invoice_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE invoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.invoice_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: invoice_line_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE invoice_line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.invoice_line_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: invoice_lines; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE invoice_lines (
    id integer DEFAULT nextval('invoice_line_id_seq'::regclass) NOT NULL,
    invoice_id integer,
    product_id integer,
    quantity text,
    price text,
    pickup_detail_id integer
);


ALTER TABLE public.invoice_lines OWNER TO YOUR_USERNAME_HERE;

--
-- Name: invoices; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE invoices (
    id integer DEFAULT nextval('invoice_id_seq'::regclass) NOT NULL,
    patron_id integer,
    date text
);


ALTER TABLE public.invoices OWNER TO YOUR_USERNAME_HERE;

--
-- Name: journals; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE journals (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.journals OWNER TO YOUR_USERNAME_HERE;

--
-- Name: kernel_theories; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE kernel_theories (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.kernel_theories OWNER TO YOUR_USERNAME_HERE;

--
-- Name: land_beds; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE land_beds (
    id integer NOT NULL,
    soil_area_id integer,
    bed_num integer NOT NULL
);


ALTER TABLE public.land_beds OWNER TO YOUR_USERNAME_HERE;

--
-- Name: land_beds_bed_num_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE land_beds_bed_num_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.land_beds_bed_num_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: land_beds_bed_num_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER SEQUENCE land_beds_bed_num_seq OWNED BY land_beds.bed_num;


--
-- Name: land_city_states; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE land_city_states (
    id integer NOT NULL,
    land_id integer NOT NULL,
    city text,
    state text
);


ALTER TABLE public.land_city_states OWNER TO YOUR_USERNAME_HERE;

--
-- Name: land_farmers; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE land_farmers (
    id integer NOT NULL,
    soil_area_id integer,
    farmer text
);


ALTER TABLE public.land_farmers OWNER TO YOUR_USERNAME_HERE;

--
-- Name: land_traits_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE land_traits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.land_traits_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: land_traits; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE land_traits (
    id integer DEFAULT nextval('land_traits_id_seq'::regclass) NOT NULL,
    name text,
    value text,
    sort text,
    status text,
    land_id integer,
    reference text
);


ALTER TABLE public.land_traits OWNER TO YOUR_USERNAME_HERE;

--
-- Name: land_width_lengths; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE land_width_lengths (
    id integer NOT NULL,
    layout_id integer,
    width text,
    length text,
    soil_area_id integer
);


ALTER TABLE public.land_width_lengths OWNER TO YOUR_USERNAME_HERE;

--
-- Name: lands_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE lands_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.lands_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: lands; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE lands (
    id integer DEFAULT nextval('lands_id_sequence'::regclass) NOT NULL,
    name text,
    agricultural_type_id integer,
    sort text,
    description text,
    status text,
    temp_parent_land_id integer,
    img_url text,
    temp_layout_id integer,
    temp_soil_area_id integer
);


ALTER TABLE public.lands OWNER TO YOUR_USERNAME_HERE;

--
-- Name: location_workdates; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE location_workdates (
    class_primary_key_string integer NOT NULL,
    date text NOT NULL,
    class_name_string text NOT NULL
);


ALTER TABLE public.location_workdates OWNER TO YOUR_USERNAME_HERE;

--
-- Name: machines; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE machines (
    id integer NOT NULL,
    name text,
    description text,
    cpu text,
    filesystems text,
    sort text,
    status text,
    user_name text,
    ip_address text,
    img_url text
);


ALTER TABLE public.machines OWNER TO YOUR_USERNAME_HERE;

--
-- Name: manifest_maxonomies_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE manifest_maxonomies_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.manifest_maxonomies_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: manmax_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE manmax_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.manmax_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: material_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.material_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: maxonomy_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE maxonomy_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.maxonomy_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: maxonomies; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE maxonomies (
    id integer DEFAULT nextval('maxonomy_id_sequence'::regclass) NOT NULL,
    name text,
    img_url text,
    status text,
    user_name text,
    sort text,
    description text,
    categorization text,
    table_name text,
    how_to_measure text,
    ocm_id integer,
    order_by text
);


ALTER TABLE public.maxonomies OWNER TO YOUR_USERNAME_HERE;

--
-- Name: moneymaker_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE moneymaker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.moneymaker_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: moneymakers; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE moneymakers (
    id integer DEFAULT nextval('moneymaker_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.moneymakers OWNER TO YOUR_USERNAME_HERE;

--
-- Name: multifunction_dates_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE multifunction_dates_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.multifunction_dates_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_images_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE plant_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.plant_images_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: object_images; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE object_images (
    id integer DEFAULT nextval('plant_images_id_seq'::regclass) NOT NULL,
    image_id integer,
    class_primary_key_string text,
    class_name_string text
);


ALTER TABLE public.object_images OWNER TO YOUR_USERNAME_HERE;

--
-- Name: observation_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE observation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.observation_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: observations; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE observations (
    id integer DEFAULT nextval('observation_id_seq'::regclass) NOT NULL,
    land_id integer,
    unit_id integer DEFAULT 21,
    ts text,
    notes text,
    design_instance_id integer,
    raw text,
    raw_read boolean DEFAULT false,
    measurement text
);


ALTER TABLE public.observations OWNER TO YOUR_USERNAME_HERE;

--
-- Name: patron_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE patron_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.patron_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: patrons; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE patrons (
    id integer DEFAULT nextval('patron_id_seq'::regclass) NOT NULL,
    name text,
    project_id integer
);


ALTER TABLE public.patrons OWNER TO YOUR_USERNAME_HERE;

--
-- Name: permaculture_topic_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE permaculture_topic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.permaculture_topic_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: pickup_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE pickup_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.pickup_detail_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: pickup_details; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE pickup_details (
    id integer DEFAULT nextval('pickup_detail_id_seq'::regclass) NOT NULL,
    date text,
    land_id integer
);


ALTER TABLE public.pickup_details OWNER TO YOUR_USERNAME_HERE;

--
-- Name: pickup_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE pickup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.pickup_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: pickup_plants_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE pickup_plants_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.pickup_plants_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: pickup_plants; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE pickup_plants (
    id integer DEFAULT nextval('pickup_plants_seq'::regclass) NOT NULL,
    pickup_id integer,
    plant_id integer,
    unit_id integer,
    quantity real
);


ALTER TABLE public.pickup_plants OWNER TO YOUR_USERNAME_HERE;

--
-- Name: pickup_plants_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE pickup_plants_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.pickup_plants_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: pickups; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE pickups (
    id integer DEFAULT nextval('pickup_id_seq'::regclass) NOT NULL,
    harvest_id integer
);


ALTER TABLE public.pickups OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_aliases; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_aliases (
    id integer NOT NULL,
    plant_id integer,
    name text
);


ALTER TABLE public.plant_aliases OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_categories; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_categories (
    id integer NOT NULL,
    name text,
    description text
);


ALTER TABLE public.plant_categories OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_families; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_families (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.plant_families OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_histories_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE plant_histories_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.plant_histories_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_histories; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_histories (
    id integer DEFAULT nextval('plant_histories_id_sequence'::regclass) NOT NULL,
    plant_list_plant_id integer,
    seed_packet_id integer
);


ALTER TABLE public.plant_histories OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_history_events_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE plant_history_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.plant_history_events_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_history_events; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_history_events (
    id integer DEFAULT nextval('plant_history_events_id_seq'::regclass) NOT NULL,
    plant_history_id integer,
    plant_count text,
    direct_seed boolean DEFAULT false
);


ALTER TABLE public.plant_history_events OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_images; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_images (
    id integer,
    plant_id integer,
    image_id integer
);


ALTER TABLE public.plant_images OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_list_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE plant_list_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.plant_list_plant_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_list_plants; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_list_plants (
    id integer DEFAULT nextval('plant_list_plant_id_seq'::regclass) NOT NULL,
    plant_list_id integer,
    plant_id integer
);


ALTER TABLE public.plant_list_plants OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_list_projects; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_list_projects (
    id integer NOT NULL,
    plant_list_id integer,
    project_id integer
);


ALTER TABLE public.plant_list_projects OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_lists_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE plant_lists_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.plant_lists_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_lists; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_lists (
    id integer DEFAULT nextval('plant_lists_sequence'::regclass) NOT NULL,
    name text,
    sort text,
    description text,
    status text,
    img_url text
);


ALTER TABLE public.plant_lists OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_unit_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE plant_unit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.plant_unit_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plant_units; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plant_units (
    id integer DEFAULT nextval('plant_unit_id_seq'::regclass) NOT NULL,
    plant_id integer,
    unit_id integer
);


ALTER TABLE public.plant_units OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plants_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE plants_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.plants_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: plants; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE plants (
    id integer DEFAULT nextval('plants_id_sequence'::regclass) NOT NULL,
    common_name text,
    scientific_name text,
    plant_category_id integer,
    plant_family_id integer
);


ALTER TABLE public.plants OWNER TO YOUR_USERNAME_HERE;

--
-- Name: postings_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE postings_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.postings_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: postings; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE postings (
    id integer DEFAULT nextval('postings_id_sequence'::regclass) NOT NULL,
    account_id integer,
    journal_id integer,
    asset_type_id integer,
    amount text,
    budget_id integer,
    sort text,
    description text,
    status text,
    transaction_date text,
    transfer_account_id integer,
    supplier_id integer,
    due_date text,
    name text,
    img_url text
);


ALTER TABLE public.postings OWNER TO YOUR_USERNAME_HERE;

--
-- Name: potential_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE potential_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.potential_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: potentials; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE potentials (
    id integer DEFAULT nextval('potential_id_seq'::regclass) NOT NULL,
    name text,
    address text,
    homepage text,
    notes text,
    sort text,
    status text,
    webmaster_info text
);


ALTER TABLE public.potentials OWNER TO YOUR_USERNAME_HERE;

--
-- Name: prices; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE prices (
    id integer DEFAULT nextval('costs_id_sequence'::regclass) NOT NULL,
    plant_id integer,
    supplier_id integer,
    date character varying(10),
    dollars character varying(10),
    unit_count text,
    quality text,
    organic_flag character varying(10),
    unit_id integer
);


ALTER TABLE public.prices OWNER TO YOUR_USERNAME_HERE;

--
-- Name: process_flows; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE process_flows (
    parent_process_id integer NOT NULL,
    child_process_id integer NOT NULL,
    description text
);


ALTER TABLE public.process_flows OWNER TO YOUR_USERNAME_HERE;

--
-- Name: process_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE process_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.process_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: profile_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.profile_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: process_profiles; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE process_profiles (
    id integer DEFAULT nextval('profile_id_seq'::regclass) NOT NULL,
    sort text,
    scope text
);


ALTER TABLE public.process_profiles OWNER TO YOUR_USERNAME_HERE;

--
-- Name: processes; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE processes (
    id integer DEFAULT nextval('process_id_sequence'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    business_plan_text_id integer NOT NULL,
    status text,
    responsibility_day_count integer DEFAULT 28 NOT NULL,
    profile_id integer,
    priority text,
    img_url text
);


ALTER TABLE public.processes OWNER TO YOUR_USERNAME_HERE;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: products; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE products (
    id integer DEFAULT nextval('product_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.products OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_book_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_book_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_books; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE project_books (
    id integer DEFAULT nextval('project_book_id_seq'::regclass) NOT NULL,
    book_id integer,
    project_id integer
);


ALTER TABLE public.project_books OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_documentation_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_documentation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_documentation_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_domain_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_domain_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_domain_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_land_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_land_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_land_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_projects; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE project_projects (
    parent_project_id integer NOT NULL,
    child_project_id integer NOT NULL
);


ALTER TABLE public.project_projects OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_question_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_question_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_seasons_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_seasons_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_seasons_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_suppliers_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_suppliers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_suppliers_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_suppliers; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE project_suppliers (
    id integer DEFAULT nextval('project_suppliers_id_seq'::regclass) NOT NULL,
    project_id integer,
    supplier_id integer
);


ALTER TABLE public.project_suppliers OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_tool_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE project_tool_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.project_tool_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: project_tools; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE project_tools (
    id integer DEFAULT nextval('project_tool_id_seq'::regclass) NOT NULL,
    project_id integer,
    tool_id integer,
    instance_name text,
    instance_img_url text,
    sort text
);


ALTER TABLE public.project_tools OWNER TO YOUR_USERNAME_HERE;

--
-- Name: projects_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE projects_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.projects_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: projects; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE projects (
    id integer DEFAULT nextval('projects_id_sequence'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    user_name text,
    status text,
    img_url text
);


ALTER TABLE public.projects OWNER TO YOUR_USERNAME_HERE;

--
-- Name: question_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.question_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: references_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE references_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.references_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: responsibility_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE responsibility_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.responsibility_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: scene_element_account_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE scene_element_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.scene_element_account_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: scene_element_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE scene_element_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.scene_element_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: scene_element_responsibility_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE scene_element_responsibility_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.scene_element_responsibility_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: scene_elements; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE scene_elements (
    id integer DEFAULT nextval('scene_element_id_seq'::regclass) NOT NULL,
    process_id integer,
    sort text,
    database_string text,
    class_name_string text,
    class_primary_key_string text,
    description text,
    yield text,
    status text,
    img_url text,
    name text
);


ALTER TABLE public.scene_elements OWNER TO YOUR_USERNAME_HERE;

--
-- Name: seed_boxes_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE seed_boxes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.seed_boxes_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: seed_packets_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE seed_packets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.seed_packets_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: seed_packets; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE seed_packets (
    id integer DEFAULT nextval('seed_packets_id_seq'::regclass) NOT NULL,
    variety_id integer,
    supplier_id integer,
    packed_for_year text,
    contents text,
    sow_instructions text,
    days_to_maturity integer,
    days_to_germination text,
    notes text,
    product_details text
);


ALTER TABLE public.seed_packets OWNER TO YOUR_USERNAME_HERE;

--
-- Name: seq_crop_plan_crops; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE seq_crop_plan_crops
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.seq_crop_plan_crops OWNER TO YOUR_USERNAME_HERE;

--
-- Name: seq_field_id; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE seq_field_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.seq_field_id OWNER TO YOUR_USERNAME_HERE;

--
-- Name: seq_greenhouse_plan_seedings_id; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE seq_greenhouse_plan_seedings_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.seq_greenhouse_plan_seedings_id OWNER TO YOUR_USERNAME_HERE;

--
-- Name: seq_stakeholder_id; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE seq_stakeholder_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.seq_stakeholder_id OWNER TO YOUR_USERNAME_HERE;

--
-- Name: shares_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE shares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.shares_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: shares; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE shares (
    id integer DEFAULT nextval('shares_id_seq'::regclass) NOT NULL,
    harvest_id integer,
    owner text,
    price text
);


ALTER TABLE public.shares OWNER TO YOUR_USERNAME_HERE;

--
-- Name: shift_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE shift_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.shift_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: shifts; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE shifts (
    id integer DEFAULT nextval('shift_id_seq'::regclass) NOT NULL,
    sort text,
    start text,
    timeout text,
    process_profile_id integer,
    day_of_week integer
);


ALTER TABLE public.shifts OWNER TO YOUR_USERNAME_HERE;

--
-- Name: shifts_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE shifts_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.shifts_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: soil_areas; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE soil_areas (
    temp_soil_area_id integer NOT NULL,
    parent_land_id integer,
    name text,
    sort text,
    status text,
    description text
);


ALTER TABLE public.soil_areas OWNER TO YOUR_USERNAME_HERE;

--
-- Name: soil_areas_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE soil_areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.soil_areas_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: soil_tests_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE soil_tests_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.soil_tests_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: song_id_pkey; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE song_id_pkey
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.song_id_pkey OWNER TO YOUR_USERNAME_HERE;

--
-- Name: song_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE song_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.song_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: songs; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE songs (
    id integer DEFAULT nextval('song_id_seq'::regclass) NOT NULL,
    name text,
    sort text,
    lyrics_url text,
    mp3_url text,
    album_id integer,
    cover_flag boolean DEFAULT false,
    album_sort text,
    chords text
);


ALTER TABLE public.songs OWNER TO YOUR_USERNAME_HERE;

--
-- Name: spacing_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE spacing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.spacing_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: spacings; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE spacings (
    id integer DEFAULT nextval('spacing_id_seq'::regclass) NOT NULL,
    plant_id integer,
    rows_per_bed text,
    inches_between_plants text,
    source text
);


ALTER TABLE public.spacings OWNER TO YOUR_USERNAME_HERE;

--
-- Name: storage_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE storage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.storage_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: storages; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE storages (
    id integer DEFAULT nextval('storage_id_seq'::regclass) NOT NULL,
    plant_id integer,
    instructions text,
    source text
);


ALTER TABLE public.storages OWNER TO YOUR_USERNAME_HERE;

--
-- Name: suppliers_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE suppliers_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.suppliers_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: suppliers; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE suppliers (
    id integer DEFAULT nextval('suppliers_id_sequence'::regclass) NOT NULL,
    name text,
    city text,
    state text,
    url text,
    bioregion text,
    sort text,
    status text,
    description text,
    user_name text,
    img_url text,
    last_password_change text
);


ALTER TABLE public.suppliers OWNER TO YOUR_USERNAME_HERE;

--
-- Name: table_definition_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE table_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.table_definition_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: tags; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE tags (
    id integer DEFAULT nextval('tag_id_seq'::regclass) NOT NULL,
    project_id integer,
    name text,
    url text,
    reference text,
    description text,
    sort text,
    status text,
    img_url text
);


ALTER TABLE public.tags OWNER TO YOUR_USERNAME_HERE;

--
-- Name: tenperdays_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE tenperdays_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999999
    CACHE 1;


ALTER TABLE public.tenperdays_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: tenperdays; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE tenperdays (
    date text NOT NULL,
    webpages_blogmastered text,
    id integer DEFAULT nextval('tenperdays_id_sequence'::regclass) NOT NULL,
    view text
);


ALTER TABLE public.tenperdays OWNER TO YOUR_USERNAME_HERE;

--
-- Name: test_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE test_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.test_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE ticket_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.ticket_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: tickets; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE tickets (
    id integer DEFAULT nextval('ticket_id_seq'::regclass) NOT NULL,
    name text,
    sort text,
    status text DEFAULT 'open'::text,
    process_id integer,
    description text,
    img_url text,
    action_to_take text
);


ALTER TABLE public.tickets OWNER TO YOUR_USERNAME_HERE;

--
-- Name: timecard_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE timecard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.timecard_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: timecards; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE timecards (
    id integer DEFAULT nextval('timecard_id_seq'::regclass) NOT NULL,
    doer_user_name text,
    date text,
    time_in text,
    time_out text,
    description text,
    project_id integer,
    process_id integer,
    shift_id integer,
    scene_element_id integer
);


ALTER TABLE public.timecards OWNER TO YOUR_USERNAME_HERE;

--
-- Name: tools_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE tools_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.tools_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: tools; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE tools (
    id integer DEFAULT nextval('tools_id_sequence'::regclass) NOT NULL,
    name text,
    description text,
    img_url text,
    sort text,
    status text
);


ALTER TABLE public.tools OWNER TO YOUR_USERNAME_HERE;

--
-- Name: units; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE units (
    id integer NOT NULL,
    name text,
    description text
);


ALTER TABLE public.units OWNER TO YOUR_USERNAME_HERE;

--
-- Name: usernames; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE usernames (
    username character varying(12) NOT NULL,
    email_address_id integer,
    dashboard_php text
);


ALTER TABLE public.usernames OWNER TO YOUR_USERNAME_HERE;

--
-- Name: value_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE value_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.value_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: varieties_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE varieties_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.varieties_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: varieties; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE varieties (
    id integer DEFAULT nextval('varieties_id_sequence'::regclass) NOT NULL,
    plant_id integer,
    name text,
    description text
);


ALTER TABLE public.varieties OWNER TO YOUR_USERNAME_HERE;

--
-- Name: visit_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE visit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.visit_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: visits; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE visits (
    id integer DEFAULT nextval('visit_id_seq'::regclass) NOT NULL,
    rough_date text,
    fact boolean,
    land_id integer,
    name text,
    plant_history_event_id integer,
    "order" text,
    count integer,
    layout_id integer
);


ALTER TABLE public.visits OWNER TO YOUR_USERNAME_HERE;

--
-- Name: visits_id_sequence; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE visits_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.visits_id_sequence OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webmaster_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE webmaster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.webmaster_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webmasters; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE webmasters (
    id integer DEFAULT nextval('webmaster_id_seq'::regclass) NOT NULL,
    name text,
    description text,
    sort text,
    status text,
    img_url text,
    user_name text
);


ALTER TABLE public.webmasters OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpage_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE webpage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.webpage_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpage_maxonomies; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE webpage_maxonomies (
    webpage_id integer,
    maxonomy_id integer
);


ALTER TABLE public.webpage_maxonomies OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpage_moneymaker_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE webpage_moneymaker_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.webpage_moneymaker_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpage_moneymakers; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE webpage_moneymakers (
    id integer DEFAULT nextval('webpage_moneymaker_id_seq'::regclass) NOT NULL,
    webpage_id integer,
    moneymaker_id integer
);


ALTER TABLE public.webpage_moneymakers OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpage_tags; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE webpage_tags (
    webpage_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.webpage_tags OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpage_webpage_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE webpage_webpage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.webpage_webpage_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpage_webpages; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE webpage_webpages (
    parent_webpage_id integer NOT NULL,
    child_webpage_id integer NOT NULL
);


ALTER TABLE public.webpage_webpages OWNER TO YOUR_USERNAME_HERE;

--
-- Name: webpages; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE webpages (
    id integer DEFAULT nextval('manifest_maxonomies_sequence'::regclass) NOT NULL,
    temp_domain_tli character varying(3),
    description text,
    name text,
    sort text,
    img_url text,
    status text,
    path text,
    parameters text
);


ALTER TABLE public.webpages OWNER TO YOUR_USERNAME_HERE;

--
-- Name: whoihelps_id_seq; Type: SEQUENCE; Schema: public; Owner: YOUR_USERNAME_HERE
--

CREATE SEQUENCE whoihelps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 999999
    CACHE 1;


ALTER TABLE public.whoihelps_id_seq OWNER TO YOUR_USERNAME_HERE;

--
-- Name: yields; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE yields (
    id integer NOT NULL,
    plant_id integer,
    estimated_yield text,
    numerator_unit_id integer,
    source text,
    denominator_unit_id integer,
    range text
);


ALTER TABLE public.yields OWNER TO YOUR_USERNAME_HERE;

--
-- Name: zachmans; Type: TABLE; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

CREATE TABLE zachmans (
    id integer NOT NULL,
    name text,
    description text
);


ALTER TABLE public.zachmans OWNER TO YOUR_USERNAME_HERE;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY indiegoals ALTER COLUMN id SET DEFAULT nextval('indiegoals_id_seq'::regclass);


--
-- Name: bed_num; Type: DEFAULT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY land_beds ALTER COLUMN bed_num SET DEFAULT nextval('land_beds_bed_num_seq'::regclass);


--
-- Name: accounts_class_name_unique; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_class_name_unique UNIQUE (class_name_string);


--
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: agricultural_types_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY agricultural_types
    ADD CONSTRAINT agricultural_types_pkey PRIMARY KEY (id);


--
-- Name: albums_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY albums
    ADD CONSTRAINT albums_pkey PRIMARY KEY (id);


--
-- Name: applications_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY applications
    ADD CONSTRAINT applications_pkey PRIMARY KEY (id);


--
-- Name: asset_types_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY asset_types
    ADD CONSTRAINT asset_types_pkey PRIMARY KEY (id);


--
-- Name: blogposts_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY blog_posts
    ADD CONSTRAINT blogposts_pkey PRIMARY KEY (id);


--
-- Name: book_id_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY books
    ADD CONSTRAINT book_id_pkey PRIMARY KEY (id);


--
-- Name: bookclips_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT bookclips_pkey PRIMARY KEY (id);


--
-- Name: budgets_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY budgets
    ADD CONSTRAINT budgets_pkey PRIMARY KEY (id);


--
-- Name: builds_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_pkey PRIMARY KEY (id);


--
-- Name: builds_project_id_unique; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_project_id_unique UNIQUE (project_id);


--
-- Name: categories_id_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_id_pkey PRIMARY KEY (id);


--
-- Name: classes_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY classes
    ADD CONSTRAINT classes_pkey PRIMARY KEY (id);


--
-- Name: costs_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT costs_pkey PRIMARY KEY (id);


--
-- Name: databases_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY databases
    ADD CONSTRAINT databases_pkey PRIMARY KEY (id);


--
-- Name: designers_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY designers
    ADD CONSTRAINT designers_pkey PRIMARY KEY (id);


--
-- Name: domain_machine_databases_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT domain_machine_databases_pkey PRIMARY KEY (domain_tli, machine_id, database_id);


--
-- Name: domain_measurements_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY domain_measurements
    ADD CONSTRAINT domain_measurements_pkey PRIMARY KEY (id);


--
-- Name: domains_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY domains
    ADD CONSTRAINT domains_pkey PRIMARY KEY (tli);


--
-- Name: email_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY email_addresses
    ADD CONSTRAINT email_addresses_pkey PRIMARY KEY (id);


--
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: goal_statements_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY goal_statements
    ADD CONSTRAINT goal_statements_pkey PRIMARY KEY (id);


--
-- Name: greenhouse_plans_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_histories
    ADD CONSTRAINT greenhouse_plans_pkey PRIMARY KEY (id);


--
-- Name: guest_passes_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY guest_passes
    ADD CONSTRAINT guest_passes_pkey PRIMARY KEY (id);


--
-- Name: harvests_pkey1; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY harvests
    ADD CONSTRAINT harvests_pkey1 PRIMARY KEY (id);


--
-- Name: host_applications_compound_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_compound_pkey PRIMARY KEY (domain_tli, application_id, machine_id);


--
-- Name: host_email_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_email_addresses_pkey PRIMARY KEY (domain_tli, machine_id, email_address_id);


--
-- Name: hosts_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY hosts
    ADD CONSTRAINT hosts_pkey PRIMARY KEY (parent_class_name_string, parent_class_primary_key_string, child_class_name_string, child_class_primary_key_string);


--
-- Name: hyperlinks_id_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY hyperlinks
    ADD CONSTRAINT hyperlinks_id_pkey PRIMARY KEY (id);


--
-- Name: images_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: invoice_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY invoice_lines
    ADD CONSTRAINT invoice_lines_pkey PRIMARY KEY (id);


--
-- Name: invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY invoices
    ADD CONSTRAINT invoices_pkey PRIMARY KEY (id);


--
-- Name: journals_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY journals
    ADD CONSTRAINT journals_pkey PRIMARY KEY (id);


--
-- Name: kernel_theories_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY kernel_theories
    ADD CONSTRAINT kernel_theories_pkey PRIMARY KEY (id);


--
-- Name: land_beds_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY land_beds
    ADD CONSTRAINT land_beds_pkey PRIMARY KEY (id);


--
-- Name: land_city_states_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY land_city_states
    ADD CONSTRAINT land_city_states_pkey PRIMARY KEY (id);


--
-- Name: land_farmers_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY land_farmers
    ADD CONSTRAINT land_farmers_pkey PRIMARY KEY (id);


--
-- Name: land_traits_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY land_traits
    ADD CONSTRAINT land_traits_pkey PRIMARY KEY (id);


--
-- Name: land_width_lengths_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY land_width_lengths
    ADD CONSTRAINT land_width_lengths_pkey PRIMARY KEY (id);


--
-- Name: lands_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY lands
    ADD CONSTRAINT lands_pkey PRIMARY KEY (id);


--
-- Name: location_workdates_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY location_workdates
    ADD CONSTRAINT location_workdates_pkey PRIMARY KEY (class_name_string, class_primary_key_string, date);


--
-- Name: machines_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY machines
    ADD CONSTRAINT machines_pkey PRIMARY KEY (id);


--
-- Name: manifest_maxonomies_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY webpages
    ADD CONSTRAINT manifest_maxonomies_pkey PRIMARY KEY (id);


--
-- Name: maxonomies_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY maxonomies
    ADD CONSTRAINT maxonomies_pkey PRIMARY KEY (id);


--
-- Name: moneymakers_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY moneymakers
    ADD CONSTRAINT moneymakers_pkey PRIMARY KEY (id);


--
-- Name: patrons_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY patrons
    ADD CONSTRAINT patrons_pkey PRIMARY KEY (id);


--
-- Name: pickup_details_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY pickup_details
    ADD CONSTRAINT pickup_details_pkey PRIMARY KEY (id);


--
-- Name: pickup_plants_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY pickup_plants
    ADD CONSTRAINT pickup_plants_pkey PRIMARY KEY (id);


--
-- Name: pickups_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY pickups
    ADD CONSTRAINT pickups_pkey PRIMARY KEY (id);


--
-- Name: pkey_business_plan_texts; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY business_plan_texts
    ADD CONSTRAINT pkey_business_plan_texts PRIMARY KEY (id);


--
-- Name: pkey_design_id; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY designs
    ADD CONSTRAINT pkey_design_id PRIMARY KEY (id);


--
-- Name: pkey_design_instances; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY design_instances
    ADD CONSTRAINT pkey_design_instances PRIMARY KEY (id);


--
-- Name: pkey_family_id; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_families
    ADD CONSTRAINT pkey_family_id PRIMARY KEY (id);


--
-- Name: pkey_id; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_units
    ADD CONSTRAINT pkey_id PRIMARY KEY (id);


--
-- Name: pkey_indiegoals; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY indiegoals
    ADD CONSTRAINT pkey_indiegoals PRIMARY KEY (id);


--
-- Name: pkey_observations; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY observations
    ADD CONSTRAINT pkey_observations PRIMARY KEY (id);


--
-- Name: pkey_order_id; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY design_orders
    ADD CONSTRAINT pkey_order_id PRIMARY KEY (id);


--
-- Name: pkey_part_id; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY design_order_items
    ADD CONSTRAINT pkey_part_id PRIMARY KEY (id);


--
-- Name: pkey_sowings; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT pkey_sowings PRIMARY KEY (id);


--
-- Name: plant_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_categories
    ADD CONSTRAINT plant_categories_pkey PRIMARY KEY (id);


--
-- Name: plant_farm_details_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT plant_farm_details_pkey PRIMARY KEY (id);


--
-- Name: plant_history_events_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_history_events
    ADD CONSTRAINT plant_history_events_pkey PRIMARY KEY (id);


--
-- Name: plant_images_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY object_images
    ADD CONSTRAINT plant_images_pkey PRIMARY KEY (id);


--
-- Name: plant_list_plants_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_list_plants
    ADD CONSTRAINT plant_list_plants_pkey PRIMARY KEY (id);


--
-- Name: plant_list_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_list_projects
    ADD CONSTRAINT plant_list_projects_pkey PRIMARY KEY (id);


--
-- Name: plant_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_lists
    ADD CONSTRAINT plant_lists_pkey PRIMARY KEY (id);


--
-- Name: plants_aliases_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plant_aliases
    ADD CONSTRAINT plants_aliases_pkey PRIMARY KEY (id);


--
-- Name: plants_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY plants
    ADD CONSTRAINT plants_pkey PRIMARY KEY (id);


--
-- Name: postings_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_pkey PRIMARY KEY (id);


--
-- Name: potentials_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY potentials
    ADD CONSTRAINT potentials_pkey PRIMARY KEY (id);


--
-- Name: process_flows_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY process_flows
    ADD CONSTRAINT process_flows_pkey PRIMARY KEY (parent_process_id, child_process_id);


--
-- Name: processes_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY processes
    ADD CONSTRAINT processes_pkey PRIMARY KEY (id);


--
-- Name: processes_priority_unique; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY processes
    ADD CONSTRAINT processes_priority_unique UNIQUE (priority);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY process_profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- Name: project_books_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY project_books
    ADD CONSTRAINT project_books_pkey PRIMARY KEY (id);


--
-- Name: project_parts_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY documentations
    ADD CONSTRAINT project_parts_pkey PRIMARY KEY (id);


--
-- Name: project_projects_child_id_unique; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY project_projects
    ADD CONSTRAINT project_projects_child_id_unique UNIQUE (child_project_id);


--
-- Name: project_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY project_projects
    ADD CONSTRAINT project_projects_pkey PRIMARY KEY (parent_project_id, child_project_id);


--
-- Name: project_supplier_id_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY project_suppliers
    ADD CONSTRAINT project_supplier_id_pkey PRIMARY KEY (id);


--
-- Name: project_tool_key; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY project_tools
    ADD CONSTRAINT project_tool_key PRIMARY KEY (id);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: scene_elements_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY scene_elements
    ADD CONSTRAINT scene_elements_pkey PRIMARY KEY (id);


--
-- Name: seed_packets_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY seed_packets
    ADD CONSTRAINT seed_packets_pkey PRIMARY KEY (id);


--
-- Name: shares_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY shares
    ADD CONSTRAINT shares_pkey PRIMARY KEY (id);


--
-- Name: shifts_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY shifts
    ADD CONSTRAINT shifts_pkey PRIMARY KEY (id);


--
-- Name: soil_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY soil_areas
    ADD CONSTRAINT soil_areas_pkey PRIMARY KEY (temp_soil_area_id);


--
-- Name: songs_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY songs
    ADD CONSTRAINT songs_pkey PRIMARY KEY (id);


--
-- Name: spacings_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY spacings
    ADD CONSTRAINT spacings_pkey PRIMARY KEY (id);


--
-- Name: storages_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY storages
    ADD CONSTRAINT storages_pkey PRIMARY KEY (id);


--
-- Name: suppliers_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY suppliers
    ADD CONSTRAINT suppliers_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: tenperdays_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY tenperdays
    ADD CONSTRAINT tenperdays_pkey PRIMARY KEY (id);


--
-- Name: tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: timecards_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY timecards
    ADD CONSTRAINT timecards_pkey PRIMARY KEY (id);


--
-- Name: tools_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY tools
    ADD CONSTRAINT tools_pkey PRIMARY KEY (id);


--
-- Name: units_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: usernames_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY usernames
    ADD CONSTRAINT usernames_pkey PRIMARY KEY (username);


--
-- Name: varieties_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY varieties
    ADD CONSTRAINT varieties_pkey PRIMARY KEY (id);


--
-- Name: webmasters_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY webmasters
    ADD CONSTRAINT webmasters_pkey PRIMARY KEY (id);


--
-- Name: webpage_moneymakers_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY webpage_moneymakers
    ADD CONSTRAINT webpage_moneymakers_pkey PRIMARY KEY (id);


--
-- Name: webpage_tags_key; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY webpage_tags
    ADD CONSTRAINT webpage_tags_key PRIMARY KEY (webpage_id, tag_id);


--
-- Name: webpage_webpages_child_is_unique; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY webpage_webpages
    ADD CONSTRAINT webpage_webpages_child_is_unique UNIQUE (child_webpage_id);


--
-- Name: webpage_webpages_double_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY webpage_webpages
    ADD CONSTRAINT webpage_webpages_double_pkey PRIMARY KEY (parent_webpage_id, child_webpage_id);


--
-- Name: zachmans_pkey; Type: CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE; Tablespace: 
--

ALTER TABLE ONLY zachmans
    ADD CONSTRAINT zachmans_pkey PRIMARY KEY (id);


--
-- Name: blogposts_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY blog_posts
    ADD CONSTRAINT blogposts_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id);


--
-- Name: budgets_scene_element_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY budgets
    ADD CONSTRAINT budgets_scene_element_id_fkey FOREIGN KEY (scene_element_id) REFERENCES scene_elements(id);


--
-- Name: business_plan_texts_goal_statement_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY business_plan_texts
    ADD CONSTRAINT business_plan_texts_goal_statement_id_fkey FOREIGN KEY (goal_statement_id) REFERENCES goal_statements(id);


--
-- Name: classes_extends_class_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY classes
    ADD CONSTRAINT classes_extends_class_id_fkey FOREIGN KEY (extends_class_id) REFERENCES classes(id);


--
-- Name: classes_zachman_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY classes
    ADD CONSTRAINT classes_zachman_id_fkey FOREIGN KEY (zachman_id) REFERENCES zachmans(id);


--
-- Name: design_instances_design_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY design_instances
    ADD CONSTRAINT design_instances_design_id_fkey FOREIGN KEY (design_id) REFERENCES designs(id);


--
-- Name: designers_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY designers
    ADD CONSTRAINT designers_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: designs_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY designs
    ADD CONSTRAINT designs_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: domain_machine_databases_database_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT domain_machine_databases_database_id_fkey FOREIGN KEY (database_id) REFERENCES databases(id);


--
-- Name: domain_machine_databases_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT domain_machine_databases_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: domain_machine_databases_machine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_databases
    ADD CONSTRAINT domain_machine_databases_machine_id_fkey FOREIGN KEY (machine_id) REFERENCES machines(id);


--
-- Name: email_addresses_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY email_addresses
    ADD CONSTRAINT email_addresses_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: fkey_account_postings; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT fkey_account_postings FOREIGN KEY (account_id) REFERENCES accounts(id);


--
-- Name: fkey_asset_type_postings; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT fkey_asset_type_postings FOREIGN KEY (asset_type_id) REFERENCES asset_types(id);


--
-- Name: fkey_book_id_book_clips; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT fkey_book_id_book_clips FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: fkey_budget_postings; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT fkey_budget_postings FOREIGN KEY (budget_id) REFERENCES budgets(id);


--
-- Name: fkey_costs_plant_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT fkey_costs_plant_id FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: fkey_costs_supplier_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT fkey_costs_supplier_id FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: fkey_demonitaor_unit_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT fkey_demonitaor_unit_id FOREIGN KEY (denominator_unit_id) REFERENCES units(id);


--
-- Name: fkey_design_order_parts_design_instance_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY design_order_items
    ADD CONSTRAINT fkey_design_order_parts_design_instance_id FOREIGN KEY (design_instance_id) REFERENCES design_instances(id);


--
-- Name: fkey_design_order_parts_design_order_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY design_order_items
    ADD CONSTRAINT fkey_design_order_parts_design_order_id FOREIGN KEY (design_order_id) REFERENCES design_orders(id);


--
-- Name: fkey_family_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plants
    ADD CONSTRAINT fkey_family_id FOREIGN KEY (plant_family_id) REFERENCES plant_families(id);


--
-- Name: fkey_goal_statements_project_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY goal_statements
    ADD CONSTRAINT fkey_goal_statements_project_id FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: fkey_harvests_project_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY harvests
    ADD CONSTRAINT fkey_harvests_project_id FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: fkey_image_id_bookclips; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT fkey_image_id_bookclips FOREIGN KEY (image_id) REFERENCES images(id);


--
-- Name: fkey_journal_postings; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT fkey_journal_postings FOREIGN KEY (journal_id) REFERENCES journals(id);


--
-- Name: fkey_land_agricultural_type_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY lands
    ADD CONSTRAINT fkey_land_agricultural_type_id FOREIGN KEY (agricultural_type_id) REFERENCES agricultural_types(id);


--
-- Name: fkey_pickup_plants_pickup_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY pickup_plants
    ADD CONSTRAINT fkey_pickup_plants_pickup_id FOREIGN KEY (pickup_id) REFERENCES pickups(id);


--
-- Name: fkey_pickup_plants_plant_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY pickup_plants
    ADD CONSTRAINT fkey_pickup_plants_plant_id FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: fkey_pickups_harvest_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY pickups
    ADD CONSTRAINT fkey_pickups_harvest_id FOREIGN KEY (harvest_id) REFERENCES harvests(id);


--
-- Name: fkey_plant_categories; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plants
    ADD CONSTRAINT fkey_plant_categories FOREIGN KEY (plant_category_id) REFERENCES plant_categories(id);


--
-- Name: fkey_plant_farm_details_plant_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT fkey_plant_farm_details_plant_id FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: fkey_plant_histories_plant_list_plant; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plant_histories
    ADD CONSTRAINT fkey_plant_histories_plant_list_plant FOREIGN KEY (plant_list_plant_id) REFERENCES plant_list_plants(id);


--
-- Name: fkey_plant_histories_seed_packet_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plant_histories
    ADD CONSTRAINT fkey_plant_histories_seed_packet_id FOREIGN KEY (seed_packet_id) REFERENCES seed_packets(id);


--
-- Name: fkey_plant_history_event_plant_history_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plant_history_events
    ADD CONSTRAINT fkey_plant_history_event_plant_history_id FOREIGN KEY (plant_history_id) REFERENCES plant_histories(id);


--
-- Name: fkey_plant_id_bookclips; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY book_clips
    ADD CONSTRAINT fkey_plant_id_bookclips FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: fkey_seed_packets_supplier_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY seed_packets
    ADD CONSTRAINT fkey_seed_packets_supplier_id FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: fkey_seed_packets_variety_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY seed_packets
    ADD CONSTRAINT fkey_seed_packets_variety_id FOREIGN KEY (variety_id) REFERENCES varieties(id);


--
-- Name: fkey_spacing_plant_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY spacings
    ADD CONSTRAINT fkey_spacing_plant_id FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: fkey_values_unit_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT fkey_values_unit_id FOREIGN KEY (unit_id) REFERENCES units(id);


--
-- Name: fkey_varities_plant_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY varieties
    ADD CONSTRAINT fkey_varities_plant_id FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: fkey_yields_unit_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY yields
    ADD CONSTRAINT fkey_yields_unit_id FOREIGN KEY (numerator_unit_id) REFERENCES units(id);


--
-- Name: host_applications_application_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_application_id_fkey FOREIGN KEY (application_id) REFERENCES applications(id);


--
-- Name: host_applications_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: host_applications_email_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_applications_email_address_id_fkey FOREIGN KEY (email_address_id) REFERENCES email_addresses(id);


--
-- Name: host_applications_machine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_applications
    ADD CONSTRAINT host_applications_machine_id_fkey FOREIGN KEY (machine_id) REFERENCES machines(id);


--
-- Name: host_email_addresses_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_email_addresses_domain_tli_fkey FOREIGN KEY (domain_tli) REFERENCES domains(tli);


--
-- Name: host_email_addresses_machine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY host_email_addresses
    ADD CONSTRAINT host_email_addresses_machine_id_fkey FOREIGN KEY (machine_id) REFERENCES machines(id);


--
-- Name: hyperlinks_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY hyperlinks
    ADD CONSTRAINT hyperlinks_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- Name: invoice_lines_invoice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY invoice_lines
    ADD CONSTRAINT invoice_lines_invoice_id_fkey FOREIGN KEY (invoice_id) REFERENCES invoices(id);


--
-- Name: invoice_lines_product_id; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY invoice_lines
    ADD CONSTRAINT invoice_lines_product_id FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: land_trains_land_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY land_traits
    ADD CONSTRAINT land_trains_land_id_fkey FOREIGN KEY (land_id) REFERENCES lands(id);


--
-- Name: plant_image_image_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY object_images
    ADD CONSTRAINT plant_image_image_id_fkey FOREIGN KEY (image_id) REFERENCES images(id);


--
-- Name: plant_list_plants_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plant_list_plants
    ADD CONSTRAINT plant_list_plants_id_fkey FOREIGN KEY (plant_list_id) REFERENCES plant_lists(id);


--
-- Name: plant_list_plants_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plant_list_plants
    ADD CONSTRAINT plant_list_plants_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plants(id);


--
-- Name: plant_list_project_plant_list_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plant_list_projects
    ADD CONSTRAINT plant_list_project_plant_list_fkey FOREIGN KEY (plant_list_id) REFERENCES plant_lists(id);


--
-- Name: plant_list_projects_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY plant_list_projects
    ADD CONSTRAINT plant_list_projects_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: posting_transfer_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT posting_transfer_account_id_fkey FOREIGN KEY (transfer_account_id) REFERENCES accounts(id);


--
-- Name: postings_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY postings
    ADD CONSTRAINT postings_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: process_flow_child_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY process_flows
    ADD CONSTRAINT process_flow_child_process_id_fkey FOREIGN KEY (child_process_id) REFERENCES processes(id);


--
-- Name: process_flow_parent_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY process_flows
    ADD CONSTRAINT process_flow_parent_process_id_fkey FOREIGN KEY (parent_process_id) REFERENCES processes(id);


--
-- Name: processes_bpt_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY processes
    ADD CONSTRAINT processes_bpt_id_fkey FOREIGN KEY (business_plan_text_id) REFERENCES business_plan_texts(id);


--
-- Name: profile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY processes
    ADD CONSTRAINT profile_id_fkey FOREIGN KEY (profile_id) REFERENCES process_profiles(id);


--
-- Name: project_books_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_books
    ADD CONSTRAINT project_books_book_id_fkey FOREIGN KEY (book_id) REFERENCES books(id);


--
-- Name: project_books_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_books
    ADD CONSTRAINT project_books_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: project_projects_child_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_projects
    ADD CONSTRAINT project_projects_child_project_id_fkey FOREIGN KEY (child_project_id) REFERENCES projects(id);


--
-- Name: project_projects_parent_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_projects
    ADD CONSTRAINT project_projects_parent_project_id_fkey FOREIGN KEY (parent_project_id) REFERENCES projects(id);


--
-- Name: project_suppliers_supplier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_suppliers
    ADD CONSTRAINT project_suppliers_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES suppliers(id);


--
-- Name: project_tools_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_tools
    ADD CONSTRAINT project_tools_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: project_tools_tool_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_tools
    ADD CONSTRAINT project_tools_tool_id_fkey FOREIGN KEY (tool_id) REFERENCES tools(id);


--
-- Name: scene_element_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY scene_elements
    ADD CONSTRAINT scene_element_process_id_fkey FOREIGN KEY (process_id) REFERENCES processes(id);


--
-- Name: shift_profile_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY shifts
    ADD CONSTRAINT shift_profile_id_fkey FOREIGN KEY (process_profile_id) REFERENCES process_profiles(id);


--
-- Name: supplier_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY project_suppliers
    ADD CONSTRAINT supplier_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: tags_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_project_id_fkey FOREIGN KEY (project_id) REFERENCES projects(id);


--
-- Name: tickets_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_process_id_fkey FOREIGN KEY (process_id) REFERENCES processes(id);


--
-- Name: usernames_email_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY usernames
    ADD CONSTRAINT usernames_email_address_id_fkey FOREIGN KEY (email_address_id) REFERENCES email_addresses(id);


--
-- Name: visits_plant_history_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT visits_plant_history_event_id_fkey FOREIGN KEY (plant_history_event_id) REFERENCES plant_history_events(id);


--
-- Name: webpage_maxonomies_maxonomy_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_maxonomies
    ADD CONSTRAINT webpage_maxonomies_maxonomy_id_fkey FOREIGN KEY (maxonomy_id) REFERENCES maxonomies(id);


--
-- Name: webpage_maxonomies_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_maxonomies
    ADD CONSTRAINT webpage_maxonomies_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id) ON DELETE CASCADE;


--
-- Name: webpage_moneymakers_moneymaker_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_moneymakers
    ADD CONSTRAINT webpage_moneymakers_moneymaker_id_fkey FOREIGN KEY (moneymaker_id) REFERENCES moneymakers(id);


--
-- Name: webpage_moneymakers_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_moneymakers
    ADD CONSTRAINT webpage_moneymakers_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id);


--
-- Name: webpage_tags_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_tags
    ADD CONSTRAINT webpage_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: webpage_tags_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_tags
    ADD CONSTRAINT webpage_tags_webpage_id_fkey FOREIGN KEY (webpage_id) REFERENCES webpages(id);


--
-- Name: webpage_webpages_child_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_webpages
    ADD CONSTRAINT webpage_webpages_child_webpage_id_fkey FOREIGN KEY (child_webpage_id) REFERENCES webpages(id);


--
-- Name: webpage_webpages_parent_webpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpage_webpages
    ADD CONSTRAINT webpage_webpages_parent_webpage_id_fkey FOREIGN KEY (parent_webpage_id) REFERENCES webpages(id);


--
-- Name: webpages_domain_tli_fkey; Type: FK CONSTRAINT; Schema: public; Owner: YOUR_USERNAME_HERE
--

ALTER TABLE ONLY webpages
    ADD CONSTRAINT webpages_domain_tli_fkey FOREIGN KEY (temp_domain_tli) REFERENCES domains(tli);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: accounts_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE accounts_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE accounts_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE accounts_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE accounts_id_sequence TO frb_admin;


--
-- Name: accounts; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE accounts FROM PUBLIC;
REVOKE ALL ON TABLE accounts FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE accounts TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE accounts TO frb_admin;


--
-- Name: agricultural_types; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE agricultural_types FROM PUBLIC;
REVOKE ALL ON TABLE agricultural_types FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE agricultural_types TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE agricultural_types TO frb_admin;


--
-- Name: album_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE album_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE album_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE album_id_seq TO YOUR_USERNAME_HERE;


--
-- Name: albums; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE albums FROM PUBLIC;
REVOKE ALL ON TABLE albums FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE albums TO YOUR_USERNAME_HERE;


--
-- Name: application_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE application_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE application_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE application_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE application_id_sequence TO frb_admin;


--
-- Name: application_instance_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE application_instance_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE application_instance_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE application_instance_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE application_instance_id_seq TO frb_admin;


--
-- Name: applications; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE applications FROM PUBLIC;
REVOKE ALL ON TABLE applications FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE applications TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE applications TO frb_admin;


--
-- Name: asset_types; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE asset_types FROM PUBLIC;
REVOKE ALL ON TABLE asset_types FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE asset_types TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE asset_types TO frb_admin;


--
-- Name: beds_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE beds_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE beds_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE beds_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE beds_id_sequence TO frb_admin;


--
-- Name: blogpost_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE blogpost_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE blogpost_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE blogpost_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE blogpost_id_seq TO frb_admin;


--
-- Name: blog_posts; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE blog_posts FROM PUBLIC;
REVOKE ALL ON TABLE blog_posts FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE blog_posts TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE blog_posts TO frb_admin;


--
-- Name: book_clips; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE book_clips FROM PUBLIC;
REVOKE ALL ON TABLE book_clips FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE book_clips TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE book_clips TO frb_admin;


--
-- Name: book_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE book_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE book_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE book_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE book_id_seq TO frb_admin;


--
-- Name: bookclips_excerpt_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE bookclips_excerpt_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bookclips_excerpt_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE bookclips_excerpt_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE bookclips_excerpt_seq TO frb_admin;


--
-- Name: bookclips_image_url_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE bookclips_image_url_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bookclips_image_url_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE bookclips_image_url_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE bookclips_image_url_seq TO frb_admin;


--
-- Name: bookclips_plant_common_name_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE bookclips_plant_common_name_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE bookclips_plant_common_name_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE bookclips_plant_common_name_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE bookclips_plant_common_name_seq TO frb_admin;


--
-- Name: books; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE books FROM PUBLIC;
REVOKE ALL ON TABLE books FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE books TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE books TO frb_admin;


--
-- Name: budget_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE budget_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE budget_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE budget_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE budget_id_sequence TO frb_admin;


--
-- Name: budgets; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE budgets FROM PUBLIC;
REVOKE ALL ON TABLE budgets FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE budgets TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE budgets TO frb_admin;


--
-- Name: build_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE build_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE build_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE build_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE build_id_seq TO frb_admin;


--
-- Name: builds; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE builds FROM PUBLIC;
REVOKE ALL ON TABLE builds FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE builds TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE builds TO frb_admin;


--
-- Name: business_plan_texts_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE business_plan_texts_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE business_plan_texts_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE business_plan_texts_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE business_plan_texts_id_seq TO frb_admin;


--
-- Name: business_plan_texts; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE business_plan_texts FROM PUBLIC;
REVOKE ALL ON TABLE business_plan_texts FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE business_plan_texts TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE business_plan_texts TO frb_admin;


--
-- Name: business_plans_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE business_plans_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE business_plans_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE business_plans_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE business_plans_sequence TO frb_admin;


--
-- Name: calendar_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE calendar_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE calendar_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE calendar_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE calendar_id_seq TO frb_admin;


--
-- Name: categories; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE categories FROM PUBLIC;
REVOKE ALL ON TABLE categories FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE categories TO YOUR_USERNAME_HERE;


--
-- Name: class_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE class_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE class_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE class_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE class_id_seq TO frb_admin;


--
-- Name: classes; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE classes FROM PUBLIC;
REVOKE ALL ON TABLE classes FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE classes TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE classes TO frb_admin;


--
-- Name: costs_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE costs_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE costs_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE costs_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE costs_id_sequence TO frb_admin;


--
-- Name: databases; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE databases FROM PUBLIC;
REVOKE ALL ON TABLE databases FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE databases TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE databases TO frb_admin;


--
-- Name: design_docs_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE design_docs_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE design_docs_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE design_docs_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE design_docs_id_seq TO frb_admin;


--
-- Name: design_instances; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE design_instances FROM PUBLIC;
REVOKE ALL ON TABLE design_instances FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE design_instances TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE design_instances TO frb_admin;


--
-- Name: design_order_items; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE design_order_items FROM PUBLIC;
REVOKE ALL ON TABLE design_order_items FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE design_order_items TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE design_order_items TO frb_admin;


--
-- Name: design_orders; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE design_orders FROM PUBLIC;
REVOKE ALL ON TABLE design_orders FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE design_orders TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE design_orders TO frb_admin;


--
-- Name: designer_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE designer_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE designer_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE designer_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE designer_id_seq TO frb_admin;


--
-- Name: designers; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE designers FROM PUBLIC;
REVOKE ALL ON TABLE designers FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE designers TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE designers TO frb_admin;


--
-- Name: designs; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE designs FROM PUBLIC;
REVOKE ALL ON TABLE designs FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE designs TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE designs TO frb_admin;


--
-- Name: documentations; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE documentations FROM PUBLIC;
REVOKE ALL ON TABLE documentations FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE documentations TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE documentations TO frb_admin;


--
-- Name: domain_application_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE domain_application_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE domain_application_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE domain_application_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE domain_application_id_seq TO frb_admin;


--
-- Name: domain_measurements; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE domain_measurements FROM PUBLIC;
REVOKE ALL ON TABLE domain_measurements FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE domain_measurements TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE domain_measurements TO frb_admin;


--
-- Name: domain_online_community_metrics_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE domain_online_community_metrics_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE domain_online_community_metrics_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE domain_online_community_metrics_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE domain_online_community_metrics_seq TO frb_admin;


--
-- Name: domains; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE domains FROM PUBLIC;
REVOKE ALL ON TABLE domains FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE domains TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE domains TO frb_admin;


--
-- Name: email_address_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE email_address_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE email_address_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE email_address_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE email_address_id_seq TO frb_admin;


--
-- Name: email_addresses; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE email_addresses FROM PUBLIC;
REVOKE ALL ON TABLE email_addresses FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE email_addresses TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE email_addresses TO frb_admin;


--
-- Name: events; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE events FROM PUBLIC;
REVOKE ALL ON TABLE events FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE events TO YOUR_USERNAME_HERE;
GRANT SELECT,INSERT,UPDATE ON TABLE events TO frb_admin;


--
-- Name: farmwork_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE farmwork_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE farmwork_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE farmwork_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE farmwork_id_seq TO frb_admin;


--
-- Name: goal_statements_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE goal_statements_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE goal_statements_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE goal_statements_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE goal_statements_id_seq TO frb_admin;


--
-- Name: goal_statements; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE goal_statements FROM PUBLIC;
REVOKE ALL ON TABLE goal_statements FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE goal_statements TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE goal_statements TO frb_admin;


--
-- Name: guest_pass_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE guest_pass_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE guest_pass_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE guest_pass_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE guest_pass_id_seq TO frb_admin;


--
-- Name: guest_passes; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE guest_passes FROM PUBLIC;
REVOKE ALL ON TABLE guest_passes FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE guest_passes TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE guest_passes TO frb_admin;


--
-- Name: harvests; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE harvests FROM PUBLIC;
REVOKE ALL ON TABLE harvests FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE harvests TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE harvests TO frb_admin;


--
-- Name: harvests_plant_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE harvests_plant_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE harvests_plant_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE harvests_plant_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE harvests_plant_id_seq TO frb_admin;


--
-- Name: host_applications; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE host_applications FROM PUBLIC;
REVOKE ALL ON TABLE host_applications FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE host_applications TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE host_applications TO frb_admin;


--
-- Name: host_databases; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE host_databases FROM PUBLIC;
REVOKE ALL ON TABLE host_databases FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE host_databases TO YOUR_USERNAME_HERE;
GRANT SELECT,INSERT,UPDATE ON TABLE host_databases TO frb_admin;


--
-- Name: host_email_addresses; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE host_email_addresses FROM PUBLIC;
REVOKE ALL ON TABLE host_email_addresses FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE host_email_addresses TO YOUR_USERNAME_HERE;


--
-- Name: hosts; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE hosts FROM PUBLIC;
REVOKE ALL ON TABLE hosts FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE hosts TO YOUR_USERNAME_HERE;
GRANT SELECT,INSERT,UPDATE ON TABLE hosts TO frb_admin;


--
-- Name: hyperlinks; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE hyperlinks FROM PUBLIC;
REVOKE ALL ON TABLE hyperlinks FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE hyperlinks TO YOUR_USERNAME_HERE;


--
-- Name: hyperlinks_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE hyperlinks_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE hyperlinks_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE hyperlinks_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE hyperlinks_id_sequence TO frb_admin;


--
-- Name: images; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE images FROM PUBLIC;
REVOKE ALL ON TABLE images FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE images TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE images TO frb_admin;


--
-- Name: indiegoals; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE indiegoals FROM PUBLIC;
REVOKE ALL ON TABLE indiegoals FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE indiegoals TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE indiegoals TO frb_admin;


--
-- Name: indiegoals_date_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE indiegoals_date_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE indiegoals_date_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_date_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_date_seq TO frb_admin;


--
-- Name: indiegoals_description_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE indiegoals_description_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE indiegoals_description_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_description_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_description_seq TO frb_admin;


--
-- Name: indiegoals_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE indiegoals_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE indiegoals_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_id_seq TO frb_admin;


--
-- Name: indiegoals_name_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE indiegoals_name_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE indiegoals_name_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_name_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE indiegoals_name_seq TO frb_admin;


--
-- Name: journals; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE journals FROM PUBLIC;
REVOKE ALL ON TABLE journals FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE journals TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE journals TO frb_admin;


--
-- Name: kernel_theories; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE kernel_theories FROM PUBLIC;
REVOKE ALL ON TABLE kernel_theories FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE kernel_theories TO YOUR_USERNAME_HERE;
GRANT SELECT,INSERT,UPDATE ON TABLE kernel_theories TO frb_admin;


--
-- Name: land_beds; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE land_beds FROM PUBLIC;
REVOKE ALL ON TABLE land_beds FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE land_beds TO YOUR_USERNAME_HERE;


--
-- Name: land_city_states; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE land_city_states FROM PUBLIC;
REVOKE ALL ON TABLE land_city_states FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE land_city_states TO YOUR_USERNAME_HERE;


--
-- Name: land_farmers; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE land_farmers FROM PUBLIC;
REVOKE ALL ON TABLE land_farmers FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE land_farmers TO YOUR_USERNAME_HERE;


--
-- Name: land_traits; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE land_traits FROM PUBLIC;
REVOKE ALL ON TABLE land_traits FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE land_traits TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE land_traits TO frb_admin;


--
-- Name: land_width_lengths; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE land_width_lengths FROM PUBLIC;
REVOKE ALL ON TABLE land_width_lengths FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE land_width_lengths TO YOUR_USERNAME_HERE;


--
-- Name: lands_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE lands_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE lands_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE lands_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE lands_id_sequence TO frb_admin;


--
-- Name: lands; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE lands FROM PUBLIC;
REVOKE ALL ON TABLE lands FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE lands TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE lands TO frb_admin;


--
-- Name: location_workdates; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE location_workdates FROM PUBLIC;
REVOKE ALL ON TABLE location_workdates FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE location_workdates TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE location_workdates TO frb_admin;


--
-- Name: machines; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE machines FROM PUBLIC;
REVOKE ALL ON TABLE machines FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE machines TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE machines TO frb_admin;


--
-- Name: manifest_maxonomies_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE manifest_maxonomies_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE manifest_maxonomies_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE manifest_maxonomies_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE manifest_maxonomies_sequence TO frb_admin;


--
-- Name: manmax_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE manmax_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE manmax_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE manmax_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE manmax_id_sequence TO frb_admin;


--
-- Name: material_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE material_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE material_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE material_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE material_id_seq TO frb_admin;


--
-- Name: maxonomy_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE maxonomy_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE maxonomy_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE maxonomy_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE maxonomy_id_sequence TO frb_admin;


--
-- Name: maxonomies; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE maxonomies FROM PUBLIC;
REVOKE ALL ON TABLE maxonomies FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE maxonomies TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE maxonomies TO frb_admin;


--
-- Name: moneymaker_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE moneymaker_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE moneymaker_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE moneymaker_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE moneymaker_id_seq TO frb_admin;


--
-- Name: moneymakers; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE moneymakers FROM PUBLIC;
REVOKE ALL ON TABLE moneymakers FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE moneymakers TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE moneymakers TO frb_admin;


--
-- Name: multifunction_dates_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE multifunction_dates_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE multifunction_dates_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE multifunction_dates_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE multifunction_dates_id_sequence TO frb_admin;


--
-- Name: plant_images_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE plant_images_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE plant_images_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_images_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_images_id_seq TO frb_admin;


--
-- Name: object_images; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE object_images FROM PUBLIC;
REVOKE ALL ON TABLE object_images FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE object_images TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE object_images TO frb_admin;


--
-- Name: observation_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE observation_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE observation_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE observation_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE observation_id_seq TO frb_admin;


--
-- Name: observations; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE observations FROM PUBLIC;
REVOKE ALL ON TABLE observations FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE observations TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE observations TO frb_admin;


--
-- Name: permaculture_topic_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE permaculture_topic_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE permaculture_topic_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE permaculture_topic_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE permaculture_topic_id_seq TO frb_admin;


--
-- Name: pickup_detail_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE pickup_detail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pickup_detail_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_detail_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_detail_id_seq TO frb_admin;


--
-- Name: pickup_details; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE pickup_details FROM PUBLIC;
REVOKE ALL ON TABLE pickup_details FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE pickup_details TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE pickup_details TO frb_admin;


--
-- Name: pickup_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE pickup_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pickup_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_id_seq TO frb_admin;


--
-- Name: pickup_plants_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE pickup_plants_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE pickup_plants_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_plants_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_plants_seq TO frb_admin;


--
-- Name: pickup_plants; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE pickup_plants FROM PUBLIC;
REVOKE ALL ON TABLE pickup_plants FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE pickup_plants TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE pickup_plants TO frb_admin;


--
-- Name: pickup_plants_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE pickup_plants_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE pickup_plants_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_plants_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE pickup_plants_sequence TO frb_admin;


--
-- Name: pickups; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE pickups FROM PUBLIC;
REVOKE ALL ON TABLE pickups FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE pickups TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE pickups TO frb_admin;


--
-- Name: plant_aliases; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_aliases FROM PUBLIC;
REVOKE ALL ON TABLE plant_aliases FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_aliases TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_aliases TO frb_admin;


--
-- Name: plant_categories; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_categories FROM PUBLIC;
REVOKE ALL ON TABLE plant_categories FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_categories TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_categories TO frb_admin;


--
-- Name: plant_families; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_families FROM PUBLIC;
REVOKE ALL ON TABLE plant_families FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_families TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_families TO frb_admin;


--
-- Name: plant_histories_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE plant_histories_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE plant_histories_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_histories_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_histories_id_sequence TO frb_admin;


--
-- Name: plant_histories; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_histories FROM PUBLIC;
REVOKE ALL ON TABLE plant_histories FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_histories TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_histories TO frb_admin;


--
-- Name: plant_history_events_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE plant_history_events_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE plant_history_events_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_history_events_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_history_events_id_seq TO frb_admin;


--
-- Name: plant_history_events; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_history_events FROM PUBLIC;
REVOKE ALL ON TABLE plant_history_events FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_history_events TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_history_events TO frb_admin;


--
-- Name: plant_list_plant_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE plant_list_plant_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE plant_list_plant_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_list_plant_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_list_plant_id_seq TO frb_admin;


--
-- Name: plant_list_plants; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_list_plants FROM PUBLIC;
REVOKE ALL ON TABLE plant_list_plants FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_list_plants TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_list_plants TO frb_admin;


--
-- Name: plant_list_projects; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_list_projects FROM PUBLIC;
REVOKE ALL ON TABLE plant_list_projects FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_list_projects TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_list_projects TO frb_admin;


--
-- Name: plant_lists_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE plant_lists_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE plant_lists_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_lists_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_lists_sequence TO frb_admin;


--
-- Name: plant_lists; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_lists FROM PUBLIC;
REVOKE ALL ON TABLE plant_lists FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_lists TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_lists TO frb_admin;


--
-- Name: plant_unit_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE plant_unit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE plant_unit_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_unit_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plant_unit_id_seq TO frb_admin;


--
-- Name: plant_units; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plant_units FROM PUBLIC;
REVOKE ALL ON TABLE plant_units FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_units TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plant_units TO frb_admin;


--
-- Name: plants_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE plants_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE plants_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plants_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE plants_id_sequence TO frb_admin;


--
-- Name: plants; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE plants FROM PUBLIC;
REVOKE ALL ON TABLE plants FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plants TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE plants TO frb_admin;


--
-- Name: postings_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE postings_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE postings_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE postings_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE postings_id_sequence TO frb_admin;


--
-- Name: postings; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE postings FROM PUBLIC;
REVOKE ALL ON TABLE postings FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE postings TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE postings TO frb_admin;


--
-- Name: potential_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE potential_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE potential_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE potential_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE potential_id_seq TO frb_admin;


--
-- Name: potentials; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE potentials FROM PUBLIC;
REVOKE ALL ON TABLE potentials FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE potentials TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE potentials TO frb_admin;


--
-- Name: prices; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE prices FROM PUBLIC;
REVOKE ALL ON TABLE prices FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE prices TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE prices TO frb_admin;


--
-- Name: process_flows; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE process_flows FROM PUBLIC;
REVOKE ALL ON TABLE process_flows FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE process_flows TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE process_flows TO frb_admin;


--
-- Name: process_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE process_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE process_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE process_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE process_id_sequence TO frb_admin;


--
-- Name: process_profiles; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE process_profiles FROM PUBLIC;
REVOKE ALL ON TABLE process_profiles FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE process_profiles TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE process_profiles TO frb_admin;


--
-- Name: processes; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE processes FROM PUBLIC;
REVOKE ALL ON TABLE processes FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE processes TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE processes TO frb_admin;


--
-- Name: project_book_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE project_book_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE project_book_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_book_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_book_id_seq TO frb_admin;


--
-- Name: project_books; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE project_books FROM PUBLIC;
REVOKE ALL ON TABLE project_books FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_books TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_books TO frb_admin;


--
-- Name: project_documentation_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE project_documentation_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE project_documentation_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_documentation_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_documentation_id_seq TO frb_admin;


--
-- Name: project_domain_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE project_domain_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE project_domain_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_domain_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_domain_seq TO frb_admin;


--
-- Name: project_land_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE project_land_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE project_land_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_land_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_land_id_seq TO frb_admin;


--
-- Name: project_projects; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE project_projects FROM PUBLIC;
REVOKE ALL ON TABLE project_projects FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_projects TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_projects TO frb_admin;


--
-- Name: project_seasons_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE project_seasons_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE project_seasons_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_seasons_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_seasons_id_sequence TO frb_admin;


--
-- Name: project_suppliers_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE project_suppliers_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE project_suppliers_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_suppliers_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_suppliers_id_seq TO frb_admin;


--
-- Name: project_suppliers; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE project_suppliers FROM PUBLIC;
REVOKE ALL ON TABLE project_suppliers FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_suppliers TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_suppliers TO frb_admin;


--
-- Name: project_tool_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE project_tool_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE project_tool_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_tool_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE project_tool_id_seq TO frb_admin;


--
-- Name: project_tools; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE project_tools FROM PUBLIC;
REVOKE ALL ON TABLE project_tools FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_tools TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE project_tools TO frb_admin;


--
-- Name: projects_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE projects_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE projects_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE projects_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE projects_id_sequence TO frb_admin;


--
-- Name: projects; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE projects FROM PUBLIC;
REVOKE ALL ON TABLE projects FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE projects TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE projects TO frb_admin;


--
-- Name: references_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE references_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE references_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE references_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE references_id_sequence TO frb_admin;


--
-- Name: responsibility_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE responsibility_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE responsibility_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE responsibility_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE responsibility_id_seq TO frb_admin;


--
-- Name: scene_element_account_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE scene_element_account_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE scene_element_account_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE scene_element_account_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE scene_element_account_id_seq TO frb_admin;


--
-- Name: scene_element_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE scene_element_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE scene_element_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE scene_element_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE scene_element_id_seq TO frb_admin;


--
-- Name: scene_element_responsibility_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE scene_element_responsibility_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE scene_element_responsibility_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE scene_element_responsibility_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE scene_element_responsibility_id_seq TO frb_admin;


--
-- Name: scene_elements; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE scene_elements FROM PUBLIC;
REVOKE ALL ON TABLE scene_elements FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE scene_elements TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE scene_elements TO frb_admin;


--
-- Name: seed_boxes_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE seed_boxes_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE seed_boxes_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seed_boxes_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seed_boxes_id_seq TO frb_admin;


--
-- Name: seed_packets_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE seed_packets_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE seed_packets_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seed_packets_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seed_packets_id_seq TO frb_admin;


--
-- Name: seed_packets; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE seed_packets FROM PUBLIC;
REVOKE ALL ON TABLE seed_packets FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE seed_packets TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE seed_packets TO frb_admin;


--
-- Name: seq_crop_plan_crops; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE seq_crop_plan_crops FROM PUBLIC;
REVOKE ALL ON SEQUENCE seq_crop_plan_crops FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_crop_plan_crops TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_crop_plan_crops TO frb_admin;


--
-- Name: seq_field_id; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE seq_field_id FROM PUBLIC;
REVOKE ALL ON SEQUENCE seq_field_id FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_field_id TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_field_id TO frb_admin;


--
-- Name: seq_greenhouse_plan_seedings_id; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE seq_greenhouse_plan_seedings_id FROM PUBLIC;
REVOKE ALL ON SEQUENCE seq_greenhouse_plan_seedings_id FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_greenhouse_plan_seedings_id TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_greenhouse_plan_seedings_id TO frb_admin;


--
-- Name: seq_stakeholder_id; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE seq_stakeholder_id FROM PUBLIC;
REVOKE ALL ON SEQUENCE seq_stakeholder_id FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_stakeholder_id TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE seq_stakeholder_id TO frb_admin;


--
-- Name: shares_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE shares_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shares_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE shares_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE shares_id_seq TO frb_admin;


--
-- Name: shares; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE shares FROM PUBLIC;
REVOKE ALL ON TABLE shares FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE shares TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE shares TO frb_admin;


--
-- Name: shift_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE shift_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shift_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE shift_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE shift_id_seq TO frb_admin;


--
-- Name: shifts; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE shifts FROM PUBLIC;
REVOKE ALL ON TABLE shifts FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE shifts TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE shifts TO frb_admin;


--
-- Name: shifts_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE shifts_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE shifts_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE shifts_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE shifts_id_sequence TO frb_admin;


--
-- Name: soil_areas; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE soil_areas FROM PUBLIC;
REVOKE ALL ON TABLE soil_areas FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE soil_areas TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE soil_areas TO frb_admin;


--
-- Name: soil_areas_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE soil_areas_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE soil_areas_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE soil_areas_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE soil_areas_id_seq TO frb_admin;


--
-- Name: soil_tests_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE soil_tests_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE soil_tests_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE soil_tests_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE soil_tests_sequence TO frb_admin;


--
-- Name: song_id_pkey; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE song_id_pkey FROM PUBLIC;
REVOKE ALL ON SEQUENCE song_id_pkey FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE song_id_pkey TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE song_id_pkey TO frb_admin;


--
-- Name: song_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE song_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE song_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE song_id_seq TO YOUR_USERNAME_HERE;


--
-- Name: songs; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE songs FROM PUBLIC;
REVOKE ALL ON TABLE songs FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE songs TO YOUR_USERNAME_HERE;


--
-- Name: spacing_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE spacing_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE spacing_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE spacing_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE spacing_id_seq TO frb_admin;


--
-- Name: spacings; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE spacings FROM PUBLIC;
REVOKE ALL ON TABLE spacings FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE spacings TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE spacings TO frb_admin;


--
-- Name: storage_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE storage_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE storage_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE storage_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE storage_id_seq TO frb_admin;


--
-- Name: storages; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE storages FROM PUBLIC;
REVOKE ALL ON TABLE storages FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE storages TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE storages TO frb_admin;


--
-- Name: suppliers_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE suppliers_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE suppliers_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE suppliers_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE suppliers_id_sequence TO frb_admin;


--
-- Name: suppliers; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE suppliers FROM PUBLIC;
REVOKE ALL ON TABLE suppliers FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE suppliers TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE suppliers TO frb_admin;


--
-- Name: table_definition_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE table_definition_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE table_definition_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE table_definition_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE table_definition_id_seq TO frb_admin;


--
-- Name: tag_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE tag_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE tag_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE tag_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE tag_id_seq TO frb_admin;


--
-- Name: tags; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE tags FROM PUBLIC;
REVOKE ALL ON TABLE tags FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE tags TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE tags TO frb_admin;


--
-- Name: tenperdays; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE tenperdays FROM PUBLIC;
REVOKE ALL ON TABLE tenperdays FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE tenperdays TO YOUR_USERNAME_HERE;


--
-- Name: test_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE test_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE test_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE test_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE test_id_sequence TO frb_admin;


--
-- Name: ticket_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE ticket_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE ticket_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE ticket_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE ticket_id_seq TO frb_admin;


--
-- Name: tickets; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE tickets FROM PUBLIC;
REVOKE ALL ON TABLE tickets FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE tickets TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE tickets TO frb_admin;


--
-- Name: timecard_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE timecard_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE timecard_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE timecard_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE timecard_id_seq TO frb_admin;


--
-- Name: timecards; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE timecards FROM PUBLIC;
REVOKE ALL ON TABLE timecards FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE timecards TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE timecards TO frb_admin;


--
-- Name: tools_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE tools_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE tools_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE tools_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE tools_id_sequence TO frb_admin;


--
-- Name: tools; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE tools FROM PUBLIC;
REVOKE ALL ON TABLE tools FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE tools TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE tools TO frb_admin;


--
-- Name: units; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE units FROM PUBLIC;
REVOKE ALL ON TABLE units FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE units TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE units TO frb_admin;


--
-- Name: usernames; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE usernames FROM PUBLIC;
REVOKE ALL ON TABLE usernames FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE usernames TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE usernames TO frb_admin;


--
-- Name: value_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE value_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE value_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE value_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE value_id_seq TO frb_admin;


--
-- Name: varieties_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE varieties_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE varieties_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE varieties_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE varieties_id_sequence TO frb_admin;


--
-- Name: varieties; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE varieties FROM PUBLIC;
REVOKE ALL ON TABLE varieties FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE varieties TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE varieties TO frb_admin;


--
-- Name: visit_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE visit_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE visit_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE visit_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE visit_id_seq TO frb_admin;


--
-- Name: visits; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE visits FROM PUBLIC;
REVOKE ALL ON TABLE visits FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE visits TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE visits TO frb_admin;


--
-- Name: visits_id_sequence; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE visits_id_sequence FROM PUBLIC;
REVOKE ALL ON SEQUENCE visits_id_sequence FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE visits_id_sequence TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE visits_id_sequence TO frb_admin;


--
-- Name: webmaster_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE webmaster_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE webmaster_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webmaster_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webmaster_id_seq TO frb_admin;


--
-- Name: webmasters; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE webmasters FROM PUBLIC;
REVOKE ALL ON TABLE webmasters FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webmasters TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webmasters TO frb_admin;


--
-- Name: webpage_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE webpage_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE webpage_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webpage_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webpage_id_seq TO frb_admin;


--
-- Name: webpage_maxonomies; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE webpage_maxonomies FROM PUBLIC;
REVOKE ALL ON TABLE webpage_maxonomies FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpage_maxonomies TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpage_maxonomies TO frb_admin;


--
-- Name: webpage_moneymaker_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE webpage_moneymaker_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE webpage_moneymaker_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webpage_moneymaker_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webpage_moneymaker_id_seq TO frb_admin;


--
-- Name: webpage_moneymakers; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE webpage_moneymakers FROM PUBLIC;
REVOKE ALL ON TABLE webpage_moneymakers FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpage_moneymakers TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpage_moneymakers TO frb_admin;


--
-- Name: webpage_webpage_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE webpage_webpage_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE webpage_webpage_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webpage_webpage_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE webpage_webpage_id_seq TO frb_admin;


--
-- Name: webpage_webpages; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE webpage_webpages FROM PUBLIC;
REVOKE ALL ON TABLE webpage_webpages FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpage_webpages TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpage_webpages TO frb_admin;


--
-- Name: webpages; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE webpages FROM PUBLIC;
REVOKE ALL ON TABLE webpages FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpages TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE webpages TO frb_admin;


--
-- Name: whoihelps_id_seq; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON SEQUENCE whoihelps_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE whoihelps_id_seq FROM YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE whoihelps_id_seq TO YOUR_USERNAME_HERE;
GRANT ALL ON SEQUENCE whoihelps_id_seq TO frb_admin;


--
-- Name: yields; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE yields FROM PUBLIC;
REVOKE ALL ON TABLE yields FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE yields TO YOUR_USERNAME_HERE;
GRANT ALL ON TABLE yields TO frb_admin;


--
-- Name: zachmans; Type: ACL; Schema: public; Owner: YOUR_USERNAME_HERE
--

REVOKE ALL ON TABLE zachmans FROM PUBLIC;
REVOKE ALL ON TABLE zachmans FROM YOUR_USERNAME_HERE;
GRANT ALL ON TABLE zachmans TO YOUR_USERNAME_HERE;


--
-- PostgreSQL database dump complete
--

