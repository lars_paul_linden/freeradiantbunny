<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05

// about this class
// This class models a field of an HTML form

class FormField {

  // attributes
  private $name;
  private $ready_to_insert;
  private $value_system_generated;
  private $value_default;
  private $value_from_user;
  private $input_type;
  private $validation_type;
  private $value_to_insert;

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // ready_to_insert
  public function set_ready_to_insert($var) {
    $this->ready_to_insert = $var;
  }
  public function get_ready_to_insert() {
    return $this->ready_to_insert;
  }

  // value_system_generated
  public function set_value_system_generated($var) {
    $this->value_system_generated = $var;
  }
  public function get_value_system_generated() {
    return $this->value_system_generated;
  }

  // value_default
  public function set_value_default($var) {
    $this->value_default = $var;
  }
  public function get_value_default() {
    return $this->value_default;
  }

  // value_from_user
  public function set_value_from_user($var) {
    $this->value_from_user = $var;
  }
  public function get_value_from_user() {
    return $this->value_from_user;
  }

  // input_type
  public function set_input_type($var) {
    $this->input_type = $var;
  }
  public function get_input_type() {
    return $this->input_type;
  }
 
  // validation_type
  public function set_validation_type($var) {
    $this->validation_type = $var;
  }
  public function get_validation_type() {
    return $this->validation_type;
  }

  // value_to_insert
  public function set_value_to_insert($var) {
    $this->value_to_insert = $var;
  }
  public function get_value_to_insert() {
    return $this->value_to_insert;
  }

  // method
  public function get_input_row($given_user_obj) {
    $markup = "";

    $name = $this->get_name();
    $error_message = "";

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    " . $name . "\n";
    $markup .= "  </td>\n";
    if ($this->get_value_system_generated()) {
      // value_system_generated has precedent over the other values
      $this->set_ready_to_insert("yes");
      $markup .= "  <td>\n";
      $value = $this->get_value_system_generated();
      $markup .= "    " . $value . "\n";
      $this->set_value_to_insert($value);
      $markup .= "  </td>\n";
    } else {

      $value = "";
      if (! isset($_POST["iteration-count"])) {
        // value_default is used if the first displaying of the form
        $value = $this->get_value_default();
      }

      // convert the database field name into an HTML attribute format
      $parameter = $this->convert_to_attribute($name);

      // set default
      $css_class = "";

      // get iteration count
      if (isset($_POST[$parameter])) {

        // validate
        include_once("validator.php");
        $validator_obj = new Validator();
        $error_message = $validator_obj->validate_form_field($parameter, $this->get_validation_type());

        //print "debug resulting error_message " . $error_message . "<br />";

        // deal with the results of the validator above
        if ($error_message) {
          $value = "";
          $css_class = "error";
          // todo fix the following so the form is not blanked
        } else {
          // ok, so use it
          $value = $_POST[$parameter];
          $this->set_ready_to_insert("yes");
          $this->set_value_to_insert($value);
        }
      }

      // output the form field element (e.g. input, textarea, etc.)
      $markup .= "  <td class=\"" . $css_class . "\">\n";
      if ($this->get_input_type() == "select") {
        if ($this->get_name() == "plant_id") {
          include_once("plants.php");
          $obj = new Plants();
          $default_id = $value;
          $markup .= $obj->output_select_list($parameter, $default_id);
        } else if ($this->get_name() == "supplier_id") {
          include_once("suppliers.php");
          $obj = new Suppliers();
          $default_id = $value;
          $obj->set_user_obj($given_user_obj);
          $markup .= $obj->output_select_list($parameter, $default_id);
        } else if ($this->get_name() == "side") {
          include_once("products.php");
          $obj = new Products();
          $default_id = $value;
          $markup .= $obj->output_select_list_side($parameter, $default_id);
        } else if ($this->get_name() == "unit_id") {
          include_once("units.php");
          $obj = new Units();
          $default_id = $value;
          $markup .= $obj->output_select_list($parameter, $default_id);
        } else if ($this->get_name() == "project_id") {
          include_once("projects.php");
          $obj = new Projects();
          $default_id = $value;
          $obj->set_user_obj($given_user_obj);
          $markup .= $obj->output_select_list($parameter, $default_id);
        } else if ($this->get_name() == "process_id") {
          include_once("processes.php");
          $obj = new Processess();
          $default_id = $value;
          $obj->set_user_obj($given_user_obj);
          $markup .= $obj->output_select_list($parameter, $default_id);
        } else {
          print "Error: no select for this parameter name<br />\n";
        }
      } else if ($this->get_input_type() == "textarea") {
        $markup .= "    <textarea name=\"" . $parameter . "\" cols=\"20\">";
        $markup .= $value;
        $this->set_value_to_insert($value);
        $markup .= "</textarea>\n";
      } else {
         // todo make the size of textbox more flexable
         $markup .= "    <input type=\"text\" name=\"" . $parameter . "\" value=\"" . $value . "\" size=\"50\" />\n";
       $this->set_value_to_insert($value);
      }
      $markup .= "  </td>\n";
    }
    if ($error_message) {
      $markup .= "  <td class=\"" . $css_class . "\">\n";
      $markup .= "    " . $error_message . "\n";
      $markup .= "  </td>\n";
    }
    $markup .= "</tr>\n";

    return $markup;
  }

  // method
  private function convert_to_attribute($given_name) {
    // so that the field names can be used as html form parameters
    // convert underscores to hyphens
    $search = "_";
    $replace = "-";
    return str_replace($search, $replace, $given_name);
  }

  // method for form
  // todo this is old code
  public function print_radio_button_list($name) {
    $markup = "";

    // parameter "name" is the attribute value for the input element
    $type = "get_goal_statements_list_alphabetical";
    $this->get_db_dash()->load($this, $type);
    $markup .= "<table border=\"1\" cellpadding=\"6\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td valign=\"top\">\n";
    $count = -1;
    foreach ($this->get_list_bliss()->get_list() as $goal_statement) {
      $count++;
      if ($count == 16) {
        $count = 0;
        $markup .= "  </td>\n";
        $markup .= "  <td valign=\"top\">\n";
      }
      $markup .= "    <input type=\"radio\" name=\"" . $name . "\" value=\"" . $goal_statement->get_id() . "\" />" . $goal_statement->get_common_name() . "<br />\n";
    }
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method for form
  // todo this is old code
  public function print_select_list($name, $default_goal_statement_id) {
    $markup = "";;

    // parameter "name" is the attribute value for the input element

    $type = "get_goal_statements_list_alphabetical";
    $this->get_db_dash()->load($this, $type);

    $markup .= "    <select name=\"" . $name . "\">\n";
    foreach ($this->get_list_bliss()->get_list() as $goal_statement) {
      $markup .= "      <option value=\"" . $goal_statement->get_id() . "\"";
      if ($default_goal_statement_id) {
        if ($default_goal_statement_id == $goal_statement->get_id()) {
          $markup .= " selected=\"selected\"";
        }
      }
      $markup .= ">" . $goal_statement->get_common_name() . "</option>\n";
    }
    $markup .= "    </select>\n";

    return $markup;
  }

}
