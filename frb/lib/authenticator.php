<?php

// FreeRadiantBunny
// Copyright (C) 2015 Lars Paul Linden
// see README.txt

// log
// version 1.4 2015-03-15

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/lib/authenticator.php

class Authenticator {

  function  __construct($given_config) {
    $this->set_given_config($given_config);
  }

  // given
  private $given_config;

  // given_config
  public function set_given_config($var) {
    $this->given_config = $var;
  }
  public function get_given_config() {
    return $this->given_config;
  }

  // given_username
  // note this never stores the value
  public function get_given_username() {
    // get parameters from user
    if (isset($_POST['username']) && $_POST['username']) {
      if (ctype_alpha($_POST['username'])) {
        if (strlen($_POST['username']) < 20) {
          // debug
          //print "debug authenticator given_username = " . $_POST['username'] . "<br />\n";
          return $_POST['username'];
        } else {
          // debug
          //print "debug authenticator given_username is too long.<br />\n";
        }
      } else {
        // debug
        //print "debug authenticator given_username is not all alpha chararacters.<br />\n";
      }
    } else {
      // debug
      //print "debug authenticator given_username is not given.<br />\n";
    }

    return "";
  }

  // given_password_as_hash
  public function get_given_password_as_hash() {
    if (isset($_POST['password']) && $_POST['password']) {
      if ($this->has_only_password_characters($_POST['password'])) {
        if (strlen($_POST['password']) < 500) {
          // debug
          //print "debug authenticator given_password = " . $_POST['password'] . "<br />\n";
          // hash will encrypt
          // debug (are you sure you want to do this one?)
          //print "debug authenticator password = " . md5($_POST['password']) . "<br />\n";
          return md5($_POST['password']);
        } else {
          // debug
          //print "debug authenticator given_password is too long.<br />\n";
        }
      } else {
        // debug
        //print "debug authenticator given_password is not all alpha chararacters.<br />\n";
      }
    } else {
        // debug
        //print "debug authenticator given_password is not given.<br />\n";
    }

    return "";
  }

  // username_obj
  public function get_username_obj() {
    if (! isset($this->username_obj)) {
      include_once("freeradiantbunny/frb/usernames.php");
      $this->username_obj = new Usernames($this->get_given_config());
    }
    return $this->username_obj;
  }

  // method
  public function output_login_form() {
    $markup = "";

    if (isset($_GET['login'])) {
      // output
      $markup .= "<form style=\"margin: 20px 20px 20px 20px;\" method=\"post\" action=\"index.php\">\n";
      $markup .= "  <div style=\"margin-bottom: 0px;\">username: <input type=\"text\" name=\"username\"></div><br />\n";
      $markup .= "  <div style=\"margin-bottom: 0px;\">password: <input type=\"password\" name=\"password\"></div><br />\n";
        $markup .= "  <div style=\"margin-left: 160px;\"><input type=\"submit\" value=\"Sign in\"></div><br />\n";
      $markup .= "</form>\n";
    } else {
      // sign in hyperlink
      $markup .=  "<a href=\"index.php?login=1\">Sign in</a><br />\n";
    }

    return $markup;
  }

  // method
  public function authenticate_via_db() {
    $markup = "";

    // debug
    $markup .= "debug authenticator: authenticate_via_db()<br />\n";

    $given_username = $this->get_given_username();
    $given_password_as_hash = $this->get_given_password_as_hash();

    // note username_obj is used to store the auth status of current user
    if ($given_username && $given_password_as_hash) { 
      // debug
      $markup .= "debug authenticator: has given_username and given_password_as_hash<br />\n";
      // user provided credentials, so authenticate
      if ($this->get_username_obj()->authenticate_via_db($given_username, $given_password_as_hash)) {
        // debug
        $markup .= "debug authenticator: successfully authenticated_via_db<br />\n";
      } else {
        // debug
        $markup .= "debug authenticator: failed to authenticated_via_db<br />\n";
      }

    } else {
      // no given_username or no given_password_as_hash, so no db check
        $markup .= "debug authenticator given_username not set or given_password not set, no authenticate can happen<br />\n";
    }

    return $markup;
  }
  
  // method
  public function output_menu() {
    $markup = "";

    // like a toggle
    // output a link to entrance or to exit
    if ($this->get_username_obj()->get_authenticated()) {
      // user is authenticated, so show link to profile page and exit
      // sign out hyperlink
      $base_url = $this->get_given_config()->get_base_url();
      $url = $base_url . "usernames/" . $this->get_username_obj()->get_authenticated()->get_username();
      $markup .= "<a href=\"" . $url . "\">" . $this->get_username_obj()->get_authenticated()->get_username() . "</a>";
      $markup .= "&nbsp;|&nbsp;";
      $url = $base_url . "index.php?action=logout";
      $markup .= "<a href=\"" . $url . "\">Sign out</a><br />\n";
    } else {
      if (isset($_GET['login'])) {
        // show form
        // nothing here
      } else {
        // sign in hyperlink
        $markup .=  "<a href=\"index.php?login=1\">Sign in</a><br />\n";
      }
    }

    return $markup;
  }

  // method
  public function has_only_password_characters($user_input) {
    $var = "";

    // note: the following is like a double-negative
    if (! preg_match("/[^a-zA-Z0-9_]/", $user_input)) {
      $var = $user_input;
    }
    return $var;
  }

  // method
  public function check_for_valid_cookie() {
    return $this->get_username_obj()->check_for_valid_cookie();
  }

  // method
  public function is_valid_cookie() {
    return $this->get_username_obj()->is_valid_cookie();
  }

  // method
  public function is_user_requesting_logout_command() {
    return $this->get_username_obj()->is_user_requesting_logout_command();
  }

  // method
  public function do_logout_command() {
    $this->get_username_obj()->do_logout_command();
  }

}
