<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/pickups.php

include_once("lib/scrubber.php");

class Pickups extends Scrubber {

  // given
  private $given_invoice_line_id;
  private $given_pickup_detail_id;

  // given_invoice_line_id
  public function set_given_invoice_line_id($var) {
    $this->given_invoice_line_id = $var;
  }
  public function get_given_invoice_line_id() {
    return $this->given_invoice_line_id;
  }

  // given_pickup_detail_id
  public function set_given_pickup_detail_id($var) {
    $this->given_pickup_detail_id = $var;
  }
  public function get_given_pickup_detail_id() {
    return $this->given_pickup_detail_id;
  }

  // attributes
  private $id;
  private $pickup_detail_obj;
  private $invoice_line_obj;
  private $delivery_stop_flag;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // pickup_detail_obj
  public function get_pickup_detail_obj() {
    if (! isset($this->pickup_detail_obj)) {
      include_once("pickup_details.php");
      $this->pickup_detail_obj = new PickupDetails($this->get_given_config());
    }
    return $this->pickup_detail_obj;
  }

  // invoice_line_obj
  public function get_invoice_line_obj() {
    if (! isset($this->invoice_line_obj)) {
      include_once("invoice_lines.php");
      $this->invoice_line_obj = new InvoiceLines($this->get_given_config());
    }
    return $this->invoice_line_obj;
  }

  // delivery_stop_flag
  public function set_delivery_stop_flag($var) {
    $this->delivery_stop_flag = $var;
  }
  public function get_delivery_stop_flag() {
    return $this->delivery_stop_flag;
  }

  // method
  private function make_pickup() {
    $obj = new Pickups($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");
 
    } else if ($this->get_given_invoice_line_id()) {
      $this->set_type("get_by_invoice_line_id");

    } else if ($this->get_given_pickup_detail_id()) {
      $this->set_type("get_by_pickup_detail_id");

    } else {
      // default
      $this->set_type("get_all");
    }

    //print "debug pickups type " . $this->get_type() . "<br />";
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_pickup_detail_id") {
      $sql = "SELECT pickups.* FROM pickups WHERE pickups.pickup_detail_id = " . $this->get_given_pickup_detail_id() . " ORDER BY pickups.id;";

    } else if ($this->get_type() == "get_by_invoice_line_id") {
      $sql = "SELECT pickups.* FROM pickups WHERE pickups.invoice_line_id = " . $this->get_given_invoice_line_id() . " ORDER BY pickups.id;";

    } else if ($this->get_type() == "get_by_id") {
      $sql = "SELECT pickups.* FROM pickups WHERE pickups.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT pickups.* FROM pickups ORDER BY pickups.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_invoice_line_id" ||
        $this->get_type() == "get_by_pickup_detail_id" ||
        $this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $pickup= $this->make_pickup();
        $pickup->set_id(pg_result($results, $lt, 0));
        $pickup->get_pickup_detail_obj()->set_id(pg_result($results, $lt, 1));
        $pickup->get_invoice_line_obj()->set_id(pg_result($results, $lt, 2));
        $pickup->set_delivery_stop_flag(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list

    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup_detail\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup plants count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    details\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    delivery_stop_flag\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $pickup) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup->get_pickup_detail_obj()->get_id() . "\n";
      $markup .= "  </td>\n";

      // pickup_plants count
      $markup .= "  <td style=\"text-align: right;\">\n";

      include_once("pickup_plants.php");
      $pickup_plants_obj = new PickupPlants($this->get_given_config());
      $url = $this->url("pickup_plants/pickup/" . $pickup->get_id());
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= $pickup_plants_obj->output_count_given_pickup_id($pickup->get_id());
      $markup .= "</a>\n";
      $markup .= "  </td>\n";

      // pickup_details
      $markup .= "  <td style=\"text-align: right;\">\n";
      include_once("pickup_details.php");
      $pickup_details_obj = new PickupDetails($this->get_given_config());
      $markup .= $pickup_details_obj->output_table_given_pickup_id($pickup->get_id());
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    // sum of yields
    //$markup .= "<tr>\n";
    //$markup .= "  <td colspan=\"3\">\n";
    //$markup .= "    sum of yields";
    //$markup .= "  </td>\n";
    //$markup .= "  <td>\n";
    //include_once("pickup_plants.php");
    //$pickup_plants_obj = new PickupPlants($this->get_given_config());
    //$pickup_plants_obj->set_given_pickup_id($pickup->get_id());
    //$markup .= $pickup_plants_obj->output_sum_of_yields($tally_yield_needed);
    //$markup .= "  </td>\n";
    //$markup .= "</tr>\n";

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  private function output_one_plant_view() {
    $markup = "";

    // get needed data
    // pickup_details
    include_once("pickup_details.php");
    $pickup_detail_obj = new Pickup_Details($this->get_given_config());
    $given_user_obj = $this->get_user_obj();
    $shares_estimate = $pickup_detail_obj->get_shares_estimate_given_plant_list_id($this->get_given_plant_list_id(), $given_user_obj);

    // guts of the list

    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup quantity<br />per share\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    shares\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup quantity<br />all shares\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $tally_quantity = 0;
    foreach ($this->get_list_bliss()->get_list() as $pickup) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td style=\"text-align: right;\">\n";
      $markup .= "    " . $pickup->get_extra_pickup_plant_obj()->get_plant_obj()->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td style=\"text-align: right;\">\n";
      $markup .= "    " . $pickup->get_extra_pickup_plant_obj()->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td style=\"text-align: right;\">\n";
      $markup .= "    " . $pickup->get_extra_pickup_plant_obj()->get_quantity() . "\n";
      $tally_quantity += $pickup->get_extra_pickup_plant_obj()->get_quantity();
      $markup .= "  </td>\n";

      $markup .= "  <td style=\"text-align: right;\">\n";
      $markup .= "    " . $shares_estimate . "\n";
      $markup .= "  </td>\n";

      $pickup_quantity_all_shares = $pickup->get_extra_pickup_plant_obj()->get_quantity() * $shares_estimate;
      $markup .= "  <td style=\"text-align: right;\">\n";
      $markup .= "    " . $pickup_quantity_all_shares . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    // totals
    $markup .= "<tr>\n";
    $markup .= "<td colspan=\"4\">\n";
    $markup .= "  totals\n";
    $markup .= "</td>\n";
    $markup .= "<td style=\"text-align: right;\">\n";
    $markup .= "   " . $tally_quantity;
    $markup .= "</td>\n";
    $markup .= "<td style=\"text-align: right;\">\n";
    $markup .= "   " . $shares_estimate;
    $markup .= "</td>\n";
    $markup .= "<td style=\"text-align: right;\">\n";
    $total = $tally_quantity * $shares_estimate;
    $markup .= "   " . $total;
    $markup .= "</td>\n";
    $markup .= "</tr>\n";
    $markup .= "</table>\n";


    return $markup;
  }

  // method
  public function output_list_table_by_crop() {
    $markup = "";

    // store the data here
    include_once("crop_date_matrix.php");
    $crop_date_matrix_obj = new CropDateMatrix($this->get_given_config());

    $markup .= "Error: need to fix in pickups.php";

    // get the data
    //include_once("pickup_plants.php");
    //foreach ($this->get_list_bliss()->get_list() as $pickup) {
      //$date = $pickup->get_date();
      //$pickup_plants_obj = new PickupPlants($this->get_given_config());
      //$pickup_plants_obj->set_given_pickup_id($pickup->get_id());
      //$pickup_plants_obj->load_matrix($date, $crop_date_matrix_obj);
    //}
    //$markup .= $crop_date_matrix_obj->output_table();

    return $markup;
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_pickup_detail_id()) {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "    These pickups are of pickup_detail id = " . $this->get_given_pickup_detail_id() . "\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }
  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<p><em>id = " . $this->get_id() . "</em></p>\n";
    $markup .= "<p><em>delivery_stop_flag = " . $this->get_delivery_stop_flag() . "</em></p>\n";

    $markup .= "<p><em>error: need to list dates</em></p>\n";

    include_once("pickup_plants.php");
    $pickup_plants_obj = new PickupPlants($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $pickup_id = $this->get_id();
    $markup .= $pickup_plants_obj->output_weekly_share_list($pickup_id, $user_obj);
;

    return $markup;
  }

  // method
  public function get_list_quick_description() {
    $markup = "";

    $list_count = $this->get_extra_pickup_plants_list_obj()->get_count($this->get_id());
    if ($list_count > 0) {
      $markup .= "<a href=\"pickup_plants?pickup_id=" . $this->get_id() . "\">";
    }
    if ($list_count == 1) {
      // singular
      $markup .= $list_count . " plant\n";
    } else {
      // plural
      $markup .= $list_count . " plants\n";
    }
    if ($list_count > 0) {
      $markup .= "</a>\n";
    }
    return $markup;
  }

  // method
  public function get_array_given_pickup_detail_id($given_pickup_detail_id) {
    $markup = "";

    $this->set_given_pickup_detail_id($given_pickup_detail_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // check for errors
    if ($markup) {
      return $markup;
    }

    // only output if there are items to output
    $pickup_special_array = array();
    if ($this->get_list_bliss()->get_count() > 0) {

      foreach ($this->get_list_bliss()->get_list() as $pickup) {

        include_once("pickup_plants.php");
        $pickup_plants_obj = new PickupPlants($this->get_given_config());
        $list_obj = $pickup_plants_obj->output_weekly_share_list_bliss($pickup->get_id(), $this->get_user_obj());

        foreach ($list_obj as $pickup_plant_obj) { 
          $plant_id = $pickup_plant_obj->get_plant_obj()->get_id();
          //print "debug pickups $plant_id<br />";
          $pickup_plant_obj->set_pickup_obj($pickup);

          if (array_key_exists($plant_id, $pickup_special_array)) {
            $pickup_list = $pickup_special_array[$plant_id];
            array_push($pickup_list, $pickup_plant_obj);
            $pickup_special_array[$plant_id] = $pickup_list;
          } else {
            //print "debug first $plant_id<br />";
            $pickup_list = array();
            array_push($pickup_list, $pickup_plant_obj);
            $pickup_special_array[$plant_id] = $pickup_list;
          }
        }
      }
    }
    return $pickup_special_array;;
  }

  // method
  public function get_array_given_project_id($given_project_id) {
    $markup = "";

    $this->set_given_project_id($given_project_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // check for errors
    if ($markup) {
      return $markup;
    }

    // only output if there are items to output
    $pickup_special_array = array();
    if ($this->get_list_bliss()->get_count() > 0) {

      //print "debug pickups project_id = " . $this->get_given_project_id();

      foreach ($this->get_list_bliss()->get_list() as $pickup) {

        include_once("pickup_plants.php");
        $pickup_plants_obj = new PickupPlants($this->get_given_config());
        $list_obj = $pickup_plants_obj->output_weekly_share_list_bliss($pickup->get_id(), $this->get_user_obj());

        foreach ($list_obj as $pickup_plant_obj) { 
          $plant_id = $pickup_plant_obj->get_plant_obj()->get_id();
          //print "debug pickups $plant_id<br />";
          $pickup_plant_obj->set_pickup_obj($pickup);

          if (array_key_exists($plant_id, $pickup_special_array)) {
            $pickup_list = $pickup_special_array[$plant_id];
            array_push($pickup_list, $pickup_plant_obj);
            $pickup_special_array[$plant_id] = $pickup_list;
          } else {
            //print "debug first $plant_id<br />";
            $pickup_list = array();
            array_push($pickup_list, $pickup_plant_obj);
            $pickup_special_array[$plant_id] = $pickup_list;
          }
        }
      }
    }
    return $pickup_special_array;;
  }

  // method
  public function get_invoices_given_pickup_detail_id($given_pickup_detail_id, $given_user_obj) {
    $markup = "";

    $this->set_given_pickup_detail_id($given_pickup_detail_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // check for errors
    if ($markup) {
      return $markup;
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      foreach ($this->get_list_bliss()->get_list() as $pickup) {

        $markup .= $pickup->get_invoice_line_obj()->get_quick_invoice();

      }

    }

    return $markup;
  }


  // method
  public function get_pickups_given_invoice_line_id($given_invoice_line_id, $given_user_obj) {
    $markup = "";

    $this->set_given_invoice_line_id($given_invoice_line_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // check for errors
    if ($markup) {
      return $markup;
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 0) {
      $markup .= "No pickups.\n";
      return $markup;
    }

    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup_details\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    delivery_stop\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";


    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $pickup) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td style=\"width: 500px;\">\n";
      $markup .= $pickup->get_pickup_detail_obj()->get_pickup_details();
      $markup .= "  </td>\n";

      // delivery_stop
      $delivery_stop_flag = $pickup->get_delivery_stop_flag();
      if ($delivery_stop_flag) {
        $markup .= "  <td style=\"text-align: center; background-color: #CD5555;\">\n";
      } else {
        $markup .= "  <td style=\"text-align: center;\">\n";
      }
      $markup .= "    " . $delivery_stop_flag . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

}
