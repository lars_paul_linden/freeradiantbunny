<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2013-04-02
// version 1.6 2016-05-07
// version 1.7 2017-01-24

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/tag_elements.php

include_once("class_standard.php");

class TagElements extends Standard {

  // attributes
  private $tag_obj;
  private $database_string;
  private $class_name_string;
  private $class_primary_key_string;

  // tag_obj
  public function get_tag_obj() {
    if (! isset($this->tag_obj)) {
      include_once("tags.php");
      $this->tag_obj = new Tags($this->get_given_config());
    }
    return $this->tag_obj;
  }

  // database_string
  public function set_database_string($var) {
    $this->database_string = $var;
  }
  public function get_database_string() {
    return $this->database_string;
  }

  // class_name_string
  public function set_class_name_string($var) {
    $this->class_name_string = $var;
  }
  public function get_class_name_string() {
    return $this->class_name_string;
  }

  // class_primary_key_string
  public function set_class_primary_key_string($var) {
    $this->class_primary_key_string = $var;
  }
  public function get_class_primary_key_string() {
    return $this->class_primary_key_string;
  }

  // method
  private function make_tag_element() {
    $obj = new TagElements($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

}
