<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-17
// version 1.6 2017-01-28

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/hyperlinks.php

include_once("lib/standard.php");

class Hyperlinks extends Standard {

  // given
  private $given_project_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given
  private $given_is_home = "";
  private $given_id_of_category;
  private $given_category_id;
  private $given_count;

  // given_is_home
  public function set_given_is_home($is_home) {
    $this->given_is_home = $is_home;
  }
  public function get_given_is_home() {
    return $this->given_is_home;
  }

  // given_id_of_category
  public function set_given_id_of_category($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "id_of_category");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_id_of_category = $var;
    }
  }
  public function get_given_id_of_category() {
    return $this->given_id_of_category;
  }

  // given_category_id
  public function set_given_category_id($category_id) {
    $this->given_category_id = $category_id;
  }
  public function get_given_category_id() {
    return $this->given_category_id;
  }

  // given_count
  public function set_given_count($count) {
    $this->given_count = $count;
  }
  public function get_given_count() {
    return $this->given_count;
  }

  // attributes
  private $date;
  private $url;
  private $addon;
  private $alphabetical;
  private $user_name;

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // url
  public function set_url($var) {
    $this->url = $var;
  }
  public function get_url() {
    return $this->url;
  }

  // addon
  public function set_addon($var) {
    $this->addon = $var;
  }
  public function get_addon() {
    return $this->addon;
  }

  // alphabetical
  public function set_alphabetical($var) {
    $this->alphabetical = $var;
  }
  public function get_alphabetical() {
    return $this->alphabetical;
  }

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // todo clean up the refuse below
  //private $pagination_obj;
  //private $reason_obj;
  //private $title;
  //private $plant_obj;

  // pagination_obj
  //public function get_pagination_obj() {
  //  if (! isset($this->pagination_obj)) {
  //    //include_once("class_pagination.php");
  //    //$this->pagination_obj = new Pagination();
  //  }
  //  return $this->pagination_obj;
  //}

  // reason_obj
  //public function get_reason_obj() {
  //  if (! isset($this->reason_obj)) {
  //    include_once("reasons.php");
  //    $this->reason_obj = new Reasons();
  //  }
  //  return $this->reason_obj;
  //}

  // title
  //public function set_title($var) {
  //  $this->title = $var;
  //}
  //public function get_title() {
  //  return $this->title;
  //}

  // potentials_obj
  //public function get_potentials_obj() {
  //  if (! isset($this->potentials_obj)) {
  //    include_once("potentials.php");
  //    $this->potentials_obj = new Potentials();
  //    $this->potentials_obj->set_hyperlink_id($this->get_id());
  //  }
  //  return $this->potentials_obj;
  //}

  // for potentials.php [0] www [1] no www
  private $links_to_find = array();

  // keyword to set the order of links on page
  private $data_sort;

  // data_sort
  public function set_data_sort($var) {
    // how the list is sorted
    // note: class_validation enforces this for the parameter (user input)
    if ($var == "alphabetical" || $var == "random") {
      $this->data_sort = $var;
    } else {
      $this->get_db_dash()->output_error("Error: not a valid data_sort method.");
    }
  }
  public function get_data_sort() {
    if (! isset($this->data_sort)) {
      // set default
      $this->data_sort = "random"; // default;
      if (isset($_GET['data-sort'])) {
        // filter
        include_once("lib/validation.php");
        $validation_obj = new Validation();
        $validated_keyword = $validation_obj->sanitize_user_input_as_keyword($_GET['data-sort']);
        $this->set_data_sort($validated_keyword);
      }
    }
    return $this->data_sort;
  }

  private $count_total_hyperlinks;     // integer of count from db

  // method
  public function get_count_total_hyperlinks() {
    if (! isset($this->count_total_hyperlinks)) {
      $this->set_type("get_count_total_hyperlinks");
      $this->prepare_query();
    }
    return $this->count_total_hyperlinks;
  }

  private $search_message = "";  // string to communicate search details

  // method
  public function get_links_to_find() {
    return $this->links_to_find;
  }

  // derived
  private $href_count;

  // href_count
  public function set_href_count($var) {
    $this->href_count = $var;
  }
  public function get_href_count() {
    return $this->href_count;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants();
    }
    return $this->plant_obj;
  }

  // img_url
  public function get_img_url() {
    return $img_url = "http://mudia.com/dash/_images/hyperlinks_stamp.png";
  }

  // method
  private function make_hyperlink() {
    $obj = new Hyperlinks($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_is_home()) {

      // special
      if ($this->get_data_sort() == "alphabetical") {
        $this->set_type("get_all");

      } else {
        // default
        $this->set_type("get_all_random");
      }
    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if (isset($_GET['command']) && $_GET['command'] == "mudiabot_data") {
      // special for linkmaster
      $this->set_type("get_mudiabot_data");
      
    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug hyperlinks type = " . $this->get_type() . "<br />\n";

    // todo clean up old code
    // $this->set_type("get_website_list_data_sort_by_alphabetical");
    // //if (! $this->get_pagination_obj()->get_last_rank()) {
    // //  $this->get_db_dash()->print_error("Error: lost place in list.");
    // //  return;
    // //}
    
    //if ($this->get_reason_obj()->get_name()) {
    //  $this->set_data_sort("alphabetical");
    //  $this->set_type("reason_alphabetical");
    //  // check that last_rank exists
    //  // this was causing a bug so it is now commented out
    //  //if (! $this->get_pagination_obj()->get_last_rank()) {
    //  //  $this->get_db_dash()->print_error("Error: lost place in list.");
    //  //  return;
    //  //}
    //}
    
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $database_name = "";
    $sql = "";

    // debug
    //print "debug hyperlinks prepare_query()<br />\n";
    // debug
    //print "debug hyperlinks type = " . $this->get_type() . "<br />\n";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // open security-wise
      // note: home so just the "pws" subset (default)
      // old is poor because it needs a reason
      $sql = "SELECT hyperlinks.* FROM hyperlinks WHERE hyperlinks.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug hyperlinks sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // open security-wise
      // todo problem that potentials uses RESTful and pws home uses parameters
      if (isset($_GET['last_rank'])) {
        // filter
        include_once("lib/validation.php");
        $validation_obj = new Validation();
        $this->get_pagination_obj()->set_last_rank($_GET['last_rank']);
      }
      // note: home so just the "pws" subset (default)

      // figure out the LIMIT
      // default
      if (0) {
        $limit = $this->get_pagination_obj()->get_count_per_page();
      }
      // allow API to set the limit
      if ($this->get_given_count()) {
        $limit = $this->get_given_count();
      }
      // todo: the sql statement above is only a subset because it appears to not get the hyperlink rows which do not have a reason
      // todo: so I have to make sure that (somewhere) that each row in hyperlinks has a row in the hyperlink_reasons table
      if (0) {
        $order_by = " ORDER BY hyperlinks.alphabetical limit " . $limit . " OFFSET " . $this->get_pagination_obj()->get_last_rank();
      }
      // todo old
      $order_by = " ORDER BY hyperlinks.alphabetical";
      // main sort
      $order_by = " ORDER BY hyperlinks.sort DESC";

      // old version with hyperlink_reasons table
      $sql = "SELECT hyperlinks.*, reasons.id, reasons.name, reasons.img_url FROM hyperlinks, hyperlink_reasons, reasons WHERE hyperlink_reasons.hyperlink_id = hyperlinks.id AND hyperlink_reasons.reason_id = reasons.id" . $order_by . ";";
      // select
      $sql = "SELECT hyperlinks.* FROM hyperlinks " . $order_by . ";";

      // debug
      //print "debug hyperlinks sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_project_id") {
      // todo fix
      $sql = "";

    } else if ($this->get_type() == "get_all_random") {
      // open security-wise
      // special for homepage
      // figure out the LIMIT
      // default
      $limit = $this->get_pagination_obj()->get_count_per_page();
      // allow API to set the limit
      if ($this->get_given_count()) {
        $limit = $this->get_given_count();
      }
      $order_by = " ORDER BY random() LIMIT " . $limit . " OFFSET " . $this->get_pagination_obj()->get_last_rank();
      $sql = "SELECT hyperlinks.*, reasons.id, reasons.name, reasons.img_url FROM hyperlinks, hyperlink_reasons, reasons WHERE hyperlink_reasons.hyperlink_id = hyperlinks.id AND hyperlink_reasons.reason_id = reasons.id " . $order_by . ";";

    } else if ($this->get_type() == "get_count_total_hyperlinks") {
      // pws
      $sql = "SELECT count(*) FROM hyperlinks;";

    } else if ($this->get_type() == "get_count") {
      // pws
      $sql = "SELECT count(*) FROM hyperlinks";

    } else if ($this->get_type() == "get_mudiabot_data") {
      // pws
      $sql = "SELECT id, url FROM hyperlinks;";

    } else if ($this->get_type() == "get_all_id") {
      // pws
      $sql = "SELECT hyperlinks.id FROM hyperlinks ORDER BY hyperlinks.id;";

    } else if ($this->get_type() == "get_with_reason") {
      // ok
      $sql = "SELECT DISTINCT ON (hyperlink_reasons.hyperlink_id) hyperlink_reasons.hyperlink_id FROM hyperlink_reasons, hyperlinks WHERE hyperlink_reasons.hyperlink_id = hyperlinks.id ORDER BY hyperlink_reasons.hyperlink_id;";

      // debug
      //print "debug sql " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

      // todo revive the following which has to do with the plants
      //$sql = "SELECT hyperlinks.id, hyperlinks.plant_id, hyperlinks.url, hyperlinks.name plants.common_name FROM hyperlinks, plants WHERE hyperlinks.plant_id = plants.id ORDER BY hyperlinks.name;";

//    } else if ($this->get_type() == "reason_alphabetical") {
//      $sql = "SELECT hyperlinks.* FROM hyperlinks, reasons, hyperlink_reasons WHERE hyperlinks.id = hyperlink_reasons.hyperlink_id and reasons.id = hyperlink_reasons.reason_id and reasons.name = '" . $this->get_reason_obj()->get_name() . "' ORDER BY alphabetical limit " . $this->get_pagination_obj()->get_count_per_page() . " OFFSET " . $this->get_pagination_obj()->get_last_rank() . ";";

//    } else if ($this->get_type() == "get_last_inserted_hyperlink") {
//      // get link with maximum id, assumed to be last row that was input
//      // subquery
//      $sql = "SELECT * FROM hyperlinks where id = (select max(id) from hyperlinks);";

//    } else if ($this->get_type() == "find_given_url") {
//      $links_to_find = $this->get_links_to_find();
//      //print "Databasedashboard: 0 " . $links_to_find[0] . "<br />\n";
//      //print "Databasedashboard: 1 " . $links_to_find[1] . "<br />\n";
//      $sql = "SELECT * FROM hyperlinks WHERE url ilike '" . $links_to_find[0] . "%' OR url ilike '" . $links_to_find[1] . "%';";

//    } else if ($this->get_type() == "no_name") {
//      $sql = "SELECT * FROM hyperlinks WHERE id = " . $instance->get_id() . ";";

//    } else if ($this->get_type() == "load_all_rows_simple") {
//      $sql = "SELECT id, url, name FROM hyperlinks ORDER BY name;";

    // todo define database based upon context (domain name, aka host)
    // todo old way of determining database
    //if (strstr($_SERVER['HTTP_HOST'], "permaculturewebsites.org")) {
    //}

    // todo move this to a more global place
    // database_name
    // this is the database that holds the table
    //$database_name = "";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if (! $results) {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": no results to transfer.");
      return $markup;
    }
    if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_all_random" ||
        $this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_hyperlink();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_date(pg_result($results, $lt, 1));
        $obj->set_url(pg_result($results, $lt, 2));
        $obj->set_name(pg_result($results, $lt, 3));
        $obj->set_addon(pg_result($results, $lt, 4));
        $obj->set_alphabetical(pg_result($results, $lt, 5));
        $obj->set_user_name(pg_result($results, $lt, 6));
        $obj->set_description(pg_result($results, $lt, 7));
        $obj->set_sort(pg_result($results, $lt, 8));
        $obj->set_status(pg_result($results, $lt, 9));
      }
    } else if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_hyperlink();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_date(pg_result($results, $lt, 1));
        $obj->set_url(pg_result($results, $lt, 2));
        $obj->set_name(pg_result($results, $lt, 3));
        $obj->set_addon(pg_result($results, $lt, 4));
        $obj->set_alphabetical(pg_result($results, $lt, 5));
        $obj->set_user_name(pg_result($results, $lt, 6));
        $obj->set_description(pg_result($results, $lt, 7));
        $obj->set_sort(pg_result($results, $lt, 8));
        $obj->set_status(pg_result($results, $lt, 9));
        $obj->set_img_url(pg_result($results, $lt, 10));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_hyperlink();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_count") {
      $this->get_list_bliss()->add_item($this);
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_count(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_count_total_hyperlinks") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        // special because it assigns data directly to variable
        $this->count_total_hyperlinks = pg_result($results, $lt, 0);
      }
    } else if ($this->get_type() == "get_all_id" ||
               $this->get_type() == "get_with_reason") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $hyperlink = $this->make_hyperlink();
        $hyperlink->set_id(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_mudiabot_data") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $hyperlink = $this->make_hyperlink();
        $hyperlink->set_id(pg_result($results, $lt, 0));
        $hyperlink->set_url(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

//      if ($this->get_type() == "hyperlinks_dates") {
//        $this->sql_statement = "SELECT date, count(*) FROM hyperlinks group by date order by date;";
//      }
//    if ($this->get_type() == "main_get_website_list" ||
//        $this->get_type() == "get_website_list_data_sort_by_alphabetical" ||
//        $this->get_type() == "reason_alphabetical" ||
//        $this->get_type() == "get_last_inserted_hyperlink" ||
//        $this->get_type() == "find_given_url") {
//      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
//        $website = $this->make_website();
//        $website->set_id(pg_result($results, $lt, 0));
//        $website->set_date(pg_result($results, $lt, 1));
//        $website->set_url(pg_result($results, $lt, 2));
//        $website->set_name(pg_result($results, $lt, 3));
//        $website->set_addon(pg_result($results, $lt, 4));
//        $website->set_alphabetical(pg_result($results, $lt, 5));
//      }
//    } else if ($this->get_type() == "load_all_rows_simple") {
//      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
//        $website = $this->make_website();
//        $website->set_id(pg_result($results, $lt, 0));
//        $website->set_url(pg_result($results, $lt, 1));
//        $website->set_name(pg_result($results, $lt, 2));
//      }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    if ($this->get_given_is_home()) {
      return $this->output_aggregate_for_homepage();
    }

    if ($this->get_type() == "get_mudiabot_data") {
      return $this->output_mudiabot_data();
    }

    // show stats
    // fix the following with a usr config file variable
    if (1) {
      $markup .= $this->show_stats();
    }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";

    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    url\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    add on\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"background: #CCCCCC;\">\n";
    $markup .= "    reasons\n";
    $markup .= "  </td>\n";
    // todo this cell is ripe for refactoring and calling a function
    $markup .= "  <td class=\"header\" style=\"width: 30px; background: #CCCCCC;\">\n";
    $url = $this->url("tags");
    $markup .= "    <a href=\"" . $url . "\">tags</a>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    user_name\n";
    //$markup .= "  </td>\n";

    $markup .= "</tr>\n";

    // initials loop globals
    $num = 0;

    foreach ($this->get_list_bliss()->get_list() as $hyperlink) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // id
      // old (but it was too wide)
      //$markup .= "  <td class=\"specify-width\">\n";
      // new
      $markup .= "  <td>\n";
      $url = $this->url("hyperlinks/" . $hyperlink->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $hyperlink->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      // status
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_status() . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $hyperlink->get_sort_cell();

      // img_url
      $markup .= "  <td class=\"specify-width\">\n";
      // todo perhaps this code can be simplied and refactored into a function
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "25";
      $markup .= "    " . $hyperlink->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // url and name
      $markup .= "  <td class=\"specify-width\">\n";
      // todo make this a class or part of an existing class
      $markup .= "    <a href=\"" . $hyperlink->get_url() . "\">" . $hyperlink->get_name() . "</a>\n";
      $markup .= "  </td>\n";

      // addon
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_addon() . "\n";
      $markup .= "  </td>\n";

      // reasons
      // todo old way from development on pws website
      include_once("hyperlink_reasons.php");
      $hyperlink_reason_obj = new HyperlinkReasons($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $hyperlink_id = $hyperlink->get_id();
      $list_markup = $hyperlink_reason_obj->get_reasons_given_hyperlink_id($hyperlink_id, $user_obj);
      if ($list_markup) {
        $markup .= "  <td class=\"specify-width\">\n";
      } else {
          $markup .= "  <td class=\"specify-width\" style=\"background-color: red;\">\n";
      }
      $markup .= $list_markup . "\n";
      $markup .= "  </td>\n";

      // tags
      include_once("tags.php");
      $tags_obj = new Tags($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $hyperlink_id = $hyperlink->get_id();
      $list_markup = $tags_obj->get_tags_given_hyperlink_id_as_list($hyperlink_id, $user_obj);
      if ($list_markup) {
        $markup .= "  <td class=\"specify-width\">\n";
        $markup .= $list_markup . "\n";
      } else {
          $markup .= "  <td class=\"specify-width\" style=\"background-color: red;\">\n";
      }
      $markup .= "  </td>\n";

      // date
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_date() . "\n";
      $markup .= "  </td>\n";

      // user_name
      //$markup .= "  <td class=\"specify-width\">\n";
      //$markup .= "    " . $hyperlink->get_user_name() . "\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // pagination menu
    // todo refactor this
    //$markup .= $this->get_pagination_obj()->output_pagination_alphabetical();

    return $markup;
  }

  // method
  public function output_simple_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\" style=\"padding: 0px 0px 6px 0px;\">\n";

    foreach ($this->get_list_bliss()->get_list() as $hyperlink) {
      $markup .= "<tr>\n";

      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    <a href=\"" . $hyperlink->get_url() . "\">" . $hyperlink->get_name() . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    foreach ($this->get_list_bliss()->get_list() as $hyperlink) {

      // id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    <a href=\"" . $hyperlink->get_url() . "\" class=\"show\">" . $hyperlink->get_id() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // img_url
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "65";
      $markup .= "    " . $hyperlink->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    <a href=\"" . $hyperlink->get_url() . "\" class=\"show\">" . $hyperlink->get_name() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // alphabetical
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    alphabetical\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_alphabetical() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // url
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    <a href=\"" . $hyperlink->get_url() . "\" class=\"show\">" . $hyperlink->get_url() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // description
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_description() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // date
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    date\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_date() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // add-on
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    addon\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_addon() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // reasons
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    reasons\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      //include_once("reasons.php");
      //$reasons_obj = new Reasons;
      //$user_obj = $this->get_user_obj();
      //$markup .= "    " . $reasons_obj->get_reasons_given_id_as_list($user_obj, $hyperlink->get_id()) . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // headtags
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    headtags\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      //include_once("headtags.php");
      //$headtags_obj = new Headtags;
      // todo develop this because the bug is in the select
      //$user_obj = $this->get_user_obj();
      //$markup .= "    " . $headtags_obj->get_headtags_given_id_as_list($user_obj, $hyperlink->get_id()) . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // sort
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_sort() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // status
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_status() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // user_name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    user_name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "    " . $hyperlink->get_user_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_sidecar($given_id) {
    $markup = "";

    $this->set_given_id($given_id);
    $markup .= $this->determine_type();

    // debug
    //print "type " . $this->get_type() . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<div style=\"margin: 4px 50% 4px 0px; padding: 6px 6px 0px 6px; background-color: #E8C782;\">\n";
    $markup .= "<h4 style=\"margin: 4px 0px 0px 4px; padding: 4px 0px 6px 1px;\">Hyperlinks</h4>\n";
      $markup .= $this->output_simple_table();
      $markup .= "</div>\n";

    }

    return $markup;
  }

  // method
  public function insert() {
    // output is based upon host
    if (strstr($_SERVER['HTTP_HOST'], "permaculturewebsites.org")) {
      // pws
      $sql = "INSERT INTO hyperlinks (date, url, name, addon, alphabetical) VALUES ('" . $this->get_date() . "', '" . $this->get_url() . "', '" . $this->get_name() . "', '" . $this->get_addon() . "', '" . $this->get_alphabetical() . "');";
      $database_name = "mudiacom_psites";
    } else {
      // lff
      $sql = "INSERT INTO hyperlinks (plant_id, url, title) VALUES (" . $this->get_plant_obj()->get_id() . ", '" . $this->get_url() . "', '" . $this->get_title() . "');";
      $database_name = "mudiacom_soiltoil";
    }
    $error_message = $this->get_db_dash()->new_insert($database_name, $sql);
    return $error_message;
  }
  // method
  public function get_array_of_all_rows_simple() {
    // database //
    $this->set_type("load_all_rows_simple");
    $this->get_db_dash()->load($this, $type);
    return $this->list;
  }

  // method
  public function output_aggregate_for_homepage() {
    $markup = "";

    // heading
    if ($this->get_reason_obj()->get_name()) {
      $markup .= "<h2><a href=\"about.php#what-qualifies\" class=\"noshow\">List of Qualified Permaculture Websites</a></h2>\n";
      $markup .= "<p class=\"qualified-websitelist\"><a href=\"about.php#what-qualifies\" class=\"noshow\">Qualified by <strong>" . $this->get_reasons_obj()->get_name_user_ready() . "</strong></a></h2>\n";
    } else {
      $markup .= "<h2>List of Permaculture Websites</h2>\n";
    }

    // output //
    if ($this->get_list_bliss()->get_count() > 0) {
    
      // list has hyperlinks
      $markup .= $this->output_list_table_for_homepage();

      // if not random data_sort and is alphabetical data_sort
      // if ($this->get_reason_obj()->get_name()) {
      //    $reason_name = $this->get_reason_obj()->get_name();
      //    $this->get_pagination_obj()->set_reason_name($reason_name);
      // }

    }

    // context-based output
    if ($this->get_data_sort() == "alphabetical") {
      // output a pagniation menu (the navigation for alphabetical)
      // pagination menu
      // for pws
      $markup .= $this->get_pagination_obj()->output_pagination_alphabetical("home");
    }

    // output a subbox displaying the list's metadata after the list
    $markup .= "<div class=\"hyperlink-subsub\">\n";
    // (1) data_sort
    if ($this->get_data_sort()) {
      $markup .= "sort: ";
      if ($this->get_data_sort() == "alphabetical") {
        $markup .= "alphabetical";
        $markup .= "| <a href=\"http://permaculturewebsites.org/home?data-sort=random\">random</a><br />\n";
      } else {
        $markup .= "<a href=\"http://permaculturewebsites.org/home?data-sort=alphabetical\">alphabetical</a>\n";
        $markup .= "| random<br />\n";
      }
    }
    $markup .= "</div>\n";

    // old navigation "more" section
    //$markup .= "<div style=\"margin: 20px 0px 30px 6px;\">\n";
    //$markup .= "<span style=\"padding: 2px 2px 2px 2px; background-color: #EFEFEF;\">\n";
    //$markup .= "<a href=\"http://permaculturewebsites.org/\"><em>more Permaculture Websites<em></a><br />\n";
    //$markup .= "</span>\n";
    //$markup .= "</div>\n";

    // add this helper message
    //if ($this->search_message) {
    //  $markup .= $this->search_message;
    //}

    return $markup;
  }

  // method
  public function output_list_table_for_homepage() {
    $markup = "";

    // guts of the list
    //  class=\"links\"
    $markup .= "<table style=\"width: 572px\" border=\"0\" cellpadding=\"1\" class=\"hyperlinks-special\">\n";
    foreach ($this->get_list_bliss()->get_list() as $hyperlink) {

      // attempt without a table
      //$markup .= "<div style=\"display: block; width: 550px;\">\n";
      //$markup .= "  <div style=\"display: inline; text-slign: left;\">";
      //$markup .= $hyperlink->get_stamp("float");
      //$markup .= "  </div>\n";
      //$markup .= "  <div style=\"display: inline; background-color: #FFFFFF; text-slign: left; margin: 0px 0px 0px 0px; float: right; width: 436px;\">\n";
      //$markup .= "    <div style=\"text-align: left;\">\n";
      //$markup .= "      <span style=\"font-size: 140%; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;\">";
      //$markup .= $hyperlink->get_name_with_link();
      //$markup .= "</span><br />\n";
      //if ($hyperlink->get_addon()) {
      //  $markup .= "      <span style=\"color: #999999;\">" .$hyperlink->get_addon() . "</span>\n";
      //}
      //$markup .= "      </div>\n";
      //$markup .= "    </div>\n";
      //$markup .= "  </div><br />\n";

      $markup .= "<tr>\n";
      $markup .= "  <td width=\"92\">\n";
      $markup .= $hyperlink->get_stamp("float");
      $markup .= "  </td>\n";
      $markup .= "  <td width=\"480\">\n";
      $markup .= "      <span style=\"font-size: 140%; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;\">" . $hyperlink->get_name_with_link() . "</span><br />\n";
      if ($hyperlink->get_addon()) {
        $markup .= "      <span style=\"color: #999999; padding: 0px 0px 6px 0px;\">" .$hyperlink->get_addon() . "</span>\n";
      }
      $markup .= "  </td>\n";
      $markup .= "<tr>\n";

      //$markup .= "  <!-- id=" . $hyperlink->get_id() . " -->\n";
      // old (but it was too wide)
      //$markup .= "  <td class=\"specify-width\">\n";
      // new
      //$markup .= "  <td style=\"padding: 0px 0px 10px 0px;\">\n";
      // old
      //$markup .= "    <span style=\"font-size: 140%; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; vertical-align: top; line-height: 90%;\">"; 
      // now
      //$markup .= "  &mdash;\n";
      //$markup .= "<br />\n";
      //$markup .= "  </td>\n";
      //if (isset($_GET['source']) && $_GET['source'] == "linkmaster.php") {
      //  //$markup .= "  <td>\n";
    }
  }

  public function remove_duplicates_and_limit_list() {
    // special
    $new_list = array();
    foreach ($this->list as $hyperlink) {
      $id = $hyperlink->get_id();
      if (! array_key_exists($id, $new_list)) {
        // not in list, so store 
        $new_list[$id] = $hyperlink;
      }
      if (count($new_list) > 50) {
        $this->search_message = "<p>Over 50 links found. List is truncated.</p>\n";
        break;
      }
    }
    $this->list = $new_list;
  }

  // method
  public function is_url_in_db($given_link) {
    $markup = "";

    // special
    // if found, return array of links
    $sites = array();

    $given_link_with_www = $given_link;
    $given_link_without_www = $given_link;
    $pos = strpos($given_link_with_www, "http://www");
    if ($pos === FALSE) {
      $given_link_with_www = preg_replace('/http:\/\//', 'http://www.', $given_link);
      //$markup .= "<p>Websitelist: 1 without www $link</p>\n";
      //$markup .= "<p>Websitelist: 1 with www $link_with_www</p>\n";
    } else {
      $given_link_without_www = preg_replace('/http:\/\/www./', 'http://', $given_link);
      //$markup .= "<p>Websitelist: 2 with www $link</p>\n";
      //$markup .= "<p>Websitelist: 2 without www $link_without_www</p>\n";
    }
    // with www
    array_push($this->links_to_find, $given_link_with_www);
    // without www  
    array_push($this->links_to_find, $given_link_without_www);

    // debug
    //$markup .= "count = " . count($this->get_links_to_find()) . "<br />\n";

    // database
    $this->set_type("find_given_url");
    $this->prepare_query();

    // output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->list as $hyperlink) {
        $id = $hyperlink->get_id();
        $url = "hyperlink/" . $id;
        array_push($sites, $url);
      }
    }
    return $sites;
  }

  // method
  public function get_list_given_category($category_id) {
    $this->set_given_category_id($category_id);
    // database //
    $type = "get_websites_given_category";
    $this->get_db_dash()->load($this, $type);
    return $this->list;
  }

  // method
  public function get_stamp($given_float = "") {
      $markup = "";
      $markup .= "   <img src=\"";
      if ($this->get_reason_obj()->get_id() == 1) {
        // qualifies domain
        $markup .= "http://permaculturewebsites.org/pws/_images/qualifies_domain_name.png";
      } else if ($this->get_reason_obj()->get_id() == "2") {
        // qualifies definition
        $markup .= "http://permaculturewebsites.org/pws/_images/qualifies_definition.png";
      } else if ($this->get_reason_obj()->get_id() == "3") {
        // qualifies book
        $markup .= "http://permaculturewebsites.org/pws/_images/qualifies_book.png";
      } else if ($this->get_reason_obj()->get_id() == "4") {
        // qualifies word
        $markup .= "http://permaculturewebsites.org/pws/_images/qualifies_word.png";
      } else {
        // default pws icon
        $markup .= "http://permaculturewebsites.org/sites/permaculturewebsites.org/files/pws_logo.png";
      }
      $markup .= "\" width=\"87\" height=\"30\" ";
      if ($given_float) {
        $markup .= "float=\"left\" ";
      }
      $markup .= "alt=\"\" style=\"padding: 2px 2px 2px 2px; display: inline;\"/>";
      return $markup;
  }

  // method
  // fix below
  // outputs data for mudiabot
  private function output_mudiabot_data() {
    $markup = "";

    $markup .= "<h3>Mudiabot Data</h3>\n";
    $markup .= "<div class=\"special\"><br />\n";
    // loop for earch line
    foreach ($this->get_list_bliss()->get_list() as $hyperlink) {
      $markup .= $hyperlink->get_id();
      $markup .= " => '";
      $markup .= $hyperlink->get_url();
      $markup .= "',<br />\n";
    }
    $markup .= "</div><!-- special --><br />\n";

    return $markup;
  }

  // method
  public function get_project_id() {
    $markup = "";

    // todo link hyperlinks to projects

    return $markup;
  }

  // method
  public function get_name_with_link() {
    return "<a href=\"" . $this->get_url() . "\" class=\"show\">" . $this->get_name() . "</a>";
  }

  // method
  public function get_name_with_link_noshow() {
    return "<a href=\"" . $this->get_url() . "\" class=\"noshow\">" . $this->get_name() . "</a>";
  }

  // method
  public function output_sidecar_given_class_name_and_id($given_class_name_string, $given_primary_key_id_string) {
    $markup = "";

    // todo this does not work because it asks something not there
    //$this->set_given_class_name_string($given_class_name_string);
    //$this->set_primary_key_id_string($given_primary_key_id_string);

    $this->determine_type();
    $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $hyperlink) {
      $markup = "";
    }

    // todo need to code this function, meanwhile warning message
    $markup = "[hyperlinks not coded yet.]";

    return $markup;
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT hyperlinks.id, hyperlinks.name FROM hyperlinks WHERE hyperlinks.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug hyperlinks: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "Hyperlinks ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function show_stats() {
    $markup = "";

    // get stats data
    $stats = array();
    $stats[0] = "total hyperlinks = " . $this->get_list_bliss()->get_count();
    
    // guts of the list
    $markup .= "<h3>show stats</h3>\n";
    $markup .= "<table class=\"plants\" style=\"padding: 0px 0px 6px 0px;\">\n";

    foreach ($stats as $stat) {
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"specify-width\">\n";
      $markup .= "  " . $stat . "<br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_list_of_all_hyperlinks($given_user_obj) {
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_all");
    $this->prepare_query();
    return $this->get_list_bliss()->get_list();
  }

}
