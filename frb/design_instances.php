<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/design_instances.php
   
include_once("lib/standard.php");

class DesignInstances extends Standard {

  // given
  private $given_design_id;

  // given_design_id
  public function set_given_design_id($var) {
    $this->given_design_id = $var;
  }
  public function get_given_design_id() {
    return $this->given_design_id;
  }

  // atttribute
  private $design_obj;
  private $unit_obj;

  // design_obj
  public function get_design_obj() {
    if (! isset($this->design_obj)) {
      include_once("designs.php");
      $this->design_obj = new Designs($this->get_given_config());
    }
    return $this->design_obj;
  }

  // unit_obj
  public function get_unit_obj() {
    if (! isset($this->unit_obj)) {
      include_once("units.php");
      $this->unit_obj = new Units($this->get_given_config());
    }
    return $this->unit_obj;
  }

  // method
  private function make_design_instance() {
    $obj = new DesignInstances($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_design_id()) {
      $this->set_type("get_by_design_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT design_instances.*, designs.name, projects.id, projects.name, projects.img_url FROM design_instances, designs, projects WHERE design_instances.design_id = designs.id AND design_instances.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug design_instances sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // todo old sql that shows an old relationship
      //$sql = "SELECT design_instances.id, design_instances.design_id, design_instances.name, design_instances.status, design_instances.land_id, design_instances.units_id, designs.name, units.name, design_instances.description, lands.name FROM design_instances, designs, units, projects, lands WHERE design_instances.land_id = lands.id AND projects.user_name = '" . $this->get_user_obj()->name . "' AND designs.id = design_instances.design_id AND design_instances.design_id = designs.id AND design_instances.units_id = units.id ORDER BY design_instances.name;";
      $sql = "SELECT design_instances.*, designs.name, projects.id, projects.name, projects.img_url FROM design_instances, designs, projects WHERE designs.id = design_instances.design_id AND projects.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT design_instances.*, designs.name, projects.id, projects.name, projects.img_url FROM design_instances, designs, projects WHERE designs.id = design_instances.design_id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_by_design_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT design_instances.*, designs.name, projects.id, projects.name, projects.img_url FROM design_instances, designs, projects WHERE designs.id = design_instances.design_id AND designs.id = " . $this->get_given_design_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug design_instances sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_design_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $design_instance_obj = $this->make_design_instance();
        $design_instance_obj->set_id(pg_result($results, $lt, 0));
        $design_instance_obj->get_design_obj()->set_id(pg_result($results, $lt, 1));
        $design_instance_obj->set_name(pg_result($results, $lt, 2));
        $design_instance_obj->set_status(pg_result($results, $lt, 3));
        $design_instance_obj->get_unit_obj()->set_id(pg_result($results, $lt, 4));
        $design_instance_obj->set_description(pg_result($results, $lt, 5));
        $design_instance_obj->set_sort(pg_result($results, $lt, 6));
        $design_instance_obj->set_img_url(pg_result($results, $lt, 7));
        $design_instance_obj->get_design_obj()->set_name(pg_result($results, $lt, 8));
        $design_instance_obj->get_design_obj()->set_id(pg_result($results, $lt, 9));
        $design_instance_obj->get_design_obj()->set_name(pg_result($results, $lt, 10));
        $design_instance_obj->get_design_obj()->set_img_url(pg_result($results, $lt, 11));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    num\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    design\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"220\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    land\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"100\">\n";
    $markup .= "    units\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $design_instance) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td width=\"header\">\n";
      $markup .= "  " . $num;
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "  " . $design_instance->get_design_obj()->get_name_with_link();
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "  " . $design_instance->get_id();
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "  " . $design_instance->get_name_with_link();
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "  " . $design_instance->get_description();
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "  " . $design_instance->get_status();
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "  " . $design_instance->get_units_obj()->get_name_with_link();
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "  </table>\n";

    return $markup;
  }

  // method form
  public function print_form_selection() {
    $markup = "";

    // database
    $type = "get_all_design_instances";
    $database_name = "plantdot_principle";
    $this->get_db_dash($database_name)->load($this, $type);

    // output
    $markup .= "<select name=\"design_instance_id\">\n";

    foreach ($this->get_list_bliss()->get_list() as $design_instance_obj) {
      $markup .= "  <option value=\"" . $design_instance_obj->get_id() . "\">" . $design_instance_obj->get_name() . " based upon " . $design_instance_obj->get_design_obj()->get_name_with_link() . "</option>\n";
    }
    $markup .= "</select>\n";

    return $markup;
  }

  // method
  public function print_simple_list($design_id) {
    $markup = "";

    $this->set_given_design_id($design_id);

    // database
    $type = "get_design_instances_given_design_id";
    $database_name = "plantdot_principle";
    $this->get_db_dash($database_name)->load($this, $type);

    // output
    $markup .= "  <table class=\"plants\">\n";
    foreach ($this->get_list_bliss()->get_list() as $design_instance_obj) {
      $markup .= "  <tr>\n";
      $markup .= "    <td>" . $design_instance_obj->get_id() . "</td>\n";
      $markup .= "    <td>" . $design_instance_obj->get_name_with_link() . "</td>\n";
      $markup .= "  </tr>\n";
    }
    $markup .= "  </table>\n";

    return $markup;
  }

  // method
  public function output_given_design_id($given_design_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_design_id($given_design_id); 
    $this->set_user_obj($given_user_obj); 

    // get_date
    $this->determine_type();
    $this->prepare_query();

    // heading
if (0) {
    $markup .= "<h3>Design Instances for this Design</h3>\n";
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  #\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  id\n";
    $markup .= "</td>\n";
    $markup .= "<td class=\"header\">\n";
    $markup .= "  name\n";
    $markup .= "</td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $design_part) {
      $markup .= "<tr>\n";
      $markup .= "<td class=\"header\">\n";
      $num++;
      $markup .= "  " . $num . "\n";
      $markup .= "</td>\n";
      $markup .= "<td>\n";
      $markup .= "  " . $design_part->get_id() . "\n";
      $markup .= "</td>\n";
      $markup .= "<td>\n";
      $markup .= "  " . $design_part->get_name_with_link() . "\n";
      $markup .= "</td>\n";
      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";
}

    return $markup;
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    $markup .= "<div class=\"subsubmenu\">\n";
    if ($this->get_type() == "get_by_project_id") {
      $url = $this->url("designs/project/" . $this->get_given_project_id());
      $markup .= "To <a href=\"" . $url . "\">designs of this project</a>\n";
    } else {
      $url = $this->url("designs");
      $markup .= "To <a href=\"" . $url . "\">designs</a>\n";
    }
    $markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";
    // only authenticated users
    if ($this->get_user_obj()) {
      //if ($this->get_user_obj()->uid) {
      //  $markup .= "<div class=\"subsubmenu-user\">\n";
      //  $markup .= "<strong>These " . get_class($this) . " were created by</strong> " . $this->get_user_obj()->name . "<br />\n";
      //  $markup .= "</div>\n";
      //}
    }
    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // todo this had to be commented out because it has no project_id column
    //if ($this->get_type() == "get_by_project_id") {
    //  $markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());
    //}
    if ($this->get_type() == "get_by_design_id") {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "  These are design instances of design id = " . $this->get_given_design_id() . ".\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_reading($units_obj) {

    // reading //
    $this->set_units_obj($units_obj);

    $design_instance_id = $this->get_design_instance_obj()->get_id();

    $reading = "design instance id <a href=\"design_instance?id=" . $design_instance_id . "\">" . $design_instance_id . "</a> is " . $this->get_status($design_instance_id);

    return $reading;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $design_instance) {

      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $design_instance->get_img_as_img_element_with_link($padding, $float, $width) . "\n";

      // table
      $markup .= "<table class=\"plants\">\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $design_instance->get_id();
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $url = $this->url("designs/" . $design_instance->get_design_obj()->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $design_instance->get_design_obj()->get_name() . "</a>\n";;
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    land\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      
      $user_obj = $this->get_user_obj();
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    units\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $design_instance->get_units_obj()->get_name_with_link();
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
      $markup .= "</table>\n";
      $markup .= "<br />\n";

      // note: get for usage outside of loop (see below)
      $design_instance_id = $$esign_instance->get_id();
    }

    // design_order_items
    //include_once("design_order_items.php");
    //$design_order_item_obj = new DesignOrderItems($this->get_given_config());
    //$design_order_item_obj->set_user_obj($this->get_user_obj());
    //$markup .= $design_order_item_obj->output_table_given_design_instance_id($esign_instance_id);
    //$markup .= "<br />\n";

    return $markup;
  }

  // method
  public function get_given($garden_id, $units_id) {
    $markup = "";

    $this->get_units_obj()->set_id($units_id);

    // debug
    //$system_status_message .= "<p>debug: units_id = " . $this->get_units_obj()->get_id() . "<p>\n";

    // todo make this work
    //$type = "get_design_instance_given_garden_and_units";
    //$sql = "SELECT design_instances.id, design_instances.design_id, design_instances.name, design_instances.status, designs.name FROM design_instances, designs WHERE design_instances.units_id = " . $this->get_units_obj()->get_id() . " and design_instances.design_id = designs.id;";
    //$database_name = "plantdot_principle";
    //$markup .= $this->get_db_dash()->load($this, $type, $sql, $database_name);

    if ($this->get_id()) {
      $system_status_message .= "<a href=\"design_instance?id=" . $this->get_id() . "\">" . $this->get_name() . "</a>\n";
      $system_status_message .= "design instance status = " . $this->get_status() . "<br />\n";
    } else {
      $system_status_message .= "down";
    }
    return $system_status_message;
  }

}
