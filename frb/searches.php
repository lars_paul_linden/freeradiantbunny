<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.2 2015-06-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/searches.php

include_once("lib/standard.php");

class Searches extends Standard {

  // given
  private $given_name = "";

  // given_name
  public function set_given_name($var) {
    $this->given_name = $var;
  }
  public function get_given_name() {
    return $this->given_name;
  }

  // attributes
  private $id;
  private $name;
  private $count;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // count
  public function set_count($var) {
    $this->count = $var;
  }
  public function get_count() {
    return $this->count;
  }

  // method
  private function make_search() {
    $obj = new Searches($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_name()) {
      $this->set_type("get_by_name");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT searches.* FROM searches WHERE id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_by_name") {
      $sql = "SELECT searches.* FROM searches WHERE name = '" . $this->get_given_name() . "';";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT searches.* FROM searches ORDER BY name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_name") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_search();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_count(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type = " . $this->get_type());
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $search) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $search->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $search->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $search->get_count() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $search) {

      $markup .= "<h2>" . $search->get_name() . "</h2>\n";
      $markup .= "<p>id = " . $search->get_id() . "</p>\n";
      $markup .= "<p>count = " . $search->get_count() . "</p>\n";
    }

    return $markup;
  }

  // method
  public function insert() {
    if ($this->get_list_bliss()->get_count() > 0) {
      # search already exists, so no insert
      return;
    }
    // set count
    $count = 1;
    $sql = "INSERT INTO searches (name, count) VALUES ('" . $this->get_given_name() . "', " . $count . ");";
    // debug
    //print "debug searches: sql = " . $sql . "<br />\n";
    $database_name = "mudiacom_soiltoil";
    $error_message = $this->get_db_dash()->new_insert($this, $database_name, $sql);
    return $error_message;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // view
    $parameter_a = new Parameter();
    $parameter_a->set_name("view");
    $parameter_a->set_validation_type_as_view();
    // todo fix this user input security issue
    "sanatize_user_input";
    //$parameter_a->set_validator_function_string("sanatize_user_input");
    array_push($parameters, $parameter_a);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function deal_with_posts() {
    $markup = "";

    include_once("lib/validator.php");
    $validator = new Validator();
    if (isset($_POST['name'])) {
      $name = $validator->sanitize_user_input($_POST['name']);
      $this->set_given_name($name);
    }

    return $markup;
  }

  // method
  protected function output_search() {
    $markup = "";

    $form_create_obj = $this->get_form_create_obj();
    $user_obj = $this->get_user_obj();
    $form_create_obj->set_command("create");
    $markup .= $form_create_obj->output_form($this, $user_obj);
    //$markup .= "<br />\n";

    // search for given name
    $markup .= $this->search_special($this->get_given_name()) . "\n";
    $markup .= "<br />\n";
   
    return $markup;
  }

  // method
  private function search_special($given_string) {
    $markup = "";
    if (! $given_string) {
      return;
    }

    // projects
    include_once("projects.php");
    $obj = new Projects($this->get_given_config());
    $markup .= $obj->search($given_string);

    // goal_statements
    include_once("goal_statements.php");
    $obj = new GoalStatements($this->get_given_config());
    $markup .= $obj->search($given_string);

    // business_plan_texts
    include_once("business_plan_texts.php");
    $obj = new BusinessPlanTexts($this->get_given_config());
    $markup .= $obj->search($given_string);

    // processes
    include_once("processes.php");
    $obj = new Processes($this->get_given_config());
    $markup .= $obj->search($given_string);

    // classes
    include_once("classes.php");
    $obj = new Classes($this->get_given_config());
    $markup .= $obj->search($given_string);

    // permaculture_topics
    include_once("permaculture_topics.php");
    $obj = new PermacultureTopics($this->get_given_config());
    $markup .= $obj->search($given_string);

    // tags
    include_once("tags.php");
    $obj = new Tags($this->get_given_config());
    $markup .= $obj->search($given_string);

    // webpages
    include_once("webpages.php");
    $obj = new Webpages($this->get_given_config());
    $markup .= $obj->search($given_string);

    return $markup;
  }

}
