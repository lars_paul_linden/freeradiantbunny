<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18
// version 1.4 2015-03-15
// version 1.5 2015-10-16

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/usernames.php

include_once("lib/standard.php");

class Usernames extends Standard {

  // set constant
  const FRB_NAME = "FreeRadiantBunny";

  function  __construct($given_config) {
    parent::__construct($given_config);
  }

  // special
  private $authenticated;

  // authenticated
  public function set_authenticated($var) {
    // debug
    //print "debug usernames set_authenticated()<br />\n";
    $this->authenticated = $var;
  }
  public function get_authenticated() {
    return $this->authenticated;
  }

  // given
  private $given_username;
  private $given_password;
  private $given_coded_cookie_word;

  // given_username
  public function set_given_username($var) {
    $this->given_username = $var;
  }
  public function get_given_username() {
    return $this->given_username;
  }

  // given_password
  public function set_given_password($var) {
    $this->given_password = $var;
  }
  public function get_given_password() {
    return $this->given_password;
  }

  // given_coded_cookie_word
  public function set_given_coded_cookie_word($var) {
    $this->given_coded_cookie_word = $var;
  }
  public function get_given_coded_cookie_word() {
    return $this->given_coded_cookie_word;
  }

  // attributes
  private $username;
  private $email_address_obj;
  private $coded_cookie_word;
  private $password;

  // username
  public function set_username($var) {
    $this->username = $var;
  }
  public function get_username() {
    return $this->username;
  }

  // method
  public function get_username_with_link() {
    $url = $this->url("usernames/" . $this->get_username());
    return "<a href=\"" .  $url . "\">" . $this->get_username() . "</a>\n";
  }

  // email_address_obj
  public function get_email_address_obj() {
    if (! isset($this->email_address_obj)) {
      include_once("email_addresses.php");
      $this->email_address_obj = new EmailAddresses($this->get_given_config());
    }
    return $this->email_address_obj;
  }

  // coded_cookie_word
  public function set_coded_cookie_word($var) {
    $this->coded_cookie_word = $var;
  }
  public function get_coded_cookie_word() {
    return $this->coded_cookie_word;
  }

  // password
  public function set_password($var) {
    $this->password = $var;
  }
  public function get_password() {
    return $this->password;
  }

  // method
  private function make_username() {
    $obj = new Usernames($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_username()) {
      $this->set_type("get_by_username");

    } else if ($this->get_given_username() && $this->get_given_password()) {
      $this->set_type("get_by_username_and_by_password");

    } else if ($this->get_given_coded_cookie_word()) {
      $this->set_type("get_by_coded_cookie_word");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_username") {
      $sql = "SELECT usernames.* from usernames where usernames.username = '" .  $this->get_given_username() . "';";

      // debug
      //print "debug usernames <em>get_by_username</em> sql " . $sql . "<br />";

    } else if ($this->get_type() == "get_by_username_and_by_password") {
      $sql = "SELECT usernames.* from usernames where usernames.username = '" .  $this->get_given_username() . "' AND usernames.password = '" .  $this->get_given_password() . "';";

      // debug
      //print "debug usernames <em>get_by_username</em> sql " . $sql . "<br />";

    } else if ($this->get_type() == "get_by_coded_cookie_word") {
      $sql = "SELECT usernames.* from usernames where usernames.coded_cookie_word = '" .  $this->get_given_coded_cookie_word() . "';";
 
      // debug
      //print "debug usernames <em>get_by_coded_cookie_word</em> sql " . $sql . "<br />";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT usernames.username, usernames.email_address_id from usernames ORDER BY usernames.username;";

      // debug
      //print "debug usernames <em>get_all</em> sql " . $sql . "<br />";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_username" ||
        $this->get_type() == "get_by_username_and_by_password" ||
        $this->get_type() == "get_by_coded_cookie_word") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_username();
        $obj->set_username(pg_result($results, $lt, 0));
        $obj->get_email_address_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_coded_cookie_word(pg_result($results, $lt, 2));
      }

    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_username();
        $obj->set_username(pg_result($results, $lt, 0));
        $obj->get_email_address_obj()->set_id(pg_result($results, $lt, 1));
      }

    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // only authenticated users
    if ($this->get_user_obj()) {
      // todo clean up the following
      //if ($this->get_user_obj()->uid) {
      //}
    }

    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    // todo fix this because sometimes it does not know email_address_id
    //$markup .= $this->output_given_variables_email_address($this->get_list_bliss()->get_count());

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // debug
    //print "debug usernames <em>output_aggregate()<br />\n";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    username\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    email_address\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $username) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $username->get_username_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $username->get_email_address_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $list_obj = $this->get_list_bliss()->get_list();
    $username_obj = $list_obj[0];

    $markup .= $username_obj->get_img_url_as_img_element() . "<br />\n";

    $markup .= "<h3>" . $username_obj->get_name() . "</h3>\n";

    $markup .= "<p> sort = " . $username_obj->get_email_address_obj()->get_name() . "</p>\n";

    // concatenate string
    if ($username_obj->get_coded_cookie_word() && $username_obj->get_state()) {
      $markup .= "<p>";
      $markup .= $username_obj->get_coded_cookie_word();
      $markup .= ", ";
      $markup .= $username_obj->get_state();
      $markup .= "</p>";
    }

    // agriculture type
    $markup .= "<h4 style=\"background-color: #CCCCCC;\">agricultural type</h4>";
    $markup .= $username_obj->get_email_address_obj()->get_img_url_as_img_element() . "<br />\n";
    $markup .= $username_obj->get_email_address_obj()->get_name();

    // layouts of this land_id
    $markup .= "<h4 style=\"background-color: #CCCCCC;\">layouts of this land_id</h4>";

    include_once("layouts.php");
    $layout_obj = new Layouts($this->get_given_config());
    if ($username_obj->get_id()) {
      $user_obj = $this->get_user_obj();
      $markup .= $layout_obj->get_layout_table_given_land_id($username_obj->get_id(), $user_obj);
    } else {
      $markup .= "<p>No username_id.</p>";
    }

    // todo consider visits with respect to land
    //$markup .= "<p>";
    //$markup .= "go to: <a href=\"visits?land_id=" . $this->get_id() . "\">visits</a>\n";
    //$markup .= "</p>";

    // consider soil tests with respect to land
    //$markup .= "<p>";
    //$markup .= "go to: <a href=\"soil_tests?land_id=" . $this->get_id() . "\">soil tests</a>\n";
    //$markup .= "</p>";

    // land_traits
    $markup .= "<h3>LandTraits</h3>\n";
    include_once("land_traits.php");
    $username_traits_obj = new LandTraits($this->get_given_config());
    if ($username_obj->get_id()) {
      $user_obj = $this->get_user_obj();
      $markup .= $username_traits_obj->get_table_given_land_id($username_obj->get_id(), $user_obj);
    } else {
      $markup .= "<p>No username_id.</p>";
    }
    

    return $markup;
  }

  // method
  public function get_build_given_id($given_email_address_id, $given_user_obj) {
    $markup = "";

    $this->set_given_email_address_id($given_email_address_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug lands given_id " . $this->get_given_id() . "<br />\n";

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no usernames were found.</p>\n";;
      return $markup;
    }

    $markup .= "<h3 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px;\">usernames</h3>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $username) {
      $num++;

      // add space for breathing room
      $markup .= "<br />\n";

      // num
      //$markup .= "  <p>\n";
      //$markup .= "    " . $num . "<br />\n";
      //$markup .= "  </p>\n";

      // name
      $username->set_given_email_address_id($this->get_given_email_address_id());
      $markup .= "    <h1>" . $username->get_name() . "</h1>\n";

      // id
      $markup .= "  <p><em>This is land id " . $username->get_id() . "</em></p>\n";

      // description
      $markup .= "  <p>\n";
      $markup .= "    " . $username->get_description() . "\n";
      //$markup .= "    <div style=\"float: right;\"><em>sort = " . $username->get_sort() . "</em></div>\n";
      $markup .= "  </p>\n";

    }

    return $markup;
  }

  // method
  public function authenticate_via_db($given_username, $given_password) {

    // debug
    //print "debug usernames: authenticate_via_db()<br />\n";

    $this->set_given_username($given_username);
    $this->set_given_password($given_password);

    // load data from database
    //$this->determine_type();
    // hard-code this to be sure of the sql
    $this->set_type("get_by_username_and_by_password");

    // debug
    //print "debug usernames type = " . $this->get_type() . "<br />\n";

    $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 1) {
      // success
      $this->set_authenticated($this->get_list_bliss()->get_first_element());
      // username was found in db, set cookie to establish login persistence
      $this->set_cookie($this->get_list_bliss()->get_first_element()->get_username(), $this->get_list_bliss()->get_first_element()->get_username());
      return 1;
    }

    return;
  }

  // method
  public function set_cookie($username_for_cookie, $given_username) {

    // add one year of seconds
    $expires_time = time() + 31556926;

    if ($this->get_given_config()->get_debug()) {
      // debug
      //print "debug usernames cookie expires_time = " . $expires_time . "<br />\n";
    }

    // set cookie
    $coded_cookie_word = $this->make_coded_cookie_word($username_for_cookie);
    setcookie(self::FRB_NAME, $coded_cookie_word, $expires_time, '/');

    // update database
    $this->update_coded_cookie_word($coded_cookie_word, $given_username);

    if ($this->get_given_config()->get_debug()) {
      //print "debug usernames: setcookie()<br />\n";
    }

  }

  // method
  private function update_coded_cookie_word($given_coded_cookie_word, $given_username) {

    // fix for database
    $sql = "UPDATE usernames set coded_cookie_word = '" . $given_coded_cookie_word . "' WHERE username = '" . $given_username . "';";
    $error_message = $this->get_db_dash()->new_update($this, $sql);

    // debug
    //print "debug usernames update sql = " . $sql . "<br />\n";

    return $error_message;

  }

  // method
  public function is_user_requesting_logout_command() {
    if (isset($_GET['action']) && $_GET['action'] == "logout") {
      return  1;
    }

    return 0;
  }

  // method
  public function do_logout_command() {
    $markup = "";

    if (isset($_GET['action']) && $_GET['action'] == "logout") {
      // get current username
      $username = $this->get_username_from_cookie();
      // set cookie to off
      unset($_COOKIE[self::FRB_NAME]);
      setcookie(self::FRB_NAME, null, -1, '/');
      // delete database coded_cookie_word
      $coded_cookie_word = "";
      $this->update_coded_cookie_word($coded_cookie_word, $username);
    }

    return $markup;
  }
 
  // method
  public function get_username_from_cookie() {
    // is there a cookie?
    if (isset($_COOKIE[self::FRB_NAME]) && $_COOKIE[self::FRB_NAME]) {
      $cookie_value = $_COOKIE[self::FRB_NAME];
      list($username, $coded_cookie_word) = explode(",", $cookie_value);
      return $username;
    }
    return "";
  }

  // method
  public function check_for_valid_cookie() {
    $markup = "";

    // debug
    //$markup .= "debug usernames check_for_valid_cookie()<br />\n";

    // is there a cookie?
    if (isset($_COOKIE[self::FRB_NAME]) && $_COOKIE[self::FRB_NAME]) {

      // debug
      $markup .= "debug usernames: cookie exists<br />\n";

      if ($this->get_given_config()->get_debug()) {
        // debug
        $markup .=  "debug usernames: coded_cookie_word of cookie = " . $_COOKIE[self::FRB_NAME] . "<br />\n";
      }

      // check if cookie matches db
      $username_obj_from_db = $this->is_user_cookie_same_as_db($_COOKIE[self::FRB_NAME]);
      if ($username_obj_from_db) {

        // debug
        $markup .=  "debug usernames coded_cookie_word matches<br />\n";

        // set authenticated
        $this->set_authenticated($username_obj_from_db);

      } else {
        // debug
        $markup .= "debug usernames cookie is not valid<br />\n";
     }

    } else {
      // debug
      $markup .= "debug usernames: cookie not set<br />\n";
    }

    return $markup;
  }

  // method
  public function is_valid_cookie() {
    if ($this->get_authenticated()) {
      return 1;
    }
    return 0;
  }

  // method
  public function set_cookie_now() {
    $this->set_cookie($_COOKIE[self::FRB_NAME]);
  }

  // method
  private function is_user_cookie_same_as_db($given_coded_cookie_word) {
    $markup = "";

    $this->set_given_coded_cookie_word($given_coded_cookie_word);

    // load data from database
    $this->determine_type();

    $this->prepare_query();

    if ($this->get_list_bliss()->get_count() == 1) {
      // debug
      //print "debug usernames db has coded_cookie_word that is valid<br />\n";
      return $this->get_list_bliss()->get_first_element();
    }

    // debug
    //print "debug usernames db has coded_cookie_word that is NOT valid<br />\n";
    return 0;
  }

  // method
  private function make_coded_cookie_hash($username) {
     $preface = "999";
     $plain_text = $preface . $username;
     $cookie_hash = md5($plain_text);    
     return $cookie_hash;
  }

  // method
  private function make_coded_cookie_word($username) {
     $cookie_value = $username . "," . $this->make_coded_cookie_hash($username);
     // debug
     //print "debug usernames cookie_value = " . $cookie_value . "<br />\n";
     return $cookie_value;
  }

  // method
  private function get_username_from_from_coded_cookie_word($cookie_value) {
     list($username, $coded_cookie_word) = explode(",", $cookie_value);
     return $username;
  }

  // method
  private function get_coded_cookie_word_from_from_cookie_value($cookie_value) {
     list($username, $coded_cookie_word) = explode(",", $cookie_value);
     return $coded_cookie_word;
  }

  // method
  public function get_sidecar_given_user_name($given_user_name, $given_user_obj) {
    $markup = "";

    $this->set_given_username($given_user_name);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no usernames were found.</p>\n";;
      return $markup;
    }

    // rows
    $username = $this->get_list_bliss()->get_first_element();

    $markup .= "username: " . $username->get_username() . "<br />\n";
    $markup .= "email_address_id: " . $username->get_email_address_obj()->get_id() . "<br />\n";
    $markup .= "        <strong><em>email_addresses instance has field:</em></strong><br />" . "\n";
    $markup .= "id: " . $username->get_email_address_obj()->get_id() . "<br />\n";
    include_once("email_addresses.php");
    $email_address_obj = new EmailAddresses($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= "email_address: " . $email_address_obj->get_email_address_given_id($username->get_email_address_obj()->get_id(), $user_obj);

    return $markup;
  }

}
