<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/pickup_details.php

include_once("lib/scrubber.php");

class PickupDetails extends Scrubber {

  // given
  private $given_id;
  private $given_land_id;
  private $given_project_id;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_land_id
  public function set_given_land_id($var) {
    $this->given_land_id = $var;
  }
  public function get_given_land_id() {
    return $this->given_land_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // attributes
  private $id;
  private $date;
  private $land_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // land_obj
  public function get_land_obj() {
    if (! isset($this->land_obj)) {
      include_once("lands.php");
      $this->land_obj = new Lands($this->get_given_config());
    }
    return $this->land_obj;
  }

  // method
  private function make_pickup_detail() {
    $obj = new PickupDetails($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_land_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_project_id()) {
      // subset
      $this->set_type("get_by_project_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT pickup_details.*, lands.name FROM pickup_details, lands WHERE pickup_details.id = " . $this->get_given_id() . " AND pickup_details.land_id = lands.id ORDER BY pickup_details.date;";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT pickup_details.*, lands.name FROM pickup_details, lands WHERE pickup_details.land_id = lands.id ORDER BY pickup_details.date, lands.status;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_pickup_detail();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_date(pg_result($results, $lt, 1));
        $obj->get_land_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_land_obj()->set_name(pg_result($results, $lt, 3));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";

    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    land\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    pickup(s) <em>at this pickup_detail</em>\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    invoice_lines\n";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $previous_previous_date = "";
    $previous_date = "";
    foreach ($this->get_list_bliss()->get_list() as $pickup_detail) {
      $markup .= "<tr>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $pickup_detail->get_id() . "\n";
      $markup .= "  </td>\n";

      // date
      $markup .= "  <td style=\"text-align: center;\">\n";
      if ($previous_date == $pickup_detail->get_date()) {
        $markup .= "    <span style=\"color: #999999;\">same</span>\n";
      } else {
        $markup .= "    " . $pickup_detail->get_date() . "\n";
      }
     $markup .= "  </td>\n";

      // land
      $markup .= "  <td>\n";
      $markup .= "    " . $pickup_detail->get_land_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // pickups
      $markup .= "  <td>\n";
      include_once("pickups.php");
      $pickup_obj = new Pickups($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $pickup_obj->get_invoices_given_pickup_detail_id($pickup_detail->get_id(), $user_obj);
      $markup .= "  </td>\n";

      // invoice_lines
      if (0) {
        $markup .= "  <td>\n";
        include_once("invoice_lines.php");
        $invoice_line_obj = new InvoiceLines($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $invoice_line_obj->get_invoice_lines_given_pickup_detail_id($pickup_detail->get_id(), $user_obj);
        $markup .= "  </td>\n";
      }

      $markup .= "</tr>\n";

      // set for next iteration (if any)
      $previous_previous_date = $previous_date;
      $previous_date = $pickup_detail->get_date();
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function output_aggregate_small() {
    $markup = "";

    //$markup .= "<p>pickup id = " . $this->get_given_pickup_id() . "</p>\n";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    land\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $pickup_detail) {
      $markup .= "<tr>\n";

      $markup .= "  <!-- id = " . $pickup_detail->get_id() . " -->\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $pickup_detail->get_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"left\">\n";
      $markup .= "    " . $pickup_detail->get_land_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<a href=\"project?id=" . $this->get_given_project_id() . "\">Back to project page</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // note: all varieties are public

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    // output given
    //$markup .= "<div class=\"given-variables\">\n";
    //$markup .= "    <em>These crop plans are of <strong>project_season_id = </em>" . $this->get_given_project_season_id() . "</strong>\n";
    //$markup .= "    <em<>and project <em>" . $this->get_project_obj()->get_name_with_link()  . "</em></p>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  public function output_table_given_pickup_id($given_pickup_id) {
    $markup = "";

    // set
    $this->set_given_pickup_id($given_pickup_id);

    // get data
    $this->determine_type();
    $this->prepare_query();

    $markup .= $this->output_aggregate_small();

    return $markup;
  }

  // methid
  public function output_table_given_harvest_id($given_harvest_id, $given_user_obj) {
    $markup = "";
 
    // set
    $this->set_given_harvest_id($given_harvest_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $this->prepare_query();

    $markup = "<h2>Pickup Details of this Harvest</h2>\n";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function get_pickup_details() {
    $user_obj = $this->get_user_obj();
    return $this->get_pickup_detail_given_id($this->get_id(), $user_obj);
  }

  // method
  public function get_pickup_detail_given_id($given_id, $given_user_obj) {
    $markup = "";
 
    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $this->prepare_query();

    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";

    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    land\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $previous_date = "";
    foreach ($this->get_list_bliss()->get_list() as $pickup_detail) {
      $markup .= "<tr>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $pickup_detail->get_id() . "\n";
      $markup .= "  </td>\n";

      // date
      $markup .= "  <td style=\"text-align: center;\">\n";
      if ($previous_date == $pickup_detail->get_date()) {
        $markup .= "    <span style=\"color: #999999;\">same</span>\n";
      } else {
        $markup .= "    " . $pickup_detail->get_date() . "\n";
      }
      $markup .= "  </td>\n";

      // land
      $markup .= "  <td style=\"width: 300px;\">\n";
      $markup .= "    " . $pickup_detail->get_land_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_preface() {
    $markup = "";
    return $markup;
  }

}
