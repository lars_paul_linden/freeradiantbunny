<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-11

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/webpage_moneymakers.php

include_once("lib/sproject.php");

class WebpageMoneymakers extends Sproject {

  // given
  private $given_domain_tli;
  private $given_webpage_id;
  private $given_moneymaker_id;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_webpage_id
  public function set_given_webpage_id($var) {
    $this->given_webpage_id = $var;
  }
  public function get_given_webpage_id() {
    return $this->given_webpage_id;
  }

  // given_moneymaker_id
  public function set_given_moneymaker_id($var) {
    $this->given_moneymaker_id = $var;
  }
  public function get_given_moneymaker_id() {
    return $this->given_moneymaker_id;
  }

  // attributes
  private $id;
  private $webpage_obj;
  private $moneymaker_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // webpage_obj
  public function get_webpage_obj() {
    if (! isset($this->webpage_obj)) {
      include_once("webpages.php");
      $this->webpage_obj = new Webpages($this->get_given_config());
    }
    return $this->webpage_obj;
  }

  // moneymaker_obj
  private function get_moneymaker_obj() {
    if (! isset($this->moneymaker_obj)) {
      include_once("moneymakers.php");
      $this->moneymaker_obj = new Moneymakers($this->get_given_config());
    }
    return $this->moneymaker_obj;
  }

  // method
  private function make_webpage_moneymaker() {
    $obj = new WebpageMoneymakers($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_webpage_id()) {
      $this->set_type("get_by_webpage_id");

    } else if ($this->get_given_moneymaker_id()) {

      if ($this->get_given_domain_tli()) {
        $this->set_type("get_by_moneymaker_id_and_domain_tli");
      } else {
        $this->set_type("get_by_moneymaker_id");
      }

    } else if ($this->get_given_domain_tli()) {
        $this->set_type("get_by_domain_tli");
    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_moneymakers.*, webpages.*, moneymakers.*, domains.tli, domains.name, domains.img_url FROM webpage_moneymakers, moneymakers, webpages, domains WHERE webpage_moneymakers.moneymaker_id = moneymakers.id AND webpage_moneymakers.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND webpage_moneymakers.id = " . $this->get_given_id() . " AND domains.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_moneymakers.*, webpages.*, moneymakers.*, domains.tli, domains.name, domains.img_url FROM webpage_moneymakers, moneymakers, webpages, domains WHERE webpage_moneymakers.moneymaker_id = moneymakers.id AND webpage_moneymakers.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND domains.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_by_webpage_id") {
      // security: only get the rows owned by the user

      // debug
      //print "debug webpage_moneymakers sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_moneymaker_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_moneymakers.*, webpages.*, moneymakers.*, domains.tli, domains.name, domains.img_url FROM webpage_moneymakers, moneymakers, webpages, domains WHERE webpage_moneymakers.moneymaker_id = moneymakers.id AND webpage_moneymakers.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND moneymakers.id = " . $this->get_given_moneymaker_id() . " AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY moneymakers.id;";

      // debug
      //print "debug webpage_moneymakers sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_moneymaker_id_and_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT webpage_moneymakers.*, webpages.*, moneymakers.*, domains.tli, domains.name, domains.img_url FROM webpage_moneymakers, moneymakers, webpages, domains WHERE webpage_moneymakers.moneymaker_id = moneymakers.id AND webpage_moneymakers.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND moneymakers.id = " . $this->get_given_moneymaker_id() . " AND webpages.domain_tli = '" . $this->get_given_domain_tli() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY moneymakers.id;";

      // debug
      //print "debug webpage_moneymakers sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "SELECT DISTINCT webpage_moneymakers.*, webpages.*, moneymakers.*, domains.tli, domains.name, domains.img_url FROM webpage_moneymakers, moneymakers, webpages, domains WHERE webpage_moneymakers.moneymaker_id = moneymakers.id AND webpage_moneymakers.webpage_id = webpages.id AND webpages.domain_tli = domains.tli AND webpages.domain_tli = '" . $this->get_given_domain_tli() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY moneymakers.id;";

      // debug
      //print "debug webpage_moneymakers sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "mudiacom_tenperday";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_webpage_id" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_by_moneymaker_id" ||
        $this->get_type() == "get_by_moneymaker_id_and_domain_tli") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage_moneymaker();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_webpage_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_moneymaker_obj()->set_id(pg_result($results, $lt, 2));
        $obj->get_webpage_obj()->set_id(pg_result($results, $lt, 3));
        $obj->get_webpage_obj()->get_domain_obj()->set_tli(pg_result($results, $lt, 4));
        $obj->get_webpage_obj()->set_description(pg_result($results, $lt, 5));
        $obj->get_webpage_obj()->set_name(pg_result($results, $lt, 6));
        $obj->get_webpage_obj()->set_sort(pg_result($results, $lt, 7));
        $obj->get_webpage_obj()->set_img_url(pg_result($results, $lt, 8));
        $obj->get_webpage_obj()->set_status(pg_result($results, $lt, 9));
        $obj->get_webpage_obj()->set_path(pg_result($results, $lt, 10));
        $obj->get_moneymaker_obj()->set_id(pg_result($results, $lt, 11));
        $obj->get_moneymaker_obj()->set_name(pg_result($results, $lt, 12));
        $obj->get_moneymaker_obj()->set_description(pg_result($results, $lt, 12));
        $obj->get_moneymaker_obj()->set_sort(pg_result($results, $lt, 12));
        $obj->get_moneymaker_obj()->set_status(pg_result($results, $lt, 13));
        $obj->get_moneymaker_obj()->set_user_name(pg_result($results, $lt, 14));
        $obj->get_moneymaker_obj()->set_img_url(pg_result($results, $lt, 15));
        $obj->get_webpage_obj()->get_domain_obj()->set_id(pg_result($results, $lt, 16));
        $obj->get_webpage_obj()->get_domain_obj()->set_name(pg_result($results, $lt, 17));
        $obj->get_webpage_obj()->get_domain_obj()->set_img_url(pg_result($results, $lt, 18));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_given_project_id()) {
      $markup .= $this->output_given_variables_project($this->get_list_bliss()->get_count());
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    webpage_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    moneymaker_id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    report\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage_moneymaker) {
      $num++;
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $webpage_moneymaker->get_webpage_obj()->get_domain_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $webpage_moneymaker->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $webpage_moneymaker->get_webpage_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= $webpage_moneymaker->get_moneymaker_obj()->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // report
      //$markup .= "<div class=\"paragraphs\">\n";
      //$markup .= "<h2>report</h2>\n";
      //if ($obj->get_report()) {
      //  $markup .= "<p>" . $obj->get_report() . "</p>\n";
      //} else {
      //  // error
      //  $markup .= $this->get_db_dash()->output_error("Error: No report.");
      //  // todo the line below was not working
      //  //$markup .= "<p style=\"error\">No report.</p>\n";
      //}
      //$markup .= "</div><!-- end class paragraphs -->\n";

      $markup .= "</tr>\n";
    }

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup = "not yet built in webpage_moneymakers";

    return $markup;
  }

  // method
  public function get_list_of_moneymaker_obj_given_webpage_id($given_webpage_id, $given_user_obj) {

    $moneymaker_obj_array = array();

    // set
    $this->set_given_webpage_id($given_webpage_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    foreach ($this->get_list_bliss()->get_list() as $obj) { 
      array_push($moneymaker_obj_array, $obj->get_moneymaker_obj());
    }

    return $moneymaker_obj_array;
  }


  // method
  public function get_webpage_list_given_moneymaker_id($given_moneymaker_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_moneymaker_id($given_moneymaker_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      return $markup;
    }

    $markup .= "<ul>\n";
    foreach ($this->get_list_bliss()->get_list() as $obj) { 
      $markup .= "<li>";
      // print tli before link so that the domain is known
      $markup .= $obj->get_webpage_obj()->get_domain_tli();
      $markup .= " ";
      $markup .= $obj->get_webpage_obj()->get_path_with_link();
      $markup .= "</li>\n";
    }
    $markup .= "<ul>\n";

    return $markup;;
  }

  // method
  public function get_webpage_list_given_moneymaker_id_and_domain_tli($given_moneymaker_id, $given_domain_tli, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_moneymaker_id($given_moneymaker_id);
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      return $markup;
    }

    $markup .= "<ul>\n";
    foreach ($this->get_list_bliss()->get_list() as $obj) { 
      $markup .= "<li>";
      // print tli before link so that the domain is known
      $markup .= $obj->get_webpage_obj()->get_domain_obj()->get_tli();
      $markup .= " ";
      $markup .= $obj->get_webpage_obj()->get_path_with_link();
      $markup .= "</li>\n";
    }
    $markup .= "<ul>\n";

    return $markup;;
  }

  // method
  public function get_count_given_tli($given_domain_tli, $given_user_obj) {

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check to see if there were any errors 
    if ($markup) {
      // todo this will print at the top of the screen, move to body of page
      print $markup . "<br />\n";;
    }

    return $this->get_list_bliss()->get_count();
  }

  // method
  protected function output_preface() {
    $markup = "";

    // todo should this be in the parent class?
    $markup .= "<br />\n";

    return $markup;
  }

}
