<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/book_clips.php

include_once("lib/scrubber.php");

class BookClips extends Scrubber {

  // attribute
  private $id;
  private $image_obj;
  private $plant_obj;
  private $excerpt;
  private $see_also;
  private $book_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // image_obj
  public function get_image_obj() {
    if (! isset($this->image_obj)) {
      include_once("images.php");
      $this->image_obj = new Images($this->get_given_config());
    }
    return $this->image_obj;
  }

  // plant_obj
  public function get_plant_obj() {
    if (! isset($this->plant_obj)) {
      include_once("plants.php");
      $this->plant_obj = new Plants($this->get_given_config());
    }
    return $this->plant_obj;
  }

  // book_obj
  public function get_book_obj() {
    if (! isset($this->book_obj)) {
      include_once("books.php");
      $this->book_obj = new Books($this->get_given_config());
    }
    return $this->book_obj;
  }

  // excerpt
  public function set_excerpt($var) {
    $this->excerpt = $var;
  }
  public function get_excerpt() {
    return $this->excerpt;
  }

  // see_also
  public function set_see_also($var) {
    $this->see_also = $var;
  }
  public function get_see_also() {
    return $this->see_also;
  }

  // method
  private function make_book_clip() {
    $obj = new BookClips($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      // single
      $this->set_type("get_by_id");

    } else {
      // default is aggregate
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_id") {
      $sql = "SELECT book_clips.id, book_clips.book_id, books.title FROM book_clips, books WHERE book_clips.book_id = books.id AND book_clips.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_all") {
      $sql = "SELECT book_clips.id, book_clips.book_id, books.title FROM book_clips, books WHERE book_clips.book_id = books.id;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_id") {
      $this->get_list_bliss()->add_item($this);  
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $this->set_id(pg_result($results, $lt, 0));
        $this->get_book_obj()->set_id(pg_result($results, $lt, 1));
        $this->get_book_obj()->set_title(pg_result($results, $lt, 2));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_book_clip();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_book_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_book_obj()->set_title(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    book\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $book_clip) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $book_clip->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td width=\"400\">\n";
      $markup .= "    " . $book_clip->get_book_obj()->get_title_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

}
