<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-17
// version 1.2 2015-01-04
// version 1.4 2015-06-19
// version 1.5 2015-10-16
// version 1.6 2016-03-25
// version 1.6 2017-08-03

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/scene_elements.php

include_once("lib/standard.php");

class SceneElements extends Standard {

  // parameters
  private $given_id;
  private $given_project_id;
  private $given_process_id;
  private $given_class_name_string;
  private $given_class_primary_key_string;
  private $given_budget_id;
  private $given_plant_list_id;
  private $given_class_id;
  private $given_filter;

  // given_id
  public function set_given_id($var) {
    $this->given_id = $var;
  }
  public function get_given_id() {
    return $this->given_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_class_name_string
  public function set_given_class_name_string($var) {
    $this->given_class_name_string = strtolower($var);
  }
  public function get_given_class_name_string() {
    return $this->given_class_name_string;
  }

  // given_class_primary_key_string
  public function set_given_class_primary_key_string($var) {
    $this->given_class_primary_key_string = $var;
  }
  public function get_given_class_primary_key_string() {
    return $this->given_class_primary_key_string;
  }

  // given_budget_id
  public function set_given_budget_id($var) {
    $this->given_budget_id = $var;
  }
  public function get_given_budget_id() {
    return $this->given_budget_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_class_id
  public function set_given_class_id($var) {
    $this->given_class_id = $var;
  }
  public function get_given_class_id() {
    return $this->given_class_id;
  }

  // given_filter
  public function set_given_filter($var) {
    $this->given_filter = $var;
  }
  public function get_given_filter() {
    return $this->given_filter;
  }

  // attributes
  private $process_obj;
  private $database_string;
  private $class_name_string;
  private $class_primary_key_string;
  private $yield;
  private $scenes_obj;

  // process_obj
  public function get_process_obj() {
    if (! isset($this->process_obj)) {
      include_once("processes.php");
      $this->process_obj = new Processes($this->get_given_config());
    }
    return $this->process_obj;
  }

  // method
  public function get_sort_no_z() {
    // validate variable before processes
    if (! $this->get_sort()) {
      return "[error: sort_no_z is not defined]";
    }
    // assumes "Z " is at the beginning of string
    return substr($this->get_sort(), 2);
  }

  // database_string
  public function set_database_string($var) {
    $this->database_string = $var;
  }
  public function get_database_string() {
    return $this->database_string;
  }

  // class_name_string
  public function set_class_name_string($var) {
    $this->class_name_string = $var;
  }
  public function get_class_name_string() {
    return $this->class_name_string;
  }

  // class_primary_key_string
  public function set_class_primary_key_string($var) {
    $this->class_primary_key_string = $var;
  }
  public function get_class_primary_key_string() {
    return $this->class_primary_key_string;
  }

  // yield
  public function set_yield($var) {
    $this->yield = $var;
  }
  public function get_yield() {
    return $this->yield;
  }

  // scenes_obj
  private function get_scenes_obj() {
    if (! $this->scenes_obj) {
      include_once("lib/scenes.php");
      $this->scenes_obj = new Scenes($this->get_given_config());
    }
    return $this->scenes_obj;
  }

  // method
  private function make_scene_element() {
    $obj = new SceneElements($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_class_name_string() && $this->get_given_class_primary_key_string()) {
      $this->set_type("get_by_class_name_and_primary_key");

    } else if ($this->get_given_budget_id()) {
      $this->set_type("get_by_budget_id");

    } else if ($this->get_given_plant_list_id()) {
      $this->set_type("get_by_plant_list_id");

    } else if ($this->get_given_class_id()) {
      $this->set_type("get_by_class_id");

    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug scene_elements type = " . $this->get_type() . "<br />\n";;

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "select scene_elements.*, processes.name, processes.sort from scene_elements, processes, business_plan_texts, goal_statements, projects WHERE processes.id = scene_elements.process_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND scene_elements.id = " . $this->get_given_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug scene_elements sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      // complexity might be hiding a mistake
      //$sql = "select scene_elements.*, processes.name, processes.sort from scene_elements, processes, business_plan_texts, goal_statements, projects WHERE processes.id = scene_elements.process_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY scene_elements.sort DESC, projects.name, scene_elements.id;";
      $sql = "select scene_elements.* from scene_elements ";
      if ($this->get_given_filter() == "zoneline") {
        $sql .= " WHERE status='" . $this->get_given_filter() . "' ";
      } 
      $sql .= "ORDER BY scene_elements.status DESC, scene_elements.sort DESC, scene_elements.name;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT scene_elements.*, processes.name, processes.sort FROM scene_elements, processes, business_plan_texts, goal_statements, projects WHERE processes.id = scene_elements.process_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY scene_elements.sort DESC, processes.name, scene_elements.id;";

      // debug
      //print "debug scene_elements sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_process_id") {
      // security: only get the rows owned by the user
      $sql = "select scene_elements.*, processes.name, processes.sort from scene_elements, processes, business_plan_texts, goal_statements, projects WHERE processes.id = scene_elements.process_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND processes.id = " . $this->get_given_process_id() . " ORDER BY scene_elements.class_name_string, scene_elements.class_primary_key_string, scene_elements.sort DESC, scene_elements.id;";

    } else if ($this->get_type() == "get_by_class_name_and_primary_key") {
      $sql = "select scene_elements.*, processes.name, processes.sort from scene_elements, processes, business_plan_texts, goal_statements, projects WHERE processes.id = scene_elements.process_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND scene_elements.class_name_string = '" . $this->get_given_class_name_string() . "' AND scene_elements.class_primary_key_string = '" . $this->get_given_class_primary_key_string() . "' AND projects.user_name = '" . $this->get_user_obj()->name . "';";

      // debug
      //print "debug scene_elements sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_budget_id") {
      // security: only get the rows owned by the user
      // todo this sql should be rewritten in order of links (associations)
      $sql = "select scene_elements.*, processes.name, processes.sort from scene_elements, processes, business_plan_texts, goal_statements, projects, budgets WHERE processes.id = scene_elements.process_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND budgets.id = " . $this->get_given_budget_id() . " AND budgets.scene_element_id = scene_elements.id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY processes.sort DESC, processes.name;";

    } else if ($this->get_type() == "get_by_plant_list_id") {
      // security: only get the rows owned by the user
      // simple list
      //$sql = "select scene_elements.* from scene_elements ORDER BY scene_elements.id;";
      $sql = "select scene_elements.* from scene_elements WHERE scene_elements.class_name_string = 'plant_lists' AND scene_elements.class_primary_key_string = '" . $this->get_given_plant_list_id() . "';";

      // debug
      //print "debug scene_elements sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_class_id") {
      $sql = "select scene_elements.* from scene_elements WHERE scene_elements.class_name_string = 'classes' AND scene_elements.class_primary_key_string = '" . $this->get_given_class_id() . "';";

      // debug
      //print "debug scene_elements sql = " . $sql . "<br />\n";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_by_budget_id" ||
        $this->get_type() == "get_by_class_name_and_primary_key") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_scene_element();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_process_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_database_string(pg_result($results, $lt, 3));
        $obj->set_class_name_string(pg_result($results, $lt, 4));
        $obj->set_class_primary_key_string(pg_result($results, $lt, 5));
        $obj->set_description(pg_result($results, $lt, 6));
        $obj->set_yield(pg_result($results, $lt, 7));
        $obj->set_status(pg_result($results, $lt, 8));
        $obj->set_img_url(pg_result($results, $lt, 9));
        $obj->set_name(pg_result($results, $lt, 10));
        $obj->get_process_obj()->set_name(pg_result($results, $lt, 11));
        $obj->get_process_obj()->set_sort(pg_result($results, $lt, 12));
      }
    } else if ($this->get_type() == "get_all" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_by_budget_id" ||
        $this->get_type() == "get_by_class_name_and_primary_key") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_scene_element();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_process_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_database_string(pg_result($results, $lt, 3));
        $obj->set_class_name_string(pg_result($results, $lt, 4));
        $obj->set_class_primary_key_string(pg_result($results, $lt, 5));
        $obj->set_description(pg_result($results, $lt, 6));
        $obj->set_yield(pg_result($results, $lt, 7));
        $obj->set_status(pg_result($results, $lt, 8));
        $obj->set_img_url(pg_result($results, $lt, 9));
        $obj->set_name(pg_result($results, $lt, 10));
      }
    } else if ($this->get_type() == "get_by_plant_list_id" ||
               $this->get_type() == "get_by_class_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_scene_element();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_process_obj()->set_id(pg_result($results, $lt, 1));
        $obj->set_sort(pg_result($results, $lt, 2));
        $obj->set_database_string(pg_result($results, $lt, 3));
        $obj->set_class_name_string(pg_result($results, $lt, 4));
        $obj->set_class_primary_key_string(pg_result($results, $lt, 5));
        $obj->set_description(pg_result($results, $lt, 6));
        $obj->set_yield(pg_result($results, $lt, 7));
        $obj->set_status(pg_result($results, $lt, 8));
        $obj->set_img_url(pg_result($results, $lt, 9));
        $obj->set_name(pg_result($results, $lt, 10));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }
  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    $markup .= "<div class=\"subsubmenu\">\n";

    // all
    $url = $this->url("scene_elements");
    $markup .= "<p><a href=\"" . $url . "\">All scene elements</a></p>\n";

    // all for this project
    //if ($this->get_given_project_id()) {
    //  $url = $this->url("scene_elements/project/" . $this->get_given_project_id());
    //  $markup .= "<p><a href=\"" . $url . "\">All scene elements for this project</a></p>\n";
    //}

    $markup .= "</div>\n";

    return $markup;
  }

  // method
  protected function output_given_variables() {
    $markup = "";

    // debug
    //print "debug scene elements type = " . $this->get_type() . "<br />\n";

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_given_project_id") {
      $markup .= "<div class=\"given-variables\">\n";
      if ($this->get_type() == "get_by_id") {
        $markup .= "The SceneElement is <strong>" . $this->get_given_id() . "</strong>.\n";
      } else if ($this->get_type() == "get_by_process_id") {
        $verb = "These are";
        $plural = "s";
        if ($this->get_list_bliss()->get_count() > 1) {
          if ($this->get_list_bliss()->get_count() == 1) {
            $verb = "This is";
            $plural = "";
          }
          $markup .= $verb . " SceneElement" . $plural . " of process <strong>" . $this->get_list_bliss()->get_first_element()->get_process_obj()->get_name_with_link() . "</strong>.\n";
        }
      } else if ($this->get_type() == "get_given_project_id") {
        $markup .= "These are SceneElements of project id <strong>" . $this->get_given_project_id() . "</strong>.\n";

      } else if ($this->get_type() == "get_all") {
        $markup .= "All SceneElements are listed.\n";

      } else {
        $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to display \"given\" variables.");

      }
      $markup .= "</div><!-- given-variables -->\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // view
    $markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  private function output_table() {
    $markup = "";
 
    if ($this->get_given_view() == "all") {

      if ($this->get_given_process_id()) {

        // set up for scene_elements for scene aggregate
        include_once("scene_elements.php");
        $scene_element_obj = new SceneElements($this->get_given_config());
        $user_obj = $this->get_user_obj();

        // socation scene play of scene_elements
        $heading = "true";
        // todo test to see if this turns off display
        //$markup .= $scene_element_obj->display($this->get_given_process_id(), $user_obj, $heading);

        // add a buffer of space
        $markup .= "<br />\n";
      }

      $markup .= $this->output_sidecar_table();

    } else if ($this->get_given_view() == "by-parent-class-name") {

        // part 1 of 3 pre-process the data into array by class_name
        $parent_hash = array();
        foreach ($this->get_list_bliss()->get_list() as $scene_element) {
          // set         
          $user_obj = $this->get_user_obj();
          $scene_element->set_user_obj($user_obj);
          // process
          $field_to_return = "class-name";
          $class_name = $scene_element->get_polymorphic_object_as_details($field_to_return);
          // data structure is an array of arrays, so build appropriately
          // is this the first object of this class_name
          if (array_key_exists($class_name, $parent_hash)) {
            // add to value's array
            $se_array = $parent_hash[$class_name];
            array_push($se_array, $scene_element);
            $parent_hash[$class_name] = $se_array;
          } else {
            // add to a new values's array
            $se_array = array($scene_element);
            $parent_hash[$class_name] = $se_array;
          }
        }

        // part 2 of 3 sort
        // sort by key
        ksort($parent_hash);

        // set globals
        $num_grand = 0;
        // total figure out how to add yield_total when not all numeric
        //$yield_total = 0;

        // part 3 of 3 output the results by looping through the hash
        foreach ($parent_hash as $class_name => $se_array) {
          // make class_name header
          $url = $this->url($class_name);
          $markup .= "<h3><a href=\"" . $url . "\">" . $class_name . "</a></h3>\n";

          // debug
          //$markup .= "debug <p>count = " . count($se_array) . "</p>\n";

          // display s.e. in a table sorted by primary_key of s.e.
          // sort se_array by id
          $sorted_se_array = array();
          foreach ($se_array as $scene_element_obj) {
            $primary_key_id = $scene_element_obj->get_class_primary_key_string();
            if (array_key_exists($primary_key_id, $sorted_se_array)) {
              $a_se_array = $sorted_se_array[$primary_key_id];
              array_push($a_se_array, $scene_element_obj);
              $sorted_se_array[$primary_key_id] = $a_se_array;
            } else {
              $a_se_array = array($scene_element_obj);
              $sorted_se_array[$primary_key_id] = $a_se_array;
            }
          }
          // sort by key
          ksort($sorted_se_array);
          // display
          $num = 0;
          $markup .= "<table class=\"scene-elements-list\">\n";
          $markup .= "<tr>\n";
          $markup .= "  <td>\n";
          $markup .= "    #\n";
          $markup .= "  </td>\n";
          //$markup .= "  <td>\n";
          //$markup .= "    process id\n";
          //$markup .= "  </td>\n";
          $markup .= "  <td>\n";
          $markup .= "    s.e. id\n";
          $markup .= "  </td>\n";
          $markup .= "  <td style=\"\">\n";
          $markup .= "    scene element\n";
          $markup .= "  </td>\n";
          if (! $this->get_given_project_id()) {
            $markup .= "  <td>\n";
            $markup .= "    project\n";
            $markup .= "  </td>\n";
          }
          //$markup .= "  <td>\n";
          //$markup .= "    account\n";
          //$markup .= "  </td>\n";
          //$markup .= "  <td>\n";
          //$url = $this->url("budgets");
          //$markup .= "    <a href=\"" . $url . "\">";
          //$markup .= "budgets";
          //$markup .= "</a>\n";
          //$markup .= "  </td>\n";
          //$markup .= "  <td>\n";
          //$markup .= "    yield\n";
          //$markup .= "  </td>\n";
          $markup .= "</tr>\n";
          foreach ($sorted_se_array as $primary_key_id => $a_se_array) {
            foreach ($a_se_array as $scene_element_obj) {
              $num++;
              $markup .= "<tr>\n";

              $markup .= "  <td>\n";
              $markup .= "    " . $num . "\n";
              $markup .= "  </td>\n";

              // process id
              //$markup .= "  <td>\n";
              //$se_obj = new SceneElements($this->get_given_config());
              //$user_obj = $this->get_user_obj();
              //$markup .= $se_obj->get_process_name_given_scene_element_id($scene_element_obj->get_id(), $user_obj);
              //$markup .= "  </td>\n";

              $markup .= "  <td>\n";
              $url = $this->url("scene_elements/" . $scene_element_obj->get_id());
              $markup .= "    <a href=\"". $url . "\">" . $scene_element_obj->get_id() . "</a>\n";
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              $markup .= "    " . $scene_element_obj->get_polymorphic_object_as_details();
              $markup .= "  </td>\n";

              if (! $this->get_given_project_id()) {
                $markup .= "  <td>\n";
                $se_obj = new SceneElements($this->get_given_config());
                $user_obj = $this->get_user_obj();
                $markup .= $se_obj->get_project_name_given_scene_element_id($scene_element_obj->get_id(), $user_obj);
                $markup .= "  </td>\n";
              }

              // accounts  
              //$account_string = "";
              //$polymorphic_object_obj = $scene_element_obj->get_polymorphic_object_as_obj();
              //if (is_object($polymorphic_object_obj)) {
              //  $account_obj = $polymorphic_object_obj->get_account_obj();
              //  if (is_object($account_obj)) {
              //    $account_string = $account_obj->get_name();
              //    $markup .= "  <td class=\"mid\" style=\"width: 26px; text-align: center;background-color: #FFFFFF;\">\n";
              //  } else {
              //    $account_string = "Error scene_elements: no account_obj<br />\n";
              //    $markup .= "  <td class=\"mid\" style=\"width: 26px; text-align: center;background-color: #CD5555;\">\n";
              //  }
              //}
              //$markup .= "    " . $account_string . "\n";
              //$markup .= "  </td>\n";

              //$markup .= "  <td>\n";
              //$yield_total += $scene_element->get_yield();
              //$markup .= "    " . $scene_element->get_yield() . "\n";
              //$markup .= "  </td>\n";

              // budgets
              //$markup .= "  <td>\n";
              //include_once("budgets.php");
              //$budget_obj = new Budgets($this->get_given_config());
              //$user_obj = $this->get_user_obj();
              //$markup .= $budget_obj->get_budget_list_given_scene_element_id($scene_element_obj->get_id(), $user_obj);
              //$markup .= "  </td>\n";

              $markup .= "</tr>\n";
            }

          }
          $num_grand += $num;
          $markup .= "</table>\n";
          // debug
          //$markup .= "<p>debug num = $num</p>\n";
          //$markup .= "<p>debu num_grand = $num_grand</p>\n";
        }
        $markup .= "<p>total scene_elements = $num_grand</p>\n";
        //$markup .= "<p>total yield = $yield_total</p>\n";

    } else if ($this->get_given_view() == "date-stack") {

        // part 1 of 3 pre-process the data into array by class_name
        $parent_hash = array();

        // todo this has some errors
        //$markup .= "<p>";
        //foreach ($this->get_list_bliss()->get_list() as $scene_element) {
          //$markup .= $scene_element->get_class_primary_key_string();
        //}
        //$markup .= "</p>";

        $markup .= "<table class=\"plants\">\n";

        // heading
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    #\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    id\n";
        $markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\">\n";
        //$markup .= "    sort\n";
        //$markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\">\n";
        //$markup .= "    process\n";
        //$markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\">\n";
        //$markup .= "    class_name_string<br />\n";
        //$markup .= "    primary_key_string<br />\n";
        //$markup .= "    database_string<br />\n";
        //$markup .= "  </td>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    polymorphic object\n";
        $markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\">\n";
        //$markup .= "    resp. day count and sort\n";
        //$markup .= "  </td>\n";
        //$markup .= "  <td class=\"header\">\n";
        //$markup .= "    responsibilities\n";
        //$markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // sort by date
        $list_sorted_by_date = $this->get_list_bliss()->get_list();
        // old
        //usort($list_sorted_by_date,'obj_cmp');
        // replacement for objects
        usort($list_sorted_by_date, array('SceneElements','cmp_obj'));

        $num = 0;
        foreach ($list_sorted_by_date as $scene_element) {

          // set
          $user_obj = $this->get_user_obj();
          $scene_element->set_user_obj($user_obj);

          // process
          $markup .= "<tr>\n";

          $num++;
          $markup .= "  <td class=\"mid\">\n";
          $markup .= "    " . $num . "<br />\n";
          $markup .= "  </td>\n";

          $markup .= "  <td class=\"mid\" align=\"left\">\n";
          $url = $this->url("scene_elements/" . $scene_element->get_id());
          $markup .= "<a href=\"" . $url . "\">";
          $markup .= $scene_element->get_id();
          $markup .= "</a>\n";
          $markup .= "  </td>\n";

          //$markup .= "  <td class=\"mid\" style=\"width: 120px; text-align: center;\">\n";
          //$markup .= "    " . $scene_element->get_sort() . "<br />\n";
          //$markup .= "  </td>\n";

          //$markup .= "  <td class=\"mid\" align=\"left\">\n";
          //$markup .= "    " . $scene_element->get_process_obj()->get_name_with_link() . "<br />\n";
          //$markup .= "  </td>\n";

          //$markup .= "  <td class=\"mid\" align=\"left\">\n";
          //$markup .= "    " . $scene_element->get_database_string() . "<br />\n";
          //$markup .= "    " . $scene_element->get_class_name_string() . "<br />\n";
          //$markup .= "    " . $scene_element->get_class_primary_key_string() . "<br />\n";
          //$markup .= "  </td>\n";

          $markup .= "  <td class=\"mid\">\n";
          $markup .= "    " . $scene_element->get_polymorphic_object_as_details() . "\n";
          $markup .= "  </td>\n";

          /**
           * @todo had to comment the code below due to errors
           */
          // responsibility day count calculated
          //$responisibility_day_count = $scene_element->get_responsibility_day_count();
          //$sort = $scene_element->get_sort_no_z();
          //include_once("responsibilities.php");
          //$obj = new Responsibilities($this->get_given_config());
          //$background_color = $obj->get_report_background_color($responisibility_day_count, $sort);
          //$markup .= "  <td style=\"background-color: " . $background_color . ";width: 250px;\">\n";
          //$markup .= "    " . $scene_element->get_sort() . "\n";
          //$markup .= "  </td>\n";

          // element_responsibility
          //$markup .= "  <td class=\"mid\">\n";
          //include_once("scene_element_responsibilities.php");
          //$ser_obj = new SceneElementResponsibilities($this->get_given_config());
          //$user_obj = $this->get_user_obj();
          //$markup .= "    " . $ser_obj->get_pm_report($scene_element->get_id(), $user_obj) . "\n";
          //$markup .= "  </td>\n";

          $markup .= "</tr>\n";
        }
        $markup .= "</table>\n";
    }

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    // todo all this view stuff is in need of being tagged and refactored
    // todo refactor here for view, sort, filter
    $markup .= "<p>view: ";
    if ($this->get_given_view() == "by-parent-class-name") {
      $markup .= "by-parent-class-name</a>";
    } else {
      // output with link
      // old
      //$url = $this->url($_GET['q'], array('absolute' => true));
      // new
      //$url = $this->url("scene_elements/");
      // third attempt
      $parameter = "?view=by-parent-class-name";
      $request_uri = $this->remove_parameter($_SERVER['REQUEST_URI']);
      $url = $request_uri . $parameter;
      $markup .= "<a href=\"" . $url . "\">by-parent-class-name</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "all") {
      $markup .= "all";
    } else {
      // output with link
      //$url = $this->url("scene_elements/");
      //$parameter = "?view=all";
      //$markup .= "<a href=\"" . $url . $parameter . "\">all</a>";
      // third attempt
      $parameter = "?view=all";
      $request_uri = $this->remove_parameter($_SERVER['REQUEST_URI']);
      $url = $request_uri . $parameter;
      $markup .= "<a href=\"" . $url . "\">all</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "date-stack") {
      $markup .= "date-stack";
    } else {
      // output with link
      // third attempt
      $parameter = "?view=date-stack";
      $request_uri = $this->remove_parameter($_SERVER['REQUEST_URI']);
      $url = $request_uri . $parameter;
      $markup .= "<a href=\"" . $url . "\">date-stack</a>";
    }
    $markup .= " | ";
    if ($this->get_given_filter() == "zoneline") {
      $markup .= "filter";
    } else {
      // third attempt
      $parameter = "?filter=zoneline";
      $request_uri = $this->remove_parameter($_SERVER['REQUEST_URI']);
      $url = $request_uri . $parameter;
      $markup .= "<a href=\"" . $url . "\">filter</a>";
    }
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  public function get_last_timecard_date($given_scene_element_obj, $given_keyword) {
    $markup = "";

    // debug
    //print "debug scene_elements get_last_timecard_date()<br />\n";

    if (! $given_scene_element_obj) {
      // error
      print "Error: scene_elements given_scene_element_obj is not known<br />\n";
    }

    if (! $given_keyword) {
      // error
      print "Error: scene_elements given_keyword is not known<br />\n";
    }

    // only limited objects can do this
    if ($given_scene_element_obj->get_class_name_string() == "applications" ||
        $given_scene_element_obj->get_class_name_string() == "domains" ||
        $given_scene_element_obj->get_class_name_string() == "soil_areas" ||
        $given_scene_element_obj->get_class_name_string() == "processes" ||
        $given_scene_element_obj->get_class_name_string() == "webpagess" ||
        $given_scene_element_obj->get_class_name_string() == "classes" ||
        $given_scene_element_obj->get_class_name_string() == "designers" ||
        $given_scene_element_obj->get_class_name_string() == "moneymakers" ||
        $given_scene_element_obj->get_class_name_string() == "projects" ||
        $given_scene_element_obj->get_class_name_string() == "hyperlinks" ||
        $given_scene_element_obj->get_class_name_string() == "design_instances" ||
        $given_scene_element_obj->get_class_name_string() == "email_addresses" ||
        $given_scene_element_obj->get_class_name_string() == "books" ||
        $given_scene_element_obj->get_class_name_string() == "machines" ||
        $given_scene_element_obj->get_class_name_string() == "designs" ||
        $given_scene_element_obj->get_class_name_string() == "suppliers" ||
        $given_scene_element_obj->get_class_name_string() == "plant_lists" ||
        $given_scene_element_obj->get_class_name_string() == "databases" ||
        $given_scene_element_obj->get_class_name_string() == "lands" ||
        $given_scene_element_obj->get_class_name_string() == "projects" ||
        $given_scene_element_obj->get_class_name_string() == "land_traits" ||
        $given_scene_element_obj->get_class_name_string() == "shifts" ||
        $given_scene_element_obj->get_class_name_string() == "postings" ||
        $given_scene_element_obj->get_class_name_string() == "tools") {

      if ($given_scene_element_obj->get_class_name_string() == "applications") {
        include_once("applications.php");
        $obj = new Applications($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "suppliers") {
        include_once("suppliers.php");
        $obj = new Suppliers($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "books") {
        include_once("books.php");
        $obj = new Books($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "email_addresses") {
        include_once("email_addresses.php");
        $obj = new EmailAddresses($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "domains") {
        include_once("domains.php");
        $obj = new Domains($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "soil_areas") {
        include_once("soil_areas.php");
        $obj = new SoilAreas($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "processes") {
        include_once("processes.php");
        $obj = new Processes($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "webpages") {
        include_once("webpages.php");
        $obj = new Webpages($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "plant_lists") {
        include_once("plant_lists.php");
        $obj = new PlantLists($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "classes") {
        include_once("classes.php");
        $obj = new Classes($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "designers") {
        include_once("designers.php");
        $obj = new Designers($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "moneymakers") {
        include_once("moneymakers.php");
        $obj = new Moneymakers($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "projects") {
        include_once("projects.php");
        $obj = new Projects($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "hyperlinks") {
        include_once("hyperlinks.php");
        $obj = new Hyperlinks($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "design_instances") {
        include_once("design_instances.php");
        $obj = new DesignInstances($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "machines") {
        include_once("machines.php");
        $obj = new Machines($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "designs") {
        include_once("designs.php");
        $obj = new Designs($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "databases") {
        include_once("databases.php");
        $obj = new Databases($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "lands") {
        include_once("lands.php");
        $obj = new Lands($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "projects") {
        include_once("projects.php");
        $obj = new Projects($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "land_traits") {
        include_once("land_traits.php");
        $obj = new LandTraits($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "tools") {
        include_once("tools.php");
        $obj = new Tools($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "postings") {
        include_once("postings.php");
        $obj = new Postings($this->get_given_config());
      } else if ($given_scene_element_obj->get_class_name_string() == "shifts") {
        include_once("shifts.php");
        $obj = new Shifts($this->get_given_config());

      } else {
        // todo: add a warning if the string is not found
      }

      // transfer credentials
      $user_obj = $this->get_user_obj();
      $obj->set_user_obj($user_obj);

      // debug
      //print "debug scene_elements: created object instance of class : " . get_class($obj) . "<br />\n";

      if (! isset($obj)) {
        $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": scene_elements failed to instantiate this object: class_name_string = " . $given_scene_element_obj->get_class_name_string());
        return $markup;
      }

      // set
      $user_obj = $this->get_user_obj();
      $obj->set_user_obj($user_obj);

      // set
      $given_id = $given_scene_element_obj->get_class_primary_key_string();

      // debug
      //print "debug scene_elements given_id = " . $given_id . "<br />\n";

      if ($given_scene_element_obj->get_class_name_string() == "domains") {
        // no id, so set to tli
        $obj->set_given_tli($given_id);
        $obj->set_type("get_by_tli");
      } else {
        // default
        $obj->set_given_id($given_id);
        $obj->set_type("get_by_id");
      }

      // debug
      //print "debug scene_elements obj type = " . $obj->get_type() . "<br />\n";
      //print "debug scene_elements obj given_id = " . $obj->get_given_id() . "<br />\n";
      //print "debug scene_elements obj class = " . get_class($obj) . "<br />\n";

      // load data from database
      $markup .= $obj->prepare_query();
      // only output if there are items to output
      if ($obj->get_list_bliss()->get_count() == 1) {
        $class_name = $given_scene_element_obj->get_class_name_string();
        $list_obj = $obj->get_list_bliss()->get_list();
        $poly_obj = $list_obj[0];

        // debug
        //$markup .= "debug scene_elements sort = " . $poly_obj->get_sort() . "<br />\n";

        if ($poly_obj->is_sort_not_expired($given_keyword)) {
          $markup .= "<p style=\"margin: 0px 0px 0px 0px; padding: 2px 2px 2px 2px; background-color: green; color: #EFEFEF; font-size: 90%;\">\n";
          $markup .= $poly_obj->is_sort_not_expired($given_keyword);
          $markup .= "</p>";
        } else {
          // debug
          //print "debug scene_elements: is_sort_not_expired() is false. given_keyword = $given_keyword<br />\n";
          // fail
          // skip
        }
      } else {
        // error
        print "Error: scene_elements count = " . $obj->get_list_bliss()->get_count() . " however it should be 1.<br />\n";

        // debug
        // print what was there for debug purposes
        foreach ($obj->get_list_bliss()->get_list() as $shift) {
          // debug
          print "debug message scene_elements shift id = " . $shift->get_id() . "<br />\n";
        }
      }

    } else {
      // debug
      print "debug message scene_elements: class_name is not known: \"" . $given_scene_element_obj->get_class_name_string() . "\"<br />\n";
    }

    return $markup;
  }

  // method
  public function output_complex($poly_obj) {
    $markup = "";

    $class_name = strtolower(get_class($poly_obj));

    // note check that the object exists
    if (! is_object($poly_obj)) {
      $markup .= "<span style=\"error\">poly_obj does not exist.</span><br/>\n";
    } else {
      $url = $this->url($class_name . "/" . $poly_obj->get_class_primary_key_string());
      $padding = "0";
      $float = "left";
      $width = "26";
      $height = "32";
      $markup .= "      <a href=\"" . $url . "\">" . $poly_obj->get_img_url_as_img_element($padding, $float, $width, $height) . "</a>\n";
    }

    // todo besides the background color, do something to tag polymorphic
    $markup .= "      <em>" . $class_name . "</em> ";
    // todo not sure but had to comment out all of this code
    //if ($poly_obj->get_id()) {
    //  $markup .= $poly_obj->get_id();
    //}
    $markup .= "<br />\n";

    if (! is_object($poly_obj)) {
      $markup .= "<span style=\"error\">poly_obj does not exist.</span><br />\n";
    } else {
      $markup .= "      " . $poly_obj->get_name_with_link_natural() . "<br />\n";
   }

    // todo on hold because it is too complex
    //if ($poly_obj->get_manifest_tli()) {
    //  $markup .= "(manifest = <strong>" . $poly_obj->get_manifest_tli() . "</strong>)";
    //} else {
    //  $markup .= "(not associated with a manifest)";
    //}

    return $markup;
  }

  // method
  public function output_simple($poly_obj, $count_num, $count_last) {
    $markup = "";

    if ($count_num == 1) {
      // old
      //$markup .= "<table style=\"margin: 2px 2px 0px 2px; padding: 16px 16px 16px 16px; background-color: #EFEFEF; color: #000000; font-size: 120%;\" border=\"1\">\n";
      // new
      $markup .= "<table class=\"polymorphic-complex\">\n";
      $markup .= "<tr>\n";
    }
    $markup .= "  <td style=\"padding: 2px 2px 2px 2px; vertical-align: top; text-align: center;\">\n";
    $class_name = strtolower(get_class($poly_obj));
    if (! isset($poly_obj)) {
      $markup .= "  <span style=\"error\">Error: poly_obj is not known.</span><br />\n";
    } else {
      $url = $this->url($class_name . "/" . $poly_obj->get_class_primary_key_string());
      $markup .= "    <a href=\"" . $url . "\">" . $poly_obj->get_img_url_as_img_element() . "</a><br />\n";
      $markup .= "    <span style=\"font-size: 60%;\">" . $poly_obj->get_name_with_link_natural() . "</span><br />\n";
    }

    $markup .= "  </td>\n";
    if ($count_num == $count_last) {
      $markup .= "</tr>\n";
      $markup .= "</table>\n";
    }
    return $markup;
  }

  // method
  public function output_sidecar($given_process_id, $given_user_obj, $style_flag = "", $given_heading = "", $column_design = "") {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_process_id");

    // debug
    //print "type " . $this->get_type() . "<br />\n";
    //print "scene_elements: user_name " . $this->get_user_obj()->name . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    $plural_string = "";
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">No <em>scene_elements</em> were found.</p>\n";
      return $markup;
    } else if ($this->get_list_bliss()->get_count() == 1) {
      $plural_string = "";
    } else if ($this->get_list_bliss()->get_count() > 1) {
      $plural_string = "s";
    }

    // list heading
    if ($given_heading) {
      if ($style_flag = "" | $style_flag == "standard_flag") {
        $markup .= "<h2>Scene Elements (list)</h2>\n";
      } else if ($style_flag == "hint_flag") {
        $markup .= "<p>This scene_element has the following scene_element" . $plural_string . ":</p>\n";
      }
    }

    if ($style_flag == "" | $style_flag == "standard_flag") {
      $markup .= "<div style=\"margin: 4px 0px 4px 0px; padding: 8px 6px 10px 6px; background-color: #E8C782; font-size: 95%; width: 100%;\">\n";
    }
    $markup .= $this->output_sidecar_table($style_flag, $column_design);
    if ($style_flag == "" | $style_flag == "standard_flag") {
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function output_polymorphic_standard($given_class_name, $given_id_for_url, $given_img_url_as_img_tag, $given_name_with_link_natural) {
    $markup = "";

    $url = $this->url($given_class_name . "/" . $given_id_for_url);

    $markup .= "<div style=\"margin: 2px 2px 2px 2px; padding: 2px 2px 2px 2px; background-color: #EFEFEF; color: #000000; font-size: 110%;\">\n";
    $markup .= "<a href=\"" . $url . "\">\n";
    $markup .= $given_img_url_as_img_tag;
    $markup .= "</a><br />\n";
    $markup .= "<em>polymorphic</em> " . $class_name . "<br />\n";
    $markup .= $given_name_with_link_natural;
    $markup .= "<br />\n";
    $markup .= "</div>\n";

    return $markup;
  }

  // method
  public function get_count_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // output count
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_classes_id_given_process_id($given_process_id, $given_user_obj, $link_flag) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_process_id");

    // debug
    //print "type " . $this->get_type() . "<br />\n";
    //print "scene_elements: user_name " . $this->get_user_obj()->name . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    // for html indentatino
    $first_flag = "1";

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        if ($scene_element->get_class_name_string() == "classes") {
          if ($first_flag) {
            $markup .= "  ";
            $first_flag = "";
          }
          if ($link_flag) {
            // note test if object is found
            if (is_object($scene_element->get_polymorphic_object_as_obj())) {
              $markup .= $scene_element->get_polymorphic_object_as_obj()->get_id_with_link() . " ";
            } else {
              // note frb provides code-line-number-to-debug error messages
              $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": scene_elements could not get_polymorphic_object_as_obj. Non-object fatal error. Unable to load data. scene_elements.id = " . $scene_element->get_id() . "");
            }
          } else {
            $markup .= "  " . $scene_element->get_polymorphic_object_as_obj()->get_id() . "<br />\n";
         }
        }
      }
    }

    return $markup;
  }

  // method
  public function get_classes_name_given_process_id($given_process_id, $given_user_obj, $link_flag) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_process_id");

    // debug
    //print "type " . $this->get_type() . "<br />\n";
    //print "scene_elements: user_name " . $this->get_user_obj()->name . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    // for html indentation
    $first_flag = "1";
    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        if ($scene_element->get_class_name_string() == "classes") {
          if ($first_flag) {
            $markup .= "  ";
            $first_flag = "";
          }
          if ($link_flag) {
            $url = $this->url("classes/" . $scene_element->get_class_primary_key_string());
            $markup .= "<a href=\"" . $url . "\">"; 
          }
          $poly_obj = $scene_element->get_polymorphic_object_as_obj();
          if (is_object($poly_obj)) {
            $markup .= $scene_element->get_polymorphic_object_as_obj()->get_name();
          } else {
            $markup .= "Error scene_elements: polymorphic_object a non-object."; 
          }
          if ($link_flag) {
            $markup .= "</a>"; 
            // todo figure out if this can be coded, meanwhile no linebreak
            //$markup .= "<br />"; 
            $markup .= " "; 
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function get_scene_element_id_list_given_processes_id($given_process_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_process_id");

    // debug
    //print "debug scene_element given_process_id " . $this->get_given_process_id() . "<br />\n";
    //print "type " . $this->get_type() . "<br />\n";
    //print "scene_elements: user_name " . $this->get_user_obj()->name . "<br />\n";

    // load data from database
    $markup .= $this->prepare_query();

    $scene_element_id_list = array();
    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        array_push($scene_element_id_list, $scene_element->get_id());
      }
    }

    return $scene_element_id_list;
  }

  // method
  protected function output_single() {
    $markup = "";

    // prints one row of aggregate design
    $markup .= $this->output_aggregate();

    $scene_element = $this->get_list_bliss()->get_first_element();

    // todo document Mollison Chapter 1 design influence
    $markup .= "<h2>Mollison Chapter 1 design influence</h2>\n";
    $markup .= "<p>derived fields</p>\n";
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header-vertical\">\n";
    $markup .= "    <em>place_id</em>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $scene_element->derive_place_id() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header-vertical\">\n";
    $markup .= "    <em>guild_id</em>\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $scene_element->derive_guild_id() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // filter
    $parameter_a = new Parameter();
    $parameter_a->set_name("filter");
    $parameter_a->set_validation_type_as_filter();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view");
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "filter") {
            $this->set_given_filter($parameter->get_value());
          }
          // 2
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          // 3
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "scene_elements";
            $db_table_field = "id";
            // todo set up below by getting the database_name from config
            $database_name = "";
            // get sort letter
            $scene_element_obj = new SceneElements($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $scene_element_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $parameter_value = $parameter->get_value();
            $this->redirect_helper($parameter_value);
          }
        }
      }
    }
    return $markup;
  }

  // method
  public function get_name_with_link($given_target_directory = "") {
    $markup = "";

    include_once("lib/factory.php");
    $factory_obj = new Factory($this->get_given_config());
    $url = $this->url($factory_obj->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_name()) {
      $markup .= $this->get_name();
    } else if ($this->get_id()) {
      $markup .= $this->get_id();
    } else {
      $markup .= "[no name or id]";
    }
    $markup .= "</a>";

    return $markup;
  }

  // method
  public function get_name_with_link_given_id($given_id = "", $given_user_obj) {
    $markup = "";

    if (! $given_id) {
      // do not know what to get, so return a null string
      return "";
    }

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug shifts given_id = " . $this->get_given_id() . "<br />\n";

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_name_with_link();
    }

    return $markup;
  }

  // method
  public function get_name() {
    // see if parent holds a value for the name
    if (parent::get_name()) {
      return parent::get_name();
    } else {
      // name is not defined, so output temp-name
      return "temp-name: " . $this->get_database_string() . "-" . $this->get_class_name_string() . "-" . $this->get_class_primary_key_string();
    }
  }

  // this is a static function
  /**
   * @todo sort by the function of an object
   */
  // from webpages, find the here
  public static function cmp_obj($a, $b) { 
    if (  $a->get_sort() ==  $b->get_sort() ){
      return 0;
    } 
    return ($a->get_sort() < $b->get_sort()) ? -1 : 1;
  } 

  // method
  public function get_processes_given_scene_element($given_class_name, $given_primary_key, $given_user_obj) {
    $markup = "";

    if (! $given_class_name ||! $given_primary_key) {
      // do not know what to get, so return a null string
      return "Error: scene_elements class_name and/or primary_key not known\n";
    }

    // set
    $this->set_given_class_name_string($given_class_name);
    $this->set_given_class_primary_key_string($given_primary_key);
    $this->set_user_obj($given_user_obj);

    // debug
    print "debug given_class_name = " . $this->get_given_class_name_string() . "<br />\n";
    print "debug given_primary_key = " . $this->get_given_class_primary_key_string() . "<br />\n";

    $this->determine_type();

    // debug
    //print "debug scene_element type = " . $this->get_type() . "<br/>\n";

    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<ul style=\"margin: 15px; padding: 0px;\">\n";
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $markup .= "  <li>" . $scene_element->get_name_with_link() . "</li>\n";
      }
      $markup .= "</ul>\n";
    }

    return $markup;
  }

  // method
  public function get_name_given_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug scene_elements given_id = " . $this->get_given_id() . "<br />\n";
    //print "debug scene_elements user_obj name = " . $given_user_obj->name . "<br />\n";
    //print "debug scene_elements user_obj name = " . $this->get_user_obj()->name . "<br />\n";

    $this->determine_type();

    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      $scene_element->set_user_obj($given_user_obj);
      //$markup .= $scene_element->get_name_with_link();
      $markup .= $scene_element->get_polymorphic_object_as_details();
    }

    return $markup;
  }

  // method
  public function get_count_given_account_id($given_account_id, $given_user_obj) {

    // set
    $this->set_given_class_name_string("accounts");
    $this->set_given_class_primary_key_string($given_account_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_count_given_machine_id($given_machine_id, $given_user_obj) {

    // set
    $this->set_given_class_name_string("machines");
    $this->set_given_class_primary_key_string($given_machine_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    // output count
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_list_given_machine_id($given_machine_id, $given_user_obj) {

    // set
    $this->set_given_class_name_string("machines");
    $this->set_given_class_primary_key_string($given_machine_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    // output count
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= "<ul style=\"margin: 0px 15px 0px 25px; padding: 10px 10px 10px 10px;\">\n";
      //$markup .= "<ul>\n";;
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $markup .= "<li style=\"margin: 0px; padding: 1px;\">";
        $markup .= "<strong>";
        $markup .= $scene_element->get_name_with_link();
        $markup .= "</strong>";
        // too much information
        //$markup .= " (process: " . $scene_element->get_process_obj()->get_name_with_link() . ")";
        $markup .= "</li>\n";
      }
      $markup .= "</ul>\n";
    }
    return $markup;

}

  // method
  public function get_scene_elements_given_application_id($given_user_obj, $given_application_id) {
    $markup = "";

    // set
    $this->set_given_class_name_string("applications");
    $this->set_given_class_primary_key_string($given_application_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= "<ul>\n";;
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $markup .= "<li>";
        $markup .= "<strong>";
        $markup .= $scene_element->get_name_with_link();
        $markup .= "</strong>";
        // too much information
        //$markup .= " (process: " . $scene_element->get_process_obj()->get_name_with_link() . ")";
        $markup .= "</li>\n";
      }
      $markup .= "</ul>\n";;
    }

    // only output if there are items to output
    return $markup;
  }

  // method
  public function get_scene_elements_given_database_id($given_user_obj, $given_database_id) {
    $markup = "";

    // todo this function can be refactored and made generic

    // set
    $this->set_given_class_name_string("databases");
    $this->set_given_class_primary_key_string($given_database_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= "<ul style=\"margin: 15px; padding: 0px;\">\n";
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $markup .= "<li>scene_element id " . $scene_element->get_id_with_link() . ":\n";;
        if ($scene_element->get_name()) {
          $markup .= $scene_element->get_name_with_link();
        } else {
          $markup .= "[no name]";
        }
        // todo the following info is too much to display
        //$markup .= " (process: " . $scene_element->get_process_obj()->get_name_with_link() . ")";
        $markup .= "</li>\n";
      }
      $markup .= "</ul>\n";;
    }

    // only output if there are items to output
    return $markup;
  }

  // method
  public function get_scene_elements_count_given_database_id($given_user_obj, $given_database_id) {
    $markup = "";

    // todo this function can be refactored and made generic

    // set
    $this->set_given_class_name_string("databases");
    $this->set_given_class_primary_key_string($given_database_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    // todo output count and only if there is a count
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->get_list_bliss()->get_count();
    }

    return $markup;
  }

  // method
  public function get_scene_elements_list_given_database_id($given_user_obj, $given_database_id) {
    $markup = "";

    // todo this function can be refactored and made generic

    // set
    $this->set_given_class_name_string("databases");
    $this->set_given_class_primary_key_string($given_database_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    // todo output count and only if there is a count
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= "<ul style=\"margin: 15px; padding: 0px;\">\n";
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $markup .= "  <li>";
        $markup .= $scene_element->get_process_obj()->get_name_with_link();
        $markup .= " </li>\n";
      }
      $markup .= "</ul>";
    }

    return $markup;
  }

  // method
  private function get_process_name_given_scene_element_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $markup .= $scene_element->get_process_obj()->get_name_with_link();
      }
    }

    return $markup;
  }

  // method
  private function get_project_name_given_scene_element_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // check for errors
    if ($markup) { 
      return "<span class=\"error\">error scene_elements " . $markup . "</span><br />\n";
    }

    if ($this->get_list_bliss()->get_count() == 1) {
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $process_id = $scene_element->get_process_obj()->get_id();
        include_once("projects.php");
        $obj = new Projects($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $obj->get_name_given_process_id($process_id, $user_obj);
      }
    } else {
      return "<span class=\"error\">error scene_elements cannot find process id</span><br />\n";
    }

    return $markup;
  }

  // method
  public function get_build_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // set this to change the view
    $this->set_given_view("by-parent-class-name");

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no SceneElements were found.</p>\n";;
      return $markup;
    }

    $markup .= $this->output_aggregate_simple_list();

    return $markup;
  }

  // method
  public function display($given_process_id, $given_user_obj, $given_heading, $debug_flag = 0) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();
    $markup .= $this->prepare_query();

    // heading
    if ($given_heading) {
      $markup .= "<h2>Scene Elements (display)</h2>\n";
    }

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() == 0) {

      $markup .= "<div style=\"margin: 4px 4px 4px 0px; padding: 10px 6px 10px 6px; background-color: #E8C782; font-size: 100%;\">\n";
      $markup .= "No play because there are no scene_elements<br />\n";
      $markup .= "</div>\n";
      return $markup;
    }

    $markup .= "<div style=\"margin: 4px 4px 4px 0px; padding: 12px; background-color: #EFEFEF;\">\n";

    // debug
    // add scene_elements to scene
    if ($debug_flag) {
      $markup .= "<h3>add scene_elements as actors to scenes</h3>\n";
      // feedback
      $markup .= "scene_elements count = " . $this->get_list_bliss()->get_count() . "<br /><br />\n";
    }

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      $num++;

      // get as polymorphic object
      $scene_element->set_user_obj($given_user_obj);
      $polymorphic_object_obj = $scene_element->get_polymorphic_object_as_obj();
      if ($debug_flag) {
        // debug
        //$markup .= "debug scene_elements num " . $num . "<br />\n";
      }

      if (is_object($polymorphic_object_obj)) {
        // add_actor
        $markup .= $this->get_scenes_obj()->add_actor($scene_element, $polymorphic_object_obj, $given_heading, $debug_flag);
      } else {
        // error
        $markup .= "debug scene_elements error: did not find polymorphic object<br />\n";
      }
    }

    // loaded, now run it!
    $markup .= $this->get_scenes_obj()->display($given_process_id, $given_user_obj, $given_heading);

    $markup .= "</div>\n";

    return $markup;
  }

  // method
  private function output_sidecar_table($style_flag = "", $column_design = "") {
    $markup = "";

    if ($style_flag == "hint_flag") {
      $markup .= $this->output_hint_sidecar_table();
      return $markup;
    }

    // tradition list of scene_elements
    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    if (! $column_design) {
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    processes\n";
      $markup .= "  </td>\n";
    }
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    class_name_string<br />\n";
    //$markup .= "    primary_key_string<br />\n";
    //$markup .= "    database_string<br />\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    polymorphic object\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"\">\n";
    $markup .= "    name &amp; description\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    process_profiles\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    accounts\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    // todo note the the following is cut and paste from elsewhere, so refactor
    //$url = $this->url("budgets");
    //$markup .= "    <a href=\"" . $url . "\">";
    //$markup .= "budgets";
    //$markup .= "</a>\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    yield\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    responsibilities\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    timecards count\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    postings count\n";
    //$markup .= "  </td>\n";

    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {

      // preprocess
      $user_obj = $this->get_user_obj();
      $scene_element->set_user_obj($user_obj);
      $scene_element->set_given_project_id($this->get_given_project_id());

      // process
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      // status
      $markup .= "  <td style=\"background-color: " . $scene_element->get_status_background_color() . "\">\n";
      $markup .= "    " . $scene_element->get_status() . "<br />\n";
      $markup .= "  </td>\n";

      if (! $column_design) {
        // img_url of processes
        $markup .= "  <td class=\"mid\" align=\"left\">\n";
        $padding = " 0px 0px 0px 0px";
        $float = "";
        $width = "25";
        $markup .= "    " . $scene_element->get_process_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
        $markup .= "<br />";
        $url = $this->url("processes/" . $scene_element->get_process_obj()->get_id());
        $markup .= "<a href=\"" . $url . "\">";
        $markup .= $scene_element->get_process_obj()->get_id();
        $markup .= "</a>\n";
        $url = $this->url("processes/" . $scene_element->get_process_obj()->get_id());
        $markup .= "<a href=\"" . $url . "\">";
        $markup .= $scene_element->get_process_obj()->get_name_with_link();
        $markup .= "</a>\n";
        $markup .= "  </td>\n";
      }

      // debug cell showing 3 variables
      //$markup .= "  <td class=\"mid\" align=\"left\" style=\"width: 200px;\">\n";
      //$markup .= "    " . $scene_element->get_database_string() . "<br />\n";
      //$markup .= "    " . $scene_element->get_class_name_string() . "<br />\n";
      //$markup .= "    " . $scene_element->get_class_primary_key_string() . "<br />\n";
      //$markup .= "  </td>\n";

      // sort
      $markup .= $scene_element->get_sort_cell();

      // id
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $url = $this->url("scene_elements/" . $scene_element->get_id());
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= $scene_element->get_id();
      $markup .= "</a>\n";
      $markup .= "  </td>\n";

      // polymorphic_object
      $markup .= "  <td class=\"mid\" style=\"width: 180px; padding: 0px 0px 0px 4px;\"\">\n";
      $markup .= "    " . $scene_element->get_polymorphic_object_as_details() . "\n";
      $markup .= "  </td>\n";

      // name
      // description
      $markup .= "  <td class=\"mid\" align=\"left\" style=\"padding: 6px; width: 300px;\">\n";
      $markup .= "    <div style=\"font-weight: 800; font-size: 120%; padding: 2px 0px 2px 0px; width: 280px;\">" . $scene_element->get_name() . "</div>\n";
      $markup .= "    " . $scene_element->get_description() . "\n";
      $markup .= "  </td>\n";

      // accounts
      //$polymorphic_object_obj = $scene_element->get_polymorphic_object_as_obj();
      //if (is_object($polymorphic_object_obj)) {
      //  $account_obj = $polymorphic_object_obj->get_account_obj();
      //  $account_string = "[not defined]";  
      //  if (is_object($account_obj)) {
      //    $account_string = $account_obj->get_name();
      //  } else {
      //    $account_string = "Error scene_elements: no account_obj<br />\n";
      //  }
      //  if (isset($account_string)) {
      //    $markup .= "  <td class=\"mid\" style=\"width: 26px; text-align: center;background-color: #FFFFFF;\">\n";
      //    $markup .= "    " . $account_string . "\n";
      //  } else {
      //    $markup .= "  <td class=\"mid\" style=\"width: 26px; text-align: center;background-color: #CD5555;\">\n";
      //  }
      //}
      //$markup .= "  </td>\n";

      // todo remove all the budget at db schema
      // todo because it is a design issue, budget should be polymorphic
      // budgets
      //include_once("budgets.php");
      //$obj = new Budgets($this->get_given_config());
      //$user_obj = $this->get_user_obj();
      //$budgets_list_string = $obj->get_budgets_list_string_given_scene_element_id($scene_element->get_id(), $user_obj);
      //if (strpos($budgets_list_string, "no budgets were found") !== false) {
      //  $markup .= "  <td style=\"text-align: left; background-color: #EE3B3B;\">\n";
      //} else {
      //  $markup .= "  <td style=\"text-align: left;\">\n";
      //}
      //$markup .= "    " . $budgets_list_string . "\n";
      //$markup .= "  </td>\n";

      // yield         
      $markup .= "  <td>\n";
      $markup .= "    " . $scene_element->get_yield() . "\n";
      $markup .= "  </td>\n";

      // timecards count
      //$markup .= "  <td style=\"text-align: center;\">\n";
      //$user_obj = $this->get_user_obj();
      //if ($this->get_given_project_id()) {
      //  $url = $this->url("timecards/project/" . $this->get_given_project_id());
      //} else {
      //  $url = $this->url("timecards/");
      //}
      //include_once("timecards.php");
      //$obj = new Timecards($this->get_given_config());
      //$user_obj = $this->get_user_obj();
      //$timecards_count = $obj->get_count_given_scene_element_id($scene_element->get_id(), $user_obj);
      //$markup .= "    <a href=\"" . $url . "\">" . $timecards_count ."</a>\n";
      //$markup .= "  </td>\n";

      // todo fix this the postings count
      //$user_obj = $this->get_user_obj();
      //if ($this->get_given_project_id()) {
      //  $url = $this->url("budgets/project/" . $this->get_given_project_id());
      //} else {
      //  $url = $this->url("budgets/");
      //}
      //include_once("postings.php");
      //$obj = new Postings($this->get_given_config());
      //$user_obj = $this->get_user_obj();
      //$postings_count = $obj->get_count_given_scene_element_id($scene_element->get_id(), $user_obj);
      //if ($postings_count) {
      //  $markup .= "  <td style=\"text-align: center; background-color: #B9D3EE;\">\n";
      //} else {
      //  $markup .= "  <td style=\"text-align: center;\">\n";
      //}
      //$markup .= "    <a href=\"" . $url . "\">" . $postings_count ."</a>\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // output totals
    if (! $column_design) {
      // output scene_element count
      $markup .= "<p><em>scene_element count = " . $num . "</em></p>\n";
    }

    return $markup;
  }

  // method
  private function output_hint_sidecar_table() {
    $markup = "";

    // tradition list of scene_elements
    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    polymorphic object\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name &amp; description\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {

      // process
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $url = $this->url("scene_elements/" . $scene_element->get_id());
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= $scene_element->get_id();
      $markup .= "</a>\n";
      $markup .= "  </td>\n";

      // polymorphic_object
      $markup .= "  <td class=\"mid\" style=\"width: 180px; padding: 0px 0px 0px 4px;\"\">\n";
      $markup .= "    " . $scene_element->get_polymorphic_object_as_details() . "\n";
      $markup .= "  </td>\n";

      // name
      // description
      $markup .= "  <td class=\"mid\" align=\"left\" style=\"padding: 4px;\">\n";
      $markup .= "    <div style=\"font-weight: 800; font-size: 110%; padding: 2px 0px 2px 0px;\">" . $scene_element->get_name() . "</div>\n";
      $markup .= "    " . $scene_element->get_description() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_aggregate_simple_list() {
    $markup = "";

    // part 1 of 3 pre-process the data into array by class_name
    $parent_hash = array();
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {

      // set         
      $user_obj = $this->get_user_obj();
      $scene_element->set_user_obj($user_obj);

      // process
      $field_to_return = "class-name";
      $class_name = $scene_element->get_polymorphic_object_as_details($field_to_return);

      // data structure is an array of arrays, so build appropriately
      // is this the first object of this class_name
      if (array_key_exists($class_name, $parent_hash)) {
        // add to value's array
        $se_array = $parent_hash[$class_name];
        array_push($se_array, $scene_element);
        $parent_hash[$class_name] = $se_array;
      } else {
        // add to a new values's array
        $se_array = array($scene_element);
        $parent_hash[$class_name] = $se_array;
      }
    }

    // part 2 of 3 sort
    // sort by key
    ksort($parent_hash);

    // part 3 of 3 output the results by looping through the hash
    // set globals
    $num = 0;
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    process\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    s.e. id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    class name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    scene element\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    account\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    yield\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    foreach ($parent_hash as $class_name => $se_array) {

      // make class_name header
      $url = $this->url($class_name);

      // display s.e. in a table sorted by primary_key of s.e.
      // sort se_array by id
      $sorted_se_array = array();
      foreach ($se_array as $scene_element_obj) {
        $primary_key_id = $scene_element_obj->get_class_primary_key_string();
        if (array_key_exists($primary_key_id, $sorted_se_array)) {
          $a_se_array = $sorted_se_array[$primary_key_id];
          array_push($a_se_array, $scene_element_obj);
          $sorted_se_array[$primary_key_id] = $a_se_array;
        } else {
          $a_se_array = array($scene_element_obj);
          $sorted_se_array[$primary_key_id] = $a_se_array;
        }
      }

      // sort by key
      ksort($sorted_se_array);

      // display
      foreach ($sorted_se_array as $primary_key_id => $a_se_array) {
        foreach ($a_se_array as $scene_element_obj) {
              $num++;
              $markup .= "<tr>\n";

              $markup .= "  <td>\n";
              $markup .= "    " . $num . "\n";
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              $se_obj = new SceneElements($this->get_given_config());
              $user_obj = $this->get_user_obj();
              $markup .= $se_obj->get_process_name_given_scene_element_id($scene_element_obj->get_id(), $user_obj);
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              $url = $this->url("scene_elements/" . $scene_element_obj->get_id());
              $markup .= "    <a href=\"". $url . "\">" . $scene_element_obj->get_id() . "</a>\n";
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              $markup .= "    " . $class_name . "\n";
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              if (is_object($scene_element_obj->get_polymorphic_object_as_obj())) {
                $markup .= "    " . $scene_element_obj->get_polymorphic_object_as_obj()->get_name();
              } else {
                $markup .= "  Error: name not found.\n";
              }
              $markup .= "  </td>\n";

              // accounts  
              $account_string = "";
              $polymorphic_object_obj = $scene_element_obj->get_polymorphic_object_as_obj();
              if (is_object($polymorphic_object_obj)) {
                // todo the following is on hold because of get_account_obj()
                //$account_obj = $polymorphic_object_obj->get_account_obj();
                //if (is_object($account_obj)) {
                //  $account_string = $account_obj->get_name();
                //  $markup .= "  <td class=\"mid\" style=\"width: 26px; text-align: center;background-color: #FFFFFF;\">\n";
                //} else {
                  $markup .= "  <td class=\"mid\" style=\"width: 26px; text-align: center;background-color: #CCCCCC;\">\n";
                  $account_string = "Error scene_elements: no account_obj<br />\n";
                //  $markup .= "  <td class=\"mid\" style=\"width: 26px; text-align: center;background-color: #CD5555;\">\n";
                //}
              }
              $markup .= "    " . $account_string . "\n";
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              //$yield_total += $scene_element->get_yield();
              $markup .= "    " . $scene_element->get_yield() . "\n";
              $markup .= "  </td>\n";

              $markup .= "</tr>\n";
        }
      }
    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  // todo this budget part of the function name could be refactored out perhaps
  public function output_scene_element_given_budget_id($given_budget_id, $given_user_obj) {
    // set
    $this->set_given_budget_id($given_budget_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error scene_elements " . $markup . "</p>\n";
    }

    // only output if there are items to output
    return $this->output_sidecar_table();
  }

  // method
  public function get_domain_tli_list_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "domains") {
        $markup .= $scene_element->get_class_primary_key_string();
      }
    }

    // debug
    //print "debug scene_elements domain_tli_list for given_process_id (" . $given_process_id . ") = " . $markup . "<br />\n";

    return $markup;
  }

  // method
  public function get_table_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

          $markup .= "<table class=\"scene-elements-list\">\n";
          $markup .= "<tr>\n";
          $markup .= "  <td>\n";
          $markup .= "    #\n";
          $markup .= "  </td>\n";
          $markup .= "  <td>\n";
          $markup .= "    s.e. id\n";
          $markup .= "  </td>\n";
          $markup .= "  <td style=\"width: 300px;\">\n";
          $markup .= "    scene element\n";
          $markup .= "  </td>\n";
          $markup .= "</tr>\n";
          $num = 0;
          foreach ($this->get_list_bliss()->get_list() as $scene_element) {
              $num++;
              $markup .= "<tr>\n";

              $markup .= "  <td>\n";
              $markup .= "    " . $num . "\n";
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              $url = $this->url("scene_elements/" . $scene_element->get_id());
              $markup .= "    <a href=\"". $url . "\">" . $scene_element->get_id() . "</a>\n";
              $markup .= "  </td>\n";

              $markup .= "  <td>\n";
              $markup .= "    " . $scene_element->get_polymorphic_object_as_details();
              $markup .= "  </td>\n";

              $markup .= "</tr>\n";
          }
          $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function get_se_list_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "plant_lists") {
        if ($scene_element->get_class_primary_key_string() == $given_plant_list_id) {
          // project img 
          if (1) {
            $markup .= $scene_element->get_project_img();
          }
          // se
          if (1) {
            $url = $this->url("scene_elements/" . $scene_element->get_id());
            $markup .= "se&nbsp;id&nbsp;<a href=\"" . $url . "\">" . $scene_element->get_id() . "</a><br />\n";
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function get_project_img() {
    $markup = "";

    include_once("projects.php");
    $project_obj = new Projects($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup = $project_obj->get_img_with_link_given_given_scene_element_id($this->get_id(), $user_obj);

    return $markup;
  }

  // method
  public function get_scene_element_name_given_class_id($given_class_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_class_id($given_class_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "classes") {
        if ($scene_element->get_class_primary_key_string() == $given_class_id) {
          // scene_element name
          $url = $this->url("scene_elements/" . $scene_element->get_id());
          $markup .= "<a href=\"" . $url . "\">" . $scene_element->get_name() . "</a><br />\n";
        }
      }
    }

    return $markup;
  }

  // method
  public function get_project_class_count_given_project_id($given_user_obj, $given_project_id) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    $count = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "projects") {
        $count++;
      }
    }

    return $count;
  }

  // method
  public function is_project_id_exists_as_projects_class($given_project_id, $given_user_obj) {
    $markup = "";

    // set
    // note the parameter is not what you think; is a a string by which to fiter for a particular scene_element field data
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    $count = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "projects") {
        if ($scene_element->get_class_primary_key_string() == $given_project_id) {
          return 1;
        }
      }
    }

    return 0;
  }

  // method
  public function get_parent_count_given_child_project_id($given_user_obj, $given_project_id) {
    $markup = "";

    // set
    //$this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    $count = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "projects") {
        if ($scene_element->get_class_primary_key_string() == $given_project_id) {
          return 1;
        }
      }
    }

    return 0;
  }

  //  method
  public function list_child_given_project_id($given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // preprocess
    $count = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "projects") {
         $count++;
      }
    }

    // process
    if ($count) {
      $markup .= "<ol>\n";
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        if ($scene_element->get_class_name_string() == "projects") {
          $markup .= "<li style=\"padding: 10px;\">";
          $found_project_id = $scene_element->get_class_primary_key_string();
          include_once("projects.php");
          $project_obj = new Projects($this->get_given_config());
          $user_obj = $this->get_user_obj();
          $markup .= $project_obj->get_img_element_with_link_given_project_id($found_project_id, $user_obj);
          $markup .= $project_obj->get_name_with_link_given_project_id($found_project_id, $user_obj);
          $markup .= "</li>";
        }
      }
      $markup .= "</ol>\n";
    }

    return $markup;
  }

  // method
  public function list_parent_given_project_id($given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    // preprocess
    $count = 0;
    $found_scene_element_id = "";
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      if ($scene_element->get_class_name_string() == "projects") {
        if ($scene_element->get_class_primary_key_string() == $given_project_id) {
          $found_scene_element_id = $scene_element->get_id();

          // debug
          //print "debug scene_elements found_scene_element_id = " . $found_scene_element_id . "<br />\n";
          $count++;
        }
      }
    }

    // do
    if ($count) {
      include_once("scene_elements.php");
      $scene_element_obj = new SceneElements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $scene_element->get_project_given_id($found_scene_element_id, $user_obj) . "\n";
    }

    return $markup;
  }

  //  method
  public function get_project_given_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup = $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      $process_id = $scene_element->get_process_obj()->get_id();

      // debug
      //print "debug scene_elements process_id = " . $process_id . "<br />\n";

      // go up
      include_once("processes.php");
      $process_obj = new Processes($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $project_id = $process_obj->get_project_id($process_id, $given_user_obj);

      // debug
      //print "debug scene_elements project_id = " . $project_id . "<br />\n";

      include_once("projects.php");
      $project_obj = new Projects($this->get_given_config());
      $markup .= $project_obj->get_img_element_with_link_given_project_id($project_id, $user_obj);
      $markup .= $project_obj->get_name_with_link_given_project_id($project_id, $user_obj);
    }

    return $markup;
  }

  // method
  public function get_budget_id_given_domains_tli($given_domain_tli, $given_user_obj) {
    return "debug scene_elements get_budget_id...<br />\n";
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug scene_elements: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error scene_elements: results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $scene_element) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $scene_element->get_id() . " " . $scene_element->get_name_with_link() . " documentation " . $scene_element->get_energy_flows_dollars("documentation") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $scene_element->get_energy_flows_dollars("documentation");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error scene_elements: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "scene_elements: no scene_elements found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // note assume away
    if ($given_account_name == "documentation") {
      $markup .= 0;
    }

    return $markup;
  }

  // method
  public function get_count_of_matching_scene_elements($given_class_name, $given_id, $given_user_obj) {
    $markup = "";

    // set
    // todo the parameters are not what you think so change to better names
    $this->set_user_obj($given_user_obj);

    // strategy of function processing
    // returns count_of_matching_scene_elements 
    // get list of all and then sift and tally

    $this->determine_type();

    $markup = $this->prepare_query();

    $count = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
 
     // sift and count
     if ($scene_element->get_class_name_string() == strtolower($given_class_name)) {
        if ($scene_element->get_class_primary_key_string() == $given_id) {
          $count++;

          // debug
          //print "debug scene_elements id = " . $scene_element->get_id() . " class_name_string = " . $scene_element->get_class_name_string() . " given_class_name = " . $given_class_name . "<br />\n";
        }
      }
    }

    if ($count) {
      return $count;
    }

    return $markup;
  }

  // method
  public function get_list_of_matching_scene_elements($given_class_name, $given_id, $given_user_obj) {
    $markup = "";

    // set
    // todo the parameters are not what you think so change to better names
    $this->set_user_obj($given_user_obj);

    // strategy of function processing
    // returns count_of_matching_scene_elements 
    // get list of all and then sift and tally

    $this->determine_type();

    $markup = $this->prepare_query();
    $count = $this->get_list_bliss()->get_count();
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
      $num++;

      // sift and count
      if ($scene_element->get_class_name_string() == strtolower($given_class_name)) {
        if ($scene_element->get_class_primary_key_string() == $given_id) {

          // debug
          //print "debug scene_elements id = " . $scene_element->get_id() . " class_name_string = " . $scene_element->get_class_name_string() . " given_class_name = " . $given_class_name . "<br />\n";
          $url = $this->url("scene_elements/" . $scene_element->get_id());
          $markup .= "<a href=\"" . $url . "\">" . $scene_element->get_id() . "</a>";
          if ($num < $count) {
            $markup .= "&nbsp;";
          }
        }
      }
    }

    return $markup;
  }

  // method
  public function get_scene_elements_list($given_class_name, $given_id, $given_user_obj) {
    $markup = "";

    // set
    // todo the parameters are not what you think so change to better names
    $this->set_user_obj($given_user_obj);

    // get list of all and then sift and tally

    $this->determine_type();

    $markup = $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $scene_element) {
 
     // sift
     if ($scene_element->get_class_name_string() == strtolower($given_class_name)) {
        // sift
        if ($scene_element->get_class_primary_key_string() == $given_id) {
          $url = $this->url("scene_elements/" . $scene_element->get_id());
          $markup .= "s.e. <a href=\"";
          $markup .= $url;
          $markup .= "\">";
          $markup .= $scene_element->get_id();
          $markup .= "</a>";
          $url = $this->url("processes/" . $scene_element->get_process_obj()->get_id());
          $markup .= " proc <a href=\"";
          $markup .= $url;
          $markup .= "\">";
          $markup .= $scene_element->get_process_obj()->get_id();
          $markup .= "</a><br />";
        }
      }
    }

    return $markup;
  }

  // method
  private function derive_place_id() {
    $markup = "";

    $class_name = $this->get_class_name_string(); 
    if ($class_name == "lands") {
      $markup .= $class_name . " code get from land instance";
      $markup .= " place ";
    } else {
      $markup .= "Error: place class_name not known.";
    }
 
    return $markup;
  }

  // method
  private function derive_guild_id() {
    $markup = "";

    $class_name = $this->get_class_name_string(); 
    if ($class_name == "lands") {
      $markup .= $class_name . " code get from land instance";
      $markup .= " guild ";
    } else {
      $markup .= "Error: guild class_name not known.";
    }
 
    return $markup;
  }

}
