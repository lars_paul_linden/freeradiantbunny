<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04
// version 1.5 2015-10-16
// version 1.6 2017-01-28
// version 1.6 2017-11-08 measurements as checklist

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/domains.php

include_once("lib/socation.php");

class Domains extends Socation {

  // given
  private $given_domain_tli;
  private $given_process_id;
  private $given_maxonomy_id;

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  public function get_given_id() {
    return $this->given_domain_tli;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_maxonomy_id
  public function set_given_maxonomy_id($var) {
    $this->given_maxonomy_id = $var;
  }
  public function get_given_maxonomy_id() {
    return $this->given_maxonomy_id;
  }

  // parameters
  // todo note the previous defaults as a history
  private $given_view = "foundation";
  private $given_sort;

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // given_sort
  public function set_given_sort($var) {
    $this->given_sort = $var;
  }
  public function get_given_sort() {
    return $this->given_sort;
  }
    
  // attributes
  private $tli;
  private $domain_name;
  private $tagline;
  private $registrar;
  private $hosting;
  private $crm;
  private $design_obj;
  private $spotlight;
  private $backups;
  private $log;
  private $ssl_cert;
  
  // domain_name
  public function set_domain_name($var) {
    $this->domain_name = $var;
  }
  public function get_domain_name() {
    return $this->domain_name;
  }

  // tli
  public function set_tli($var) {
    $this->tli = $var;
  }
  public function get_tli() {
    return $this->tli;
  }

  public function get_tli_with_link() {
    // todo slowly things get more specified and more complicated
    $url = "http://" . $this->get_domain_name();
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_tli() . "</a>";
  }
  public function get_tli_with_natural_link() {
    $url = $this->url("domains/" . $this->get_tli());
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_tli() . "</a>";
  }

  // id (overrides standard)
  public function set_id($var) {
    $this->set_tli($var);
  }
  public function get_id() {
    return $this->get_tli();
  }

  public function get_domain_name_with_link() {
    $url = "http://" . $this->get_domain_name();
    return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_domain_name() . "</a>";
  }
  // todo not sure if the following is needed
  //public function get_domain_name_with_link_natural() {
  //  $url = $this->url("domains/" . $this->get_tli());
  //  return "<a href=\"" . $url . "\" class=\"show\">" . $this->get_tli() . "</a>";
  //}

  // tagline
  public function set_tagline($var) {
    $this->tagline = $var;
  }
  public function get_tagline() {
    return $this->tagline;
  }

  // registrar
  public function set_registrar($var) {
    $this->registrar = $var;
  }
  public function get_registrar() {
    return $this->registrar;
  }

  // method
  public function get_cell_status_as_first_line() {
    $markup = "";

    $markup .= "  <td style=\"background-color: " . $project->get_status_background_color() . "\">\n";
    $markup .= "      " . $this->get_status() . "\n";
    $markup .= "    </td>\n";

    //$subject = $this->get_registrar();
    //$registrar_info_first_line = substr($subject, 0, strpos($subject, "\n"));
    //if (strstr($registrar_info_first_line, "58")) {
      //$markup .= "    <td style=\"background-color: #A6D785;";
      // special add background
      // todo remove the hard-coded id below
      //$table_name_string = "suppliers";
      //$id = "58";
      //$registrar_info_first_line = substr($subject, 0, strpos($subject, "\n"));
      //if (strpos($registrar_info_first_line, "expires")) {
      //  $color_hex = "#A6D785";
      //} else {
      //  $color_hex = "red";
      //}
      //$markup .= $this->get_cell_background_element_parts($table_name_string, $id, $color_hex);
      //$markup .= "\">\n";
      //$markup .= "      " . $registrar_info_first_line . "\n";
      //$markup .= "    </td>\n";
    //} else if (strstr($registrar_info_first_line, "basecamp")) {
      //$markup .= "    <td style=\"background-color: #A6D785;\">\n";
      //$markup .= "      " . $this->get_registrar() . "\n";
      //$markup .= "    </td>\n";
   //} else if (strpos($registrar_info_first_line, "client-owned") !== false) {
      //$markup .= "    <td style=\"background-color: #A6D785;\">\n";
      //$markup .= "      " . $this->get_registrar() . "\n";
      //$markup .= "    </td>\n";
    //} else {
      // # gray71
      //$markup .= "    <td style=\"background-color: #AAAAAA;\">\n";
      //$markup .= "      " . $this->get_registrar() . "\n";
      //$markup .= "    </td>\n";
    //}

    return $markup;
  }

  // method
  public function get_status_as_first_line() {
    $markup = "";

    $markup .= "      " . $this->get_status() . "\n";

    return $markup;
  }

  // method
  public function get_cell_registrar_as_checklist() {
    $markup = "";

    if ($this->get_registrar()) {
        if ($this->get_registrar() == "local") {
          $markup .= "    <td style=\"text-align: center; background-color: #AAD4A3;\">\n";
        } else if ($this->get_registrar() == "client") {
          $markup .= "    <td style=\"text-align: center; background-color: #DDBBA3;\">\n";
        } else if ($this->get_registrar() == "subdir") {
          $markup .= "    <td style=\"text-align: center; background-color: #DDEFDD;\">\n";
    	} else {
	  $markup .= "    <td style=\"text-align: center; background-color: #AAFFB3;\">\n";
        }
    } else {
      $markup .= "    <td style=\"text-align: center; background-color: #B9AADD;\">\n";
    }
    // note shorten string
    $start_of_string = 0;
    $length_of_string = 5;
    $registrar_short = substr($this->get_registrar(), $start_of_string, $length_of_string);
    // note remove quotes from string
    $registrar_short = preg_replace("/\"/", "", $registrar_short);
    $markup .= $registrar_short . "<br />\n";
    $markup .= "    </td>\n";

    return $markup;
  }

  // method
  public function get_cell_hosting_as_checklist() {
    $markup = "";

    $hosting_string_to_return = $this->get_hosting();
    // if empty then dull background color
    if (! $hosting_string_to_return) {
      $markup .= "    <td style=\"text-align: center; background-color: #B9AADD;\">\n";
    } else {
      $markup .= "    <td style=\"text-align: center; background-color: #AAFFB3;\">\n";
      // todo try to extract the suppliers id
      //a todo if there is a particular string in the field
       $length = 5;
       $hosting_string_to_return = substr($hosting_string_to_return, 0, $length);
      $markup .= $hosting_string_to_return . "<br />\n";

    }
    $markup .= "    </td>\n";

    return $markup;
  }

  // method
  public function get_cell_registrar() {
    $markup = "";

    $subject = $this->get_registrar();
    $background_color = "#EFEFEF";
    if (strpos($subject, "suppliers") !== false) {
      $background_color = "#AAFFB3";
    } else if (strpos($subject, "dns") !== false) {
      $background_color = "#A4AD4D";
    } else if (strpos($subject, "client") !== false) {
      $background_color = "#E6FFB3;";
    } else if (strpos($subject, "dir") !== false) { 
      // flat yellow
      $background_color = "#E6FF88";
    } else if (strpos($subject, "user") !== false) {
      $background_color = "#AAFFB3";
    } else if ($this->get_registrar() == "local_dns") {
      $background_color = "#AAFFB3";
    } else if ($this->get_registrar() == "vacant") {
      $background_color = "#B9AADD";
    }
    $markup .= "    <td style=\"background-color: " . $background_color . ";\">\n";
    $markup .= "      " . $this->get_registrar() . "\n";
    $markup .= "    </td>\n";
    
    return $markup;
  }   

  // hosting
  public function set_hosting($var) {
    $this->hosting = $var;
  }
  public function get_hosting() {
    return $this->hosting;
  }

  // method
  public function get_cell_hosting_as_first_line($in_cell = "DEFAULT-IS-ON") {
    $markup = "";

    $subject = $this->get_hosting();
    $hosting_info_first_line = substr($subject, 0, strpos($subject, "\n"));

    // gray71
    // defai;t
    $background_color = "#AAAAAA";
    if (strstr($hosting_info_first_line, "45")) {
      // glowing gree
      $background_color = "#AAFFB3";
    } else if (strstr($hosting_info_first_line, "whm")) {
      // dark purple
      $background_color = "#8470FF";

    } else if (strstr($hosting_info_first_line, "reseller-rbo")) {
      // light purple
      $background_color = "#AAAAFF";

    } else if (strstr($hosting_info_first_line, "reseller-bvf")) {
      // color
      $background_color = "#DD7AFF";

    } else if (strstr($hosting_info_first_line, "whm mudia")) {
      // dark orange
      $background_color = "#EE7722";

    } else if (strstr($hosting_info_first_line, "client")) {
      // todo color no named
      $background_color = "#EE77AA";

    } else if (strstr($hosting_info_first_line, "reseller-mud")) {
      // light orange
      $background_color = "#FFC469";

    } else if (strstr($hosting_info_first_line, "suppliers/62")) {
      $background_color = "#BF5FFF";
  
    } else if (strstr($hosting_info_first_line, "basecamp")) {
      // red
      $background_color = "#CC9999";
  
    } else if (strstr($hosting_info_first_line, "client-hosted")) {
      # gray71
      $background_color = "#AAAAAA";

    } else if (strstr($hosting_info_first_line, "not online")) {
      # gray71
      $background_color = "#AAAAAA";
    }

    $markup .= "  <td style=\"background-color: " . $background_color . "\">\n";
    $markup .= "      " . $hosting_info_first_line . "\n";
    if ($in_cell) {
      $markup .= "    </td>\n";
    }

    return $markup;
  }

  // crm
  public function set_crm($var) {
    $this->crm = $var;
  }
  public function get_crm() {
    return $this->crm;
  }

  // design_obj
  public function get_design_obj() {
    if (! isset($this->design_obj)) {
      include_once("designs.php");
      $this->design_obj = new Designs($this->get_given_config());
    }
    return $this->design_obj;
  }

  // webpage_obj
  public function get_webpage_obj() {
    if (! isset($this->webpage_obj)) {
      include_once("webpages.php");
      $this->webpage_obj = new Webpages($this->get_given_config());
    }
    return $this->webpage_obj;
  }

  // maxonomy_obj
  public function get_maxonomy_obj() {
    if (! isset($this->maxonomy_obj)) {
      include_once("maxonomies.php");
      $this->maxonomy_obj = new Maxonomies($this->get_given_config());
    }
    return $this->maxonomy_obj;
  }

  // method
  public function get_design_as_link() {
    $url = $this->url("designs/" . $this->get_design());
    return "<a href=\"" . $url . "\">designs/" . $this->get_design_obj()->get_id() . "</a>";
  }

  // method
  public function get_cell_design_as_first_line() {
    $markup = "";

    if (! $this->get_design()) {
      return "<td></td>\n";
    }
    $subject = $this->get_design_as_link();
    if (strstr($subject, "designs")) {
      $markup .= "    <td style=\"background-color: #0099CC;";
      $known_good_ids = array("784", "504", "224", "35", "280", "32", "15", "672", "448", "560", "616", "18", "336", "43", "392", "42");
      foreach ($known_good_ids as $id) {
        if (strstr($subject, $id)) {
          // special add background
          // todo remove the hard-coded id below
          $table_name_string = "designs";
          $color_hex = "#0099CC";
          $markup .= $this->get_cell_background_element_parts($table_name_string, $id, $color_hex);
        }
      }
    } else if (strstr($subject, "projects")) {
      if (strstr($subject, "66")) {
        $markup .= "    <td style=\"background-color: #0099CC;";
        // special add background
        // todo remove the hard-coded id below
        $table_name_string = "projects";
        $id = "66";
        $color_hex = "#0099CC";
        $markup .= $this->get_cell_background_element_parts($table_name_string, $id, $color_hex);
      } else {
        $markup .= "    <td style=\"background-color: #FFFFFF;";
      }
    } else {
      $markup .= "    <td style=\"background-color: #FFFFFF;";
    }
    $markup .= "\">\n";
    $markup .= "      " . $subject . "\n";
    $markup .= "    </td>\n";

    return $markup;
  }

  // user_name
  public function set_user_name($var) {
    $this->user_name = $var;
  }
  public function get_user_name() {
    return $this->user_name;
  }

  // spotlight
  public function set_spotlight($var) {
    $this->spotlight = $var;
  }
  public function get_spotlight() {
    return $this->spotlight;
  }

  // backups
  public function set_backups($var) {
    $this->backups = $var;
  }
  public function get_backups() {
    return $this->backups;
  }

  // method
  public function get_cell_backups_as_first_line() {
    $markup = "";

    $subject = $this->get_backups();
    $lines = explode("\n", $subject);
    $first_line_string = $lines[0];
    if (strstr($first_line_string, "backup-wizard")) {
      $markup .= "  <td style=\"background-color: #0099CC;\">\n";
    } else if (strstr($first_line_string, "client-backup")) { 
      $markup .= "    <td style=\"background-color: #8B8682;\">\n";
    } else {
      $markup .= "  <td>\n";
    }
    $markup .= "    " . $first_line_string . "\n";
    $markup .= "  </td>\n";

    return $markup;
  }

  // method
  public function get_cell_databases_as_first_line() {
    $markup = "";

    include_once("databases.php");
    $databases_obj = new Databases($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $output_flag = "list";
    $subject = $databases_obj->get_databases_given_domain_tli($this->get_tli(), $user_obj, $output_flag);
    if (strstr($subject, "client-backup")) {
      $markup .= "    <td style=\"background-color: #0099CC;\">\n";
    } else if (strstr($subject, "table")) {
      $markup .= "    <td style=\"background-color: #0099CC;\">\n";
    } else {
      // no databases at all
      $markup .= "    <td style=\"background-color: #0099CC;\">\n";
    }
    $markup .= "      " . $subject . "\n";
    $markup .= "    </td>\n";

    return $markup;
  }

  // method
  public function get_cell_status_2_as_first_line() {
    $markup = "";

    $subject = $this->get_status();
    $status_info_first_line = substr($subject, 0, strpos($subject, "\n"));
    if (strstr($status_info_first_line, "focus")) {
      $markup .= "  <td style=\"background-color: #0099CC;\">\n";
    } else if (strstr($status_info_first_line, "blog")) {
      $markup .= "  <td style=\"background-color: #0099CC;\">\n";
    } else {
      $markup .= "  <td>\n";
    }
    $markup .= "    " . $status_info_first_line . "\n";
    $markup .= "  </td>\n";

    return $markup;
  }

  // method
  public function get_cell_machinehosts_as_first_line($in_cell = "DEFAULT-IS-ON") {
    $markup = "";

    include_once("machines.php");
    $machines_obj = new Machines($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $machines_string = $machines_obj->get_machines_list_given_domain_tli($this->get_tli(), $user_obj);

    if (strpos($machines_string, "No machines found.") !== false) {
      if ($in_cell) {
        # gray71
        $markup .= "    <td style=\"background-color: #AAAAAA;\">\n";
      }
    } else {
      if ($in_cell) {
        # light green 
     }
    }
    $markup .= "      " . $machines_string . "<br />\n";
    if ($in_cell) {
      $markup .= "    </td>\n";
    }

    return $markup;
  }

  // method
  public function get_cell_applications_as_first_line() {
    $markup = "";

    // todo this is really old and it encapulsates content and structure and style wow
    // todo also note that it goes directionly to the applications object (ignoring host_applications)
    include_once("applications.php");
    $applications_obj = new Applications($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $subject = $applications_obj->get_applications_comma_list_given_domain_tli($this->get_tli(), $user_obj);
    if ($this->get_tli() == "frb" ||
        $this->get_tli() == "pws" ||
        $this->get_tli() == "ogr" ||
        $this->get_tli() == "bvo") {
      # light green
      $markup .= "    <td style=\"background-color: #A6D785;\">\n";
    } else {
      $markup .= "    <td style=\"background-color: #FFFFFF;\">\n";
    }
    $markup .= "      " . $subject . "\n";
    $markup .= "    </td>\n";

    return $markup;
  }

  // derived
  // todo clean up these to a standard location in file
  private $tenperday_count;

  // tenperday_count
  public function reset_tenperday_count() {
    $this->tenperday_count = 0;
  }
  public function get_tenperday_count() {
    return $this->tenperday_count;
  }
  public function add_tenperday_count($var) {
    $this->tenperday_count = $this->tenperday_count + $var;
  }

  // log
  public function set_log($var) {
    $this->log = $var;
  }
  public function get_log() {
    return $this->log;
  }

  // ssl_cert
  public function set_ssl_cert($var) {
    $this->ssl_cert = $var;
  }
  public function get_ssl_cert() {
    return $this->ssl_cert;
  }

  // method
  private function make_domain() {
    $obj = new Domains($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_domain_tli()) {
      $this->set_type("get_by_given_domain_tli");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_maxonomy_id()) {
      $this->set_type("get_by_maxonomy_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_tli") {
      // security: only get the rows owned by the user
      // todo user_name is specified in the domains table (not projects table)
      // todo this is because the domains table is in a different database
      // todo when the databases are merged, the user_name will be in projects
      $sql = "select domains.* from domains WHERE domains.tli = '" . $this->get_given_domain_tli() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user

      if ($this->get_given_sort() == "horizontal" || $this->get_given_view() == "horizontal" || $this->get_given_sort() == "full" || $this->get_given_view() == "full" || $this->get_given_sort() == "problemography" || $this->get_given_view() == "problemography") {
        $sql = "select domains.* from domains WHERE domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY domains.spotlight DESC, domains.sort DESC;";

      } else if ($this->get_given_sort() == "foundation" || $this->get_given_view() == "foundation") {
        // note ORDER BY is now by domains.sort DESC
        $sql = "select domains.* from domains WHERE domains.user_name = '" . $this->get_user_obj()->name . "' AND domains.status != 'offline' ORDER BY domains.sort DESC, domains.spotlight, domains.tli;";

      } else if ($this->get_given_sort() == "measurement" || $this->get_given_view() == "measurement") {
        $sql = "select domains.* from domains WHERE domains.tli = domain_measurements.domain_tli AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY domains.spotlight DESC, domains.sort DESC;";

      } else {
        $sql = "select domains.* from domains WHERE domains.spotlight = '1' AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY sort DESC, tli;";
      }

    } else if ($this->get_type() == "get_by_project_id") {
      $sql = "SELECT domains.* FROM domains, scene_elements, processes, business_plan_texts, goal_statements, projects WHERE scene_elements.class_name_string = 'domains' AND domains.tli = scene_elements.class_primary_key_string AND scene_elements.process_id = processes.id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.goal_statement_id = goal_statements.id AND goal_statements.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . ";";

    } else if ($this->get_type() == "get_by_process_id") {
$sql = "SELECT domains.* FROM domains, scene_elements WHERE scene_elements.class_name_string = 'domains' AND domains.tli = scene_elements.class_primary_key_string AND scene_elements.process_id = " . $this->get_given_process_id() . ";";

    } else if ($this->get_type() == "get_by_maxonomy_id") {
      // dev here
     $sql = "SELECT domains.*, webpages.id, webpages.name, webpages.path, webpages.status, maxonomies.id, maxonomies.name FROM domains, maxonomies, webpages, webpage_maxonomies WHERE domains.tli = webpages.domain_tli AND webpages.id = webpage_maxonomies.webpage_id AND webpage_maxonomies.maxonomy_id = " . $this->get_given_maxonomy_id() . " AND webpage_maxonomies.maxonomy_id = maxonomies.id ORDER BY domains.tli, webpages.name, maxonomies.order_by;";

      // debug
      // print "debug domains.php: $sql\n";
      
    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data. type = " . $this->get_type());
    }

    // execute query
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
       $this->get_type() == "get_by_tli" || 
       $this->get_type() == "get_by_project_id" || 
       $this->get_type() == "get_by_process_id" || 
       $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_domain();
        $obj->set_tli(pg_result($results, $lt, 0));
        $obj->set_domain_name(pg_result($results, $lt, 1));
        $obj->set_tagline(pg_result($results, $lt, 2));
        $obj->set_img_url(pg_result($results, $lt, 3));
        $obj->set_sort(pg_result($results, $lt, 4));
        $obj->set_registrar(pg_result($results, $lt, 5));
        $obj->set_hosting(pg_result($results, $lt, 6));
        $obj->set_status(pg_result($results, $lt, 7));
        $obj->set_crm(pg_result($results, $lt, 8));
        $obj->set_name(pg_result($results, $lt, 9));
        $obj->set_user_name(pg_result($results, $lt, 10));
        $obj->set_spotlight(pg_result($results, $lt, 11));
        $obj->set_backups(pg_result($results, $lt, 12));
        $obj->set_log(pg_result($results, $lt, 13));
        $obj->get_design_obj()->set_id(pg_result($results, $lt, 14)); 
        $obj->set_description(pg_result($results, $lt, 15));
        $obj->set_ssl_cert(pg_result($results, $lt, 16));
      }
    } else if ($this->get_type() == "get_by_maxonomy_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_domain();
        $obj->set_tli(pg_result($results, $lt, 0));
        $obj->set_domain_name(pg_result($results, $lt, 1));
        $obj->set_tagline(pg_result($results, $lt, 2));
        $obj->set_img_url(pg_result($results, $lt, 3));
        $obj->set_sort(pg_result($results, $lt, 4));
        $obj->set_registrar(pg_result($results, $lt, 5));
        $obj->set_hosting(pg_result($results, $lt, 6));
        $obj->set_status(pg_result($results, $lt, 7));
        $obj->set_crm(pg_result($results, $lt, 8));
        $obj->set_name(pg_result($results, $lt, 9));
        $obj->set_user_name(pg_result($results, $lt, 10));
        $obj->set_spotlight(pg_result($results, $lt, 11));
        $obj->set_backups(pg_result($results, $lt, 12));
        $obj->set_log(pg_result($results, $lt, 13));
        $obj->set_description(pg_result($results, $lt, 15));
        $obj->set_ssl_cert(pg_result($results, $lt, 16));
        $obj->get_webpage_obj()->set_id(pg_result($results, $lt, 17));
        $obj->get_webpage_obj()->set_name(pg_result($results, $lt, 18));
        $obj->get_webpage_obj()->set_path(pg_result($results, $lt, 19));
        $obj->get_webpage_obj()->set_status(pg_result($results, $lt, 20));
        $obj->get_maxonomy_obj()->set_id(pg_result($results, $lt, 21));
        $obj->get_maxonomy_obj()->set_name(pg_result($results, $lt, 22));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    if ($this->get_type() == "get_by_project_id") {

      $markup .= $this->output_table_project_id();

    } else if ($this->get_type() == "get_all") {

      $markup .= $this->output_view();
      $markup .= $this->output_sort();
      $markup .= $this->output_table();

    }

    return $markup;
  }

  // method
  private function output_table_project_id() {
    $markup = "";

    // set up a utility class
    include_once("lib/timekeeper.php");
    $timekeeper_obj = new Timekeeper();

    // output
    $markup .= "<table class=\"plants\">\n";
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td style=\"background-color: #FFF;\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // img
      $markup .= "  <td style=\"background-color: #FFF;\">\n";
      // retrieve img_url from database
      $lookup_domain_obj = new Domains($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $lookup_domain_obj->get_img_url_as_element_given_tli($domain->get_tli(), $user_obj) . "\n";
      $markup .= "  </td>\n";

      // name with link
      $markup .= "  <td style=\"background-color: #FFF;\">\n";
      $markup .= "    " . $domain->get_tli_with_natural_link() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_view() {
    $markup = "";

    // view start
    if ($this->get_given_view() == "foundation") {
      $markup .= "foundation</a>";
    } else {
      $url = $this->url("domains");
      $parameter = "?view=foundation";
      $markup .= "<a href=\"" . $url . $parameter . "\">foundation</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "horizontal") {
      $markup .= "horizontal";
    } else {
      $url = $this->url("domains");
      $parameter = "?view=horizontal";
      $markup .= "<a href=\"" . $url . $parameter . "\">horizontal</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "measurements") {
      $markup .= "measurements";
    } else {
      $url = $this->url("domains");
      $parameter = "?view=measurements";
      $markup .= "<a href=\"" . $url . $parameter . "\">measurements</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "full") {
      $markup .= "full";
    } else {
      $url = $this->url("domains");
      $parameter = "?view=full";
      $markup .= "<a href=\"" . $url . $parameter . "\">full</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "problemography") {
      $markup .= "problemography";
    } else {
      $url = $this->url("domains");
      $parameter = "?view=problemography";
      $markup .= "<a href=\"" . $url . $parameter . "\">problemography</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "moneymakers") {
      $markup .= "moneymakers";
    } else {
      $url = $this->url("moneymakers");
      $markup .= "<a href=\"" . $url . "\">moneymakers</a>";
    }
    $markup .= "<br />";
    // view end

    return $markup;
  }

  // method
  public function output_sort() {
    $markup = "";

    // sort start
    if ($this->get_given_sort() == "status") {
      $markup .= "status</a>";
    } else {
      $url = $this->url("");
      $parameter = "?sort=status";
      $markup .= "<a href=\"" . $url . $parameter . "\">status</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "sort") {
      $markup .= "sort";
    } else {
      $url = $this->url("sort");
      $parameter = "?view=sort";
      $markup .= "<a href=\"" . $url . $parameter . "\">sort</a>";
    }
    $markup .= " | ";
    if ($this->get_given_view() == "name") {
      $markup .= "name";
    } else {
      $url = $this->url("");
      $parameter = "?sort=name";
      $markup .= "<a href=\"" . $url . $parameter . "\">name</a>";
    }
    $markup .= "<br />";
    // sort end

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // output actual table code
    // todo fix the special font shrinking below
    // todo set the size of the domains first display of menu
    $markup .= "<table class=\"plants\" style=\"font-size: 100%;\">\n";

    if ($this->get_given_view() == "horizontal") {
      $markup .= $this->output_horizontal_display();

    } else if ($this->get_given_view() == "foundation") {
      $markup .= $this->output_foundation_display();

    } else if ($this->get_given_view() == "measurements") {
      $markup .= $this->output_vertical_measurements_display();

    } else if ($this->get_given_view() == "problemography") {
      $markup .= $this->output_problemography_display();

    } else {
      $markup .= $this->output_vertical_display();
    }

    return $markup;
  }        

  // method
  private function output_vertical_display() {
    $markup = "";

    // each domain row is a row in the table
    $markup .= "\n  <!-- veritical_display -->\n";

    // headers
    $markup .= "  <tr>\n";

    if (1) {
      // #
      $markup .= "    <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "    </td>\n";
    }

    if (1) {
      // sort
      $column_name = "sort";
      // set up a utility class
      include_once("lib/timekeeper.php");
      $timekeeper_obj = new Timekeeper();
      $markup .= $timekeeper_obj->get_cell_colorized_given_sort_date($this->get_sort(), $column_name);

      // todo clean up the old version of sort that is below
      // // sort
      // // set up hyperlink so that user can sort rows
      // $markup .= "    <td class=\"header\">\n";
      // $url = $this->url("domains/");
      // $parameter = "?sort=sort";
      // if ($this->get_given_view()) {
      //   $parameter .= "&amp;view=" . $this->get_given_view();
      // }
      // $markup .= "      <a href=\"" . $url . $parameter . "\">sort</a>\n";
      // $markup .= "    </td>\n";
    }

    if (1) {
      // tli
      $markup .= "    <td class=\"header\">\n";
      $url = $this->url("domains/");
      $parameter = "?sort=tli";
      $markup .= "      <a href=\"" . $url . $parameter . "\">tli</a>\n";
      $markup .= "    </td>\n";
    }

    if (1) {
      // img_url
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      img\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // domain_name
      // set up hyperlink so that user can sort rows
      $markup .= "    <td class=\"header\">\n";
      $url = $this->url("domains/"); 
      $parameter = "?sort=domain_name";
      $markup .= "      <a href=\"" . $url . $parameter . "\">domain name</a>\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // name
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      name\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // registrar
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      registrar\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // spotlight
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      spotlight\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // tagline
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      tagline\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // design
      // set up hyperlink so that user can sort rows
      $row_name = "design";
      $markup .= "    <td class=\"header\">\n";
      $url = $this->url("domains/");
      $parameter = "?sort=" . $row_name;
      $markup .= "      <a href=\"" . $url . $parameter . "\">" . $row_name . "</a>\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // hosting
      $markup .= "    <td class=\"header\">\n";
      $url = $this->url("domains/");
      $this_sort = "hosting";
      $parameter = "?sort=" . $this_sort;
      $markup .= "      <a href=\"" . $url . $parameter . "\">" . $this_sort . "</a>\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // problemography
      // todo hard-coded not, but can count the number of problemographies
      $column_count = 8;
      $markup .= "    <td class=\"header\" colspan=\"" . $column_count . "\">\n";
      $markup .= "      problemography\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full" || $this->get_given_view() == "foundation") {
      //
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      status\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full" || $this->get_given_view() == "foundation") {
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      moneymakers\n";
      $markup .= "      " . $this->get_status() . "\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      // crm
      // set up hyperlink so that user can sort rows
      $row_name = "crm";
      $markup .= "    <td class=\"header\">\n";
      $url = $this->url("domains");
      $parameter = "?sort=" . $row_name;
      $markup .= "      <a href=\"" . $url . $parameter . "\">" . $row_name . "</a>\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full" || $this->get_given_view() == "foundation") {
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      webpages\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full") {
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      applications\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "full" || $this->get_given_view() == "measurements") {
      // measurements
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      measurements\n";
      $markup .= "    </td>\n";
    }

    $markup .= "  </tr>\n";
 
    $count_total_webpages = 0;
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {

      $markup .= "  <tr>\n";

      # deal with user_obj
      $user_obj = $this->get_user_obj();
      $domain->set_user_obj($user_obj);
        
      if (1) {
        $num++;
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      " . $num . "<br />\n";
        $markup .= "    </td>\n";
      }

      if (1) {
        $markup .= "    <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $domain->get_sort() . "<br />\n";
        $markup .= "    </td>\n";
      }

      if (1) {
        $markup .= "    <td class=\"mid\">\n";
        $url = $this->url("domains/" . $domain->get_tli());
        $markup .= "      <a href=\"" . $url . "\">" . $domain->get_tli() . "</a><br />\n";
        $markup .= "    </td>\n";
      }

      if (1) {
        $markup .= "    <td class=\"mid\" style=\"text-align: center\">\n";
        $markup .= "      <a href=\"http://" . $domain->get_domain_name() . "\"><img src=\"" . $domain->get_img_url() . "\" alt=\"" . $domain->get_tli() . "\" width=\"46\" /></a><br />\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      <a href=\"http://" . $domain->get_domain_name() . "\">" . $domain->get_domain_name() . "</a>\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        // name
        $markup .= "    <td class=\"mid\" style=\"vertical-align: top;\">\n";
        $markup .= "      " . $domain->get_name() . "\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        // tagline
        if (strstr($domain->get_tagline(), "<pre>")) {
          $markup .= "    <td style=\"color: #FFC125; background-color: #7F00FF; margin: 3px 3px 3px 0px; padding: 3px 3px 3px 3px;\">\n";
        } else {
          $markup .= "    <td>\n";
        }

        $markup .= "      " . $domain->get_tagline() . "\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        // design
        $markup .= "    <td class=\"mid\" style=\"vertical-align: top;\">\n";
        $markup .= "      " . $domain->get_design_obj()->get_id() . "\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      " . $domain->get_registrar() . "\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      " . $domain->get_spotlight() . "\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      " . $domain->get_hosting() . "\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "      " . $domain->get_problemography($domain->get_tli()) . "\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "  <td style=\"background-color: " . $project->get_status_background_color() . "\">\n";
        $markup .= "      <div style=\"font-size: 80%;\">\n";
        $markup .= "      " . $domain->get_status() . "\n";
        $markup .= "      </div>\n";
        $markup .= "  </td>\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "    <td class=\"mid\">\n";
        // get moneymakers data
        //include_once("moneymakers.php");
        //$moneymaker_obj = new Moneymakers($this->get_given_config());
        //$user_obj = $this->get_user_obj();
        //$moneymaker_data = $moneymaker_obj->get_count_moneymakers_given_tli($domain->get_tli(), $user_obj);
        //$url = $this->url("moneymakers/domain/" . $domain->get_tli());
        //$markup .= "<a href=\"" . $url . "\">";
        //$markup .= "mm " . $moneymaker_data;
        //$markup .= "</a>";
        //$markup .= "\n";
        //$markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        // webpages
        $markup .= "    <td class=\"mid\">\n";
        include_once("webpages.php");
        $webpage_obj = new Webpages($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $count_webpages = $webpage_obj->get_count_webpages($domain->get_tli(), $user_obj);
        $count_total_webpages += $count_webpages;
        // make url
        $url = $this->url("webpages/domain/" . $domain->get_tli());
        // todo: document key "rt = running total"
        $markup .= "<a href=\"" . $url . "\">" . $count_webpages . " for " . $domain->get_tli() . "</a><br /><span style=\"color: #6666CC;\">of " . $count_total_webpages . " rt</span>\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "full") {
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      " . $domain->get_crm() . "\n";
        $markup .= "    </td>\n";
      }

      // applications
      if ($this->get_given_view() == "full") {
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      <div style=\"font-size: 80%;\">\n";
        // part 1
        //$webpage_obj = new Webpages($this->get_given_config());
        //$user_obj = $this->get_user_obj();
        //$applications_report = "";
        //$applications_report .= " <em>apps</em>&nbsp;<span style=\"font-size: 140%;\">" . $webpage_obj->get_count_webpages_type_application($domain->get_tli(), $user_obj) . "</span><br />\n";
        // part 2
        //include_once("applications.php");
        //$application_obj = new Applications($this->get_given_config());
        //$url = $this->url("applications/domain/" . $domain->get_tli());
        //$applications_report .= "<a href=\"" . $url . "\">";
        //$applications_report .= "app rows ";
        //$applications_report .= "<span style=\"font-size: 140%;\">" . $application_obj->get_count_applications_given_tli($domain->get_tli()) . "</span>\n";
        //$markup .= "      " . $applications_report . "</a><br />\n";
        //$markup .= "      </div>\n";
        $markup .= "    </td>\n";
      }

      $markup .= "  </tr>\n";
    }
    $markup .= "</table>\n";

    // todo clean up the following notes
    if ($this->get_given_view() == "full") {
      //$markup .= "<p><em>tenperday count</em> = " . $tenperday_count . "</p>\n";
      $markup .= "<h2>historical tenperday counts</h2>\n";
      $markup .= "<p>[make this a link so that the processing is not felt here.]</p>\n";
    }

    return $markup;
  }

  // method
  private function output_problemography_display() {
    $markup = "";

    // each domain row is a row in the table
    $markup .= "\n  <!-- veritical_display -->\n";

    // headers
    $markup .= "  <tr>\n";

    if (1) {
      // #
      $markup .= "    <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "    </td>\n";
    }

    if (1) {
      // sort
      $column_name = "sort";
      $markup .= "    <td class=\"header\">\n";
      $markup .= "     sort\n";
      $markup .= "    </td>\n";
    }

    if (1) {
      // tli
      $markup .= "    <td class=\"header\">\n";
      $url = $this->url("domains/");
      $parameter = "?sort=tli";
      $markup .= "      <a href=\"" . $url . $parameter . "\">tli</a>\n";
      $markup .= "    </td>\n";
    }

    if (1) {
      // img_url
      $markup .= "    <td class=\"header\">\n";
      $markup .= "      img\n";
      $markup .= "    </td>\n";
    }

    if ($this->get_given_view() == "problemography") {
      // problemography
      // todo hard-coded not, but can count the number of problemographies
      $column_count = 8;
      $markup .= "    <td class=\"header\" colspan=\"" . $column_count . "\">\n";
      $markup .= "      problemography\n";
      $markup .= "    </td>\n";
    }

    $markup .= "  </tr>\n";
 
    $count_total_webpages = 0;
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {

      $markup .= "  <tr>\n";

      # deal with user_obj
      $user_obj = $this->get_user_obj();
      $domain->set_user_obj($user_obj);
        
      if (1) {
        $num++;
        $markup .= "    <td class=\"mid\">\n";
        $markup .= "      " . $num . "<br />\n";
        $markup .= "    </td>\n";
      }

      if (1) {
        $markup .= "    <td class=\"mid\" align=\"left\">\n";
        $markup .= "    " . $domain->get_sort() . "<br />\n";
        $markup .= "    </td>\n";
      }

      if (1) {
        $markup .= "    <td class=\"mid\">\n";
        $url = $this->url("domains/" . $domain->get_tli());
        $markup .= "      <a href=\"" . $url . "\">" . $domain->get_tli() . "</a><br />\n";
        $markup .= "    </td>\n";
      }

      if (1) {
        $markup .= "    <td class=\"mid\" style=\"text-align: center\">\n";
        $markup .= "      <a href=\"http://" . $domain->get_domain_name() . "\"><img src=\"" . $domain->get_img_url() . "\" alt=\"" . $domain->get_tli() . "\" width=\"46\" /></a><br />\n";
        $markup .= "    </td>\n";
      }

      if ($this->get_given_view() == "problemography") {
        $markup .= "      " . $domain->get_problemography($domain->get_tli()) . "\n";
      }

      $markup .= "  </tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  private function output_foundation_display() {
    $markup = "";

    // note: each domain row is a row in the table

    // headers
    $markup .= "  <tr>\n";

    // num
    $markup .= "    <td class=\"header\">\n";
    $markup .= "      #\n";
    $markup .= "    </td>\n";

    // status
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "      status\n";
    $markup .= "    </td>\n";

    // sort
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $url = $this->url("domains/");
    $parameter = "?sort=sort";
    $markup .= "      <a href=\"" . $url . $parameter . "\">sort</a>\n";
    $markup .= "    </td>\n";

    // tli
    $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px;\">\n";
    $url = $this->url("domains/");
    $parameter = "?sort=tli";
    $markup .= "<a href=\"" . $url . $parameter . "\">tli</a>\n";
    $markup .= "    </td>\n";

    // img_url
    $markup .= "    <td class=\"header\">\n";
    $markup .= "      img\n";
    $markup .= "    </td>\n";

    // domain_name
    $markup .= "    <td class=\"header\" style=\"width: 200px; padding: 0px 0px 0px 3px; text-align: center;\">\n";
    $markup .= "      domain_name<br />\n";
    $markup .= "    </td>\n";

    // name
    if (0) {
      $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px;\">\n";
      $markup .= "      name\n";
      $markup .= "    </td>\n";
    }

    // registrar
    $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px; text-align: left;\">\n";
    $markup .= "      &nbsp;reg&nbsp;\n";
    $markup .= "    </td>\n";

    // hosting
    $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px; text-align: left;\">\n";
    $markup .= "      &nbsp;host&nbsp;\n";
    $markup .= "    </td>\n";

    // crm
    $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px; text-align: center;\">\n";
    $markup .= "      &nbsp;crm&nbsp;\n";
    $markup .= "    </td>\n";

    // webpages
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "      &nbsp;web&nbsp;\n";
    $markup .= "    </td>\n";

    // count from tenperdays
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $url = $this->url("tenperdays");
    $markup .= "      &nbsp;<a href=\"" . $url . "\">count</a>&nbsp;\n";
    $markup .= "    </td>\n";

    // spotlight
    //$markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px; text-align: center;\">\n";
    //$markup .= "      spotlight\n";
    //$markup .= "    </td>\n";

    // applications via host_applications
    $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px; text-align: center;\">\n";
    $url = $this->url("applications");
    $markup .= "      &nbsp;<a href=\"" . $url . "\">app</a>&nbsp;\n";
    $markup .= "    </td>\n";

    // databases via host_databases
    $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px; text-align: center;\">\n";
    $url = $this->url("databases");
    $markup .= "      &nbsp;<a href=\"" . $url . "\">dbs</a>&nbsp;\n";
    $markup .= "    </td>\n";

    // email_addresses via host_email_addresses
    $markup .= "    <td class=\"header\" style=\"padding: 0px 0px 0px 3px; text-align: center;\">\n";
    $url = $this->url("email_addresses");
    $markup .= "      &nbsp;<a href=\"" . $url . "\">ema</a>&nbsp;\n";
    $markup .= "    </td>\n";

    // domain_measurements count
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $markup .= "      &nbsp;meas&nbsp;\n";
    $markup .= "    </td>\n";

    // webpages are associated with maxonomies via webpage_maxonomies
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $url = $this->url("maxonomies");
    $markup .= "      &nbsp;<a href=\"" . $url . "\">maxo</a>&nbsp;\n";
    $markup .= "    </td>\n";

    // webpages are associated with moneymakers via webpage_moneymakers
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $url = $this->url("moneymakers");
    $markup .= "      &nbsp;<a href=\"" . $url . "\">mone</a>&nbsp;\n";
    $markup .= "    </td>\n";

    // webpages are associated with tags via webpage_tags
    $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    $url = $this->url("tags");
    $markup .= "      &nbsp;<a href=\"" . $url . "\">tags</a>&nbsp;\n";
    $markup .= "    </td>\n";

    // scene_elements count
    //$markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
    //$markup .= "      scene elements\n";
    //$markup .= "    </td>\n";

    $details_flag = "";
    if ($details_flag) {

      // description
      //$markup .= "    <td class=\"header\">\n";
      //$markup .= "      description\n";
      //$markup .= "    </td>\n";

      // machines_hosts
      //$url = $this->url("hosts/domain/");
      //$markup .= "      <a href=\"" . $url . "\">machine_hosts</a>\n";
      //$markup .= "      <a href=\"" . $url . "\">mh</a>\n";

      // design_id
      $markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
      $markup .= "      design_id\n";
      $markup .= "    </td>\n";

      // budgets
      //$markup .= "    <td class=\"header\">\n";
      //$markup .= "      budgets\n";
      //$markup .= "    </td>\n";

      // blogposts
      //$markup .= "    <td class=\"header\" style=\"text-align: center;\">\n";
      //$markup .= "      blogposts\n";
      //$markup .= "    </td>\n";
    }

    $markup .= "  </tr>\n";

    // set up the variables for the counters
    $count_total_webpages = 0;

    // color
    $group = "#A6D785"; #guacamole

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {
      $markup .= "  <tr>\n";

      // deal with user_obj
      $user_obj = $this->get_user_obj();
      $domain->set_user_obj($user_obj);
        
      // num
      $num++;
      $markup .= "    <td class=\"mid\" style=\"background-color: " . $group . ";\">\n";
      $markup .= "      " . $num . "\n";
      $markup .= "    </td>\n";

      // status
      // default
      $background_color = "#A6D785";
      // define special
      $special_first_character = "z";
      if (strpos($domain->get_status_special_first_only(), $special_first_character) !== false ||
          strpos($domain->get_status(), "care") !== false ||
          strpos($domain->get_status(), "tenperday") !== false) {
        // light green 
        $background_color = "#AAFFB3";
      } else if (strpos($domain->get_status(), "demo") !== false ||
                 strpos($domain->get_status(), "dev") !== false ||
                 strpos($domain->get_status(), "mirror") !== false) {
        // colorize
        $background_color = "#A648AA";
      } else if (strpos($domain->get_status(), "expired") !== false ||
                 strpos($domain->get_status(), "client") !== false ||
                 strpos($domain->get_status(), "retired") !== false) {
        // colorize
        $background_color = "#999977";
      }
      if ($domain->get_status() == "zoneline") {
        $markup .= "  <td style=\"padding: 2px; text-align: center; background-color: " . $domain->get_status_background_color() . "\">\n";
      } else {
        $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: " . $background_color . ";\">\n";
      }
      $markup .= "      " . $domain->get_status() . "<br />";
      $markup .= "    </td>\n";

      // sort
      $sort = $domain->get_sort();
      $column_name = "sort";
      $class_name_for_url = "domains";
      // set up a utility class
      include_once("lib/timekeeper.php");
      $timekeeper_obj = new Timekeeper();
      $id = $domain->get_id();
      $given_view = $this->get_given_view();
      $markup .= $timekeeper_obj->get_cell_colorized_given_sort_date($sort, $column_name, $class_name_for_url, $id, $given_view);
 
      // tli
      $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #A6D785;\">\n";
      $url = $this->url("domains/" . $domain->get_tli());
      $markup .= "      <a href=\"" . $url . "\">" . $domain->get_tli() . "</a><br />";
      $markup .= "    </td>\n";

      // img_url
      $markup .= "    <td class=\"mid\" style=\"text-align: center; background-color: " . $this->get_td_background_color($domain->get_img_url()) . ";\">\n";
      $size = "20";
      $markup .= "      <a href=\"http://" . $domain->get_domain_name() . "\"><img src=\"" . $domain->get_img_url() . "\" alt=\"" . $domain->get_tli() . "\" width=\"" . $size . "\" height=\"" . $size . "\" style=\"vertical-align: middle;\"></a>\n";
      $markup .= "    </td>\n";

      // domain_name as link
      $markup .= "    <td class=\"mid\" style=\"background-color: " . $group . ";\">\n";
      $url = "http://" . $domain->get_domain_name();
      $markup .= "      <a href=\"" . $url . "\">" . $domain->get_domain_name() . "</a><br />\n";
      $markup .= "    </td>\n";
 
      // name
      if (0) {
        $markup .= "    <td style=\"padding: 2px; text-align: left; background-color: #A6D785;\">\n";
        $markup .= $domain->get_name() . "\n";
        $markup .= "    </td>\n";
      }

      // registrar
      $markup .= $domain->get_cell_registrar_as_checklist() . "\n";

      // hosting
      $markup .= $domain->get_cell_hosting_as_checklist() . "\n";

      // crm
      if (strpos($domain->get_crm(), "applications/51") > -1) {
        $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #AAFFB3;\">\n";
        $markup .= "      wp\n";
      } else {
        $markup .= "    <td style=\"background-color: #B9AADD;\">\n";
      }
      $markup .= "    </td>\n";

      // webpages
      // webpages --> webpage_count
      include_once("webpages.php");
      $webpage_obj = new Webpages($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $count_webpages = $webpage_obj->get_count_webpages($domain->get_tli(), $user_obj);
      if ($count_webpages) {
        $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #A6D785;\">\n";
        // todo the following line might make more REST sense, so re-consider
        //$url = $this->url("webpages/domain/" . $domain->get_tli());
        // todo not sure why this works but it appears to be coded for it
        $url = $this->url("webpages/" . $domain->get_tli());
        $markup .= "<a href=\"" . $url . "\">" . $count_webpages;
        // todo turn off label to make the screen have less clutter (text itself)
        $markup .= "</a>\n";
      } else {
        $markup .= "<td>\n";
      }

      // count from tenperdays
      include_once("tenperdays.php");
      $tenperday_obj = new Tenperdays($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $count = $tenperday_obj->get_webpage_count_given_tli($domain->get_tli(), $user_obj);
      if ($count) {
        $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #A6D785;\">\n";
        $url = $this->url("tenperdays");
        $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $count . "</a>";
        $markup .= "\n";
      } else {
        $markup .= "    <td>\n";
      }
      $markup .= "    </td>\n";

      // spotlight
      //$markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #AAFFB3;\">\n";
      //$markup .= "      " . $domain->get_spotlight() . "\n";
      //$markup .= "    </td>\n";

      // get applications via host_applications table
      include_once("host_applications.php");
      $host_application_obj = new HostApplications($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $domain_tli = $domain->get_tli();
      $applications_string = $host_application_obj->get_applications_count_given_domain_tli($domain_tli, $user_obj);
      if ($applications_string) {
        $markup .= "    <td style=\"text-align: center; background-color: #AAFFB3;\">\n";
        $url = $this->url("applications/" . $domain->get_tli());
        $markup .= "      <a href=\"" . $url . "\">" . $applications_string . "</a>\n";
      } else {
        $markup .= "    <td style=\"background-color: #CCCCCC;text-align: center;\">\n";
      }
      $markup .= "    </td>\n";

      // dbs
      // get databases via host_databases table
      include_once("host_databases.php");
      $host_database_obj = new HostDatabases($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $domain_tli = $domain->get_tli();
      $databases_string = $host_database_obj->get_databases_count_given_domain_tli($domain_tli, $user_obj, "zoneline");
      if ($databases_string) {
        $markup .= "    <td style=\"text-align: center; background-color: #AAFFB3;\">\n";
        $url = $this->url("databases/" . $domain->get_tli());
        $markup .= "      <a href=\"" . $url . "\">" . $databases_string . "</a>\n";
      } else {
        $markup .= "    <td style=\"background-color: #CCCCCC;text-align: center;\">\n";
      }
      $markup .= "    </td>\n";

      // ema
      // get email_addresses via host_email_addresses table
      include_once("host_email_addresses.php");
      $host_email_address_obj = new HostEmailAddresses($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $domain_tli = $domain->get_tli();
      $email_addresses_string = $host_email_address_obj->get_email_addresses_count_given_domain_tli($domain_tli, $user_obj);
      if ($email_addresses_string) {
        $markup .= "    <td style=\"text-align: center; background-color: #AAFFB3;\">\n";
        $url = $this->url("email_addresses/" . $domain->get_tli());
        $markup .= "      <a href=\"" . $url . "\">" . $email_addresses_string . "</a>\n";
      } else {
        $markup .= "    <td style=\"background-color: #CCCCCC;text-align: center;\">\n";
      }
      $markup .= "    </td>\n";

      // domain_measurements count (dm count)
      include_once("domain_measurements.php");
      $domain_measurement_obj = new DomainMeasurements($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $measurement = $domain_measurement_obj->get_count_given_tli($domain->get_tli(), $user_obj);
      if ($measurement >= 22) {
        $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #AAFFB3;\">\n";
      } else {
        $markup .= "    <td style=\"padding: 2px; text-align: center;\">\n";
      }
      if ($measurement) {
        $markup .= "    " . $measurement . "\n";
      }
      $markup .= "    </td>\n";

      // maxonomies
      include_once("webpage_maxonomies.php");
      $webpage_maxonomy_obj = new WebpageMaxonomies($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $webpage_maxonomies_data = $webpage_maxonomy_obj->get_count_given_tli($domain->get_tli(), $user_obj);
      if ($webpage_maxonomies_data) {
          $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #AAFFB3;\">\n";
      } else {
        $markup .= "    <td style=\"padding: 2px; text-align: center;\">\n";
      }
      if (isset($webpage_maxonomies_data) && $webpage_maxonomies_data) {
        $url = $this->url("maxonomies/?domain_tli=" . $domain->get_tli());
        $markup .= "<a href=\"" . $url . "\" class=\"show\">";
        $markup .= $webpage_maxonomies_data;
        $markup .= "</a>";
        $markup .= "\n";
      }
      $markup .= "    </td>\n";

      // moneymakers
      include_once("webpage_moneymakers.php");
      $webpage_moneymaker_obj = new WebpageMoneymakers($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $webpage_moneymaker_data = $webpage_moneymaker_obj->get_count_given_tli($domain->get_tli(), $user_obj);
      if ($webpage_moneymaker_data) {
        $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #AAFFB3;\">\n";
      } else {
        $markup .= "    <td style=\"padding: 2px; text-align: center;\">\n";
      }
      if (isset($webpage_moneymaker_data) && $webpage_moneymaker_data) {
        $url = $this->url("moneymakers/" . $domain->get_tli());
        $markup .= "<a href=\"" . $url . "\" class=\"show\">";
        $markup .= $webpage_moneymaker_data;
        $markup .= "</a>";
        $markup .= "\n";
      }
      $markup .= "    </td>\n";

      // tags
      include_once("webpage_tags.php");
      $webpage_tag_obj = new WebpageTags($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $webpage_tag_data = $webpage_tag_obj->get_count_given_tli($domain->get_tli(), $user_obj);
      if ($webpage_tag_data) {
          $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: #AAFFB3;\">\n";
      } else {
        $markup .= "    <td style=\"padding: 2px; text-align: center;\">\n";
      }
      if (isset($webpage_tag_data) && $webpage_tag_data) {
        $url = $this->url("tags/?domain_tli=" . $domain->get_tli());
        $markup .= "<a href=\"" . $url . "\" class=\"show\">";
        $markup .= $webpage_tag_data;
        $markup .= "</a>";
        $markup .= "\n";
      }
      $markup .= "    </td>\n";

      // scene_elements
      // todo design way to hook up the domains instance to a scene_elements instance on other site
      //include_once("scene_elements.php");
      //$scene_element_obj = new SceneElements($this->get_given_config());
      //$user_obj = $this->get_user_obj();
      // todo note that this is hard-coded and should be a db issue
      //$class_name = get_class($this);
      //$id = $domain->get_id();
      //$count_of_matching_scene_elements = $scene_element_obj->get_count_of_matching_scene_elements($class_name, $id, $user_obj);
      //$scene_element_obj = new SceneElements($this->get_given_config());
      //$scene_elements_list_string = $scene_element_obj->get_list_of_matching_scene_elements($class_name, $id, $user_obj);
      //if (is_numeric($count_of_matching_scene_elements)) {
      //  $markup .= "    <td style=\"width: 0px; padding: 2px; text-align: center; background-color: #A6D785;\">\n";
      //  $markup .= $scene_elements_list_string . "\n";
      //} else {
      //  $markup .= "    <td style=\"width: 0px; padding: 2px; text-align: center;\">\n";
      //  $markup .= "    </td>\n";
      //}

      if ($details_flag) {

        // machine_hosts
        $in_cell = "";
        //$markup .= $domain->get_cell_machinehosts_as_first_line($in_cell) . "\n";

        // design_id
        if (1) { 
          $design_id = $domain->get_design_obj()->get_id();
          if ($design_id) {
            $markup .= "    <td style=\"width: 100px; padding: 2px; text-align: center; background-color: #A6D785;\">\n";
            $url = $this->url("designs/" . $design_id);
            $markup .= "<a href=\"" . $url . "\">" . $design_id . "</a>\n";
          } else {
            $markup .= "    <td style=\"background-color: #999999;\">\n";
          }
          $markup .= "    </td>\n";
        }

        // budgets
        if (0) { 
          include_once("budgets.php");
          $budget_obj = new Budgets($this->get_given_config());
          $user_obj = $this->get_user_obj();
          $start_date = "2014-05-01";
          $budget_list_string = $budget_obj->get_budgets_summary_given_domain_tli($domain->get_tli(), $user_obj, $start_date);
          if ($budget_list_string) {
            $markup .= "  <td style=\"color: yellow; text-align: left; background-color: #A6D785;\">\n";
          } else {
            # gray71
            $markup .= "  <td style=\"text-align: left; background-color: #AAAAAA;\">\n";
          }   
          $markup .= "      " . $budget_list_string . "\n";
          $markup .= "    </td>\n";
        }

        // blogposts
        //$markup .= "    <td style=\"text-align: center;\">\n";
        //include_once("blogposts.php");
        //$blogpost_obj = new Blogposts($this->get_given_config());
        //$user_obj = $this->get_user_obj();
        //$count_blogposts = $blogpost_obj->get_count_blogposts($domain->get_tli(), $user_obj);
        //$url = $this->url("blogposts/domains/" . $domain->get_tli());
        //$markup .= "<a href=\"" . $url . "\">" . $count_blogposts . "</a>";
        //$markup .= "    </td>\n";
      }

      $markup .= "</tr>\n";

      // bonus table row below
      $markup .= "  <tr>\n";
      // maxonomies
      if (0) { 
        $markup .= "  <td colspan=\"13\" style=\"color: yellow; text-align: left; background-color: #A6D785;\">\n";
        # todo here is the thinking check me on this
        # todo maxonomies are associated with webpages
        # todo see webpage_maxonomies
        include_once("webpage_maxonomies.php");
        $webpage_maxonomies_obj = new WebpageMaxonomies($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $webpage_maxonomies_obj->get_maxonomies_with_given_domain_tli($domain->get_tli(), $user_obj);
        $markup .= "    </td>\n";
      }
      $markup .= "  </tr>\n"; 
      // relationships email_addresses part 1 of 2
      if (0) { 
        $markup .= "  <tr>\n";
        $markup .= "  <td colspan=\"13\" style=\"color: yellow; text-align: left; background-color: #A6D785;\">\n";
        // PART 1 of 2 email_addresses
        include_once("email_addresses.php");
        $email_address_obj = new EmailAddresses($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= "    " . $email_address_obj->get_email_addresses_given_domain_tli($domain->get_tli(), $user_obj);
        $markup .= "    </td>\n";
        $markup .= "  </tr>\n";
      }
      // relationships email_addresses part 2 of 2
      if (0) { 
        $markup .= "  <tr>\n";
        $markup .= "    <td colspan=\"13\" style=\"color: yellow; text-align: left; background-color: #A6D785; padding: 2px 2px 2px 2px;\">\n";
        // PART 2 of 2 email_addresses
        include_once("host_email_addresses.php");
        $host_email_address_obj = new HostEmailAddresses($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $host_email_address_obj->get_email_addresses_given_domain_tli($domain->get_tli(), $user_obj);
        $markup .= "      </td>\n";
        $markup .= "    </tr>\n";
      }
      // relationships applications
      if (0) { 
        $markup .= "  <tr>\n";
        $markup .= "    <td colspan=\"13\" style=\"color: yellow; text-align: left; background-color: #A6D785; padding: 2px 2px 2px 2px;\">\n";
        include_once("host_applications.php");
        $host_application_obj = new HostApplications($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $host_application_obj->get_applications_given_domain_tli($domain->get_tli(), $user_obj);
        $markup .= "      </td>\n";
        $markup .= "    </tr>\n";
      }
    }
    $markup .= "</table>\n";

    // output totals
    //$markup .= "<p> total webpage count = " . $count_webpages . "</p>\n";

    return $markup;
  }

  // method
  private function output_vertical_measurements_display() {
    $markup = "";

    // note: each domain row is a row in the table

    // headers
    $markup .= "<tr>\n";

    // num
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";

    // sort
    // set up hyperlink so that user can sort rows
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("domains/");
    $parameter = "?sort=sort";
    if ($this->get_given_view()) {
      $parameter .= "&amp;view=" . $this->get_given_view();
    }
    $markup .= "    <a href=\"" . $url . $parameter . "\">sort</a>\n";
    $markup .= "  </td>\n";

    // tli
    $markup .= "  <td class=\"header\">\n";
    $url = $this->url("domains/");
    $parameter = "?sort=tli";
    $markup .= "    <a href=\"" . $url . $parameter . "\">tli</a>\n";
    $markup .= "  </td>\n";

    // img_url
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";

    // measurements
    $markup .= "  <td class=\"header\" style=\"width: 340px;\">\n";
    $markup .= "    measurements\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";
 
    $count_total_webpages = 0;
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {
      $markup .= "<tr>\n";

      # deal with user_obj
      $user_obj = $this->get_user_obj();
      $domain->set_user_obj($user_obj);
        
      $num++;
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "    " . $domain->get_sort() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"mid\">\n";
      $url = $this->url("domains/" . $domain->get_tli());
      $markup .= "    <a href=\"" . $url . "\">" . $domain->get_tli() . "</a>\n";
      $markup .= "  </td>\n";

      //
      $markup .= "  <td class=\"mid\" style=\"text-align: center\">\n";
      $markup .= "    <a href=\"http://" . $domain->get_domain_name() . "\"><img src=\"" . $domain->get_img_url() . "\" alt=\"" . $domain->get_tli() . "\" width=\"46\" /></a>\n";
      $markup .= "  </td>\n";

      // measurements
      include_once("domain_measurements.php");
      $domain_measurement_obj = new DomainMeasurements($this->get_given_config());
      $markup .= "    " . $domain_measurement_obj->get_output_cell_given_tli($domain->get_tli()) . "\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  private function output_horizontal_display() {
    $markup = "";

    // reset
    $this->reset_tenperday_count();

    // loop for each row of data about the top row

    // row loop for icons
    if (1) {
      $markup .= "<tr>\n";
      // note: the following code is in file three times now
      // setup
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        $markup .= "  <td class=\"mid\" style=\"background-color: #FFF; text-align: center;\" width=\"9%\">\n";
        $padding = "2";
        $float = "";
        $width = "60";
        $url = "http://" . $domain->get_domain_name();
        $markup .= "<a href=\"" . $url . "\">";
        $markup .= $domain->get_img_url_as_img_element($padding, $float, $width);
        $markup .= "</a>\n";
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";
    }

    // row loop for tli
    if (1) {
      $markup .= "<tr>\n";
      // note: the following code is in file three times now
      // setup
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        $markup .= "  <td class=\"mid\" style=\"text-align: center;\" width=\"9%\">\n";
        $markup .= "    " . $domain->get_tli_with_natural_link() . "\n";
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";
    }

    // row loop for webpages
    $markup .= "<tr>\n";
    // note: the following code is in file three times now
    // setup
    $count_total_webpages = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {

      // cell 2
      // output cell
      $markup .= "  <td class=\"mid\" style=\"text-align: center;\" width=\"9%\">\n";
      // webpages
      // todo note that this block of code in in this file twice
      // todo take out hard-coded domain name below and fix ? in ULI
      // tally
      include_once("webpages.php");
      $webpage_obj = new Webpages($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $count_webpages = $webpage_obj->get_count_webpages($domain->get_tli(), $user_obj);
      $count_total_webpages += $count_webpages;
      // make url
      $url = $this->url("webpages/domain/"  . $domain->get_tli());
      // todo: document key "rt = running total"
      $markup .= "<a href=\"" . $url . "\">" . $count_webpages . "&nbsp;for&nbsp;" . $domain->get_tli() . "</a>&nbsp;<span style=\"color: #6666CC;\">of&nbsp;" . $count_total_webpages . "</span>\n";
      $markup .= "  </td>\n";
    }
    $markup .= "</tr>\n";

    // row loop for applications
    if (0) {
      $markup .= "<tr>\n";
      // note: the following code is in file three times now
      // setup
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        // cell 3
        $markup .= "  <td class=\"mid\" style=\"text-align: center;\" width=\"9%\">\n";
        $markup .= "    <div style=\"font-size: 80%;\">\n";
        // part 1
        $webpage_obj = new Webpages($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $applications_report = "";
        $applications_report .= " <em>apps</em>&nbsp;<span style=\"font-size: 140%;\">" . $webpage_obj->get_count_webpages_type_application($domain->get_tli(), $user_obj) . "</span>\n";
        // part 2
        include_once("applications.php");
        $application_obj = new Applications($this->get_given_config());
        $url = $this->url("applications/domain/" . $domain->get_tli());
        $applications_report .= "<a href=\"" . $url . "\">";
        $applications_report .= "app rows ";
        $applications_report .= "<span style=\"font-size: 140%;\">" . $application_obj->get_count_applications_given_tli($domain->get_tli()) . "</span>\n";
        $markup .= "    " . $applications_report . "</a>\n";
        $markup .= "    </div>\n";
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";
    }

    // row
    // loop for moneymakers
    //$markup .= "<tr>\n";
    // note: the following code is in file four times now
    // setup
    //foreach ($this->get_list_bliss()->get_list() as $domain) {
      // cell 4
      //$markup .= "  <td class=\"mid\" style=\"text-align: center;\" width=\"9%\">\n";
      // part 1
      //include_once("moneymakers.php");
      //$moneymaker_obj = new Moneymakers($this->get_given_config());
      //$user_obj = $this->get_user_obj();
      //$moneymaker_data = $moneymaker_obj->get_count_moneymakers_given_tli($domain->get_tli(), $user_obj);
      //$url = $this->url("moneymakers/domain/" . $domain->get_tli());
      //$markup .= "<a href=\"" . $url . "\">";
      //$markup .= "mm " . $moneymaker_data;
      //$markup .= "</a>";
      //$markup .= "\n";
      //$markup .= "  </td>\n";
    //}
    //$markup .= "</tr>\n";

    // row
    // loop for designs
    $markup .= "<tr>\n";
    // note: the following code is in file multiple times now
    // setup
    foreach ($this->get_list_bliss()->get_list() as $domain) {
      // cell 4
      $markup .= "  <td class=\"mid\" style=\"text-align: center;\" width=\"9%\">\n";
      // part 1
      include_once("designs.php");
      $design_obj = new Designs($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $design_data = $design_obj->get_count_given_tli($domain->get_tli(), $user_obj);
      $url = $this->url("designs/domain/" . $domain->get_tli());
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= "designs&nbsp;count&nbsp;" . $design_data;
      $markup .= "</a>";
      $markup .= "\n";
      $markup .= "  </td>\n";
    }
    $markup .= "</tr>\n";

    // row

    // loop for registar
    if (0) {
      $markup .= "<tr>\n";
      // note: the following code is in file multiple times now
      // setup
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        // part 1
        $registrar_data = $domain->get_registrar();
        $markup .= "  <td class=\"mid\" style=\"text-align: left; vertical-align: top;\">\n";
        $markup .= "<div style=\"font-size: 80%;\">\n";
        $markup .= "<span style=\"font-size: 140%;\">";
        $markup .= $registrar_data;
        $markup .= "</span>";
        $markup .= "\n";
        $markup .= "</div>\n";
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";
    }

    // row
    // loop for location_workdate (tenperday)
    if (0) {
      $markup .= "<tr>\n";
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        $markup .= "  <td class=\"mid\" style=\"vertical-align: top; text-align: center;\">\n";
        $markup .= $domain->get_tenperday($this);
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";
    }

    // row
    // loop status
    // todo use the following line as a horizontal on/off switch
    if (1) {
      if ($this->get_given_view() == "horizontal") {
        $markup .= "<tr>\n";
        // loop to get the cells of the row
        foreach ($this->get_list_bliss()->get_list() as $domain) {
          // part 1: colorized based upon span style
          $needle = "/background-color: #B4EEB4; font-weight: 600;/";
          if (preg_match($needle, $domain->get_status())) {
            $markup .= "  <td style=\"vertical-align: top; background-color: #B4EEB4;\">\n";
          } else {
            $markup .= "  <td style=\"vertical-align: top;\">\n";
          } 
          $markup .= "    " . $domain->get_status() . "\n";
          $markup .= "  </td>\n";
        }
        $markup .= "</tr>\n";
      }
    }

    // row for hosting
    if (0) {
      // loop for hosting + whm + cpanel + email
      $markup .= "<tr>\n";
      // note: the following code is in file four times now
      // setup
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        // part 1
        $hosting_data = $domain->get_hosting();
        $markup .= "  <td class=\"mid\" style=\"text-align: left; vertical-align: top;\">\n";
        $markup .= "<div style=\"font-size: 80%;\">\n";
        $markup .= "<span style=\"font-size: 140%;\">";
        $markup .= $hosting_data;
        $markup .= "</span>";
        $markup .= "\n";
        $markup .= "</div>\n";
        $markup .= "  </td>\n";
      }
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // output totals
    $markup .= "<p><em>tenperday count</em> = " . $this->get_tenperday_count() . "</p>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "<table class=\"plants\" border=\"1\">\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $domain) {

      // column 1
      // number
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    \n";
      $markup .= "  </td>\n";

      // column 2
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    fields<br />(&amp; associations)\n";
      $markup .= "  </td>\n";

      // column 3
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    domain_measurements\n";
      $markup .= "    webmaster_responsibility\n";
      $markup .= "  </td>\n";

      // column 4
      $markup .= "    <td class=\"header\" style=\"width: 120px; padding: 0px 0px 0px 3px;\">\n";
      $markup .= "    data<br />\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
      $markup .= "<tr>\n";

      // tli
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    1\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    tli\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      include_once("domain_measurements.php");
      $domain_measurement_obj = new DomainMeasurements($this->get_given_config());
      // todo need to incorporate this following line into the function parameters below it
      $user_obj = $this->get_user_obj();
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "tli");
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $domain->get_tli() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // domain_name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    2\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    domain_name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      include_once("domain_measurements.php");
      $domain_measurement_obj = new DomainMeasurements($this->get_given_config());
      // todo need to incorporate this following line into the function parameters below it
      $user_obj = $this->get_user_obj();
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "domain_name");
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    <a href=\"http://" . $domain->get_domain_name() . "\">" . $domain->get_domain_name() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // tagline
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    3\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    tagline\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "tagline");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_tagline()) . ";\">\n";
      $markup .= "  <div style=\"width: 300px;padding: 10px;\">\n";
      $markup .= "    " . $domain->get_tagline() . "\n";
      $markup .= "  </div>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // img_url
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    4\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "img_url");
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $markup .= "    <a href=\"http://" . $domain->get_domain_name() . "\"><img src=\"" . $domain->get_img_url() . "\" alt=\"" . $domain->get_tli() . "\" width=\"66\" /></a><br />\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // sort
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    5\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "sort");
      $markup .= "  </td>\n";
      $sort = $domain->get_sort();
      $column_name = "sort";
      $class_name_for_url = "domains";
      // set up a utility class
      include_once("lib/timekeeper.php");
      $timekeeper_obj = new Timekeeper();
      $id = $domain->get_id();
      $given_view = $this->get_given_view();
      $markup .= $timekeeper_obj->get_cell_colorized_given_sort_date($sort, $column_name, $class_name_for_url, $id, $given_view);
      $markup .= "</tr>\n";

      // registrar
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    6\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    registrar\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "registrar");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_registrar()) . ";\">\n";
      $markup .= "    " . $domain->get_registrar() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // hosting
      if (1) {
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    7\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
        $markup .= "    hosting\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "hosting");
        $markup .= "  </td>\n";
        $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_hosting()) . ";\">\n";
        $markup .= "    " . $domain->get_hosting() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";
      }

      // status
      if (1) {
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    8\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
        $markup .= "    status\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "status");
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= "  <div style=\"width: 350px;\">\n";
        $markup .= "    " . $domain->get_status() . "\n";
        $markup .= "  </div>\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";
      }

      // crm
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    9\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    crm\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "crm");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_crm()) . ";\">\n";
      $markup .= "    " . $domain->get_crm() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    10\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "name");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_name()) . ";\">\n";
      $markup .= "    " . $domain->get_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // user_name
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    11\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    user_name\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "user_name");
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $domain->get_user_name() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // spotlight
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    12\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    spotlight\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "spotlight");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_spotlight()) . ";\" class=\"mid\">\n";
      $markup .= "    " . $domain->get_spotlight() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // backups
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    13\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    backups\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "backups");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_backups()) . ";\">\n";
      $markup .= "    " . $domain->get_backups() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // log
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    14\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    log\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "log");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_log()) . ";\">\n";
      $markup .= "    <pre>" . $domain->get_log() . "</pre>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // design_id
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    15\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    design_id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      //$markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "design_id");
      $markup .= "[design_id is on hold]";
      $markup .= "  </td>\n";
      if ($domain->get_design_obj()->get_id()) {
        $markup .= "  <td>\n";
        $markup .= $domain->get_design_obj()->get_id() . "\n";
        // todo see this is weird because there already is an object
        // todo why not just allow the existing object to discovery
        // todo so this means that program-wide should look at new objects
        // todo and consider if they should just pop in and out of existence
        // todo which brings up the idea of how much is streamed and blogged
        // on hold
        if (0) {
          include_once("designs.php");
          $designs_obj = new Designs($this->get_given_config());
          $user_obj = $this->get_user_obj();
          $markup .= "    " . $designs_obj->get_name_with_link_given_id($domain->get_design_obj()->get_id(), $user_obj) . "\n";
          }
      } else {
        $markup .= "  <td style=\"background-color: red;\">\n";
      }
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // description
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    16\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "description");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_description()) . ";\">\n";
      $markup .= "  <div>\n";
      $markup .= "    " . $domain->get_description() . "\n";
      $markup .= "  </div>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // ssl_cert
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    17\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #EFEFEF;\">\n";
      $markup .= "    ssl_cert\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "ssl_cert");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"background-color: " . $this->get_td_background_color($domain->get_ssl_cert()) . ";\">\n";
      $markup .= "  <div>\n";
      $markup .= "    " . $domain->get_ssl_cert() . "\n";
      $markup .= "  </div>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // webpages
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    A\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
      $markup .= "    webpages\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "webpages");
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"stand-out\">\n";
      // todo this was changed because the url changed (check REST model)
      //$url = $this->url("webpages/domain/" . $domain->get_tli());
      $url = $this->url("webpages/" . $domain->get_tli());
      // get count
      include_once("webpages.php");
      $webpage_obj = new Webpages($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $count_webpages = $webpage_obj->get_count_webpages($domain->get_tli(), $user_obj);
      $markup .= "  <a href=\"" . $url . "\">" . $count_webpages . " webpages for " . $domain->get_tli() . "</a>\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // project_domains
      // the projects class may not be installed
      if (0) {
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    B\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
        $markup .= "    project_domains\n";
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "project_domains");
        $markup .= "  </td>\n";
        $markup .= "  <td>\n";
        include_once("projects.php");
        $projects_obj = new Projects($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $output_projects = $projects_obj->get_project_img_url_as_element_with_link_given_tli($domain->get_tli(), $user_obj);
        // todo decide if this whole table is better output than an img_url
        //$output_projects = $projects_obj->get_projects_given_tli($domain->get_tli(), $user_obj);
        $markup .= "    " . $output_projects . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";
      }

      // host_databases
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    C\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
      $markup .= "    host_databases\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "host_databases");
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      include_once("databases.php");
      $databases_obj = new Databases($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $databases_obj->get_databases_given_domain_tli($domain->get_tli(), $user_obj) . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // host_applications
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    D\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
      $markup .= "    applications\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "applications");
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      // applications
      include_once("applications.php");
      $application_obj = new Applications($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $application_obj->get_applications_list_given_domain_tli($domain->get_tli(), $user_obj);
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // host_email_addresses
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    E\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
      $markup .= "    host_email_addresses\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "host_email_addresses");
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      // email_addresses
      include_once("email_addresses.php");
      $email_address_obj = new EmailAddresses($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $email_address_obj->get_email_addresses_given_domain_tli($domain->get_tli(), $user_obj);
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      // budgets
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    F\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
      $markup .= "    budgets\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "budgets");
      $markup .= "  </td>\n";
      // output budgets
      include_once("budgets.php");
      $budget_obj = new Budgets($this->get_given_config());
      $user_obj = $this->get_user_obj();
      // todo clean up old replaced function
      //$budget_list_string = $budget_obj->get_budget_list_given_domain_tli($domain->get_tli(), $user_obj);
      $start_date = "2014-05-01";
      // todo some of these classes may not be installed
      //$budget_list_string = $budget_obj->get_budgets_summary_given_domain_tli($domain->get_tli(), $user_obj, $start_date);
      $budget_list_string = "[on hold]";
      if ($budget_list_string) {
        $markup .= "  <td style=\"color: #000; text-align: left; background-color: #A6D785;\">\n";
      } else {
        # gray71
        $markup .= "  <td style=\"text-align: left; background-color: #AAAAAA;\">\n";
      }   
      $markup .= "      " . $budget_list_string . "\n";
      $markup .= "    </td>\n";

      // budgets
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    G\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
      $markup .= "    analytics\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "analytics");
      $markup .= "  </td>\n";
      $markup .= "  <td style=\"text-align: left; background-color: #AAAAAA;\">\n";
      $markup .= "     (see google-analystics)\n";
      $markup .= "    </td>\n";

      // yawp-agent
      //$markup .= "<tr>\n";
      //$markup .= "  <td class=\"header\">\n";
      //$markup .= "    H\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td class=\"header\" style=\"background-color: #BBFF44;\">\n";
      //$markup .= "    yawp-agent\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= $domain_measurement_obj->get_output_given_tli_and_fieldname($domain->get_tli(), "yawp-agent");
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$markup .= "    [see indiegoal yawp-agent design xml]<br/ >\n";
      //$markup .= "  </td>\n";
      //$markup .= "</tr>\n";


    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function get_list_of_domain_tli() {
    $domain_tli_array = array();

    $this->set_type("get_all");

    $this->determine_type();

    $markup = $this->prepare_query();

    if ($markup) {
      //error
      print "error domains.php: " . $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $domain) {
      array_push($domain_tli_array, $domain->get_tli());
    }

    return $domain_tli_array;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("sort");
    $parameter_a->set_validation_type_as_sort();
    array_push($parameters, $parameter_a);

    // view
    $parameter_b = new Parameter();
    $parameter_b->set_name("view"); 
    $parameter_b->set_validation_type_as_view();
    array_push($parameters, $parameter_b);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_tli();
    array_push($parameters, $parameter_c);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "sort") {
            $this->set_given_sort($parameter->get_value());
          }
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "domains";
            $db_table_field = "tli";
            // get sort letter
            $domain_obj = new Domains($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $domain_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter);
          }
        }
      }
    }
    return $markup;
  }

  // method
  public function get_domain_name_given_tli($given_domain_tli, $given_user_obj) {
    $domain_name = "";

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);
    $this->set_type("get_by_id");

    $this->determine_type();

    $markup = $this->prepare_query();

    // todo deal with markup variable returned above

    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $domain_name = $obj->get_domain_name();
    }

    return $domain_name;
  }

  // special
  public function get_project_id() {
    $markup = "";

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_project_obj()->get_id();
    }

    return $markup;
  }

  private function get_problemography($given_tli) {
    $markup = "";

    include_once("webpage_maxonomies.php");
    $obj = new WebpageMaxonomies($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $maxonomies_array = $obj->get_problemography($given_tli, $user_obj);
    foreach ($maxonomies_array as $string) {
      $markup .= "  <td class=\"mid\">\n";
      $markup .= $string;;
      $markup .= "  </td>\n";
    }

    return $markup;
  }

  // method
  public function get_img_url_as_element_given_tli($given_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_tli);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there is 1 item in the list
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no donains was found.</p>\n";;
      return $markup;
    }
    if ($this->get_list_bliss()->get_count() > 1) {
      $markup .= "<p style=\"error\">more than 1 donain were found.</p>\n";;
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $domain) {
      // return data
      $width = 65;
      return $domain->get_img_as_img_element_with_link("", "", $width);
    }
  }

  // method
  public function get_tenperday($given_this) {
    $markup = "";

    // note this is the tenperday img_url matrix
    // notethis is a stack of img_url
    // setup
    $markup .= "    <div style=\"font-size: 80%;\">\n";
    include_once("location_workdates.php");
    $location_workdate_obj = new LocationWorkdates($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $workdates = $location_workdate_obj->get_workdates($this->get_tli(), $user_obj);
    if ($workdates) {
      // add the workdate
      $markup .= $workdates;
      // deal with tally
      // count is based upon the number of pairs of href strings
      // pairs because a workdate has an img_url that has a hyperlink
      $this_count = ( substr_count($workdates, "href") / 2 );
      // debug
      //print "debug domains this_count = " . $this_count . "<br />\n";
      $given_this->add_tenperday_count($this_count);
    } else {
      $markup .= "&nbsp;\n";
    }
    $markup .= "    </div>\n";
    return $markup;
  }

  // method
  private function get_cell_background_element_parts($given_table_name_string, $given_id, $given_color_hex) {
    $markup = "";

    // special for this id, add a background as td attribute with img_url
    if ($given_table_name_string == "suppliers") {
      include_once("suppliers.php");
      $obj = new Suppliers($this->get_given_config());
    } else if ($given_table_name_string == "applications") {
      include_once("applications.php");
      $obj = new Applications($this->get_given_config());
    } else if ($given_table_name_string == "designs") {
      include_once("designs.php");
      $obj = new Designs($this->get_given_config());
    } else if ($given_table_name_string == "projects") {
      include_once("projects.php");
      $obj = new Projects($this->get_given_config());
    } else {
      // fatal error for this function
      // debug
      //print "debug error domains: table_name_string is not known<br />\n";
      return $markup;
    }
    $user_obj = $this->get_user_obj();
    $img_url = $obj->get_img_url_given_id($given_id, $user_obj);
    //$markup .= "background-image: url(" . $img_url . "); background-color: " . $given_color_hex . "; background-repeat: no-repeat; background-position: center; background-size: 36px 34px;";
    // debug try this below so that it blanks out the img (which was showing up on the next in line
    $img_url = "";

    return $markup;
  }

  // method
  public function get_domains_array_given_project_id($given_project_id, $given_user_obj) {
    $array_of_domains = array();

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      print "error domains results = " . $results . "<br />\n";
    }

    foreach ($this->get_list_bliss()->get_list() as $domain) {
      array_push($array_of_domains, $domain->get_domain_obj()->get_tli());
    }

    return $array_of_domains;
  }

  // method
  public function get_domains_list_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      print "error domains results = " . $results . "<br />\n";
    }

    foreach ($this->get_list_bliss()->get_list() as $domain) {
      $markup .= $domain->get_tli_with_natural_link() . "<br />";
    }

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // todo clean up extra coe
  // webpages
  //$markup .= "  <td class=\"mid\">\n";
  //include_once("webpages.php");
  //$webpage_obj = new Webpages($this->get_given_config());
  //$user_obj = $this->get_user_obj();
  //$count_webpages = $webpage_obj->get_count_webpages($domain->get_tli(), $user_obj);
  //$url = $this->url("webpages/domain/" . $domain->get_tli());
  //$markup .= "<a href=\"" . $url . "\">" . $count_webpages . " for " . $domain->get_tli() . "</a>\n";
  //$markup .= "  </td>\n";
  
  // webpages
  //$markup .= "  <td class=\"mid\">\n";
  //include_once("webpages.php");
  //$webpage_obj = new Webpages($this->get_given_config());
  //$user_obj = $this->get_user_obj();
  //$count_webpages = $webpage_obj->get_count_webpages($domain->get_tli(), $user_obj);
  //$url = $this->url("webpages/domain/" . $domain->get_tli());
  //$markup .= "<a href=\"" . $url . "\">" . $count_webpages . " for " . $domain->get_tli() . "</a>\n";
  //$markup .= "  </td>\n";
  
  // tenperday
  //$markup .= "  <td class=\"mid\">\n";
  //$markup .= "    " . $domain->get_tenperday() . "\n";
  //$markup .= "  </td>\n";

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Machines") {
      $markup .= "debug domains: ignore this direction, use other direction<br />\n";
    } else {
      $markup .= "debug domains: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_given_project_id($given_type, $given_project_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //$markup .= "debug domains: " . get_class($this) . " energy_flows given_project_id = " . $given_project_id . "<br />\n";

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error domains results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        $num++;
        if ($given_type == "details") {
          // create a string
          $markup .= $num . " " . $domain->get_tli_with_link() . " registrars " . $domain->get_energy_flows_dollars("registrars") . "<br />";
          $markup .= $num . " " . $domain->get_tli_with_link() . " hostings " . $domain->get_energy_flows_dollars("hostings") . "<br />";
        } else if ($given_type == "dollars") {
          // get dollars and add to total_dollars
          $total_dollars += $domain->get_energy_flows_dollars("registrars");
          $total_dollars += $domain->get_energy_flows_dollars("hostings");
        } else {
          // todo fix this so that it goes through error system
          $markup .= "error domains: type is not known<br />\n";
        }
      }
      // set total
      if ($given_type == "dollars") {
        $markup .= $total_dollars;
      }
    } else {
      // todo fix this so that it goes through error system
      $markup .= "domains: no domains found<br />";
    }

    return $markup;
  }

  // method
  public function get_energy_flows_dollars($given_account_name) {
    $markup = "";

    // preprocess
    include_once("scene_elements.php");
    $scene_element_obj = new SceneElements($this->get_given_config());
    $user_obj = $this->get_user_obj();
    // todo note that this is hard-coded and should be a db issue
    $class_name = get_class($this);
    $id = $this->get_id();
    $count_of_matching_scene_elements = $scene_element_obj->get_count_of_matching_scene_elements($class_name, $id, $user_obj);
    
    // debug
    //$markup .= "debug domains: count_of_matching_scene_elements = " . $count_of_matching_scene_elements . "\n";

    if ($given_account_name == "registrars") {
      // note extract XML from registrars field
      $field_string = $this->get_registrar();

      // get first line
      // extract string based upon substring
      $account_rule_string = substr($field_string, 0, strpos($field_string, "account-rule"));
      //$markup .= $account_rule_string;

      // narrow
      // extract string based upon substring
      $amount_string = substr($account_rule_string, 0, strpos($account_rule_string, "-dollars-per-year"));

      // note convert to monthly units
      $monthly_amount_string = $this->convert_to_monthly($amount_string);

      // note convert to portion of scene_elements
      $markup .= $this->convert_to_portion_of_scene_elements($monthly_amount_string, $count_of_matching_scene_elements);

    } else if ($given_account_name == "hostings") {
      // note extract XML from hosting field
      $field_string = $this->get_hosting();

      // get first line
      // extract string based upon substring
      $account_rule_string = substr($field_string, 0, strpos($field_string, "account-rule"));
      //$markup .= $account_rule_string;

      // narrow
      // extract string based upon substring
      // try for both, scooping what is there
      $amount_string = substr($account_rule_string, 0, strpos($account_rule_string, "-dollars-per-month"));
      $markup .= $amount_string;
      $amount_string = substr($account_rule_string, 0, strpos($account_rule_string, "-dollars-per-year"));
      $monthly_amount_string = $this->convert_to_monthly($amount_string);
      $markup .= $this->convert_to_portion_of_scene_elements($monthly_amount_string, $count_of_matching_scene_elements);

    } else {
      $markup .= "error domains: given_account_type not known.<br />\n";
    }

    return $markup;
  }

  // method
  private function get_status_special_first_only() {

    // define special
    $special_first_character = "*";

    // search for special
    $status = $this->get_status();
    if (strstr($status, $special_first_character)) {
      return $special_first_character;
    } else if (strstr($status, "demo") ||
               strstr($status, "sub") ||
               strstr($status, "dev") ||
               strstr($status, "care") ||
               strstr($status, "expired") ||
               strstr($status, "retired") ||
               strstr($status, "client") ||
               strstr($status, "mirror")) {
      return $status;
    }

    return "";
  }


  // method
  public function get_webpages_given_maxonomy_id($given_maxonomy_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_maxonomy_id($given_maxonomy_id);
    $this->set_user_obj($given_user_obj);

    $this->determine_type();

    $results = $this->prepare_query();

    if ($results) {
      // todo fix this so that it goes through error system
      print "error domains results = " . $results . "<br />\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $num = 0;
      $total_dollars = 0;
      $markup .= "<table>\n";
      $markup .= "<tr>\n";
      $markup .= "<td>\n";
      $markup .= "maxonomy\n";
      $markup .= "</td>\n"; 
      $markup .= "<td>\n";
      $markup .= "status\n";
      $markup .= "</td>\n";
      $markup .= "<td>\n";
      $markup .= "domain tli\n";
      $markup .= "</td>\n";
      $markup .= "<td>\n";
      $markup .= "webpage\n";
      $markup .= "</td>\n";
      $markup .= "<td>\n";
      $markup .= "webpage path\n";
      $markup .= "</td>\n";
      $markup .= "</tr>\n";
      $zoneline_count = 0;
      foreach ($this->get_list_bliss()->get_list() as $domain) {
        $num++;
        $markup .= "<tr>\n";
        $markup .= "<td>\n";
        $markup .= $domain->get_maxonomy_obj()->get_name_with_link() . "\n";
        $markup .= "</td>\n"; 
        if ($domain->get_status() == "zoneline") {
          $zoneline_count++;
          $markup .= "  <td style=\"padding: 2px; text-align: center; background-color: " . $domain->get_status_background_color() . "\">\n";
        } else {
          $markup .= "    <td style=\"padding: 2px; text-align: center; background-color: " . $background_color . ";\">\n";
        }
        $markup .= $domain->get_status() . "\n";
        $markup .= "</td>\n"; 
        $markup .= "<td>\n";
        $markup .= $domain->get_tli() . "\n";
        $markup .= "</td>\n";
        $markup .= "<td>\n";
        $markup .= $domain->get_webpage_obj()->get_name_with_link() . "\n";
        $markup .= "</td>\n";
        $markup .= "<td>\n";
        $markup .= $domain->get_webpage_obj()->get_path() . "\n";
        $markup .= "</td>\n";
        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
      $markup .= "<p>status zoneline count = " . $zoneline_count . "</p>\n";
    } else {
      // todo fix this so that it goes through error system
      $markup .= "domains: no domains found<br />";
    }

    return $markup;
  }

  // method
  public function get_td_background_color($given_data = "") {
    $markup = "";

    $color_z = "#DC779A";
    $color_1b = "#84BE6A"; # green

    if ($given_data) {
      return $color_1b;
    }
    return $color_z;
  }

}
