<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/shifts.php

include_once("lib/standard.php");

class Shifts extends Standard {

  // given
  private $given_profile_id;
  private $given_project_id;
  private $given_process_id;
  private $given_ticket_id;

  // given_profile_id
  public function set_given_profile_id($var) {
    $this->given_profile_id = $var;
  }
  public function get_given_profile_id() {
    return $this->given_profile_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_process_id
  public function set_given_process_id($var) {
    $this->given_process_id = $var;
  }
  public function get_given_process_id() {
    return $this->given_process_id;
  }

  // given_ticket_id
  public function set_given_ticket_id($var) {
    $this->given_ticket_id = $var;
  }
  public function get_given_ticket_id() {
    return $this->given_ticket_id;
  }

  // attributes
  private $id;
  private $sort;
  private $start;
  private $timeout;
  private $process_profile_obj;
  private $day_of_week;
  private $icalendar_obj;  // derived

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  public function get_id_with_link() {
    $markup = "";
    $url = $this->url("shifts/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\">" . $this->get_id() . "</a>";
    return $markup;
  }

  // sort
  public function set_sort($var) {
    $this->sort = $var;
  }
  public function get_sort() {
    return $this->sort;
  }

  // start
  public function set_start($var) {
    $this->start = $var;
  }
  public function get_start() {
    return $this->start;
  }

  // start_as_ampm
  public function get_start_as_ampm() {
    return $this->get_ampm($this->get_start());
  }
  public function get_ampm($given_military_time) {
    if ($given_military_time == "6") {
      return "6 am";
    } else if ($given_military_time == "9") {
      return "9 am";
    } else if ($given_military_time == "13") {
      return "1 pm";
    } else if ($given_military_time == "15") {
      return "3 pm";
    } else if ($given_military_time == "16") {
      return "4 pm";
    } else if ($given_military_time == "18") {
      return "6 pm";
    } else if ($given_military_time == "21") {
      return "9 pm";
    }
    return "[error: " . $given_military_time . "?]";
  }

  // timeout
  public function set_timeout($var) {
    $this->timeout = $var;
  }
  public function get_timeout() {
    return $this->timeout;
  }

  public function get_timeout_as_ampm() {
    return $this->get_ampm($this->get_timeout());
  }

  // process_profile_obj
  //public function get_process_profile_obj() {
  //  if (! isset($this->process_profile_obj)) {
  //    include_once("process_profiles.php");
  //    $this->process_profile_obj = new ProcessProfiles($this->get_given_config());
  //  }
  //  return $this->process_profile_obj;
  //}

  // day_of_week
  public function set_day_of_week($var) {
    $this->day_of_week = $var;
  }
  public function get_day_of_week() {
    return $this->day_of_week;
  }

  public function get_day_of_week_as_day_name() {
    if ($this->day_of_week == 0) {
      return "Sunday";
    } else if ($this->day_of_week == 1) {
      return "Monday";
    } else if ($this->day_of_week == 2) {
      return "Tuesday";
    } else if ($this->day_of_week == 3) {
      return "Wednesday";
    } else if ($this->day_of_week == 4) {
      return "Thursday";
    } else if ($this->day_of_week == 5) {
      return "Friday";
    } else if ($this->day_of_week == 6) {
      return "Saturday";
    }
    return "Error in shifts: day of week integer is not known<br />\n";
  }

  // method
  public function get_time_span() {
    // define globals
    $time_span = "?";
    $time_start = 0;
    $time_timeout = 0;

    // check value for errors in start
    $start_string_length = strlen($this->get_start());
    if ($start_string_length < 3 || $start_string_length > 4) {
      $time_span = "error shifts: start is wrong length";
      return $time_span;
    }

    // check value for errors in timeout
    $timeout_string_length = strlen($this->get_timeout());
    if ($timeout_string_length < 3 || $timeout_string_length > 4) {
      $time_span = "error shifts: timeout is wrong length";
      return $time_span;
    }

    // search for "am" or "pm" and add hours for start
    $start_substring = substr($this->get_start(), -2);
    if ($start_substring == "am") {
      $hours_start = str_replace("am", "", $this->get_start());
      $time_start = 0 + $hours_start;
    } else if ($start_substring == "pm") {
      $hours_start = str_replace("pm", "", $this->get_start());
      $time_start = 12 + $hours_start;
    }
    // search for "am" or "pm" and add hours for timeout
    $timeout_substring = substr($this->get_timeout(), -2);
    if ($timeout_substring == "am") {
      $hours_timeout = str_replace("am", "", $this->get_timeout());
      $time_timeout = 0 + $hours_timeout;
    } else if ($timeout_substring == "pm") {
      $hours_timeout = str_replace("pm", "", $this->get_timeout());
      $time_timeout = 12 + $hours_timeout;
    }

    // calculate difference (the time_span)
    if ($time_start && $time_timeout) {
      $time_span = $time_timeout - $time_start;
    }

    return $time_span;
  }

  // img_url
  // no stter
  public function get_img_url() {
    return "http://mudia.com/dash/_images/smw.gif";
  }

  // icalendar_obj
  public function get_icalendar_obj() {
    if (! isset($this->icalendar_obj)) {
      include_once("utility/icalendars.php");
      $this->icalendar_obj = new Icalendars($this->get_given_config());
    }
    return $this->icalendar_obj;
  }

  // method
  private function make_shift() {
    $obj = new Shifts($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_profile_id()) {
      $this->set_type("get_by_profile_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_process_id()) {
      $this->set_type("get_by_process_id");

    } else if ($this->get_given_ticket_id()) {
      $this->set_type("get_by_ticket_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // debug
    //print "debug shifts: type (starting prepare_query()) = " . $this->get_type() . "<br />\n";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      // note: distinct below
      // todo clean up very old sql by sending to code archive dustbucket
      //$sql = "SELECT shifts.*, process_profiles.scope FROM shifts, process_profiles WHERE shifts.process_profile_id = process_profiles.id AND shifts.id = " . $this->get_given_id() . ";";
      // sql
      $sql = "SELECT shifts.* FROM shifts WHERE shifts.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug shifts sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      // todo clean up old error-filled sql
      //$sql = "SELECT shifts.*, process_profiles.scope FROM shifts, process_profiles WHERE shifts.process_profile_id = process_profiles.id ORDER BY shifts.day_of_week, CAST(shifts.start AS INTEGER), shifts.start, shifts.timeout;";
      // new sql
      $sql = "SELECT shifts.* FROM shifts ORDER BY shifts.day_of_week, CAST(shifts.start AS INTEGER), shifts.start, shifts.timeout;";

    } else if ($this->get_type() == "get_by_profile_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT shifts.*, process_profiles.scope FROM shifts, process_profiles WHERE shifts.process_profile_id = process_profiles.id AND shifts.process_profile_id = " . $this->get_given_profile_id() . " ORDER BY shifts.day_of_week, CAST(shifts.start AS INTEGER), shifts.start, shifts.timeout;";

    } else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT DISTINCT ON (shifts.id) shifts.*, process_profiles.scope FROM shifts, process_profiles, processes, business_plan_texts, projects  WHERE shifts.process_profile_id = process_profiles.id AND process_profiles.id = processes.profile_id AND processes.business_plan_text_id = business_plan_texts.id AND business_plan_texts.project_id = projects.id AND projects.id = " . $this->get_given_project_id() . " ORDER BY shifts.id;";
       // old: ORDER BY shifts.day_of_week, CAST(shifts.start AS INTEGER), shifts.start, shifts.timeout

      // debug
      //print "debug shifts sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_process_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT shifts.* FROM shifts, tickets WHERE shifts.id = tickets.shift_id AND tickets.process_id = " . $this->get_given_process_id() . " ORDER BY shifts.day_of_week, CAST(shifts.start AS INTEGER), shifts.start, shifts.timeout;";

    } else if ($this->get_type() == "get_by_ticket_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT shifts.* FROM shifts, tickets WHERE shifts.id = tickets.shift_id AND tickets.id = " . $this->get_given_ticket_id() . " ORDER BY shifts.day_of_week, CAST(shifts.start AS INTEGER), shifts.start, shifts.timeout;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_profile_id" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_process_id" ||
        $this->get_type() == "get_by_ticket_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_shift();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_sort(pg_result($results, $lt, 1));
        $obj->set_start(pg_result($results, $lt, 2));
        $obj->set_timeout(pg_result($results, $lt, 3));
        $obj->set_day_of_week(pg_result($results, $lt, 4));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu() {
    $markup = "";

    // todo note the this object may not know it's project
    //$markup .= "<div class=\"subsubmenu\">\n";
    //$url = $this->url("projects/" . $this->get_given_project_id());
    //$markup .= "<a href=\"" . $url . "\">Back to project page</a><br />\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";
    // only authenticated users
    //if ($this->get_user_obj()) {
    //  if ($this->get_user_obj()->uid) {
        // todo fix the following
        //$markup .= "<div class=\"subsubmenu-user\">\n";
        //$markup .= "<strong>This icalendar_data was created by</strong> " . $this->get_user_obj()->name . "<br />\n";
        //$markup .= "</div>\n";
        //$markup .= "<div class=\"subsubmenu-user\">\n";
        //$markup .= "<strong>These " . $this->get_class_print_name() . " were declared by</strong> " . $this->get_user_obj()->name . "<br />\n";
    //    $markup .= "</div>\n";
    //  }
    //}

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // debug
    //print "debug shifts: given_profile_id = " . $this->get_given_profile_id(). "<br/>\n";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    day_of_week\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 40px;\">\n";
    $markup .= "    start\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 40px;\">\n";
    $markup .= "    timeout\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    hours\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    tickets\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    $num_assigned = 0;
    $total_hours = 0;
    $total_hours_assigned = 0;
    foreach ($this->get_list_bliss()->get_list() as $shift) {
      $num++;

      if ($shift->get_day_of_week() == "0" ||
          $shift->get_day_of_week() == "2" ||
          $shift->get_day_of_week() == "4" ||
          $shift->get_day_of_week() == "6") {
        $markup .= "<tr style=\"background-color: #EFCC33;\">\n";
      } else {
        $markup .= "<tr>\n";
      }

      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $shift->get_sort_cell() . "\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $shift->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $shift->get_day_of_week_as_day_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $shift->get_start_as_ampm() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $shift->get_timeout_as_ampm() . "\n";
      $markup .= "  </td>\n";

      // hours
      $markup .= "  <td>\n";
      $hours = $shift->get_timeout() - $shift->get_start();
      $total_hours += $hours;
      $markup .= "    " . $hours . "\n";
      $markup .= "  </td>\n";

      // tickets
      include_once("tickets.php");
      $ticket_obj = new Tickets($this->get_given_config());
      $user_obj = $this->get_user_obj();
      // todo fix what is on hold below
      $tickets_data = "";
      //$tickets_data = $ticket_obj->get_tickets_given_shift_id($shift->get_id(), $user_obj);
      if ($tickets_data) {
        $num_assigned++;
        $total_hours_assigned += $hours;
        $markup .= "  <td>\n";
      } else {
        $markup .= "  <td style=\"background-color: #EFEFCC;\">";
      }
      $markup .= "    " . $tickets_data . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    $markup .= "<br />\n";

    $markup .= "<p>total shifts = " . $num. "</p>\n";
    $markup .= "<p>total shifts assigned = " . $num_assigned . "</p>\n";
    $markup .= "<br />\n";

    $markup .= "<p>total hours = " . $total_hours . "</p>\n";
    $markup .= "<p>total hours assigned = " . $total_hours_assigned . "</p>\n";
    $markup .= "<br />\n";

    if (0) {
      $markup .= "<h3>icalendar data</h3>\n";
      $markup .= "<pre>\n";
      foreach ($this->get_list_bliss()->get_list() as $shift) {
          // if the icalendar_data does not exist, then output error messag
          //if ($shift->get_icalendar_data()) {
          //  // old (this pulls the data from a field)
          //  //$markup .= $shift->get_icalendar_data() . "<br />\n";
          //  // new (this crafts the data from scratch)
            $markup .= $shift->craft_vcalendar();
            $markup .= "\n";
          //} else {
          //  $markup .= "<p style=\"error\">Shifts: shift <em>id</em> = " . $shift->get_id() . " has no <em>icalendar_data</em>.</p>\n";
          //}

      }
      $markup .= "</pre>\n";
    }

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $shift) {
      // single
      $markup .= "<h3>icalendar data</h3>\n";
      $markup .= "<p>project = " . $shift->get_project_obj()->get_name_with_link() . "</p>\n";
    }

    return $markup;
  }

  // method
  public function get_shifts_given_project_id_with_html_entities($given_project_id, $given_user_obj) {
    $markup = $this->get_shifts_given_project_id($given_project_id, $given_user_obj);
    return htmlentities($markup);
  }

  // method
  public function get_shifts_given_project_id($given_project_id, $given_user_obj, $display = "") {
    $markup = "";

    // set
    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    if ($display == "count-only") {
      $markup .= $this->get_list_bliss()->get_count();

    } else {
      // return the array
      $markup .= "<div style=\"font-size: 60%;\">\n";
      foreach ($this->get_list_bliss()->get_list() as $shift) {

        // option 1 of 2
        // the func. in the next line is on hold
        //$markup .= $shift->get_icalendar_obj()->get_whole($shift->get_icalendar_data());

        // option 2 of 2
        //$shift->get_icalendar_obj()->extract_variables($shift->get_icalendar_data());
        $markup .= $shift->get_icalendar_obj()->get_dtstart_simple();
        $markup .= "-";
        $markup .= $shift->get_icalendar_obj()->get_dtend_simple(); 
        $markup .= ":";
        $markup .= $shift->get_icalendar_obj()->get_summary() . "<br />\n";
      }
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_name_with_link_given_id($given_id = "", $given_user_obj) {
    $markup = "";

    if (! $given_id) {
      // do not know what to get, so return a null string
      return "";
    }

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug shifts given_id = " . $this->get_given_id() . "<br />\n";

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $markup .= $obj->get_name_with_link();
    }

    return $markup;
  }

  // method
  public function craft_vcalendar() {
    $markup = "";

    $markup .= "BEGIN:VCALENDAR\n";
    $markup .= "BEGIN:VEVENT\n";
    $markup .= "SUMMARY:" . $this->get_name() . " (id is " . $this->get_id() . ")\n";
    $markup .= "DTSTART:" . $this->get_start() . "\n";
    $markup .= "DTEND:" . $this->get_end() . "\n";
    //$markup .= "ATTACH:https://permaculturewebsites.org/projects/53\n";
    $markup .= "END:VEVENT\n";
    $markup .= "END:VCALENDAR\n";

    return $markup;
  }

  // method
  public function get_build_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no shifts were found.</p>\n";;
      return $markup;
    }

    $markup .= "<h3 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px;\">shifts</h3>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $shift) {

      $user_obj = $this->get_user_obj();
      $shift->set_user_obj($user_obj);

      $markup .= "  <p>\n";
      $markup .= "    " . $shift->get_id() . "<br >\n";
      $markup .= "  </p>\n";

      $markup .= "  <p>\n";
      $markup .= "    " . $shift->get_name_with_hidden_link() . "\n";
      $markup .= "  <p>\n";


    }

    return $markup;
  }

  // method
  public function get_count_given_profile_id($given_profile_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_profile_id($given_profile_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error shifts " . $markup . "</p>\n";
    }

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_shifts_given_profile_id($given_profile_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_profile_id($given_profile_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error shifts " . $markup . "</p>\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<table border=\"1\">\n";

      // rows
      $time_span_count = 0;
      foreach ($this->get_list_bliss()->get_list() as $shift) {
        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_day_of_week_as_day_name() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_start_as_ampm() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_timeout_as_ampm() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $time_span = $shift->get_timeout() - $shift->get_start();
        $markup .= "    " . $time_span . "\n";
        $time_span_count += $time_span;
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
      // totals
      //$markup .= "    <p><em>total shift hours:</em> " . $time_span_count . "</p>\n";

    } else {
      $markup .= "<div style=\"background-color: red;\">";
      $markup .= "  <p>No shift.</p>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_shift_given_process_id($given_process_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_process_id($given_process_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error shifts " . $markup . "</p>\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<table border=\"1\">\n";

      // rows
      $time_span_count = 0;
      foreach ($this->get_list_bliss()->get_list() as $shift) {
        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_day_of_week_as_day_name() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_start_as_ampm() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_timeout_as_ampm() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $time_span = $shift->get_timeout() - $shift->get_start();
        $markup .= "    " . $time_span . "\n";
        $time_span_count += $time_span;
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
      // totals
      //$markup .= "    <em>total shift hours:</em> " . $time_span_count . "\n";

    } else {
      $markup .= "<div style=\"background-color: red;\">";
      $markup .= "  <p>No shift.</p>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method
  public function get_shifts_list_given_ticket_id($given_ticket_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_ticket_id($given_ticket_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    // check for errors
    if ($markup) {
      return "<p class=\"error\">error shifts " . $markup . "</p>\n";
    }

    if ($this->get_list_bliss()->get_count() > 0) {

      $markup .= "<table border=\"1\">\n";

      // rows
      $time_span_count = 0;
      foreach ($this->get_list_bliss()->get_list() as $shift) {
        $markup .= "<tr>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_id_with_link() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_day_of_week_as_day_name() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_start_as_ampm() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $shift->get_timeout_as_ampm() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $time_span = $shift->get_timeout() - $shift->get_start();
        $markup .= "    " . $time_span . "\n";
        $time_span_count += $time_span;
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";

    } else {
      $markup .= "<div style=\"background-color: red;\">";
      $markup .= "  <p>No shift.</p>\n";
      $markup .= "</div>\n";
    }

    return $markup;
  }

}
