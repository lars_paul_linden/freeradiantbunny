<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.2 2015-01-05

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/kernel_theory_sets.php

include_once("lib/scrubber.php");

class KernelTheorySets extends Scrubber {

  // attributes
  private $id;
  private $name;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // method
  private function make_kernel_theory_set() {
    $obj = new KernelTheorySets($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = ";";

      // debug
      //print "debug scene_elements sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = ";";
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_scene_element();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . ": does not know the type.");
    }
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // view
    //$markup .= $this->output_view();
    //$markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

}
