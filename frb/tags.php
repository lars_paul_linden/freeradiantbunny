<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-04
// version 1.6 2016-05-06
// version 1.6 2017-11-29

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/tags.php

include_once("lib/standard.php");

class Tags extends Standard {

  // given
  private $given_hyperlink_id;
  private $given_permaculture_topic_id;
  
  // given_hyperlink_id
  public function set_given_hyperlink_id($var) {
    //$error_message = $this->get_validation_obj()->validate_id($var, "hyperlink_id");
    //if ($error_message) {
    //  $this->set_error_message($error_message);
    //} else {
      $this->given_hyperlink_id = $var;
    //}
  }
  public function get_given_hyperlink_id() {
    return $this->given_hyperlink_id;
  }

  // given_permaculture_topic
  public function set_given_permaculture_topic_id($var) {
    $this->given_permaculture_topic_id = $var;
  }
  public function get_given_permaculture_topic_id() {
    return $this->given_permaculture_topic_id;
  }

  // attribute
  private $permaculture_topic_obj;

  // permaculture_topic_obj
  public function get_permaculture_topic_obj() {
    if (! isset($this->permaculture_topic_obj)) {
      include_once("permaculture_topics.php");
      $this->permaculture_topic_obj = new PermacultureTopics($this->get_given_config());
    }
    return $this->permaculture_topic_obj;
  }

  // method
  private function make_tag() {
    $obj = new Tags($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_hyperlink_id()) {
      $this->set_type("get_by_hyperlink_id");

    } else if ($this->get_given_permaculture_topic_id()) {
      $this->set_type("get_by_permaculture_topic_id");

    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug tags type = " . $this->get_type();
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT tags.* FROM tags WHERE tags.id = " . $this->get_given_id() . ";";

      // debug
      //print "debug tags sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      $sql = "SELECT tags.* FROM tags ORDER BY tags.sort DESC, tags.name;";

    } else if ($this->get_type() == "get_by_hyperlink_id") {
      // wow!
      $sql = "select tags.* FROM tags, tag_elements WHERE tags.id = tag_elements.tag_id AND tag_elements.class_primary_key_string = '" . $this->get_given_hyperlink_id() . "'  AND tag_elements.class_name_string = 'hyperlinks' ORDER BY tags.id;";

    } else if ($this->get_type() == "get_by_permaculture_topic_id") {
      // wow!
      $sql = "select tags.* FROM tags WHERE tags.permaculture_topic_id = " . $this->get_given_permaculture_topic_id() . " ORDER BY tags.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_hyperlink_id" ||
        $this->get_type() == "get_by_permaculture_topic_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $tag = $this->make_tag();
        $tag->set_id(pg_result($results, $lt, 0));
        $tag->set_name(pg_result($results, $lt, 1));
        $tag->set_description(pg_result($results, $lt, 2));
        $tag->set_sort(pg_result($results, $lt, 3));
        $tag->set_status(pg_result($results, $lt, 4));
        $tag->set_img_url(pg_result($results, $lt, 5));
        $tag->get_permaculture_topic_obj()->set_id(pg_result($results, $lt, 6));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $tag = $this->make_tag();
        $tag->set_id(pg_result($results, $lt, 0));
        $tag->set_name(pg_result($results, $lt, 1));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type = " . $this->get_type());
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // todo archive the below and it must be something to do with
    // todo the moving of the code from lff to pws
    //if (strstr($_SERVER['HTTP_HOST'], "permaculturewebsites.org")) {
    //  $markup .= $this->output_tag_table();
    //  return $markup;
    //}

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\" style=\"width: 160px;\">\n";
    //$markup .= "    status\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 160px;\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    img_url\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 300px;\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    <a href=\"http://permaculturewebsites.org/pws/webpages.php\">webpages</a> ";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    permacaulture topics\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    <a href=\"http://permaculturewebsites.org/pws/hyperlinks.php\">hyperlinks</a> ";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    <a href=\"http://permaculturewebsites.org/pws/books.php\">books</a> ";
    //$markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $tag) {
      $markup .= "<tr>\n";

      // num
      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // status
      //$markup .= "  <td>\n";
      //$markup .= "    " . $tag->get_status() . "\n";
      //$markup .= "  </td>\n";

      // sort
      $markup .= $tag->get_sort_cell();

      // id with link
      $markup .= "  <td>\n";
      $markup .= "    " . $tag->get_id_with_link() . "\n";
      $markup .= "  </td>\n";

      // name with link
      $markup .= "  <td>\n";
      $markup .= "    " . $tag->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // webpages
      include_once("webpage_tags.php");
      $webpage_tag_obj = new WebpageTags($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $webpage_tag_string = $webpage_tag_obj->get_webpages_given_tag_id($tag->get_id(), $user_obj);
      if ($webpage_tag_string) {
        $markup .= "  <td class=\"specify-width\">\n";
        $markup .= $webpage_tag_string . "\n";
      } else {
        $markup .= "  <td class=\"specify-width\" style=\"background-color: #3466AA;\">\n";
      }
      $markup .= "  </td>\n";

      // permaculture_topic_id
      //$permaculture_topic_id = $tag->get_permaculture_topic_obj()->get_id();
      //if ($permaculture_topic_id) {
      //  $markup .= "  <td class=\"specify-width\">\n";
      //  include_once("permaculture_topics.php");
      //  $permaculture_topic_obj = new PermacultureTopics($this->get_given_config());
      //  $user_obj = $this->get_user_obj();
      //  $markup .= $permaculture_topic_obj->get_name_with_link_given_id($user_obj, $permaculture_topic_id);
      //  $markup .= "\n";
      //} else {
      //  $markup .= "  <td class=\"specify-width\" style=\"background-color: #3466AA;\">\n";
      //}
      //$markup .= "  </td>\n";

      // hyperlinks
      //$markup .= "  <td>\n";
      //$markup .= "    " . "\n";
      //$markup .= "  </td>\n";

      // books
      //$markup .= "  <td>\n";
      //$markup .= "    " . "\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_tag_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    tag\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $tag) {
      $markup .= "<tr>\n";

      $markup .= "  <td>\n";
      $num++;
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <!-- id = " . $tag->get_id() . " -->\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $tag->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $tag->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    $markup .= "<div class=\"subsubmenu\">\n";

    // note: visit a friend
    // tags
    if ($this->get_type() == "get_by_id") {
      $url = $this->url("tags"); 
      $markup .= "<em>See all</em>: <a href=\"" . $url . "\">Tags</a><br />\n";
    }
    // permaculture_topics
    $url = $this->url("permaculture_topics"); 
    $markup .= "<em>See also</em>: <a href=\"" . $url . "\">Permaculture Topics</a><br />\n";
    // searches
    $url = $this->url("searches"); 
    $markup .= "<em>See also</em>: <a href=\"" . $url . "\">Searches</a><br />\n";

    if ($this->get_type() != "get_all") {

      //$tag_obj = new Tags();
      //$tag_obj->determine_type(); 
      //$tag_obj->prepare_query();
      //foreach ($tag_obj->get_list_bliss()->get_list() as $tag) {
      //  $markup .= "<ul style=\"margin: 10px 0px 0px 0px; padding: 0px 0px 0px 14px;\">\n";
      //  $markup .= "  <li>" . $tag->get_name_with_link() . "</li>\n";
      //  $markup .= "</ul>\n";
      //}
    
    }

    if (isset($this->get_user_obj()->name)) {
      // add
      // todo fix the following so that it works
      //$markup .= $this->get_create_obj()->output_add_link($this);
    }

    $markup .= "</div>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    foreach ($this->get_list_bliss()->get_list() as $tag) {

      // id
      $markup .= "<p>" . $tag->get_id() . "</p>\n";
      // img
      $padding = " 0px 0px 0px 0px";
      $float = "";
      $width = "35";
      $markup .= "    " . $tag->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      // name
      $markup .= "<h2>" . $tag->get_name() . "</h2>\n";
      $markup .= "<p>description = " . $tag->get_description() . "</p>\n";
      $markup .= "<p>sort = " . $tag->get_sort() . "</p>\n";
      $markup .= "<p>status = " . $tag->get_status() . "</p>\n";

      $markup .= "<table class=\"plants\">\n";
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    permaculture topic\n";
      $markup .= "  </td>\n";
      // permaculture_topic
      $permaculture_topic_id = $tag->get_permaculture_topic_obj()->get_id();
      if ($permaculture_topic_id) {
        $markup .= "  <td class=\"specify-width\">\n";
        include_once("permaculture_topics.php");
        $permaculture_topic_obj = new PermacultureTopics($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= $permaculture_topic_obj->get_name_with_link_given_id($user_obj, $permaculture_topic_id);
        $markup .= "\n";
      } else {
        $markup .= "  <td class=\"specify-width\" style=\"background-color: red;\">\n";
      }
      $markup .= "  </td>\n";
      $markup .= "  </table>\n";

    }

    return $markup;
  }

  // method
  public function get_project_id_given_tag_id($given_tag_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_tag_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_project_obj()->get_id();
      // debug
      //print "debug tags projects = " . $project_id . "<br />\n"; 
      $markup .= $project_id;
    }

    return $markup;
  }

  // method
  public function insert() {
    $sql = "INSERT INTO tags (name, project_id) VALUES ('" . $this->get_name() . "', '" . $this->get_project_obj()->get_id() .");";
    print "sql = " . $sql . "<br />\n";
    $database_name = "plantdot_soiltoil";
    $error_message = $this->get_db_dash()->new_insert($database_name, $sql);
    return $error_message;
  }

  // method
  public function get_tags_given_hyperlink_id_as_list($given_hyperlink_id, $given_user_obj) {
    $markup = "";
 
    // set
    $this->set_given_hyperlink_id($given_hyperlink_id);
    $this->set_user_obj($given_user_obj);

    // debug
    //print "debug tags: given_hyperlink_id = " . $given_hyperlink_id . "<br />\n";

    $this->determine_type();
    $markup .= $this->prepare_query();
    // check for errors
    if ($markup) {
      return $this->get_db_dash()->output_error("Error " . get_class($this) . ": prepare_query() has a message = " . $markup . ".");
    }

    $last_num = count($this->get_list_bliss()->get_list());
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $tag) {
      $num++;
      $url = $this->url("tags/", $tag->get_id()); 
      $markup .= "<a href=\"" . $url . "\">";
      $markup .= $tag->get_name();
      $markup .= "</a>\n";
      if ($num < $last_num) {
        $markup .= ", ";
      }
    }

    return $markup;
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT tags.id, tags.name FROM tags WHERE tags.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug tags: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "Tags ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function get_tag_sym_given_id($given_id, $given_user_obj) {
    $markup = "";

    // todo there is a chance that this method is not used anymore

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $tag) {
      $url = $this->url("tags/" . $tag->get_id());
      $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $tag->get_name() . "</a>\n";
    }

    return $markup;
  }

  // method
  public function get_tag_given_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $tag) {
      $url = $this->url("tags/" . $tag->get_id());
      $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $tag->get_name() . "</a>\n";
    }

    return $markup;
  }

  // method
  public function get_list_given_permaculture_topic_id($given_permaculture_topic_id) {
    $markup = "";

    // set
    $this->set_given_permaculture_topic_id($given_permaculture_topic_id);
    $this->determine_type();
    $markup .= $this->prepare_query();

    // check for errors
    if ($markup) {
      return $markup;
    }

    if ($this->get_list_bliss()->get_count() > 0) {
      $last_flag = $this->get_list_bliss()->get_count();
      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $tag) {
        $num++;
        $markup .= "    " . $tag->get_name_with_link() . "</a>";
        if ($num < $last_flag) {
          $markup .= "; ";
        }
      }
    }

    return $markup;
  }

  // todo clean this up from category class
  public function output_add_category_form() {
    $markup .= "<h3>Add Category</h3>\n";
    $markup .= "<form method=\"post\" action=\"linkmaster.php\">\n";
    $markup .= "  <input type=\"hidden\" name=\"command\" value=\"insert_category\" />\n";
    $markup .= "<table border=\"0\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    code\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    <input type=\"text\" name=\"code\" size=\"90\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    <input type=\"text\" name=\"name\" size=\"90\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    <input type=\"text\" name=\"description\" size=\"90\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "    parent\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    include_once("categories.php");
    $categories_obj = new Categories($this->get_given_config());
    $categories_obj->output_form_select("parent_id");
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "<tr>\n";
    $markup .= "  <td>\n";
    $markup .= "   &nbsp;\n";
    $markup .= "  </td>\n";
    $markup .= "  <td align=\"right\">\n";
    $markup .= "    <input type=\"submit\" name=\"submit\" value=\"Add\" />\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    $markup .= "</table>\n";
    $markup .= "</form>\n";

    return $markup;
  }

  // todo clean this up from category class
  // method
  public function output_form_select($parameter_name) {
    $markup = "";

    // select control for a form

    // database //
    $this->set_type("get_categories_list");
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }

    // output
    $markup .= "<select name=\"" . $parameter_name . "\">\n";
    foreach ($this->get_categories_array() as $category_obj) {
      if ($parameter_name == "category_id" && $category_obj->get_name() == "__root__") {
        continue;
      }
      $markup .= "  <option value=\"" . $category_obj->get_id() . "\">" . $category_obj->get_name() . "</option>\n";
    }
    $markup .= "</select>\n";

    return $markup;
  }

}
