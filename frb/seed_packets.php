<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.1 2014-08-18
// version 1.2 2015-01-04
// version 1.6 2016-04-09

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/seed_packets.php

include_once("lib/standard.php");

class SeedPackets extends Standard {

  // given
  private $given_variety_id;
  private $given_supplier_id;
  private $given_project_id;
  private $given_plant_list_id;
  private $given_filter;
 
  // given_variety_id
  public function set_given_variety_id($var) {
    $this->given_variety_id = $var;
  }
  public function get_given_variety_id() {
    return $this->given_variety_id;
  }

  // given_supplier_id
  public function set_given_supplier_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "supplier_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_supplier_id = $var;
    }
  }
  public function get_given_supplier_id() {
    return $this->given_supplier_id;
  }

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_filter
  public function set_given_filter($var) {
    $this->given_filter = $var;
  }
  public function get_given_filter() {
    return $this->given_filter;
  }

  // attributes
  private $variety_obj;
  private $supplier_obj;
  private $packed_for_year;
  private $contents;
  private $sow_instructions;
  private $days_to_maturity;
  private $days_to_germination;
  private $notes;
  private $product_details;

  // variety_obj
  public function get_variety_obj() {
    if (! isset($this->variety_obj)) {
      include_once("varieties.php");
      $this->variety_obj = new Varieties($this->get_given_config());
    }
    return $this->variety_obj;
  }

  // supplier_obj
  public function get_supplier_obj() {
    if (! isset($this->supplier_obj)) {
      include_once("suppliers.php");
      $this->supplier_obj = new Suppliers($this->get_given_config());
    }
    return $this->supplier_obj;
  }

  // packed_for_year
  public function set_packed_for_year($var) {
    $this->packed_for_year = $var;
  }
  public function get_packed_for_year() {
    return $this->packed_for_year;
  }

  // contents
  public function set_contents($var) {
    $this->contents = $var;
  }
  public function get_contents() {
    return $this->contents;
  }

  // sow_instructions
  public function set_sow_instructions($var) {
    $this->sow_instructions = $var;
  }
  public function get_sow_instructions() {
    return $this->sow_instructions;
  }

  // days_to_maturity
  public function set_days_to_maturity($var) {
    $this->days_to_maturity = $var;
  }
  public function get_days_to_maturity() {
    return $this->days_to_maturity;
  }

  // days_to_germination
  public function set_days_to_germination($var) {
    $this->days_to_germination = $var;
  }
  public function get_days_to_germination() {
    return $this->days_to_germination;
  }

  // notes
  public function set_notes($var) {
    $this->notes = $var;
  }
  public function get_notes() {
    return $this->notes;
  }

  // product_details
  public function set_product_details($var) {
    $this->product_details = $var;
  }
  public function get_product_details() {
    return $this->product_details;
  }

  // method
  private function make_seed_packet() {
    $obj = new SeedPackets($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_variety_id()) {
      $this->set_type("get_by_variety_id");

    } else if ($this->get_given_plant_list_id()) {
      $this->set_type("get_by_plant_list_id");

    } else {
      // default
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT seed_packets.*, varieties.name, suppliers.name FROM seed_packets, varieties, suppliers WHERE seed_packets.id = " . $this->get_given_id() . " AND seed_packets.variety_id = varieties.id AND seed_packets.supplier_id = suppliers.id;";

    } else if ($this->get_type() == "get_seed_packets_given_project_season_id") {
      $sql = "SELECT seed_packets.id, seed_packets.variety_id, seed_packets.supplier_id, seed_packets.packed_for_year, seed_packets.contents, seed_packets.organic_flag, seed_packets.sow_instructions, plants.common_name, varieties.name, suppliers.name, seed_packets.days_to_maturity FROM seed_packets, plants, varieties, suppliers WHERE seed_packets.variety_id = varieties.id AND seed_packets.supplier_id = suppliers.id ORDER BY plants.common_name;";

    } else if ($this->get_type() == "get_by_variety_id") {
      $sql = "SELECT seed_packets.id, seed_packets.variety_id, seed_packets.supplier_id, seed_packets.packed_for_year, seed_packets.contents, seed_packets.sow_instructions, seed_packets.days_to_maturity, seed_packets.days_to_germination, seed_packets.notes, varieties.plant_id, plants.common_name, varieties.name, suppliers.name, seed_packets.product_details FROM seed_packets, plants, varieties, suppliers WHERE seed_packets.supplier_id = suppliers.id AND seed_packets.variety_id = varieties.id AND varieties.plant_id = plants.id AND seed_packets.variety_id = " . $this->get_given_variety_id() . " ORDER BY plants.common_name, varieties.name;";

    } else if ($this->get_type() == "get_by_plant_list_id") {
      $sql = "SELECT seed_packets.* FROM seed_packets, plant_histories, plant_list_plants WHERE plant_list_plants.plant_list_id = " . $this->get_given_plant_list_id() . " AND plant_list_plants.id = plant_histories.plant_list_plant_id AND plant_histories.seed_packet_id = seed_packets.id ORDER BY seed_packets.id;";

    } else if ($this->get_type() == "get_all") {

      // this old select only returned a partial list
      //$sql = "SELECT seed_packets.id, seed_packets.variety_id, seed_packets.supplier_id, seed_packets.packed_for_year, seed_packets.contents, seed_packets.sow_instructions, seed_packets.days_to_maturity, seed_packets.days_to_germination, seed_packets.notes, varieties.plant_id, plants.common_name, varieties.name, suppliers.name FROM seed_packets, plants, varieties, suppliers, projects, project_suppliers WHERE seed_packets.supplier_id = suppliers.id AND seed_packets.variety_id = varieties.id AND varieties.plant_id = plants.id AND suppliers.id = project_suppliers.supplier_id AND project_suppliers.project_id = projects.id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY plants.common_name, varieties.name;";
      $sql = "SELECT seed_packets.* FROM seed_packets ORDER BY seed_packets.sort DESC, seed_packets.supplier_id, seed_packets.id";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_seed_packet();
        $user_obj = $this->get_user_obj();
        $obj->set_user_obj($user_obj);
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_variety_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_packed_for_year(pg_result($results, $lt, 3));
        $obj->set_contents(pg_result($results, $lt, 4));
        $obj->set_sow_instructions(pg_result($results, $lt, 5));
        $obj->set_days_to_maturity(pg_result($results, $lt, 6));
        $obj->set_days_to_germination(pg_result($results, $lt, 7));
        $obj->set_notes(pg_result($results, $lt, 8));
        $obj->set_product_details(pg_result($results, $lt, 9));
        $obj->set_sort(pg_result($results, $lt, 10));
        $obj->set_status(pg_result($results, $lt, 11));
        $obj->get_variety_obj()->set_name(pg_result($results, $lt, 12));
        $obj->get_supplier_obj()->set_name(pg_result($results, $lt, 13));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_seed_packet();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_variety_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_packed_for_year(pg_result($results, $lt, 3));
        $obj->set_contents(pg_result($results, $lt, 4));
        $obj->set_sow_instructions(pg_result($results, $lt, 5));
        $obj->set_days_to_maturity(pg_result($results, $lt, 6));
        $obj->set_days_to_germination(pg_result($results, $lt, 7));
        $obj->set_notes(pg_result($results, $lt, 8));
        $obj->set_product_details(pg_result($results, $lt, 9));
        $obj->set_sort(pg_result($results, $lt, 10));
        $obj->set_status(pg_result($results, $lt, 11));
        $obj->set_name(pg_result($results, $lt, 12));
        $obj->set_img_url(pg_result($results, $lt, 13));
        $obj->set_description(pg_result($results, $lt, 14));
      }
    } else if ($this->get_type() == "get_by_variety_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_seed_packet();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_variety_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_packed_for_year(pg_result($results, $lt, 3));
        $obj->set_contents(pg_result($results, $lt, 4));
        $obj->set_sow_instructions(pg_result($results, $lt, 5));
        $obj->set_days_to_maturity(pg_result($results, $lt, 6));
        $obj->set_days_to_germination(pg_result($results, $lt, 7));
        $obj->set_notes(pg_result($results, $lt, 8));
        $obj->get_variety_obj()->get_plant_obj()->set_id(pg_result($results, $lt, 9));
        $obj->get_variety_obj()->get_plant_obj()->set_common_name(pg_result($results, $lt, 10));
        $obj->get_variety_obj()->set_name(pg_result($results, $lt, 11));
        $obj->get_supplier_obj()->set_name(pg_result($results, $lt, 12));
        $obj->set_product_details(pg_result($results, $lt, 13));
      }
    } else if ($this->get_type() == "get_by_plant_list_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_seed_packet();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_variety_obj()->set_id(pg_result($results, $lt, 1));
        $obj->get_supplier_obj()->set_id(pg_result($results, $lt, 2));
        $obj->set_packed_for_year(pg_result($results, $lt, 3));
        $obj->set_contents(pg_result($results, $lt, 4));
        $obj->set_sow_instructions(pg_result($results, $lt, 5));
        $obj->set_days_to_maturity(pg_result($results, $lt, 6));
        $obj->set_days_to_germination(pg_result($results, $lt, 7));
        $obj->set_notes(pg_result($results, $lt, 8));
        $obj->set_product_details(pg_result($results, $lt, 9));
        $obj->set_sort(pg_result($results, $lt, 10));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu_get_all() {
    $markup = "";

    //if ($this->get_type() == "get_all") {
    //  $markup .= "<div class=\"subsubmenu\">\n";
    //  $url = $this->url("varieties");
    //  $markup .= "  See Also: <a href=\"" . $url . "\">Varieties</a><br />\n";
    //  $markup .= "</div>\n";
    //}

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // this was producing an error message because uid was not known
    //$markup .= $this->output_user_info_message();

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // filter menu before table
    if ($this->get_given_filter()) {
      $markup .= "";
      $markup .= "<p>filter: ";
      $markup .= $this->get_given_filter();
      $url = $this->url("seed_packets");
      $markup .= " (<a href=\"" . $url . "\">reset to all</a>)";
      $markup .= "</p>\n";
    }

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\" style=\"width: 80px;\">\n";
    //$markup .= "    status\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 170px;\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    invoice\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 102px;\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    notes\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    variety\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    contents\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    packed_for_year\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant_histories count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // show crop plan plants error report
    $plant_id_list = array();

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $seed_packet) {

      // deal with filter parameter
      if ($this->get_given_filter()) {
        // filter does not output
        // debug
        //print "debug seed_packets given_filter = " . $this->get_given_filter() . "<br />\n";
        //print "debug seed_packets invoice = " . $seed_packet->get_invoice() . "<br />\n";
        if ($this->get_given_filter() == $seed_packet->get_invoice()) {
          // debug
          //print "debug seed_packets given_filter = " . $this->get_given_filter() . "<br />\n";
        } else {
          continue;
        }
      }


      // todo clean up the following old comments and code below
      // show crop plan plants error report
      // populate a list that is used after the table is output
      //$plant_id = $seed_packet->get_variety_obj()->get_plant_obj()->get_id();
      //array_push($plant_id_list, $plant_id);

      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";;
      $markup .= "  </td>\n";

      // status
      //$markup .= "  <td>\n";
      //$markup .= "    " . $seed_packet->get_status();
      //$markup .= "  </td>\n";

      // suppliers
      $markup .= "  <td>\n";
      include_once("suppliers.php");
      $supplier_obj = new Suppliers($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $supplier_obj->get_name_with_link_given_id($seed_packet->get_supplier_obj()->get_id(), $user_obj);
      $markup .= "  </td>\n";

      // invoice
      $markup .= "  <td>\n";
      $markup .= "    " . $seed_packet->get_invoice_with_link();
      $markup .= "  </td>\n";

      // sort
      $markup .= "  <td>\n";
      $markup .= "    " . $seed_packet->get_sort();
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $url = $this->url("seed_packets/" . $seed_packet->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $seed_packet->get_id() . "</a>\n";
      $markup .= "  </td>\n";

      // notes
      $markup .= "  <td>\n";
      $markup .= "    " . $seed_packet->get_notes();
      $markup .= "  </td>\n";

      // variety
      $markup .= "  <td>\n";
      include_once("varieties.php");
      $variety_obj = new Varieties($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $variety_obj->get_name_with_link_given_id($seed_packet->get_variety_obj()->get_id(), $user_obj);
      $markup .= "  </td>\n";

      // plants
      // use variety instance to find plant instance
      $plant_id = $variety_obj->get_plant_id_given_id($seed_packet->get_variety_obj()->get_id(), $user_obj);
      include_once("plants.php");
      $plant_obj = new Plants($this->get_given_config());
      $markup .= "  <td>\n";
      $markup .= $plant_obj->get_name_with_link_given_id($plant_id, $user_obj);
      $markup .= "  </td>\n";

      // contents
      //$markup .= "  <td>\n";
      //$markup .= "    " . $seed_packet->get_contents() . "\n";
      //$markup .= "  </td>\n";

      // packed_for_year
      //$markup .= "  <td>\n";
      //$markup .= "    " . $seed_packet->get_packed_for_year() . "\n";
      //$markup .= "  </td>\n";

      // plant_histories count
      $markup .= "  <td>\n";
      include_once("plant_histories.php");
      $plant_history_obj = new PlantHistories($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $markup .= $plant_history_obj->get_count_with_link_given_seed_packet_id($seed_packet->get_id(), $user_obj);
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // filter menu after table
    if ($this->get_given_filter()) {
      $markup .= "";
      $markup .= "<p>filter: ";
      $markup .= $this->get_given_filter();
      $url = $this->url("seed_packets");
      $markup .= " (<a href=\"" . $url . "\">reset to all</a>)";
      $markup .= "</p>\n";
    }

    // todo this is very old and krufty down-right-ancient code
    // todo so good luck with that brother
    // show crop plan plants error report
    //include_once("class_list_control.php");
    //$list_control_obj = new ListControl($this->get_given_config());
    //$given_crop_plan_id = $this->get_given_crop_plan_id();
    //$user_obj = $this->get_user_obj();
    //$markup .= $list_control_obj->get_missing($plant_id_list, $given_crop_plan_id, $user_obj);

    return $markup;
  }

  // method
  protected function output_single_nonstandard() {
    $markup = "";

    $markup .= "<h2>id " . $this->get_id() . "</h2>\n";

    // todo this is an odd artifact, so figure out what I can learn from it
    // there is no list like the aggregation version, so
    $seed_packet = $this;

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_supplier_obj()->get_name_with_link() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // variety
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    variety\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    include_once("varieties.php");
    $variety_obj = new Varieties($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $variety_obj->get_name_with_link_given_id($seed_packet->get_variety_obj()->get_id(), $user_obj);
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // plant
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    // variety has a plant_id that has a common_name
    $markup .= "  <td>\n";
    $plant_id = $variety_obj->get_plant_id_given_id($seed_packet->get_variety_obj()->get_id(), $user_obj);
    include_once("plants.php");
    $plant_obj = new Plants($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $markup .= $plant_obj->get_name_with_link_given_id($plant_id, $user_obj);
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // notes
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    notes\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_notes() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // packed_for_year
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    packed_for_year\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_packed_for_year() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // contents
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    contents\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_contents() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // product_details
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    product_details\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_product_details() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // sow_instructions
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sow instructions\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_sow_instructions() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // days_to_maturity
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    days to maturity\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_days_to_maturity() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // days_to_germination
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    days to germination\n";
    $markup .= "  </td>\n";
    $markup .= "  <td>\n";
    $markup .= "    " . $seed_packet->get_days_to_germination() . "\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_supplier_and_year() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    id\n";
    //$markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    packed for year\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $markup .= "<tr>\n";

    //$markup .= "  <td>\n";
    //$markup .= "    " . $this->get_id() . "\n";
    //$markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    " . $this->get_supplier_obj()->get_name_with_link() . "\n";
    $markup .= "  </td>\n";

    $markup .= "  <td>\n";
    $markup .= "    " . $this->get_packed_for_year() . "\n";
    $markup .= "  </td>\n";

    $markup .= "</tr>\n";
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_mini_table() {
    $markup = "";

    // self-populate the given_id
    $given_id = $this->get_id();
    $this->set_given_id($given_id);

    // get data
    $this->determine_type();
    $this->prepare_query();

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    variety\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    foreach ($this->get_list_bliss()->get_list() as $seed_packet) {
      $markup .= "<tr>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    " . $seed_packet->get_id() . "\n";
      $markup .= "  </td>\n";

      // variety
      $markup .= "  <td>\n";
      $markup .= "    " . $seed_packet->get_variety_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // supplier
      $markup .= "  <td>\n";
      $markup .= "    " . $seed_packet->get_supplier_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_sidecar($given_variety_id) {
    $markup = "";

    // set
    $this->set_given_variety_id($given_variety_id);
    $this->determine_type();
    $this->prepare_query();
    $markup .= "<h2>Seed Packets</h2>\n";
    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function get_project_id_given_seed_packet_id($given_seed_packet_id, $given_user_obj) {
    $markup = "";

    // todo this is not working

    // set
    $this->set_given_id($given_seed_packet_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_supplier_obj()->get_project_obj()->get_id();

      // debug
      //print "debug seed_packets:  projects = " . $project_id . "<br />\n"; 

      $markup .= $project_id;
    }

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

  // method
  public function get_project_id() {
    $markup = "";

    // todo need to be able to remove this
    // todo but for now seed_packets needs this for scrubber
    // todo because scrubber must think seed_packets is standard
    // todo and do all standards have a project in some way?

    return $markup;
  }

  // method
  public function output_seed_packet_count_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }

    $count = $this->get_list_bliss()->get_count();

    $markup .= "<p>" . $count . "</p>";

    return $markup;
  }

  // method
  public function output_info_given_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // all because this returns a list rather than a string
    if ($markup) {
      print $markup;
    }


    foreach ($this->get_list_bliss()->get_list() as $seed_packet) {
      $url = $this->url("seed_packets/" . $seed_packet->get_id());
      $markup .= "<a href=\"" . $url . "\">" . $seed_packet->get_id() . "</a>";
      $markup .= " " . $seed_packet->get_variety_obj()->get_name() . " ";
      $markup .= " " . $seed_packet->get_contents();
    }

    return $markup;
  }

  // method
  private function get_invoice() {
    $markup = "";

    $subject = $this->get_description();
    // extract using class attribute
    $pattern = "/<p class=.invoice.>(.*)<\/p>/";
    // debug
    //print "debug seed_packets subject = " . $subject . "<br />\n";
    if (preg_match($pattern, $subject, $matches)) {
      // debug
      //print "debug seed_packets invoice found " . $matches[1] . "<br />\n";
      // returns invoice (a string that refers to an actual invoice)
      $markup .= $matches[1];
    } else {
      // no match, so no invoice
      return "";
    }

    return $markup;
  }

  // method
  private function get_invoice_with_link() {
    $markup = "";

    $invoice_string = $this->get_invoice();
    $url = $this->url("seed_packets" . "?" . "filter=" . $invoice_string);
    $markup .= "<a href=\"" . $url . "\">" . $invoice_string . "</a>";

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // sort
    $parameter_a = new Parameter();
    $parameter_a->set_name("filter");
    $parameter_a->set_validator_function_string("filter");
    array_push($parameters, $parameter_a);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          // 1
          if ($parameter->get_name() == "filter") {
            $this->set_given_filter($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

}
