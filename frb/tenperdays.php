<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.2 2015-01-04
// version 1.6 2017-11-07

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/tenperdays.php

include_once("lib/scrubber.php");

class Tenperdays extends Scrubber {

  // attribute
  private $id;
  private $date;
  private $count;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // date
  public function set_date($var) {
    $this->date = $var;
  }
  public function get_date() {
    return $this->date;
  }

  // count
  public function set_count($var) {
    $this->count = $var;
  }
  public function get_count() {
    return $this->count;
  }

  // method
  private function make_tenperday() {
    $obj = new Tenperdays($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT tenperdays.* FROM tenperdays WHERE id = " . $self->get_id(). ";";
     
    } else if ($this->get_type() == "get_all") {
      $order_by = "ORDER BY date, id";
      $sql = "SELECT tenperdays.* FROM tenperdays " . $order_by . ";";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_tenperday();
        $obj->set_date(pg_result($results, $lt, 0));
        $obj->set_id(pg_result($results, $lt, 1));
        $obj->set_count(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_subsubmenu() {
  }


  // method
  protected function output_aggregate() {
    $markup = "";

    //$markup .= $this->output_view();
    $markup .= $this->output_table();

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function get_webpages_string() {
    $markup = "";

    include_once("webpages.php");
    $webpage_obj = new Webpages($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $webpages_string = $webpage_obj->get_webpages_sorted_today($user_obj);

    return $webpages_string;
  }

  // method
  public function get_webpage_string_count() {
    $markup = "";

    $webpages_string = $this->get_webpages_string();
    # counting rows by counting break elements
    $line_count = substr_count($webpages_string, "<br />");

    return $line_count;
  }

  // method
  public function get_webpage_count_given_tli($given_tli, $given_user_obj) {
    $markup = "";

    include_once("webpages.php");
    $webpage_obj = new Webpages($this->get_given_config());
    $user_obj = $this->get_user_obj();
    // note a different function than above, this is the given_tli stuff hppening
    $webpages_string = $webpage_obj->get_webpages_sorted_today_given_tli($given_tli, $user_obj);
    # counting rows bu counting break elements
    $line_count = substr_count($webpages_string, "<br />");

    return $line_count;
  }

  // method
  private function output_table() {
    $markup = "";

    // webpages
    $markup .= $this->get_webpages_string();
    $markup .= "<br />\n";

    // count (today)
    $webpage_string_count = $this->get_webpage_string_count();
    if ($webpage_string_count >= 10) {
       $markup .= "<p style=\"font-size: 130%;\"><strong>tenperday</strong> count is currently " . $webpage_string_count . "</p>\n";
    } else {
       $markup .= "<div style=\"color: yellow; background-color: red; padding: 4px 2px 4px 12px;\"><p>FAIL! count is " . $webpage_string_count . "</p></div>\n";
    }    
    $markup .= "<br />\n";

    // save count (today)
    // every time the table is accessed, there could be a new count, so save to database
    $this->store_today_count_in_table($webpage_string_count);

    // display tenperday history
    $num= 0;
    $sum = 0;
    $markup .= "<table cellpadding=\"4\" border=\"1\">";
    $markup .= "<tr>";
    $markup .= "<td>";
    $markup .= "  #";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  id";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  date";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  gap";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  count";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  sum";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  avg";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  2nd";
    $markup .= "</td>";
    $markup .= "<td>";
    $markup .= "  3rd";
    $markup .= "</td>";
    $markup .= "</tr>";
    // note save to array and then reverse output of row so today date is at top
    $next_date_no_y;
    $next_average;
    $next_second_derivative;
    $rows = array();

    foreach ($this->get_list_bliss()->get_list() as $tenperday) {

      $num++;
      $row = "";
      // todo figure out if this belongs here
      // id
      $row .= "<tr>\n";

      // num
      $row .= "<td>";
      $row .= $num;
      $row .= "</td>\n";

      // id
      $row .= "<td>";
      $row .= $tenperday->get_id();
      $row .= "</td>\n";

      // date
      include_once("lib/dates.php");
      $date_obj = new Dates();
      $date_no_y = $date_obj->remove_y($tenperday->get_date());
      if ($date_obj->is_today($date_no_y)) {
        // today, so colorize
        $row .= "<td style=\"background-color: #A6D785;\">\n";
      } else {
        $row .= "<td>\n";
      }
      $row .= $tenperday->get_date();
      $row .= "</td>\n";

      // gap
      if (! isset($next_date_no_y)) {
        // prep for the next iteration
	// todo I know I know everthing is reversed
        $row .= "<td style=\"background-color: blue;\">";
      } else {
        if ($tenperday->is_gap_one_day($date_no_y, $next_date_no_y)) {
          $row .= "<td style=\"text-align: center;\">";
        } else {
          $row .= "<td style=\"text-align: center; background-color: red;\">";
        }
        $row .= $tenperday->is_gap_one_day($date_no_y, $next_date_no_y);
      }
      $next_date_no_y = $date_no_y;
      $row .= "</td>";

      if ($tenperday->get_count() >= 10) {
        $row .= "<td style=\"text-align: right; background-color: yellow;\">";
      } else {
        $row .= "<td style=\"text-align: right;\">";
      }
      $row .= $tenperday->get_count();
      $sum += $tenperday->get_count(); 
      $row .= "</td>";
      $row    .= "<td style=\"text-align: right;\">";
      $row    .= " " . $sum . "\n";
      $markup .= "</td>\n";

      // average
      $average = round($sum / $num, 1);
      if ($average >= 10) {
        $row    .= "<td style=\"text-align: right; background-color: yellow;\">";
      } else {
        $row .= "<td style=\"text-align: right;\">";
      }
      $row .= " " . number_format($average, 1) . "\n";
      $row .= "</td>\n";

      // second derivative
      // is the average going up or down?
      $second_derivative = "";
      if (! isset($next_average)) {
        // prep for the next iteration
	// todo I know I know everthing is reversed
        $row .= "<td style=\"text-align: right; background-color: blue;\">";
      } else {
        $second_derivative = $average - $next_average;
        $third_derivative = $second_derivative - $next_second_derivative;
        if ($second_derivative >= 0) {
          $row .= "<td style=\"text-align: right; background-color: green;\">";
        } else {
          $row .= "<td style=\"text-align: right; background-color: red;\">";
        }
        $row .= " " . number_format($second_derivative, 1) . "\n";
      }
      $next_average = $average;
      $row .= "</td>\n";

      // third derivative
      // is the second derivative going up or down?
      if (! isset($next_second_derivative)) {
        // prep for the next iteration
	// todo I know I know everthing is reversed
        $row .= "<td style=\"text-align: right; background-color: blue;\">";
      } else {
        $third_derivative = $second_derivative - $next_second_derivative;
        if ($third_derivative >= 0) {
          $row .= "<td style=\"text-align: right; background-color: green;\">";
        } else {
          $row .= "<td style=\"text-align: right; background-color: red;\">";
        }
        $row .= " " . number_format($third_derivative, 1) . "\n";
      }
      $next_second_derivative = $second_derivative;
      $row .= "</td>\n";

      $row .= "</tr>\n";
      array_push($rows, $row);
    }
    $rows = array_reverse($rows);
    foreach ($rows as $row) {
      $markup .=  $row;
    }
    $markup .= "</table>";
    $markup .= "<br />";

    $markup .= "code: be aware of missing dates in table<br />";

    return $markup;
  }

  // method
  private function store_today_count_in_table($count) {
    $markup = "";

    // todo this is a soft tenperday (only rows not days considered)
    // todo build a hard tenperday (missing dates are added with 0 count)

    // set up
    include_once("lib/dates.php");
    $date_obj = new Dates();

    // is_today
    $found_today_row_in_database = ""; 
    foreach ($this->get_list_bliss()->get_list() as $tenperday) {
      // raise is_today flag? 
      $date_no_y = $date_obj->remove_y($tenperday->get_date());
      if ($date_obj->is_today($date_no_y)) {
        $found_today_row_in_database = "1";
      }
    }

    // store it
    include_once("lib/timekeeper.php");
    $timekeeper_obj = new Timekeeper();
    $now_date = $timekeeper_obj->get_now_date();

    // set for this class
    $table_name = "tenperdays";

    // create sql statement for update or insert
    $sql = "";
    if ($found_today_row_in_database) { 
      // update sql
      $sql = "UPDATE " . $table_name . " set count = '" . $count . "' WHERE date = 'Y " . $now_date . "';";

    } else {
      // insert sql
      $sql = "INSERT INTO " . $table_name . " (date, count) VALUES ('Y " . $now_date . "',  '" . $count . "');";

    }

    // change database
    $markup .= $this->get_db_dash()->new_update($this, $sql);

    // refresh
    // get data from database again
    $this->get_list_bliss()->empty_list();
    // load data from database
    $this->determine_type();
    $markup = $this->prepare_query();

    return $markup;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // view
    $parameter_a = new Parameter();
    $parameter_a->set_name("view");
    $parameter_a->set_validation_type_as_view();
    array_push($parameters, $parameter_a);

    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
        }
      }
    }
    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    // todo this was turned off because all was silly, really only about webpages
    $markup .= "<p>view: ";
    $views = array("webpages", "all");
    $last_pos = count($views) - 1;
    foreach ($views as $view) {
      $markup .= $this->get_menu_item($view, "view");
      if ($view != $views[$last_pos]) {
        $markup .= " | ";
      }
    }
    $markup .= "</p>\n";

    return $markup;
  }

  // method
  protected function output_preface() {
    $markup = "";

    return $markup;
  }

  // method
  protected function is_gap_one_day($date_1, $date_2) {
    $markup = "";

    // count the days between the given dates
    // this is probably here because of scrubber object wanting it
    $timestamp = strtotime($date_2 . " + 1 day");
    $next_date = date('Y-m-d', $timestamp);
    if ($next_date == $date_1) {
      return 1;
    }

    return 0;
  }

  // method
  protected function get_given_project_id() {
    $markup = "";

    return $markup;
  }

}
