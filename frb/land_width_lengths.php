<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-08
// version 1.1 2014-08-20
// version 1.2 2015-01-11

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/land_width_lengths.php

class LandWidthLengths {

  // attributes
  private $id;
  private $width;
  private $length;
  private $land_obj;
  private $soil_area_obj;

  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // width
  public function set_width($var) {
    $this->width = $var;
  }
  public function get_width() {
    return $this->width;
  }

  // length
  public function set_length($var) {
    $this->length = $var;
  }
  public function get_length() {
    return $this->length;
  }

  // land_obj
  public function get_land_obj() {
    if (! isset($this->land_obj)) {
      include_once("lands.php");
      $this->land_obj = new Lands($this->get_given_config());
    }
    return $this->land_obj;
  }

  // soil_area_obj
  public function get_soil_area_obj() {
    if (! isset($this->soil_area_obj)) {
      include_once("soil_areas.php");
      $this->soil_area_obj = new SoilAreas($this->get_given_config());
    }
    return $this->soil_area_obj;
  }

  // method
  private function make_land_width_length() {
    $obj = new LandWidthLengths($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);    
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else {
      // default
      $this->set_type("get_all");
    }

  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // get sql statement
    if ($this->get_type() == "get_by_id") {
      //$sql = 
      
    } else if ($this->get_type() == "get_all") {
      //$sql = 

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }

    return $markup;
  }
  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_land_width();
        $obj->set_id(pg_result($results, $lt, 0));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= "not yet coded";

    return $markup;
  }

}
