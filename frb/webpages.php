<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-11
// version 1.5 2015-10-16
// version 1.6 2016-05-05

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/webpages.php

include_once("lib/socation.php");

class Webpages extends Socation {

  // given
  private $given_submit;
  private $given_project_id;
  private $given_domain_tli;
  private $given_description_id;
  private $given_maxonomy_id;
  private $given_webpage_id;
  private $given_sort;
  private $given_view = "online"; // default

  // given_submit;
  public function set_given_submit($var) {
    $this->given_submit = $var;
  }
  public function get_given_submit() {
    return $this->given_submit;
  }

  // given_project_id;
  public function set_given_project_id($given_project_id) {
    $this->given_project_id = $given_project_id;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_domain_tli
  public function set_given_domain_tli($var) {
    $this->given_domain_tli = $var;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_description_id
  public function set_given_description_id($var) {
    $this->given_description_id = $var;
  }
  public function get_given_description_id() {
    return $this->given_description_id;
  }

  // given_maxonomy_id
  public function set_given_maxonomy_id($var) {
    $this->given_maxonomy_id = $var;
  }
  public function get_given_maxonomy_id() {
    return $this->given_maxonomy_id;
  }

  // given_webpage_id
  public function set_given_webpage_id($var) {
    $this->given_webpage_id = $var;
  }
  public function get_given_webpage_id() {
    return $this->given_webpage_id;
  }

  // given_sort;
  public function set_given_sort($var) {
    $this->given_sort = $var;
  }
  public function get_given_sort() {
    return $this->given_sort;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // attribute
  private $parameters;
  private $domain_tli;
  private $path;
  #private $webpage_maxonomy_obj;

  // parameters
  public function set_parameters($var) {
    $this->parameters = $var;
  }
  public function get_parameters() {
    return $this->parameters;
  }

  // domain_tli
  public function set_domain_tli($var) {
    $this->domain_tli = $var;
  }
  public function get_domain_tli() {
    return $this->domain_tli;
  }

  // path
  public function set_path($var) {
    $this->path = $var;
  }
  public function get_path() {
    return $this->path;
  }

  // is this needed below
  // $this->domain_obj->set_user_obj($this->get_user_obj());

  // attibutes
  private $domain_obj;

  // domain_obj
  public function get_domain_obj() {
    if (! isset($this->domain_obj)) {
      include_once("domains.php");
      $this->domain_obj = new domains($this->get_given_config());
      $this->domain_obj->set_tli($this->get_domain_tli());;
    }
    return $this->domain_obj;
  }

  // method
  //public function get_webpage_maxonomy_obj() {
  //  if (! isset($this->webpage_maxonomy_obj)) {
  //    include_once("webpage_maxonomies.php");
  //    $this->webpage_maxonomy_obj = new WebpageMaxonomies($this->get_given_config());
  //  }
  //  return $this->webpage_maxonomy_obj;
  //}

  public function get_path_with_link() {
    $markup = "";
    include_once("lib/factory.php");
    $factory_obj = new Factory($this->get_given_config());
    $url = $this->url($factory_obj->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_path()) {
      $markup .= $this->get_path();
    } else {
      if ($this->get_id()) {
        $markup .= $this->get_id();
      } else {
        // note: is this too dangerous or just a good fallback plan
        if ($this->get_given_id()) {
          $markup .= $this->get_given_id();
        } else {
          // no data was found, so return null string
          print "error class_standard: no id or given_id<br />\n";
          print "error class_standard: subclass = " . get_class($this) . "<br />\n";
          return "";
        }
      }
    }
    $markup .= "</a>";
    return $markup;
  }

  public function get_path_with_link_to_path() {
    $markup = "";
    // todo consider that this dev option is in the wrong place
    // todo but for now it gets the job done
    //if ($this->get_status() == "dev") {
    //  $url = "http://" . $this->get_domain_name_dev() . $this->get_path();
    //} else {
    $url = "http";
    if ($this->get_domain_obj()->get_ssl_cert()) {
      $url .= "s";
    }
    $url .= "://" . $this->get_domain_obj()->get_domain_name() . $this->get_path();
    if ($this->get_parameters()) {
      $url .= $this->get_parameters();
    }
    $markup .= "    <a href=\"" . $url . "\">" . $url . "</a><br />\n";

    return $markup;
  }

  // todo is this needed below

  public function get_domain_tli_with_link() {
    $markup = "";

    // todo the following line has two other lines like it in this file
    //$url = $this->url("domains/" . $this->get_domain_tli());
    $url = $this->url($this->get_domain_tli());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_domain_tli()) {
      $markup .= $this->get_domain_tli();
    } else {
      print "<p style=\"error\">Error webpages: no manifest tli</p>\n";
      return "";
    }
    $markup .= "</a>";

    return $markup;
  }

  // found_max_workdate
  //public function set_found_max_workdate($var) {
  //  $this->found_max_workdate = $var;
  //}
  //public function get_found_max_workdate() {
  //  return $this->found_max_workdate;
  //}

  //private $serial_number;
  //private $manmax_tests_array = array();
  //private $manifest_obj;
  //private $found_max_workdate;

  // serial_number
  //public function set_serial_number($var) {
  //  $this->serial_number = $var;
  //}
  //public function get_serial_number() {
  //  return $this->serial_number;
  //}

  // method
  //public function get_object_given_serial_number($given_serial_number) {
  //  foreach ($this->get_list_bliss()->get_list() as $manmax) {
  //    if ($given_serial_number == $manmax->get_serial_number()) {
  //      return $manmax;
  //    }
  //  }
  //  return "";
  //}

  // derived
  private $description_count;
  private $manifest_path_count;

  // description_count
  public function set_description_count($var) {
    $this->description_count = $var;
  }
  public function get_description_count() {
    return $this->description_count;
  }

  // manifest_path_count
  public function set_manifest_path_count($var) {
    $this->manifest_path_count = $var;
  }
  public function get_manifest_path_count() {
    return $this->manifest_path_count;
  }

  // method
  private function make_webpage() {
    $obj = new Webpages($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_sort()) {
      $this->set_type("get_by_sort");

    } else if ($this->get_given_maxonomy_id() && $this->get_given_domain_tli()) {
      // both set
      // todo this was found here
      //$this->set_type("for_rallytally");
      $this->set_type("get_by_maxonomy_id_and_domain_tli");

    } else if ($this->get_given_domain_tli()) {
        $this->set_type("get_by_domain_tli");

      // note: override
      // note: a domain_tli may be accompanied by a description
      if ($this->get_given_description_id()) {
        $this->set_type("get_by_description_id");
      }

    } else if ($this->get_given_project_id()) {
        $this->set_type("get_by_project_id");

    } else if ($this->get_given_maxonomy_id()) {
        $this->set_type("get_by_maxonomy_id");

    } else {
      // default
      $this->set_type("get_all");
    }

    // debug
    //print "debug webpages type = " . $this->get_type() . "<br />\n";
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // todo this might no longer be necessary
    if (! $this->get_user_obj()) {
      // not allowed to define sql permission not granted
      $markup .= $this->get_db_dash()->output_error("Error: User obj is not true. Unable to load data.");
      return $markup;
    }

    // todo this might no longer be necessary
    if (! property_exists($this->get_user_obj(), "name")) {
      // not allowed to define sql permission not granted
      $markup .= $this->get_db_dash()->output_error("Error: User obj has no name. Unable to load data.");
      return $markup;
    }

    // debug
    //print "debug webpages prepare_query() type = " . $this->get_type() . "<br />\n";

    // initialize
    $sql = "";

    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "select webpages.*, domains.domain_name, domains.img_url, domains.ssl_cert FROM webpages, domains WHERE domains.tli = webpages.domain_tli AND webpages.id = " . $this->get_given_id() . " AND domains.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "get_by_sort") {
      // security: only get the rows owned by the user
      $sql = "select webpages.*, domains.domain_name, domains.img_url FROM webpages, domains WHERE domains.tli = webpages.domain_tli AND webpages.sort = '" . $this->get_given_sort() . "' ORDER BY webpages.name, webpages.domain_tli, webpages.id;";

    } else if ($this->get_type() == "get_all") {
      // security: only get the rows owned by the user
      if ($this->get_given_view() == "urls") {
        // view = urls filters for status=zoneline
        // backup with group by
        $sql = "select webpages.*, domains.domain_name, domains.img_url, domains.ssl_cert FROM webpages, domains WHERE domains.tli = webpages.domain_tli AND domains.user_name = '" . $this->get_user_obj()->name . "' AND domains.status = 'zoneline' ORDER BY webpages.sort DESC, webpages.domain_tli, webpages.path;";
       } else {
         // backup with group by
         $sql = "select webpages.*, domains.domain_name, domains.img_url, domains.ssl_cert FROM webpages, domains WHERE domains.tli = webpages.domain_tli AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY webpages.sort DESC, webpages.domain_tli, webpages.path;";
      }

} else if ($this->get_type() == "get_by_project_id") {
      // security: only get the rows owned by the user
      // todo: cannot span 2 databases to get this data
      print "debug webpages cannot span 2 databases<br />\n";
      $sql = "";

    } else if ($this->get_type() == "get_by_maxonomy_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT DISTINCT webpages.*, domains.domain_name, domains.img_url FROM webpages, domains, maxonomies, webpage_maxonomies WHERE domains.tli = webpages.domain_tli AND webpage_maxonomies.webpage_id = webpages.id AND webpage_maxonomies.maxonomy_id = maxonomies.id AND maxonomies.id = " . $this->get_given_maxonomy_id() . " AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY webpages.domain_tli, webpages.name, webpages.id;";

    } else if ($this->get_type() == "get_by_maxonomy_id_and_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "select distinct webpages.id, domain_tli, webpages.description, webpages.name, webpages.sort, webpages.img_url, webpages.status, webpages.path, webpages.parameters, domains.domain_name, domains.img_url FROM webpages, domains, maxonomies, webpage_maxonomies WHERE domains.tli = '" . $this->get_given_domain_tli() . "' AND domains.tli = webpages.domain_tli AND webpage_maxonomies.webpage_id = webpages.id AND webpage_maxonomies.maxonomy_id = maxonomies.id AND maxonomies.id = " . $this->get_given_maxonomy_id() . " AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY webpages.id;";

      // debug
      //print "webpages.php: debug sql = $sql\n";
      
    } else if ($this->get_type() == "get_by_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "select webpages.*, domains.domain_name, domains.img_url FROM webpages, domains WHERE domains.tli = webpages.domain_tli AND domains.tli = '" . $this->get_given_domain_tli() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "' ORDER BY webpages.sort DESC, webpages.path, webpages.name, webpages.id;";

    } else if ($this->get_type() == "get_by_description_id") {
      // todo check it out a left join sql statement
      //$sql = "select webpages.id, webpages.domain_tli, webpages.description, webpages.maxonomy_id, webpages.path, webpages.sort, domains.domain_name, maxonomies.name, maxonomies.img_url, webpages.img_url FROM webpages, domains, maxonomies WHERE webpages.id IN (select webpages.id FROM webpages WHERE domain_tli = '" . $this->get_given_domain_tli() . "' AND webpages.description = '" . $this->get_given_description_id() . "') AND webpages.domain_tli = domains.tli AND webpages.maxonomy_id = maxonomies.id GROUP BY webpages.id, webpages.domain_tli, webpages.description, webpages.maxonomy_id, webpages.path, webpages.sort, domains.domain_name, maxonomies.name, maxonomies.img_url, webpages.img_url ORDER BY webpages.maxonomy_id, webpages.path;";
     $sql = "SELECT webpage_child.*, domains.domain_name, domains.img_url FROM webpages as webpage_parent, webpages as webpage_child, webpage_webpages, domains WHERE webpage_parent.id = '" . $this->get_given_description_id() . "' AND domains.tli = webpage_parent.domain_tli AND domains.tli = '" . $this->get_given_domain_tli() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "' AND webpage_parent.id = webpage_webpages.parent_webpage_id AND webpage_webpages.child_webpage_id = webpage_child.id ORDER BY webpage_child.description, webpage_child.path;";

    } else if ($this->get_type() == "get_count_all_domain_tli") {
      // security: only get the rows owned by the user
      $sql = "select webpages.*, domains.domain_name, domains.img_url FROM webpages, domains WHERE domains.tli = webpages.domain_tli AND webpages.domain_tli = '" . $this->get_given_domain_tli() . "' AND domains.user_name = '" . $this->get_user_obj()->name . "';";

    } else if ($this->get_type() == "for_rallytally") {
      // this gets all of the webpages of a given tli
      $sql = "select description, path FROM webpages, webpage_maxonomies WHERE webpages.domain_tli = '" . $this->get_given_domain_tli() . "' AND webpage_maxonomies.maxonomy_id = " . $this->get_given_maxonomy_id() . " AND webpage_maxonomies.webpage_id = webpages.id ORDER BY webpages.sort DESC;";
      // print "debug sql = ". $sql . "<br />\n";

    } else if ($this->get_type() == "get_row_count") {
      $sql = "SELECT count(*) FROM webpages;";

    } else if ($this->get_type() == "get_oldest_sort_obj") {
      $sql = "SELECT id, name, sort FROM webpages ORDER BY sort, name LIMIT 1;";
    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data. The type = " . $this->get_type() . "<br />\n");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if (! $results) {
      return "";
    }
    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_by_sort" ||
        $this->get_type() == "get_by_description_id" ||
        $this->get_type() == "get_by_project_id"  ||
        $this->get_type() == "get_by_maxonomy_id"  ||
        $this->get_type() == "get_by_maxonomy_id_and_domain_tli"  ||
        $this->get_type() == "get_count_all_domain_tli") {
      $num = 1;
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->get_domain_obj()->set_tli(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_name(pg_result($results, $lt, 3));
        $obj->set_sort(pg_result($results, $lt, 4));
        $obj->set_img_url(pg_result($results, $lt, 5));
        $obj->set_status(pg_result($results, $lt, 6));
        $obj->set_path(pg_result($results, $lt, 7));
        $obj->set_parameters(pg_result($results, $lt, 8));
        $obj->get_domain_obj()->set_domain_name(pg_result($results, $lt, 9));
        $obj->get_domain_obj()->set_img_url(pg_result($results, $lt, 10));
        // todo figure out why I needed this kludge and fix it
        // transfer data
        //$obj->set_given_description_id($this->get_given_description_id());
        if ($this->get_type() == "get_all" ||
            $this->get_type() == "get_by_id") {
          $obj->get_domain_obj()->set_ssl_cert(pg_result($results, $lt, 11));
        }
      }
    } else if ($this->get_type() == "for_rallytally") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage();
        $obj->set_description(pg_result($results, $lt, 0));
        $obj->set_path(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "search") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage();
        $obj->set_domain_tli(pg_result($results, $lt, 0));
        $obj->set_description_count(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_by_given_description_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_domain_tli(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->get_webpage_maxonomy_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_path(pg_result($results, $lt, 4));
        $obj->set_sort(pg_result($results, $lt, 5));
        $obj->set_found_max_workdate(pg_result($results, $lt, 6));
        $obj->get_domain_obj()->set_domain_name(pg_result($results, $lt, 7));
        $obj->get_webpage_maxonomy_obj()->set_name(pg_result($results, $lt, 8));
        $obj->get_webpage_maxonomy_obj()->set_img_url(pg_result($results, $lt, 9));
        $obj->set_img_url(pg_result($results, $lt, 10));
        // debug
        //print "debug: " . $obj->get_found_max_workdate() . "<br />\n";
      }
    } else if ($this->get_type() == "get_row_count") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpage();
        $obj->set_row_count(pg_result($results, $lt, 0));
      }
    } else if ($this->get_type() == "get_oldest_sort_obj") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_webpages();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_oldest_sort_date(pg_result($results, $lt, 2));
      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method
  protected function output_aggregate() {
    $markup = "";

    if ($this->get_given_view() == "urls") {

      $markup .= $this->output_view();

      $markup .= $this->output_urls();

} else if ($this->get_type() == "get_all" ||
               $this->get_type() == "get_by_project_id") {

      $markup .= $this->output_view();

      $markup .= $this->output_table_all();

    } else if ($this->get_type() == "get_by_domain_tli" ||
               $this->get_type() == "get_by_description_id") {

      $markup .= $this->output_table();

    } else if ($this->get_type() == "for_rallytally") {

      // part 3
      // todo
      print "debug output_aggregate part 3<br />\n";

      $markup .= $this->output_table_for_rallytally(); 

    } else if ($this->get_type() == "olddescription") {

      // this is no longer in use

      // part 4
      // todo
      print "debug output_aggregate part 4<br />\n";

      $markup .= "<em>given_description_id = </em><h2 style=\"display: inline;\">" . $this->get_given_description_id() . "</h2>\n";

      // table group
      $markup .= "<table class=\"plants\">\n";

      // heading
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    domain\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    domain_tli\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description count\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $domain) {

        $markup .= "<tr>\n";

        $num++;
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    domain-here\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $domain->get_domain_tli() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td align=\"right\">\n";
        $url = $this->url("webpages") . "?tli=" . $domain->get_domain_tli();
        $markup .= "    <a href=\"" . $url . "\">" . $domain->get_description_count() . "</a>\n";
	        $markup .= "  </td>\n";
      }
      $markup .= "</table>\n";
      $markup .= "<br />\n";

      include_once("domains.php");
      $domain_obj = new Domains($this->get_given_config());
      $domain_obj->set_user_obj($this->get_user_obj());
      $domain_tli_array = $domain_obj->get_list_of_domain_tli();
      $found = array();
      foreach ($domain_tli_array as $domain_tli) {
        //$markup .= $domain_tli . "<br />\n";
        foreach ($this->get_list_bliss()->get_list() as $webpage) {
          //$markup .= $webpage->get_domain_tli() . "<br />\n";
          if ($domain_tli == $webpage->get_domain_tli()) {
            $found[$domain_tli] = "found"; 
          }
        }
      }
      foreach ($domain_tli_array as $domain_tli) {
        if (array_key_exists($domain_tli, $found)) {
          // skip
          //$markup .= "debug: found " . $domain_tli . "<br />\n";
        } else {
          $markup .= "<p class=\"error\">existing <strong>domains.tli</strong> has not <strong>domain_tli</strong>: <em>" . $domain_tli . "</em><br />\n";
        }
      }
    } else if ($this->get_type() == "for_description_group") {

      // part 5
      // todo
      print "debug output_aggregate part 5<br />\n";

      // debug
      //print "debug output_aggregate 4<br />\n";
      $markup .= "<em>given_webpage_id = </em><h2 style=\"display: inline;\">" . $this->get_given_webpage_id() . "</h2>\n";
      // note: add_date should have happened
      // so this should reflect the added date
      $markup .= $this->output_table_of_description_group();

    }      

    return $markup;
  }

  // method
  private function output_table_all() {
    $markup = "";

    // part 1
    // debug
    //print "debug output_aggregate get_all()<br />\n";
      
    $markup .= "<table class=\"plants\">\n";

    // heading
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" style=\"width: 60px;\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    domains\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    path\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    child webpages count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    workdates count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    maxonomies count\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    PK of manifest is:<br />\n";
    //$markup .= "    manifests.tli == domains.tli<br />\n";
    //$markup .= "    description\n";
    //$markup .= "  </td>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage) {

      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td class=\"mid\">\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $webpage->get_sort_cell() . "\n";

      // domains
      $markup .= "  <td class=\"mid\" align=\"left\">\n";
      $padding = "10";
      $float = "";
      $width = "65";
      $markup .= $webpage->get_domain_obj()->get_img_as_img_element_with_link($padding, $float, $width)  . "\n";
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td class=\"mid\" align=\"left\" style=\"padding: 4px 10px 2px 10px;\">\n";
      $markup .= $webpage->get_id_with_link() . "<br />\n";
      $markup .= "  </td>\n";

      // path
      $markup .= "  <td>\n";
      //$markup .= $webpage->get_path_with_link_to_path() . "\n";
      $markup .= "  </td>\n";

      // name
      $markup .= "  <td>\n";
      $markup .= $webpage->get_name() . "\n";
      $markup .= "  </td>\n";

      // child webpages count
      $markup .= "  <td>\n";
      $markup .= "[?]\n";
      $markup .= "  </td>\n";

      // maxonomies count
      $markup .= "  <td>\n";
      $markup .= "[?]\n";
      $markup .= "  </td>\n";

      // more
      // manifest is...
      //$markup .= "  <td class=\"mid\">\n";
      //$markup .= "  <table border=\"0\">\n";
      //$markup .= "  <tr>\n";
      //$markup .= "    <td>\n";
      // for the firstimage
      //$padding = "";
      //$float = "left";
      //$width = "64";
      //$markup .= $webpage->get_manifest_obj()->get_img_url_as_img_element($padding, $float, $width);
      //$markup .= "    </td>\n";
      //$markup .= "    <td>\n";
      // for the second image
      //$padding = "";
      //$float = "";
      //$width = "64";
      //$markup .= $webpage->get_domain_obj()->get_img_url_as_img_element($padding, $float, $width);
      // debug
      //$markup .= "debug domain_tli = " . $webpage->get_domain_tli_with_link() . "<br />";
      //$markup .= "    </td>\n";
      //$markup .= "    <td>\n";
      //$markup .= "      " . $webpage->get_description() . "<br />\n";
      //$markup .= "    </td>\n";
      //$markup .= "  </tr>\n";
      //$markup .= "  </table>\n";
      //$markup .= "  </td>\n";
      //$markup .= "  <td>\n";
      //$padding = "";
      //$float = "";
      //$width = "65";
      //$markup .= "    " . $webpage->get_webpage_maxonomy_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      //$markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

      // todo retire this old code
      // $markup .= "<em>given_id = </em><h2 style=\"display: inline;\">" . $this->get_given_id() . "</h2>\n";

      $markup .= "<table class=\"plants\">\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $webpage) {

        // heading
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\" style=\"width: 150px;\">\n";
        $markup .= "    #\n";
        $markup .= "  </td>\n";
        $num++;
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // id
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    id\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $webpage->get_id() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // domain_tli
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    domain_tli\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        $url = $this->url("domains/" . $webpage->get_domain_obj()->get_tli());
        $markup .= "    <a href=\"" . $url . "\">" . $webpage->get_domain_obj()->get_tli() . "</a>\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // name
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    name\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $webpage->get_name() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // description
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    description\n";
        $markup .= "  </td>\n";
        $markup .= "  <td align=\"left\">\n";
        $markup .= "    " . $webpage->get_description() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // img_url
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    img_url\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        $url = "http://" . $webpage->get_domain_obj()->get_domain_name() . $webpage->get_path();
        $markup .= "<a href=\"" . $url . "\">";
        $width = "45";
        $height= "65";
        $markup .= $webpage->get_img_url_as_img_element("", "", $width, $height);
        $markup .= "</a>\n";;
        $markup .= "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // status
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    status\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\" style=\"background-color: " . $webpage->get_status_background_color() . ";\">\n";
        $markup .= "    " . $webpage->get_status() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // path
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    path\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
    	$markup .= "    " . $webpage->get_path_with_link_to_path() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // sort
        // parameters
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    parameters\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        $markup .= $webpage->get_parameters() . "\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    sort\n";
        $markup .= "  </td>\n";
        $markup .= $webpage->get_sort_cell() . "\n";
        $markup .= "</tr>\n";

        // maxonomy_id
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    maxonomies (associated with this webpage)\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        //$markup .= $webpage->special_out("icon_format_flag");
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

        // tags
        $markup .= "<tr>\n";
        $markup .= "  <td class=\"header\">\n";
        $markup .= "    tags (associated via webpage_tags)\n";
        $markup .= "  </td>\n";
        $markup .= "  <td class=\"mid\">\n";
        include_once("webpage_tags.php");
        $webpage_tag_obj = new WebpageTags($this->get_given_config());
        $user_obj = $this->get_user_obj();
        //$webpage_tag_list = $webpage_tag_obj->get_tag_list_given_webpage_id($webpage->get_id(), $user_obj);
        //$markup .= $webpage_tag_list . "<br />\n";
        $markup .= "  </td>\n";
        $markup .= "</tr>\n";

      }
      $markup .= "</table>\n";
      $markup .= "<br />\n";

    return $markup;
  }

  // method
  public function output_table() {
    $markup = "";

    // resort
    //$sort_name_to_run = "problemography";
    //$user_obj = $this->get_user_obj();
    //$this->get_list_bliss()->resort($sort_name_to_run, $user_obj);

    // headings
    $markup .= "<table class=\"plants\">\n";
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    domain\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    img_url\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    path / title / name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    tags\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    maxonomy\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage) {

      $markup .= "</tr>\n";

      $num++;
      $markup .= "<td>\n";
      $markup .= "    " . $num . "<br />\n";
      $markup .= "</td>\n";

      // status
      $markup .= "  <td style=\"background-color: " . $webpage->get_status_background_color() . ";\">\n";
      $markup .= "    " . $webpage->get_status() . "<br />\n";
      $markup .= "</td>\n";

      // domain
      $markup .= "<td>\n";	
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $webpage->get_domain_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "</td>\n";

      // sort
      // previous
      //$column_name = "sort";
      //include_once("lib/timekeeper.php");
      //$timekeeper_obj = new Timekeeper();
      //$color = $timekeeper_obj->calculate_cell_color($column_name, $webpage->get_sort());
      //$markup .= "  <td style=\"background: $color; text-align: center; vertical-align: middle;\">\n";
      //$markup .= "    " . $webpage->get_sort() . "\n";
      //$markup .= "  </td>\n";
      // new dynamic sort button
      $markup .= $webpage->get_sort_cell() . "\n";

      // id
      $markup .= "<td>\n";
      $url = $this->url("webpages/" . $webpage->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $webpage->get_id() . "</a><br />\n";
      $markup .= "</td>\n";

      // img_url
      $markup .= "<td>\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $webpage->get_img_as_img_element_with_link($padding, $float, $width) . "<br />\n";
      $markup .= "</td>\n";

      // path / title / name
      $markup .= "<td>\n";

      if ($webpage->get_status() == "dev") {
      	$url = "http://" . $webpage->get_domain_name_dev() . $webpage->get_path();
      } else {
        $url = "http://" . $webpage->get_domain_obj()->get_domain_name() . $webpage->get_path();
      }

      // name
      $markup .= "    <span style=\"background-color: #EFEFEF; padding: 2px;\"><a href=\"" . $url . "\">" . $webpage->get_name() . "</a></span><br />\n";
      // path
      $markup .= "    <span style=\"padding: 2px;\"><strong><a href=\"" . $url . "\">" . $webpage->get_path() . "</a></strong></span><br />\n";

      // title
      //$url = $this->url("webpages/" . $webpage->get_id());
      //$markup .= "    <a href=\"" . $url . "\">[get-title-element]</a><br />\n";
      // debug
      //print "debug webpages url = $url <br />\n";
      $markup .= "</td>\n";

      // tags
      //$markup .= "<td style=\"font-size:50%\">\n";
      //include_once("webpage_tags.php");
      //$webpage_tag_obj = new WebpageTags($this->get_given_config());
      //$user_obj = $this->get_user_obj();
      //$webpage_tag_list = $webpage_tag_obj->get_tag_list_given_webpage_id($webpage->get_id(), $user_obj);
      //$markup .= $webpage_tag_list . "\n";
      //$markup .= "</td>\n";

      // maxonomy
      //$markup .= "<td style=\"text-align: center;\">\n";
      //$markup .= $webpage->special_out();
      //$markup .= "</td>\n";

      $markup .= "</tr>\n";

    }
    $markup .= "</table>\n";

          // on hold
          if (0) {
            // organize data into a hierarchy
            // initialize variable in which to store the hierarchical data
            // load the array so that the classes list stays intact
            $original = $this->get_list_bliss()->get_list();

            //print "debug count in original = " . count($original) . "<br />\n";
            // declare the associative array that holds the hierarchy of manmax
            $target_data = array();  # serial_number of manmax_obj => array of manmax_obj

            // the root node
            $manmax_root;        # used to carry obj from one loop to another loop
            $manmax_root_id = 0; # used to delete object form original list

            // now loop to get root
            $id = 0;
            foreach ($original as $manmax) {
              if ($manmax->get_manifest_obj()->get_status() == "root") {
                $manmax_root = $manmax; # save for next loop
                // add to target array
                $manmax_serial_number = $manmax->get_serial_number();
                $target_data[$manmax_serial_number] = array();
                // capture id so that it can be removed from array
                $manmax_root_id = $id;
                 break;
               }
               $id++;
            }
            // remove root object from original array
            unset($original[$manmax_root_id]);
          
            // debug
            //print "<pre>";
            //var_dump($original);
            //print "</pre>";

            // check that a root was found
            if (count($target_data) == 1) {
              // ok
              //print "debug root was found<br />\n";
            } else {
              $markup .= "<p class=\"error\">Error: no <kbd>root</kbd> was found.</p>\n";
              return $markup;
            }

            // now that the root is safely loaded, load the second level
            //print "debug count below root = " . count($original) . "<br />\n";
            $target_data_second_level = array();
            $data_third_level = array();
            // loop for second level
            foreach ($original as $manmax) {
              //print "debug this sort = " . $manmax->get_manifest_obj()->get_status() . "<br />\n";
              if ($manmax->get_manifest_obj()->get_status() == $manmax_root->get_description()) {
                //print "debug this found description = " . $manmax->get_description() . "<br />\n";
                // found, so store
                $manmax_serial_number = $manmax->get_serial_number();
                $target_data_second_level[$manmax_serial_number] = array();
              } else {
                // add to third level so that it can be done later
                array_push($data_third_level, $manmax);
              }
            }
            // add the array that was found to the target array
            $manmax_root_serial_number = $manmax_root->get_serial_number();
            $target_data[$manmax_root_serial_number] = $target_data_second_level;

            $target_data_third_level = array();

            // here with what third level
            //print "debug count at data_third_level = " . count($data_third_level) . "<br />\n";
            foreach ($data_third_level as $manmax) {
              $manmax_serial_number = $manmax->get_serial_number();
              //print "debug on third level <strong>" . $manmax_serial_number . " " . $manmax->get_description() . " ... " . $manmax->get_manifest_obj()->get_status() . "</strong><br />\n";
              // find within the target
              foreach ($target_data as $target_data_key => $target_data_value) {
                // debug
                //print "target_data_key = " . $target_data_key . "<br />\n";
                foreach ($target_data_value as $target_data_second_level_key => $target_data_second_level_value) {
                  //print "key: " . $target_data_second_level_key . "<br />\n";
                  //print "debug searching for     " . $manmax->get_manifest_obj()->get_status() . "<br />\n";
                  // given serial_number, get object
                  $second_level_manmax = $this->get_object_given_serial_number($target_data_second_level_key);
                  //print "debug searching against " . $second_level_manmax->get_description() . "<br />\n";
                  if ($manmax->get_manifest_obj()->get_status() == $second_level_manmax->get_description()) {
                    //print "<strong>match!</strong> " . $manmax->get_manifest_obj()->get_status() . "<br />\n";
                    // save to be added later
                    $target_data_third_level[$manmax_serial_number] = $target_data_second_level_key;
                    break;
                  }
                }
              }
            }
            // add third level
            // loop through array that holds all of the serials numbers to be added
            foreach ($target_data_third_level as $manmax_serial_number => $the_target_data_second_level_key) {
              //print "debug third level serial number = " . $manmax_serial_number . " " . $the_target_data_second_level_key . "<br />\n";
              // loop through existing target
              foreach ($target_data as $target_data_key => $target_data_value) {
                //print "debug " . $target_data_key . "<br />\n";
                foreach ($target_data_value as $target_data_second_key => $target_data_second_value) {
                  //print "debug " . $target_data_second_key . "<br />\n";
                  if ($target_data_second_key == $the_target_data_second_level_key) {
                    //print "debug matched " . $target_data_second_key . " " . $manmax_serial_number . "<br />\n";
                    // pushing into a subarray
                    $target_data[$target_data_key][$target_data_second_key][$manmax_serial_number] = array();
                  }
                }
              }
            }
          
            // debug
            //print "<pre>";
            //var_dump($target_data);
            //print "</pre>";

            // count what was transferred
            $transferred_count = 0;
            foreach ($target_data as $target_data_key => $target_data_value) {
              $transferred_count++;
              foreach ($target_data_value as $target_data_second_key => $target_data_second_value) {
                $transferred_count++;
                foreach ($target_data_second_value as $target_data_third_key => $target_data_third_value) {
                  $transferred_count++;
                }
              }
            }

            // check that all of the objects have been transferred
            //print "debug count list = " . count($this->get_list_bliss()->get_list()) . "<br />\n";
            //print "debug count transferred = " . $transferred_count . "<br />\n";
            if (count($this->get_list_bliss()->get_list()) != $transferred_count) {
              $markup .= "<p class=\"error\">Error: at least 1 maxonomy_manifest had not place in the hierarchy.</p>\n";
            }
          
            // output data
            $markup .= $this->output_ul($target_data);

            // todo clean up notes
            $markup .= "<div style=\"background-color: #0099AA; padding: 4px 4px 4px;\">\n";
            $markup .= "<p>Need to check that all the files on the <strong>server and database</strong> match the files that represented by this <strong>root domain_tli and description</strong></p>\n";
            $markup .= "<p>One way to to this is to output XML of all the files and then use a script. or perhaps have the PHP figure it out.</p>\n";
            $markup .= "</div>\n";

          } // end if-statement hold

    return $markup;
  }

  // method
  public function output_table_of_domain_tli_group() {
    $markup = "";

      // (this is a simple table and was replaced by hierarchical version)
      $markup .= "<table class=\"plants\">\n";

      // heading
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    domain_tli\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    manifests.status\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    manifest_path count\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $domain) {

        $markup .= "<tr>\n";

        $num++;
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $domain->get_domain_tli() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $domain->get_description() . "\n";
        $markup .= "  </td>\n";

        if ($domain->get_manifest_obj()->get_status() == "root") {
          $markup .= "  <td align=\"right\" style=\"background-color: #4949aa; color: yellow;\">\n";
        } else {
          $markup .= "  <td align=\"right\">\n";
        }
        $markup .= "    " . $domain->get_manifest_obj()->get_status() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td align=\"right\">\n";
        $url = "webpages?tli=" . $domain->get_domain_tli() . "&amp;description_id=" . $domain->get_description();
        $markup .= "    <a href=\"" . $url . "\">" . $domain->get_manifest_path_count() . "</a>\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";
      $markup .= "<br />\n";

      $markup .= "<p>Need to check that all the files on the <strong>server and database</strong> match the files that represented by this <strong>root domain_tli and description</strong></p>\n";
      $markup .= "<p>One way to to this is to output XML of all the files and then use a script. or perhaps have the PHP figure it out.</p>\n";

    return $markup;
  }

  // method
  private function make_fields($given_serial_number) {
    $fields = "";

    // note: wow, what a journey along the code path that was just to get here!
    // this, then, finally!, outputs a string of data
    // get object
    $manmax = $this->get_object_given_serial_number($given_serial_number);

    // make number padding
    if (strlen($manmax->get_manifest_path_count()) == 1) {
      // add space
      $fields .= "&nbsp;";
    }
    $url = "webpages?tli=" . $manmax->get_domain_tli() . "&amp;description_id=" . $manmax->get_description();
    $fields .= $manmax->get_domain_obj()->get_id();
    $fields .= "&nbsp;";
    $fields .= " == ";
    $fields .= "&nbsp;";
    $fields .= $manmax->get_domain_obj()->get_id();
    $fields .= "&nbsp;";
    $fields .= $manmax->get_manifest_path_count();
    $fields .= "&nbsp;";
    $fields .= "<a href=\"" . $url . "\">";
    $fields .= $manmax->get_description();
    $fields .= "</a>";

    return $fields;
  }

  // method (recursive function that outputs hierarchical data)
  function output_url($target_data) {
    $markup = "";

    $markup .= "<ul style=\"font-family: monospace; font-size: 20px;\">\n";
    foreach($target_data as $serial_number => $value_array){
      if(is_array($value_array)){
        $item = $this->make_fields($serial_number) . "\n" . $this->output_ul($value_array);
      } else {
        $item = $value_array;
      }
      $markup .= "\t<li style=\"padding: 0px 0px 0px 0px; list-style: none;\">$item</li>\n";
    }
    $markup .= "</ul>\n";

    return $markup;
  }

    // method
    public function output_table_of_description_group() {
      $markup = "";

      $url = "webpages?tli=" . $this->get_given_domain_tli();
      $markup .= "<p><em>to <a href=\"" . $url . "\"><em>" . $this->get_given_domain_tli() . " webpages</a></em></p>\n";

      // check with drupal
      $node_data = "";
      if (strstr($this->get_given_description_id(), "drupal")) {
        //print "debug contains drupal<br />\n";
        if ($this->get_given_domain_tli() == "rad") {
          //print "debug contains rad<br />\n";
          // get exsiting drupal node titles
          // query 1 (node)
          $sql = "SELECT node.nid, node.type, node.title FROM node ORDER BY node.type, node.title;";
          $node_data = $this->get_mysql_data($sql, "1");

          // query 2 (url_alias)
          $sql = "SELECT url_alias.source, url_alias.alias FROM url_alias;";
          $url_alias_data = $this->get_mysql_data($sql, "2");

          // output
          foreach ($node_data as $key => $value) {
            //$markup .= "nid = " . $value[0] . "<br />\n";
            //$markup .= "type = " . $value[1] . "<br />\n";
            //$markup .= "title = " . $value[2] . "<br />\n";
            foreach ($url_alias_data as $key2 => $value2) {
              $node_node = "node/" . $value[0];
              //$markup .= "node_node = " . $node_node . "<br />\n";
              if ($node_node == $value2[0]) {
                //$markup .= "source = " . $value2[0] . "<br />\n";
                //$markup .= "alias = " . $value2[1] . "<br /><br />\n";
                // add to array
                array_push($node_data[$key], $value2[1]);
                continue 2;
              }
            }
            // no alias
            //$markup .= "source = n/a<br />\n";
            //$markup .= "alias = n/a<br /><br />\n";
            array_push($node_data[$key], $value2[0]);
          }
          // debug
          foreach ($node_data as $key => $value) {
            //$markup .= "nid = " . $value[0] . "<br />\n";
            //$markup .= "type = " . $value[1] . "<br />\n";
            //$markup .= "title = " . $value[2] . "<br />\n";
            //$markup .= "alias = " . $value[3] . "<br />\n";
            //$markup .= "<br />\n";
          }
        }
      }

      $markup .= "<table class=\"plants\">\n";

      // heading
      $markup .= "<tr>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    #\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    id\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    domain_tli\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    description\n";
      $markup .= "  </td	>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    maxonomy\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    path\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    img_url\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    moneymaker\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    sort\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    max&nbsp;workdate\n";
      $markup .= "  </td>\n";
      $markup .= "  <td class=\"header\">\n";
      $markup .= "    rallytally\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";

      $num = 0;
      foreach ($this->get_list_bliss()->get_list() as $webpage) {

        $markup .= "<tr>\n";

        $num++;
        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $num . "<br />\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $url = $this->url("webpages/" . $webpage->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $webpage->get_id() . "</a>\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $webpage->get_domain_tli() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td class=\"mid\">\n";
        $markup .= "    " . $webpage->get_description() . "\n";
        $markup .= "  </td>\n";

        // maxonomy_id
        $markup .= "  <td class=\"mid\">\n";
        // prepare
        $maxonomy_name = $webpage->get_webpage_maxonomy_obj()->get_name();
        $url = $this->url("maxonomies/" . $webpage->get_webpage_maxonomy_obj()->get_id());
	$markup .= "    <a href=\"" . $url . "\">" . $webpage->get_webpage_maxonomy_obj()->get_img_url_as_img_element() . "</a> " . $webpage->get_webpage_maxonomy_obj()->get_name_with_link() . "\n";
        $markup .= "  </td>\n";

        // img_url
        $markup .= "  <td class=\"mid\">\n";
        if ($webpage->get_webpage_maxonomy_obj()->get_id() == 12) {
          $markup .= "    " . $webpage->get_path() . "\n";
        } else if ($webpage->get_webpage_maxonomy_obj()->get_id() == 7 ||
            $webpage->get_webpage_maxonomy_obj()->get_id() == 8) {
          // application or description, so make a hyperlink
          $url = "webpages?tli=" . $webpage->get_domain_tli() . "&amp;description_id=" . $webpage->get_path();
          $markup .= "    <a href=\"" . $url . "\">" . $webpage->get_path() . "</a>\n";
        } else {
          $markup .= "    " . $webpage->get_path() . "\n";
          $markup .= "    <a href=\"http://" . $webpage->get_domain_obj()->get_domain_name() . $webpage->get_path() . "\">&rarr;</a><br />\n";
          // code to be fixed (perhaps)
          //$markup .= "<strong style=\"color: red;\">FIX CODE</strong><br />\n";
          // check if this is a drupal node
          //foreach ($node_data as $key => $value) {
          //  //$markup .= "debug " . $value[3] . "<br />\n";
          //  if (("/" . $value[3]) == $webpage->get_path()) {
          //    $markup .= "<strong>found</strong><br />\n";
          //    $node_data[$key][4] = "found";
          //    break;
          //  }
          //}
        }

        $markup .= "  </td>\n";

        // img_url (repaired)
        $markup .= "  <td>\n";
        if ($webpage->get_img_url()) {
          // todo convert this link and text to a call to the ..with_link() function
          $markup .= "    <a href=\"http://" . $webpage->get_domain_obj()->get_domain_name() . $webpage->get_path() . "\">";
          $img_width = "65";
          $markup .= $webpage->get_img_url_as_img($img_width);
          $markup .= "</a>\n";;
        }
        $markup .= "  </td>\n";

        // moneymaker
        $markup .= "  <td class=\"mid\">\n";
        include_once("moneymakers.php");
        $moneymaker_obj = new Moneymakers($this->get_given_config());
        $user_obj = $this->get_user_obj();
        $markup .= "    " . $moneymaker_obj->get_data_given_webpage_id($webpage->get_id(), $user_obj) . "\n";
        $markup .= "  </td>\n";

        // sort
        //// if the string has a dash like my 15-Sep-2012 timestamp
        //if (substr($webpage->get_sort(), 1, 1) == "-" ||
        //    substr($webpage->get_sort(), 2, 1) == "-") {
        //  $markup .= "  <td class=\"mid\" style=\"background-color: #AFFFCC;\">\n";
        //} else {
        //  $markup .= "  <td class=\"mid\">\n";
        //}
        $markup .= $webpage->get_sort_cell() . "\n";
        //$markup .= "  </td>\n";

        // cell with button or workdate
        if ($webpage->get_found_max_workdate()) {
          $markup .= $this->get_manifest_obj()->output_button_or_workdate($webpage->get_domain_tli(), $webpage->get_description(), $webpage->get_id(), $webpage->get_found_max_workdate());
        } else {
          $markup .= "  <td class=\"mid\">\n";
          $markup .= "  </td>\n";
        }

        // rallytally
        $markup .= "  <td>\n";
        $markup .= "  <div style=\"font-size: 50%;\">\n";
        include_once("rallytallies.php");
        $rallytally_obj = new RallyTallies($this->get_given_config());
        $domain_name = $webpage->get_domain_obj()->get_domain_name();
        $markup .= $rallytally_obj->get_link($webpage->get_domain_tli(), $domain_name);
        $markup .= "  </div>\n";
        $markup .= "  </td>\n";

        $markup .= "</tr>\n";
      }
      $markup .= "</table>\n";

      if ($node_data) {
        foreach ($node_data as $key => $value) {
          if ($value[3] == "found") {
            // ok
          } else {
            $markup .= "<p class=\"error\">not found <strong>" . $value[2] . "</strong></p>\n";
          }
        }
      }

      return $markup;
    }

  // method
  public function print_manmax_tests($tenperday_obj) {
    // get data from database
    $table = "manmax_tests";
    $this->do_database($table, $this->get_id(), $tenperday_obj);

    // $markup .=    
    $markup .= "<table class=\"domains\">\n";
    foreach ($this->manmax_tests_array as $manmax_test) {
      $markup .= "<tr>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $manmax_test->get_id() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $manmax_test->get_date() . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "    " . $manmax_test->get_result() . "\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
  }

  // method
  //public function make_manmax_test() {
  //  include_once("manmax_test.php");
  //  $manmax_test = new ManMaxTest();
  //  array_push($this->manmax_tests_array, $manmax_test);  
  //  return $manmax_test;
  //}

  // method
  public function do_database($table, $one, $tenperday_obj) {

    // get data
    $tenperday_obj->get_db_dash()->craft_sql_statement($table, $one);
    $tenperday_obj->get_db_dash()->execute();

    // load data into objects
    $results = $tenperday_obj->get_db_dash()->get_results();
    for ($lt = 0; $lt < pg_numrows($results); $lt++) {
      if ($table == "manmax_tests") {
        $manmax_test = $this->make_manmax_test();
        $manmax_test->set_id(pg_result($results, $lt, 0));
        $manmax_test->set_manmax_id(pg_result($results, $lt, 1));
        $manmax_test->set_test_id(pg_result($results, $lt, 2));
        $manmax_test->set_result(pg_result($results, $lt, 3));
        $manmax_test->set_date(pg_result($results, $lt, 4));
      } else {
        $markup .= "<p>error: table is not known for db results: \"$table\"\n";
      }
    }
  }

  // method
  public function print_maxonomy_manifests_table($tli, $description_id, $base) {

    // finally! here are the details of the maxonomy_manifests

    // output

    // output heading with a "back" link
    if ($base != "root") {
      // this is a child in the hierarchy, so output a "back" link
      $referer_url = $_SERVER['HTTP_REFERER'];
      if ($referer_url) {
        $markup .= "<br />\n";
        $markup .= "<p style=\"padding: opx 0px 0px 8px; color: #000000;\"><a href=\"" . $referer_url . "\">back</a></p>\n";
      }
    }

    // subtitle for this section
    $markup .= "<h1>Maxonomy Manifests</h1>\n";

    // todo figure out this subheading
    // $markup .= "<h2 style=\"padding: 0px 0px 0px 13px;\">" . $given_parent_path . " <em>container</em></h2>\n";

    // start table
    $markup .= "<table class=\"domains\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    maxonomy name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    rallytally\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    maxonomy manifest\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    manifest\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    development page\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    workdate\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    gallery\n";
    $markup .= "  </td>\n";
    $markup .= "<tr>\n";

    // output rows of data
    $this->output_rows($tli, $description_id);

    $markup .= "</table>\n";
    $markup .= "<br />\n";

  }

  // method
  public function output_rows($tli, $description_id) {

    // output the rows of data with plenty of processing

    // an explanation of this function:
    // the data was selected from the db based upon tli and version
    // however, the display of that data depends upon...
    // the maxonomy type of the given maxonomy_manifest

    // about the parameters
    // this function is given given a tli and version
    // a tli is a three letter identifier of a domain
    // a version is a unique name defining a manifest

    // there is a third parameter "maxonomy_id"
    // this prints out just the rows which are of that value
    // this is used for containers

    // so capture that user input
    $given_maxonomy_id = "";
    if (isset($_GET['maxonomy_id'])) {
      # parameter
      $given_maxonomy_id = $_GET['maxonomy_id'];
    }

    // set up data
    $domain = $this->get_domain_obj_given_tli($tli, $this->domains_array);

    // set up more data
    // each maxonomy type has an image
    include_once("maxonomies.php");
    $webpage_maxonomy_obj = new Maxonomies();
    $maxonomy_icons_array = $webpage_maxonomy_obj->get_maxonomy_icons_array($this->maxonomies_array);

    // tally as it goes
    $_images_count = 0;
    $_styles_count = 0;
    $favicon_count = 0;
    $redirect_count = 0;
    $robots_txt_count = 0;
    $error_page = 0;
    $num = 0; 

    // loop, displaying each webpage
    foreach ($this->webpages_array as $webpage) {

      // set up data
      $maxonomy_id = $webpage->get_maxonomy_id();
      $maxonomy_name = $webpage_maxonomy_obj->get_maxonomy_name_given_maxonomy_id($this->maxonomies_array, $maxonomy_id);

      if ($given_maxonomy_id) {
        // do not count
        // and filter those rows not of this id
        if ($given_maxonomy_id != $webpage->get_maxonomy_id()) {
          continue;
        }
      } else {
        // skip maxonomy_manifests whose type is a "contained" object
        // this means that the object is gathered together in a directory
        // that directory may be a real directory, like "_images"
        // or that directory may be a virtual directory, ...
        // for example, we'll say the "redirect" are displayed together
        if ($maxonomy_name == "_images") {
            $_images_count++;
            continue;
        } else if ($maxonomy_name == "redirect") {
            $redirect_count++;
            continue;
        } else if ($maxonomy_name == "favicon") {
            $favicon_count++;
            continue;
        } else if ($maxonomy_name == "_styles") {
            $_styles_count++;
            continue;
        } else if ($maxonomy_name == "robots.txt") {
            $robots_txt_count++;
            continue;
        } else if ($maxonomy_name == "error_page") {
            $error_page_count++;
            continue;
        }
      }

      // row start
      $markup .= "<tr>\n";

      // num
      $num++;
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // maxonomy_name and maxonomy_id and maxonomy_icon
      $maxonomy_cell = "";
      $maxonomy_cell .= "  <td>\n";
      // build maxonomy table cell once
      $maxonomy_cell .= "    <!-- maxonomy_id = " . $maxonomy_id . " -->\n";
      if ($maxonomy_name) {
        if (array_key_exists($maxonomy_name, $maxonomy_icons_array)) {
          $maxonomy_cell .= "    <img src=\"" . $maxonomy_icons_array[$maxonomy_name] . "\" alt=\"\" />\n";
        } else {
        }
        $maxonomy_cell .= "    " . $maxonomy_name . "\n";
      } else {
        $maxonomy_cell .= "?\n";
      }
      $maxonomy_cell .= "  </td>\n";
      $markup .= $maxonomy_cell;

      // $markup .= rallytally link
      $markup .= "  <td>\n";
      if ($maxonomy_name == "homepage" ||
          $maxonomy_name == "_styles") {
        $markup .= "    <a href=\"rallytally.php?tli=" . $tli . "&amp;description-id=" . $description_id . "&amp;maxonomy_id=" . $maxonomyid . "\">rallytally</a>\n";
      } else {
        $markup .= "  &nbsp;\n";
      }
      $markup .= "  </td>\n";

      // id
      $markup .= "  <td>\n";
      $markup .= "    <!-- " . $webpage->get_id() . "-->\n";
      $markup .= "    " . $webpage->get_id() . "\n";
      $markup .= "  </td>\n";

      // sort
      //if ($webpage->get_sort() == "ok") {
      //  $markup .= "  <td style=\"background-color: green;\">\n";
      //} else if ($webpage->get_sort() == "todo") {
      //  $markup .= "  <td style=\"background-color: darkred;\">\n";
      //} else {
      //  $markup .= "  <td style=\"background-color: orange; color: #26466D;\">\n";
      //}
      $markup .= $webpage->get_sort_cell() . "\n";
      //$markup .= "  </td>\n";

      // maxonomy_manifest link
      // different based upon the maxonomy_name
      // output cell based upon the maxonomy type that it is
      if ($maxonomy_name == "application") {

        // validation of the application name
        // search through application table's rows
        $found_application_obj;
        foreach ($this->applications_array as $application) {
          if ($application->get_name() == $webpage->get_path()) {
            $flag = "found";
            $found_application_obj = $application;
            break;
          }
        }
 
        if ($found_application_obj) {

          // 1 cell of this series
          $markup .= "  <td style=\"background-color: #8A2BE2;\">\n";
          $markup .= "    <a href=\"" . $application->get_url() . "\">" . $webpage->get_path() . "</a>";
          $markup .= "</td>\n";

          // 2 cell of this series
          // container
          $markup .= "<td>\n";
          $path =$webpage->get_path();
          $markup .= "    <a href=\"manifest.php?tli=" . $tli . "&description-id=" . $webpage->get_path() . "\">manifest</a><br />\n";
          $markup .= "</td>\n";

          // 3 cell of this series
          // development_page
          $markup .= "<td>\n";
          $markup .= "  <a href=\"" . $found_application_obj->get_development_url() . "\">designdoc</a>\n";
          $markup .= "</td>\n";

        } else {
          // error
          // application not found, so send warning message
          $markup .= "<td colspan=\"3\">\n";
          $markup .= "  <p style=\"background-color: red;\">Warning: this <em>application</em> is not in the <strong>applications</strong> table.</p>\n";
          $markup .= "</td>\n";
        }

      } else if ($maxonomy_name == "description") {

        $markup .= "  <td>\n";
        $manifest_link = $domain->get_domain_name() . $manifest_stem . $webpage->get_path();
        $markup .= "    <a href=\"http://" . $manifest_link . "\">" . $webpage->get_path() . "</a>";
        $markup .= "  </td>\n";
        $markup .= "  <td colspan=\"2\">\n";
        $markup .= "    <a href=\"manifest.php?tli=" . $tli . "&description-id=" . $webpage->get_path() . "\">manifest</a><br />\n";
        $markup .= "  </td>\n";

      } else {

        // default (typically "homepage" and "blanks")
        $markup .= "  <td colspan=\"3\">\n";
        // there might be an path
        $manifest_link = $domain->get_domain_name() . $manifest_stem . $webpage->get_path();
        $markup .= "    <a href=\"http://" . $manifest_link . "\">" . $webpage->get_path() . "</a><br />\n";

        $markup .= "  </td>\n";
      }

      // gallery
      $markup .= "  <td>\n";
      $markup .= "    " . $webpage->get_gallery() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }

    // the regular rows have been output
    // now is the time to output the aggregate message of skipped rows 
    if ($_images_count) {
      $maxonomy_name = "_images";
      $maxonomy_id = $webpage_maxonomy_obj->get_maxonomy_id_given_maxonomy_name($this->maxonomies_array, $maxonomy_name);
      $src = $maxonomy_icons_array[$maxonomy_name];
      $page_count = $_images_count;
      $this->print_container_cells($src, $maxonomy_name, $maxonomy_id, $tli, $description_id, $page_count);
    }
    if ($_styles_count) {
      $maxonomy_name = "_styles";
      $maxonomy_id = $webpage_maxonomy_obj->get_maxonomy_id_given_maxonomy_name($this->maxonomies_array, $maxonomy_name);
      $src = $maxonomy_icons_array[$maxonomy_name];
      $page_count = $_styles_count;
      $this->print_container_cells($src, $maxonomy_name, $maxonomy_id, $tli, $description_id, $page_count);
    }
    if ($favicon_count) {
      $maxonomy_name = "favicon";
      $maxonomy_id = $webpage_maxonomy_obj->get_maxonomy_id_given_maxonomy_name($this->maxonomies_array, $maxonomy_name);
      $src = $maxonomy_icons_array[$maxonomy_name];
      $page_count = $favicon_count;
      $this->print_container_cells($src, $maxonomy_name, $maxonomy_id, $tli, $description_id, $page_count);
    }
    if ($redirect_count) {
      $maxonomy_name = "redirect";
      $maxonomy_id = $webpage_maxonomy_obj->get_maxonomy_id_given_maxonomy_name($this->maxonomies_array, $maxonomy_name);
      $src = $maxonomy_icons_array[$maxonomy_name];
      $page_count = $redirect_count;
      $this->print_container_cells($src, $maxonomy_name, $maxonomy_id, $tli, $description_id, $page_count);
    }
    if ($robots_txt_count) {
      $maxonomy_name = "robots.txt";
      $maxonomy_id = $webpage_maxonomy_obj->get_maxonomy_id_given_maxonomy_name($this->maxonomies_array, $maxonomy_name);
      $src = $maxonomy_icons_array[$maxonomy_name];
      $page_count = $robots_txt_count;
      $this->print_container_cells($src, $maxonomy_name, $maxonomy_id, $tli, $description_id, $page_count);
    }
    if ($error_page_count) {
      $maxonomy_name = "error_page";
      $maxonomy_id = $webpage_maxonomy_obj->get_maxonomy_id_given_maxonomy_name($this->maxonomies_array, $maxonomy_name);
      $src = $maxonomy_icons_array[$maxonomy_name];
      $page_count = $error_page_count;
      $this->print_container_cells($src, $maxonomy_name, $maxonomy_id, $tli, $description_id, $page_count);
    }
  }

  // method
  public function get_mysql_data($sql, $description_id) {
      // todo does not work yet because it needs to use database interface
      // connect to mysql database server
      $host = "localhost";
      $username = "";
      $password = "";
      $hd = mysql_connect($host, $username, $password)
          or die ("Unable to connect");
 
      // select database
      $database = "";
      mysql_select_db ($database, $hd)
            or die ("Unable to select database");
 
      // execute query
      $result = mysql_query($sql, $hd)
          or die ("Unable to run query");
 
      // iteration loop
      $data = array();
      $num = 0;
      while ($row = mysql_fetch_assoc($result)) {
        $num++;
        if ($description_id == "1") {
          $data[$num] = array($row["nid"], $row["type"], $row["title"]);
        } else if ($description_id == "2") {
          $data[$num] = array($row["source"], $row["alias"]);
        }
      }

      // close
      mysql_close($hd);

    return $data;
  }

  // method
  function get_count_webpages($given_tli, $given_user_obj) {
      $count_webpages = "unknown";

      // get total number of rows for this tli

      // load data from database
      $this->set_given_domain_tli($given_tli);
      $this->set_user_obj($given_user_obj);

      // load data from database
      $this->set_type("get_count_all_domain_tli");
      $markup = $this->prepare_query();
      // check for error message
      if ($markup) {
        print $markup;
      }

      // return row count
      return $this->get_list_bliss()->get_count();
  }

  // method
  function get_count_webpages_type_application($given_tli, $given_user_obj) {
      $count_webpages = "0";

      // load data from database
      $this->set_given_domain_tli($given_tli);
      $this->set_user_obj($given_user_obj);

      // load data from database
      $this->determine_type();
      $markup = $this->prepare_query();
      // check for error message
      if ($markup) {
        print $markup;
      }

      foreach ($this->get_list_bliss()->get_list() as $webpage_obj) {
        // add the number of manifes_maxonomies for this domain_tli
        // note: id 7 is for "application"
        if ($webpage_obj->get_webpage_maxonomy_obj()->get_id() == 7) {
          $count_webpages += 1;
        }
      }

      return $count_webpages;
  }

  // method
  private function output_table_for_rallytally() {
    $markup = "";

    // set up data
    include_once("domains.php");
    $domain_obj= new Domains();
    $user_obj = $this->get_user_obj();
    $base_url = $domain_obj->get_domain_name_given_tli($this->get_given_domain_tli(), $user_obj);

    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $url_string = "http://" . $base_url . $obj->get_path();
      $markup .= "<p class=\"url\"><a href=\"" . $url_string . "\">" . $url_string . "</a></p>\n";
    }

    return $markup;
  }

  // method
  public function get_count_given_maxonomy_id($given_maxonomy_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_maxonomy_id($given_maxonomy_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // get count
    $markup .= $this->get_list_bliss()->get_count();

    return $markup;
  }

  // method
  public function get_list_given_maxonomy_id($given_maxonomy_id, $given_user_obj, $given_status_filter_string = "") {
    $markup = "";

    // set
    $this->set_given_maxonomy_id($given_maxonomy_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    //$count = $this->get_list_bliss()->get_count();
    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      if ($webpage->get_status() == $given_status_filter_string) {
        $url = $this->url("domains/" . $webpage->get_domain_obj()->get_tli());
        $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $webpage->get_domain_obj()->get_tli() . "</a>";
        $markup .= "-";
        $url = $this->url("webpages/" . $webpage->get_id());
        $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $webpage->get_id() . "</a>";
        //if ($num != $count) {
        //  $markup .= ", ";
        //}
        $markup .= " ";
      }
    }

    return $markup;
  }

  // method
  public function get_list_given_maxonomy_id_and_domain_tli($given_maxonomy_id, $given_domain_tli, $given_user_obj) {
    $markup = "";

    if (! $given_domain_tli) {
       return "webpages.php: error given_domain_tli not set\n";
    }
    
    // set
    $this->set_given_maxonomy_id($given_maxonomy_id);
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    //$count = $this->get_list_bliss()->get_count();
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      $num++;
      $url = $this->url("webpages/" . $webpage->get_id());
      $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $webpage->get_id() . "</a>";
      //if ($num != $count) {
      //  $markup .= ", ";
      //}
      $markup .= " ";
    }

    return $markup;
  }

  // method
  public function get_list_with_name_given_maxonomy_id($given_maxonomy_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_maxonomy_id($given_maxonomy_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    //$count = $this->get_list_bliss()->get_count();
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      $num++;
      $url = $this->url("webpages/" . $webpage->get_id());
      $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $webpage->get_id() . "</a>";
      $markup .= " ";
      $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $webpage->get_name_with_link() . "</a>";
      //if ($num != $count) {
      //  $markup .= ", ";
      //}
      $markup .= "<br />";
    }

    return $markup;
  }

  // method
  public function output_button_or_workdate($given_tli, $given_max_workdate) {
    $markup = "";

    $is_green_flag = "";
    $style = "";

    // debug
    //print "debug webpage_date = " . $webpage_date . "<br />\n";

    // todo need to get webpage_date from somewhere
    $webpage_date = "";
    if ($webpage_date) {
      // a workdate exists, so see if it today's
      // get today's date
      include_once("lib/timekeeper.php");
      $timekeeper_obj = new Timekeeper();
      $date_today = $timekeeper_obj->get_today_date_as_first_year_style();
      // debug
      //print "debug date_today = " . $date_today . "<br />\n";
      if ($webpage_date == $date_today) {
        $is_green_flag = "true";
        $style = " style=\"background-color: green;\"";
      }
    }
    // ready to create cell
    //$markup .= "  <td " . $style . ">\n";
    //$markup .= "  " . $webpage_date . "<br />";
    if ($is_green_flag) {
      // skip because it has a date for today already
      // so, no button is necessary
    } else {
      // no workdate yet, so button
      if (0) {
        $markup .= "<!-- embedded table -->\n";
        $markup .= "<table border=\"0\">\n";
        $markup .= "<tr>\n";
        $markup .= "  <td>\n";
        $markup .= "    " . $this->get_date_submit_button($given_tli);
        $markup .= "  </td>\n";
        $markup .= "  </tr>\n";
        $markup .= "  </table> <!-- embedded -->\n";
	}
    }
    //$markup .= "  </td>\n";

    return $markup;
  }

  // method
  private function get_date_submit_button($given_tli) {
    $markup = "";

    $tab = "";
    // todo this next line has another line like it in this file
    //$url = $this->url("webpages/domains/" . $given_tli);
    $url = $this->url("webpages/" . $given_tli);
    $markup .= $tab . "<form action=\"" . $url . "\" method=\"get\">\n";
    $markup .= $tab . "    <input type=\"hidden\" name=\"webpage_id\" value=\"" . $this->get_id() . "\" />\n";
    //$markup .= $tab . "    <input type=\"hidden\" name=\"tli\" value=\"" . $given_tli . "\" />\n";
    //$markup .= $tab . "    <input type=\"hidden\" name=\"description-id\" value=\"" . $this->get_given_description_id() . "\" />\n";
    $markup .= $tab . "    <button name=\"submit\" value=\"add_date\">add date</button>\n";
    $markup .= $tab . "    </form>\n";

    return $markup;
  }

  // special
  public function get_project_id() {
    $markup = "";

    return "[not-yet-defined]";
  }

  // this is a static function
  // todo sort by the method of an object
    public static function cmp_obj( $a, $b ) { 
    if( $a->get_webpage_maxonomy_obj()->get_maxonomy_obj()->get_order_by() == $b->get_webpage_maxonomy_obj()->get_maxonomy_obj()->get_order_by()){ return 0 ; } 
    return ($a->get_webpage_maxonomy_obj()->get_maxonomy_obj()->get_order_by() > $b->get_webpage_maxonomy_obj()->get_maxonomy_obj()->get_order_by()) ? -1 : 1;
  } 

  // method
  public function output_given_variables_extra() {
    $markup = "";

    if ($this->get_given_domain_tli()) {
  
      $url = $this->url("domains");
      $markup .= "This is <a href=\"" . $url . "\">domains</a> <strong>" . $this->get_given_domain_tli() . "</strong>.";
    }
    if ($this->get_given_description_id()) {
      $class_type = "This is description_id " ;
      $given_variable = $this->get_given_description_id();
      $name_with_link = "";
      $markup_being_sent = "";
      $markup .= $this->output_given_variables_boilerplate($class_type, $given_variable, $name_with_link, $markup_being_sent);
    }

    return $markup;
  }

  // method
  public function get_row_count_from_db($given_user_obj) {
    $markup = "";

    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->set_type("get_row_count");
    $markup .= $this->prepare_query();

    if ($this->get_list_bliss()->get_count()) {
      return $this->get_list_bliss()->get_first_element()->get_row_count();
    } else {
      return "error no row count";
    }
  }

  // method
  public function get_oldest_sort_object_from_db() {
    $markup = "";

    // load data from database
    $this->set_type("get_oldest_sort_obj");
    $markup .= $this->prepare_query();

    if ($markup) {
      $markup .= "<p>error get_oldest_sort_object_from_db()</p>\n";
      return $markup;
    }

    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      // debug
      //print "debug webpages oldest obj name = " . $webpage->get_name() . "<br />\n";

      return $webpage;
    }

  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // submit
    $parameter_a = new Parameter();
    $parameter_a->set_name("submit");
    $parameter_a->set_validation_type_as_command();
    array_push($parameters, $parameter_a);

    // webpage_id
    $parameter_b = new Parameter();
    $parameter_b->set_name("webpage_id");
    $parameter_b->set_validation_type_as_id();
    array_push($parameters, $parameter_b);

    // tli
    //$parameter_c = new Parameter();
    //$parameter_c->set_name("tli");
    //$parameter_c->set_validation_type_as_tli();
    //array_push($parameters, $parameter_c);

    // make-sort-today
    $parameter_c = new Parameter();
    $parameter_c->set_name("make-sort-today");
    $parameter_c->set_validation_type_as_id();
    array_push($parameters, $parameter_c);

    // view
    $parameter_d = new Parameter();
    $parameter_d->set_name("view");
    $parameter_d->set_validation_type_as_view();
    array_push($parameters, $parameter_d);
    // get parameters (if any) and validate
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "submit") {
            $this->set_given_submit($parameter->get_value());
            // debug
            print "debug webpages parameter submit = " . $this->get_given_submit() . "<br />\n";
          }
          if ($parameter->get_name() == "view") {
            $this->set_given_view($parameter->get_value());
          }
          if ($parameter->get_name() == "webpage_id") {
            $this->set_given_webpage_id($parameter->get_value());
            // debug
            print "debug webpages parameter webpage_id = " . $this->get_given_webpage_id() . "<br />\n";
          }
          //if ($parameter->get_name() == "tli") {
          //  $this->set_given_domain_tli($parameter->get_value());
          //  // debug
          //  print "debug webpages parameter tli = " . $this->get_given_domain_tli() . "<br />\n";
          //}
          if ($parameter->get_name() == "make-sort-today") {
            $this->set_given_make_sort_today_id($parameter->get_value());
            // perform update
            $table_name = "webpages";
            $db_table_field = "id";
            $database_name = "mudiacom_soiltoil";
            // get sort letter
            $webpage_obj = new Webpages($this->get_given_config());
            $user_obj = $this->get_user_obj();
            $sort_letter = $webpage_obj->get_sort_letter_given_id($parameter->get_value(), $user_obj);
            $markup .= $this->update_sort_to_today($table_name, $db_table_field, $sort_letter, $database_name);
            // redirect_simple
            $target_page = $parameter->get_value();
            $this->redirect_helper($target_page);
          }
        }
      }
    }

    if ($this->get_given_submit() == "add_date") {
      $this->insert_add_date();
      // redirect
      include_once("lib/url_helper.php");
      $url_helper = new UrlHelper();
      // todo this next line has 3 other lines like it in this file
      //$target_page = $this->url("webpages/domain/" . $this->get_given_domain_tli());
      $target_page = $this->url("webpages/" . $this->get_given_domain_tli());
      $url_helper->redirect_simple($target_page);
      
    }

    return $markup;
  }

  // method
  //public function insert_add_date() {
  //  $markup = "";
  //
  //  // set for this class
  //  $table_name = "location_workdates";
  //  $class_name_string = "webpages";
  //
  //  // set up
  //  include_once("lib/timekeeper.php");
  //  $timekeeper_obj = new Timekeeper();
  //
  //  // sql
  //  $sql = "INSERT INTO " . $table_name . " (class_primary_key_string, date, class_name_string) VALUES ('" . $this->get_given_webpage_id() . "',  '" . $timekeeper_obj->get_now_date() . "', '" . $class_name_string . "');";
  //  // change database
  //  $markup .= $this->get_db_dash()->new_update($this, $sql);
  //
  //  return $markup;
  //}

  // method
  public function get_tenperday($given_user_obj) {
    $markup = "";

    $this->set_user_obj($given_user_obj);

    // load data from database
    $markup .= $this->set_type("get_all");
    $markup .= $this->prepare_query();

    if ($markup) {
      $markup .= "<p>error webpages: prepare_query() had an error</p>\n";
      return $markup;
    }

    // set some variables
    $padding = "10";
    $float = "";
    $width = "66";
    include_once("lib/dates.php");
    $date_obj = new Dates();

    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      //$webpage_date = $location_workdate_obj->get_most_recent_date_given_id($webpage->get_id());
      //$webpage_date_year_style = $date_obj->convert_from_twodash_to_date_as_first_year_style($webpage_date);
      $webpage_date_year_style = "";
      // debug
      //print "debug webpages_date_no_y = " . $webpage_date_year_style . "<br />\n";
      // filter for today
      if ($date_obj->is_today($webpage_date_year_style)) {
        $url = $this->url("webpages/" . $webpage->get_id());
        $width = "60";
        $height = "65";
        $markup .= "<table border=\"0\" style=\"display: inline; vertical-align: top;\">\n";
        $markup .= "<tr>\n";
        $markup .= "<td style=\"width: 30px; text-align: center; font-size: 80%;\">\n";
        $markup .= "  <a href=\"" . $url . "\">" . $webpage->get_img_url_as_img_element($padding, $float, $width, $height)  . "</a><br />\n";
        $url = "http://" . $webpage->get_domain_obj()->get_domain_name() . $webpage->get_path();
        $markup .= "  <span style=\"font-size: 80%;\"><a href=\"" . $url . "\">" . $webpage->get_name() . "</a></span>\n";
        $markup .= "</td>\n";
        $markup .= "</tr>\n";
        $markup .= "</table>\n";
      }
    }

    return $markup;
  }

  // method
  public function relate($given_pm_obj) {
    $markup = "";

    if (get_class($given_pm_obj) == "Domains") {
      $markup .= "debug webpages: <strong>ok, map webpages to domains</strong><br />\n";
    } else if (get_class($given_pm_obj) == "PlantLists") {
      $markup .= "debug webpages: ok, not related, so ignore<br />\n";
    } else if (get_class($given_pm_obj) == "Lands") {
      $markup .= "debug webpages: ok, not related, so ignore<br />\n";
    } else {
      $markup .= "debug webpages: error, not defined, so modify relate() function<br />\n";
    }

    return $markup;
  }

  // method
  public function search($given_string) {
    $markup = "";
    $this->set_type("search");
    $sql = "SELECT webpages.id, webpages.name FROM webpages WHERE webpages.name ilike '%" . $given_string . "%';";
    // debug
    //print "debug webpages: sql = " . $sql . "<br />\n";
    $markup .= parent::load_data($this, $sql);
    foreach ($this->get_list_bliss()->get_list() as $obj) {
      $markup .= "Webpages ";
      $markup .= "id ";
      $markup .= $obj->get_id();
      $markup .= " ";
      $markup .= $obj->get_name_with_link();
      $markup .= "<br />";
    }
    return $markup;
  }

  // method
  public function special_out($icon_flag = "") {
    $markup = "";
    
    include_once("webpage_maxonomies.php");
    $webpage_maxonomy_obj = new WebpageMaxonomies($this->get_given_config());
    $user_obj = $this->get_user_obj();
    $webpage_maxonomy_obj_array = $webpage_maxonomy_obj->get_list_of_maxonomy_obj_given_webpage_id($this->get_id(), $user_obj);

    // output count
    if ($icon_flag == "") {
      $url = $this->url("maxonomies/webpages/" . $this->get_id());
      $markup .= "<a href=\"" . $url . "\">" . count($webpage_maxonomy_obj_array) . "</a>\n";
      return $markup;
    }

    // output icon
    $padding = "";
    $float = "";
    $width = "65";
    foreach ($webpage_maxonomy_obj_array as $webpage_maxonomy_obj) {
        $markup .= $webpage_maxonomy_obj->get_img_as_img_element_with_link($padding, $float, $width) . "<br />\n";
        $markup .= $webpage_maxonomy_obj->get_name() . "<br />\n";
    }

    return $markup;
  }

  // method
  public function get_webpage_sym_given_id($given_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      $url = $this->url("webpages/" . $webpage->get_id());
      $markup .= "<a href=\"" . $url . "\" class=\"show\">" . $webpage->get_name() . "</a><br />\n";
    }

    return $markup;
  }

  public function get_domain_name_dev() {
    // get data from a db field
    $description = $this->get_description();
    //print "description=" . $description . "<br />\n";
    // get all the string after a string
    $prefix = 'dev=';
    $index = strpos($description, $prefix);
    if ($index === FALSE) {       
      // bail
      // return default of the normal situation
      // todo this might not be the current natural behavior for this method
      return $this->get_domain_obj()->get_domain_name();
    }
    //print "index=" . $index . "<br />\n";
    // add the number of characters of the dev= string so that it is removed
    $index += 4;
    $last_part = substr($description, $index);
    $string = $last_part;
    // print "string=" . $string . "<br />\n";
    // get all the string before a string
    $suffix = '=dev';
    $index = strpos($string, $suffix);
    //print "index=" . $index. "<br />\n";
    if ($index === FALSE) {       
      // bail
      // return default of the normal situation
      // todo this might not be the current natural behavior for this method
      return $this->get_domain_obj()->get_domain_name();
    }
    $what_is_left = substr($string, 0, $index);
    return $what_is_left;
  }

  public function get_webpages_sorted_today($given_user_obj) {
    $markup = "";

    include_once("lib/timekeeper.php");
    $timekeeper_obj = new Timekeeper();
    $now_date = $timekeeper_obj->get_now_date();
    $sort = "Y " . $now_date;

    // set
    $this->set_given_sort($sort);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      $num++;
      $url = $this->url("webpages/" . $webpage->get_id());
      $markup .= $num . " <a href=\"" . $url . "\" class=\"show\">" . $webpage->get_name() . "</a><br />\n";
    }

    return $markup;
  }

  // method
  public function get_webpages_sorted_today_given_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    // this one is different because it filters both during the sql and during the looping

    // set
    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // get sort as now_date
    include_once("lib/timekeeper.php");
    $timekeeper_obj = new Timekeeper();
    $now_date = $timekeeper_obj->get_now_date();
    $sort_now_date = "Y " . $now_date;

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $webpage) {
      //print "webpags debug: " . $webpage->get_domain_obj()->get_tli() . " given " . $given_domain_tli . "<br />\n";
      //print "webpags debug: " . $webpage->get_sort() . " given " . $sort_now_date . "<br />\n";
      if ($webpage->get_sort() == $sort_now_date) {
          $num++;
          $url = $this->url("webpages/" . $webpage->get_id());
          $markup .= $num . " <a href=\"" . $url . "\" class=\"show\">" . $webpage->get_name() . "</a><br />\n";
       }
    }

    return $markup;
  }

  // method
  public function output_urls() {
    $markup = "";

    $num = 0;

    $markup .= "<pre>";
    foreach ($this->get_list_bliss()->get_list() as $webpage) {
        $num++;
        $markup .= "<kbd><url>";
        $markup .= "http";
        if ($webpage->get_domain_obj()->get_ssl_cert() == "t") {
           $markup .= "s";
        }
        $markup .= "://";
        if ($webpage->get_domain_obj()->get_domain_name()) {
      	    $markup .= $webpage->get_domain_obj()->get_domain_name();
        }
        if ($webpage->get_path()) {
            $markup .= $webpage->get_path();
        }
        if ($webpage->get_parameters()) {
            // if more than one line then output FLAG
            if (substr_count($webpage->get_parameters(), '?') > 1) {
        	    # skip
         	} else {
     	        $markup .= "<!-- parameters = " . $webpage->get_parameters() . " -->";
         	}
        }
        $markup .=  "</url></kbd>\n";
    }
    $markup .= "</pre>";
    $markup .= "<br />\n";
    $markup .= "urls count = " . $num . "<br />\n";
    $markup .= "<br />\n";

    return $markup;
  }

  // method
  private function output_view() {
    $markup = "";

    $markup .= "<p>view: ";
     $views = array("online", "urls");
    $last_pos = count($views) - 1;
    foreach ($views as $view) {
      $markup .= $this->get_menu_item($view, "view");
      if ($view != $views[$last_pos]) {
        $markup .= " | ";
      }
    }
    $markup .= "</p>\n";

    return $markup;
  }

}
