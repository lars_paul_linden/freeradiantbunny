<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.2 2015-01-18

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/visits.php

include_once("lib/scrubber.php");

class Visits extends Scrubber {

  // given 
  private $given_project_id;
  private $given_plant_list_id;
  private $given_plant_history_event_id;
  private $given_soil_area_id;

  // given_project_id
  public function set_given_project_id($var) {
    $this->given_project_id = $var;
  }
  public function get_given_project_id() {
    return $this->given_project_id;
  }

  // given_plant_list_id
  public function set_given_plant_list_id($var) {
    $this->given_plant_list_id = $var;
  }
  public function get_given_plant_list_id() {
    return $this->given_plant_list_id;
  }

  // given_plant_history_event_id;
  public function set_given_plant_history_event_id($var) {
    $error_message = $this->get_validation_obj()->validate_id($var, "plant_history_event_id");
    if ($error_message) {
      $this->set_error_message($error_message);
    } else {
      $this->given_plant_history_event_id = $var;
    }
  }
  public function get_given_plant_history_event_id() {
    return $this->given_plant_history_event_id;
  }

  // given_soil_area_id
  public function set_given_soil_area_id($var) {
    $this->given_soil_area_id = $var;
  }
  public function get_given_soil_area_id() {
    return $this->given_soil_area_id;
  }

  // attributes
  private $id;
  private $rough_date;
  private $fact;
  private $land_obj;
  private $name;
  private $plant_history_event_obj;
  private $order;
  private $count;
  private $layout;
 
  // id
  public function set_id($var) {
    $this->id = $var;
  }
  public function get_id() {
    return $this->id;
  }

  // rough_date
  public function set_rough_date($var) {
    $this->rough_date = $var;
  }
  public function get_rough_date() {
    return $this->rough_date;
  }

  // fact
  public function set_fact($var) {
    $this->fact = $var;
  }
  public function get_fact() {
    return $this->fact;
  }

  public function get_fact_user_facing() {
    if ($this->fact == "t") {
      return "fact";
    }
    return "plan";
  }

  // todo land_obj was soil_area_obj

  // land_obj
  public function get_land_obj() {
    if (! isset($this->land_obj)) {
      include_once("lands.php");
      $this->land_obj = new Lands($this->get_given_config());
    }
    return $this->land_obj;
  }

  // name
  public function set_name($var) {
    $this->name = $var;
  }
  public function get_name() {
    return $this->name;
  }

  // order
  public function set_order($var) {
    $this->order = $var;
  }
  public function get_order() {
    return $this->order;
  }

  // count
  public function set_count($var) {
    $this->count = $var;
  }
  public function get_count() {
    return $this->count;
  }

  // layout
  public function set_layout($var) {
    $this->layout = $var;
  }
  public function get_layout() {
    return $this->layout;
  }

  // plant_history_event_obj
  public function get_plant_history_event_obj() {
    if (! isset($this->plant_history_event_obj)) {
      include_once("plant_history_events.php");
      $this->plant_history_event_obj = new PlantHistoryEvents($this->get_given_config());
    }
    return $this->plant_history_event_obj;
  }

 
  // method
  private function make_visit() {
    $obj = new Visits($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  public function deal_with_parameters() {
    $markup = "";

    // define parameter namespace
    $parameters = array();

    // create an instance for each parameter
    include_once("lib/parameter.php");
    
    // plant_history_id
    $parameter_a = new Parameter();
    $parameter_a->set_name("plant_history_id");
    $parameter_a->set_validator_function_string("id");
    array_push($parameters, $parameter_a);

    // get parameters (if any) and valirough_date
    $this->process_parameters($parameters);

    // deal with aftermath
    foreach ($parameters as $parameter) {
      if ($parameter->get_error_message()) {
        // error, so get message
        $markup .= $parameter->get_error_message();
      } else {
        // no error, so see if there is a user_value
        if (! $parameter->get_value()) {
          // no users_value, so do nothing
        } else {
          // users_value exists, so store
          // store depending upon the parameter_name
          if ($parameter->get_name() == "plant_history_id") {
            $this->set_given_plant_history_event_id($parameter->get_value());
          }
        }
      }
    }

    return $markup;
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id()) {
      $this->set_type("get_by_id");

    } else if ($this->get_given_plant_list_id()) {
      $this->set_type("get_by_plant_list_id");

    } else if ($this->get_given_plant_history_event_id()) {
      $this->set_type("get_by_plant_history_event_id");

    } else if ($this->get_given_project_id()) {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_soil_area_id()) {
      $this->set_type("get_by_soil_area_id");

    } else {
      $this->set_type("get_all");
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      $sql = "SELECT plant_history_events.id, plant_history_events.plant_history_id, plant_history_events.plant_count, plant_history_events.direct_seed FROM plant_history_events WHERE plant_history_events.id = " . $this->get_given_id() . ";";

    } else if ($this->get_type() == "get_by_soil_area_id") {
      //$sql = "SELECT visits.id, visits.rough_date, visits.fact, visits.soil_area_id, visits.name, visits.plant_history_event_id, plants.id, plants.common_name, varieties.id, varieties.name, suppliers.id, suppliers.name, plant_history_events.plant_count, plant_history_events.direct_seed, visits.order, visits.count FROM soil_areas, visits, plant_history_events, plant_histories, plant_list_plants, plant_lists, plants, varieties, seed_packets, suppliers WHERE plant_history_events.id = visits.plant_history_event_id AND plant_history_events.plant_history_id = plant_histories.id AND plant_histories.plant_list_plant_id = plant_list_plants.id AND plant_list_plants.plant_list_id = plant_lists.id AND soil_areas.id = " . $this->get_given_soil_area_id() . " AND plants.id = plant_list_plants.plant_id AND varieties.id = seed_packets.variety_id AND seed_packets.id = plant_histories.seed_packet_id AND suppliers.id = seed_packets.supplier_id AND soil_areas.id = visits.soil_area_id ORDER BY visits.order, visits.rough_date, soil_areas.layout_id, soil_areas.bed_num, plant_history_events.plant_count;";
      // todo fix the sql below (it is not correct)
      $sql = "select visits.* from visits";

    } else if ($this->get_type() == "get_by_plant_list_id") {
      //$sql = "SELECT visits.id, visits.rough_date, visits.fact, visits.soil_area_id, visits.name, visits.plant_history_event_id, plants.id, plants.common_name, varieties.id, varieties.name, suppliers.id, suppliers.name, plant_history_events.plant_count, plant_history_events.direct_seed, visits.order, visits.count FROM soil_areas, visits, plant_history_events, plant_histories, plant_list_plants, plant_lists, plants, varieties, seed_packets, suppliers WHERE plant_history_events.id = visits.plant_history_event_id AND plant_history_events.plant_history_id = plant_histories.id AND plant_histories.plant_list_plant_id = plant_list_plants.id AND plant_list_plants.plant_list_id = plant_lists.id AND plant_lists.id = " . $this->get_given_plant_list_id() . " AND plants.id = plant_list_plants.plant_id AND varieties.id = seed_packets.variety_id AND seed_packets.id = plant_histories.seed_packet_id AND suppliers.id = seed_packets.supplier_id AND soil_areas.id = visits.soil_area_id ORDER BY visits.order, soil_areas.layout_id, soil_areas.bed_num, plant_history_events.plant_count;";
      // todo fix the sql below it is not accurate
      $sql = "SELECT visits.* FROM visits, plant_histories, plant_list_plants WHERE visits.plant_history_event_id = plant_histories.id AND plant_histories.plant_list_plant_id = plant_list_plants.id AND plant_list_plants.plant_list_id = " . $this->get_given_plant_list_id() . " ORDER BY visits.id;";

    } else if ($this->get_type() == "get_by_plant_history_event_id") {
      $sql = "SELECT visits.* FROM visits WHERE visits.plant_history_event_id = " . $this->get_given_plant_history_event_id() . " ORDER BY visits.id;";

      // debug
      //print "debug visits sql = " . $sql . "<br />\n";

    } else if ($this->get_type() == "get_by_project_id") {
      // a project has plant_lists, so this is a precursor because a user must select a plant list
      $sql = "SELECT plant_lists.id, plant_lists.name FROM plant_lists, plant_list_projects WHERE plant_lists.id = plant_list_projects.plant_list_id AND plant_list_projects.project_id = " . $this->get_given_project_id() . " ORDER BY plant_lists.sort;";

    } else if ($this->get_type() == "get_by_project_id") {

    } else if ($this->get_type() == "get_all") {
      //$sql = "SELECT plant_history_events.id, plant_history_events.plant_history_id, plant_history_events.name, visits.rough_date, plant_history_events.plant_count FROM visits, plant_history_events, plant_histories, plant_list_plants, plant_lists, plant_list_projects, projects WHERE plant_history_events.plant_history_id = plant_histories.id AND plant_histories.plant_list_plant_id = plant_list_plants.id ANd plant_list_plants.plant_list_id = plant_lists.id AND plant_lists.id = plant_list_projects.plant_list_id AND plant_list_projects.project_id = projects.id AND visits.plant_history_event_id = plant_history_events.id ORDER BY visits.rough_date;";
      $sql = "SELECT visits.* FROM visits ORDER BY visits.rough_date;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql);
    }
    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_soil_area_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_visit();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_rough_date(pg_result($results, $lt, 1));
        $obj->set_fact(pg_result($results, $lt, 2));
        //$obj->get_land_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_name(pg_result($results, $lt, 4));
        $obj->get_plant_history_event_obj()->set_id(pg_result($results, $lt, 5));
        $obj->get_plant_history_event_obj()->get_plant_history_obj()->get_plant_list_plant_obj()->get_plant_obj()->set_id(pg_result($results, $lt, 6));
        $obj->get_plant_history_event_obj()->get_plant_history_obj()->get_plant_list_plant_obj()->get_plant_obj()->set_common_name(pg_result($results, $lt, 7));
        $obj->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_variety_obj()->set_id(pg_result($results, $lt, 8));
        $obj->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_variety_obj()->set_name(pg_result($results, $lt, 9));
        $obj->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_supplier_obj()->set_id(pg_result($results, $lt, 10));
        $obj->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_supplier_obj()->set_name(pg_result($results, $lt, 11));
        $obj->get_plant_history_event_obj()->set_plant_count(pg_result($results, $lt, 12));
        $obj->get_plant_history_event_obj()->set_direct_seed(pg_result($results, $lt, 13));
        $obj->set_order(pg_result($results, $lt, 14));
        $obj->set_count(pg_result($results, $lt, 15));
      }
    } else if ($this->get_type() == "get_by_plant_history_event_id" ||
               $this->get_type() == "get_by_plant_list_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_visit();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_rough_date(pg_result($results, $lt, 1));
        $obj->set_fact(pg_result($results, $lt, 2));
        //$obj->get_land_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_name(pg_result($results, $lt, 4));
        $obj->get_plant_history_event_obj()->set_id(pg_result($results, $lt, 5));
        $obj->set_order(pg_result($results, $lt, 6));
        $obj->set_count(pg_result($results, $lt, 7));
        //$obj->get_layout()->set_id(pg_result($results, $lt, 8));
        // debug
        //print "debug visits id = " . $obj->get_id() . "<br />\n";
      }
    } else if ($this->get_type() == "get_by_project_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_visit();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
      }
    } else if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_visit();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_rough_date(pg_result($results, $lt, 1));
        $obj->set_fact(pg_result($results, $lt, 2));
        //$obj->get_land_obj()->set_id(pg_result($results, $lt, 3));
        $obj->set_name(pg_result($results, $lt, 4));
        $obj->get_plant_history_event_obj()->set_id(pg_result($results, $lt, 5));
        $obj->set_order(pg_result($results, $lt, 6));
        $obj->set_count(pg_result($results, $lt, 7));
        //$obj->get_layout()->set_id(pg_result($results, $lt, 8));

      }
    } else {
      return $this->get_db_dash()->output_error("Error: " . get_class($this) . " does not know the type.");
    }

  }

  // method menu 1
  public function output_subsubmenu_get_all() {
    $markup = "";

    if ($this->get_given_plant_list_id() ||
        $this->get_type() == "get_all" ||
        $this->get_given_plant_list_id()) {
      $markup .= "<div class=\"subsubmenu\">\n";
    }
    if ($this->get_given_plant_list_id()) {
      $url = $this->url("plant_lists/" . $this->get_given_plant_list_id());
      $markup .= "To <a href=\"" . $url . "\">Plant List</a><br />\n";
      $markup .= "<br />\n";
    }
    if ($this->get_type() == "get_all") {
      $url = $this->url("plant_histories");
      $markup .= "  See Also: <a href=\"" . $url . "\">PlantHistories</a><br />\n";
    }
    if ($this->get_given_plant_list_id() ||
        $this->get_type() == "get_all" ||
        $this->get_given_plant_list_id()) {
      $markup .= "</div>\n";
    }

    return $markup;
  }

  // method menu 2
  public function output_user_info() {
    $markup = "";

    // todo
    //$markup .= $this->output_user_info_message();

    return $markup;
  }

  // method menu 3
  protected function output_given_variables() {
    $markup = "";

    if ($this->get_type() == "get_by_project_id") {
      $markup .= "<div class=\"given-variables\">\n";
      $markup .= "To list a set of <strong>Visits</strong>, click on a <strong>plant_list</strong> name.";
      $markup .= "</div>\n";
    }
    if ($this->get_type() == "get_by_plant_list_id") {
      $markup .= "<div class=\"given-variables\">\n";
      include_once("plant_lists.php");
      $plant_lists_obj = new PlantLists($this->get_given_config());
      $user_obj = $this->get_user_obj();
      $plant_lists_obj->set_user_obj($user_obj);
      $given_plant_list_id = $this->get_given_plant_list_id();
      $markup .= $plant_lists_obj->output_plant_list_name_given_id($given_plant_list_id);
      $markup .= "</div><!-- given-variables -->\n";
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    if ($this->get_type() == "get_by_project_id") {
      // this lists helps the user select a particular list
      include_once("plant_lists.php");
      $plant_list_obj = new PlantLists($this->get_given_config());
      $list_obj = $this->get_list_bliss()->get_list();
      $type_url = "visits";
      $type_string = "Visits";
      $markup .= $plant_list_obj->output_plant_lists_table($type_url, $type_string, $list_obj);
      return $markup;
    } else {
      $markup .= $this->output_table();
    }
    
    return $markup;
  }


  // method
  public function output_table() {
    $markup = "";

    // preprocess 1 of 2
    // initialize this array for an error report
    // that shows plant_history_events that do not have visits yet
    $error_report_obj_list = new ListBliss();

    // preprocess 2 of 2
    // helps to remove plant common_name when the same as above row
    $flag_plant_history_event_id = "";;

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant history event id #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant history event\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    variety\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    supplier\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    direct seed\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    plant count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"150\">\n";
    $markup .= "    rough date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    fact\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    soil area\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    $num_inside = 0;
    foreach ($this->get_list_bliss()->get_list() as $visit) {
      $num++;

      // save id for error report
      $visit_id = $visit->get_plant_history_event_obj()->get_id();
      $error_report_obj_list->add_item($visit_id);

      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td class=\"header\">\n";
      if ($flag_plant_history_event_id == $visit->get_plant_history_event_obj()->get_id()) {
        // duplicate, so skip
      } else {
        $num_inside++;
        $markup .= "    " . $num_inside . "\n";
      }
      $markup .= "  </td>\n";

      $markup .= "  <td align=\"center\">\n";
      if ($flag_plant_history_event_id == $visit->get_plant_history_event_obj()->get_id()) {
        // duplicate, so skip
      } else {
        $markup .= "    " . $visit->get_plant_history_event_obj()->get_id() . "\n";
      }
      // set for the next iteration
      $flag_plant_history_event_id = $visit->get_plant_history_event_obj()->get_id();
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_plant_history_event_obj()->get_plant_history_obj()->get_plant_list_plant_obj()->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_variety_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      //$markup .= "    " . $visit->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_supplier_obj()->get_name_with_link() . "\n";
      $markup .= "  [not yet developed]\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_plant_history_event_obj()->get_direct_seed_user_facing() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_plant_history_event_obj()->get_plant_count() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_rough_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_fact_user_facing() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      //if (is_numeric($visit->get_land_obj()->get_id())) {
      //  // looks like an idea so create a hyperlink
      //  $url = $this->url("soil_areas/" . $visit->get_land_obj()->get_id()) ;
      //  $markup .= "    <a href=\"" . $url . "\">" . $visit->get_land_obj()->g//et_full_name() . "</a>\n";
      //} else {
      //  $markup .= "    " . $visit->get_land_obj()->get_id() . "\n";
      //}
      $markup .= "  [to be developed]\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    // show error report
    //include_once("lib/errors.php");
    //$errors_obj = new Errors();
    //$class = get_class($this);
    //$plant_list_id = $this->get_given_plant_list_id();
    //$user_obj = $this->get_user_obj();
    //$markup .= $errors_obj->output_missing_error_report($error_report_obj_list, $num, $class, $plant_list_id, $user_obj);

    return $markup;
  }

  // method
  public function output_list() {
    $markup = "";

    // todo fix this function
    // set
    //$this->set_given_plant_id($given_plant_id);
    //$this->set_type("get_variety_given_plant_id");

    // load data from database
    //$this->determine_type();
    //$markup .= $this->prepare_query();

    // only output if there are items to output 
    //if ($this->get_list_bliss()->get_count() > 0) {
    //  $markup .= $this->output_set();
    //}

    return $markup;
  }

  // method
  public function output_set() {
    $markup = "";

    $markup .= "not yet implemented\n";

    return $markup;
  }

  // method
  public function output_table_given_plant_history_event_id($given_plant_history_event_id) {
    $markup = "";

    // set
    $this->set_given_plant_history_event_id($given_plant_history_event_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output 
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_list_table();
    }

    return $markup;
  }

  // method
  public function output_list_bliss_given_plant_history_event_id($given_plant_history_event_id) {
    $markup = "";

    // set
    $this->set_given_plant_history_event_id($given_plant_history_event_id);

    // load data from database

    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output 
    if ($this->get_list_bliss()->get_count() > 0) {
      return $this->get_list_bliss();
    }

    return $markup;
  }

  // method
  public function get_days_since_seeding($given_date) {
    $markup = "";

    if ($given_date < $this->get_rough_date()) {
      $markup .= "-";
    }
    $markup .= $this->get_date_interval($given_date, $this->get_rough_date());

    return $markup;
  }

  // method
  private function get_date_interval($date_string_1, $date_string_2) {
    // get date span

    include_once("lib/dates.php");
    $dates_obj = new Dates();
    $rough_date = $this->get_rough_date();
    $plant_event_date_obj = $dates_obj->get_date_obj($rough_date);

    // example format P2Y4DT6H8M
    if ($given_days_to_maturity) {

      $interval_format = "P" . $given_days_to_maturity . "D";

      //print "debug interval format = " . $interval_format . "<br />";

      $date_interval = new DateInterval($interval_format);

      $date_maturity_obj = date_add($plant_event_date_obj, $date_interval);

      $markup .= $date_maturity_obj->format("Y-m-d");

    } else {
      $markup .= "<p class=\"error\">Error: days_to_maturity is not known.</p>";
    }
    return $markup;
  }

  // method
  private function get_yield($given_date) {
    $markup = "";

    // the seeding might not have even taken place yet
    if ($this->get_rough_date() > $given_date) {

      // seed planting in the figure, so output empty cells
      $markup .= "  <td align=\"right\">\n";
      $markup .= "  </td>\n";
      return $markup;
    }

    // seeds have been planted in virtual time, so continue with output

    $color = "#EEC591";
    $markup .= "  <td style=\"background-color: " . $color . "\" align=\"right\">\n";

    // 5-7
    $markup .= "    " . $this->get_yield_quantity_cells($given_date, $difference);
    // 8  
    $markup .= "    " . $this->get_yield_units_cell($difference);

    return $markup;
  }

  // method
  private function get_yield_units_cell($difference) {
    $markup = "";

    if ($difference >= 0) {
      $color = "#EEC591";
      $markup .= "  <td style=\"background-color: " . $color . "\" align=\"right\">\n";
      $plant_list_plant_id =  $this->get_plant_history_event_obj()->get_plant_history_obj()->get_plant_list_plant_obj()->get_id();
      include_once("plant_list_plants.php");
      $plant_list_plant_obj = new PlantListPlants($this->get_given_config());
      $plant_id = $plant_list_plant_obj->get_plant_id_given_id($plant_list_plant_id, $this->get_user_obj());
      include_once("plant_units.php");
      $plant_unit_obj = new PlantUnits($this->get_given_config());
      $markup .= $plant_unit_obj->get_unit_name_given_plant_id($plant_id);
      $markup .= "\n";
      $markup .= "  </td>\n";
    } else {
      $markup .= "  <td align=\"right\">\n";
      $markup .= "  </td>\n";
    }

    return $markup;
  }

  // todo clean up code that helps sort the visits by rough_dates

      //$rank_rough_date = $plant_history_event->get_rough_date();
      // todo fix the old class_rough_dates code
      // ->output_rough_date_given($last_spring_frost_month, $last_spring_frost_rough_date, $first_fall_frost_month, $first_fall_frost_rough_date);
      //$events_array[$rank_rough_date] = $plant_history_event;
    //}
    // 2 of 3 sort
    //ksort($events_array);
    // 3 of 3 output
    //foreach ($events_array as $rank_rough_date => $plant_history_event) {

    // todo figure out where germination rates impact the system
    // consider germination rates and plantlet survival rates
    // assume 85% of the seeds will not germinate
    // todo take into consideration that the rate drops when seeds are older and other factors
    //$germination_rates = 0.85;
    // todo does this need to consider laws of conditional probability
    //$plant_count_germinated = $plant_count * $germination_rates;
    // remove decimal (because there is no such thing as one half a plant
    //$plant_count_germinated_as_string = number_format($plant_count_germinated, 0);

  // method
  public function output_sidecar($given_user_obj, $given_plant_history_event_id) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);
    $this->set_given_plant_history_event_id($given_plant_history_event_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_sidecar_table();
    }

    return $markup;
  }

  // method
  public function output_sidecar_table() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $markup .= "<tr>\n";
    // column headings
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\" width=\"150\">\n";
    $markup .= "    rough date\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    fact\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    soil area\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $visit) {
      $num++;

      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_id() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_rough_date() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_fact_user_facing() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td style=\"background-color: red;\">\n";
      //if (is_numeric($visit->get_land_obj()->get_id())) {
      //  // looks like an id so create a hyperlink
      //  $url = url("soil_areas/" . $visit->get_land_obj()->get_id()) ;
      //  $markup .= "    <a href=\"" . $url . "\">" . $visit->get_land_obj()->get_full_name() . "</a>\n";
      //} else {
      //  $markup .= "    " . $visit->get_land_obj()->get_id() . "\n";
      //}
      $markup .= "  [to be developed]\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_name() . "\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }

  // method
  public function output_sidecar_given_soil_area_id($given_user_obj, $given_soil_area_id, $given_current_year) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);
    $this->set_given_soil_area_id($given_soil_area_id);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {
      $markup .= $this->output_layout_sowing_table($given_current_year);
    } else {
      // list is empty, so...
      // provide something for markup, a "no" message
      $markup .= "<p>No visits.</p>\n";
    }

    return $markup;
  }

  // method
  public function output_layout_visit_table($given_current_year) {
    $markup = "";
    $skip_year_message = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    $verbose = 1;

    //$markup .= "<tr>\n";
    // column headings
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    #\n";
    //$markup .= "  </td>\n";
    //if ($verbose) {
    //  $markup .= "  <td class=\"header\">\n";
    //  $markup .= "    sowing id\n";
    //  $markup .= "  </td>\n";
    //}
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    plant\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    variety\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\">\n";
    //$markup .= "    supplier\n";
    //$markup .= "  </td>\n";
    //$markup .= "  <td class=\"header\" width=\"150\">\n";
    //$markup .= "    rough date\n";
    //$markup .= "  </td>\n";
    //if ($verbose) {
    //  $markup .= "  <td class=\"header\">\n";
    //  $markup .= "    direct seed\n";
    //  $markup .= "    plant count\n";
    //  $markup .= "    name\n";
    //  $markup .= "  </td>\n";
    //}
    //$markup .= "</tr>\n";

    $num = 0;
    $num_inside = 0;
    $total_count = 0;
    foreach ($this->get_list_bliss()->get_list() as $visit) {
      $num++;

      // filter for given year
      $rough_date = $visit->get_rough_date();
      $year = substr($rough_date, 0, 4);
      //print "debug visits current year " . $given_current_year . "\n";
      //print "this    year " . $year . "\n";
      if (! strcmp($year, $given_current_year)) {
        //print "keep " . $visit->get_rough_date() . "<br />\n";
      } else {
        //print "skip " . $visit->get_rough_date() . "<br />\n";
        //$skip_year_message .= "skipping sowing " . $visit->get_id() . " " . $visit->get_rough_date() . "<br />\n";;
        continue;
      }

      $markup .= "<tr>\n";

      $markup .= "  <td class=\"header\">\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      if ($verbose) {
        $markup .= "  <td>\n";
        $url = url("visits/" . $visit->get_id());
        $markup .= "    <a href=\"" . $url . "\">" . $visit->get_id() . "</a>\n";
        $markup .= "  </td>\n";
      }

        $markup .= "  <td>\n";
        $markup .= "    " . $visit->get_order() . "\n";
        $markup .= "  </td>\n";

        $markup .= "  <td>\n";
        $markup .= "    " . $visit->get_count() . "\n";
        if ($visit->get_count()) {
          $total_count += $visit->get_count();
        }
        $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_plant_history_event_obj()->get_plant_history_obj()->get_plant_list_plant_obj()->get_plant_obj()->get_common_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // variety
      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_variety_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // supplier
      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_plant_history_event_obj()->get_plant_history_obj()->get_seed_packet_obj()->get_supplier_obj()->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      $markup .= "  <td>\n";
      $markup .= "    " . $visit->get_rough_date() . "\n";
      $markup .= "  </td>\n";

      if ($verbose) {
        $markup .= "  <td style=\"font-size:60%\">\n";
        $markup .= "    <ul>\n";
        $markup .= "      <li>" . $visit->get_plant_history_event_obj()->get_direct_seed_user_facing() . "</li>\n";
        $markup .= "      <li>" . $visit->get_plant_history_event_obj()->get_plant_count() . "</li>\n";
        $markup .= "      <li>" . $visit->get_name() . "</li>\n";
        $markup .= "    </ul>\n";
        $markup .= "  </td>\n";
      }

      $markup .= "</tr>\n";
    }
    if ($total_count) {
      $markup .= "<tr>\n";
      $markup .= "  <td colspan=\"3\">\n";
      $markup .= "  total\n";
      $markup .= "  </td>\n";
      $markup .= "  <td>\n";
      $markup .= "  " . $total_count . "\n";
      $markup .= "  </td>\n";
      $markup .= "  <td colspan=\"5\">\n";
      $markup .= "  </td>\n";
      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";
    //$markup .= $skip_year_message;

    return $markup;
  }

  // method
  protected function output_single() {
    $markup = "";

    $markup .= $this->output_aggregate();

    return $markup;
  }

  // method
  public function output_visit_count_given_plant_list_id($given_plant_list_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_plant_list_id($given_plant_list_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($markup) {
      return $markup;
    }   

    // only output if there are items to output
    $count = $this->get_list_bliss()->get_count();
    $markup = "<p>" . $count . "</p>\n";

    return $markup;
  }

}
