<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05
// version 1.1 2014-08-21
// version 1.2 2015-01-14

// about this class
// http://freeradiantbunny.org/main/en/docs/frb/budgets.php

include_once("lib/standard.php");

class Budgets extends Standard {

  // given
  public $given_scene_element_id;
  public $given_posting_id;
  public $given_domain_tli;
  private $given_view = "online"; // default

  // given_scene_element_id;
  public function set_given_scene_element_id($given_scene_element_id) {
    $this->given_scene_element_id = $given_scene_element_id;
  }
  public function get_given_scene_element_id() {
    return $this->given_scene_element_id;
  }

  // given_posting_id;
  public function set_given_posting_id($given_posting_id) {
    $this->given_posting_id = $given_posting_id;
  }
  public function get_given_posting_id() {
    return $this->given_posting_id;
  }

  // given_domain_tli;
  public function set_given_domain_tli($given_domain_tli) {
    $this->given_domain_tli = $given_domain_tli;
  }
  public function get_given_domain_tli() {
    return $this->given_domain_tli;
  }

  // given_view
  public function set_given_view($var) {
    $this->given_view = $var;
  }
  public function get_given_view() {
    return $this->given_view;
  }

  // img_url
  public function get_img_url() {
    return $img_url = "http://mudia.com/dash/_images/budget_stamp.png";
  }

  // attributes
  private $scene_element_obj;

  // scene_element_obj
  public function get_scene_element_obj() {
    if (! isset($this->scene_element_obj)) {
      include_once("scene_elements.php");
      $this->scene_element_obj = new SceneElements($this->get_given_config());
    }
    return $this->scene_element_obj;
  }

  // method
  private function get_name_and_id_with_link() {
    $markup = "";

    include_once("lib/factory.php");
    $factory = new Factory($this->get_given_config());
    $url = $this->url($factory->get_class_name_given_object($this) . "/" . $this->get_id());
    $markup .= "<a href=\"" . $url . "\" class=\"show\">";
    if ($this->get_name()) {
      $markup .= $this->get_name();
      // add id
      $markup .= "&nbsp;";
      $markup .= $this->get_id();
    } else {
      if ($this->get_id()) {
        $markup .= $this->get_id();
      } else {
        // note: is this too dangerous or just a good fallback plan
        if ($this->get_given_id()) {
          $markup .= $this->get_given_id();
        } else {
          // no data was found, so return null string
          print "error standard: no id or given_id<br />\n";
          print "error standard: subclass = " . get_class($this) . "<br />\n";
          return "";
        }
      }
    }
    $markup .= "</a>";

    return $markup;
  }

  // method
  private function make_budget() {
    $obj = new Budgets($this->get_given_config());
    $obj->set_user_obj($this->get_user_obj());
    $this->get_list_bliss()->add_item($obj);
    return $obj;
  }

  // method
  protected function process_command() {
    // todo no commands yet
  }

  // method
  protected function determine_type() {

    if ($this->get_given_id() != "") {
      $this->set_type("get_by_id");

    } else if ($this->get_given_project_id() != "") {
      $this->set_type("get_by_project_id");

    } else if ($this->get_given_scene_element_id() != "") {
      $this->set_type("get_by_scene_element_id");

    } else if ($this->get_given_posting_id() != "") {
      $this->set_type("get_by_posting_id");

    } else if ($this->get_given_domain_tli() != "") {
      $this->set_type("get_by_domain_tli");

    } else {
      // default
      $this->set_type("get_all");
     
    }
  }

  // method
  protected function prepare_query() {
    $markup = "";

    // initialize
    $sql = "";

    // debug
    //print "debug " . $this->get_type() . "<br />";

    // figure out what to load
    if ($this->get_type() == "get_by_id") {
      // security: only get the rows owned by the user
      $sql = "SELECT budgets.*, projects.id, projects.name, projects.img_url FROM scene_elements, budgets, processes, business_plan_texts, goal_statements, projects WHERE projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND processes.id = scene_elements.process_id AND budgets.id = " . $this->get_given_id() . " AND scene_elements.id = budgets.scene_element_id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY budgets.name;";

      // debug
      //print "debug budgets sql = " . $sql . "<br />";

    } else if ($this->get_type() == "get_all") { 
     // security: only get the rows owned by the user
      $sql = "SELECT budgets.* FROM budgets ORDER BY budgets.sort DESC, budgets.name;";

    } else if ($this->get_type() == "get_by_project_id") { 
      // security: only get the rows owned by the user
      $sql = "SELECT budgets.*, projects.id, projects.name, projects.img_url FROM scene_elements, budgets, processes, business_plan_texts, goal_statements, projects WHERE projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND processes.id = scene_elements.process_id AND projects.id = " . $this->get_given_project_id() . " AND scene_elements.id = budgets.scene_element_id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY budgets.name;";

    } else if ($this->get_type() == "get_by_scene_element_id") { 
      // security: only get the rows owned by the user
      $sql = "SELECT budgets.*, projects.id, projects.name, projects.img_url FROM scene_elements, budgets, processes, business_plan_texts, goal_statements, projects WHERE projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND processes.id = scene_elements.process_id AND scene_elements.id = " . $this->get_given_scene_element_id() . " AND scene_elements.id = budgets.scene_element_id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY budgets.name;";

    } else if ($this->get_type() == "get_by_posting_id") { 
      // security: only get the rows owned by the user
      $sql = "SELECT budgets.*, projects.id, projects.name, projects.img_url FROM budgets, scene_elements, processes, business_plan_texts, goal_statements, projects, postings WHERE projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND processes.id = scene_elements.process_id AND postings.id = " . $this->get_given_posting_id() . " AND scene_elements.id = budgets.scene_element_id AND budgets.id = postings.budget_id AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY budgets.name;";

    } else if ($this->get_type() == "get_by_domain_tli") { 
      // security: only get the rows owned by the user
      $sql = "SELECT budgets.*, projects.id, projects.name, projects.img_url FROM scene_elements, budgets, processes, business_plan_texts, goal_statements, projects, domains WHERE projects.id = goal_statements.project_id AND goal_statements.id = business_plan_texts.goal_statement_id AND business_plan_texts.id = processes.business_plan_text_id AND processes.id = scene_elements.process_id AND domains.tli = '" . $this->get_given_domain_tli() . "' AND scene_elements.id = budgets.scene_element_id AND scene_elements.class_name_string = 'domains' AND scene_elements.class_primary_key_string = domains.tli AND projects.user_name = '" . $this->get_user_obj()->name . "' ORDER BY budgets.name;";

    } else {
      $markup .= $this->get_db_dash()->output_error("Error: " . get_class($this) . ": type is not known. Unable to load data.");
    }

    // define database
    $database_name = "plantdot_soiltoil";

    // execute function
    if ($sql) {
      $markup .= parent::load_data($this, $sql, $database_name);
    }

    return $markup;
  }

  // method
  protected function transfer($results) {

    if ($this->get_type() == "get_by_id" ||
        $this->get_type() == "get_by_project_id" ||
        $this->get_type() == "get_by_scene_element_id" ||
        $this->get_type() == "get_by_domain_tli" ||
        $this->get_type() == "get_by_posting_id") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_budget();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->get_scene_element_obj()->set_id(pg_result($results, $lt, 5));
        $obj->get_project_obj()->set_id(pg_result($results, $lt, 6));
        $obj->get_project_obj()->set_name(pg_result($results, $lt, 7));
        $obj->get_project_obj()->set_img_url(pg_result($results, $lt, 8));
      }
    } else if ($this->get_type() == "get_all") {
      for ($lt = 0; $lt < pg_numrows($results); $lt++) {
        $obj = $this->make_budget();
        $obj->set_id(pg_result($results, $lt, 0));
        $obj->set_name(pg_result($results, $lt, 1));
        $obj->set_description(pg_result($results, $lt, 2));
        $obj->set_sort(pg_result($results, $lt, 3));
        $obj->set_status(pg_result($results, $lt, 4));
        $obj->get_scene_element_obj()->set_id(pg_result($results, $lt, 5));
      }
    } else {
      return $this->get_db_dash()->output_error("Sorry, " . get_class($this) . " does not know the type.");
    }

  }

  // method
  public function output_subsubmenu() {
    $markup = "";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<a href=\"project?id=" . $this->get_given_project_id() . "\">Back to project page</a>\n";
    //$markup .= "</div>\n";

    //$markup .= "<div class=\"subsubmenu\">\n";
    //$markup .= "<a href=\"project?id=" . $this->get_given_project_id() . "\">Back to project page</a>\n";
    //$markup .= "</div>\n";

    return $markup;
  }

  // method
  public function output_user_info() {
    $markup = "";

    // only authenticated users
    if ($this->get_user_obj()) {
      // todo fix username
      //if ($this->get_user_obj()->uid) {
      //  $markup .= "<div class=\"subsubmenu-user\">\n";
      //  $markup .= "<strong>These budgets were created by</strong> " . $this->get_user_obj()->name . "<br />\n";
      //  $markup .= "</div>\n";
      //}
    }

    return $markup;
  }

  // method
  protected function output_aggregate() {
    $markup = "";

    // guts of the list
    $markup .= "<table class=\"plants\">\n";

    // column headings
    $markup .= "<tr>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    #\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    project\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    status\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    sort\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    id\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    name\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    description\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    scene_element\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    postings count\n";
    $markup .= "  </td>\n";
    $markup .= "  <td class=\"header\">\n";
    $markup .= "    derived budget net\n";
    $markup .= "  </td>\n";
    $markup .= "</tr>\n";

    // rows
    $num = 0;
    foreach ($this->get_list_bliss()->get_list() as $budget) {
      $markup .= "<tr>\n";

      $num++;

      // nmum
      $markup .= "  <td>\n";
      $markup .= "    " . $num . "\n";
      $markup .= "  </td>\n";

      // project img element
      $markup .= "  <td>\n";
      $padding = "";
      $float = "";
      $width = "65";
      $markup .= "    " . $budget->get_project_obj()->get_img_as_img_element_with_link($padding, $float, $width) . "\n";
      $markup .= "  </td>\n";

      // status
      $markup .= "  <td style=\"text-align: center; background-color: " . $budget->get_status_background_color() . "; font-size: 50%;\">\n";
      $markup .= "    " . $budget->get_status() . "\n";
      $markup .= "  </td>\n";

      // sort
      $markup .= $budget->get_sort_cell();

      // id with link
      $markup .= "  <td>\n";
      $markup .= "    " . $budget->get_id_with_link() . "<br >\n";
      $markup .= "  </td>\n";

      // name with link
      $markup .= "  <td>\n";
      $markup .= "    " . $budget->get_name_with_link() . "\n";
      $markup .= "  </td>\n";

      // description
      $markup .= "  <td>\n";
      $markup .= "    " . $budget->get_description() . "\n";
      $markup .= "  </td>\n";

      //
      $markup .= "  <td>\n";
      $scene_element_id = $budget->get_scene_element_obj()->get_id();
      $user_obj = $this->get_user_obj();
      $markup .= "    " . $budget->get_scene_element_obj()->get_name_with_link_given_id($scene_element_id, $user_obj) . "\n";
      $markup .= "  </td>\n";

      // postings count
      include_once("postings.php");
      $postings_obj = new Postings($this->get_given_config());
      $budget_id = $budget->get_id();
      $user_obj = $this->get_user_obj();
      $postings_count = $postings_obj->get_postings_count_given_budget_id($budget_id, $user_obj);
      $markup .= "  <td>\n";
      $url = $this->url("budgets/" . $budget->get_id());
      $markup .= "    <a href=\"" . $url . "\">" . $postings_count . "</a>\n";
      $markup .= "  </td>\n";

      // derived budget net
      $markup .= "  <td>\n";
      $url = $this->url("budgets/" . $budget->get_id());
      $user_obj = $this->get_user_obj();
      //$markup .= "    <a href=\"" . $url . "\">" . $this->get_derived_budget_net($user_obj) . "</a>\n";
      $markup .= "  </td>\n";

      $markup .= "</tr>\n";
    }
    $markup .= "</table>\n";

    return $markup;
  }


  // method
  protected function output_single_nonstandard() {
    $markup = "";

    // todo removed this because the design changed
    // scene_element
    //$markup .= "<h3>SceneElement</h3>\n";
    //include_once("scene_elements.php");
    //$scene_element_obj = new SceneElements($this->get_given_config());
    //$budget_id = $this->get_id();
    //$user_obj = $this->get_user_obj();
    //$markup .= $scene_element_obj->output_scene_element_given_budget_id($budget_id, $user_obj);

    // name with link
    $markup .= "<p>project = " . $this->get_project_obj()->get_name_with_link() . "</p>\n";

    // accounts
    $markup .= "<h3>Accounts</h3>\n";
    include_once("budget_accounts.php");
    $budget_accounts_obj = new BudgetAccounts($this->get_given_config());
    $budget_id = $this->get_id();
    $user_obj = $this->get_user_obj();
    $markup .= $budget_accounts_obj->output_accounts_list_given_budget_id($budget_id, $user_obj);

    // postings
    $markup .= "<h3>Postings</h3>\n";
    include_once("postings.php");
    $postings_obj = new Postings($this->get_given_config());
    $budget_id = $this->get_id();
    $user_obj = $this->get_user_obj();
    $markup .= $postings_obj->output_sidecar($budget_id, $user_obj);

    return $markup;
  }

  // method
  public function get_project_id_given_budget_id($given_budget_id, $given_user_obj) {
    $markup = "";

    // set
    $this->set_given_id($given_budget_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() > 0) {

      $list_obj = $this->get_list_bliss()->get_list();
      $obj = $list_obj[0];
      $project_id = $obj->get_project_obj()->get_id();
      //print "debug plant list projects = " . $project_id . "<br />\n"; 
      $markup .= $project_id;
    }

    return $markup;
  }

  // method
  public function get_build_given_id($given_project_id, $given_user_obj) {
    $markup = "";

    $this->set_given_project_id($given_project_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // return null string
      $markup .= "";
      return $markup;
    }

    $markup .= "<h3 style=\"background-color: #EFEFEF;padding: 2px 2px 2px 2px;\">budgets</h3>\n";

    // rows
    foreach ($this->get_list_bliss()->get_list() as $budget) {

      $user_obj = $this->get_user_obj();
      $budget->set_user_obj($user_obj);

      // debug
      //print "debug budgets given_user_obj name " . $budget->get_user_obj()->name . "<br />\n";

      $markup .= "  <p>\n";
      $markup .= "    " . $budget->get_id() . "<br >\n";
      $markup .= "  </p>\n";

      $markup .= "  <p>\n";
      $markup .= "    " . $budget->get_name_with_hidden_link() . "\n";
      $markup .= "  <p>\n";

      $markup .= "  <p>\n";
      $markup .= "    " . $budget->get_description() . "\n";
      $markup .= "  </p>\n";

      $user_obj = $this->get_user_obj();
      $markup .= $budget->get_build($user_obj);

    }

    return $markup;
  }

  // method
  public function get_build($given_user_obj) {
    $markup = "";

    // postings
    $markup .= "<h3>Postings</h3>\n";
    include_once("postings.php");
    $postings_obj = new Postings($this->get_given_config());
    $budget_id = $this->get_id();
    $user_obj = $this->get_user_obj();
    $markup .= $postings_obj->get_build($budget_id, $user_obj);

    return $markup;
  }

  // method
  public function get_budgets_list_string_given_scene_element_id($given_scene_element_id, $given_user_obj) {
    $markup = "";

    $this->set_given_scene_element_id($given_scene_element_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no budgets.</p>\n";;
      return $markup;
    }

    // rows
    foreach ($this->get_list_bliss()->get_list() as $budget) {

      $markup .= "  <p>\n";
      $markup .= "    budget " . $budget->get_id() . " " . $budget->get_name_with_link() . "\n";
      $markup .= "  </p>\n";

    }

    return $markup;
  }

  // method
  public function get_budget_list_given_posting_id($given_posting_id, $given_user_obj) {
    $markup = "";

    $this->set_given_posting_id($given_posting_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no budgets.</p>\n";;
      return $markup;
    }

    // rows
    foreach ($this->get_list_bliss()->get_list() as $budget) {

      $markup .= "  <p>\n";
      $markup .= "    budget " . $budget->get_id() . " " . $budget->get_name_with_link() . "\n";
      $markup .= "  </p>\n";

    }

    return $markup;
  }

  // method
  public function get_budget_list_given_scene_element_id($given_scene_element_id, $given_user_obj) {
    $markup = "";

    $this->set_given_scene_element_id($given_scene_element_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      $markup .= "<p style=\"error\">no budgets.</p>\n";;
      return $markup;
    }

    // rows
    foreach ($this->get_list_bliss()->get_list() as $budget) {

      $markup .= "  <p>\n";
      $markup .= "    budget " . $budget->get_id() . " " . $budget->get_name_with_link() . "\n";
      $markup .= "  </p>\n";

    }

    return $markup;
  }

  // method
  public function get_budget_list_given_domain_tli($given_domain_tli, $given_user_obj) {
    $markup = "";

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // return null string
      $markup .= "";
      return $markup;
    }
  }

  // method
  public function get_budgets_summary_given_domain_tli($given_domain_tli, $given_user_obj, $given_start_date) {
    $markup = "";

    // reset
    $this->get_list_bliss()->empty_list();

    $this->set_given_domain_tli($given_domain_tli);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    if ($this->get_list_bliss()->get_count() > 0) {
      foreach ($this->get_list_bliss()->get_list() as $budget) {
        $markup .= $budget->get_id_with_link() . " " . $budget->get_name_with_link() . "<br />\n";
      }
    } else {
      $markup .= "<p>[No budgets found.]</p>\n";
    }

    return $markup;
  }

  // method
  public function get_budgets_summary_given_scene_element_id($given_scene_element_id, $given_user_obj, $given_start_date) {
    $markup = "";

    // reset
    $this->get_list_bliss()->empty_list();

    $this->set_given_scene_element_id($given_scene_element_id);
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();

    $markup .= $this->prepare_query();

    // only output if there are items to output
    if ($this->get_list_bliss()->get_count() < 1) {
      // return null string
      $markup .= "";
      return $markup;
    }

    // rows
    foreach ($this->get_list_bliss()->get_list() as $budget) {

      //$markup .= "budget: ";
      $markup .= $budget->get_name_and_id_with_link() . "\n";
      // todo note that the 2015 business is hard-coded (alert user here)
      //$markup .= "2015: ";
      $markup .= $budget->get_total() . "<br />\n";
      // note the following produces a great deal of information
      //$markup .= $budget->get_due($given_start_date) . "<br />\n";

    }

    return $markup;
  }

  // method
  public function get_derived_budget_net($given_user_obj, $given_domain_tli = "") {
    $markup = "";
 
    // todo not sure what this should be, so null string for now
    $given_start_date = "";

    $markup .= $this->get_budgets_summary_given_domain_tli($given_domain_tli, $given_user_obj, $given_start_date);
 
    return $markup;
  }

  // method
  public function get_derived_budget_net_given_scene_element_id_list(array $given_scene_element_id_list, $given_user_obj) {
    $markup = "";
 
    // todo not sure what this should be, so null string for now
    $given_start_date = "";

    foreach ($given_scene_element_id_list as $scene_element_id) {
      // debug
      //$markup .= "debug budgets scene_element_id = " . $scene_element_id . "<br />\n";
      $markup .= $this->get_budgets_summary_given_scene_element_id($scene_element_id, $given_user_obj, $given_start_date);
    }
 
    return $markup;
  }

  // method
  private function get_due($given_start_date) {
    $markup = "";

    // postings
    include_once("postings.php");
    $posting_obj = new Postings($this->get_given_config());
    $budget_id = $this->get_id();
    $user_obj = $this->get_user_obj();
    $markup .= $posting_obj->get_simple_table($budget_id, $user_obj, $given_start_date);
 
    return $markup;
  }

  // method
  private function get_total() {
    $markup = "";

    // postings
    include_once("postings.php");
    $postings_obj = new Postings($this->get_given_config());
    $budget_id = $this->get_id();
    $user_obj = $this->get_user_obj();
    $total_only_flag = "1";
    $markup .= $postings_obj->output_sidecar($budget_id, $user_obj, $total_only_flag);

    return $markup;
  }

  // method
  public function get_count($given_user_obj) {
    $markup = "";

    // set
    $this->set_user_obj($given_user_obj);

    // load data from database
    $this->determine_type();
    $markup .= $this->prepare_query();

    // only output if there are items to output
    return $this->get_list_bliss()->get_count();
  }

}
