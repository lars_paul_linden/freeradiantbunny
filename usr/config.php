<?php

// FreeRadiantBunny
// Copyright (C) 2014 Lars Paul Linden
// see README.txt

// log
// version 1.0 2014-07-05

// about this file
// This class contains the user's preferences

class Config {

  // user-defined variable
  // user should defind the base here so that embedded url links work
  // in other words, this helps the resolving of links for stylesheets
  // base_url
  private $base_url = "http://localhost/~username/frb-demo/";

  // method
  public function get_base_url() {
    return $this->base_url;
  }

  // user-defined variable
  // these are names of classes in url style format (with underscores)
  // known_class_names
  private $known_class_names = array("projects", "goal_statements", "business_plan_texts", "processes", "scene_elements", "project_projects", "process_flows", "classes", "budgets", "tools", "books", "hyperlinks", "keywords", "machines", "moneymakers", "designs", "design_instances", "plant_lists", "webpages", "builds", "lands", "domains", "applications", "suppliers", "designers", "email_addresses", "harvests", "shares", "process_profiles", "timecards", "tickets", "pickups");

  // method
  public function get_known_class_names() {
    return $this->known_class_names;
  }

  // databases
  private $databases = array("example_database_name");

  // method
  public function get_default_database_name() {
    return $this->databases[0];
  }

  // connection_string
  // add new connectins using associative array
  // this is the design of the array: database_name => connectino string
  private $connection_strings = array("example_database_name" => "dbname=YOUR_DATABASE_NAME user=YOUR_DATABASE_USERNAME password=YOUR_PASSWORD_HERE host=localhost");

  // method
  public function get_connection_string($given_database_name) {
    foreach ($this->databases as $database) {
      if ($given_database_name == $database) {
        return $this->connection_strings[$database];
      }
    }

    // connection_string not found, so return null string
    // if not found, return first element as default
    $default_database = $this->get_default_database_name();
    return $this->connection_strings[$default_database];
  }

  // site_name
  private $site_name = "FreeRadiantBunny Demo";

  // method
  public function get_site_name() {
    return $this->site_name;
  }

  // tagline
  private $tagline = "This is a demo of FreeRadiantBunny.";

  // method
  public function get_tagline() {
    return $this->tagline;
  }

  // description
  private $description = "This is a demo of FreeRadiantBunny. FreeRadiantBunny is an information system for developing a permaculture-based Site Analysis & Assessment report.";

  // method
  public function get_description() {
    return $this->description;
  }

  // author
  private $author = "YOUR_NAME <YOUR_EMAIL@ADDRESS.COM>.";

  // method
  public function get_author() {
    return $this->author;
  }

  // debug
  private $debug;

  // method
  public function set_debug($var) {
    $this->debug = $var;
  }
  // method
  public function get_debug() {
    return $this->debug;
  }

  // debug
  private $site_logo = "logo.png";

  // method
  public function get_site_logo() {
    return $this->site_logo;
  }

  // debug
  private $image_dir = "_images/";

  // method
  public function get_image_dir() {
    return $this->image_dir;
  }

  // method
  public function get_site_logo_full_path() {
    return $this->get_base_url() . $this->get_image_dir() . $this->get_site_logo();
  }
  
  // method
  public function get_links_for_footer() {
    return "<a href=\"projects\">projects</a><br />\n";
  }
  
}
